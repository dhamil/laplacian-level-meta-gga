import numpy as np
from itertools import product

def get_fcc_vecs(a):
    dlv = np.zeros((3,3))
    for i in range(3):
        dlv[i,i] = a
    bas = np.zeros((4,3))
    for i in range(1,4):
        for j in range(3):
            if i-1 == j:
                continue
            bas[i,j] = a/2
    return dlv,bas

def make_fcc_supercell(a,Nx,Ny,Nz):

    sc_dlv,fcc_bas = get_fcc_vecs(1)
    basv = []
    nscv = product(np.arange(0,Nx,1),np.arange(0,Ny,1),np.arange(0,Nz,1))

    for iv in nscv:
        ix,iy,iz = iv
        for abas in fcc_bas:
            tmpv = np.einsum('i,ij->j',[ix,iy,iz],sc_dlv) + abas
            if tmpv[0] < Nx and tmpv[1] < Ny and tmpv[2] < Nz:
                basv.append([x for x in tmpv])
    basv = a*np.asarray(basv)
    dlv = np.zeros((3,3))
    dlv[0,0] = a*Nx
    dlv[1,1] = a*Ny
    dlv[2,2] = a*Ny
    return dlv, basv

def make_fcc_supercell_poscar(a,Nx,Ny,Nz,elt,sysname=None):
    dlv,bas = make_fcc_supercell(1,Nx,Ny,Nz)

    nbas = bas.shape[0]

    ccell = np.array([Nx/2,Ny/2,Nz/2])
    #centroid = [np.sum(bas[:,i])/nbas for i in range(3)]

    dev = np.zeros(nbas)
    for i in range(nbas):
        dev[i] = np.sum((ccell - bas[i])**2)**(0.5)
    icen = np.argmin(dev)

    tstr = '{:}\n'.format(sysname)
    tstr += '   {:}\n'.format(a)
    for i in range(3):
        tstr += '     {:}    {:}    {:}\n'.format(*dlv[i])
    tstr += '   {:}\n'.format(elt)
    tstr += '     {:}\n'.format(nbas)
    tstr += 'C\n'
    for i in range(nbas):
        tstr += '  {:}  {:}  {:}\n'.format(*bas[i])
    with open('POSCAR_sc','w+') as tmpfl:
        tmpfl.write(tstr)

    tstr = '{:}\n'.format(sysname)
    tstr += '   {:}\n'.format(a)
    for i in range(3):
        tstr += '     {:}    {:}    {:}\n'.format(*dlv[i])
    tstr += '   {:}\n'.format(elt)
    tstr += '     {:}\n'.format(nbas-1)
    tstr += 'C\n'
    for i in range(nbas):
        if i == icen:
            continue
        tstr += '  {:}  {:}  {:}\n'.format(*bas[i])
    with open('POSCAR_sc_mv','w+') as tmpfl:
        tmpfl.write(tstr)


    return

if __name__=="__main__":

    uopts = {}
    with open('cpars.txt','r') as tfl:
        for iln,aln in enumerate(tfl):
            tmp = [x.strip() for x in aln.strip().split('=')]
            if tmp[0] == 'a':
                tval = float(tmp[1])
            elif tmp[0] in ['Nx','Ny','Nz']:
                tval = int(tmp[1])
            else:
                tval = tmp[1]
            uopts[tmp[0].strip()] = tval
    make_fcc_supercell_poscar(uopts['a'],uopts['Nx'],uopts['Ny'],uopts['Nz'],uopts['elt'],\
        sysname=uopts['sys'])
