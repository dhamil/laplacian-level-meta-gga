from os import sys,path
import numpy as np
from sjeos import conv_en,sjeos_fit
from glob import glob

pref_order = ['CA','PE','PS','SCAN','R2SCAN','R2SL','R2SL_corrected_ST','OFR2','OFR2_corrected_ST']

def ptmv_analysis(wdir='./'):
    sjeos_pars = sjeos_fit(wdir=wdir+'sjeos/ev_data/')
    a0_sjeos = (4*sjeos_pars['V0'])**(1/3)
    dlv = np.zeros((3,3))
    with open(wdir+'supercell_no_relax/POSCAR','r') as tfl:
        for iln,aln in enumerate(tfl):
            if iln == 1:
                scl = float(aln.split()[0])
            elif 1 < iln < 5:
                dlv[iln-2] = [scl*float(u) for u in aln.split()]
            elif iln == 6:
                break
    a0_sc_rel = abs(np.linalg.det(dlv))**(1/3)/2
    sc_pars = conv_en(wdir+'supercell_no_relax/OSZICAR')
    mv_pars = conv_en(wdir+'supercell_mv/OSZICAR')
    emvf = mv_pars['E'] - 31/32*sc_pars['E']
    tstr = 'a0 SJEOS (A0), a0 SC relax (A0), E_MVF (eV)\n'
    tstr += '{:}, {:}, {:}'.format(a0_sjeos,a0_sc_rel,emvf)
    with open(wdir+'./Pt_MV.csv','w+') as tfl:
        tfl.write(tstr)
    return a0_sjeos,a0_sc_rel,emvf

if __name__ == "__main__":

    wdir = './'
    if len(sys.argv) > 1:
        if sys.argv[1] == 'all':
            resd = {}
            tstr = 'DFA, a0 SJEOS (A0), a0 SC relax (A0), E_MVF (eV)\n'
            tstr_tex = 'DFA & $a_0$ SJEOS (\\AA{}) & $a_0$ SC relax (\\AA{}) & $E_\\mathrm{MVF}$ (eV) \\\\ \n'
            for adir in glob('Pt_MV_*'):
                if path.isfile(adir):
                    continue
                fnl = adir[6:]
                resd[fnl] = {}
                resd[fnl]['a0 EOS'],resd[fnl]['a0 rel'],resd[fnl]['E_MVF'] = \
                    ptmv_analysis(wdir=adir+'/')

            for fnl in pref_order:
                if fnl in resd:
                    tstr += '{:}, {:}, {:}, {:}\n'.format(fnl,resd[fnl]['a0 EOS'],\
                        resd[fnl]['a0 rel'],resd[fnl]['E_MVF'])
                    tstr_tex += '{:} & {:.3f} & {:.3f} & {:.3f} \\\\ \n'.format(\
                        fnl,resd[fnl]['a0 EOS'], resd[fnl]['a0 rel'],resd[fnl]['E_MVF'])
            with open('./Pt_MV_all.csv','w+') as tfl:
                tfl.write(tstr)
            with open('./Pt_MV_all.tex','w+') as tfl:
                tfl.write(tstr_tex)
            exit()
        else:
            wdir = 'Pt_MV_'+sys.argv[1]+'/'
    ptmv_analysis(wdir=wdir)
