#!/bin/bash
#PBS -l walltime=2:00:00:00
#PBS -q normal
#PBS -l nodes=2:ppn=20
#PBS -N Pt_PE_sc
#PBS -j oe
#PBS -o oe_sc.txt
#PBS -m a
#PBS -M kaplan@temple.edu

module --force purge ; module load intel-libs
cd "$PBS_O_WORKDIR"

vasp_exec=/home/tuf53878/vasp6_lst/bin/vasp_std
rm -rf supercell_no_relax
mkdir supercell_no_relax
cp INCAR_sc_toten supercell_no_relax/INCAR
cp POTCAR supercell_no_relax/
cp KPOINTS supercell_no_relax/
cp POSCAR_sc supercell_no_relax/POSCAR

cd supercell_no_relax
mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec
