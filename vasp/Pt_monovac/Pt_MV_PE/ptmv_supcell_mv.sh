#!/bin/bash
#PBS -l walltime=2:00:00:00
#PBS -q normal
#PBS -l nodes=2:ppn=20
#PBS -N Pt_PE_mv
#PBS -j oe
#PBS -o oe_mv.txt
#PBS -m a
#PBS -M kaplan@temple.edu

module --force purge ; module load intel-libs
cd "$PBS_O_WORKDIR"

vasp_exec=/home/tuf53878/vasp6_lst/bin/vasp_std
rm -rf supercell_mv_isif2
mkdir supercell_mv_isif2
cp INCAR_sc supercell_mv_isif2/INCAR
cp POTCAR supercell_mv_isif2/
cp KPOINTS supercell_mv_isif2/
cp POSCAR_sc_mv supercell_mv_isif2/POSCAR

cd supercell_mv_isif2
mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec
mkdir relax
for tmp in OUTCAR OSZICAR POSCAR INCAR EIGENVAL vasprun.xml DOSCAR PROCAR ; do 
  mv $tmp relax/
done
mv CONTCAR POSCAR
cp ../INCAR_sc_toten ./INCAR
mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec
rm WAVECAR