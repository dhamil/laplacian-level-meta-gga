#!/bin/bash
#PBS -l walltime=24:00:00
#PBS -q normal
#PBS -l nodes=1:ppn=20
#PBS -N Pt_PE_sjeos
#PBS -j oe
#PBS -o oe_sjeos.txt
#PBS -m a
#PBS -M kaplan@temple.edu

module --force purge ; module load intel-libs
cd "$PBS_O_WORKDIR"

sysname=Pt
struc=fcc

vasp_exec=/home/tuf53878/vasp6_lst/bin/vasp_std
writer_exec=/home/tuf53878/llmgga_testing/PTMV/poscar_writer.py

sjeos_exec=/home/tuf53878/llmgga_testing/PTMV/sjeos.py
v_to_a_exec=/home/tuf53878/llmgga_testing/PTMV/vol_to_cubic_lp.py
scposwr=/home/tuf53878/llmgga_testing/PTMV/mk_fcc_supercell.py

rm -rf sjeos
mkdir -p sjeos/ev_data

rm -f POSCAR_sc POSCAR_sc_mv
cp INCAR_sp sjeos/INCAR
cp POTCAR sjeos/
cd sjeos

for apar in 3.778 3.802 3.825 3.849 3.873 3.897 3.921 3.944 3.968 3.992 4.016 4.039 ; do 
  python3 $writer_exec -symm=$struc -a=$apar -name=$sysname
  mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec
  mv OSZICAR "./ev_data/osz_$apar.txt"
  mv DOSCAR "./ev_data/dos_$apar.txt"
  mv OUTCAR "./ev_data/out_$apar.txt"
  mv POSCAR "./ev_data/pos_$apar.txt"
  mv vasprun.xml "./ev_data/vrun_$apar.xml"
  mv EIGENVAL "./ev_data/eig_$apar.txt"
  mv PROCAR "./ev_data/pro_$apar.txt"
done

rm WAVECAR 
cd ev_data ; python3 $sjeos_exec ; cd ../..

v0="$(grep 'V0' ./sjeos/ev_data/sjeos_fit.csv)"
v0=${v0:3}
a0=$(python3 $v_to_a_exec -symm=$struc -v=$v0 -print)
echo $a0
printf "a=$a0\nNx=2\nNy=2\nNz=2\nsys=Pt\nelt=Pt" > cpars.txt
python3 $scposwr

