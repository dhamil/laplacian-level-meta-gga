from os import sys

def main():
    opts = ['symm','a','b','c','coa','name']
    wd = {'name':'Unknown'}
    instr = sys.argv[1:]
    for it_str,istr in enumerate(instr):
        if istr[0] == '-':
            tv = istr[1:].split('=')
            if tv[0] in opts:
                wd[tv[0]]=tv[1]

    ostr = wd['name']+'\n'
    if wd['symm'] == 'sc':
        ostr += '    {:}\n'.format(wd['a'])
        ostr += '  1.0 0.0 0.0\n'+'  0.0 1.0 0.0\n'+'  0.0 0.0 1.0\n'
        ostr += ' 1\n'+' d\n'+ '0.0 0.0 0.0'
    elif wd['symm'] in ['fcc','ds','rs','zb']:
        ostr += '    {:}\n'.format(wd['a'])
        ostr += '  0.0 0.5 0.5\n'+'  0.5 0.0 0.5\n'+'  0.5 0.5 0.0\n'
        if wd['symm'] == 'fcc':
            ostr += ' 1\n'+' d\n'+ '0.0 0.0 0.0'
        elif wd['symm'] == 'ds':
            ostr += ' 2\n'+' d\n'+ '-0.125 -0.125 -0.125\n'+ ' 0.125  0.125  0.125\n'
        elif wd['symm'] == 'rs':
            ostr += ' 1 1\n'+' d\n' + '0.0 0.0 0.0\n'+'0.5 0.5 0.5\n'
        elif wd['symm'] == 'zb':
            ostr += ' 1 1\n'+' d\n'+ '0.0 0.0 0.0\n'+ ' 0.25  0.25  0.25\n'
    elif wd['symm'] == 'bcc':
        ostr += '    {:}\n'.format(wd['a'])
        ostr += '  -0.5  0.5  0.5\n'+'   0.5 -0.5  0.5\n'+'   0.5  0.5 -0.5\n'
        ostr += ' 1\n'+' d\n'+ '0.0 0.0 0.0'
    elif wd['symm'] == 'hcp':
        if 'c' in wd and 'coa' not in wd:
            wd['coa'] = float(wd['c'])/float(wd['a'])
        else:
            wd['coa']=float(wd['coa'])
        ostr += '    {:}\n'.format(wd['a'])
        ostr += '1.0000000000000000  0.0000000000000000  0.0000000000000000\n'
        ostr += '0.5000000000000000  0.8660254037844386  0.0000000000000000\n'
        ostr += '0.0000000000000000  0.0000000000000000  {:.16f}\n'.format(wd['coa'])
        ostr += '2\n' + ' d\n'
        ostr += '0.0000000000000000  0.0000000000000000  0.0000000000000000\n'
        ostr += '0.3333333333333333  0.3333333333333333  0.5000000000000000\n'
    ofl = open('POSCAR','w+')
    ofl.write(ostr)
    ofl.close()

if __name__=="__main__":
    main()
