import numpy as np
from os import system

gga_regex = ['PE','PS']
mgga_regex = ['SCAN','R2SCAN','R2SL','OFR2','TASK']

# from H. Pan, et al., Phys. Rev. B 85, 014111 (2012); DOI 10.1103/PhysRevB.85.014111
a0=3.913

def job_scripts(fnl):
    wdir = './Pt_MV_{:}/'.format(fnl)

    str = "#!/bin/bash\n#PBS -l walltime=24:00:00\n#PBS -q normal\n\
#PBS -l nodes=1:ppn=20\n#PBS -N Pt_{:}_sjeos\n#PBS -j oe\n#PBS -o oe_sjeos.txt\n\
#PBS -m a\n#PBS -M kaplan@temple.edu\n\n".format(fnl)

    str += 'module --force purge ; module load intel-libs\ncd "$PBS_O_WORKDIR"\n\n'

    str+='sysname=Pt\nstruc=fcc\n\n'

    str+='vasp_exec=/home/tuf53878/vasp6_lst/bin/vasp_std\n'
    str+='writer_exec=/home/tuf53878/llmgga_testing/PTMV/poscar_writer.py\n\n'
    str+='sjeos_exec=/home/tuf53878/llmgga_testing/PTMV/sjeos.py\n'
    str+='v_to_a_exec=/home/tuf53878/llmgga_testing/PTMV/vol_to_cubic_lp.py\n'
    str +='scposwr=/home/tuf53878/llmgga_testing/PTMV/mk_fcc_supercell.py\n\n'

    vsrch = 0.1
    npts = 12

    ascl = a0*np.linspace((1-vsrch)**(1/3),(1 + vsrch)**(1/3),npts)
    al = [round(tmp,3) for tmp in ascl]

    str+='rm -rf sjeos\nmkdir -p sjeos/ev_data\n\n'

    str += 'rm -f POSCAR_sc POSCAR_sc_mv\n'
    str += 'cp INCAR_sp sjeos/INCAR\n'
    str += 'cp POTCAR sjeos/\n'
    str += 'cd sjeos\n\n'

    str += 'for apar in '
    for ap in al:
        str += '{:} '.format(ap)
    str += "; do \n  python3 $writer_exec -symm=$struc -a=$apar -name=$sysname\n"
    str += "  mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec\n"
    str += '  mv OSZICAR "./ev_data/osz_$apar.txt"\n'
    str += '  mv DOSCAR "./ev_data/dos_$apar.txt"\n'
    str += '  mv OUTCAR "./ev_data/out_$apar.txt"\n'
    str += '  mv POSCAR "./ev_data/pos_$apar.txt"\n'
    str += '  mv vasprun.xml "./ev_data/vrun_$apar.xml"\n'
    str += '  mv EIGENVAL "./ev_data/eig_$apar.txt"\n'
    str += '  mv PROCAR "./ev_data/pro_$apar.txt"\ndone\n\n'
    str += 'rm WAVECAR \n'

    str += 'cd ev_data ; python3 $sjeos_exec ; cd ../..\n\n'
    str += 'v0="$(grep '+ "'V0'" + ' ./sjeos/ev_data/sjeos_fit.csv)"\nv0=${v0:3}\n'
    str += 'a0=$(python3 $v_to_a_exec -symm=$struc -v=$v0 -print)\necho $a0\n'

    str += 'printf "a=$a0\\nNx=2\\nNy=2\\nNz=2\\nsys=Pt\\nelt=Pt" > cpars.txt\n'
    str += 'python3 $scposwr\n\n'

    tmpfl = open(wdir+'/'+'ptmv_sjeos.sh','w+')
    tmpfl.write(str)
    tmpfl.close()

    str = "#!/bin/bash\n#PBS -l walltime=2:00:00:00\n#PBS -q normal\n\
#PBS -l nodes=2:ppn=20\n#PBS -N Pt_{:}_sc\n#PBS -j oe\n#PBS -o oe_sc.txt\n\
#PBS -m a\n#PBS -M kaplan@temple.edu\n\n".format(fnl)

    str += 'module --force purge ; module load intel-libs\ncd "$PBS_O_WORKDIR"\n\n'
    str+='vasp_exec=/home/tuf53878/vasp6_lst/bin/vasp_std\n'

    str += 'rm -rf supercell\n'
    str += 'mkdir supercell\n'
    str += 'cp INCAR_sc supercell/INCAR\n'
    str += 'cp POTCAR supercell/\n'
    str += 'cp KPOINTS supercell/\n'
    str += 'cp POSCAR_sc supercell/POSCAR\n\n'
    str += 'cd supercell\n'
    str += "mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec\n"
    str += 'mkdir relax\n'
    str += 'for tmp in OUTCAR OSZICAR POSCAR INCAR EIGENVAL vasprun.xml DOSCAR PROCAR ; do \n'
    str += '  mv $tmp relax/\n'
    str += 'done\n'
    str += 'mv CONTCAR POSCAR\n'
    str += 'cp ../INCAR_sc_toten ./INCAR\n'
    str += "mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec\n"
    str += 'rm WAVECAR'
    #str += 'cd ..\n\n'
    tmpfl = open(wdir+'/'+'ptmv_supcell.sh','w+')
    tmpfl.write(str)
    tmpfl.close()

    str = "#!/bin/bash\n#PBS -l walltime=2:00:00:00\n#PBS -q normal\n\
#PBS -l nodes=2:ppn=20\n#PBS -N Pt_{:}_mv\n#PBS -j oe\n#PBS -o oe_mv.txt\n\
#PBS -m a\n#PBS -M kaplan@temple.edu\n\n".format(fnl)

    str += 'module --force purge ; module load intel-libs\ncd "$PBS_O_WORKDIR"\n\n'
    str+='vasp_exec=/home/tuf53878/vasp6_lst/bin/vasp_std\n'

    str += 'rm -rf supercell_mv\n'
    str += 'mkdir supercell_mv\n'
    str += 'cp INCAR_sc supercell_mv/INCAR\n'
    str += 'cp POTCAR supercell_mv/\n'
    str += 'cp KPOINTS supercell_mv/\n'
    str += 'cp POSCAR_sc_mv supercell_mv/POSCAR\n\n'
    str += 'cd supercell_mv\n'
    str += "mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec\n"
    str += 'mkdir relax\n'
    str += 'for tmp in OUTCAR OSZICAR POSCAR INCAR EIGENVAL vasprun.xml DOSCAR PROCAR ; do \n'
    str += '  mv $tmp relax/\n'
    str += 'done\n'
    str += 'mv CONTCAR POSCAR\n'
    str += 'cp ../INCAR_sc_toten ./INCAR\n'
    str += "mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec\n"
    str += 'rm WAVECAR'

    tmpfl = open(wdir+'/'+'ptmv_supcell_mv.sh','w+')
    tmpfl.write(str)
    tmpfl.close()

    return wdir

def incar_script(ofl='./INCAR',opts={},wcalc='sp'):

    if wcalc == 'sp':
        # params for single-point SJEOS calc.
        tagd = {'SYSTEM': 'unknown', 'NCORE': 4, 'KPAR': 5, 'ALGO': 'Normal', 'PREC': 'Accurate',
        'EDIFF': 1.e-6, 'ENCUT': 800, 'ISMEAR': -5, 'GGA': 'PE', 'ISTART': 1, 'NELM': 200,
        'NELMIN': 6, 'NSW': 0, 'IBRION': -1, 'KSPACING': 0.08, 'KGAMMA': True,
        'ADDGRID': False, 'LMIXTAU': True, 'LASPH': True, 'LMAXMIX': 4, 'LREAL': False,
        'LWAVE': True, 'LCHARG': False, 'LVTOT': False, 'LORBIT': 11
        }

    elif wcalc == 'scr':
        # params for relaxation with stress tensor
        tagd = {'SYSTEM': 'unknown', 'NCORE': 4, 'KPAR': 5, 'ALGO': 'Normal', 'PREC': 'Accurate',
        'EDIFF': 1.e-6, 'ENCUT': 800, 'ISMEAR': 1, 'SIGMA': 0.2, 'GGA': 'PE', 'NELM': 200,
        'NELMIN': 6, 'NSW': 100, 'IBRION': 2, 'NSW': 100, 'ISIF': 3, 'EDIFFG': -0.001,
        'ADDGRID': False, 'LMIXTAU': True, 'LASPH': True, 'LMAXMIX': 4, 'LREAL': 'Auto',
        'LWAVE': True, 'LCHARG': False, 'LVTOT': False, 'LORBIT': 11
        }

    elif wcalc == 'scf':
        # params for total energy calc with stress tensor
        tagd = {'SYSTEM': 'unknown', 'NCORE': 4, 'KPAR': 5, 'ALGO': 'Normal', 'PREC': 'Accurate',
        'EDIFF': 1.e-6, 'ENCUT': 800, 'ISMEAR': -5, 'GGA': 'PE', 'NELM': 200,
        'NELMIN': 6, 'NSW': 0, 'IBRION': -1, 'ISTART': 1,
        'ADDGRID': False, 'LMIXTAU': True, 'LASPH': True, 'LMAXMIX': 4, 'LREAL': False,
        'LWAVE': False, 'LCHARG': False, 'LVTOT': False, 'LORBIT': 11
        }

    for akey in opts:
        tagd[akey] = opts[akey]

    npad = 10
    str = ''
    for akey in tagd:
        padstr = ' '*(npad - len(akey))
        if akey in ['EDIFF','EDIFFG']:
            str += '{:}{:}= '.format(akey,padstr) + ('{:} \n'.format(tagd[akey])).upper()
        elif type(tagd[akey]) is bool:
            str += '{:}{:}= .{:}. \n'.format(akey,padstr,tagd[akey])
        else:
            str += '{:}{:}= {:} \n'.format(akey,padstr,tagd[akey])

    tmpfl = open(ofl,'w+')
    tmpfl.write(str)
    tmpfl.close()
    return

if __name__=="__main__":

    for fnl in ['PE','PS','SCAN','R2SCAN','R2SL','OFR2']:
        optd = {}
        if fnl in gga_regex:
            optd['GGA'] = fnl
        elif fnl in mgga_regex:
            optd['GGA'] = 'PE'
            optd['METAGGA'] = fnl
        wdir = './Pt_MV_{:}/'.format(fnl)
        system('rm -rf {:}'.format(wdir))
        system('mkdir {:}'.format(wdir))

        job_scripts(fnl)
        kstr = 'Automesh\n0\nG\n11 11 11\n0.0 0.0 0.0'
        with open(wdir+'KPOINTS','w+') as tfl:
            tfl.write(kstr)

        optd['SYSTEM'] = 'Pt single pt'
        incar_script(ofl=wdir+'/INCAR_sp',opts=optd,wcalc='sp')

        optd['SYSTEM'] = 'Pt sup cell toten'
        incar_script(ofl=wdir+'/INCAR_sc_toten',opts=optd,wcalc='scf')

        optd['SYSTEM'] = 'Pt sup cell rel'
        incar_script(ofl=wdir+'/INCAR_sc',opts=optd,wcalc='scr')
        system('cp ./POTCAR_Pt_pv ./Pt_MV_{:}/POTCAR'.format(fnl))
