from os import sys

def main():

    wd = {'nion': 1,'print':False}

    opts = ['v','symm']
    instr = sys.argv[1:]
    for it_str,istr in enumerate(instr):
        if istr[0] == '-':
            tv = istr[1:].split('=')
            if tv[0] in opts:
                wd[tv[0]]=tv[1]
            elif tv[0] == 'print':
                wd['print']=True
    wd['v'] = float(wd['v'])
    if wd['symm'] == 'sc':
        wd['fac'] = 1.0
    elif wd['symm'] == 'bcc':
        wd['fac'] = 2.0
    elif wd['symm'] in ['fcc','ds','rs','zb']:
        wd['fac'] = 4.0
        if wd['symm'] in ['ds','rs','zb']:
            wd['nion'] = 2.0
    a0 = (wd['v']*wd['fac']*wd['nion'])**(1/3)
    if wd['print']:
        print(a0)
    return a0

if __name__=='__main__':

    main()
