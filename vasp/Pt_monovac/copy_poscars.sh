#!/bin/bash

for dfa in PE PS SCAN R2SCAN R2SL OFR2 ; do
  cp "./SJEOS_POSCARS/$dfa/POSCAR_sc" "./Pt_MV_$dfa/POSCAR_sc"
  cp "./SJEOS_POSCARS/$dfa/POSCAR_sc_mv" "./Pt_MV_$dfa/POSCAR_sc_mv"
done
