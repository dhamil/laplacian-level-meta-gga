
from os import system

source_dir = '/Volumes/backup_on_the_go/research/llmgga_data/Pt_monovac/'
dest_dir = '/Users/aaronkaplan/Dropbox/phd.nosync/lap_lev_mgga/vasp/Pt_monovac/'

dfas = ['PE','PS','SCAN','R2SCAN','R2SL','OFR2']
system('rm -rf {:}SJEOS_POSCARS/'.format(dest_dir))
system('rm -f {:}SJEOS_POSCARS.tar'.format(dest_dir))
for dfa in dfas:
    system('mkdir -p {:}SJEOS_POSCARS/{:}'.format(dest_dir,dfa))
    system('cp {:}Pt_MV_{:}/POSCAR_sc {:}SJEOS_POSCARS/{:}/'.format(source_dir,dfa,dest_dir,dfa))
    system('cp {:}Pt_MV_{:}/POSCAR_sc_mv {:}SJEOS_POSCARS/{:}/'.format(source_dir,dfa,dest_dir,dfa))
system('tar --disable-copyfile -cvf {:}SJEOS_POSCARS.tar {:}SJEOS_POSCARS'.format(dest_dir,dest_dir))
