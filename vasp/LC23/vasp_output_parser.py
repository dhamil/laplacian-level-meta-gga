from os import listdir, getcwd
import numpy as np

from eos_fits import eos_fit

def conv_en(flnm,all_stage=False):

    pars = {}
    # F is Helmholtz free energy, F = E - TS, where finite T (and S) comes from smearing
    # E is energy extrapolated without smearing, E = F + TS
    # dE is change in energy between penultimate and ultimate runs
    # mag is the magnetiziation within the calculation cell

    its = 0
    with open(flnm,'r') as infl:
        for iln,ln in enumerate(infl):
            ln = ln.split()
            if all_stage:
                indx = ln[0]
            else:
                indx = flnm
            if ln[1] == 'F=':
                its = its + int(oln[1])
                if len((ln[7][1:]).split('E'))==1:
                    dE_temp = 0.0
                else:
                    dE_temp=float(ln[7][1:])
                if len(ln) == 10:
                    # ISPIN = 2
                    pars[indx]={'F': float(ln[2]), 'E': float(ln[4]), 'dE': dE_temp, 'mag': float(ln[9]), 'its': its}
                else:
                    # ISPIN = 1, spin unpolarized
                    pars[indx]={'F': float(ln[2]), 'E': float(ln[4]), 'dE': dE_temp, 'mag': 0.0, 'its': its}
            oln = ln
    if all_stage:
        return pars
    else:
        return pars[indx]

def get_doscar_vol(wdir=None):

    if wdir is not None:
        dir = wdir
    else:
        dir = getcwd()
    osz_l = []
    for fli in listdir(dir):
        if fli.startswith('osz_'):
            osz_l.append(fli)
    nosz = len(osz_l)

    dos_l = ['dos'+x[3:] for x in osz_l]
    vol_l = np.zeros(nosz)
    en_l = np.zeros(nosz)

    for ifl,fl in enumerate(osz_l):

        with open(dir +'/'+dos_l[ifl],'r') as tinfl:
            for iln,ln in enumerate(tinfl):
                ln = (ln.strip()).split()
                if iln == 0:
                    nion = float(ln[0])
                elif iln == 1:
                    vol_l[ifl] = nion*float(ln[0])
                    break

        tpars=conv_en(dir +'/'+fl)
        en_l[ifl] = tpars['E']

    isort = np.argsort(vol_l)
    vol_l = vol_l[isort]
    en_l = en_l[isort]

    return vol_l,en_l


def hcp_coa_vols(wdir=None):

    if wdir is not None:
        dir = wdir
    else:
        dir = getcwd()
    osz_l = []
    for fli in listdir(dir):
        if fli.startswith('osz_'):
            osz_l.append(fli)
    nosz = len(osz_l)

    pos_l = ['pos'+x[3:] for x in osz_l]
    vol_l = np.zeros(nosz)
    en_l = np.zeros(nosz)
    a = np.zeros((3,3))

    for ifl,fl in enumerate(osz_l):

        with open(dir +'/'+pos_l[ifl],'r') as tinfl:
            for iln,ln in enumerate(tinfl):
                ln = (ln.strip()).split()
                if iln == 1:
                    scl = float(ln[0])
                elif 1 < iln < 5:
                    a[iln-2] = [scl*float(u) for u in ln]
                elif iln == 5:
                    break
        # first get area of triangle in plane, this is 1/2 |a1 x a2|
        area = 0.5*(np.sum(np.cross(a[0],a[1])**2))**(0.5)
        # for hcp, this area is Sqrt(3)*a**2/4 --> get a
        apar = (4*area/3**(0.5))**(0.5)
        vol = np.abs(np.linalg.det(a))
        # for hcp, primitive volume is sqrt(3)/2 * a**3 * (c/a) --> get c/a
        coa = 2*vol/(3**(0.5)*apar**3)
        vol_l[ifl] = coa

        tpars=conv_en(dir +'/'+fl)
        en_l[ifl] = tpars['E']
        nion = 1.0

    isort = np.argsort(vol_l)
    vol_l = vol_l[isort]
    en_l = en_l[isort]

    return vol_l,en_l

def get_poscar_vol(wdir=None):

    if wdir is not None:
        dir = wdir
    else:
        dir = getcwd()
    osz_l = []
    for fli in listdir(dir):
        if fli.startswith('osz_'):
            osz_l.append(fli)
    nosz = len(osz_l)

    pos_l = ['pos'+x[3:] for x in osz_l]
    vol_l = np.zeros(nosz)
    en_l = np.zeros(nosz)
    dlv = np.zeros((3,3))

    for ifl,fl in enumerate(osz_l):
        vol_l[ifl] = cell_vol_from_poscar(dir +'/'+pos_l[ifl])

    isort = np.argsort(vol_l)
    vol_l = vol_l[isort]
    en_l = en_l[isort]

    return vol_l,en_l

def cell_vol_from_poscar(fl):
    with open(fl,'r') as tinfl:
        for iln,ln in enumerate(tinfl):
            ln = (ln.strip()).split()
            if iln == 1:
                scl = float(ln[0])
            elif 1 < iln < 5:
                dlv[iln-2] = [scl*float(u) for u in ln]
            elif iln == 5:
                break
    return abs(np.linalg.det(a))

if __name__ == "__main__":

    vol,en = get_doscar_vol()
    np.savetxt('./e_v_curve.csv',np.transpose((vol,en)),delimiter=',',header='Volume (A0**3), Energy (eV)')

    for ffn in ['MURN','VIN','BM','SJEOS']:
        fpars = eos_fit(vol,en,fit_fn=ffn)
        with open('./eos_{:}_fit_pars.csv'.format(ffn),'w+') as tfl:
            for akey in fpars:
                tfl.write('{:}, {:}\n'.format(akey,fpars[akey]))
