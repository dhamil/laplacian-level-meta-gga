#!/bin/bash
#PBS -l walltime=12:00:00
#PBS -q normal
#PBS -l nodes=1:ppn=20
#PBS -N sjeos_GaAs_PE
#PBS -j oe
#PBS -o oe.txt
#PBS -m a
#PBS -M kaplan@temple.edu

module --force purge ; module load intel-libs
cd "$PBS_O_WORKDIR"

sysname=GaAs
struc=zb

vasp_exec=/home/tuf53878/vasp6/bin/vasp_std
writer_exec=/home/tuf53878/llmgga_testing/LC23/poscar_writer.py
v_to_a_exec=/home/tuf53878/llmgga_testing/LC23/vol_to_cubic_lp.py
gap_exec=/home/tuf53878/llmgga_testing/LC23/bandgaps_helper.py

rm -f WAVECAR CHGCAR CHG

rm -rf ev_data
mkdir -p ev_data

for apar in 5.446 5.481 5.515 5.549 5.583 5.618 5.652 5.686 5.72 5.755 5.789 5.823 ; do 
  python3 $writer_exec -symm=$struc -a=$apar -name=$sysname
  mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec
  mkdir "./ev_data/$apar" 
  mv OSZICAR "./ev_data/$apar/"
  mv DOSCAR "./ev_data/$apar/"
  mv OUTCAR "./ev_data/$apar/"
  mv POSCAR "./ev_data/$apar/"
  mv EIGENVAL "./ev_data/$apar/"
  mv vasprun.xml "./ev_data/$apar/"
  mv IBZKPT "./ev_data/$apar/"
  mv PROCAR "./ev_data/$apar/"
done


rm -rf gap ; mkdir gap

printf -v aeq "%.3f" "$a0"
python3 $writer_exec -symm=$struc -a=$aeq -name=$sysname

mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec

python3 $gap_exec incar GGA=PE

mv INCAR INCAR_init
cp INCAR_init gap/INCAR_init
mv OSZICAR gap/OSZ_init
mv OUTCAR gap/OUT_init
mv DOSCAR gap/DOS_init
mv EIGENVAL gap/EIG_init
mv vasprun.xml gap/vasprun_init
mv IBZKPT gap/IBZKPT_init
mv PROCAR gap/PROCAR_init
mv INCAR_gap INCAR

mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec

mv INCAR INCAR_gap
mv INCAR_init INCAR

python3 $gap_exec gap

mv OSZICAR gap/OSZ_gap
mv OUTCAR gap/OUT_gap
mv DOSCAR gap/DOS_gap
mv EIGENVAL gap/EIG_gap

rm WAVECAR