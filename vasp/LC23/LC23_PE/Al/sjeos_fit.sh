#!/bin/bash
#PBS -l walltime=12:00:00
#PBS -q normal
#PBS -l nodes=1:ppn=20
#PBS -N sjeos_Al_PE
#PBS -j oe
#PBS -o oe.txt
#PBS -m a
#PBS -M kaplan@temple.edu

module --force purge ; module load intel-libs
cd "$PBS_O_WORKDIR"

sysname=Al
struc=fcc

vasp_exec=/home/tuf53878/vasp6/bin/vasp_std
writer_exec=/home/tuf53878/llmgga_testing/LC23/poscar_writer.py
v_to_a_exec=/home/tuf53878/llmgga_testing/LC23/vol_to_cubic_lp.py

rm -f WAVECAR CHGCAR CHG

rm -rf ev_data
mkdir -p ev_data

for apar in 3.88 3.905 3.929 3.954 3.978 4.002 4.027 4.051 4.076 4.1 4.124 4.149 ; do 
  python3 $writer_exec -symm=$struc -a=$apar -name=$sysname
  mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec
  mkdir "./ev_data/$apar" 
  mv OSZICAR "./ev_data/$apar/"
  mv DOSCAR "./ev_data/$apar/"
  mv OUTCAR "./ev_data/$apar/"
  mv POSCAR "./ev_data/$apar/"
  mv EIGENVAL "./ev_data/$apar/"
  mv vasprun.xml "./ev_data/$apar/"
  mv IBZKPT "./ev_data/$apar/"
  mv PROCAR "./ev_data/$apar/"
done


rm WAVECAR