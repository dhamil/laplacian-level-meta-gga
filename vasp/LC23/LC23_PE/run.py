from os import system

sols = [
    'Li','Na', 'K', 'Rb', 'Cs', 'Ca', 'Sr', 'Ba','Al', 'Cu', 'Rh', 'Pd', 'Ag',
    'C', 'SiC', 'Si', 'Ge', 'GaAs', 'LiF', 'LiCl', 'NaF', 'NaCl', 'MgO'
    ]

elts = ['Li', 'Na', 'Ca', 'K', 'Rb', 'Cs', 'Sr', 'Ba', 'Al', 'Cu', 'Rh',
'Pd', 'Ag', 'C', 'Si', 'Ge','Ga','As','F','Cl','Mg','O']

for sol in sols:
    wstr = 'cd {:} ; printf '.format(sol)+'"%s "'+' "$(echo {:} ; qsub sjeos_fit.sh)" '.format(sol)
    #wstr = 'cd LC20_POTCARs/{:} ; printf '.format(sol)+'"%s "'+' $( echo {:} ; grep TITEL POTCAR ) "\n"'.format(sol)
    system(wstr)

for sol in elts:
    wstr = 'cd atoms/{:} ; printf '.format(sol)+'"%s "'+' "$(echo {:} ; qsub runcalc.sh)" '.format(sol)
    system(wstr)
