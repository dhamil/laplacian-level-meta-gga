#!/bin/bash
#PBS -l walltime=12:00:00
#PBS -q normal
#PBS -l nodes=1:ppn=20
#PBS -N sjeos_Ba_PE
#PBS -j oe
#PBS -o oe.txt
#PBS -m a
#PBS -M kaplan@temple.edu

module --force purge ; module load intel-libs
cd "$PBS_O_WORKDIR"

sysname=Ba
struc=bcc

vasp_exec=/home/tuf53878/vasp6/bin/vasp_std
writer_exec=/home/tuf53878/llmgga_testing/LC23/poscar_writer.py
v_to_a_exec=/home/tuf53878/llmgga_testing/LC23/vol_to_cubic_lp.py

rm -f WAVECAR CHGCAR CHG

rm -rf ev_data
mkdir -p ev_data

for apar in 4.831 4.862 4.892 4.922 4.953 4.983 5.014 5.044 5.074 5.105 5.135 5.166 ; do 
  python3 $writer_exec -symm=$struc -a=$apar -name=$sysname
  mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec
  mkdir "./ev_data/$apar" 
  mv OSZICAR "./ev_data/$apar/"
  mv DOSCAR "./ev_data/$apar/"
  mv OUTCAR "./ev_data/$apar/"
  mv POSCAR "./ev_data/$apar/"
  mv EIGENVAL "./ev_data/$apar/"
  mv vasprun.xml "./ev_data/$apar/"
  mv IBZKPT "./ev_data/$apar/"
  mv PROCAR "./ev_data/$apar/"
done


rm WAVECAR