#!/bin/bash
#PBS -l walltime=12:00:00
#PBS -q normal
#PBS -l nodes=1:ppn=20
#PBS -N sjeos_Rb_PE
#PBS -j oe
#PBS -o oe.txt
#PBS -m a
#PBS -M kaplan@temple.edu

module --force purge ; module load intel-libs
cd "$PBS_O_WORKDIR"

sysname=Rb
struc=bcc

vasp_exec=/home/tuf53878/vasp6/bin/vasp_std
writer_exec=/home/tuf53878/llmgga_testing/LC23/poscar_writer.py
v_to_a_exec=/home/tuf53878/llmgga_testing/LC23/vol_to_cubic_lp.py

rm -f WAVECAR CHGCAR CHG

rm -rf ev_data
mkdir -p ev_data

for apar in 5.387 5.421 5.455 5.489 5.523 5.557 5.591 5.625 5.658 5.692 5.726 5.76 ; do 
  python3 $writer_exec -symm=$struc -a=$apar -name=$sysname
  mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec
  mkdir "./ev_data/$apar" 
  mv OSZICAR "./ev_data/$apar/"
  mv DOSCAR "./ev_data/$apar/"
  mv OUTCAR "./ev_data/$apar/"
  mv POSCAR "./ev_data/$apar/"
  mv EIGENVAL "./ev_data/$apar/"
  mv vasprun.xml "./ev_data/$apar/"
  mv IBZKPT "./ev_data/$apar/"
  mv PROCAR "./ev_data/$apar/"
done


rm WAVECAR