import numpy as np
from scipy.optimize import leastsq,bisect
from scipy.interpolate import splrep,splev

def murnaghan_eos(v,c):
    v0,e0,b0,b1 = c
    return e0 - b0*v0/(b1 - 1) + b0*v/(b1**2 - b1)*((v0/v)**b1 + b1 - 1)

def murnaghan_jac(v,c):
    jac = np.zeros((v.shape[0],4))
    v0,e0,b0,b1 = c
    x = v0/v
    jac[:,0] = -b0/(b1 - 1) + b0/(b1 - 1)*x**(b1-1)
    jac[:,1] = 1.0
    jac[:,2] =  -v0/(b1 - 1) + v/(b1**2 - b1)*( x**b1 + b1 - 1)
    jac[:,3] = b0*v0/(b1-1)**2 - b0*v*(2*b1 - 1)/(b1**2 - b1)**2*(x**b1 + b1 - 1)\
        + b0*v/(b1**2 - b1)*( x**b1*np.log(x) + 1)
    return jac

def vinet_eos(v,c):
    v0,e0,b0,b1 = c
    x = (v/v0)**(1/3)
    fac = 4*b0*v0/(b1-1)**2
    arg = 3/2*(b1-1)*(1-x)
    efac = (arg - 1)*np.exp(arg)
    return e0 + fac*(1 + efac)

def vinet_jac(v,c):
    jac = np.zeros((v.shape[0],4))
    v0,e0,b0,b1 = c
    x = (v/v0)**(1/3)
    fac = 4*b0*v0/(b1-1)**2

    d_x_dv0 = -x/(3*v0)
    cf = 3/2*(b1-1)
    arg = cf*(1-x)
    earg = np.exp(arg)
    v_int = 1 + (arg - 1)*earg

    d_eos_dx = -fac*cf*(1 + (arg-1))*earg
    jac[:,0] = 4*b0/(b1-1)**2*v_int + d_eos_dx*d_x_dv0
    jac[:,1] = 1

    jac[:,2] = 4*v0/(b1-1)**2*v_int
    jac[:,3] = -2*fac/(b1-1)*v_int + 3/2*(1-x)*fac*(1 + (arg-1) )*earg
    return jac

def birch_murnaghan_eos(v,c):
    t = 1/v**(2/3)
    return c[0] + t*(c[1] + t*(c[2] + t*c[3]))

def sjeos_eos(v,c):
    t = 1/v**(1/3)
    return c[0] + t*(c[1] + t*(c[2] + t*c[3]))

def sjeos_eos_dict(v,d):
    x = (d['V0']/v)**(1/3)
    return d['omega'] + x*(d['gamma'] + x*(d['beta'] + x*d['alpha']))


def eos_fit(v,en,fit_fn='SJEOS',soft_exit=False):

    # default values help catch if fitting routine fails
    opars = {'V0': -1, 'B0': -1, 'B1': -1, 'E0': 1e20, 'R2': 0.0}
    eavg = np.sum(en)/en.shape[0]

    if fit_fn in ['SJEOS','BM']:
        # linear fit

        if fit_fn == 'SJEOS':
            vfac = 1
            vexp = 3
            feos = sjeos_eos
        elif fit_fn == 'BM':
            vfac = 2
            vexp = 3/2
            feos = birch_murnaghan_eos

        cmat = np.zeros((en.shape[0],4))
        for i in range(4):
            cmat[:,i] = v**(-vfac*i/3)

        sps,res,_ ,_ = np.linalg.lstsq(cmat,en,rcond=None)
        rdcnd = sps[2]**2 - 3*sps[3]*sps[1]
        if rdcnd < 0:
            if soft_exit:
                print('WARNING: {:} fit failed, imaginary volumes!'.format(fit_fn))
            else:
                raise SystemExit('WARNING: {:} fit failed, imaginary volumes!'.format(fit_fn))
            return opars

        v0m = (-(sps[2] + rdcnd**(0.5))/sps[1])**vexp
        e0m = 1e20
        if v0m > 0.0:
            e0m = feos(v0m,sps)

        v0p = ((-sps[2] + rdcnd**(0.5))/sps[1])**vexp
        e0p = 1e20
        if v0p > 0.0:
            e0p = feos(v0p,sps)

        if e0m < e0p and v0m > 0.0:
            opars['V0'] = v0m
        elif e0m > e0p and v0p > 0.0:
            opars['V0'] = v0p

        if fit_fn == 'SJEOS':
            # need these to describe other thermodyanamic quantities
            opars['alpha'] = sps[3]/opars['V0']
            opars['beta'] = sps[2]/opars['V0']**(2/3)
            opars['gamma'] = sps[1]/opars['V0']**(1/3)
            opars['omega'] = sps[0]

            opars['B0'] = (18*opars['alpha'] + 10*opars['beta'] \
                + 4*opars['gamma'])/(9*opars['V0'])
            opars['B1'] = (108*opars['alpha'] + 50*opars['beta'] \
                + 16*opars['gamma'])/(27*opars['B0']*opars['V0'])
            opars['E0'] = opars['alpha'] + opars['beta'] + opars['gamma'] + opars['omega']

        elif fit_fn == 'BM':

            opars['B1'] = 2*(8*sps[2] + 7*sps[1]*opars['V0']**(2/3))\
                /( 3*(sps[2] + sps[1]*opars['V0']**(2/3)) )
            opars['B0'] = 16*sps[3]/(9*(opars['B1']-4)*opars['V0']**3)
            opars['E0'] = sps[0] + 9/16*opars['B0']*opars['V0']*(opars['B1']-6)

        for i in range(4):
            tkey = 'c{:}'.format(i)
            opars[tkey] = sps[i]

        srs = (feos(v,sps) - en)**2

    elif fit_fn in ['MURN','VIN']:

        # nonlinear solvers

        iemin = np.argmin(en)
        v00 = v[iemin]
        e00 = en[iemin]
        hin = min(len(en)-1,iemin+1)
        lin = max(0,iemin-1)
        # B(V) = V E''(V). Use finite difference to approx this
        # B1(V) = d B / d P, don't have a good way to guess that
        b01 = v[iemin]*(en[hin]-2*en[iemin]+ en[lin])/(0.5*(v[hin]-v[lin]))**2
        b10 = 1.5
        pars = [v00,e00,b01,b10]

        if fit_fn == 'MURN':
            obj = lambda c : murnaghan_eos(v,c)-en
            jac = lambda c : murnaghan_jac(v,c)
        elif fit_fn == 'VIN':
            obj = lambda c : vinet_eos(v,c)-en
            jac = lambda c : vinet_jac(v,c)

        fps,suc = leastsq(obj,pars,Dfun=jac)
        opars['V0'] = fps[0]
        opars['E0'] = fps[1]
        opars['B0'] = fps[2]
        opars['B1'] = fps[3]

        srs = obj(fps)**2
        
    elif fit_fn == 'SPLINE':

        minind = np.argmin(en)
        splps = splrep(v, en, s=0)
        sinterp = lambda x : splev(x, splps, der=0)
        dinterp = lambda x : splev(x, splps, der=1)
        minpars = bisect(dinterp,v[max(0,minind-1)], \
            v[min(v.shape[0]-1,minind+1)],full_output=True,maxiter=500)
        if not minpars[1].converged:
            print('WARNING, bisection could not locate minimum, code = {:}'.format(minpars[1].flag))
        opars['V0'] = minpars[0]
        opars['E0'] = sinterp(opars['V0'])
        opars['B0'] = opars['V0']*splev(opars['V0'],splps,der=2)
        opars['SPLINE_PARS'] = splps
        srs = sinterp(v) - en

    else:
        raise SystemExit('Unknown fitting function {:}'.format(fit_fn))


    opars['R2'] = 1 - np.sum(srs)/np.sum((en - eavg)**2)

    return opars


if __name__ == "__main__":

    env = np.array([[33.51118,-3.5334649],[34.13374,-3.5367674],
    [34.78934,-3.5394347],[35.42758,-3.5413234],[36.09958,-3.5425646],
    [36.78003,-3.5431276],[37.44232,-3.5430619],[38.13948,-3.5423918],
    [38.81795,-3.5411959],[39.53203,-3.5393638],[40.22686,-3.5371432],
    [40.95807,-3.5343387]])

    mps = eos_fit(env[:,0],env[:,1],fit_fn='MURN',soft_exit=False)
    print('M',mps)
    bmps = eos_fit(env[:,0],env[:,1],fit_fn='BM',soft_exit=False)
    print('BM',bmps)
    vps = eos_fit(env[:,0],env[:,1],fit_fn='VIN',soft_exit=False)
    print('Vinet',vps)
    sps = eos_fit(env[:,0],env[:,1],fit_fn='SJEOS',soft_exit=False)
    print('SJEOS',sps)
    #print(mps['R2'],bmps['R2'],vps['R2'],sps['R2'])

    import matplotlib.pyplot as plt
    vl = np.linspace(env[:,0].min(),env[:,0].max(),5000)
    plt.plot(vl,murnaghan_eos(vl,[mps['V0'],mps['E0'],mps['B0'],mps['B1']]))
    plt.plot(vl,birch_murnaghan_eos(vl,[bmps['c0'],bmps['c1'],bmps['c2'],bmps['c3']])\
        ,linestyle='--')
    plt.plot(vl,vinet_eos(vl,[vps['V0'],vps['E0'],vps['B0'],vps['B1']]),linestyle='-.')
    plt.plot(vl,sjeos_eos(vl,[sps['c0'],sps['c1'],sps['c2'],sps['c3']]),linestyle=':')
    plt.scatter(env[:,0],env[:,1])
    plt.show()
