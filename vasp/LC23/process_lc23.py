from os import sys,system
from glob import glob
import numpy as np

from sjeos import sjeos_fit, conv_en

"""
    reference cubic lattice constant data with zero-point corrections
    (in angstrom) from
    P. Hao, Y. Fang, J. Sun, G.I. Csonka, P.H.T. Philipsen, and J.P. Perdew,
    Phys. Rev. B 85, 014111 (2012), https://doi.org/10.1103/PhysRevB.85.014111

    Except Cs, which was taken from Table S1 of the SI of
    F. Tran, J. Stelzl, and P. Blaha, J. Chem. Phys. 144, 204120 (2016),
    https://doi.org/10.1063/1.4948636
"""
a0_ref = {
'Li': 3.451, 'Na': 4.207, 'K': 5.211, 'Rb': 5.580, 'Cs': 6.043,
'Ca': 5.555, 'Sr': 6.042,
'Ba': 5.004,'Al': 4.019, 'Cu': 3.595, 'Rh': 3.793,
'Pd': 3.876, 'Ag': 4.063, 'C': 3.555, 'SiC': 4.348,
'Si': 5.422, 'Ge': 5.644, 'GaAs': 5.641, 'LiF': 3.974,
'LiCl': 5.072, 'NaF': 4.570, 'NaCl': 5.565, 'MgO': 4.188
}

eV_ang3_to_gpa = 1.602176634e2 # https://physics.nist.gov/cgi-bin/cuu/Value?e

"""
    bulk moduli in GPa with zero point corrections,
    from Table S8 from the SI of
    F. Tran, J. Stelzl, and P. Blaha, J. Chem. Phys. 144, 204120 (2016),
    https://doi.org/10.1063/1.4948636
"""
b0_ref = {
    'Li': 13.1, 'Na': 7.9, 'K': 3.8, 'Rb': 3.6, 'Cs': 2.3,
    'Ca': 15.9, 'Sr': 12.0,
    'Ba': 10.6, 'Al': 77.1, 'Cu': 144.3, 'Rh': 277.1,
    'Pd': 187.2, 'Ag': 105.7, 'C': 454.7, 'SiC': 229.1,
    'Si': 101.3, 'Ge': 79.4, 'GaAs': 76.7, 'LiF': 76.3,
    'LiCl': 38.7, 'NaF': 53.1, 'NaCl': 27.6, 'MgO': 169.8
}

"""
    cohesive energies in eV/atom with thermal and zero point corrections,
    from Table S15 from the SI of
    F. Tran, J. Stelzl, and P. Blaha, J. Chem. Phys. 144, 204120 (2016),
    https://doi.org/10.1063/1.4948636
"""
e0_ref = {
    'Li': 1.67, 'Na': 1.12, 'K': 0.94, 'Rb': 0.86, 'Cs': 0.81,
    'Ca': 1.87, 'Sr': 1.73,
    'Ba': 1.91, 'Al': 3.43, 'Cu': 3.51, 'Rh': 5.78,
    'Pd': 3.93, 'Ag': 2.96, 'C': 7.55, 'SiC': 6.48,
    'Si': 4.68, 'Ge': 3.89, 'GaAs': 3.34, 'LiF': 4.46,
    'LiCl': 3.59, 'NaF': 3.97, 'NaCl': 3.34, 'MgO': 5.20
}

struc = {
'Li': 'bcc', 'Na': 'bcc', 'K': 'bcc', 'Rb': 'bcc', 'Cs': 'bcc',
'Ca': 'fcc', 'Sr': 'fcc', 'Ba': 'bcc',
'Al': 'fcc', 'Cu': 'fcc', 'Rh': 'fcc', 'Pd': 'fcc', 'Ag': 'fcc',
'C': 'ds', 'SiC': 'zb', 'Si': 'ds', 'Ge': 'ds', 'GaAs': 'zb',
'LiF': 'rs', 'LiCl': 'rs', 'NaF': 'rs', 'NaCl': 'rs', 'MgO': 'rs'
}

elts = ['Li', 'Na', 'Ca', 'K', 'Rb', 'Cs', 'Sr', 'Ba', 'Al', 'Cu', 'Rh',
'Pd', 'Ag', 'C', 'Si', 'Ge','Ga','As','F','Cl','Mg','O']

def molecule_parser(mol):

    atoms = {}
    tstr = ''
    nstr = ''
    nchar = len(mol)-1
    last_was_num = False
    for ichar,achar in enumerate(mol):
        if ichar > 0:
            if achar.isupper() and not last_was_num:
                atoms[tstr] = 1
                tstr = ''
            elif achar.isupper() and last_was_num:
                atoms[tstr] = int(float(nstr))
                nstr = ''
                tstr = ''
                last_was_num = False
            elif achar.isnumeric():
                last_was_num = True
                nstr += achar

        if not achar.isnumeric():
            tstr += achar
    if achar.isnumeric():
        atoms[tstr] = int(float(nstr))
    else:
        atoms[tstr] = 1

    return atoms

def get_gap(flnm='DOSCAR'):

    cnt = 0
    found_cbm = False
    prev = [0.0,0.0]
    with open(flnm,'r') as tmpfl:

        for iln,ln in enumerate(tmpfl):
            if iln < 5:
                continue
            elif iln == 5:
                if len(ln.split())< 5:
                    print('WARNING: severe, something wrong with DOSCAR')
                    raise SystemExit()
                tmp = ln.split()
                nedos = int(tmp[2])
                ef = round(float(tmp[3]),3)
            else:

                if len(ln.split()) == 5 and cnt > 0:
                    break
                if cnt == nedos:
                    break

                tmp = [float(x) for x in (ln.strip()).split()]
                if (tmp[0] <= ef + 1.e-3) and (tmp[1] > 0.0) and (prev[1] > 0.0):
                    vbm = tmp[0]
                elif (tmp[0] >= ef - 1.e-3) and (tmp[1] > 0.0) and (prev[1]==0.0):
                    cbm = tmp[0]
                    found_cbm = True
                    break

                prev = [tmp[0],tmp[1]]
                cnt += 1
    if not found_cbm:
        # catch in case of metal
        cbm = vbm
    gap = cbm-vbm
    return gap

def process_gaps_single_dfa(dfa):

    ofl = 'LC23_{:}/{:}_gaps.csv'.format(dfa,dfa)
    wstr = 'Solid, {:}, Expt. \n'.format(dfa)

    """
        Experimental reference values taken from
        T. Aschebrock and S. Kuemmel,
            Phys. Rev. Research 1, 033082 (2019)
            DOI: 10.1103/PhysRevResearch.1.033082

    """
    exptd = {'Ge': 0.74, 'Si': 1.17, 'GaAs': 1.52, 'SiC':2.42, 'C': 5.48,
    'MgO': 7.22, 'LiCl': 9.4, 'LiF':13.6}

    me = 0.0
    mae = 0.0
    nsol = len(exptd.keys())
    for isol,sol in enumerate(exptd):
        wstr += '{:}'.format(sol)
        wfl = './LC23_{:}/{:}/gap/DOS_gap'.format(dfa,sol)
        tgap = get_gap(flnm=wfl)
        wstr += ', {:.2f}'.format(tgap)
        wstr += ', {:.2f} \n'.format(exptd[sol])
        me += tgap - exptd[sol]
        mae += abs(tgap - exptd[sol])
    wstr += 'ME, {:.2f}, \n'.format(me/nsol)
    wstr += 'MAE, {:.2f}, \n'.format(mae/nsol)

    with open(ofl,'w+') as tmpfl:
        tmpfl.write(wstr)

    return

def find_failures(opts={'out_fl':'OUTCAR','osz_fl':'OSZICAR','log_fl':'failed_runs.csv','write_to_file':True}):

    system('grep NELM {:} > ._temp_outcar_file_'.format(opts['out_fl']))
    with open('._temp_outcar_file_','r') as tmpfl:
        for ln in tmpfl:
            wln = ln.split()
            if wln[0] == 'NELM':
                if wln[2][-1]==';':
                    nelm = int(wln[2][:-1])
                else:
                    nelm = int(wln[2])
    system('rm ._temp_outcar_file_')
    runs = []
    with open(opts['osz_fl'],'r') as tmpfl:
        for iln,ln in enumerate(tmpfl):
            wln = ln.split()
            if wln[1]=='F=':
                runs.append([int(wln[0]),last_it,last_it<nelm])
            else:
                if wln[0] != 'N':
                    last_it = int(wln[1])
    tstr = 'Cycle, No. SCF steps, No. SCF steps < NELM = {:}\n'.format(nelm)
    nfail = 0
    for i in range(len(runs)):
        tstr += '{:}, {:}, {:}\n'.format(*runs[i])
        if not runs[i][2]:
            nfail += 1
    tstr += '====,====,====\n'
    tstr += 'No. failed runs, {:}\n'.format(nfail)
    if opts['write_to_file']:
        tmpfl = open(opts['log_fl'],'w+')
        tmpfl.write(tstr)
        tmpfl.close()
    return tstr,runs,nfail,nelm

def find_all_failed_runs_lc23(dirs,logfile='all_runs.txt',atom=False):

    wstr = ''

    nfail = 0
    nrun = 0
    for iadir,adir in enumerate(dirs):

        wstr += '{:}\n'.format(adir)
        osz_l = glob(adir+'/osz_*.txt')
        wstr += 'File, Iterations, NELM, Its < NELM \n'
        for oszfl in osz_l:
            osztmp = oszfl.split('osz_')
            outfl = osztmp[0]+'out_'+osztmp[-1]
            tstr,runs,tfail,nelm = find_failures(opts={'out_fl':outfl,'osz_fl':oszfl,'write_to_file':False})
            wstr += '{:}, {:}, {:}, {:}\n'.format(oszfl.split('/')[-1],runs[0][1],nelm,runs[0][2])
            nfail += tfail
            nrun += 1
        if iadir < len(dirs)-1:
            wstr += '====,====,====,====\n'
    ostr = 'Total, number of, failed runs, {:}\n'.format(nfail)
    ostr += 'out of, {:} total runs ({:}%) \n\n'.format(nrun,nfail/nrun*100)
    ostr += wstr
    tmpfl = open(logfile,'w+')
    tmpfl.write(ostr)
    tmpfl.close()

    return


def vol_to_cubic_lp(symm,vol,qprint=False):

    wd = {'nion': 1,'print':qprint,'v':vol,'symm':symm}

    if wd['symm'] == 'sc':
        wd['fac'] = 1.0
    elif wd['symm'] == 'bcc':
        wd['fac'] = 2.0
    elif wd['symm'] in ['fcc','ds','rs','zb']:
        wd['fac'] = 4.0
        if wd['symm'] in ['ds','rs','zb']:
            wd['nion'] = 2.0
    a0 = (wd['v']*wd['fac']*wd['nion'])**(1/3)
    if wd['print']:
        print(a0)
    return a0

def lc23_analysis():

    fnl = sys.argv[1]
    bdir = 'LC23_'+fnl

    alkalis = ['Li', 'Na', 'K', 'Rb', 'Cs']
    nalk = len(alkalis)

    metals = ['Li', 'Na', 'K', 'Rb', 'Cs', 'Ca', 'Sr', 'Ba', 'Al', 'Cu', 'Rh', 'Pd', 'Ag']
    nmetals = len(metals)
    ninsul = len(struc.keys())-nmetals

    nsol = len(a0_ref)
    lps = np.zeros(nsol)
    a0_errs = np.zeros(nsol)
    a0_mae_mets = 0.0
    a0_mae_alk = 0.0
    a0_mae_insul = 0.0
    a0_me_mets = 0.0
    a0_me_alk = 0.0
    a0_me_insul = 0.0

    b0_errs = np.zeros(nsol)
    bms = np.zeros(nsol)
    b0_mae_mets = 0.0
    b0_mae_alk = 0.0
    b0_mae_insul = 0.0
    b0_me_mets = 0.0
    b0_me_alk = 0.0
    b0_me_insul = 0.0

    e0_errs = np.zeros(nsol)
    e0s = np.zeros(nsol)
    e0_mae_mets = 0.0
    e0_mae_alk = 0.0
    e0_mae_insul = 0.0
    e0_me_mets = 0.0
    e0_me_alk = 0.0
    e0_me_insul = 0.0

    atom_ens = {}
    af_str = 'Atom, No. SCF steps, NELM \n'
    n_at_fail = 0

    for atom in elts:
        with open(bdir+'/atoms/{:}/INCAR'.format(atom),'r') as tmpfl:
            for ln in tmpfl:
                wln = [x.strip() for x in ln.strip().split('=')]
                if wln[0] == 'NELM':
                    nelm = int(wln[1])
                    break
        tdict = conv_en(bdir+'/atoms/{:}/OSZICAR'.format(atom))
        atom_ens[atom] = tdict['E']
        af_str += '{:}, {:}, {:} \n'.format(atom,tdict['its'],nelm)
        if tdict['its'] >= nelm:
            n_at_fail += 1
    af_str += '===,===,===\n'
    af_str += 'No. failures, {:}, ({:}%)'.format(n_at_fail,n_at_fail/len(elts))
    with open(bdir+'/'+fnl+'_LC23_atom_runs.csv','w+') as ofl:
        ofl.write(af_str)


    conv_d = {}

    for isol,solid in enumerate(a0_ref):
        cdir = '{:}/{:}/ev_data'.format(bdir,solid)

        syspars = sjeos_fit(hcpcoa=False,wdir=cdir,soft_exit=True)
        syspars['B0'] *= eV_ang3_to_gpa

        if (syspars['V0'] != syspars['V0']) or (syspars['B0'] != syspars['B0']) or syspars['V0'] < 0.0 :
            conv_d[solid] = False
            a0_errs[isol] = 0.0
            b0_errs[isol] = 0.0
            e0_errs[isol] = 0.0
            nsol -= 1
            if solid in alkalis:
                nalk -= 1
            if solid in metals:
                 nmetals -= 1
            else:
                 ninsul -= 1
        else:
            conv_d[solid] = True
            lps[isol] = vol_to_cubic_lp(struc[solid],syspars['V0'])
            a0_errs[isol] = lps[isol] - a0_ref[solid]

            bms[isol] = syspars['B0']
            b0_errs[isol] = syspars['B0'] - b0_ref[solid]

            cons = molecule_parser(solid)
            cons_en = 0.0
            ncons = 0
            for atom in cons:
                cons_en += cons[atom]*atom_ens[atom]
                ncons += 1
            if struc[solid] == 'ds':
                ncons = 2
                cons_en *= 2
            e0s[isol] = (cons_en - syspars['E0'])/ncons
            e0_errs[isol] = e0s[isol] - e0_ref[solid]

        if solid in alkalis:
            a0_mae_alk += abs(a0_errs[isol])
            a0_me_alk += a0_errs[isol]

            b0_mae_alk += abs(b0_errs[isol])
            b0_me_alk += b0_errs[isol]

            e0_mae_alk += abs(e0_errs[isol])
            e0_me_alk += e0_errs[isol]

        if solid in metals:
            a0_mae_mets += abs(a0_errs[isol])
            a0_me_mets += a0_errs[isol]

            b0_mae_mets += abs(b0_errs[isol])
            b0_me_mets += b0_errs[isol]

            e0_mae_mets += abs(e0_errs[isol])
            e0_me_mets += e0_errs[isol]

        else:
            a0_mae_insul += abs(a0_errs[isol])
            a0_me_insul += a0_errs[isol]

            b0_mae_insul += abs(b0_errs[isol])
            b0_me_insul += b0_errs[isol]

            e0_mae_insul += abs(e0_errs[isol])
            e0_me_insul += e0_errs[isol]

    a0_me = np.sum(a0_errs)/nsol
    a0_mae = np.sum(np.abs(a0_errs))/nsol

    a0_mae_mets /= nmetals
    a0_me_mets /= nmetals
    a0_mae_alk /= nalk
    a0_me_alk /= nalk
    a0_mae_insul /= ninsul
    a0_me_insul /= ninsul

    b0_me = np.sum(b0_errs)/nsol
    b0_mae = np.sum(np.abs(b0_errs))/nsol

    b0_mae_mets /= nmetals
    b0_me_mets /= nmetals
    b0_mae_alk /= nalk
    b0_me_alk /= nalk
    b0_mae_insul /= ninsul
    b0_me_insul /= ninsul

    e0_me = np.sum(e0_errs)/nsol
    e0_mae = np.sum(np.abs(e0_errs))/nsol

    e0_mae_mets /= nmetals
    e0_me_mets /= nmetals
    e0_mae_alk /= nalk
    e0_me_alk /= nalk
    e0_mae_insul /= ninsul
    e0_me_insul /= ninsul


    tmsk = np.zeros(len(a0_ref),dtype=bool)
    nconv20 = 0
    for isol,sol in enumerate(a0_ref):

        if sol in ['K', 'Rb', 'Cs'] or not conv_d[solid]:
            continue
        else:
            tmsk[isol] = True
            nconv20 += 1

    a0_me_20 = np.sum(a0_errs[tmsk])/nconv20
    a0_mae_20 = np.sum(np.abs(a0_errs[tmsk]))/nconv20
    b0_me_20 = np.sum(b0_errs[tmsk])/nconv20
    b0_mae_20 = np.sum(np.abs(b0_errs[tmsk]))/nconv20
    e0_me_20 = np.sum(e0_errs[tmsk])/nconv20
    e0_mae_20 = np.sum(np.abs(e0_errs[tmsk]))/nconv20


    with open('./'+bdir+'/LC23_a0_results_'+fnl+'.csv','w+') as ofl:
        ofl.write('Solid (struc), a0 (Å), a0 error\n')
        for isol,solid in enumerate(a0_ref):
            ofl.write(('{:} ({:}), {:}, {:}\n').format(solid,struc[solid],lps[isol],a0_errs[isol]))
        ofl.write('====\n')
        ofl.write(('ME (metals), {:}\n').format(a0_me_mets))
        ofl.write(('MAE (metals), {:}\n').format(a0_mae_mets))
        ofl.write('====\n')
        ofl.write(('ME (alkalis), {:}\n').format(a0_me_alk))
        ofl.write(('MAE (alkalis), {:}\n').format(a0_mae_alk))
        ofl.write('====\n')
        ofl.write(('ME (insulators), {:}\n').format(a0_me_insul))
        ofl.write(('MAE (insulators), {:}\n').format(a0_mae_insul))
        ofl.write('====\n')
        ofl.write(('ME (total), {:}\n').format(a0_me))
        ofl.write(('MAE (total), {:}\n').format(a0_mae))
        ofl.write('====\n')
        ofl.write(('ME (LC20), {:}\n').format(a0_me_20))
        ofl.write(('MAE (LC20), {:}').format(a0_mae_20))

    with open('./'+bdir+'/LC23_B0_results_'+fnl+'.csv','w+') as ofl:
        ofl.write('Solid (struc), B0 (GPa), B0 error\n')
        for isol,solid in enumerate(a0_ref):
            ofl.write(('{:} ({:}), {:}, {:}\n').format(solid,struc[solid],bms[isol],b0_errs[isol]))
        ofl.write('====\n')
        ofl.write(('ME (metals), {:}\n').format(b0_me_mets))
        ofl.write(('MAE (metals), {:}\n').format(b0_mae_mets))
        ofl.write('====\n')
        ofl.write(('ME (alkalis), {:}\n').format(b0_me_alk))
        ofl.write(('MAE (alkalis), {:}\n').format(b0_mae_alk))
        ofl.write('====\n')
        ofl.write(('ME (insulators), {:}\n').format(b0_me_insul))
        ofl.write(('MAE (insulators), {:}\n').format(b0_mae_insul))
        ofl.write('====\n')
        ofl.write(('ME (total), {:}\n').format(b0_me))
        ofl.write(('MAE (total), {:}\n').format(b0_mae))
        ofl.write('====\n')
        ofl.write(('ME (LC20), {:}\n').format(b0_me_20))
        ofl.write(('MAE (LC20), {:}').format(b0_mae_20))

    with open('./'+bdir+'/LC23_E0_results_'+fnl+'.csv','w+') as ofl:
        ofl.write('Solid (struc), E0 (eV/atom), E0 error\n')
        for isol,solid in enumerate(a0_ref):
            ofl.write(('{:} ({:}), {:}, {:}\n').format(solid,struc[solid],e0s[isol],e0_errs[isol]))
        ofl.write('====\n')
        ofl.write(('ME (metals), {:}\n').format(e0_me_mets))
        ofl.write(('MAE (metals), {:}\n').format(e0_mae_mets))
        ofl.write('====\n')
        ofl.write(('ME (alkalis), {:}\n').format(e0_me_alk))
        ofl.write(('MAE (alkalis), {:}\n').format(e0_mae_alk))
        ofl.write('====\n')
        ofl.write(('ME (insulators), {:}\n').format(e0_me_insul))
        ofl.write(('MAE (insulators), {:}\n').format(e0_mae_insul))
        ofl.write('====\n')
        ofl.write(('ME (total), {:}\n').format(e0_me))
        ofl.write(('MAE (total), {:}\n').format(e0_mae))
        ofl.write('====\n')
        ofl.write(('ME (LC20), {:}\n').format(e0_me_20))
        ofl.write(('MAE (LC20), {:}').format(e0_mae_20))

    wdir = []
    for sol in struc:
        wdir.append(bdir+'/'+sol+'/ev_data')
    find_all_failed_runs_lc23(wdir,logfile=bdir+'/'+fnl+'_LC23_runs.csv')

    process_gaps_single_dfa(fnl)

    return

if __name__=="__main__":

    lc23_analysis()
