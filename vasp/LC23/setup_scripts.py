import numpy as np
from os import system
from process_lc23 import a0_ref,struc,elts,molecule_parser

insuls = ['C','GaAs','Ge','LiCl','LiF','MgO','Si','SiC']
gga_regex = ['PE','PS']
mgga_regex = ['SCAN','R2SCAN','R2SL','OFR2']

def iso_atom_script(dfa,sys,dir='./'):
    str = '#!/bin/bash\n'
    str += '#PBS -l walltime=1:00:00:00\n'
    str += '#PBS -q normal\n'
    str += '#PBS -l nodes=1:ppn=20\n'
    str += '#PBS -N {:}_{:}\n'.format(dfa,sys)
    str += '#PBS -j oe\n'
    str += '#PBS -o oe.txt\n'
    str += '#PBS -m a\n'
    str += '#PBS -M kaplan@temple.edu\n'
    str += 'module --force purge ; module load intel-libs\n'
    str += 'cd "$PBS_O_WORKDIR"\n'
    str += 'vasp_exec=/home/tuf53878/vasp6/bin/vasp_gam\n'
    str += "mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec"

    with open(dir+'runcalc.sh','w+') as tfl:
        tfl.write(str)
    return

def sjeos_script(sys,fnl):
    wdir = 'LC23_{:}/{:}'.format(fnl,sys)
    system('rm -rf '+wdir)
    system('mkdir -p '+wdir)

    str = "#!/bin/bash\n#PBS -l walltime=12:00:00\n#PBS -q normal\n\
#PBS -l nodes=1:ppn=20\n#PBS -N sjeos_{:}_{:}\n#PBS -j oe\n#PBS -o oe.txt\n\
#PBS -m a\n#PBS -M kaplan@temple.edu\n\n".format(sys,fnl)

    str += 'module --force purge ; module load intel-libs\ncd "$PBS_O_WORKDIR"\n\n'

    str+='sysname={:}\nstruc={:}\n\n'.format(sys,struc[sys])

    str+='vasp_exec=/home/tuf53878/vasp6/bin/vasp_std\n'
    #str+='sjeos_exec=/home/tuf53878/llmgga_testing/LC23/sjeos.py\n'
    str+='writer_exec=/home/tuf53878/llmgga_testing/LC23/poscar_writer.py\n'
    str+='v_to_a_exec=/home/tuf53878/llmgga_testing/LC23/vol_to_cubic_lp.py\n'
    if sys in insuls:
        str += 'gap_exec=/home/tuf53878/llmgga_testing/LC23/bandgaps_helper.py\n'
    str += '\nrm -f WAVECAR CHGCAR CHG\n'
    str+='\nrm -rf ev_data\nmkdir -p ev_data\n\nfor apar in '

    vsrch = 0.1
    npts = 12

    ascl = a0_ref[sys]*np.linspace((1-vsrch)**(1/3),(1 + vsrch)**(1/3),npts)
    al = [round(tmp,3) for tmp in ascl]

    for ap in al:
        str += '{:} '.format(ap)
    str += "; do \n  python3 $writer_exec -symm=$struc -a=$apar -name=$sysname\n"
    str += "  mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec\n"
    str += '  mkdir "./ev_data/$apar" \n'
    str += '  mv OSZICAR "./ev_data/$apar/"\n'
    str += '  mv DOSCAR "./ev_data/$apar/"\n'
    str += '  mv OUTCAR "./ev_data/$apar/"\n'
    str += '  mv POSCAR "./ev_data/$apar/"\n'
    str += '  mv EIGENVAL "./ev_data/$apar/"\n'
    str += '  mv vasprun.xml "./ev_data/$apar/"\n'
    str += '  mv IBZKPT "./ev_data/$apar/"\n'
    str += '  mv PROCAR "./ev_data/$apar/"\n'
    #str += '  cp INCAR "./ev_data/$apar/"\n'
    str += 'done\n\n'
    #str += 'cd ev_data ; python3 $sjeos_exec ; cd ..\n'
    #str += 'v0="$(grep '+ "'V0'" + ' ./ev_data/sjeos_fit.csv)"\nv0=${v0:3}\n'
    #str += 'a0=$(python3 $v_to_a_exec -symm=$struc -v=$v0 -print)\necho $a0\n'

    if sys in insuls:
        str += '\nrm -rf gap ; mkdir gap\n\n'
        str += 'printf -v aeq "%.3f" "$a0"\n'
        str += 'python3 $writer_exec -symm=$struc -a=$aeq -name=$sysname\n\n'
        str += "mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec\n\n"
        if sys == 'LiF':
            str += 'python3 $gap_exec incar NEDOS=3001 '
        else:
            str += 'python3 $gap_exec incar '
        if fnl in gga_regex:
            str += 'GGA={:}'.format(fnl)
        elif fnl in mgga_regex:
            str += 'GGA=PE METAGGA={:}'.format(fnl)
        str += '\n\nmv INCAR INCAR_init\n'
        str += 'cp INCAR_init gap/INCAR_init\n'
        str += 'mv OSZICAR gap/OSZ_init\nmv OUTCAR gap/OUT_init\nmv DOSCAR gap/DOS_init\n'
        str += 'mv EIGENVAL gap/EIG_init\n'
        str += 'mv vasprun.xml gap/vasprun_init\n'
        str += 'mv IBZKPT gap/IBZKPT_init\n'
        str += 'mv PROCAR gap/PROCAR_init\n'
        str += 'mv INCAR_gap INCAR\n\n'
        str += "mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec\n\n"
        str += 'mv INCAR INCAR_gap\nmv INCAR_init INCAR\n\n'
        str += 'python3 $gap_exec gap\n\n'
        str += 'mv OSZICAR gap/OSZ_gap\nmv OUTCAR gap/OUT_gap\nmv DOSCAR gap/DOS_gap\n'
        str += 'mv EIGENVAL gap/EIG_gap\n'

    str += '\nrm WAVECAR'
    tmpfl = open(wdir+'/'+'sjeos_fit.sh','w+')
    tmpfl.write(str)
    tmpfl.close()

    return wdir

def incar_script(ofl='./INCAR',opts={},atom = False):

    if atom:
        # easier to save separate parameters for the isolated atoms
        tagd = {'SYSTEM': 'unknown', 'NCORE': 4, 'KPAR': 1, 'ALGO': 'A', 'LSUBROT': False,
        'PREC': 'Accurate', 'EDIFF': 1.e-5, 'ENCUT': 600, 'ISMEAR': 0,
        'SIGMA': 0.1, 'ISPIN': 2, 'MAGMOM': '1.0',
        'GGA': 'PE', 'NSW': 0, 'IBRION': -1, 'NELM': 500, 'NELMIN': 6,
        'ADDGRID': False, 'LMIXTAU': True, 'LASPH': True, 'LMAXMIX': 4, 'LREAL': False,
        'AMIX': 0.1, 'BMIX': 0.0001, 'AMIX_MAG': 0.4, 'BMIX_MAG': 0.0001,
        'LWAVE': False, 'LCHARG': False, 'LVTOT': False, 'LORBIT': 11
        }
    else:
        tagd = {'SYSTEM': 'unknown', 'NCORE': 4, 'KPAR': 5, 'ALGO': 'Normal', 'PREC': 'Accurate',
        'EDIFF': 1.e-6, 'ENCUT': 600, 'ISMEAR': -5, 'KSPACING': 0.08, 'KGAMMA': True,
        'GGA': 'PE', 'NSW': 0, 'IBRION': -1, 'NELM': 200, 'NELMIN': 6,
        'ADDGRID': False, 'LMIXTAU': True, 'LASPH': True, 'LMAXMIX': 4, 'LREAL': False,
        'LWAVE': True, 'LCHARG': False, 'LVTOT': False, 'LORBIT': 11, 'ISTART': 1
        }
    for akey in opts:
        tagd[akey] = opts[akey]

    npad = 10
    str = ''
    for akey in tagd:
        padstr = ' '*(npad - len(akey))
        if akey in ['EDIFF','EDIFFG']:
            str += '{:}{:}= '.format(akey,padstr) + ('{:} \n'.format(tagd[akey])).upper()
        elif type(tagd[akey]) is bool:
            str += '{:}{:}= .{:}. \n'.format(akey,padstr,tagd[akey])
        else:
            str += '{:}{:}= {:} \n'.format(akey,padstr,tagd[akey])

    tmpfl = open(ofl,'w+')
    tmpfl.write(str)
    tmpfl.close()
    return

if __name__=="__main__":

    """
    --------------------------------------
        needed for isolated atoms
    """
    kpoints_str = 'G only\n1\nC\n0 0 0 1'
    """
    --------------------------------------
    """


    for fnl in ['PE']:#,'PS','SCAN','R2SCAN','R2SL','OFR2']:
        optd = {}
        if fnl in gga_regex:
            optd['GGA'] = fnl
        elif fnl in mgga_regex:
            optd['GGA'] = 'PE'
            optd['METAGGA'] = fnl
        system('rm -rf ./LC23_{:}'.format(fnl))
        system('mkdir -p ./LC23_{:}/atoms'.format(fnl))
        system('cp run.py ./LC23_{:}/run.py'.format(fnl))
        for sol in a0_ref:
            optd['SYSTEM'] = sol
            dir = sjeos_script(sol,fnl)
            incar_script(ofl=dir+'/INCAR',opts=optd)

            if sol in ['GaAs','LiCl','LiF','MgO','SiC','NaF','NaCl']:
                comp = molecule_parser(sol)
                tmpstr = 'cat '
                for elt in comp:
                    tmpstr += 'LC23_POTCARs/{:}/POTCAR '.format(elt)
                tmpstr += '> ./LC23_{:}/{:}/POTCAR'.format(fnl,sol)
            else:
                tmpstr = 'cp LC23_POTCARs/{:}/POTCAR ./LC23_{:}/{:}/POTCAR'.format(sol,fnl,sol)
            system(tmpstr)

        bdir = './LC23_{:}/atoms/'.format(fnl)

        for atom in elts:

            #if atom in ['C','Si','O','F','Rh','Pd','Cu']:
            #    bfac = 10.0
            #elif atom in ['Li', 'Na', 'K', 'Rb', 'Cs', 'Al']:
            #    bfac = 14.0
            #else:
            #    bfac = 12.0
            bfac = 14
            box_dims = [bfac,bfac+0.1,bfac + 0.2]

            wdir = bdir+atom+'/'
            system('mkdir -p ./LC23_{:}/atoms/{:}'.format(fnl,atom))
            iso_atom_script(fnl,atom,dir=wdir)

            with open(wdir+'KPOINTS','w+') as tfl:
                tfl.write(kpoints_str)

            system('cp LC23_POTCARs/{:}/POTCAR ./LC23_{:}/atoms/{:}/POTCAR'.format(atom,fnl,atom))

            poscar_str = '{:}\n 1.0\n'.format(atom)
            poscar_str += '{:}  0.0  0.0\n'.format(box_dims[0])
            poscar_str += '0.0  {:}  0.0\n'.format(box_dims[1])
            poscar_str += '0.0  0.0  {:}\n'.format(box_dims[2])
            poscar_str += '  {:}\n  1\n d\n'.format(atom)
            poscar_str += '0.5  0.5  0.5'

            with open(wdir+'POSCAR','w+') as tfl:
                tfl.write(poscar_str)

            optd['SYSTEM'] = atom
            incar_script(ofl=wdir+'INCAR',opts=optd,atom=True)
