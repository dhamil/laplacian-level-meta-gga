from os import listdir, getcwd
import numpy as np
from scipy.optimize import leastsq

def conv_en(flnm,all_stage=False):

    pars = {}
    # F is Helmholtz free energy, F = E - TS, where finite T (and S) comes from smearing
    # E is energy extrapolated without smearing, E = F + TS
    # dE is change in energy between penultimate and ultimate runs
    # mag is the magnetiziation within the calculation cell

    its = 0
    with open(flnm,'r') as infl:
        for iln,ln in enumerate(infl):
            ln = ln.split()
            if all_stage:
                indx = ln[0]
            else:
                indx = flnm
            if ln[1] == 'F=':
                its = its + int(oln[1])
                if len((ln[7][1:]).split('E'))==1:
                    dE_temp = 0.0
                else:
                    dE_temp=float(ln[7][1:])
                if len(ln) == 10:
                    # ISPIN = 2
                    pars[indx]={'F': float(ln[2]), 'E': float(ln[4]), 'dE': dE_temp, 'mag': float(ln[9]), 'its': its}
                else:
                    # ISPIN = 1, spin unpolarized
                    pars[indx]={'F': float(ln[2]), 'E': float(ln[4]), 'dE': dE_temp, 'mag': 0.0, 'its': its}
            oln = ln
    if all_stage:
        return pars
    else:
        return pars[indx]


def sjeos_fit(hcpcoa=False,wdir=None,soft_exit=False):

    """
        adapted from V. N. Staroverov, G. E. Scuseria, J. Tao, and J. P. Perdew,
        Tests of a ladder of density functionals for bulk solids and surfaces
        Phys. Rev. B 69, 075102 (2004)
        DOI: 10.1103/PhysRevB.69.075102
    """


    """
        NB: this routine expects to find OSZICAR files saved in the format
        osz_"identifier" or osz_"identifier".txt
        "identifier" can be an arbitrary idn
    """

    # default values help catch if fitting routine fails
    out_pars = {'V0': -1, 'B0': -1, 'B1': -1, 'E0': 1e20, 'alpha': 0.0,
    'beta': 0.0, 'gamma': 0.0, 'omega': 0.0, 'R2': 0.0}

    if wdir is not None:
        dir = wdir
    else:
        dir = getcwd()
    osz_l = []
    for fli in listdir(dir):
        if fli.startswith('osz_'):
            osz_l.append(fli)
    nosz = len(osz_l)

    if hcpcoa:
        pos_l = ['pos'+x[3:] for x in osz_l]
        vol_l = np.zeros(nosz)
        en_l = np.zeros(nosz)
        a = np.zeros((3,3))
        for ifl,fl in enumerate(osz_l):

            with open(dir +'/'+pos_l[ifl],'r') as tinfl:
                for iln,ln in enumerate(tinfl):
                    ln = (ln.strip()).split()
                    if iln == 1:
                        scl = float(ln[0])
                    elif 1 < iln < 5:
                        a[iln-2] = [scl*float(u) for u in ln]
                    elif iln == 5:
                        break
            # first get area of triangle in plane, this is 1/2 |a1 x a2|
            area = 0.5*(np.sum(np.cross(a[0],a[1])**2))**(0.5)
            # for hcp, this area is Sqrt(3)*a**2/4 --> get a
            apar = (4*area/3**(0.5))**(0.5)
            vol = np.abs(np.linalg.det(a))
            # for hcp, primitive volume is sqrt(3)/2 * a**3 * (c/a) --> get c/a
            coa = 2*vol/(3**(0.5)*apar**3)
            vol_l[ifl] = coa

            tpars=conv_en(dir +'/'+fl)
            en_l[ifl] = tpars['E']
            nion = 1.0

    else:
        dos_l = ['dos'+x[3:] for x in osz_l]
        vol_l = np.zeros(nosz)
        en_l = np.zeros(nosz)

        for ifl,fl in enumerate(osz_l):

            with open(dir +'/'+dos_l[ifl],'r') as tinfl:
                for iln,ln in enumerate(tinfl):
                    ln = (ln.strip()).split()
                    if iln == 0:
                        nion = float(ln[0])
                    elif iln == 1:
                        vol_l[ifl] = float(ln[0])
                        break

            tpars=conv_en(dir +'/'+fl)
            en_l[ifl] = tpars['E']

    tfn = lambda p, x: p[3] + p[2]*x + p[1]*x**2 + p[0]*x**3
    erfn = lambda p: tfn(p,1/vol_l**(1/3)) - en_l

    p0 = [1.0,1.0,1.0,1.0]

    [aas,bs,cs,ds], scs = leastsq(erfn,p0[:])

    rdcnd = bs**2 - 3*aas*cs
    if rdcnd < 0:
        with open(dir +'/e_v_curve.csv','w+') as outfl:
            outfl.write('Unit cell volume (Ang**3), Energy (eV)\n')
            for ien,en in enumerate(en_l):
                outfl.write(('{:},{:}\n').format(vol_l[ien],en))
        if soft_exit:
            print('WARNING: SJEOS fit failed, imaginary volumes!')
        else:
            raise SystemExit('WARNING: SJEOS fit failed, imaginary volumes!')
        return out_pars

    v0m = -(bs + np.sqrt(rdcnd))/cs
    e0m = 1e20
    if v0m > 0.0:
        e0m = tfn([aas,bs,cs,ds],1/v0m)

    v0p = (-bs + np.sqrt(rdcnd))/cs
    e0p = 1e20
    if v0p > 0.0:
        e0p = tfn([aas,bs,cs,ds],1/v0p)

    if e0m < e0p and v0m > 0.0:
        out_pars['V0'] = v0m**3
    elif e0m > e0p and v0p > 0.0:
        out_pars['V0'] = v0p**3

    aa = aas/out_pars['V0']
    bb = bs/out_pars['V0']**(2.0/3.0)
    cc = cs/out_pars['V0']**(1.0/3.0)
    # don't forget that V0 is volume/atom, B0 and B1 conventionally reported
    # without considering how many atoms are in the unit cell
    vcell = nion*out_pars['V0']
    out_pars['B0'] = (18*aa + 10*bb + 4*cc)/(9*vcell)
    out_pars['B1'] = (108*aa + 50*bb+16*cc)/(27*out_pars['B0']*vcell)
    # E0 does not contain this factor, because the total energies are fitted, not energies/atom
    out_pars['E0'] = aa + bb + cc + ds
    out_pars['alpha'] = aa # need these to describe other thermodyanamic quantities
    out_pars['beta'] = bb
    out_pars['gamma'] = cc
    out_pars['omega'] = ds
    out_pars['nion'] = nion
    eavg = np.sum(en_l)/en_l.shape[0]
    out_pars['R2'] = 1 - np.sum(erfn([aas,bs,cs,ds])**2)/np.sum((en_l - eavg)**2)

    with open(dir +'/sjeos_fit.csv','w+') as outfl:
        outfl.write('SJEOS,parameters \n')
        for par in out_pars:
            outfl.write(('{:},{:}\n').format(par,out_pars[par]))
    with open(dir +'/e_v_curve.csv','w+') as outfl:
        outfl.write('Unit cell volume (Ang**3), Energy (eV)\n')
        ivol_srt = np.argsort(vol_l)
        vol_l = vol_l[ivol_srt]
        en_l = en_l[ivol_srt]
        for ien,en in enumerate(en_l):
            outfl.write(('{:},{:}\n').format(vol_l[ien],en))

    return out_pars


if __name__ == "__main__":

    sjeos_fit()
