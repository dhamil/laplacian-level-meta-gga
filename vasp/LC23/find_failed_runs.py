from os import system,sys
from glob import glob

def find_failures(opts={'out_fl':'OUTCAR','osz_fl':'OSZICAR','log_fl':'failed_runs.csv','write_to_file':True}):

    system('grep NELM {:} > ._temp_outcar_file_'.format(opts['out_fl']))
    with open('._temp_outcar_file_','r') as tmpfl:
        for ln in tmpfl:
            wln = ln.split()
            if wln[0] == 'NELM':
                if wln[2][-1]==';':
                    nelm = int(wln[2][:-1])
                else:
                    nelm = int(wln[2])
    system('rm ._temp_outcar_file_')
    runs = []
    with open(opts['osz_fl'],'r') as tmpfl:
        for iln,ln in enumerate(tmpfl):
            wln = ln.split()
            if wln[1]=='F=':
                runs.append([int(wln[0]),last_it,last_it<nelm])
            else:
                if wln[0] != 'N':
                    last_it = int(wln[1])
    tstr = 'Cycle, No. SCF steps, No. SCF steps < NELM = {:}\n'.format(nelm)
    nfail = 0
    for i in range(len(runs)):
        tstr += '{:}, {:}, {:}\n'.format(*runs[i])
        if not runs[i][2]:
            nfail += 1
    tstr += '====,====,====\n'
    tstr += 'No. failed runs, {:}\n'.format(nfail)
    if opts['write_to_file']:
        tmpfl = open(opts['log_fl'],'w+')
        tmpfl.write(tstr)
        tmpfl.close()
    return tstr,runs,nfail,nelm

def find_all_failed_runs_lc20(dirs,logfile='all_runs.txt'):

    wstr = ''

    nfail = 0
    nrun = 0
    for iadir,adir in enumerate(dirs):

        wstr += '{:}\n'.format(adir)
        osz_l = glob(adir+'/osz_*.txt')
        wstr += 'File, Iterations, NELM, Its < NELM \n'
        for oszfl in osz_l:
            osztmp = oszfl.split('osz_')
            outfl = osztmp[0]+'out_'+osztmp[-1]
            tstr,runs,tfail,nelm = find_failures(opts={'out_fl':outfl,'osz_fl':oszfl,'write_to_file':False})
            wstr += '{:}, {:}, {:}, {:}\n'.format(oszfl.split('/')[-1],runs[0][1],nelm,runs[0][2])
            nfail += tfail
            nrun += 1
        if iadir < len(dirs)-1:
            wstr += '====,====,====,====\n'
    ostr = 'Total, number of, failed runs, {:}\n'.format(nfail)
    ostr += 'out of, {:} total runs ({:}%) \n\n'.format(nrun,nfail/nrun*100)
    ostr += wstr
    tmpfl = open(logfile,'w+')
    tmpfl.write(ostr)
    tmpfl.close()

    return

if __name__=='__main__':

    sols = [
    'Li', 'Na', 'Ca', 'Sr', 'Ba','Al', 'Cu', 'Rh', 'Pd', 'Ag',
    'C', 'SiC', 'Si', 'Ge', 'GaAs', 'LiF', 'LiCl', 'NaF', 'NaCl', 'MgO'
    ]
    ppnd = sys.argv[1]
    wdir = []
    for sol in sols:
        wdir.append(ppnd+'/'+sol)
    if len(sys.argv)>2:
        find_all_failed_runs_lc20(wdir,logfile=sys.argv[2])
    else:
        find_all_failed_runs_lc20(wdir)
