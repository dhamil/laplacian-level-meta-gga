import sys
from process_lc23 import a0_ref,b0_ref,e0_ref,struc

to_do = {'PE': 'PBE', 'PS': 'PBEsol', 'SCAN': 'SCAN', 'R2SCAN': 'r2SCAN',
'R2SL': 'r2SCAN', 'OFR2': 'OFR2'}

for iwork in range(3):

    dat = {}
    errs = {}

    if iwork == 0:
        refd = a0_ref
        targf_pre = './data/a0_data/LC23_a0_results_'
        outf = './LC23_a0_results.tex'
        unit = '\AA{}'
    elif iwork == 1:
        refd = b0_ref
        targf_pre = './data/B0_data/LC23_B0_results_'
        outf = './LC23_B0_results.tex'
        unit = 'GPa'
    elif iwork == 2:
        refd = e0_ref
        targf_pre = './data/E0_data/LC23_E0_results_'
        outf = './LC23_E0_results.tex'
        unit = 'eV/atom'


    for fnl in to_do:
        dat[fnl] = []
        errs[fnl] = []
        wfl = open(targf_pre+'{:}.csv'.format(fnl.lower()),'r')
        for iln,ln in enumerate(wfl):

            wln = ln.strip().split(',')
            if 1 <= iln <= 23:
                dat[fnl].append(float(wln[1]))
                errs[fnl].append(float(wln[2]))
            elif 25 <= iln:
                if wln[0] == '====':
                    continue
                errs[fnl].append(float(wln[1]))
        wfl.close()

    fstr = 'Solid (structure) & Reference ({:}) & PBE & PBEsol & SCAN & \\rrscan \
& \\rrscan-L & OFR2 \\\\ \hline \n'.format(unit)
    for isol,sol in enumerate(refd):
        if isol == len(refd)-1:
            tchar = '\hline \n'
        else:
            tchar = '\n'
        fstr += '{:} ({:}) & {:}'.format(sol,struc[sol],refd[sol])
        for fnl in to_do:
            fstr += ' & {:.3f}'.format(errs[fnl][isol])
        fstr += ' \\\\ {:}'.format(tchar)

    isol += 1
    fstr += 'ME & (metals)'
    for fnl in to_do:
        fstr += ' & {:.3f}'.format(errs[fnl][isol])
    fstr += '\\\\ \n'
    isol += 1
    fstr += 'MAE & (metals)'
    for fnl in to_do:
        fstr += ' & {:.3f}'.format(errs[fnl][isol])
    fstr += ' \\\\ \hline \n'

    isol += 1
    fstr += 'ME & (alkalis)'
    for fnl in to_do:
        fstr += ' & {:.3f}'.format(errs[fnl][isol])
    fstr += ' \\\\ \n'
    isol += 1
    fstr += 'MAE & (alkalis)'
    for fnl in to_do:
        fstr += ' & {:.3f}'.format(errs[fnl][isol])
    fstr += ' \\\\ \hline \n'

    isol += 1
    fstr += 'ME & (insulators)'
    for fnl in to_do:
        fstr += ' & {:.3f}'.format(errs[fnl][isol])
    fstr += ' \\\\ \n'
    isol += 1
    fstr += 'MAE & (insulators)'
    for fnl in to_do:
        fstr += ' & {:.3f}'.format(errs[fnl][isol])
    fstr += ' \\\\ \hline \n'

    isol += 1
    fstr += 'ME & (total)'
    for fnl in to_do:
        fstr += ' & {:.3f}'.format(errs[fnl][isol])
    fstr += ' \\\\ \n'
    isol += 1
    fstr += 'MAE & (total)'
    for fnl in to_do:
        fstr += ' & {:.3f}'.format(errs[fnl][isol])
    fstr += ' \\\\ \hline \n'

    isol += 1
    fstr += 'ME & (LC20)'
    for fnl in to_do:
        fstr += ' & {:.3f}'.format(errs[fnl][isol])
    fstr += ' \\\\ \n'
    isol += 1
    fstr += 'MAE & (LC20)'
    for fnl in to_do:
        fstr += ' & {:.3f}'.format(errs[fnl][isol])
    fstr += ' \\\\ \n'

    wfl = open(outf,'w+')
    wfl.write(fstr)
    wfl.close()
