from os import sys,system
from glob import glob
import numpy as np

"""
    reference cubic lattice constant data with zero-point corrections
    (in angstrom) from
    P. Hao, Y. Fang, J. Sun, G.I. Csonka, P.H.T. Philipsen, and J.P. Perdew,
    Phys. Rev. B 85, 014111 (2012), https://doi.org/10.1103/PhysRevB.85.014111
"""
a0_ref = {
'Li': 3.451, 'Na': 4.207, 'Ca': 5.555, 'Sr': 6.042,
'Ba': 5.004,'Al': 4.019, 'Cu': 3.595, 'Rh': 3.793,
'Pd': 3.876, 'Ag': 4.063, 'C': 3.555, 'SiC': 4.348,
'Si': 5.422, 'Ge': 5.644, 'GaAs': 5.641, 'LiF': 3.974,
'LiCl': 5.072, 'NaF': 4.570, 'NaCl': 5.565, 'MgO': 4.188
}

eV_ang3_to_gpa = 1.602176634e2 # https://physics.nist.gov/cgi-bin/cuu/Value?e

"""
    bulk moduli in GPa with zero point corrections,
    from Table S8 from the SI of
    F. Tran, J. Stelzl, and P. Blaha, J. Chem. Phys. 144, 204120 (2016),
    https://doi.org/10.1063/1.4948636
"""
b0_ref = {
    'Li': 13.1, 'Na': 7.9, 'Ca': 15.9, 'Sr': 12.0,
    'Ba': 10.6, 'Al': 77.1, 'Cu': 144.3, 'Rh': 277.1,
    'Pd': 187.2, 'Ag': 105.7, 'C': 454.7, 'SiC': 229.1,
    'Si': 101.3, 'Ge': 79.4, 'GaAs': 76.7, 'LiF': 76.3,
    'LiCl': 38.7, 'NaF': 53.1, 'NaCl': 27.6, 'MgO': 169.8
}

struc = {
'Li': 'bcc', 'Na': 'bcc', 'Ca': 'fcc', 'Sr': 'fcc', 'Ba': 'bcc',
'Al': 'fcc', 'Cu': 'fcc', 'Rh': 'fcc', 'Pd': 'fcc', 'Ag': 'fcc',
'C': 'ds', 'SiC': 'zb', 'Si': 'ds', 'Ge': 'ds', 'GaAs': 'zb',
'LiF': 'rs', 'LiCl': 'rs', 'NaF': 'rs', 'NaCl': 'rs', 'MgO': 'rs'
}

def find_failures(opts={'out_fl':'OUTCAR','osz_fl':'OSZICAR','log_fl':'failed_runs.csv',\
    'write_to_file':True}):

    system('grep NELM {:} > ._temp_outcar_file_'.format(opts['out_fl']))
    with open('._temp_outcar_file_','r') as tmpfl:
        for ln in tmpfl:
            wln = ln.split()
            if wln[0] == 'NELM':
                if wln[2][-1]==';':
                    nelm = int(wln[2][:-1])
                else:
                    nelm = int(wln[2])
    system('rm ._temp_outcar_file_')
    runs = []
    with open(opts['osz_fl'],'r') as tmpfl:
        for iln,ln in enumerate(tmpfl):
            wln = ln.split()
            if wln[1]=='F=':
                runs.append([int(wln[0]),last_it,last_it<nelm])
            else:
                if wln[0] != 'N':
                    last_it = int(wln[1])
    tstr = 'Cycle, No. SCF steps, No. SCF steps < NELM = {:}\n'.format(nelm)
    nfail = 0
    for i in range(len(runs)):
        tstr += '{:}, {:}, {:}\n'.format(*runs[i])
        if not runs[i][2]:
            nfail += 1
    tstr += '====,====,====\n'
    tstr += 'No. failed runs, {:}\n'.format(nfail)
    if opts['write_to_file']:
        tmpfl = open(opts['log_fl'],'w+')
        tmpfl.write(tstr)
        tmpfl.close()
    return tstr,runs,nfail,nelm

def find_all_failed_runs_lc20(dirs,logfile='all_runs.txt'):

    wstr = ''

    nfail = 0
    nrun = 0
    for iadir,adir in enumerate(dirs):

        wstr += '{:}\n'.format(adir)
        wstr += 'File, Iterations, NELM, Its < NELM \n'
        tstr,runs,tfail,nelm = find_failures(opts={'out_fl':adir+'/OUTCAR',\
        'osz_fl':adir+'/OSZICAR','write_to_file':False})
        for arun in runs:
            wstr += '{:}, {:}, {:}, {:}\n'.format(arun[0],arun[1],nelm,arun[2])
            nrun += 1
        nfail += tfail
        if iadir < len(dirs)-1:
            wstr += '====,====,====,====\n'
    ostr = 'Total, number of, failed runs, {:}\n'.format(nfail)
    ostr += 'out of, {:} total runs ({:}%) \n\n'.format(nrun,nfail/nrun*100)
    ostr += wstr
    tmpfl = open(logfile,'w+')
    tmpfl.write(ostr)
    tmpfl.close()

    return


def vol_to_cubic_lp(symm,vol,qprint=False,vol_per_at=True):

    wd = {'nion': 1,'print':qprint,'v':vol,'symm':symm}

    if wd['symm'] == 'sc':
        wd['fac'] = 1.0
    elif wd['symm'] == 'bcc':
        wd['fac'] = 2.0
    elif wd['symm'] in ['fcc','ds','rs','zb']:
        wd['fac'] = 4.0
        if wd['symm'] in ['ds','rs','zb']:
            wd['nion'] = 2.0
    if vol_per_at:
        a0 = (wd['v']*wd['fac']*wd['nion'])**(1/3)
    else:
        a0 = (wd['v']*wd['fac'])**(1/3)
    if wd['print']:
        print(a0)
    return a0

def get_cubic_latt_vec(a,symm):

    if symm == 'sc':
        dlv = a*np.array([[1,0,0],[0,1,0],[0,0,1]])
        bas = np.zeros((1,3))
    elif symm == 'bcc':
        dlv = a*np.array([[-0.5, 0.5, 0.5],[0.5, -0.5, 0.5],[0.5, 0.5, -0.5]])
        bas = np.zeros((1,3))
    elif symm in ['fcc','ds','rs','zb']:
        dlv = a*np.array([[0.0, 0.5, 0.5],[0.5, 0.0, 0.5],[0.5, 0.5, 0.0]])
        if symm == 'fcc':
            bas = np.zeros((1,3))
        elif symm == 'ds':
            bas = np.array([[0.125, 0.125, 0.125], [0.875, 0.875, 0.875]])
        elif symm == 'rs':
            bas = np.array([[0.0,0.0,0.0],[0.5,0.5,0.5]])
        elif symm == 'zb':
            bas = np.array([[0.0, 0.0, 0.0],[0.25, 0.25, 0.25]])
        for ibas,abas in enumerate(bas):
            bas[ibas] = np.einsum('i,ij->j',abas,dlv)
    return dlv,bas

def check_cubic_symm(a,dlv,bas,symm,prec = 1.e-10):

    tdlv,tbas = get_cubic_latt_vec(a,symm)
    wkind = [0,1,2]
    nmatch = {'DLV': 0, 'BAS': 0}
    for ialv,alv in enumerate(dlv):
        for iblv in wkind:
            tmp = np.sum((tdlv[iblv] - alv)**2)**(0.5)
            if tmp < prec:
                nmatch['DLV'] += 1
                wkind2 = [x for x in wkind]
                wkind = []
                for x in wkind2:
                    if x == iblv:
                        continue
                    wkind.append(x)
                break

    nbas = len(bas)
    wkind = [i for i in range(nbas)]
    for ialv,alv in enumerate(bas):
        for iblv in wkind:
            tmp = np.sum((tbas[iblv] - alv)**2)**(0.5)
            if tmp < prec:
                nmatch['BAS'] += 1
                wkind2 = [x for x in wkind]
                wkind = []
                for x in wkind2:
                    if x == iblv:
                        continue
                    wkind.append(x)
                break

    return {'DLV': (nmatch['DLV']==3), 'BAS': (nmatch['BAS']==nbas)}

def lc20_analysis():

    #sjeos_exec = '~/Desktop/vuk/main.py -sjeos'
    fnl = sys.argv[1]#.lower()
    bdir = 'LC20_'+fnl#'./results_'+fnl

    metals = ['Li', 'Na', 'Ca', 'Sr', 'Ba', 'Al', 'Cu', 'Rh', 'Pd', 'Ag']
    nmetals = len(metals)
    ninsul = len(struc.keys())-nmetals

    nsol = len(a0_ref)
    lps = np.zeros(nsol)
    a0_errs = np.zeros(nsol)
    a0_mae_mets = 0.0
    a0_mae_insul = 0.0
    a0_me_mets = 0.0
    a0_me_insul = 0.0

    for isol,solid in enumerate(a0_ref):
        dlv = np.zeros((3,3))
        nion = 1
        with open('{:}/{:}/CONTCAR'.format(bdir,solid),'r') as tfl:
            for iln,aln in enumerate(tfl):
                if iln == 1:
                    scl = float(aln.split()[0])
                elif 1 < iln < 5:
                    dlv[iln-2] = [scl*float(u) for u in aln.split()]
                elif iln == 6:
                    nion_l = [int(y) for y in aln.split()]
                    nion = int(np.sum(nion_l))
                    cvol = abs(np.linalg.det(dlv))
                    bas = np.zeros((nion,3))
                elif iln == 7:
                    bastpe = (aln.split()[0]).lower()[0]
                elif 7 < iln < 8+nion:
                    tmp = [float(u) for u in aln.split()]
                    if bastpe == 'd':
                        bas[iln-8] = tmp[0]*dlv[0] + tmp[1]*dlv[1] +tmp[2]*dlv[2]
                    elif bastpe == 'c':
                        bas[iln-8] = tmp[:]
                elif iln == 8 + nion:
                    break

        lps[isol] = vol_to_cubic_lp(struc[solid],cvol,qprint=False,vol_per_at=False)
        a0_errs[isol] = lps[isol] - a0_ref[solid]
        symmd = check_cubic_symm(lps[isol],dlv,bas,struc[solid])
        if not symmd['DLV']:
            print('WARNING, direct lattice vectors for {:} may not have right symmetry'.format(solid))
        if not symmd['BAS']:
            print('WARNING, basis vectors for {:} may not have right symmetry'.format(solid))

        if solid in metals:
            a0_mae_mets += abs(a0_errs[isol])
            a0_me_mets += a0_errs[isol]

        else:
            a0_mae_insul += abs(a0_errs[isol])
            a0_me_insul += a0_errs[isol]

    a0_me = np.sum(a0_errs)/nsol
    a0_mae = np.sum(np.abs(a0_errs))/nsol

    a0_mae_mets /= nmetals
    a0_me_mets /= nmetals
    a0_mae_insul /= ninsul
    a0_me_insul /= ninsul

    with open('./'+bdir+'/LC20_a0_results_'+fnl+'.csv','w+') as ofl:
        ofl.write('Solid (struc), a0, a0 error\n')
        for isol,solid in enumerate(a0_ref):
            ofl.write(('{:} ({:}), {:}, {:}\n').format(solid,struc[solid],lps[isol],a0_errs[isol]))
        ofl.write('====\n')
        ofl.write(('ME (metals), {:}\n').format(a0_me_mets))
        ofl.write(('MAE (metals), {:}\n').format(a0_mae_mets))
        ofl.write('====\n')
        ofl.write(('ME (insulators), {:}\n').format(a0_me_insul))
        ofl.write(('MAE (insulators), {:}\n').format(a0_mae_insul))
        ofl.write('====\n')
        ofl.write(('ME (total), {:}\n').format(a0_me))
        ofl.write(('MAE (total), {:}').format(a0_mae))

    wdir = []
    for solid in struc:
        wdir.append(bdir+'/'+solid+'/')
    find_all_failed_runs_lc20(wdir,logfile=bdir+'/'+fnl+'_LC20_runs.csv')

    return

if __name__=="__main__":

    lc20_analysis()
