import numpy as np

from process_lc20 import struc

def py_exp_to_tex_exp(num):
    tmpstr = ('{:.4e}'.format(num)).split('e')
    tmpstr[1] = '{:}'.format(int(tmpstr[1]))
    return '${:} \\times 10^{{{:}}}$'.format(*tmpstr)


eos_dir = '../lc20/data/a0_data/'
st_dir = './data/'

lp_d = {}
fnls = {'PS': 'PBEsol', 'R2SCAN': 'r$^2$SCAN', 'R2SL': 'r$^2$SCAN-L, \\ref{kaplan2022}', 'R2SL_corrected_ST': 'r$^2$SCAN-L', 'OFR2': 'OFR2, \\ref{kaplan2022}', 'OFR2_corrected_ST': 'OFR2'}
dirs = {'EOS': eos_dir, 'ST': st_dir}
diffs = {}

for fnl in fnls:
    lp_d[fnl] = {'EOS': {}, 'ST': {} }
    diffs[fnl] = {}
    for adir in dirs:
        tfnl = fnl
        if adir == 'EOS' and '_corrected_ST' in fnl:
            tfnl = fnl.split('_corrected_ST')[0]
        with open(dirs[adir]+'LC20_a0_results_{:}.csv'.format(tfnl),'r') as tfl:
            for iln,aln in enumerate(tfl):
                if iln == 0:
                    continue
                if iln == 21:
                    break
                tmp = aln.strip().split(',')
                sol = tmp[0].split(' ')[0]
                lp_d[fnl][adir][sol] = float(tmp[1])

    diffs[fnl]['MD'] = 0.0
    diffs[fnl]['MAD'] = 0.0
    for sol in lp_d[fnl]['EOS']:
        diffs[fnl][sol] = lp_d[fnl]['EOS'][sol] - lp_d[fnl]['ST'][sol]
        diffs[fnl]['MD'] += diffs[fnl][sol]
        diffs[fnl]['MAD'] += abs(diffs[fnl][sol])
    diffs[fnl]['MD'] = diffs[fnl]['MD']/20
    diffs[fnl]['MAD'] = diffs[fnl]['MAD']/20

tstr = 'Solid (struc)'
tstr2 = 'Solid (struc)'
for fnl in fnls:
    tstr += ', {:}'.format(fnl)
    tstr2 += ' & {:}'.format(fnls[fnl])
tstr += '\n'
tstr2 += ' \\\\ \hline \n'

for isol,sol in enumerate(struc):
    tstr += '{:} ({:})'.format(sol,struc[sol])
    tstr2 += '{:} ({:})'.format(sol,struc[sol])
    for fnl in fnls:
        tstr += ', {:}'.format(diffs[fnl][sol])
        tstr2 += ' & {:}'.format(py_exp_to_tex_exp(diffs[fnl][sol]))
    tstr += '\n'
    if isol == len(struc)-1:
        tstr2 += ' \\\\ \hline \n'
    else:
        tstr2 += ' \\\\ \n'
tstr += 'MD'
tstr2 += 'MD'
for fnl in fnls:
    tstr += ', {:}'.format(diffs[fnl]['MD'])
    tstr2 += ' & {:}'.format(py_exp_to_tex_exp(diffs[fnl]['MD']))
tstr += '\nMAD'
tstr2 += ' \\\\ \nMAD'
for fnl in fnls:
    tstr += ', {:}'.format(diffs[fnl]['MAD'])
    tstr2 += ' & {:}'.format(py_exp_to_tex_exp(diffs[fnl]['MAD']))
tstr2 += ' \\\\'

with open(st_dir+'eos_st_comp.csv','w+') as tfl:
    tfl.write(tstr)
with open(st_dir+'eos_st_comp.tex','w+') as tfl:
    tfl.write(tstr2)
