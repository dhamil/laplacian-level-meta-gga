elts = ['Li', 'Na', 'Ca', 'Sr', 'Ba', 'Al', 'Cu', 'Rh', 'Pd', 'Ag', 'C', 'Si',
'Ge','Ga','As','F','Cl','Mg','O']

if __name__=="__main__":

    str = 'Element, Pseudopot., Valence \n'
    for elt in elts:
        with open('./LC20_POTCARs/{:}/POTCAR'.format(elt),'r') as tmpfl:
            for iln,ln in enumerate(tmpfl):
                if iln == 0:
                    pp_name = ln.strip()
                elif iln == 1:
                    val = int(float(ln.strip()))
                    break
            str += '{:}, {:}, {:}\n'.format(elt,pp_name,val)
    with open('LC20_POTCAR_list.csv','w+') as tmpfl:
        tmpfl.write(str)
