import numpy as np
from os import system
from process_lc20 import a0_ref,struc

insuls = ['C','GaAs','Ge','LiCl','LiF','MgO','Si','SiC']
metals = ['Li', 'Na', 'Ca', 'Sr', 'Ba', 'Al', 'Cu', 'Rh', 'Pd', 'Ag']
gga_regex = ['PE','PS']
mgga_regex = ['SCAN','R2SCAN','R2SL','OFR2','TASK']


def sjeos_script(sys,fnl):
    wdir = 'LC20_{:}/{:}'.format(fnl,sys)
    system('rm -rf '+wdir)
    system('mkdir -p '+wdir)

    str = "#!/bin/bash\n#PBS -l walltime=12:00:00\n#PBS -q normal\n\
#PBS -l nodes=1:ppn=20\n#PBS -N LC20_ST_{:}_{:}\n#PBS -j oe\n#PBS -o oe.txt\n\
#PBS -m a\n#PBS -M kaplan@temple.edu\n\n".format(sys,fnl)

    str += 'module --force purge ; module load intel-libs\ncd "$PBS_O_WORKDIR"\n\n'

    str+='sysname={:}\nstruc={:}\n\n'.format(sys,struc[sys])

    str+='vasp_exec=/home/tuf53878/vasp6_lst/bin/vasp_std\n'
    str+='writer_exec=/home/tuf53878/llmgga_testing/lc20/poscar_writer.py\n\n'

    str += "python3 $writer_exec -symm=$struc -a={:} -name=$sysname\n".format(round(a0_ref[sys],3))
    str += "mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec\n"

    tmpfl = open(wdir+'/'+'lc20_st.sh','w+')
    tmpfl.write(str)
    tmpfl.close()

    return wdir

def incar_script(ofl='./INCAR',opts={}):

    tagd = {'SYSTEM': 'unknown', 'NCORE': 4, 'KPAR': 5, 'ALGO': 'Normal', 'PREC': 'Accurate',
    'EDIFF': 1.e-6, 'ENCUT': 800, 'ISMEAR': -5, 'KSPACING': 0.08, 'KGAMMA': True,
    'GGA': 'PE',  'NELM': 200, 'NELMIN': 6,
    'NSW': 100, 'IBRION': 2, 'ISIF':3, 'EDIFFG': -0.001,
    'ADDGRID': False, 'LMIXTAU': True, 'LASPH': True, 'LMAXMIX': 4, 'LREAL': False,
    'LWAVE': False, 'LCHARG': False, 'LVTOT': False, 'LORBIT': 11
    }
    for akey in opts:
        tagd[akey] = opts[akey]

    npad = 10
    str = ''
    for akey in tagd:
        padstr = ' '*(npad - len(akey))
        if akey in ['EDIFF','EDIFFG']:
            str += '{:}{:}= '.format(akey,padstr) + ('{:} \n'.format(tagd[akey])).upper()
        elif type(tagd[akey]) is bool:
            str += '{:}{:}= .{:}. \n'.format(akey,padstr,tagd[akey])
        else:
            str += '{:}{:}= {:} \n'.format(akey,padstr,tagd[akey])

    tmpfl = open(ofl,'w+')
    tmpfl.write(str)
    tmpfl.close()
    return

if __name__=="__main__":

    """
    for sol in a0_ref:
        system('mkdir -p LC20_POTCARs/{:}'.format(sol))
        system('cp lc20_inputs/{:}/POTCAR LC20_POTCARs/{:}/POTCAR '.format(sol,sol))
    exit()
    """
    for fnl in ['PS','R2SCAN','R2SL','OFR2']:
        optd = {}
        if fnl in gga_regex:
            optd['GGA'] = 'PS'
        elif fnl in mgga_regex:
            optd['GGA'] = 'PE'
            optd['METAGGA'] = fnl
        system('rm -rf ./LC20_{:}'.format(fnl))
        system('mkdir ./LC20_{:}'.format(fnl))
        system('cp run.py ./LC20_{:}/run.py'.format(fnl))
        for sol in a0_ref:
            optd['SYSTEM'] = sol
            if sol in metals:
                optd['ISMEAR'] = 1
                optd['SIGMA'] = 0.2
            else:
                optd['ISMEAR'] = 0
                optd['SIGMA'] = 0.05
            dir = sjeos_script(sol,fnl)
            incar_script(ofl=dir+'/INCAR',opts=optd)
            system('cp LC20_POTCARs/{:}/POTCAR ./LC20_{:}/{:}/POTCAR'.format(sol,fnl,sol))
