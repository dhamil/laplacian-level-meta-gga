#!/bin/bash
#PBS -l walltime=12:00:00
#PBS -q normal
#PBS -l nodes=1:ppn=20
#PBS -N Ef_VPt2_PS
#PBS -j oe
#PBS -o oe.txt
#PBS -m a
#PBS -M kaplan@temple.edu

module --force purge ; module load intel-libs
cd "$PBS_O_WORKDIR"

vasp_exec=/home/tuf53878/vasp6_lst/bin/vasp_std
rm -rf relax ; mkdir relax
cp INCAR_rel relax/INCAR
cp POSCAR relax/POSCAR
cp KPOINTS relax/KPOINTS
cp POTCAR relax/POTCAR

cd relax
mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec

cd ..
rm -rf toten ; mkdir toten
cp relax/CONTCAR toten/POSCAR
cp INCAR_toten toten/INCAR
cp POTCAR toten/POTCAR
cp KPOINTS relax/KPOINTS
mv relax/WAVECAR toten/WAVECAR

cd toten
mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec

rm -f WAVECAR
