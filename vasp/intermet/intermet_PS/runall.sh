#!/bin/bash

for asys in HfOs VPt2 ScPt Hf Os V Pt Sc
do
  cd $asys
  qsub get_toten.sh
  cd ..
done