from glob import glob
import numpy as np

kpdens = 700

def vnorm(v):
    return np.dot(v,v)**(0.5)

for afl in glob('POSCAR_*'):
    dlv = np.zeros((3,3))
    rlv = np.zeros((3,3))
    with open(afl,'r') as tfl:
        for iln,aln in enumerate(tfl):
            if iln == 1:
                scl = float(aln.split()[0])
            elif 1 < iln < 5:
                dlv[iln-2] = [scl*float(u) for u in aln.split()]
            elif iln == 6:
                break

    cvol = abs(np.linalg.det(dlv))
    for ind in range(3):
        rlv[ind] = 2*np.pi*np.cross(dlv[(ind+1)%3],dlv[(ind+2)%3])/cvol
    rlvn = np.zeros(3)
    for ind in range(3):
        rlvn[ind] = vnorm(rlv[ind])
    dk = round((rlvn[0]*rlvn[1]*rlvn[2]/cvol/kpdens)**(1/3),3)

    while dk > 0.0:
        nkv = np.zeros(3)
        for ind in range(3):
            nkv[ind] = max(1,int(np.ceil(vnorm(rlv[ind])/dk)))
        Nk = nkv[0]*nkv[1]*nkv[2]
        kdeff = Nk/cvol
        if kdeff >= kpdens:
            break
        dk = round(dk-1.e-3,3)
    print(afl,dk,kdeff,nkv)
