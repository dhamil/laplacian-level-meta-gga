import numpy as np
from os import sys,path
from glob import glob

nion_d = {'HfOs': {'Hf': 1, 'Os': 1},
'Hf': {'Hf': 2},
'Os': {'Os': 2},
'Pt': {'Pt': 1},
'VPt2': {'V': 1, 'Pt': 2},
'ScPt': {'Pt': 1, 'Sc': 1},
'Sc': {'Sc': 2},
'V': {'V': 1}
}

pref_order = ['CA','PE','PS','SCAN','R2SCAN','R2SL', 'R2SL_corrected_ST','OFR2','OFR2_corrected_ST']

total_d = {}
for asys in nion_d:
    tmp = 0
    for bsys in nion_d[asys]:
        tmp += nion_d[asys][bsys]
    total_d[asys] = tmp

comp_l = ['HfOs','ScPt','VPt2']

def conv_en(flnm,all_stage=False):

    pars = {}
    # F is Helmholtz free energy, F = E - TS, where finite T (and S) comes from smearing
    # E is energy extrapolated without smearing, E = F + TS
    # dE is change in energy between penultimate and ultimate runs
    # mag is the magnetiziation within the calculation cell

    its = 0
    with open(flnm,'r') as infl:
        for iln,ln in enumerate(infl):
            ln = ln.split()
            if all_stage:
                indx = ln[0]
            else:
                indx = flnm
            if ln[1] == 'F=':
                its = its + int(oln[1])
                if len((ln[7][1:]).split('E'))==1:
                    dE_temp = 0.0
                else:
                    dE_temp=float(ln[7][1:])
                if len(ln) == 10:
                    # ISPIN = 2
                    pars[indx]={'F': float(ln[2]), 'E': float(ln[4]), 'dE': dE_temp, 'mag': float(ln[9]), 'its': its}
                else:
                    # ISPIN = 1, spin unpolarized
                    pars[indx]={'F': float(ln[2]), 'E': float(ln[4]), 'dE': dE_temp, 'mag': 0.0, 'its': its}
            oln = ln
    if all_stage:
        return pars
    else:
        return pars[indx]


def imet_analysis(dir='./'):

    en_d = {}
    lstr2 = 'Solid, E_tot (eV/atom)\n'
    for asys in nion_d:
        osz_d = conv_en(dir+asys+'/toten/OSZICAR')
        en_d[asys] = osz_d['E']/total_d[asys]
        lstr2 += '{:}, {:}\n'.format(asys,en_d[asys])

    form_d = {}
    lstr1 = 'Solid, E_f (eV/atom)\n'
    for asys in comp_l:
        cpot = 0.0
        for bsys in nion_d[asys]:
            cpot += nion_d[asys][bsys]*en_d[bsys]
        form_d[asys] = en_d[asys] - cpot/total_d[asys]
        lstr1 += '{:}, {:}\n'.format(asys,form_d[asys])

    ostr = lstr1 + '--,--\n'+lstr2
    with open(dir+'imet_log.csv','w+') as tfl:
        tfl.write(ostr)

    return en_d, form_d

if __name__== "__main__":

    wdir='./'
    if len(sys.argv) > 1:
        if sys.argv[1] == 'all':
            dstr = {'DFA': []}
            dstr_tex = {'DFA': []}
            for asys in comp_l:
                dstr[asys] = asys
                dstr_tex[asys] = asys
            hstr = 'Solid'
            hstr_tex = 'Solid'
            #for tdir in glob('intermet_*'):
            for fnl in pref_order:
                tdir = 'intermet_'+fnl
                if not path.isdir(tdir):
                    continue
                hstr += ', {:}'.format(tdir[9:])
                hstr_tex += ' & {:}'.format(tdir[9:])
                _,tmp_d = imet_analysis(dir=tdir+'/')
                for asys in comp_l:
                    dstr[asys] += ', {:}'.format(tmp_d[asys])
                    dstr_tex[asys] += ' & {:.3f}'.format(tmp_d[asys])
            ostr = hstr + '\n'
            for asys in comp_l:
                ostr += dstr[asys] + '\n'
            with open('./imet_logall.csv','w+') as tfl:
                tfl.write(ostr)

            ostr = hstr_tex + ' \\\\ \n'
            for asys in comp_l:
                ostr += dstr_tex[asys] + ' \\\\ \n'
            with open('./imet_logall.tex','w+') as tfl:
                tfl.write(ostr)
            exit()

        else:
            wdir = 'intermet_'+sys.argv[1]+'/'
    imet_analysis(dir=wdir)
