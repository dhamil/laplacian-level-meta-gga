import numpy as np
from os import system

gga_regex = ['PE','PS']
mgga_regex = ['SCAN','R2SCAN','R2SL','OFR2','TASK']

sys_d = {'HfOs': 'HfOs', 'VPt2': 'Pt2V', 'ScPt': 'PtSc', 'Hf': 'Hf', 'Os': 'Os',
    'V': 'V', 'Pt': 'Pt', 'Sc': 'Sc'}
potcar_d = {'Hf': 'Hf_pv', 'Os': 'Os_pv', 'Pt': 'Pt_pv', 'Sc': 'Sc_sv', 'V': 'V_sv'}

def job_script(sys,fnl):
    wdir = './intermet_{:}/{:}'.format(fnl,sys)

    str = "#!/bin/bash\n#PBS -l walltime=12:00:00\n#PBS -q normal\n\
#PBS -l nodes=1:ppn=20\n#PBS -N Ef_{:}_{:}\n#PBS -j oe\n#PBS -o oe.txt\n\
#PBS -m a\n#PBS -M kaplan@temple.edu\n\n".format(sys,fnl)

    str += 'module --force purge ; module load intel-libs\ncd "$PBS_O_WORKDIR"\n\n'

    str+='vasp_exec=/home/tuf53878/vasp6_lst/bin/vasp_std\n'

    str += 'rm -rf relax ; mkdir relax\n'
    str += 'cp INCAR_rel relax/INCAR\n'
    str += 'cp POSCAR relax/POSCAR\n'
    if sys == 'VPt2':
        str += 'cp KPOINTS relax/KPOINTS\n'
    str += 'cp POTCAR relax/POTCAR\n\n'

    str += 'cd relax\n'
    str += "mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec\n\n"

    str += 'cd ..\n'
    str += 'rm -rf toten ; mkdir toten\n'
    str += 'cp relax/CONTCAR toten/POSCAR\n'
    str += 'cp INCAR_toten toten/INCAR\n'
    str += 'cp POTCAR toten/POTCAR\n'
    if sys == 'VPt2':
        str += 'cp KPOINTS relax/KPOINTS\n'
    str += 'mv relax/WAVECAR toten/WAVECAR\n\n'

    str += 'cd toten\n'
    str += "mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec\n\n"

    str += 'rm -f WAVECAR\n'

    tmpfl = open(wdir+'/'+'get_toten.sh','w+')
    tmpfl.write(str)
    tmpfl.close()

    return

def incar_script(ofl='./INCAR',opts={},wpars='relax',skip_keys=[]):

    if wpars == 'relax':
        # params for relaxation with stress tensor
        tagd = {'SYSTEM': 'unknown', 'NCORE': 4, 'KPAR': 5, 'ALGO': 'Normal', 'PREC': 'Accurate',
        'EDIFF': 1.e-6, 'ENCUT': 600, 'ISMEAR': 1, 'SIGMA': 0.2, 'GGA': 'PE', 'NELM': 200,
        'KSPACING': 0.08, 'KGAMMA': True,
        'NELMIN': 6, 'NSW': 100, 'IBRION': 2, 'NSW': 100, 'ISIF': 3, 'EDIFFG': -0.001,
        'ADDGRID': False, 'LMIXTAU': True, 'LASPH': True, 'LMAXMIX': 4, 'LREAL': False,
        'LWAVE': True, 'LCHARG': False, 'LVTOT': False, 'LORBIT': 11, 'ISPIN': 2
        }
    elif wpars == 'toten':
        tagd = {'SYSTEM': 'unknown', 'NCORE': 4, 'KPAR': 5, 'ALGO': 'Normal', 'PREC': 'Accurate',
        'EDIFF': 1.e-6, 'ENCUT': 600, 'ISMEAR': -5, 'KSPACING': 0.08, 'KGAMMA': True,
        'GGA': 'PE', 'NSW': 0, 'IBRION': -1, 'NELM': 200, 'NELMIN': 6, 'ISTART': 1,
        'ADDGRID': False, 'LMIXTAU': True, 'LASPH': True, 'LMAXMIX': 4, 'LREAL': False,
        'LWAVE': False, 'LCHARG': False, 'LVTOT': False, 'LORBIT': 11, 'ISPIN': 2
        }

    for akey in opts:
        tagd[akey] = opts[akey]

    npad = 10
    str = ''
    for akey in tagd:
        if akey in skip_keys:
            continue
        padstr = ' '*(npad - len(akey))
        if akey in ['EDIFF','EDIFFG']:
            str += '{:}{:}= '.format(akey,padstr) + ('{:} \n'.format(tagd[akey])).upper()
        elif type(tagd[akey]) is bool:
            str += '{:}{:}= .{:}. \n'.format(akey,padstr,tagd[akey])
        else:
            str += '{:}{:}= {:} \n'.format(akey,padstr,tagd[akey])

    tmpfl = open(ofl,'w+')
    tmpfl.write(str)
    tmpfl.close()
    return

def molecule_parser(mol):

    atoms = {}
    tstr = ''
    nstr = ''
    nchar = len(mol)-1
    last_was_num = False
    for ichar,achar in enumerate(mol):
        if ichar > 0:
            if achar.isupper() and not last_was_num:
                atoms[tstr] = 1
                tstr = ''
            elif achar.isupper() and last_was_num:
                atoms[tstr] = int(float(nstr))
                nstr = ''
                tstr = ''
                last_was_num = False
            elif achar.isnumeric():
                last_was_num = True
                nstr += achar

        if not achar.isnumeric():
            tstr += achar
    if achar.isnumeric():
        atoms[tstr] = int(float(nstr))
    else:
        atoms[tstr] = 1

    return atoms

def vnorm(v):
    return np.dot(v,v)**(0.5)

def get_kspacing(sol,kpdens=700):
    dlv = np.zeros((3,3))
    rlv = np.zeros((3,3))
    with open('./POSCARS_OQMD/POSCAR_{:}'.format(sys_d[sol]),'r') as tfl:
        for iln,aln in enumerate(tfl):
            if iln == 1:
                scl = float(aln.split()[0])
            elif 1 < iln < 5:
                dlv[iln-2] = [scl*float(u) for u in aln.split()]
            elif iln == 6:
                nion_l = [int(x) for x in aln.strip().split()]
                break

    cvol = abs(np.linalg.det(dlv))
    for ind in range(3):
        rlv[ind] = 2*np.pi*np.cross(dlv[(ind+1)%3],dlv[(ind+2)%3])/cvol
    rlvn = np.zeros(3)
    for ind in range(3):
        rlvn[ind] = vnorm(rlv[ind])
    dk = round((rlvn[0]*rlvn[1]*rlvn[2]/cvol/kpdens)**(1/3),3)

    while dk > 0.0:
        nkv = np.zeros(3)
        for ind in range(3):
            nkv[ind] = max(1,int(np.ceil(vnorm(rlv[ind])/dk)))
        Nk = nkv[0]*nkv[1]*nkv[2]
        kdeff = Nk/cvol
        if kdeff >= kpdens:
            break
        dk = round(dk-1.e-3,3)
    return dk,nion_l

if __name__=="__main__":

    for fnl in ['PS']:#,'PE','SCAN','R2SCAN','R2SL','OFR2']:
        optd = {}
        if fnl in gga_regex:
            optd['GGA'] = fnl
        elif fnl in mgga_regex:
            optd['GGA'] = 'PE'
            optd['METAGGA'] = fnl

        wdir = './intermet_{:}'.format(fnl)
        system('rm -rf {:}'.format(wdir))
        system('mkdir {:}'.format(wdir))

        rstr = '#!/bin/bash\n\n'
        rstr += 'for asys in'

        for sol in sys_d:

            system('mkdir {:}/{:}'.format(wdir,sol))
            rstr += ' {:}'.format(sol)

            ats = molecule_parser(sys_d[sol])
            str = 'cat'
            for at in ats:
                str += ' ./POTCARs/{:}/POTCAR'.format(potcar_d[at])
            str += ' > ./intermet_{:}/{:}/POTCAR'.format(fnl,sol)
            system(str)

            job_script(sol,fnl)
            system('cp ./POSCARS_OQMD/POSCAR_{:} {:}/{:}/POSCAR'.format(sys_d[sol],wdir,sol))

            optd['SYSTEM'] = sol
            if sol == 'VPt2':
                """
                    VASP can symmetrize hexagonal k meshes so they're no longer
                    hexagonal, even if the mesh is an odd number of k-points/axis,
                    and if it's Gamma-centered.
                    Explicitly use KPOINTS for VPt2 to get around this.
                """

                kstr = 'Symm\n0\nG\n'
                kstr += '33  33  33\n'
                kstr += '0.0 0.0 0.0'
                with open(wdir+'/VPt2/KPOINTS','w+') as tfl:
                    tfl.write(kstr)
                skpps = ['KSPACING','KGAMMA']
            else:
                skpps = []
            optd['KSPACING'],nion_l = get_kspacing(sol,kpdens=700)
            nion = sum(nion_l)

            optd['MAGMOM'] = '{:}*3.5'.format(nion)
            incar_script(ofl=wdir+'/{:}/INCAR_toten'.format(sol),opts=optd,\
                wpars='toten',skip_keys=skpps)
            incar_script(ofl=wdir+'/{:}/INCAR_rel'.format(sol),opts=optd,\
                wpars='relax',skip_keys=skpps)

        rstr += '\ndo\n'
        rstr += '  cd $asys\n  qsub get_toten.sh\n  cd ..\ndone'
        with open(wdir+'/runall.sh','w+') as tfl:
            tfl.write(rstr)
