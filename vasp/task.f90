!===============================================================
! TASK exchange meta-GGA of T. Aschebrock and S. Kuemmel,
!       Phys. Rev. Research 1, 033082 (2019)
!       DOI: 10.1103/PhysRevResearch.1.033082

! code author: Aaron Kaplan
! last revised: 1 September, 2021
!===============================================================


subroutine task_x(n0,n1,gn0,gn1,tau0,tau1,ex,dexdn0,dexdn1,dexdgn0,&
  &        dexdgn1,dexdtau0,dexdtau1)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in) :: n0,n1,gn0,gn1,tau0,tau1
  real(dp), intent(out) :: ex,dexdn0,dexdn1,dexdgn0,dexdgn1,dexdtau0,dexdtau1

  real(dp) :: ex0,ex1

  ex0 = 0._dp ; ex1 = 0._dp
  dexdn0 = 0._dp ; dexdn1 = 0._dp
  dexdgn0 = 0._dp ; dexdgn1 = 0._dp
  dexdtau0 = 0._dp ; dexdtau1 = 0._dp

  if (n0 > 0._dp) then
    call task_x_unp(2._dp*n0,2._dp*gn0,2._dp*tau0,ex0,dexdn0,dexdgn0,dexdtau0)
  end if

  if (n1 > 0._dp) then
    call task_x_unp(2._dp*n1,2._dp*gn1,2._dp*tau1,ex1,dexdn1,dexdgn1,dexdtau1)
  end if

  ex = (ex0 + ex1)/2._dp

end subroutine task_x


subroutine task_x_unp(n,gn,tau,ex,dexdn,dexdgn,dexdtau)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in) :: n,gn,tau
  real(dp), intent(out) :: ex,dexdn,dexdgn,dexdtau

  real(dp) :: exlda, d_exlda_dn
  real(dp) :: kf, p, d_p_dn,d_p_dgn, tt
  real(dp) :: fx, d_fx_dp, d_fx_dalpha
  real(dp) :: tau0, d_tau0_dn, tauw, d_tauw_dn, d_tauw_dgn
  real(dp) :: alpha, d_alpha_dn, d_alpha_dgn, d_alpha_dtau

  real(dp), parameter :: pi = 3.14159265358979323846264338327950288419_dp

  kf = (3._dp*pi**2*n)**(1._dp/3._dp)
  exlda = -3._dp/(4._dp*pi)*kf*n

  p = (gn/(2._dp*kf*n))**2

  tau0 = 0.3_dp*kf**2*n
  tauw = gn**2/(8._dp*n)
  ! regularization, ensure that alpha >= 0
  tt = max(tau,tauw)
  alpha = (tt - tauw)/tau0

  call task_fx(p,alpha,fx,d_fx_dp,d_fx_dalpha)
  ex = exlda*fx

  d_exlda_dn = -kf/pi

  d_p_dn = -8._dp/3._dp *p/n
  d_p_dgn = gn/(2._dp*(kf*n)**2)

  d_tau0_dn = kf**2/2._dp
  d_tauw_dn = -tauw/n
  d_tauw_dgn = gn/(4._dp*n)

  d_alpha_dn = -(d_tauw_dn + alpha*d_tau0_dn)/tau0
  d_alpha_dgn = -d_tauw_dgn/tau0
  d_alpha_dtau = 1._dp/tau0

  dexdn = d_exlda_dn*fx + exlda*(d_fx_dp*d_p_dn + d_fx_dalpha*d_alpha_dn)
  dexdgn = exlda*(d_fx_dp*d_p_dgn + d_fx_dalpha*d_alpha_dgn)
  dexdtau = exlda*d_fx_dalpha*d_alpha_dtau

end subroutine task_x_unp


subroutine task_fx(p,a,fx,dfxdp,dfxda)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in) :: p,a
  real(dp), intent(out) :: fx,dfxdp,dfxda

  integer :: i
  real(dp) :: hx1, dhx,ief, omief, d_hx1_dp, d_ief_da, gx, gxd, d_gx_dp
  real(dp) :: tmp, dtmp
  real(dp), parameter :: ch = 4.9479_dp, hx0 = 1.174_dp
  integer, parameter :: cd = 10
  real(dp), parameter :: anu(3) = (/ 0.938719_dp, -0.076371_dp, -0.0150899_dp /)
  real(dp), parameter :: bnu(5) = (/ -0.628591_dp, -2.10315_dp, -0.5_dp, &
 &       0.103153_dp, 0.128591_dp /)

  hx1 = 0._dp
  d_hx1_dp = 0._dp
  do i = 1,3
    call cheb_rat_pol(p,i-1,tmp,dtmp)
    hx1 = hx1 + anu(i)*tmp
    d_hx1_dp = d_hx1_dp + anu(i)*dtmp
  end do

  ief = 0._dp
  d_ief_da = 0._dp
  do i = 1,5
    call cheb_rat_pol(a,i-1,tmp,dtmp)
    ief = ief + bnu(i)*tmp
    d_ief_da = d_ief_da + bnu(i)*dtmp
  end do
  omief = 1._dp - ief

  gx = 1._dp
  d_gx_dp = 0._dp
  if (p > 1.d-16) then
    tmp = exp(-ch/p**(0.25_dp))
    gx = 1._dp - tmp
    d_gx_dp = -0.25_dp*ch/p**(1.25_dp)*tmp
  end if
  gxd = gx**cd

  dhx = hx1 - hx0
  fx = hx0*gx + omief*dhx*gxd
  dfxdp = hx0*d_gx_dp + omief*(d_hx1_dp*gxd &
  &     + cd*dhx*gx**(cd-1)*d_gx_dp)
  dfxda = -d_ief_da*dhx*gxd

end subroutine task_fx


subroutine cheb_rat_pol(x,i,ri,dridx)
  ! See John P. Boyd, J. Comput. Phys. 70, 63 (1987),
  !   https://doi.org/10.1016/0021-9991(87)90002-7

  !    and

  !    W.H. Press, S.A. Teukolsky, W.T. Vetterling, B.P. Flannery
  !    Numerical Recipes in Fortran 77, 1992
  !    Ch. 5.8 (Chebyshev Approximation)

  ! Chebyshev polynomial T_n(x) = cos(n * arccos(x))
  ! Rational Chebyshev polynomial R_n(x) = T_n((x-1)/(x+1))

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  integer, intent(in) :: i
  real(dp), intent(in) :: x
  real(dp), intent(out) :: ri,dridx

  real(dp) :: arg

  if (i == 0) then
      ri = 1._dp
      dridx = 0._dp
  else if (i == 1) then
      ri = (x - 1._dp)/(x + 1._dp)
      dridx = 2._dp/(x + 1._dp)**2
  else if (i == 2) then
      ri = (x**2 - 6*x + 1._dp)/(x + 1._dp)**2
      !dridx =( 2*x - 6._dp - 2*ri/(x + 1._dp) )/(x + 1._dp)**2
      dridx = 8._dp*(x - 1._dp)/(x + 1._dp)**3
  else if (i == 3) then
      ri = (x**3 - 15*x**2 + 15*x - 1._dp)/(x + 1._dp)**3
      dridx = 6._dp*(3*x**2 - 10*x + 3._dp)/(x + 1._dp)**4
      !dridx = (3*x**2 - 30*x + 15 - 3*ri/(x + 1._dp))/(x + 1._dp)**3
  else if (i == 4) then
      ri = (x**4 - 28*x**3 + 70*x**2 - 28*x + 1._dp)/(x + 1._dp)**4
      dridx = 32._dp*(x**3 - 7*x**2 + 7*x - 1._dp)/(x + 1._dp)**5
      !dridx = (4*x**3 - 84*x**2 + 140*x - 28 - 4*ri/(x + 1._dp))&
      !&       /(x + 1._dp)**4
  else
    arg = 1._dp*i*acos((x - 1._dp)/(x + 1._dp))
    ri = cos(arg)
    dridx = i*sin(arg)/( (x + 1._dp)*max(1.d-12,x**(0.5_dp)) )
  end if

end subroutine cheb_rat_pol


subroutine ec_pw92_wrap(nu,nd,ec,decdnu,decdnd)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in) :: nu,nd
  real(dp), intent(out) :: ec,decdnu,decdnd

  real(dp) :: n, rs,z, d_ec_drs, d_ec_dz, d_rs_dn,d_z_dnu,d_z_dnd,tmp
  real(dp), parameter :: pi = 3.14159265358979323846264338327950288419_dp

  n = nu + nd

  rs = (3._dp/(4._dp*pi*n))**(1._dp/3._dp)
  z = min(max(-0.99999999999990_dp,(nu - nd)/n),0.99999999999990_dp)

  call ec_pw92(rs,z,ec,d_ec_drs,d_ec_dz)

  d_rs_dn = -rs/n/3._dp
  d_z_dnu = 2._dp*nd/n**2
  d_z_dnd = -2._dp*nu/n**2

  tmp = ec + n*d_ec_drs*d_rs_dn
  decdnu = tmp + n*d_ec_dz*d_z_dnu
  decdnd = tmp + n*d_ec_dz*d_z_dnd
  ec = ec*n

end subroutine ec_pw92_wrap

subroutine ec_pw92(rs,zeta,ec,d_ec_drs,d_ec_dzeta)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)


  real(dp),intent(in) :: rs,zeta
  real(dp),intent(out) :: ec,d_ec_drs,d_ec_dzeta

  real(dp),parameter :: fz_den=(2._dp**(4._dp/3._dp)-2._dp),fdd0 = 8._dp/9._dp/fz_den
  real(dp) :: eps0,deps0,eps1,deps1,ac,dac
  real(dp) :: fz,dfz

  call pw92_g(rs,0.031091_dp,0.21370_dp,7.5957_dp,3.5876_dp,&
 &       1.6382_dp,0.49294_dp,eps0,deps0)
  if (abs(zeta)==0._dp) then
    ec = eps0
    d_ec_drs = deps0
    d_ec_dzeta = 0._dp
  else

    fz = ((1._dp + zeta)**(4._dp/3._dp) &
   &     + (1._dp - zeta)**(4._dp/3._dp) - 2._dp)/fz_den

    dfz = 4._dp/3._dp*((1._dp + zeta)**(1._dp/3._dp) &
   &     - (1._dp - zeta)**(1._dp/3._dp))/fz_den

    call pw92_g(rs,0.015545_dp,0.20548_dp,14.1189_dp,6.1977_dp,&
   &       3.3662_dp,0.62517_dp,eps1,deps1)
    call pw92_g(rs,0.016887_dp,0.11125_dp,10.357_dp,3.6231_dp,&
   &       0.88026_dp,0.49671_dp,ac,dac)

    ec = eps0*(1._dp - fz*zeta**4) - ac*fz/fdd0*(1._dp - zeta**4) + eps1*fz*zeta**4

    d_ec_drs = deps0*(1._dp - fz*zeta**4) + deps1*fz*zeta**4 &
   &       - dac*fz/fdd0*(1._dp - zeta**4)
    d_ec_dzeta = 4*zeta**3*fz*(eps1 - eps0 + ac/fdd0) &
   &       + dfz*(zeta**4 * (eps1 - eps0) - (1._dp - zeta**4)*ac/fdd0)
  end if

end subroutine ec_pw92

subroutine pw92_g(rs,a,alp,b1,b2,b3,b4,eps,deps)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp),intent(in) :: rs,a,alp,b1,b2,b3,b4
  real(dp),intent(out) :: eps,deps
  real(dp) :: rsh,q0,q1,q1p

  rsh = rs**(0.5_dp)

  q0 = -2*a*(1._dp + alp*rs)
  q1 = 2*a*(b1*rsh + b2*rs + b3*rsh**3 + b4*rs**2)
  q1p = a*(b1/rsh + 2*b2 + 3*b3*rsh + 4*b4*rs)

  eps = q0*log(1._dp + 1._dp/q1)
  deps = -2*a*alp*log(1._dp + 1._dp/q1) - q0*q1p/(q1**2 + q1)

end subroutine pw92_g
