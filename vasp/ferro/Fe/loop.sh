#!/bin/bash
#PBS -l walltime=02:00:00
#PBS -q normal
#PBS -l nodes=2:ppn=16
#PBS -N vasp_eos_fe
#PBS -j oe
#PBS -o vaspo.txt
#PBS -m abe
#PBS -M kaplan@temple.edu
#

module load intel-libs
module load intel-math
#
cd "$PBS_O_WORKDIR"


rm POSCAR
#for i in  8.0 8.5 9.0 9.5 10.0 10.5 11.0 11.5 12.0 12.5 ; do
for i in 10.0 ; do
#for i in 10.58 10.78 10.98 11.18 11.38 11.58 11.78 11.98 12.18 ; do
cat >POSCAR <<!
bcc fe
   -$i
   -0.5 0.5 0.5
   0.5 -0.5 0.5
   0.5 0.5 -0.5
     1
  cart
   0 0 0
!
echo "v= $i"
mpirun -np $PBS_NP ~/vasp/scan/vasp/bin/vasp_std
E=`awk '/F=/ {print $0}' OSZICAR` ; echo $i $E  >>SUMMARY.txt
done
cat SUMMARY.txt

bash cleanup.sh
