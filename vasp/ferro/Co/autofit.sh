#!/bin/bash
#PBS -l walltime=12:00:00
#PBS -q normal
#PBS -l nodes=2:ppn=20
#PBS -N sjeos_co
#PBS -j oe
#PBS -o oe.txt
#PBS -m a
#PBS -M kaplan@temple.edu

module --force purge ; module load intel-libs
cd "$PBS_O_WORKDIR"

sysname=Co
struc=hcp

vasp_exec=/home/tuf53878/vasp6/bin/vasp_std
sjeos_exec=/home/tuf53878/vuk/main.py
writer_exec=/home/tuf53878/vuk/poscar_writer.py
v_to_a_exec=/home/tuf53878/vuk/vol_to_cubic_lp.py
rm -rf ev_data
mkdir -p ev_data

a0=2.51
coa0=1.62
v0=$(echo "0.8660254037844386*${coa0}*${a0}*${a0}*${a0}" | bc)

cdelta=0.02
vdelta=0.2

for vstep in 0.0 -1.0 1.0 -2.0 2.0 -3.0 3.0 -4.0 4.0 -5.0 5.0; do

  tv0=$(echo "${v0}+$vstep*$vdelta" | bc)
  mkdir ev_data/data_$tv0

  for cstep in 0.0 -1.0 1.0 -2.0 2.0 -3.0 3.0 -4.0 4.0 -5.0 5.0; do
    tc0=$(echo "${coa0}+$cstep*$cdelta" | bc)
    ta0=$(echo "e(l(${tv0}/${coa0}/0.8660254037844386)/3.0)" | bc -l)
    python3 $writer_exec -symm=$struc -a=$ta0 -coa=$tc0 -name=$sysname
    mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec
    mv OSZICAR "./ev_data/data_$tv0/osz_$ta0.txt"
    mv DOSCAR "./ev_data/data_$tv0/dos_$ta0.txt"
    mv OUTCAR "./ev_data/data_$tv0/out_$ta0.txt"
    mv POSCAR "./ev_data/data_$tv0/pos_$ta0.txt"
  done

  cd ev_data/data_$tv0 ; python3 $sjeos_exec -sjeos -hcpcoa ; cd ../..
  ccopt="$(grep 'V0' ./ev_data/data_$tv0/sjeos_fit.csv)"
  ccopt=${ccopt:3}
  aaopt=$(echo "e(l(${tv0}/${ccopt}/0.8660254037844386)/3.0)" | bc -l)
  python3 $writer_exec -symm=$struc -a=$aaopt -coa=$ccopt -name=$sysname
  mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec
  mv OSZICAR "./ev_data/osz_$tv0.txt"
  mv DOSCAR "./ev_data/dos_$tv0.txt"
  mv OUTCAR "./ev_data/out_$tv0.txt"

done

cd ev_data/equil ; python3 $sjeos_exec -sjeos -hcpcoa; cd ../..
ccopt="$(grep 'V0' ./ev_data/equil/sjeos_fit.csv)"
ccopt=${ccopt:3}
aaopt=$(echo "e(l(${v0}/${ccopt}/0.8660254037844386)/3.0)" | bc -l)
echo $v0 $ccopt $aaopt

python3 $writer_exec -symm=$struc -a=$aaopt -coa=$ccopt -name=$sysname
mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec
mv OSZICAR "./ev_data/osz_equil.txt"
mv DOSCAR "./ev_data/dos_equil.txt"
mv OUTCAR "./ev_data/out_equil.txt"
mv POSCAR "./ev_data/pos_equil.txt"
