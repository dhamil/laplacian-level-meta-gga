from os import sys
from os import system
from setup_scripts import incar_script
import platform

to_do = ['C','GaAs','Ge','LiCl','LiF','MgO','Si','SiC']
fnls = {'PS': 'PBEsol', 'OFR2': 'OFR2', 'R2SL': 'r2SCAN-L', 'R2SCAN': 'r2SCAN'}
#fnls = ['pbesol','ofr2','r2scanl','r2scan']

def get_ef(flnm='DOSCAR'):

    dosfl = open(flnm,'r')
    for iln,ln in enumerate(dosfl):
        if iln == 5:
            ef = float(ln.split()[3])
            break
    dosfl.close()

    return ef

def write_gap_incar(opts={}):

    wd = {'deps': 1.e-2, 'NEDOS': 2001, 'file': 'DOSCAR'}
    for anopt in opts:
        wd[anopt] = opts[anopt]
    ndosh = (wd['NEDOS']-wd['NEDOS']%2)//2

    ef = get_ef(flnm=wd['file'])

    iopts = {'EMIN': ef-wd['deps']*ndosh, 'EMAX': ef+wd['deps']*ndosh, 'NEDOS':wd['NEDOS'],
    }
    if 'GGA' in wd:
        iopts['GGA'] = wd['GGA']
    if 'METAGGA' in wd:
        iopts['METAGGA'] = wd['METAGGA']
    if 'SYSTEM' in wd:
        iopts['SYSTEM'] = wd['SYSTEM']
    incar_script(ofl='INCAR_gap',opts=iopts)

    return

def get_gap(flnm='DOSCAR',quiet=False):

    cnt = 0
    found_cbm = False
    prev = [0.0,0.0]
    with open(flnm,'r') as tmpfl:

        for iln,ln in enumerate(tmpfl):
            if iln < 5:
                continue
            elif iln == 5:
                if len(ln.split())< 5:
                    print('WARNING: severe, something wrong with DOSCAR')
                    raise SystemExit()
                tmp = ln.split()
                nedos = int(tmp[2])
                ef = round(float(tmp[3]),3)
            else:

                if len(ln.split()) == 5 and cnt > 0:
                    break
                if cnt == nedos:
                    break

                tmp = [float(x) for x in (ln.strip()).split()]
                if (tmp[0] <= ef + 1.e-6) and (tmp[1] > 0.0) and (prev[1] > 0.0):
                    vbm = tmp[0]
                elif (tmp[0] >= ef - 1.e-6) and (tmp[1] > 0.0) and (prev[1]==0.0):
                    cbm = tmp[0]
                    found_cbm = True
                    break

                prev = [tmp[0],tmp[1]]
                cnt += 1
    if not found_cbm:
        # catch in case of metal
        cbm = vbm
    gap = cbm-vbm
    if not quiet:
        print('------------------------------')
        print('GAP = {:} eV'.format(gap))
        print('VBM = {:} eV '.format(vbm))
        print('CBM = {:} eV'.format(cbm))
        print('------------------------------')
    return gap

def poscar_writer(optd):
    opts = ['symm','a','b','c','coa','name','file']
    wd = {'name':'Unknown','file':'POSCAR'}
    for anopt in optd:
        if anopt in opts:
            wd[anopt] = optd[anopt]

    ostr = wd['name']+'\n'
    if wd['symm'] == 'sc':
        ostr += '    {:}\n'.format(wd['a'])
        ostr += '  1.0 0.0 0.0\n'+'  0.0 1.0 0.0\n'+'  0.0 0.0 1.0\n'
        ostr += ' 1\n'+' d\n'+ '0.0 0.0 0.0'
    elif wd['symm'] in ['fcc','ds','rs','zb']:
        ostr += '    {:}\n'.format(wd['a'])
        ostr += '  0.0 0.5 0.5\n'+'  0.5 0.0 0.5\n'+'  0.5 0.5 0.0\n'
        if wd['symm'] == 'fcc':
            ostr += ' 1\n'+' d\n'+ '0.0 0.0 0.0'
        elif wd['symm'] == 'ds':
            ostr += ' 2\n'+' d\n'+ '-0.125 -0.125 -0.125\n'+ ' 0.125  0.125  0.125\n'
        elif wd['symm'] == 'rs':
            ostr += ' 1 1\n'+' d\n' + '0.0 0.0 0.0\n'+'0.5 0.5 0.5\n'
        elif wd['symm'] == 'zb':
            ostr += ' 1 1\n'+' d\n'+ '0.0 0.0 0.0\n'+ ' 0.25  0.25  0.25\n'
    elif wd['symm'] == 'bcc':
        ostr += '    {:}\n'.format(wd['a'])
        ostr += '  -0.5  0.5  0.5\n'+'   0.5 -0.5  0.5\n'+'   0.5  0.5 -0.5\n'
        ostr += ' 1\n'+' d\n'+ '0.0 0.0 0.0'
    elif wd['symm'] == 'hcp':
        if 'c' in wd and 'coa' not in wd:
            wd['coa'] = float(wd['c'])/float(wd['a'])
        else:
            wd['coa']=float(wd['coa'])
        ostr += '    {:}\n'.format(wd['a'])
        ostr += '1.0000000000000000  0.0000000000000000  0.0000000000000000\n'
        ostr += '0.5000000000000000  0.8660254037844386  0.0000000000000000\n'
        ostr += '0.0000000000000000  0.0000000000000000  {:.16f}\n'.format(wd['coa'])
        ostr += '2\n' + ' d\n'
        ostr += '0.0000000000000000  0.0000000000000000  0.0000000000000000\n'
        ostr += '0.3333333333333333  0.3333333333333333  0.5000000000000000\n'
    ofl = open(wd['file'],'w+')
    ofl.write(ostr)
    ofl.close()
    return

def make_poscars():
    lc20_res_dir = '/Users/aaronkaplan/Dropbox/phd.nosync/lap_lev_mgga/vasp/lc20/data/'
    poscar_w_exec = '~/Desktop/vuk/poscar_writer.py'

    for fnl in fnls:
        wfile = lc20_res_dir+'lc_20_results_{:}.csv'.format(fnl)
        system('mkdir -p ./{:}_gaps'.format(fnl))
        for sol in to_do:
            system('cp -r ./{:} ./{:}_gaps/'.format(sol,fnl))
        tmpfl = open(wfile,'r')
        for iln,ln in enumerate(tmpfl):
            if iln == 0:
                continue
            if ln.strip() == '====':
                break
            tmp = [x for x in (ln.strip()).split(',')]
            a0 = float(tmp[1])
            tmp2 = tmp[0].split('(')
            sol = tmp2[0][:-1]
            struc = tmp2[1][:-1]
            if sol in to_do:
                wfl = './{:}_gaps/{:}/POSCAR'.format(fnl,sol)
                poscar_writer({'name':sol,'file':wfl,'symm':struc,'a':a0})

    return

def process_all_results():

    ofl = 'gap_summary.csv'
    wstr = 'Solid'

    for fnl in fnls:
        wstr += ', {:}'.format(fnls[fnl])
    wstr += ', Expt. \n'

    """
        Experimental reference values taken from
        T. Aschebrock and S. Kuemmel,
            Phys. Rev. Research 1, 033082 (2019)
            DOI: 10.1103/PhysRevResearch.1.033082

    """
    exptd = {'Ge': 0.74, 'Si': 1.17, 'GaAs': 1.52, 'SiC':2.42, 'C': 5.48,
        'MgO': 7.22, 'LiCl': 9.4, 'LiF':13.6}

    me = {fnl: 0.0 for fnl in fnls}
    mae = {fnl: 0.0 for fnl in fnls}
    nsol = len(exptd.keys())

    for isol,sol in enumerate(exptd):
        wstr += '{:}'.format(sol)
        for fnl in fnls:
            #wfl = './{:}_gaps/{:}/DOSCAR'.format(fnl,sol)
            wfl = './LC20_{:}/{:}/gap/DOS_gap'.format(fnl,sol)
            tgap = get_gap(flnm=wfl,quiet=True)
            wstr += ', {:.2f}'.format(tgap)
            err = tgap - exptd[sol]
            me[fnl] += err/nsol
            mae[fnl] += abs(err)/nsol
        wstr += ', {:.2f} \n'.format(exptd[sol])
    wstr += 'ME'
    for fnl in fnls:
        wstr += ', {:.2f}'.format(me[fnl])
    wstr += ', \n'
    wstr += 'MAE'
    for fnl in fnls:
        wstr += ', {:.2f}'.format(mae[fnl])
    wstr += ', \n'

    with open(ofl,'w+') as tmpfl:
        tmpfl.write(wstr)

    system('cp gap_summary.csv gap_summary.tex')
    if platform.system().lower() == 'darwin':
        system("sed -i '' -e 's/, / \& /g' gap_summary.tex")
    else:
        system("sed -i 's/, / \& /g' gap_summary.tex")

    str=''
    with open('gap_summary.tex','r') as tmpfl:
        for ln in tmpfl:
            str += ln.strip() + ' \\\\ \n'
    with open('gap_summary.tex','w') as tmpfl:
        tmpfl.write(str)

    return

if __name__ == "__main__":

    tmp = sys.argv
    if len(tmp) < 2:
        print('No option specified!')
    else:
        if tmp[1] == 'incar':
            optd = {}
            if len(tmp) > 2:
                opts = tmp[2:]
                for anopt in opts:
                    topt = anopt.split('=')
                    if topt[0] == 'NEDOS':
                        optd['NEDOS'] = int(topt[1])
                    elif topt[0] == 'deps':
                        optd['deps'] = float(topt[1])
                    else:
                        optd[topt[0]] = topt[1]
            write_gap_incar(opts=optd)

        elif tmp[1] == 'gap':
            get_gap()
        elif tmp[1] == 'setup':
            make_poscars()
        elif tmp[1] == 'process':
            process_all_results()
        else:
            raise SystemExit('Unknown option')
