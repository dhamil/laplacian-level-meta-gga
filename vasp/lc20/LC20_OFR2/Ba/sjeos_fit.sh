#!/bin/bash
#PBS -l walltime=12:00:00
#PBS -q normal
#PBS -l nodes=1:ppn=20
#PBS -N sjeos_Ba_OFR2
#PBS -j oe
#PBS -o oe.txt
#PBS -m a
#PBS -M kaplan@temple.edu

module --force purge ; module load intel-libs
cd "$PBS_O_WORKDIR"

sysname=Ba
struc=bcc

vasp_exec=/home/tuf53878/vasp6/bin/vasp_std
sjeos_exec=/home/tuf53878/llmgga_testing/lc20/sjeos.py
writer_exec=/home/tuf53878/llmgga_testing/lc20/poscar_writer.py
v_to_a_exec=/home/tuf53878/llmgga_testing/lc20/vol_to_cubic_lp.py

rm -rf ev_data
mkdir -p ev_data

for apar in 4.831 4.862 4.892 4.922 4.953 4.983 5.014 5.044 5.074 5.105 5.135 5.166 ; do 
  python3 $writer_exec -symm=$struc -a=$apar -name=$sysname
  mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec
  mv OSZICAR "./ev_data/osz_$apar.txt"
  mv DOSCAR "./ev_data/dos_$apar.txt"
  mv OUTCAR "./ev_data/out_$apar.txt"
done

cd ev_data ; python3 $sjeos_exec ; cd ..
v0="$(grep 'V0' ./ev_data/sjeos_fit.csv)"
v0=${v0:3}
a0=$(python3 $v_to_a_exec -symm=$struc -v=$v0 -print)
echo $a0