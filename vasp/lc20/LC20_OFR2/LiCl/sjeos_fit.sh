#!/bin/bash
#PBS -l walltime=12:00:00
#PBS -q normal
#PBS -l nodes=1:ppn=20
#PBS -N sjeos_LiCl_OFR2
#PBS -j oe
#PBS -o oe.txt
#PBS -m a
#PBS -M kaplan@temple.edu

module --force purge ; module load intel-libs
cd "$PBS_O_WORKDIR"

sysname=LiCl
struc=rs

vasp_exec=/home/tuf53878/vasp6/bin/vasp_std
sjeos_exec=/home/tuf53878/llmgga_testing/lc20/sjeos.py
writer_exec=/home/tuf53878/llmgga_testing/lc20/poscar_writer.py
v_to_a_exec=/home/tuf53878/llmgga_testing/lc20/vol_to_cubic_lp.py
gap_exec=/home/tuf53878/llmgga_testing/lc20/bandgaps_helper.py

rm -rf ev_data
mkdir -p ev_data

for apar in 4.897 4.928 4.959 4.989 5.02 5.051 5.082 5.113 5.143 5.174 5.205 5.236 ; do 
  python3 $writer_exec -symm=$struc -a=$apar -name=$sysname
  mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec
  mv OSZICAR "./ev_data/osz_$apar.txt"
  mv DOSCAR "./ev_data/dos_$apar.txt"
  mv OUTCAR "./ev_data/out_$apar.txt"
done

cd ev_data ; python3 $sjeos_exec ; cd ..
v0="$(grep 'V0' ./ev_data/sjeos_fit.csv)"
v0=${v0:3}
a0=$(python3 $v_to_a_exec -symm=$struc -v=$v0 -print)
echo $a0

rm -rf gap ; mkdir gap

printf -v aeq "%.3f" "$a0"
python3 $writer_exec -symm=$struc -a=$aeq -name=$sysname

mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec

python3 $gap_exec incar GGA=PE METAGGA=OFR2

mv INCAR INCAR_init
mv OSZICAR gap/OSZ_init
mv OUTCAR gap/OUT_init
mv DOSCAR gap/DOS_init
mv EIGENVAL gap/EIG_init
mv INCAR_gap INCAR

mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec

mv INCAR INCAR_gap
mv INCAR_init INCAR

python3 $gap_exec gap

mv OSZICAR gap/OSZ_gap
mv OUTCAR gap/OUT_gap
mv DOSCAR gap/DOS_gap
mv EIGENVAL gap/EIG_gap
