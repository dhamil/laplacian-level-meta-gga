#!/bin/bash
#PBS -l walltime=12:00:00
#PBS -q normal
#PBS -l nodes=1:ppn=20
#PBS -N sjeos_Sr_OFR2
#PBS -j oe
#PBS -o oe.txt
#PBS -m a
#PBS -M kaplan@temple.edu

module --force purge ; module load intel-libs
cd "$PBS_O_WORKDIR"

sysname=Sr
struc=fcc

vasp_exec=/home/tuf53878/vasp6/bin/vasp_std
sjeos_exec=/home/tuf53878/llmgga_testing/lc20/sjeos.py
writer_exec=/home/tuf53878/llmgga_testing/lc20/poscar_writer.py
v_to_a_exec=/home/tuf53878/llmgga_testing/lc20/vol_to_cubic_lp.py

rm -rf ev_data
mkdir -p ev_data

for apar in 5.833 5.87 5.907 5.944 5.98 6.017 6.054 6.09 6.127 6.164 6.2 6.237 ; do 
  python3 $writer_exec -symm=$struc -a=$apar -name=$sysname
  mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec
  mv OSZICAR "./ev_data/osz_$apar.txt"
  mv DOSCAR "./ev_data/dos_$apar.txt"
  mv OUTCAR "./ev_data/out_$apar.txt"
done

cd ev_data ; python3 $sjeos_exec ; cd ..
v0="$(grep 'V0' ./ev_data/sjeos_fit.csv)"
v0=${v0:3}
a0=$(python3 $v_to_a_exec -symm=$struc -v=$v0 -print)
echo $a0