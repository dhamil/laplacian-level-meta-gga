from os import sys,system
import numpy as np

def vol_to_cubic_lp(symm,vol,qprint=False):

    wd = {'nion': 1,'print':qprint,'v':vol,'symm':symm}

    if wd['symm'] == 'sc':
        wd['fac'] = 1.0
    elif wd['symm'] == 'bcc':
        wd['fac'] = 2.0
    elif wd['symm'] in ['fcc','ds','rs','zb']:
        wd['fac'] = 4.0
        if wd['symm'] in ['ds','rs','zb']:
            wd['nion'] = 2.0
    a0 = (wd['v']*wd['fac']*wd['nion'])**(1/3)
    if wd['print']:
        print(a0)
    return a0

def lc20_analysis():

    sjeos_exec = '~/Desktop/vuk/main.py -sjeos'
    dir = {'PBE': 'ev_data_pbe', 'LLMG': 'ev_data_llmg'}
    if sys.argv[1].upper() in dir:
        fnl = sys.argv[1].upper()
    else:
        raise ValueError(('Unrecognized functional {:}').format(sys.argv[1]))

    """
        reference cubic lattice constant data (in angstrom) from
        P. Hao, Y. Fang, J. Sun, G.I. Csonka, P.H.T. Philipsen, and J.P. Perdew,
        Phys. Rev. B 85, 014111 (2012), https://doi.org/10.1103/PhysRevB.85.014111
    """
    benchmarks = {
    'Li': 3.451, 'Na': 4.207, 'Ca': 5.555, 'Sr': 6.042,
    'Ba': 5.004,'Al': 4.019, 'Cu': 3.595, 'Rh': 3.793,
    'Pd': 3.876, 'Ag': 4.063, 'C': 3.555, 'SiC': 4.348,
    'Si': 5.422, 'Ge': 5.644, 'GaAs': 5.641, 'LiF': 3.974,
    'LiCl': 5.072, 'NaF': 4.570, 'NaCl': 5.565, 'MgO': 4.188
    }

    struc = {
    'Li': 'bcc', 'Na': 'bcc', 'Ca': 'fcc', 'Sr': 'fcc', 'Ba': 'bcc',
    'Al': 'fcc', 'Cu': 'fcc', 'Rh': 'fcc', 'Pd': 'fcc', 'Ag': 'fcc',
    'C': 'ds', 'SiC': 'zb', 'Si': 'ds', 'Ge': 'ds', 'GaAs': 'zb',
    'LiF': 'rs', 'LiCl': 'rs', 'NaF': 'rs', 'NaCl': 'rs', 'MgO': 'rs'
    }

    nsol = len(benchmarks)
    lps = np.zeros(nsol)
    errs = np.zeros(nsol)
    for isol,solid in enumerate(benchmarks):
        cdir = '{:}/{:}'.format(solid,dir[fnl])
        system(('cd {:} ; python3 {:}').format(cdir,sjeos_exec))
        with open(cdir+'/sjeos_fit.csv','r') as infl:
            for ln in infl:
                ln = ln.strip().split.(',')
                if ln[0]=='V0':
                    vuc = float(ln[1])
                    break
        lps[isol] = vol_to_cubic_lp(struc[solid],vuc)
        errs[isol] = lps[isol] - benchmarks[solid]

    me = np.sum(errs)/nsol
    mae = np.sum(np.abs(errs))/nsol

    with open('./lc_20_results_'+fnl+'.csv','w+') as ofl:
        ofl.write('Solid (struc), a0, a0 error\n')
        for isol,solid in enumerate(benchmarks):
            ofl.write(('{:} ({:}), {:}, {:}\n').format(solid,struc[solid],lps[isol],errs[isol]))
        ofl.write('====\n')
        ofl.write(('ME, {:}\n').format(me))
        ofl.write(('MAE, {:}').format(mae))

    return

if __name__=="__main__":

    lc20_analysis()
