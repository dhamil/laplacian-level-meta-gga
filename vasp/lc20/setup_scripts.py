import numpy as np
from os import system
from process_lc20 import a0_ref,struc

insuls = ['C','GaAs','Ge','LiCl','LiF','MgO','Si','SiC']
gga_regex = ['PE','PS']
mgga_regex = ['SCAN','R2SCAN','R2SL','OFR2','TASK']


def sjeos_script(sys,fnl):
    wdir = 'LC20_{:}/{:}'.format(fnl,sys)
    system('rm -rf '+wdir)
    system('mkdir -p '+wdir)

    str = "#!/bin/bash\n#PBS -l walltime=12:00:00\n#PBS -q normal\n\
#PBS -l nodes=1:ppn=20\n#PBS -N sjeos_{:}_{:}\n#PBS -j oe\n#PBS -o oe.txt\n\
#PBS -m a\n#PBS -M kaplan@temple.edu\n\n".format(sys,fnl)

    str += 'module --force purge ; module load intel-libs\ncd "$PBS_O_WORKDIR"\n\n'

    str+='sysname={:}\nstruc={:}\n\n'.format(sys,struc[sys])

    str+='vasp_exec=/home/tuf53878/vasp6/bin/vasp_std\n'
    str+='sjeos_exec=/home/tuf53878/llmgga_testing/lc20/sjeos.py\n'
    str+='writer_exec=/home/tuf53878/llmgga_testing/lc20/poscar_writer.py\n'
    str+='v_to_a_exec=/home/tuf53878/llmgga_testing/lc20/vol_to_cubic_lp.py\n'
    if sys in insuls:
        str += 'gap_exec=/home/tuf53878/llmgga_testing/lc20/bandgaps_helper.py\n'
    str+='\nrm -rf ev_data\nmkdir -p ev_data\n\nfor apar in '

    vsrch = 0.1
    npts = 12

    ascl = a0_ref[sys]*np.linspace((1-vsrch)**(1/3),(1 + vsrch)**(1/3),npts)
    al = [round(tmp,3) for tmp in ascl]

    for ap in al:
        str += '{:} '.format(ap)
    str += "; do \n  python3 $writer_exec -symm=$struc -a=$apar -name=$sysname\n"
    str += "  mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec\n"
    str += '  mv OSZICAR "./ev_data/osz_$apar.txt"\n'
    str += '  mv DOSCAR "./ev_data/dos_$apar.txt"\n'
    str += '  mv OUTCAR "./ev_data/out_$apar.txt"\ndone\n\n'
    str += 'cd ev_data ; python3 $sjeos_exec ; cd ..\n'
    str += 'v0="$(grep '+ "'V0'" + ' ./ev_data/sjeos_fit.csv)"\nv0=${v0:3}\n'
    str += 'a0=$(python3 $v_to_a_exec -symm=$struc -v=$v0 -print)\necho $a0'

    if sys in insuls:
        str += '\n\nrm -rf gap ; mkdir gap\n\n'
        str += 'printf -v aeq "%.3f" "$a0"\n'
        str += 'python3 $writer_exec -symm=$struc -a=$aeq -name=$sysname\n\n'
        str += "mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec\n\n"
        if sys == 'LiF':
            str += 'python3 $gap_exec incar NEDOS=3001 '
        else:
            str += 'python3 $gap_exec incar '
        if fnl in gga_regex:
            str += 'GGA={:}'.format(fnl)
        elif fnl in mgga_regex:
            str += 'GGA=PE METAGGA={:}'.format(fnl)
        str += '\n\nmv INCAR INCAR_init\n'
        str += 'mv OSZICAR gap/OSZ_init\nmv OUTCAR gap/OUT_init\nmv DOSCAR gap/DOS_init\n'
        str += 'mv EIGENVAL gap/EIG_init\n'
        str += 'mv INCAR_gap INCAR\n\n'
        str += "mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec\n\n"
        str += 'mv INCAR INCAR_gap\nmv INCAR_init INCAR\n\n'
        str += 'python3 $gap_exec gap\n\n'
        str += 'mv OSZICAR gap/OSZ_gap\nmv OUTCAR gap/OUT_gap\nmv DOSCAR gap/DOS_gap\n'
        str += 'mv EIGENVAL gap/EIG_gap\n'

    tmpfl = open(wdir+'/'+'sjeos_fit.sh','w+')
    tmpfl.write(str)
    tmpfl.close()

    return wdir

def incar_script(ofl='./INCAR',opts={}):

    tagd = {'SYSTEM': 'unknown', 'NCORE': 4, 'KPAR': 5, 'ALGO': 'Normal', 'PREC': 'Accurate',
    'EDIFF': 1.e-6, 'ENCUT': 800, 'ISMEAR': -5, 'KSPACING': 0.08, 'KGAMMA': True,
    'GGA': 'PE', 'NSW': 0, 'IBRION': -1, 'NELM': 200, 'NELMIN': 6,
    'ADDGRID': False, 'LMIXTAU': True, 'LASPH': True, 'LMAXMIX': 4, 'LREAL': False,
    'LWAVE': False, 'LCHARG': False, 'LVTOT': False, 'LORBIT': 11
    }
    for akey in opts:
        tagd[akey] = opts[akey]

    npad = 10
    str = ''
    for akey in tagd:
        padstr = ' '*(npad - len(akey))
        if akey in ['EDIFF','EDIFFG']:
            str += '{:}{:}= '.format(akey,padstr) + ('{:} \n'.format(tagd[akey])).upper()
        elif type(tagd[akey]) is bool:
            str += '{:}{:}= .{:}. \n'.format(akey,padstr,tagd[akey])
        else:
            str += '{:}{:}= {:} \n'.format(akey,padstr,tagd[akey])

    tmpfl = open(ofl,'w+')
    tmpfl.write(str)
    tmpfl.close()
    return

if __name__=="__main__":

    """
    for sol in a0_ref:
        system('mkdir -p LC20_POTCARs/{:}'.format(sol))
        system('cp lc20_inputs/{:}/POTCAR LC20_POTCARs/{:}/POTCAR '.format(sol,sol))
    exit()
    """
    for fnl in ['PS','R2SCAN','R2SL','OFR2']:
        optd = {}
        if fnl in gga_regex:
            optd['GGA'] = fnl
        elif fnl in mgga_regex:
            optd['GGA'] = 'PE'
            optd['METAGGA'] = fnl
        system('rm -rf ./LC20_{:}'.format(fnl))
        system('mkdir ./LC20_{:}'.format(fnl))
        system('cp run.py ./LC20_{:}/run.py'.format(fnl))
        for sol in a0_ref:
            optd['SYSTEM'] = sol
            dir = sjeos_script(sol,fnl)
            incar_script(ofl=dir+'/INCAR',opts=optd)
            system('cp LC20_POTCARs/{:}/POTCAR ./LC20_{:}/{:}/POTCAR'.format(sol,fnl,sol))
