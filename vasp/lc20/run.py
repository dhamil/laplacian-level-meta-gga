from os import system

sols = [
    'Li','Na', 'Ca', 'Sr', 'Ba','Al', 'Cu', 'Rh', 'Pd', 'Ag',
    'C', 'SiC', 'Si', 'Ge', 'GaAs', 'LiF', 'LiCl', 'NaF', 'NaCl', 'MgO'
    ]
for sol in sols:
    wstr = 'cd {:} ; printf '.format(sol)+'"%s "'+' "$(echo {:} ; qsub sjeos_fit.sh)" '.format(sol)
    #wstr = 'cd LC20_POTCARs/{:} ; printf '.format(sol)+'"%s "'+' $( echo {:} ; grep TITEL POTCAR ) "\n"'.format(sol)
    system(wstr)
