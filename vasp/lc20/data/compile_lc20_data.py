import sys
sys.path.append('..')
from process_lc20 import a0_ref,b0_ref,struc

to_do = {
    'PS': 'PBEsol', 'SCAN': 'SCAN','R2SCAN': 'r$^2$SCAN', 'R2SL': 'r$^2$SCAN-L',
    'OFR2': 'OFR2'
}

for iwork in range(2):

    dat = {}
    errs = {}

    if iwork == 0:
        refd = a0_ref
        targf_pre = 'a0_data/LC20_a0_results_'
        outf = './LC20_a0_results.tex'
        unit = '\AA{}'
    elif iwork == 1:
        refd = b0_ref
        targf_pre = 'B0_data/LC20_B0_results_'
        outf = './LC20_B0_results.tex'
        unit = 'GPa'

    for fnl in to_do:
        dat[fnl] = []
        errs[fnl] = []
        wfl = open(targf_pre+'{:}.csv'.format(fnl.lower()),'r')
        for iln,ln in enumerate(wfl):

            wln = ln.strip().split(',')
            if 1 <= iln <= 20:
                dat[fnl].append(float(wln[1]))
                errs[fnl].append(float(wln[2]))
            elif 22 <= iln:
                if wln[0] == '====':
                    continue
                errs[fnl].append(float(wln[1]))
        wfl.close()

    fstr = 'Solid (structure) & Reference ({:})'.format(unit)
    for fnl in to_do:
        fstr += ' & {:}'.format(to_do[fnl])
    fstr += ' \\\\ \hline \n'

    for isol,sol in enumerate(refd):
        if isol == len(refd)-1:
            tchar = '\hline \n'
        else:
            tchar = '\n'
        fstr += '{:} ({:}) & {:}'.format(sol,struc[sol],refd[sol])
        for fnl in to_do:
            fstr += ' & {:.3f}'.format(errs[fnl][isol])
        fstr += ' \\\\ {:}'.format(tchar)

    isol += 1
    fstr += 'ME & (metals)'
    for fnl in to_do:
        fstr += ' & {:.3f}'.format(errs[fnl][isol])
    fstr += '\\\\ \n'
    isol += 1
    fstr += 'MAE & (metals)'
    for fnl in to_do:
        fstr += ' & {:.3f}'.format(errs[fnl][isol])
    fstr += ' \\\\ \hline \n'

    isol += 1
    fstr += 'ME & (insulators)'
    for fnl in to_do:
        fstr += ' & {:.3f}'.format(errs[fnl][isol])
    fstr += ' \\\\ \n'
    isol += 1
    fstr += 'MAE & (insulators)'
    for fnl in to_do:
        fstr += ' & {:.3f}'.format(errs[fnl][isol])
    fstr += ' \\\\ \hline \n'

    isol += 1
    fstr += 'ME & (total)'
    for fnl in to_do:
        fstr += ' & {:.3f}'.format(errs[fnl][isol])
    fstr += ' \\\\ \n'
    isol += 1
    fstr += 'MAE & (total)'
    for fnl in to_do:
        fstr += ' & {:.3f}'.format(errs[fnl][isol])
    fstr += ' \\\\ \n'

    wfl = open(outf,'w+')
    wfl.write(fstr)
    wfl.close()
