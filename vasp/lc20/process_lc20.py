from os import sys,system
from glob import glob
import numpy as np

from sjeos import sjeos_fit

"""
    reference cubic lattice constant data with zero-point corrections
    (in angstrom) from
    P. Hao, Y. Fang, J. Sun, G.I. Csonka, P.H.T. Philipsen, and J.P. Perdew,
    Phys. Rev. B 85, 014111 (2012), https://doi.org/10.1103/PhysRevB.85.014111
"""
a0_ref = {
'Li': 3.451, 'Na': 4.207, 'Ca': 5.555, 'Sr': 6.042,
'Ba': 5.004,'Al': 4.019, 'Cu': 3.595, 'Rh': 3.793,
'Pd': 3.876, 'Ag': 4.063, 'C': 3.555, 'SiC': 4.348,
'Si': 5.422, 'Ge': 5.644, 'GaAs': 5.641, 'LiF': 3.974,
'LiCl': 5.072, 'NaF': 4.570, 'NaCl': 5.565, 'MgO': 4.188
}

eV_ang3_to_gpa = 1.602176634e2 # https://physics.nist.gov/cgi-bin/cuu/Value?e

"""
    bulk moduli in GPa with zero point corrections,
    from Table S8 from the SI of
    F. Tran, J. Stelzl, and P. Blaha, J. Chem. Phys. 144, 204120 (2016),
    https://doi.org/10.1063/1.4948636
"""
b0_ref = {
    'Li': 13.1, 'Na': 7.9, 'Ca': 15.9, 'Sr': 12.0,
    'Ba': 10.6, 'Al': 77.1, 'Cu': 144.3, 'Rh': 277.1,
    'Pd': 187.2, 'Ag': 105.7, 'C': 454.7, 'SiC': 229.1,
    'Si': 101.3, 'Ge': 79.4, 'GaAs': 76.7, 'LiF': 76.3,
    'LiCl': 38.7, 'NaF': 53.1, 'NaCl': 27.6, 'MgO': 169.8
}

struc = {
'Li': 'bcc', 'Na': 'bcc', 'Ca': 'fcc', 'Sr': 'fcc', 'Ba': 'bcc',
'Al': 'fcc', 'Cu': 'fcc', 'Rh': 'fcc', 'Pd': 'fcc', 'Ag': 'fcc',
'C': 'ds', 'SiC': 'zb', 'Si': 'ds', 'Ge': 'ds', 'GaAs': 'zb',
'LiF': 'rs', 'LiCl': 'rs', 'NaF': 'rs', 'NaCl': 'rs', 'MgO': 'rs'
}

def find_failures(opts={'out_fl':'OUTCAR','osz_fl':'OSZICAR','log_fl':'failed_runs.csv','write_to_file':True}):

    system('grep NELM {:} > ._temp_outcar_file_'.format(opts['out_fl']))
    with open('._temp_outcar_file_','r') as tmpfl:
        for ln in tmpfl:
            wln = ln.split()
            if wln[0] == 'NELM':
                if wln[2][-1]==';':
                    nelm = int(wln[2][:-1])
                else:
                    nelm = int(wln[2])
    system('rm ._temp_outcar_file_')
    runs = []
    with open(opts['osz_fl'],'r') as tmpfl:
        for iln,ln in enumerate(tmpfl):
            wln = ln.split()
            if wln[1]=='F=':
                runs.append([int(wln[0]),last_it,last_it<nelm])
            else:
                if wln[0] != 'N':
                    last_it = int(wln[1])
    tstr = 'Cycle, No. SCF steps, No. SCF steps < NELM = {:}\n'.format(nelm)
    nfail = 0
    for i in range(len(runs)):
        tstr += '{:}, {:}, {:}\n'.format(*runs[i])
        if not runs[i][2]:
            nfail += 1
    tstr += '====,====,====\n'
    tstr += 'No. failed runs, {:}\n'.format(nfail)
    if opts['write_to_file']:
        tmpfl = open(opts['log_fl'],'w+')
        tmpfl.write(tstr)
        tmpfl.close()
    return tstr,runs,nfail,nelm

def find_all_failed_runs_lc20(dirs,logfile='all_runs.txt'):

    wstr = ''

    nfail = 0
    nrun = 0
    for iadir,adir in enumerate(dirs):

        wstr += '{:}\n'.format(adir)
        osz_l = glob(adir+'/osz_*.txt')
        wstr += 'File, Iterations, NELM, Its < NELM \n'
        for oszfl in osz_l:
            osztmp = oszfl.split('osz_')
            outfl = osztmp[0]+'out_'+osztmp[-1]
            tstr,runs,tfail,nelm = find_failures(opts={'out_fl':outfl,'osz_fl':oszfl,'write_to_file':False})
            wstr += '{:}, {:}, {:}, {:}\n'.format(oszfl.split('/')[-1],runs[0][1],nelm,runs[0][2])
            nfail += tfail
            nrun += 1
        if iadir < len(dirs)-1:
            wstr += '====,====,====,====\n'
    ostr = 'Total, number of, failed runs, {:}\n'.format(nfail)
    ostr += 'out of, {:} total runs ({:}%) \n\n'.format(nrun,nfail/nrun*100)
    ostr += wstr
    tmpfl = open(logfile,'w+')
    tmpfl.write(ostr)
    tmpfl.close()

    return


def vol_to_cubic_lp(symm,vol,qprint=False):

    wd = {'nion': 1,'print':qprint,'v':vol,'symm':symm}

    if wd['symm'] == 'sc':
        wd['fac'] = 1.0
    elif wd['symm'] == 'bcc':
        wd['fac'] = 2.0
    elif wd['symm'] in ['fcc','ds','rs','zb']:
        wd['fac'] = 4.0
        if wd['symm'] in ['ds','rs','zb']:
            wd['nion'] = 2.0
    a0 = (wd['v']*wd['fac']*wd['nion'])**(1/3)
    if wd['print']:
        print(a0)
    return a0

def lc20_analysis():

    #sjeos_exec = '~/Desktop/vuk/main.py -sjeos'
    fnl = sys.argv[1]#.lower()
    bdir = 'LC20_'+fnl#'./results_'+fnl

    metals = ['Li', 'Na', 'Ca', 'Sr', 'Ba', 'Al', 'Cu', 'Rh', 'Pd', 'Ag']
    nmetals = len(metals)
    ninsul = len(struc.keys())-nmetals

    nsol = len(a0_ref)
    lps = np.zeros(nsol)
    a0_errs = np.zeros(nsol)
    a0_mae_mets = 0.0
    a0_mae_insul = 0.0
    a0_me_mets = 0.0
    a0_me_insul = 0.0

    b0_errs = np.zeros(nsol)
    bms = np.zeros(nsol)
    b0_mae_mets = 0.0
    b0_mae_insul = 0.0
    b0_me_mets = 0.0
    b0_me_insul = 0.0

    for isol,solid in enumerate(a0_ref):
        cdir = '{:}/{:}/ev_data'.format(bdir,solid)
        """
        system(('cd {:} ; python3 {:}').format(cdir,sjeos_exec))
        syspars = {}
        with open(cdir+'/sjeos_fit.csv','r') as infl:
            for ln in infl:
                ln = ln.strip().split(',')
                if ln[0]=='V0':
                    syspars['V0'] = float(ln[1])
                elif ln[0] == 'B0':
                    syspars['B0'] = eV_ang3_to_gpa*float(ln[1])
                if len(syspars)==2:
                    break
        """
        syspars = sjeos_fit(hcpcoa=False,wdir=cdir,soft_exit=True)
        syspars['B0'] *= eV_ang3_to_gpa

        if (syspars['V0'] != syspars['V0']) or (syspars['B0'] != syspars['B0']) or syspars['V0'] < 0.0 :
            a0_errs[isol] = 0.0
            b0_errs[isol] = 0.0
            nsol -= 1
            if solid in metals:
                 nmetals -= 1
            else:
                 ninsul -= 1
        else:
            lps[isol] = vol_to_cubic_lp(struc[solid],syspars['V0'])
            a0_errs[isol] = lps[isol] - a0_ref[solid]
            bms[isol] = syspars['B0']
            b0_errs[isol] = syspars['B0'] - b0_ref[solid]

        if solid in metals:
            a0_mae_mets += abs(a0_errs[isol])
            a0_me_mets += a0_errs[isol]

            b0_mae_mets += abs(b0_errs[isol])
            b0_me_mets += b0_errs[isol]
        else:
            a0_mae_insul += abs(a0_errs[isol])
            a0_me_insul += a0_errs[isol]

            b0_mae_insul += abs(b0_errs[isol])
            b0_me_insul += b0_errs[isol]

    a0_me = np.sum(a0_errs)/nsol
    a0_mae = np.sum(np.abs(a0_errs))/nsol

    a0_mae_mets /= nmetals
    a0_me_mets /= nmetals
    a0_mae_insul /= ninsul
    a0_me_insul /= ninsul

    b0_me = np.sum(b0_errs)/nsol
    b0_mae = np.sum(np.abs(b0_errs))/nsol

    b0_mae_mets /= nmetals
    b0_me_mets /= nmetals
    b0_mae_insul /= ninsul
    b0_me_insul /= ninsul

    with open('./'+bdir+'/LC20_a0_results_'+fnl+'.csv','w+') as ofl:
        ofl.write('Solid (struc), a0, a0 error\n')
        for isol,solid in enumerate(a0_ref):
            ofl.write(('{:} ({:}), {:}, {:}\n').format(solid,struc[solid],lps[isol],a0_errs[isol]))
        ofl.write('====\n')
        ofl.write(('ME (metals), {:}\n').format(a0_me_mets))
        ofl.write(('MAE (metals), {:}\n').format(a0_mae_mets))
        ofl.write('====\n')
        ofl.write(('ME (insulators), {:}\n').format(a0_me_insul))
        ofl.write(('MAE (insulators), {:}\n').format(a0_mae_insul))
        ofl.write('====\n')
        ofl.write(('ME (total), {:}\n').format(a0_me))
        ofl.write(('MAE (total), {:}').format(a0_mae))

    with open('./'+bdir+'/LC20_B0_results_'+fnl+'.csv','w+') as ofl:
        ofl.write('Solid (struc), B0, B0 error\n')
        for isol,solid in enumerate(a0_ref):
            ofl.write(('{:} ({:}), {:}, {:}\n').format(solid,struc[solid],bms[isol],b0_errs[isol]))
        ofl.write('====\n')
        ofl.write(('ME (metals), {:}\n').format(b0_me_mets))
        ofl.write(('MAE (metals), {:}\n').format(b0_mae_mets))
        ofl.write('====\n')
        ofl.write(('ME (insulators), {:}\n').format(b0_me_insul))
        ofl.write(('MAE (insulators), {:}\n').format(b0_mae_insul))
        ofl.write('====\n')
        ofl.write(('ME (total), {:}\n').format(b0_me))
        ofl.write(('MAE (total), {:}').format(b0_mae))

    wdir = []
    for sol in struc:
        wdir.append(bdir+'/'+sol+'/ev_data')
    find_all_failed_runs_lc20(wdir,logfile=bdir+'/'+fnl+'_LC20_runs.csv')

    return

if __name__=="__main__":

    lc20_analysis()
