subroutine meta_lsd_x(nu,nd,tauu,taud,ex,dexdnu,dexdnd,dexdtauu,dexdtaud)
  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in) :: nu,nd,tauu,taud
  real(dp), intent(out) :: ex,dexdnu,dexdnd,dexdtauu,dexdtaud

  real(dp) :: exu,exd

  exu = 0._dp ; exd = 0._dp
  dexdnu = 0._dp ; dexdnd = 0._dp
  dexdtauu = 0._dp ; dexdtaud = 0._dp 
  if (nu > 0._dp) call meta_lsd_x_unp(2._dp*nu,2._dp*tauu,exu,dexdnu,dexdtauu)
  if (nd > 0._dp) call meta_lsd_x_unp(2._dp*nd,2._dp*taud,exd,dexdnd,dexdtaud)
  ex = (exu + exd)/2._dp

end subroutine meta_lsd_x

subroutine meta_lsd_x_unp(n,tau,ex,dexdn,dexdtau)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in) :: n,tau
  real(dp), intent(out) :: ex,dexdn,dexdtau

  real(dp), parameter :: nmix = 1._dp/6._dp
  real(dp), parameter :: pi = 3.14159265358979323846264338327950288419_dp

  real(dp) :: kf,exlda,d_exlda_dn,tau0,t,d_t_dn,d_t_dtau,fx,dfx

  kf = (3._dp*pi**2*n)**(1._dp/3._dp)
  exlda = -3._dp/(4._dp*pi)*kf*n
  d_exlda_dn = -kf/pi

  tau0 = 0.3_dp*kf**2*n

  t = tau/tau0
  d_t_dn = -5._dp/3._dp*t/n
  d_t_dtau = 1._dp/tau0

  fx = t**(4._dp*nmix/5._dp)
  dfx = (4._dp*nmix/5._dp)*fx/t

  ex = fx*exlda
  dexdn = fx*d_exlda_dn + exlda*dfx*d_t_dn
  dexdtau = exlda*dfx*d_t_dtau

end subroutine meta_lsd_x_unp
