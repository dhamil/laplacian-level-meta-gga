----------------------------------------
Begin Batch Job Prologue Thu Oct 21 12:22:12 EDT 2021
Job ID:           433508
Username:         tuf53878
Group:            perdew
Job Name:         Na_llmgga
Resources List:   walltime=02:00:00,nodes=1:ppn=20,neednodes=1:ppn=20
Queue:            normal
Account:          
Nodes:            e022 
----------------------------------------
End Batch Job Prologue Thu Oct 21 12:22:12 EDT 2021
----------------------------------------
 running on   20 total cores
 distrk:  each k-point on   20 cores,    1 groups
 distr:  one band on    4 cores,    5 groups
 using from now: INCAR     
 vasp.6.1.2 22Jul20 (build Oct 19 2021 11:46:22) complex                        
  
 POSCAR found :  1 types and       1 ions
 LDA part: xc-table for Pade appr. of Perdew
 generate k-points for:   27   27   27
 POSCAR, INCAR and KPOINTS ok, starting setup
 FFT: planning ...
 WAVECAR not read
 entering main loop
       N       E                     dE             d eps       ncg     rms          rms(c)
DAV:   1     0.170127677796E+02    0.17013E+02   -0.48650E+03 11200   0.835E+02
DAV:   2    -0.379184428454E+01   -0.20805E+02   -0.20801E+02 13120   0.102E+02
DAV:   3    -0.401887967816E+01   -0.22704E+00   -0.22703E+00 14290   0.148E+01
DAV:   4    -0.401998358514E+01   -0.11039E-02   -0.11039E-02 15130   0.972E-01
DAV:   5    -0.401998989840E+01   -0.63133E-05   -0.63133E-05 15240   0.566E-02    0.195E-01
DAV:   6    -0.352019713122E+01    0.49979E+00   -0.18698E-01 11450   0.477E+00    0.291E-01
DAV:   7    -0.355081685269E+01   -0.30620E-01   -0.10815E-02 11940   0.117E+00    0.230E-01
DAV:   8    -0.355385344155E+01   -0.30366E-02   -0.14094E-03 15140   0.505E-01    0.155E-01
DAV:   9    -0.355506197466E+01   -0.12085E-02   -0.38974E-03 11810   0.103E+00    0.557E-02
DAV:  10    -0.355491967955E+01    0.14230E-03   -0.16856E-04 13200   0.197E-01    0.303E-02
DAV:  11    -0.355481528182E+01    0.10440E-03   -0.31674E-04 11540   0.286E-01    0.121E-02
DAV:  12    -0.355479011035E+01    0.25171E-04   -0.14734E-05 11490   0.654E-02    0.575E-04
DAV:  13    -0.355478681541E+01    0.32949E-05   -0.96973E-08 11270   0.451E-03    0.431E-04
DAV:  14    -0.355477814757E+01    0.86678E-05   -0.31065E-08  6020   0.233E-03    0.103E-04
DAV:  15    -0.355477702781E+01    0.11198E-05   -0.55554E-09  5720   0.978E-04    0.439E-05
DAV:  16    -0.355477676560E+01    0.26221E-06   -0.97630E-10  5710   0.382E-04
   1 F= -.35547768E+01 E0= -.35547768E+01  d E =0.000000E+00
 -----------------------------------------------------------------------------
|                                                                             |
|           W    W    AA    RRRRR   N    N  II  N    N   GGGG   !!!           |
|           W    W   A  A   R    R  NN   N  II  NN   N  G    G  !!!           |
|           W    W  A    A  R    R  N N  N  II  N N  N  G       !!!           |
|           W WW W  AAAAAA  RRRRR   N  N N  II  N  N N  G  GGG   !            |
|           WW  WW  A    A  R   R   N   NN  II  N   NN  G    G                |
|           W    W  A    A  R    R  N    N  II  N    N   GGGG   !!!           |
|                                                                             |
|     Tetrahedron method does not include variations of the Fermi             |
|     occupations, so forces and stress will be inaccurate. We suggest        |
|     using a different smearing scheme ISMEAR = 1 or 2 for metals in         |
|     relaxations.                                                            |
|                                                                             |
 -----------------------------------------------------------------------------

----------------------------------------
Begin Batch Job Epilogue Thu Oct 21 12:24:29 EDT 2021
Job ID:           433508
Username:         tuf53878
Group:            perdew
Job Name:         Na_llmgga
Session:          3212
Limits:           walltime=02:00:00,nodes=1:ppn=20,neednodes=1:ppn=20
Resources:        cput=00:45:11,vmem=18954184kb,walltime=00:02:17,mem=2113424kb,energy_used=0
Queue:            normal
Account:          
----------------------------------------
End Batch Job Epilogue Thu Oct 21 12:24:50 EDT 2021
----------------------------------------
