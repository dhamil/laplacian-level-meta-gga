import numpy as np
import matplotlib.pyplot as plt
from os.path import isfile

fnls = [('ofr2','OFR2'),('pbesol','PBEsol')]#('r2scan','r$^2$SCAN')]
colors = ['darkblue','darkorange','tab:green']
lsls = ['-','--','-.']

# geometry should be same for these
with open('./ofr2/basis_vectors.csv'.format(dir),'r') as infl:
    for iln,ln in enumerate(infl):
        if iln == 2:
            a0h = abs(float((ln.strip()).split(',')[0]))
            a0 = 2*a0h
            break

fig,ax = plt.subplots(figsize=(5,4))

ybds = [1e20,-1e20]
for i in range(len(fnls)):

    dir, plabel = fnls[i]

    if isfile('./{:}_locpot_origin_to_bc.csv'.format(dir)):
        absc,ord = np.transpose(np.genfromtxt('./{:}_locpot_origin_to_bc.csv'.format(dir),
            delimiter=',',skip_header=1))
    else:
        x,y,z,vks = np.transpose(np.genfromtxt('./{:}/locpot_unp.csv'.format(dir),
            delimiter=',',skip_header=1))

        maskxy = np.abs(x - y) < 1.e-12
        maskyz = np.abs(y - z) < 1.e-12
        maskzx = np.abs(z - x) < 1.e-12
        mask = maskxy & maskyz & maskzx

        xn = x[mask]
        vn = vks[mask]

        mask = xn <= a0h
        absc = xn[mask]/a0
        ord = vn[mask]

        np.savetxt('./{:}_locpot_origin_to_bc.csv'.format(dir),np.transpose((absc,ord)),
            delimiter=',',header='x=y=z (a0), v_ks_loc (eV)')

    ybds[0] = min(ybds[0],ord.min())
    ybds[1] = max(ybds[1],ord.max())

    ax.plot(absc,ord,label=plabel,linewidth=2,linestyle=lsls[i],color=colors[i])

autoa = 0.529177249 # from VASP's constant.inc
rcore = 2.200*autoa
ax.set_xlim([0,1/2])
ax.set_ylim([1.05*ybds[0],1.05*ybds[1]])
ticks = [i/8 for i in range(5)]
ticknames = ['0.0', '$a_0/8$', '$a_0/4$', '$3a_0/8$', '$a_0/2$']
plt.xticks(ticks,ticknames)
ax.set_xlabel('$x=y=z$',fontsize=14)
ax.set_ylabel('$v_\\mathrm{KS}^\\mathrm{local}$ (eV)',fontsize=14)

ax.hlines(0.0,*ax.get_xlim(),color='gray',linestyle='--')
ax.vlines(rcore/a0,*ax.get_ylim(),color='gray',linestyle='--')
ax.vlines(1/2 - rcore/a0,*ax.get_ylim(),color='gray',linestyle='--')

ax.annotate('C',(rcore/(2*a0),0.85*ax.get_ylim()[1]),fontsize=14)
ax.annotate('C',(1/2 - rcore/(2*a0),0.85*ax.get_ylim()[1]),fontsize=14)
ax.annotate('V',(1/4,0.85*ax.get_ylim()[1]),fontsize=14)

plt.title('Na local KS potential; OFR2 (blue) and PBEsol (orange)',fontsize=12)
#ax.legend(fontsize=14,title='Na',title_fontsize=14,loc='upper center',
#    bbox_to_anchor=(1.04, 1.0))
#plt.show()
plt.savefig('./Na_local_vks.pdf',bbox_inches='tight',dpi=600)
