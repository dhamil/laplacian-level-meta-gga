program main

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), parameter :: cxv(8) = (/ 1._dp, -0.667_dp, -0.4445555_dp,&
  &     -0.663086601049_dp, 1.451297044490_dp, -0.887998041597_dp,&
  &     0.234528941479_dp, -0.023185843322_dp /)
  integer :: i
  real(dp) :: f1,f2

  f1 = 0._dp
  f2 = 0._dp
  do i = 1,7
      f1 = f1 + (1._dp*i)*cxv(i+1)
      f2 = f2 + i*(i-1._dp)*cxv(i+1)
  end do
  print*,f1,f2

end program main
