from os import sys, system, path
import numpy as np
import matplotlib.pyplot as plt
from random import randint

def wincar(kpar,ncore):
    istr = 'NCORE     =  {:}\n'.format(ncore)
    istr += 'KPAR      =  {:}\n'.format(kpar)
    istr += 'ALGO      =  Normal\nPREC      =  Accurate\nENCUT     =  600 \
    \nISMEAR    =  -5 \nKSPACING  =  0.08 \nKGAMMA    = .TRUE. \nGGA = PE \
    \nMETAGGA = R2SCAN \nNSW       =  0 \nIBRION    =  -1 \nNELM = 100 \nNELMIN = 6 \
    \nADDGRID = .FALSE. \nLMIXTAU = .TRUE. \nLASPH = .TRUE. \nLMAXMIX = 4 \
    \nLREAL   = .FALSE. \nLWAVE = FALSE \nLCHARG = FALSE \nLORBIT = 11'
    with open('INCAR','w+') as ofl:
        ofl.write(istr)
    return


if __name__=='__main__':

    wd = {'nc':1, 'kp':1}
    for tmp in sys.argv[2:]:
        if '=' in tmp:
            tmp2 = tmp.split('=')
            if tmp2[0] == 'kp':
                wd['kp'] = int(tmp2[1])
            elif tmp2[0] == 'nc':
                wd['nc'] = int(tmp2[1])

    if sys.argv[1] == 'wincar':
        wincar(wd['kp'],wd['nc'])
    elif sys.argv[1] == 'append':

        tmpfl = 'tmp_'
        for i in range(5):
            tmpfl += '{:}'.format(randint(0,9))
        tmpfl += '.txt'
        system("grep 'Total CPU time used' OUTCAR > {:}".format(tmpfl))
        with open(tmpfl,'r') as infl:
            for iln,ln in enumerate(infl):
                rtime = ln.strip().split()[-1]
                break
        system("rm {:}".format(tmpfl))
        if path.isfile('./vasp_runs.csv'):
            char = 'a'
            wheader=False
        else:
            char = 'w+'
            wheader=True
        tfile = open('./vasp_runs.csv',char)
        if wheader:
            tfile.write('KPAR, NCORE, RTIME (s)\n')
        tfile.write('{:},{:},{:}\n'.format(wd['kp'],wd['nc'],rtime))
        tfile.close()
    elif sys.argv[1] == 'proc':
        dat = np.genfromtxt('./vasp_runs.csv',delimiter=',',skip_header=1)
        datavg = [[*dat[0],dat[0,2]**2]]
        datnm = [1]
        ncore_used = []
        for i in range(1,dat.shape[0]):
            nomatch = True
            for j in range(len(datavg)):
                if dat[i,1] not in ncore_used:
                    ncore_used.append(dat[i,1])
                if dat[i,0]==datavg[j][0] and dat[i,1]==datavg[j][1]:
                    datavg[j][2] += dat[i,2]
                    datavg[j][3] += dat[i,2]**2
                    datnm[j] += 1
                    nomatch = False
                    break
            if nomatch:
                datavg.append([*dat[i],dat[i,2]**2])
                datnm.append(1)
        for i in range(len(datavg)):
            datavg[i][2] /= datnm[i]
            datavg[i][3] /= datnm[i]

        datavg = np.asarray(datavg)
        datavg[:,3] = (np.maximum(0.0,datavg[:,3]-datavg[:,2]**2))**(0.5)

        np.savetxt('./vasp_runs_averaged.csv',datavg[np.argsort(datavg[:,2])],delimiter=',',
            header='KPAR, NCORE, Avg. runtime (s), Std. dev rtime (s)')
        ibest = np.argmin(datavg[:,-2])
        print('BEST:')
        print('KPAR={:}, NCORE={:}, runtime={:} s'.format(*datavg[ibest]))

        possk = {}
        for ik,ak in enumerate(datavg[:,0]):
            if ak not in possk:
                possk[ak] = []
            possk[ak].append([datavg[ik,1],datavg[ik,2]])

        for ak in possk:
            tmp = np.asarray(possk[ak])
            plt.plot(tmp[:,0],np.log10(tmp[:,1]),label='KPAR={:}'.format(ak))
        plt.xticks(ncore_used)
        plt.xlabel('NCORE')
        plt.ylabel('$\log_{10}[\\mathrm{CPU~time~(sec)}]$')
        plt.legend()
        plt.show()
