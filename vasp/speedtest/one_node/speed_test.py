from os import sys, system, path

def wincar(kpar,ncore):
    istr = 'NCORE     =  {:}\n'.format(ncore)
    istr += 'KPAR      =  {:}\n'.format(kpar)
    istr += 'ALGO      =  Normal\nPREC      =  Accurate\nENCUT     =  600 \
    \nISMEAR    =  -5 \nKSPACING  =  0.08 \nKGAMMA    = .TRUE. \nGGA = PE \
    \nMETAGGA = R2SCAN \nNSW       =  0 \nIBRION    =  -1 \nNELM = 100 \nNELMIN = 6 \
    \nADDGRID = .FALSE. \nLMIXTAU = .TRUE. \nLASPH = .TRUE. \nLMAXMIX = 4 \
    \nLREAL   = .FALSE. \nLWAVE = FALSE \nLCHARG = FALSE \nLORBIT = 11'
    with open('INCAR','w+') as ofl:
        ofl.write(istr)
    return


if __name__=='__main__':

    wd = {'nc':1, 'kp':1}
    for tmp in sys.argv[2:]:
        if '=' in tmp:
            tmp2 = tmp.split('=')
            if tmp2[0] == 'kp':
                wd['kp'] = int(tmp2[1])
            elif tmp2[0] == 'nc':
                wd['nc'] = int(tmp2[1])

    if sys.argv[1] == 'wincar':
        wincar(wd['kp'],wd['nc'])
    elif sys.argv[1] == 'append':
        system("grep 'Total CPU time used' OUTCAR > tmp.txt")
        with open('tmp.txt','r') as infl:
            for iln,ln in enumerate(infl):
                rtime = ln.strip().split()[-1]
                break
        system("rm tmp.txt")
        if path.isfile('./vasp_runs.csv'):
            char = 'a'
            wheader=False
        else:
            char = 'w+'
            wheader=True
        tfile = open('./vasp_runs.csv',char)
        if wheader:
            tfile.write('KPAR, NCORE, RTIME (s)\n')
        tfile.write('{:},{:},{:}\n'.format(wd['kp'],wd['nc'],rtime))
        tfile.close()
