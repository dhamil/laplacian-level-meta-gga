#!/bin/bash
#PBS -l walltime=2:00:00
#PBS -q normal
#PBS -l nodes=1:ppn=20
#PBS -N Ba_llmgga
#PBS -j oe
#PBS -o oe.txt
#PBS -m a
#PBS -M kaplan@temple.edu
#
module --force purge ; module load intel-libs

cd "$PBS_O_WORKDIR"
rm -f CHGCAR WAVECAR


time mpirun --mca btl '^openib' -np $PBS_NP ~/vasp6/bin/vasp_std


rm -f CHGCAR WAVECAR
