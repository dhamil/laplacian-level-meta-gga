#!/bin/bash
#PBS -l walltime=2:00:00
#PBS -q normal
#PBS -l nodes=1:ppn=20
#PBS -N Ba_llmgga
#PBS -j oe
#PBS -o oe.txt
#PBS -m abe
#PBS -M kaplan@temple.edu
#
module --force purge ; module load intel-libs

cd "$PBS_O_WORKDIR"
rm -f CHGCAR WAVECAR

for nc in 1 2 4 10 20 ; do
echo $nc

time mpirun --mca btl '^openib' -np $PBS_NP ~/vasp6/bin/vasp_std

done

rm -f CHGCAR WAVECAR
