#!/bin/bash
#PBS -l walltime=6:00:00
#PBS -q normal
#PBS -l nodes=1:ppn=20
#PBS -N test_vasp_timing
#PBS -j oe
#PBS -o oe.txt
#PBS -m abe
#PBS -M kaplan@temple.edu
#
module --force purge ; module load intel-libs

cd "$PBS_O_WORKDIR"

rm -rf outcars
rm -f vasp_runs.csv
mkdir outcars

kp=1
for nc in 2 4 5 10 20 ; do
  for i in 1 2 3 ; do
    python3 speed_test.py wincar kp=$kp nc=$nc
    mpirun --mca btl '^openib' -np $PBS_NP ~/vasp6/bin/vasp_std
    python3 speed_test.py append kp=$kp nc=$nc
    mv OUTCAR "outcars/out_kp=$kp""_nc=$nc""_$i.txt"
  done
done

kp=2
for nc in 2 5 10 ; do
  for i in 1 2 3 ; do
    python3 speed_test.py wincar kp=$kp nc=$nc
    mpirun --mca btl '^openib' -np $PBS_NP ~/vasp6/bin/vasp_std
    python3 speed_test.py append kp=$kp nc=$nc
    mv OUTCAR "outcars/out_kp=$kp""_nc=$nc""_$i.txt"
  done
done

kp=4
for nc in 5 ; do
  for i in 1 2 3 ; do
    python3 speed_test.py wincar kp=$kp nc=$nc
    mpirun --mca btl '^openib' -np $PBS_NP ~/vasp6/bin/vasp_std
    python3 speed_test.py append kp=$kp nc=$nc
    mv OUTCAR "outcars/out_kp=$kp""_nc=$nc""_$i.txt"
  done
done

kp=5
for nc in 2 4 ; do
  for i in 1 2 3 ; do
    python3 speed_test.py wincar kp=$kp nc=$nc
    mpirun --mca btl '^openib' -np $PBS_NP ~/vasp6/bin/vasp_std
    python3 speed_test.py append kp=$kp nc=$nc
    mv OUTCAR "outcars/out_kp=$kp""_nc=$nc""_$i.txt"
  done
done

kp=10
for nc in 2 ; do
  for i in 1 2 3 ; do
    python3 speed_test.py wincar kp=$kp nc=$nc
    mpirun --mca btl '^openib' -np $PBS_NP ~/vasp6/bin/vasp_std
    python3 speed_test.py append kp=$kp nc=$nc
    mv OUTCAR "outcars/out_kp=$kp""_nc=$nc""_$i.txt"
  done
done
