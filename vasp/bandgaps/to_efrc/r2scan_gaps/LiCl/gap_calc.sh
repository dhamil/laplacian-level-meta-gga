#!/bin/bash
#PBS -l walltime=6:00:00
#PBS -q normal
#PBS -l nodes=1:ppn=20
#PBS -N gap_calc
#PBS -j oe
#PBS -o oe.txt
#PBS -m a
#PBS -M kaplan@temple.edu

module --force purge ; module load intel-libs
cd "$PBS_O_WORKDIR"

vasp_exec=/home/tuf53878/vasp6/bin/vasp_std

mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec

python3 ~/llmgga_testing/bandgaps/bandgaps_helper.py incar nedos=10000

mv INCAR INCAR_init
mv OSZICAR OSZ_init
mv OUTCAR OUT_init
mv DOSCAR DOS_init
mv EIGENVAL EIG_init

mv INCAR_gap INCAR
mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec
mv INCAR INCAR_gap
mv INCAR_init INCAR

python3 ~/llmgga_testing/bandgaps/bandgaps_helper.py gap
