from os import sys
from os import system

nedos_default = 6001
deps_default = 1.e-3

def get_ef(flnm='DOSCAR'):

    dosfl = open(flnm,'r')
    for iln,ln in enumerate(dosfl):
        if iln == 5:
            ef = float(ln.split()[3])
            break
    dosfl.close()

    return ef

def write_gap_incar(opts={}):

    if 'deps' in opts:
        deps = opts['deps']
    else:
        deps = deps_default
    if 'nedos' in opts:
        nedos = opts['nedos']
    else:
        nedos = nedos_default
    if 'file' in opts:
        flnm = opts['file']
    else:
        flnm = 'DOSCAR'
    ndosh = (nedos-nedos%2)//2

    ef = get_ef(flnm=flnm)

    incar_str = 'NCORE     =  4\nKPAR      =  1\nALGO      =  Normal\n'
    incar_str += 'PREC      =  Accurate\nEDIFF     =  1E-06\nENCUT     =  800\n'
    incar_str += 'ISMEAR    =  -5\nKSPACING  =  0.08\nKGAMMA    = .TRUE.\nGGA = PE\n'
    incar_str += 'NSW       =  0\nIBRION    =  -1\n'

    incar_str += '\nEMIN    = {:}\nEMAX    = {:}\nNEDOS = {:}\n'.format(ef-deps*ndosh,
        ef+deps*ndosh,nedos)

    incar_str += '\nNELM = 200 \nNELMIN = 6 \n'
    incar_str += '\nADDGRID = .FALSE. \nMETAGGA = OFR2 \nLMIXTAU = .TRUE. \n'
    incar_str += 'LASPH = .TRUE. \nLMAXMIX = 4 \nLREAL   = .FALSE.\n'

    incar_str += '\nLWAVE = FALSE \nLCHARG = FALSE \nLORBIT = 11'

    with open('INCAR_gap','w+') as tmpfl:
        tmpfl.write(incar_str)
    return

def get_gap(flnm='DOSCAR'):

    cnt = 0
    with open(flnm,'r') as tmpfl:

        for iln,ln in enumerate(tmpfl):
            if iln < 5:
                continue
            elif iln == 5:
                tmp = ln.split()
                nedos = int(tmp[2])
                ef = float(tmp[3])
            else:

                if len(ln.split()) == 5 and cnt > 0:
                    break
                if cnt == nedos:
                    break

                tmp = [float(x) for x in (ln.strip()).split()]
                if (tmp[0] <= ef + 1.e-6) and (tmp[1] > 0.0):
                    vbm = tmp[0]
                elif (tmp[0] > ef + 1.e-6) and (tmp[1] > 0.0):
                    cbm = tmp[0]
                    break
                cnt += 1
    print('------------------------------')
    print('GAP = {:} eV'.format(cbm-vbm))
    print('VBM = {:} eV '.format(vbm))
    print('CBM = {:} eV'.format(cbm))
    print('------------------------------')
    return

def poscar_writer(optd):
    opts = ['symm','a','b','c','coa','name','file']
    wd = {'name':'Unknown','file':'POSCAR'}
    for anopt in optd:
        if anopt in opts:
            wd[anopt] = optd[anopt]

    ostr = wd['name']+'\n'
    if wd['symm'] == 'sc':
        ostr += '    {:}\n'.format(wd['a'])
        ostr += '  1.0 0.0 0.0\n'+'  0.0 1.0 0.0\n'+'  0.0 0.0 1.0\n'
        ostr += ' 1\n'+' d\n'+ '0.0 0.0 0.0'
    elif wd['symm'] in ['fcc','ds','rs','zb']:
        ostr += '    {:}\n'.format(wd['a'])
        ostr += '  0.0 0.5 0.5\n'+'  0.5 0.0 0.5\n'+'  0.5 0.5 0.0\n'
        if wd['symm'] == 'fcc':
            ostr += ' 1\n'+' d\n'+ '0.0 0.0 0.0'
        elif wd['symm'] == 'ds':
            ostr += ' 2\n'+' d\n'+ '-0.125 -0.125 -0.125\n'+ ' 0.125  0.125  0.125\n'
        elif wd['symm'] == 'rs':
            ostr += ' 1 1\n'+' d\n' + '0.0 0.0 0.0\n'+'0.5 0.5 0.5\n'
        elif wd['symm'] == 'zb':
            ostr += ' 1 1\n'+' d\n'+ '0.0 0.0 0.0\n'+ ' 0.25  0.25  0.25\n'
    elif wd['symm'] == 'bcc':
        ostr += '    {:}\n'.format(wd['a'])
        ostr += '  -0.5  0.5  0.5\n'+'   0.5 -0.5  0.5\n'+'   0.5  0.5 -0.5\n'
        ostr += ' 1\n'+' d\n'+ '0.0 0.0 0.0'
    elif wd['symm'] == 'hcp':
        if 'c' in wd and 'coa' not in wd:
            wd['coa'] = float(wd['c'])/float(wd['a'])
        else:
            wd['coa']=float(wd['coa'])
        ostr += '    {:}\n'.format(wd['a'])
        ostr += '1.0000000000000000  0.0000000000000000  0.0000000000000000\n'
        ostr += '0.5000000000000000  0.8660254037844386  0.0000000000000000\n'
        ostr += '0.0000000000000000  0.0000000000000000  {:.16f}\n'.format(wd['coa'])
        ostr += '2\n' + ' d\n'
        ostr += '0.0000000000000000  0.0000000000000000  0.0000000000000000\n'
        ostr += '0.3333333333333333  0.3333333333333333  0.5000000000000000\n'
    ofl = open(wd['file'],'w+')
    ofl.write(ostr)
    ofl.close()
    return

def make_poscars():
    lc20_res_dir = '/Users/aaronkaplan/Dropbox/phd.nosync/lap_lev_mgga/vasp/lc20/data/'
    poscar_w_exec = '~/Desktop/vuk/poscar_writer.py'
    to_do = ['C','GaAs','Ge','LiCl','LiF','MgO','Si','SiC']
    fnls = ['r2scan','r2scanl','ofr2']
    for fnl in fnls:
        wfile = lc20_res_dir+'lc_20_results_{:}.csv'.format(fnl)
        system('mkdir ./{:}_gaps'.format(fnl))
        for sol in to_do:
            system('cp -r ./{:} ./{:}_gaps/'.format(sol,fnl))
        tmpfl = open(wfile,'r')
        for iln,ln in enumerate(tmpfl):
            if iln == 0:
                continue
            if ln.strip() == '====':
                break
            tmp = [x for x in (ln.strip()).split(',')]
            a0 = float(tmp[1])
            tmp2 = tmp[0].split('(')
            sol = tmp2[0][:-1]
            struc = tmp2[1][:-1]
            if sol in to_do:
                wfl = './{:}_gaps/{:}/POSCAR'.format(fnl,sol)
                poscar_writer({'name':sol,'file':wfl,'symm':struc,'a':a0})

    return

if __name__ == "__main__":

    tmp = sys.argv
    if len(tmp) < 2:
        print('No option specified!')
    else:
        if tmp[1] == 'incar':
            optd = {}
            if len(tmp) > 2:
                opts = tmp[2:]
                for anopt in opts:
                    topt = anopt.split('=')
                    if topt[0] == 'nedos':
                        optd['nedos'] = int(topt[1])
                    elif topt[0] == 'deps':
                        optd['deps'] = float(topt[1])
                    elif topt[0] == 'file':
                        optd['file'] = topt[1]
            write_gap_incar(opts=optd)

        elif tmp[1] == 'gap':
            get_gap()
        elif tmp[1] == 'setup':
            make_poscars()
