To patch OFR2, SCAN-L, rSCAN-L, and r2SCAN-L into a VASP 6.1.2 METAGGA.F file, run\
*patch < metagga_ofr2_no_st.diff*\
for the version without the stress tensor (will be slightly faster), or \
*patch < metagga_ofr2_with_st.diff*\
for the version with the Laplacian-level stress tensor.
