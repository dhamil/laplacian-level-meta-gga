#!/bin/bash
#PBS -l walltime=2:00:00
#PBS -q normal
#PBS -l nodes=2:ppn=20
#PBS -N ofr2_ae6
#PBS -j oe
#PBS -o oe.txt
#PBS -m a
#PBS -M kaplan@temple.edu

module --force purge ; module load intel-libs
cd "$PBS_O_WORKDIR"

vasp_exec=/home/tuf53878/vasp6/bin/vasp_std

mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec
