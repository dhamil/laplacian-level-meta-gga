from os import sys,system
from math import ceil

ats = ['C','H','O','S','Si']
"""
    LT03 / Original reference data from Table 1 of
        B.J. Lynch and D.G. Truhlar, J. Phys. Chem. Lett. A 107, 8996 (2003),
        doi: 10.1021/jp035287b,
        and correction, ibid. 108, 1460 (2004),
        doi: 10.1021/jp0379190

    HK12 reference values (in kJ/mol) are the frozen core values from Table 4 of:
        R. Haunschild and W. Klopper, Theor. Chem. Acc. 131, 1112 (2012), doi:10.1007/s00214-012-1112-3
"""
benchmarks = {
    'LT03': {'SiH4': 322.40, 'SiO': 192.08, 'S2': 101.67, 'C3H4': 704.79,
        'C2H2O2': 633.35, 'C4H8': 1149.01},
    'HK12': {'SiH4': 1358.1, 'SiO': 804.8, 'S2': 431.9, 'C3H4': 2934.9,
        'C2H2O2': 2646.2, 'C4H8': 4792.3}
}


"""
    geometries from Peverati, R.; Truhlar, D. G. J. Chem. Phys. 2011, 135, 191102.
    https://comp.chem.umn.edu/db/dbs/mgae109.html
"""
geom_file = {'SiH4': 'SiH4', 'SiO': 'SiO', 'S2': 'S2', 'C3H4': 'C3H4_pro',
    'C2H2O2': 'HCOCOH', 'C4H8': 'C4H8_cyc'}

kcal_to_kJ = 4.184
eV_to_kcalmol = 23.06054783061903

# conversion to kcal/mol, which I guess is more standard for chemistry
for asys in benchmarks['HK12']:
    benchmarks['HK12'][asys] /= kcal_to_kJ


def job_sub_script(dfa,sys,dir='./'):
    str = '#!/bin/bash\n'
    str += '#PBS -l walltime=1:00:00:00\n'
    str += '#PBS -q normal\n'
    str += '#PBS -l nodes=1:ppn=20\n'
    str += '#PBS -N {:}_{:}_ae6\n'.format(dfa,sys)
    str += '#PBS -j oe\n'
    str += '#PBS -o oe.txt\n'
    str += '#PBS -m a\n'
    str += '#PBS -M kaplan@temple.edu\n'
    str += 'module --force purge ; module load intel-libs\n'
    str += 'cd "$PBS_O_WORKDIR"\n'
    str += 'vasp_exec=/home/tuf53878/vasp6/bin/vasp_gam\n'
    str += "mpirun --mca btl '^openib' -np $PBS_NP $vasp_exec"

    with open(dir+'runcalc.sh','w+') as tfl:
        tfl.write(str)
    return

def incar_script(nelec,nion,dir='./',opts={}):

    tagd = {'SYSTEM': 'unknown', 'NCORE': 20, 'KPAR': 1,
    'ALGO': 'A', 'LSUBROT': False, 'PREC': 'Accurate', #'NBANDS': ceil(3/2*nelec),
    'EDIFF': 1.e-5, 'ENCUT': 1000, 'ISMEAR': 0, 'SIGMA': 0.1,
    'ISPIN': 2, 'MAGMOM': '{:}*1.0'.format(nion), #'LDIPOL': True, 'IDIPOL': 4,
    'GGA': 'PE', 'NSW': 0, 'IBRION': -1, 'NELM': 500, 'NELMIN': 6,
    'ADDGRID': False, 'LMIXTAU': True, 'LASPH': True, 'LMAXMIX': 4, 'LREAL': False,
    'AMIX': 0.1, 'BMIX': 0.0001, 'AMIX_MAG': 0.4, 'BMIX_MAG': 0.0001,
    'LWAVE': False, 'LCHARG': False, 'LVTOT': False, 'LORBIT': 11
    }
    for akey in opts:
        if akey == 'DFA':
            continue
        tagd[akey] = opts[akey]

    npad = 10
    str = ''
    for akey in tagd:
        padstr = ' '*(npad - len(akey))
        if akey in ['EDIFF','EDIFFG']:
            str += '{:}{:}= '.format(akey,padstr) + ('{:} \n'.format(tagd[akey])).upper()
        elif type(tagd[akey]) is bool:
            str += '{:}{:}= .{:}. \n'.format(akey,padstr,tagd[akey])
        else:
            str += '{:}{:}= {:} \n'.format(akey,padstr,tagd[akey])

    tmpfl = open(dir+'INCAR','w+')
    tmpfl.write(str)
    tmpfl.close()
    return


def molecule_parser(mol):

    atoms = {}
    tstr = ''
    nstr = ''
    nchar = len(mol)-1
    last_was_num = False
    for ichar,achar in enumerate(mol):
        if ichar > 0:
            if achar.isupper() and not last_was_num:
                atoms[tstr] = 1
                tstr = ''
            elif achar.isupper() and last_was_num:
                atoms[tstr] = int(float(nstr))
                nstr = ''
                tstr = ''
                last_was_num = False
            elif achar.isnumeric():
                last_was_num = True
                nstr += achar

        if not achar.isnumeric():
            tstr += achar
    if achar.isnumeric():
        atoms[tstr] = int(float(nstr))
    else:
        atoms[tstr] = 1

    return atoms

def setup_test(opts):

    box_dims = [10.0,10.1,10.2]
    box_center = [x/2 for x in box_dims]

    cdir = './AE6_{:}/'.format(opts['DFA'])

    system('rm -rf {:}'.format(cdir))
    system('mkdir {:}'.format(cdir))

    jscript = 'from os import system\n'
    jscript += "for sys in ['C','H','O','S','Si','SiH4','SiO','S2','C3H4','C2H2O2','C4H8']:\n"
    jscript += "  system('cd {:} ; qsub runcalc.sh'.format(sys))"
    with open(cdir+'runall.py','w+') as tfl:
        tfl.write(jscript)

    iopts = {}
    if opts['DFA'] in ['PE','PS']:
        iopts['GGA'] = opts['DFA']
    elif opts['DFA'] in ['SCAN','R2SCAN','R2SL','OFR2']:
        iopts['GGA'] = 'PE'
        iopts['METAGGA'] = opts['DFA']

    nelec_d = {}
    for atom in ats:
        with open('./pseudopots/POTCAR_{:}'.format(atom),'r') as tfl:
            for iln, ln in enumerate(tfl):
                if iln == 1:
                    nelec_d[atom] = int(float(ln.strip()))
                    break

        wdir = cdir + atom +'/'
        system('mkdir -p {:}'.format(wdir))

        job_sub_script(opts['DFA'],atom,dir=wdir)

        kpoints_str = 'G only\n1\nC\n0 0 0 1'
        with open(wdir+'KPOINTS','w+') as tfl:
            tfl.write(kpoints_str)

        iopts['SYSTEM'] = atom
        incar_script(nelec_d[atom],1,dir=wdir,opts=iopts)

        system('cp ./pseudopots/POTCAR_{:} {:}POTCAR'.format(atom,wdir))

        poscar_str = '{:}\n 1.0\n'.format(atom)
        poscar_str += '{:}  0.0  0.0\n'.format(box_dims[0])
        poscar_str += '0.0  {:}  0.0\n'.format(box_dims[1])
        poscar_str += '0.0  0.0  {:}\n'.format(box_dims[2])
        poscar_str += '  {:}\n  1\n cart\n'.format(atom)
        poscar_str += ('{:}  '*3).format(*box_center)

        with open(wdir+'POSCAR','w+') as tfl:
            tfl.write(poscar_str)

    for mol in geom_file:

        wdir = cdir + mol +'/'
        system('mkdir -p {:}'.format(wdir))

        job_sub_script(opts['DFA'],mol,dir=wdir)
        kpoints_str = 'G only\n1\nC\n0 0 0 1'
        with open(wdir+'KPOINTS','w+') as tfl:
            tfl.write(kpoints_str)

        cons = molecule_parser(mol)
        mol_nelec = 0
        nion = 0
        geo_d = {}
        for atom in cons:
            nion += cons[atom]
            mol_nelec += nelec_d[atom]*cons[atom]
            geo_d[atom] = []

        iopts['SYSTEM'] = mol
        incar_script(mol_nelec,nion,dir=wdir,opts=iopts)

        with open('./ref_geom/{:}.xyz'.format(geom_file[mol]),'r') as tfl:
            for iln,ln in enumerate(tfl):
                if iln < 2:
                    continue
                tmpv = (ln.strip()).split()
                geo_d[tmpv[0]].append([float(x) for x in tmpv[1:]])

        poscar_str = '{:}\n 1.0\n'.format(mol)
        poscar_str += '{:}  0.0  0.0\n'.format(box_dims[0])
        poscar_str += '0.0  {:}  0.0\n'.format(box_dims[1])
        poscar_str += '0.0  0.0  {:}\n  '.format(box_dims[2])
        for atom in cons:
            poscar_str += '{:} '.format(atom)
        poscar_str += '\n  '
        for atom in cons:
            poscar_str += '{:} '.format(cons[atom])
        poscar_str += '\n cart\n'

        potcar_cmnd = 'cat '
        for atom in cons:
            potcar_cmnd += './pseudopots/POTCAR_{:} '.format(atom)
            for atom_pos in geo_d[atom]:
                catom_pos = [box_center[ix] + atom_pos[ix] for ix in range(3)]
                poscar_str += ('{:.5f}  '*3 +'\n').format(*catom_pos)

        potcar_cmnd += '> {:}POTCAR'.format(wdir)
        system(potcar_cmnd)

        with open(wdir+'POSCAR','w+') as tfl:
            tfl.write(poscar_str)

    return


def conv_en(flnm,all_stage=False):

    pars = {}
    # F is Helmholtz free energy, F = E - TS, where finite T (and S) comes from smearing
    # E is energy extrapolated without smearing, E = F + TS
    # dE is change in energy between penultimate and ultimate runs
    # mag is the magnetiziation within the calculation cell

    its = 0
    with open(flnm,'r') as infl:
        for iln,ln in enumerate(infl):
            ln = ln.split()
            if all_stage:
                indx = ln[0]
            else:
                indx = flnm
            if ln[1] == 'F=':
                its = its + int(oln[1])
                if len((ln[7][1:]).split('E'))==1:
                    dE_temp = 0.0
                else:
                    dE_temp=float(ln[7][1:])
                if len(ln) == 10:
                    # ISPIN = 2
                    pars[indx]={'F': float(ln[2]), 'E': float(ln[4]), 'dE': dE_temp, 'mag': float(ln[9]), 'its': its}
                else:
                    # ISPIN = 1, spin unpolarized
                    pars[indx]={'F': float(ln[2]), 'E': float(ln[4]), 'dE': dE_temp, 'mag': 0.0, 'its': its}
            oln = ln
    if all_stage:
        return pars
    else:
        return pars[indx]


def run_ae6_analysis(fnl):

    tdir = 'AE6_{:}'.format(fnl)
    mol_ens = {}
    at_ens = {}
    ae_en = {}
    errs = {}
    me = {ref: 0.0 for ref in benchmarks}
    mae = {ref: 0.0 for ref in benchmarks}

    nmol = len(benchmarks['LT03'])

    logfile = tdir+'/AE6_{:}.csv'.format(fnl)
    wfl = open(logfile,'w+')

    wfl.write('Atom, Energy (kcal/mol), , \n')
    for atom in ats:
        osz = conv_en(tdir+'/'+atom+'/OSZICAR')
        at_ens[atom] = osz['E']*eV_to_kcalmol
        wfl.write('{:}, {:}, , \n'.format(atom,at_ens[atom]))

    wfl.write('====\n')
    tstr = 'Molecule, Energy (kcal/mol), AE (kcal/mol)'
    for ref in benchmarks:
        tstr += ', AE rel. err {:} (kcal/mol)'.format(ref)
    wfl.write(tstr +'\n')
    for mol in benchmarks['LT03']:

        osz = conv_en(tdir+'/'+mol+'/OSZICAR')
        mol_ens[mol] = osz['E']*eV_to_kcalmol
        ae_en[mol] = -mol_ens[mol]

        cons_ats = molecule_parser(mol)
        for anat in cons_ats:
            ae_en[mol] += cons_ats[anat]*at_ens[anat]

        tstr = '{:}, {:}, {:}'.format(mol,mol_ens[mol],ae_en[mol])
        for ref in benchmarks:
            tmperr = ae_en[mol] - benchmarks[ref][mol]
            #errs[mol] = tmperr
            me[ref] += tmperr/nmol
            mae[ref] += abs(tmperr)/nmol
            tstr += ', {:}'.format(tmperr)
        wfl.write(tstr + '\n')

    wfl.write('====\n')
    tstr1 = 'ME (kcal/mol), , '
    tstr2 = 'MAE (kcal/mol), , '
    for ref in benchmarks:
        tstr1 += ', {:}'.format(me[ref])
        tstr2 += ', {:}'.format(mae[ref])
    wfl.write(tstr1 + '\n' + tstr2 + '\n')

    wfl.close()

    return

if __name__ == "__main__":

    if sys.argv[1] == 'setup':
        opts= {'DFA':'PE'}
        for opts['DFA'] in ['R2SCAN','R2SL','OFR2']:
            setup_test(opts)
    elif sys.argv[1] == 'analysis':
        run_ae6_analysis(sys.argv[2])
