from ae6_analysis import benchmarks

sys_order = ['SiH4','SiO','S2','C3H4','C2H2O2','C4H8']
nsys = len(sys_order)
fnls = ['PE','PS','SCAN','R2SCAN','R2SL','OFR2']

aed = {}
for fnl in fnls:
    aed[fnl] = {}
    with open('./AE6_{:}/AE6_{:}.csv'.format(fnl,fnl),'r') as infl:
        for iln,ln in enumerate(infl):
            if iln < 8 or iln == 14:
                continue

            tmp = (ln.strip()).split(',')
            if iln < 14:
                aed[fnl][tmp[0]] = float(tmp[2])
            elif iln == 15:
                aed[fnl]['ME LT03'] = float(tmp[3])
                aed[fnl]['ME HK12'] = float(tmp[4])
            elif iln == 16:
                aed[fnl]['MAE LT03'] = float(tmp[3])
                aed[fnl]['MAE HK12'] = float(tmp[4])

outfl = './ae6_summary.csv'
outfl_tex = './ae6_summary.tex'
str = 'Molecule, '
strt = 'Molecule & '
for fnl in fnls:
    str += fnl+', '
    strt += fnl+' & '
str += '\n'
strt += '\n'

for mol in sys_order:
    tmp = []
    for fnl in fnls:
        tmp.append(aed[fnl][mol])
    str += ('{:}, ' + '{:}, '*(len(fnls)-1) + '{:} \n').format(mol,*tmp)
    strt += ('{:} & ' + '{:.2f} & '*(len(fnls)-1) + '{:.2f} \\\\ \n').format(mol,*tmp)

for mol in ['ME LT03','MAE LT03','ME HK12','MAE HK12']:
    tmp = []
    for fnl in fnls:
        tmp.append(aed[fnl][mol])
    str += ('{:}, '+ '{:}, '*(len(fnls)-1) + '{:} \n').format(mol,*tmp)
    strt += ('{:} & '+ '{:.2f} & '*(len(fnls)-1) + '{:.2f} \\\\ \n').format(mol,*tmp)

fl1 = open(outfl,'w+')
fl1.write(str)
fl1.close()

fl1 = open(outfl_tex,'w+')
fl1.write(strt)
fl1.close()
