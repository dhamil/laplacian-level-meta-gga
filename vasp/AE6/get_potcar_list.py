
ats = ['C','H','O','S','Si']

if __name__=="__main__":

    str = 'Element, Pseudopot., Valence \n'
    for elt in ats:
        with open('./pseudopots/POTCAR_{:}'.format(elt),'r') as tmpfl:
            for iln,ln in enumerate(tmpfl):
                if iln == 0:
                    pp_name = ln.strip()
                elif iln == 1:
                    val = int(float(ln.strip()))
                    break
            str += '{:}, {:}, {:}\n'.format(elt,pp_name,val)
    with open('AE6_POTCAR_list.csv','w+') as tmpfl:
        tmpfl.write(str)
