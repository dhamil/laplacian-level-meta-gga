from ae6_analysis import benchmarks

refset='HK12'
sys_order = ['SiH4','SiO','S2','C3H4','C2H2O2','C4H8']
nsys = len(sys_order)
fnls = ['PE','PS','R2SCAN','R2SL','OFR2']

aed = {}
for fnl in fnls:
    aed[fnl] = {}
    with open('./AE6_{:}/AE6_{:}.csv'.format(fnl,fnl),'r') as infl:
        for iln,ln in enumerate(infl):
            if iln < 8:
                continue
            elif iln >= 14:
                break
            tmp = (ln.strip()).split(',')
            aed[fnl][tmp[0]] = float(tmp[2]) - benchmarks[refset][tmp[0]]
    aed[fnl]['ME'] = 0.0
    aed[fnl]['MAE'] = 0.0
    for mol in sys_order:
        aed[fnl]['ME'] += aed[fnl][mol]
        aed[fnl]['MAE'] += abs(aed[fnl][mol])
    aed[fnl]['ME'] /= nsys
    aed[fnl]['MAE'] /= nsys

outfl = './ae6_summary.csv'
outfl_tex = './ae6_summary.tex'
str = 'Molecule, Reference, '
strt = 'Molecule & Reference & '
for fnl in fnls:
    str += fnl+', '
    strt += fnl+' & '
str += '\n'
strt += '\n'

for mol in sys_order:
    tmp = []
    for fnl in fnls:
        tmp.append(aed[fnl][mol])
    str += ('{:}, ' + '{:}, '*len(fnls) + '{:} \n').format(mol,benchmarks[refset][mol],*tmp)
    strt += ('{:} & ' + '{:.3f} & '*len(fnls) + '{:.3f} \\\\ \n').format(mol,benchmarks[refset][mol],*tmp)

for mol in ['ME','MAE']:
    tmp = []
    for fnl in fnls:
        tmp.append(aed[fnl][mol])
    str += ('{:}, ,'+ '{:}, '*(len(fnls)-1) + '{:} \n').format(mol,*tmp)
    strt += ('{:} & & '+ '{:.3f} & '*(len(fnls)-1) + '{:.3f} \\\\ \n').format(mol,*tmp)

fl1 = open(outfl,'w+')
fl1.write(str)
fl1.close()

fl1 = open(outfl_tex,'w+')
fl1.write(strt)
fl1.close()
