
program testtask

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), parameter :: pi = 3.14159265358979323846264338327950288419_dp

  real(dp), parameter :: al(8) = (/ 0._dp, 0.5_dp, 1._dp, 1.5_dp, 2._dp, 2.5_dp,&
  &     3._dp, 1.d10 /)
  real(dp), parameter :: smin = 1.d-1, smax = 1.d2, rmin = 1.d-6, rmax = 5.d2
  real(dp), parameter :: nmin = 1.d-14
  integer, parameter :: ns = 10**3, nr = 10**5

  integer :: ia, js
  real(dp) :: fx,dfxdp,dfxda, sl(ns), ds, r(nr), rho(nr), wg(nr), n, agn, tau
  real(dp) :: ex,ec, tmp(6), int_ex, int_ec, rstep, nelec

  do js = 0,ns-1
    sl(1+js) = log(smin) + js*(log(smax)-log(smin))/(ns - 1._dp)
  end do
  sl = exp(sl)

  open(unit=1,file='./task_fx.csv')
  write(1,*) 's, alpha, Fx'
  do ia = 1, size(al)
    do js = 1,ns
      call task_fx(sl(js)**2,al(ia),fx,dfxdp,dfxda)
      write(1,*) sl(js), al(ia), fx
    end do
  end do
  close(1)

  rstep = (log(rmax)-log(rmin))/(nr - 1._dp)
  do js = 0,nr-1
    rho(1+js) = log(rmin) + js*rstep
  end do
  r = exp(rho)
  wg = 4*pi*r**3*rstep

  int_ex = 0._dp
  int_ec = 0._dp
  nelec = 0._dp
  do js = 1,nr
    n = exp(-2*r(js))/pi
    nelec = nelec + n*wg(js)
    agn = 2*n
    tau = agn**2/(8._dp*n)

    n = max(nmin,n)
    agn = max(nmin,agn)
    call task_x(n,0._dp,agn,0._dp,tau,0._dp,ex,tmp(1),tmp(2),tmp(3),&
      &        tmp(4),tmp(5),tmp(6))
    int_ex = int_ex + wg(js)*ex
    call ec_pw92_wrap(n,0._dp,ec,tmp(1),tmp(2))
    int_ec = int_ec + wg(js)*ec
  end do
  print*,nelec,int_ex,int_ec

end program testtask
