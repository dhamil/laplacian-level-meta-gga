import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm

dat = np.genfromtxt('./task_fx.csv')
ns = 1000
na = 8

colors = [cm.get_cmap('viridis')(i/na) for i in range(na)]
for i in range(na):
    plt.plot(dat[1+i*ns:(i+1)*ns,0],dat[1+i*ns:(i+1)*ns,2],color=colors[i], \
        label='$\\alpha={:}$'.format(dat[i*ns+1,1]))
    plt.annotate('$\\alpha={:}$'.format(dat[i*ns+1,1]),\
        (dat[1+i*ns,0]+.001,1.01*dat[1+i*ns,2]),color=colors[i])
#plt.legend(fontsize=12)
plt.xscale('log')
plt.xlim(dat[1,0],dat[ns,0])
plt.xlabel('$s$',fontsize=12)
plt.ylabel('$F_\\mathrm{x}^\\mathrm{TASK}(s,\\alpha)$',fontsize=12)
plt.show()
