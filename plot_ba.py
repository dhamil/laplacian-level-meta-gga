from os import path,system
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)

from atomic_densities.orbs import write_dens


wdir = './alpha_bar_plots/'
if not path.isdir(wdir):
    system('mkdir '+wdir)

atlist = ['Ne','Ar','Kr','Xe']
flist = [wdir+at+'_K99.csv' for at in atlist]

to_do = []
for ifl,fl in enumerate(flist):
    if not path.isfile(fl):
        to_do.append(atlist[ifl])

if len(to_do) > 0:
    wflist = [wdir+at+'_K99.csv' for at in to_do]
    write_dens(atlist,'K99',do_all=False,save=True,rect_grid=True, anggr=True, \
        rmax = 100, filenames=wflist,nodmask=True)

fig, ax = plt.subplots(2,2,figsize=(10,8))

iax = 0
for iat in range(4):

    tdens = np.genfromtxt(flist[iat],delimiter=',',skip_header=1)
    r = tdens[:,0]
    n = tdens[:,2] + tdens[:,3]
    gn = np.abs(tdens[:,6])

    tauw = gn**2/(8*n)
    tau0 = 0.3*(3*np.pi**2*n)**(2/3)*n

    tau = tdens[:,7] + tdens[:,8]
    tmsk = tau < tauw
    tau[tmsk] = tauw[tmsk]

    alpha = (tau - tauw)/tau0
    ba001 = (tau - tauw)/(tau0 + 1.e-3*tauw)
    ba01 = (tau - tauw)/(tau0 + 1.e-2*tauw)

    if iat > 1:
        iax = 1
    jax = iat%2

    ax[iax,jax].plot(r,alpha,color='darkblue',label='$\\alpha$')
    ax[iax,jax].plot(r,ba001,color='darkorange',linestyle='--',\
        label='$\\overline{\\alpha}(\\eta=0.001)$')
    ax[iax,jax].plot(r,ba01,color='tab:green',linestyle='-.',\
        label='$\\overline{\\alpha}(\\eta=0.01)$')

    ax[iax,jax].set_xlim(r.min(),r.max())
    ax[iax,jax].set_xscale('log')
    ax[iax,jax].set_xlabel('$r$',fontsize=14)
    if atlist[iat] == 'Ne':
        lbd = 1.e-4
    else:
        lbd = 1.e-2
    ax[iax,jax].set_ylim(lbd,1.e2)
    ax[iax,jax].set_yscale('log')
    ax[iax,jax].annotate(atlist[iat],(0.05,0.85),fontsize=20,xycoords='axes fraction')

ax[0,0].legend(fontsize=14)
#plt.show()
plt.savefig(wdir+'/iso_orb_noble_gas.pdf',dpi=600,bbox_inches='tight')
