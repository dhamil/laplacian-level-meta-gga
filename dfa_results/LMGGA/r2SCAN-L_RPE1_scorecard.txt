Error breakdown for r2SCAN-L X with r2SCAN-L C

LLMG parameters:  c0 = 0,  c1 = 3,  c2 = 0,  c3 = 0,

  H atom Exc=-0.3074 Eh (-1.6194% error); Ex = -0.3047 Eh, Ec = -0.0028 Eh
  He atom Exc=-1.0466 Eh (-2.0034% error); Ex = -1.0025 Eh, Ec = -0.0441 Eh ; not used as a norm

---------------------
Rare gas atoms:
---------------------
  Ne:
      Ex=-11.9588 Eh, -1.2320% error
      Ec=-0.3746 Eh, -4.2048% error
      Exc=-12.3334 Eh, -1.3250% error
  Ar:
      Ex=-29.8386 Eh, -1.1575% error
      Ec=-0.7339 Eh, 1.1707% error
      Exc=-30.5725 Eh, -1.1029% error
  Kr:
      Ex=-93.0466 Eh, -0.8983% error
      Ec=-1.8532 Eh, 0.1500% error
      Exc=-94.8998 Eh, -0.8780% error
  Xe:
      Ex=-177.7309 Eh, -0.8198% error
      Ec=-3.0146 Eh, 0.4069% error
      Exc=-180.7455 Eh, -0.7996% error

XC MAPE 1.0264%
 X MAPE 1.0269%
 C MAPE 1.4831%

    Bx=-0.1837, err=-18.6708%
    Cx=0.1959, err=-23.2142%
    Bc=0.0370, err=-4.5722%
    Bxc=-0.1467, err=-21.5894%

---------------------
Jellium Surface:
---------------------
  rs=2:
      sigma_xc = 3273.8727 erg/cm^2 (-4.0764% error)
      sigma_x = 2292.2891 erg/cm^2 (-12.6414% error)
      sigma_c = 981.5835 erg/cm^2 (24.4086% error)
  rs=3:
      sigma_xc = 745.9802 erg/cm^2 (-4.4840% error)
      sigma_x = 394.6369 erg/cm^2 (-24.9740% error)
      sigma_c = 351.3433 erg/cm^2 (37.7817% error)
  rs=4:
      sigma_xc = 258.1541 erg/cm^2 (-3.6738% error)
      sigma_x = 89.0102 erg/cm^2 (-43.3056% error)
      sigma_c = 169.1439 erg/cm^2 (52.3819% error)
  rs=5:
      sigma_xc = 112.7992 erg/cm^2 (-0.1777% error)
      sigma_x = 16.7533 erg/cm^2 (-70.6083% error)
      sigma_c = 96.0459 erg/cm^2 (71.5106% error)

  XC MAPE 3.1030%
  X MAPE 37.8823%
  C MAPE 46.5207%

---------------------
Jellium cluster:
---------------------
  rs=2:
      sigma_xc = 3260.5899 erg/cm^2 (-4.4656% error)
  rs=3:
      sigma_xc = 745.6987 erg/cm^2 (-4.5200% error)
  rs=3.25:
      sigma_xc = 561.1361 erg/cm^2 (-3.6602% error)
  rs=4:
      sigma_xc = 259.0631 erg/cm^2 (-3.3347% error)
  rs=5:
      sigma_xc = 113.7775 erg/cm^2 (0.6880% error)

  XC MAPE 3.3337%

  rs=2:
      gamma = 1.7688 mhartree/bohr (147.3119% error)
  rs=3.25:
      gamma = 0.5915 mhartree/bohr (248.1472% error)
  rs=4:
      gamma = 0.3413 mhartree/bohr (617.4061% error)

  XC MAPE 337.6217%


NB: percent error defined as:
 100% * (approx/reference - 1)