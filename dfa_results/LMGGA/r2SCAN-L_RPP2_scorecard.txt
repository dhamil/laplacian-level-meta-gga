Error breakdown for r2SCAN-L X with r2SCAN-L C

LLMG parameters:  c0 = 0,  c1 = 3,  c2 = 0,  c3 = 0,

  H atom Exc=-0.3106 Eh (-0.6145% error); Ex = -0.3094 Eh, Ec = -0.0012 Eh
  He atom Exc=-1.0566 Eh (-1.0694% error); Ex = -1.0154 Eh, Ec = -0.0412 Eh ; not used as a norm

---------------------
Rare gas atoms:
---------------------
  Ne:
      Ex=-12.0259 Eh, -0.6782% error
      Ec=-0.3706 Eh, -5.2271% error
      Exc=-12.3964 Eh, -0.8205% error
  Ar:
      Ex=-29.9524 Eh, -0.7804% error
      Ec=-0.7285 Eh, 0.4332% error
      Exc=-30.6810 Eh, -0.7519% error
  Kr:
      Ex=-93.2481 Eh, -0.6836% error
      Ec=-1.8490 Eh, -0.0766% error
      Exc=-95.0971 Eh, -0.6719% error
  Xe:
      Ex=-178.0216 Eh, -0.6576% error
      Ec=-3.0099 Eh, 0.2492% error
      Exc=-181.0315 Eh, -0.6427% error

XC MAPE 0.7217%
 X MAPE 0.7000%
 C MAPE 1.4965%

    Bx=-0.1872, err=-17.0938%
    Cx=0.1890, err=-25.9228%
    Bc=0.0368, err=-4.8926%
    Bxc=-0.1504, err=-19.6196%

---------------------
Jellium Surface:
---------------------
  rs=2:
      sigma_xc = 3272.7483 erg/cm^2 (-4.1093% error)
      sigma_x = 2289.6814 erg/cm^2 (-12.7408% error)
      sigma_c = 983.0669 erg/cm^2 (24.5966% error)
  rs=3:
      sigma_xc = 745.3131 erg/cm^2 (-4.5694% error)
      sigma_x = 392.6076 erg/cm^2 (-25.3598% error)
      sigma_c = 352.7055 erg/cm^2 (38.3159% error)
  rs=4:
      sigma_xc = 257.7414 erg/cm^2 (-3.8278% error)
      sigma_x = 87.4007 erg/cm^2 (-44.3308% error)
      sigma_c = 170.3407 erg/cm^2 (53.4601% error)
  rs=5:
      sigma_xc = 112.5222 erg/cm^2 (-0.4228% error)
      sigma_x = 15.3816 erg/cm^2 (-73.0148% error)
      sigma_c = 97.1406 erg/cm^2 (73.4654% error)

  XC MAPE 3.2323%
  X MAPE 38.8615%
  C MAPE 47.4595%

---------------------
Jellium cluster:
---------------------
  rs=2:
      sigma_xc = 3273.9673 erg/cm^2 (-4.0736% error)
  rs=3:
      sigma_xc = 749.1532 erg/cm^2 (-4.0777% error)
  rs=3.25:
      sigma_xc = 563.4868 erg/cm^2 (-3.2567% error)
  rs=4:
      sigma_xc = 259.3620 erg/cm^2 (-3.2231% error)
  rs=5:
      sigma_xc = 113.0988 erg/cm^2 (0.0874% error)

  XC MAPE 2.9437%

  rs=2:
      gamma = 1.5899 mhartree/bohr (122.2941% error)
  rs=3.25:
      gamma = 0.5140 mhartree/bohr (202.5292% error)
  rs=4:
      gamma = 0.2964 mhartree/bohr (522.9851% error)

  XC MAPE 282.6028%


NB: percent error defined as:
 100% * (approx/reference - 1)