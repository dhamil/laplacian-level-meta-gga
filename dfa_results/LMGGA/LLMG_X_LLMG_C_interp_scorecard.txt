Error breakdown for LLMG X with LLMG C

LLMG parameters:  c0 = 0.170082,  c1 = 0.391628,  c2 = 0.0,  c3 = 0.0,

  H atom Exc=-0.2686 Eh (-14.0402% error); Ex = -0.2621 Eh, Ec = -0.0065 Eh
  He atom Exc=-0.8985 Eh (-15.8738% error); Ex = -0.8552 Eh, Ec = -0.0433 Eh ; not used as a norm

---------------------
Rare gas atoms:
---------------------
  Ne:
      Ex=-10.8279 Eh, -10.5726% error
      Ec=-0.3548 Eh, -9.2667% error
      Exc=-11.1826 Eh, -10.5317% error
  Ar:
      Ex=-27.5780 Eh, -8.6458% error
      Ec=-0.7130 Eh, -1.7052% error
      Exc=-28.2910 Eh, -8.4829% error
  Kr:
      Ex=-88.6509 Eh, -5.5801% error
      Ec=-1.7763 Eh, -4.0070% error
      Exc=-90.4271 Eh, -5.5497% error
  Xe:
      Ex=-171.3165 Eh, -4.3993% error
      Ec=-2.9304 Eh, -2.3971% error
      Exc=-174.2469 Eh, -4.3663% error

XC MAPE 7.2326%
 X MAPE 7.2994%
 C MAPE 4.3440%

    Bx=-0.0547, err=-75.7915%
    Cx=0.1701, err=-33.3156%
    Bc=0.0385, err=-0.6301%
    Bxc=-0.0162, err=-91.3510%

---------------------
Jellium Surface:
---------------------
  rs=2:
      sigma_xc = 3501.1988 erg/cm^2 (2.5842% error)
      sigma_x = 2726.9712 erg/cm^2 (3.9242% error)
      sigma_c = 774.2276 erg/cm^2 (-1.8723% error)
  rs=3:
      sigma_xc = 795.0514 erg/cm^2 (1.7992% error)
      sigma_x = 545.4539 erg/cm^2 (3.6985% error)
      sigma_c = 249.5975 erg/cm^2 (-2.1186% error)
  rs=4:
      sigma_xc = 271.3573 erg/cm^2 (1.2527% error)
      sigma_x = 161.0832 erg/cm^2 (2.6008% error)
      sigma_c = 110.2741 erg/cm^2 (-0.6539% error)
  rs=5:
      sigma_xc = 116.1579 erg/cm^2 (2.7946% error)
      sigma_x = 57.9294 erg/cm^2 (1.6306% error)
      sigma_c = 58.2284 erg/cm^2 (3.9794% error)

  XC MAPE 2.1077%
  X MAPE 2.9635%
  C MAPE 2.1561%

---------------------
Jellium cluster:
---------------------
  rs=2:
      sigma_xc = 3402.8374 erg/cm^2 (-0.2978% error)
  rs=3:
      sigma_xc = 765.9605 erg/cm^2 (-1.9257% error)
  rs=3.25:
      sigma_xc = 573.4955 erg/cm^2 (-1.5383% error)
  rs=4:
      sigma_xc = 260.5943 erg/cm^2 (-2.7633% error)
  rs=5:
      sigma_xc = 111.6400 erg/cm^2 (-1.2036% error)

  XC MAPE 1.5457%

  rs=2:
      gamma = 3.4992 mhartree/bohr (389.2664% error)
  rs=3.25:
      gamma = 1.3415 mhartree/bohr (689.6000% error)
  rs=4:
      gamma = 0.8564 mhartree/bohr (1700.1351% error)

  XC MAPE 926.3338%


NB: percent error defined as:
 100% * (approx/reference - 1)