Error breakdown for r2SCAN-L X with r2SCAN-L C

LLMG parameters:  c0 = 0.819411,  c1 = 0.201352,  c2 = 0.18502,  c3 = 1.53804,

  H atom Exc=-0.3120 Eh (-0.1507% error); Ex = -0.3116 Eh, Ec = -0.0004 Eh
  He atom Exc=-1.0655 Eh (-0.2366% error); Ex = -1.0268 Eh, Ec = -0.0387 Eh ; not used as a norm

------------------------------------------
Rare gas atoms (BBB93 dataset):
------------------------------------------
  Ne:
      Ex=-12.2522 Eh, 1.1913% error
      Ec=-0.3341 Eh, -14.5558% error
      Exc=-12.5863 Eh, 0.6987% error
  Ar:
      Ex=-30.5928 Eh, 1.3410% error
      Ec=-0.6457 Eh, -10.9929% error
      Exc=-31.2385 Eh, 1.0516% error
  Kr:
      Ex=-94.6026 Eh, 0.7590% error
      Ec=-1.7456 Eh, -5.6611% error
      Exc=-96.3482 Eh, 0.6349% error
  Xe:
      Ex=-180.3346 Eh, 0.6332% error
      Ec=-2.8535 Eh, -4.9579% error
      Exc=-183.1882 Eh, 0.5410% error

XC MAPE 0.7315%
 X MAPE 0.9811%
 C MAPE 9.0419%

    Bx=-0.2547, err=12.7772%
    Cx=0.2818, err=10.4567%
    Bc=0.0394, err=1.6336%
    Bxc=-0.2153, err=15.0841%

---------------------
Jellium Surface:
---------------------
  rs=2:
      sigma_xc = 3161.6416 erg/cm^2 (-7.3647% error)
      sigma_x = 2035.9602 erg/cm^2 (-22.4101% error)
      sigma_c = 1125.6814 erg/cm^2 (42.6719% error)
  rs=3:
      sigma_xc = 703.2407 erg/cm^2 (-9.9564% error)
      sigma_x = 264.6142 erg/cm^2 (-49.6931% error)
      sigma_c = 438.6265 erg/cm^2 (72.0104% error)
  rs=4:
      sigma_xc = 238.9627 erg/cm^2 (-10.8348% error)
      sigma_x = 15.2743 erg/cm^2 (-90.2711% error)
      sigma_c = 223.6883 erg/cm^2 (101.5210% error)
  rs=5:
      sigma_xc = 103.2164 erg/cm^2 (-8.6580% error)
      sigma_x = -27.9587 erg/cm^2 (-149.0503% error)
      sigma_c = 131.1751 erg/cm^2 (134.2412% error)

  XC MAPE 9.2035%
  X MAPE 77.8561%
  C MAPE 87.6111%

---------------------
Jellium cluster:
---------------------
  rs=2:
      sigma_xc = 3068.4489 erg/cm^2 (-10.0953% error)
  rs=3:
      sigma_xc = 682.3898 erg/cm^2 (-12.6261% error)
  rs=3.25:
      sigma_xc = 511.5223 erg/cm^2 (-12.1783% error)
  rs=4:
      sigma_xc = 233.6530 erg/cm^2 (-12.8160% error)
  rs=5:
      sigma_xc = 102.0727 erg/cm^2 (-9.6702% error)

  XC MAPE 11.4772%

  rs=2:
      gamma = 1.8590 mhartree/bohr (159.9223% error)
  rs=3.25:
      gamma = 0.7298 mhartree/bohr (329.5772% error)
  rs=4:
      gamma = 0.4455 mhartree/bohr (836.3986% error)

  XC MAPE 441.9661%


NB: percent error defined as:
 100% * (approx/reference - 1)