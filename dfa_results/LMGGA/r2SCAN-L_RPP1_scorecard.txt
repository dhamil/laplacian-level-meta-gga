Error breakdown for r2SCAN-L X with r2SCAN-L C

LLMG parameters:  c0 = 0.819411,  c1 = 0.201352,  c2 = 0.18502,  c3 = 1.53804,

  H atom Exc=-0.3074 Eh (-1.6256% error); Ex = -0.3043 Eh, Ec = -0.0031 Eh
  He atom Exc=-1.0415 Eh (-2.4810% error); Ex = -0.9957 Eh, Ec = -0.0458 Eh ; not used as a norm

------------------------------------------
Rare gas atoms (BBB93 dataset):
------------------------------------------
  Ne:
      Ex=-11.8384 Eh, -2.2267% error
      Ec=-0.3901 Eh, -0.2268% error
      Exc=-12.2285 Eh, -2.1641% error
  Ar:
      Ex=-29.5682 Eh, -2.0531% error
      Ec=-0.7579 Eh, 4.4772% error
      Exc=-30.3261 Eh, -1.8999% error
  Kr:
      Ex=-92.4095 Eh, -1.5768% error
      Ec=-1.8989 Eh, 2.6232% error
      Exc=-94.3085 Eh, -1.4956% error
  Xe:
      Ex=-176.7619 Eh, -1.3605% error
      Ec=-3.0751 Eh, 2.4224% error
      Exc=-179.8370 Eh, -1.2982% error

XC MAPE 1.7145%
 X MAPE 1.8043%
 C MAPE 2.4374%

    Bx=-0.1571, err=-30.4333%
    Cx=0.1651, err=-35.2933%
    Bc=0.0361, err=-6.8643%
    Bxc=-0.1210, err=-35.3124%

---------------------
Jellium Surface:
---------------------
  rs=2:
      sigma_xc = 3336.1434 erg/cm^2 (-2.2519% error)
      sigma_x = 2396.4376 erg/cm^2 (-8.6723% error)
      sigma_c = 939.7059 erg/cm^2 (19.1009% error)
  rs=3:
      sigma_xc = 764.1619 erg/cm^2 (-2.1560% error)
      sigma_x = 431.1208 erg/cm^2 (-18.0379% error)
      sigma_c = 333.0412 erg/cm^2 (30.6044% error)
  rs=4:
      sigma_xc = 264.8075 erg/cm^2 (-1.1912% error)
      sigma_x = 104.0654 erg/cm^2 (-33.7163% error)
      sigma_c = 160.7421 erg/cm^2 (44.8127% error)
  rs=5:
      sigma_xc = 115.5452 erg/cm^2 (2.2524% error)
      sigma_x = 23.1376 erg/cm^2 (-59.4077% error)
      sigma_c = 92.4076 erg/cm^2 (65.0135% error)

  XC MAPE 1.9629%
  X MAPE 29.9586%
  C MAPE 39.8829%

---------------------
Jellium cluster:
---------------------
  rs=2:
      sigma_xc = 3362.9919 erg/cm^2 (-1.4652% error)
  rs=3:
      sigma_xc = 768.7570 erg/cm^2 (-1.5676% error)
  rs=3.25:
      sigma_xc = 577.5366 erg/cm^2 (-0.8445% error)
  rs=4:
      sigma_xc = 265.1896 erg/cm^2 (-1.0487% error)
  rs=5:
      sigma_xc = 116.3641 erg/cm^2 (2.9771% error)

  XC MAPE 1.5806%

  rs=2:
      gamma = 1.3618 mhartree/bohr (90.4093% error)
  rs=3.25:
      gamma = 0.4557 mhartree/bohr (168.2520% error)
  rs=4:
      gamma = 0.2689 mhartree/bohr (465.2691% error)

  XC MAPE 241.3101%


NB: percent error defined as:
 100% * (approx/reference - 1)