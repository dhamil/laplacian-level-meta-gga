Error breakdown for SCAN-L X with SCAN-L C

LLMG parameters:  c0 = 0.819411,  c1 = 0.201352,  c2 = 0.18502,  c3 = 1.53804,

  H atom Exc=-0.3120 Eh (-0.1480% error); Ex = -0.3116 Eh, Ec = -0.0004 Eh
  He atom Exc=-1.0655 Eh (-0.2310% error); Ex = -1.0269 Eh, Ec = -0.0387 Eh ; not used as a norm

------------------------------------------
Rare gas atoms (BBB93 dataset):
------------------------------------------
  Ne:
      Ex=-12.2761 Eh, 1.3886% error
      Ec=-0.3314 Eh, -15.2443% error
      Exc=-12.6075 Eh, 0.8682% error
  Ar:
      Ex=-30.6904 Eh, 1.6643% error
      Ec=-0.6391 Eh, -11.9006% error
      Exc=-31.3295 Eh, 1.3460% error
  Kr:
      Ex=-94.7939 Eh, 0.9627% error
      Ec=-1.7385 Eh, -6.0474% error
      Exc=-96.5323 Eh, 0.8272% error
  Xe:
      Ex=-180.7462 Eh, 0.8628% error
      Ec=-2.8380 Eh, -5.4770% error
      Exc=-183.5842 Eh, 0.7584% error

XC MAPE 0.9499%
 X MAPE 1.2196%
 C MAPE 9.6673%

    Bx=-0.2678, err=18.5744%
    Cx=0.3042, err=19.2275%
    Bc=0.0396, err=2.2962%
    Bxc=-0.2282, err=21.9442%

---------------------
Jellium Surface:
---------------------
  rs=2:
      sigma_xc = 3294.0725 erg/cm^2 (-3.4845% error)
      sigma_x = 2266.8159 erg/cm^2 (-13.6122% error)
      sigma_c = 1027.2565 erg/cm^2 (30.1973% error)
  rs=3:
      sigma_xc = 726.1989 erg/cm^2 (-7.0168% error)
      sigma_x = 303.8894 erg/cm^2 (-42.2264% error)
      sigma_c = 422.3095 erg/cm^2 (65.6116% error)
  rs=4:
      sigma_xc = 244.3409 erg/cm^2 (-8.8280% error)
      sigma_x = 24.4149 erg/cm^2 (-84.4491% error)
      sigma_c = 219.9259 erg/cm^2 (98.1315% error)
  rs=5:
      sigma_xc = 104.5128 erg/cm^2 (-7.5108% error)
      sigma_x = -25.7614 erg/cm^2 (-145.1954% error)
      sigma_c = 130.2742 erg/cm^2 (132.6325% error)

  XC MAPE 6.7100%
  X MAPE 71.3708%
  C MAPE 81.6432%

---------------------
Jellium cluster:
---------------------
  rs=2:
      sigma_xc = 3146.6756 erg/cm^2 (-7.8032% error)
  rs=3:
      sigma_xc = 693.2716 erg/cm^2 (-11.2328% error)
  rs=3.25:
      sigma_xc = 518.4741 erg/cm^2 (-10.9848% error)
  rs=4:
      sigma_xc = 235.3301 erg/cm^2 (-12.1903% error)
  rs=5:
      sigma_xc = 101.9859 erg/cm^2 (-9.7470% error)

  XC MAPE 10.3916%

  rs=2:
      gamma = 1.4999 mhartree/bohr (109.7222% error)
  rs=3.25:
      gamma = 0.6860 mhartree/bohr (303.7661% error)
  rs=4:
      gamma = 0.4363 mhartree/bohr (817.1342% error)

  XC MAPE 410.2075%


NB: percent error defined as:
 100% * (approx/reference - 1)