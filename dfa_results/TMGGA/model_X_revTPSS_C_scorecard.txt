Error breakdown for model X with revTPSS C

  H atom Exc=-0.3125 Eh (-0.0000% error); Ex = -0.3125 Eh, Ec = -0.0000 Eh
  He atom Exc=-1.0767 Eh (0.8180% error); Ex = -1.0306 Eh, Ec = -0.0462 Eh ; not used as a norm

------------------------------------------
Rare gas atoms (K99 dataset):
------------------------------------------
  Ne:
      Ex=-11.7032 Eh, -3.3430% error
      Ec=-0.3652 Eh, -6.6060% error
      Exc=-12.0684 Eh, -3.4450% error
  Ar:
      Ex=-29.1190 Eh, -3.5413% error
      Ec=-0.7282 Eh, 0.3824% error
      Exc=-29.8471 Eh, -3.4492% error
  Kr:
      Ex=-91.3760 Eh, -2.6776% error
      Ec=-1.7978 Eh, -2.8416% error
      Exc=-93.1739 Eh, -2.6807% error
  Xe:
      Ex=-174.9176 Eh, -2.3897% error
      Ec=-2.9558 Eh, -1.5514% error
      Exc=-177.8734 Eh, -2.3759% error

XC MAPE 2.9877%
 X MAPE 2.9879%
 C MAPE 2.8453%

    Bx=-0.0976, err=-56.7831%
    Cx=0.0684, err=-73.2063%
    Bc=0.0384, err=-0.9753%
    Bxc=-0.0592, err=-68.3364%

---------------------
Jellium Surface:
---------------------
  rs=2:
      sigma_xc = 3573.3377 erg/cm^2 (4.6979% error)
      sigma_x = 2801.8341 erg/cm^2 (6.7772% error)
      sigma_c = 771.5036 erg/cm^2 (-2.2175% error)
  rs=3:
      sigma_xc = 821.6795 erg/cm^2 (5.2086% error)
      sigma_x = 571.1080 erg/cm^2 (8.5757% error)
      sigma_c = 250.5716 erg/cm^2 (-1.7366% error)
  rs=4:
      sigma_xc = 282.1888 erg/cm^2 (5.2943% error)
      sigma_x = 170.7546 erg/cm^2 (8.7609% error)
      sigma_c = 111.4341 erg/cm^2 (0.3911% error)
  rs=5:
      sigma_xc = 120.6367 erg/cm^2 (6.7582% error)
      sigma_x = 61.4665 erg/cm^2 (7.8360% error)
      sigma_c = 59.1702 erg/cm^2 (5.6611% error)

  XC MAPE 5.4897%
  X MAPE 7.9874%
  C MAPE 2.5016%

---------------------
Jellium cluster:
---------------------
  rs=2:
      sigma_xc = 3665.1111 erg/cm^2 (7.3868% error)
  rs=3:
      sigma_xc = 822.6835 erg/cm^2 (5.3372% error)
  rs=3.25:
      sigma_xc = 614.0210 erg/cm^2 (5.4194% error)
  rs=4:
      sigma_xc = 275.7628 erg/cm^2 (2.8966% error)
  rs=5:
      sigma_xc = 114.9536 erg/cm^2 (1.7289% error)

  XC MAPE 4.5538%

  rs=2:
      gamma = -0.4888 mhartree/bohr (-168.3402% error)
  rs=3.25:
      gamma = -0.1763 mhartree/bohr (-203.7452% error)
  rs=4:
      gamma = -0.1269 mhartree/bohr (-366.6657% error)

  XC MAPE 246.2504%


NB: percent error defined as:
 100% * (approx/reference - 1)