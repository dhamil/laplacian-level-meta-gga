Error breakdown for MVS X with PBE C

  H atom Exc=-0.3185 Eh (1.9118% error); Ex = -0.3125 Eh, Ec = -0.0060 Eh
  He atom Exc=-1.0731 Eh (0.4734% error); Ex = -1.0310 Eh, Ec = -0.0420 Eh ; not used as a norm

------------------------------------------
Rare gas atoms (K99 dataset):
------------------------------------------
  Ne:
      Ex=-12.1208 Eh, 0.1061% error
      Ec=-0.3513 Eh, -10.1607% error
      Exc=-12.4721 Eh, -0.2151% error
  Ar:
      Ex=-30.1330 Eh, -0.1823% error
      Ec=-0.7067 Eh, -2.5749% error
      Exc=-30.8397 Eh, -0.2384% error
  Kr:
      Ex=-93.8885 Eh, -0.0016% error
      Ec=-1.7672 Eh, -4.4953% error
      Exc=-95.6557 Eh, -0.0884% error
  Xe:
      Ex=-179.0970 Eh, -0.0575% error
      Ec=-2.9183 Eh, -2.7998% error
      Exc=-182.0153 Eh, -0.1027% error

XC MAPE 0.1612%
 X MAPE 0.0868%
 C MAPE 5.0077%

    Bx=-0.2215, err=-1.9243%
    Cx=0.2455, err=-3.7891%
    Bc=0.0386, err=-0.2259%
    Bxc=-0.1828, err=-2.2759%

---------------------
Jellium Surface:
---------------------
  rs=2:
      sigma_xc = 3061.1541 erg/cm^2 (-10.3090% error)
      sigma_x = 2233.2078 erg/cm^2 (-14.8930% error)
      sigma_c = 827.9464 erg/cm^2 (4.9362% error)
  rs=3:
      sigma_xc = 632.3327 erg/cm^2 (-19.0355% error)
      sigma_x = 357.5578 erg/cm^2 (-32.0232% error)
      sigma_c = 274.7749 erg/cm^2 (7.7549% error)
  rs=4:
      sigma_xc = 193.5574 erg/cm^2 (-27.7771% error)
      sigma_x = 69.1748 erg/cm^2 (-55.9396% error)
      sigma_c = 124.3825 erg/cm^2 (12.0563% error)
  rs=5:
      sigma_xc = 72.6825 erg/cm^2 (-35.6792% error)
      sigma_x = 5.6607 erg/cm^2 (-90.0689% error)
      sigma_c = 67.0218 erg/cm^2 (19.6818% error)

  XC MAPE 23.2002%
  X MAPE 48.2312%
  C MAPE 11.1073%

---------------------
Jellium cluster:
---------------------
  rs=2:
      sigma_xc = 2902.4004 erg/cm^2 (-14.9604% error)
  rs=3:
      sigma_xc = 601.1344 erg/cm^2 (-23.0302% error)
  rs=3.25:
      sigma_xc = 441.3970 erg/cm^2 (-24.2179% error)
  rs=4:
      sigma_xc = 186.0429 erg/cm^2 (-30.5810% error)
  rs=5:
      sigma_xc = 71.0211 erg/cm^2 (-37.1495% error)

  XC MAPE 25.9878%

  rs=2:
      gamma = 1.8555 mhartree/bohr (159.4439% error)
  rs=3.25:
      gamma = 0.7353 mhartree/bohr (332.8280% error)
  rs=4:
      gamma = 0.4684 mhartree/bohr (884.6193% error)

  XC MAPE 458.9637%


NB: percent error defined as:
 100% * (approx/reference - 1)