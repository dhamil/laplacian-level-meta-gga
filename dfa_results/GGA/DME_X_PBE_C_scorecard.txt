Error breakdown for DME X with PBE C

  H atom Exc=-0.3185 Eh (1.9123% error); Ex = -0.3125 Eh, Ec = -0.0060 Eh
  He atom Exc=-1.0727 Eh (0.4445% error); Ex = -1.0307 Eh, Ec = -0.0420 Eh ; not used as a norm

------------------------------------------
Rare gas atoms (BBB93 dataset):
------------------------------------------
  Ne:
      Ex=-11.9748 Eh, -1.1001% error
      Ec=-0.3513 Eh, -10.1607% error
      Exc=-12.3261 Eh, -1.3835% error
  Ar:
      Ex=-29.6722 Eh, -1.7087% error
      Ec=-0.7067 Eh, -2.5750% error
      Exc=-30.3789 Eh, -1.7290% error
  Kr:
      Ex=-92.3679 Eh, -1.6212% error
      Ec=-1.7672 Eh, -4.4953% error
      Exc=-94.1351 Eh, -1.6767% error
  Xe:
      Ex=-176.3192 Eh, -1.6076% error
      Ec=-2.9183 Eh, -2.7995% error
      Exc=-179.2375 Eh, -1.6273% error

XC MAPE 1.6041%
 X MAPE 1.5094%
 C MAPE 5.0076%

    Bx=-0.1226, err=-45.7003%
    Cx=0.0604, err=-76.3354%
    Bc=0.0386, err=-0.2263%
    Bxc=-0.0840, err=-55.1141%

---------------------
Jellium Surface:
---------------------
  rs=2:
      sigma_xc = 3207.9595 erg/cm^2 (-6.0076% error)
      sigma_x = 2380.0132 erg/cm^2 (-9.2983% error)
      sigma_c = 827.9464 erg/cm^2 (4.9362% error)
  rs=3:
      sigma_xc = 701.4723 erg/cm^2 (-10.1828% error)
      sigma_x = 426.6974 erg/cm^2 (-18.8788% error)
      sigma_c = 274.7749 erg/cm^2 (7.7549% error)
  rs=4:
      sigma_xc = 230.0967 erg/cm^2 (-14.1430% error)
      sigma_x = 105.7142 erg/cm^2 (-32.6661% error)
      sigma_c = 124.3825 erg/cm^2 (12.0563% error)
  rs=5:
      sigma_xc = 94.0333 erg/cm^2 (-16.7847% error)
      sigma_x = 27.0115 erg/cm^2 (-52.6115% error)
      sigma_c = 67.0218 erg/cm^2 (19.6818% error)

  XC MAPE 11.7795%
  X MAPE 28.3637%
  C MAPE 11.1073%

---------------------
Jellium cluster:
---------------------
  rs=2:
      sigma_xc = 3192.8640 erg/cm^2 (-6.4499% error)
  rs=3:
      sigma_xc = 702.3962 erg/cm^2 (-10.0645% error)
  rs=3.25:
      sigma_xc = 522.9657 erg/cm^2 (-10.2136% error)
  rs=4:
      sigma_xc = 232.2634 erg/cm^2 (-13.3345% error)
  rs=5:
      sigma_xc = 95.9257 erg/cm^2 (-15.1100% error)

  XC MAPE 11.0345%

  rs=2:
      gamma = 1.0307 mhartree/bohr (44.1132% error)
  rs=3.25:
      gamma = 0.2791 mhartree/bohr (64.3037% error)
  rs=4:
      gamma = 0.1295 mhartree/bohr (172.1431% error)

  XC MAPE 93.5200%


NB: percent error defined as:
 100% * (approx/reference - 1)