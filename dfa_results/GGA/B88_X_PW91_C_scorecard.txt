Error breakdown for B88 X with PW91 C

  H atom Exc=-0.3140 Eh (0.4697% error); Ex = -0.3098 Eh, Ec = -0.0042 Eh
  He atom Exc=-1.0660 Eh (-0.1858% error); Ex = -1.0255 Eh, Ec = -0.0406 Eh ; not used as a norm

---------------------
Rare gas atoms:
---------------------
  Ne:
      Ex=-12.1379 Eh, 0.2466% error
      Ec=-0.3715 Eh, -4.9795% error
      Exc=-12.5094 Eh, 0.0831% error
  Ar:
      Ex=-30.1533 Eh, -0.1148% error
      Ec=-0.7604 Eh, 4.8286% error
      Exc=-30.9138 Eh, 0.0012% error
  Kr:
      Ex=-93.8716 Eh, -0.0196% error
      Ec=-1.9062 Eh, 3.0150% error
      Exc=-95.7778 Eh, 0.0390% error
  Xe:
      Ex=-179.0420 Eh, -0.0882% error
      Ec=-3.1431 Eh, 4.6875% error
      Exc=-182.1851 Eh, -0.0095% error

XC MAPE 0.0332%
 X MAPE 0.1173%
 C MAPE 4.3776%

    Bx=-0.2168, err=-4.0035%
    Cx=0.2312, err=-9.3715%
    Bc=0.0331, err=-14.6448%
    Bxc=-0.1837, err=-1.8006%

---------------------
Jellium Surface:
---------------------
  rs=2:
      sigma_xc = 3175.3475 erg/cm^2 (-6.9632% error)
      sigma_x = 2383.0213 erg/cm^2 (-9.1836% error)
      sigma_c = 792.3261 erg/cm^2 (0.4216% error)
  rs=3:
      sigma_xc = 701.5793 erg/cm^2 (-10.1691% error)
      sigma_x = 444.1176 erg/cm^2 (-15.5670% error)
      sigma_c = 257.4617 erg/cm^2 (0.9654% error)
  rs=4:
      sigma_xc = 231.3603 erg/cm^2 (-13.6715% error)
      sigma_x = 117.8110 erg/cm^2 (-24.9611% error)
      sigma_c = 113.5493 erg/cm^2 (2.2967% error)
  rs=5:
      sigma_xc = 94.5169 erg/cm^2 (-16.3567% error)
      sigma_x = 35.0109 erg/cm^2 (-38.5774% error)
      sigma_c = 59.5061 erg/cm^2 (6.2608% error)

  XC MAPE 11.7901%
  X MAPE 22.0723%
  C MAPE 2.4861%

---------------------
Jellium cluster:
---------------------
  rs=2:
      sigma_xc = 3162.1566 erg/cm^2 (-7.3496% error)
  rs=3:
      sigma_xc = 700.2466 erg/cm^2 (-10.3397% error)
  rs=3.25:
      sigma_xc = 521.5651 erg/cm^2 (-10.4541% error)
  rs=4:
      sigma_xc = 231.1422 erg/cm^2 (-13.7529% error)
  rs=5:
      sigma_xc = 94.4487 erg/cm^2 (-16.4171% error)

  XC MAPE 11.6627%

  rs=2:
      gamma = 1.6206 mhartree/bohr (126.5899% error)
  rs=3.25:
      gamma = 0.5034 mhartree/bohr (196.2843% error)
  rs=4:
      gamma = 0.2823 mhartree/bohr (493.4062% error)

  XC MAPE 272.0935%


NB: percent error defined as:
 100% * (approx/reference - 1)