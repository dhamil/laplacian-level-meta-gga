Error breakdown for B88 X with ACGGA C

  H atom Exc=-0.3173 Eh (1.5454% error); Ex = -0.3098 Eh, Ec = -0.0076 Eh
  He atom Exc=-1.0726 Eh (0.4353% error); Ex = -1.0255 Eh, Ec = -0.0472 Eh ; not used as a norm

------------------------------------------
Rare gas atoms (BBB93 dataset):
------------------------------------------
  Ne:
      Ex=-12.1379 Eh, 0.2466% error
      Ec=-0.3745 Eh, -4.2094% error
      Exc=-12.5124 Eh, 0.1072% error
  Ar:
      Ex=-30.1533 Eh, -0.1148% error
      Ec=-0.7507 Eh, 3.4813% error
      Exc=-30.9040 Eh, -0.0304% error
  Kr:
      Ex=-93.8716 Eh, -0.0196% error
      Ec=-1.8548 Eh, 0.2357% error
      Exc=-95.7263 Eh, -0.0147% error
  Xe:
      Ex=-179.0420 Eh, -0.0882% error
      Ec=-3.0491 Eh, 1.5544% error
      Exc=-182.0910 Eh, -0.0611% error

XC MAPE 0.0534%
 X MAPE 0.1173%
 C MAPE 2.3702%

    Bx=-0.2168, err=-4.0035%
    Cx=0.2312, err=-9.3715%
    Bc=0.0361, err=-6.7125%
    Bxc=-0.1807, err=-3.4428%

---------------------
Jellium Surface:
---------------------
  rs=2:
      sigma_xc = 3092.6622 erg/cm^2 (-9.3858% error)
      sigma_x = 2383.0213 erg/cm^2 (-9.1836% error)
      sigma_c = 709.6408 erg/cm^2 (-10.0582% error)
  rs=3:
      sigma_xc = 666.2180 erg/cm^2 (-14.6968% error)
      sigma_x = 444.1176 erg/cm^2 (-15.5670% error)
      sigma_c = 222.1005 erg/cm^2 (-12.9018% error)
  rs=4:
      sigma_xc = 213.5325 erg/cm^2 (-20.3237% error)
      sigma_x = 117.8110 erg/cm^2 (-24.9611% error)
      sigma_c = 95.7215 erg/cm^2 (-13.7644% error)
  rs=5:
      sigma_xc = 84.5167 erg/cm^2 (-25.2064% error)
      sigma_x = 35.0109 erg/cm^2 (-38.5774% error)
      sigma_c = 49.5059 erg/cm^2 (-11.5967% error)

  XC MAPE 17.4032%
  X MAPE 22.0723%
  C MAPE 12.0803%

---------------------
Jellium cluster:
---------------------
  rs=2:
      sigma_xc = 3075.9703 erg/cm^2 (-9.8749% error)
  rs=3:
      sigma_xc = 664.6411 erg/cm^2 (-14.8987% error)
  rs=3.25:
      sigma_xc = 492.1256 erg/cm^2 (-15.5085% error)
  rs=4:
      sigma_xc = 213.5708 erg/cm^2 (-20.3094% error)
  rs=5:
      sigma_xc = 84.7003 erg/cm^2 (-25.0439% error)

  XC MAPE 17.1271%

  rs=2:
      gamma = 1.5349 mhartree/bohr (114.6124% error)
  rs=3.25:
      gamma = 0.4781 mhartree/bohr (181.4285% error)
  rs=4:
      gamma = 0.2686 mhartree/bohr (464.5860% error)

  XC MAPE 253.5423%


NB: percent error defined as:
 100% * (approx/reference - 1)