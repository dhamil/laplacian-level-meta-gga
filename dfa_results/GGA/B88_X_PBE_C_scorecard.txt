Error breakdown for B88 X with PBE C

  H atom Exc=-0.3157 Eh (1.0341% error); Ex = -0.3098 Eh, Ec = -0.0060 Eh
  He atom Exc=-1.0675 Eh (-0.0485% error); Ex = -1.0255 Eh, Ec = -0.0420 Eh ; not used as a norm

---------------------
Rare gas atoms:
---------------------
  Ne:
      Ex=-12.1379 Eh, 0.2466% error
      Ec=-0.3513 Eh, -10.1607% error
      Exc=-12.4891 Eh, -0.0790% error
  Ar:
      Ex=-30.1533 Eh, -0.1148% error
      Ec=-0.7067 Eh, -2.5750% error
      Exc=-30.8601 Eh, -0.1725% error
  Kr:
      Ex=-93.8716 Eh, -0.0196% error
      Ec=-1.7672 Eh, -4.4953% error
      Exc=-95.6388 Eh, -0.1061% error
  Xe:
      Ex=-179.0420 Eh, -0.0882% error
      Ec=-2.9183 Eh, -2.7995% error
      Exc=-181.9603 Eh, -0.1329% error

XC MAPE 0.1226%

    Bx=-0.2168, err=-4.0035%
    Cx=0.2312, err=-9.3715%
    Bc=0.0386, err=-0.2263%
    Bxc=-0.1781, err=-4.7855%

---------------------
Jellium Surface:
---------------------
  rs=2:
      sigma_xc = 3210.9677 erg/cm^2 (-5.9195% error)
      sigma_x = 2383.0213 erg/cm^2 (-9.1836% error)
      sigma_c = 827.9464 erg/cm^2 (4.9362% error)
  rs=3:
      sigma_xc = 718.8925 erg/cm^2 (-7.9523% error)
      sigma_x = 444.1176 erg/cm^2 (-15.5670% error)
      sigma_c = 274.7749 erg/cm^2 (7.7549% error)
  rs=4:
      sigma_xc = 242.1936 erg/cm^2 (-9.6293% error)
      sigma_x = 117.8110 erg/cm^2 (-24.9611% error)
      sigma_c = 124.3825 erg/cm^2 (12.0563% error)
  rs=5:
      sigma_xc = 102.0327 erg/cm^2 (-9.7056% error)
      sigma_x = 35.0109 erg/cm^2 (-38.5774% error)
      sigma_c = 67.0218 erg/cm^2 (19.6818% error)

  XC MAPE 8.3017%
  X MAPE 22.0723%
  C MAPE 11.1073%

---------------------
Jellium cluster:
---------------------
  rs=2:
      sigma_xc = 3203.9769 erg/cm^2 (-6.1243% error)
  rs=3:
      sigma_xc = 719.6546 erg/cm^2 (-7.8547% error)
  rs=3.25:
      sigma_xc = 538.4144 erg/cm^2 (-7.5613% error)
  rs=4:
      sigma_xc = 242.8915 erg/cm^2 (-9.3689% error)
  rs=5:
      sigma_xc = 102.4306 erg/cm^2 (-9.3534% error)

  XC MAPE 8.0525%

  rs=2:
      gamma = 1.5210 mhartree/bohr (112.6669% error)
  rs=3.25:
      gamma = 0.4675 mhartree/bohr (175.1686% error)
  rs=4:
      gamma = 0.2588 mhartree/bohr (444.0208% error)

  XC MAPE 243.9521%


NB: percent error defined as:
 100% * (approx/reference - 1)