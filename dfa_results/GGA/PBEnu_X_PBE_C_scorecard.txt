Error breakdown for PBEnu X with PBE C

  H atom Exc=-0.3125 Eh (0.0000% error); Ex = -0.3065 Eh, Ec = -0.0060 Eh
  He atom Exc=-1.0586 Eh (-0.8780% error); Ex = -1.0166 Eh, Ec = -0.0420 Eh ; not used as a norm

------------------------------------------
Rare gas atoms (BBB93 dataset):
------------------------------------------
  Ne:
      Ex=-12.1673 Eh, 0.4898% error
      Ec=-0.3513 Eh, -10.1607% error
      Exc=-12.5186 Eh, 0.1567% error
  Ar:
      Ex=-30.2484 Eh, 0.1999% error
      Ec=-0.7067 Eh, -2.5750% error
      Exc=-30.9551 Eh, 0.1348% error
  Kr:
      Ex=-94.1004 Eh, 0.2241% error
      Ec=-1.7672 Eh, -4.4953% error
      Exc=-95.8676 Eh, 0.1328% error
  Xe:
      Ex=-179.3848 Eh, 0.1031% error
      Ec=-2.9183 Eh, -2.7995% error
      Exc=-182.3031 Eh, 0.0553% error

XC MAPE 0.1199%
 X MAPE 0.2542%
 C MAPE 5.0076%

    Bx=-0.2282, err=1.0280%
    Cx=0.2486, err=-2.5694%
    Bc=0.0386, err=-0.2263%
    Bxc=-0.1895, err=1.2877%

---------------------
Jellium Surface:
---------------------
  rs=2:
      sigma_xc = 3219.6855 erg/cm^2 (-5.6641% error)
      sigma_x = 2391.7391 erg/cm^2 (-8.8514% error)
      sigma_c = 827.9464 erg/cm^2 (4.9362% error)
  rs=3:
      sigma_xc = 728.3323 erg/cm^2 (-6.7436% error)
      sigma_x = 453.5574 erg/cm^2 (-13.7724% error)
      sigma_c = 274.7749 erg/cm^2 (7.7549% error)
  rs=4:
      sigma_xc = 248.8714 erg/cm^2 (-7.1376% error)
      sigma_x = 124.4888 erg/cm^2 (-20.7078% error)
      sigma_c = 124.3825 erg/cm^2 (12.0563% error)
  rs=5:
      sigma_xc = 106.7550 erg/cm^2 (-5.5265% error)
      sigma_x = 39.7332 erg/cm^2 (-30.2926% error)
      sigma_c = 67.0218 erg/cm^2 (19.6818% error)

  XC MAPE 6.2679%
  X MAPE 18.4060%
  C MAPE 11.1073%

---------------------
Jellium cluster:
---------------------
  rs=2:
      sigma_xc = 3207.0237 erg/cm^2 (-6.0351% error)
  rs=3:
      sigma_xc = 727.4579 erg/cm^2 (-6.8556% error)
  rs=3.25:
      sigma_xc = 545.8606 erg/cm^2 (-6.2828% error)
  rs=4:
      sigma_xc = 248.9376 erg/cm^2 (-7.1128% error)
  rs=5:
      sigma_xc = 106.8681 erg/cm^2 (-5.4265% error)

  XC MAPE 6.3426%

  rs=2:
      gamma = 1.8802 mhartree/bohr (162.8846% error)
  rs=3.25:
      gamma = 0.6194 mhartree/bohr (264.5910% error)
  rs=4:
      gamma = 0.3619 mhartree/bohr (660.5808% error)

  XC MAPE 362.6855%


NB: percent error defined as:
 100% * (approx/reference - 1)