Error breakdown for AES X with AES C

  H atom Exc=-0.2902 Eh (-7.1291% error); Ex = -0.2680 Eh, Ec = -0.0222 Eh
  He atom Exc=-0.9965 Eh (-6.6944% error); Ex = -0.8840 Eh, Ec = -0.1125 Eh ; not used as a norm

------------------------------------------
Rare gas atoms (BBB93 dataset):
------------------------------------------
  Ne:
      Ex=-11.4876 Eh, -5.1238% error
      Ec=-0.4701 Eh, 20.2324% error
      Exc=-11.9577 Eh, -4.3306% error
  Ar:
      Ex=-29.0856 Eh, -3.6518% error
      Ec=-0.8582 Eh, 18.3047% error
      Exc=-29.9438 Eh, -3.1366% error
  Kr:
      Ex=-91.9696 Eh, -2.0454% error
      Ec=-1.9378 Eh, 4.7236% error
      Exc=-93.9074 Eh, -1.9145% error
  Xe:
      Ex=-176.2397 Eh, -1.6519% error
      Ec=-3.1023 Eh, 3.3288% error
      Exc=-179.3421 Eh, -1.5699% error

XC MAPE 2.7379%
 X MAPE 3.1182%
 C MAPE 11.6474%

    Bx=-0.1839, err=-18.5902%
    Cx=0.3001, err=17.6170%
    Bc=0.0407, err=5.1583%
    Bxc=-0.1431, err=-23.5065%

---------------------
Jellium Surface:
---------------------
  rs=2:
      sigma_xc = 3287.4806 erg/cm^2 (-3.6777% error)
      sigma_x = 2327.7158 erg/cm^2 (-11.2913% error)
      sigma_c = 959.7648 erg/cm^2 (21.6432% error)
  rs=3:
      sigma_xc = 753.9140 erg/cm^2 (-3.4681% error)
      sigma_x = 417.5128 erg/cm^2 (-20.6249% error)
      sigma_c = 336.4012 erg/cm^2 (31.9220% error)
  rs=4:
      sigma_xc = 262.5555 erg/cm^2 (-2.0315% error)
      sigma_x = 104.1023 erg/cm^2 (-33.6928% error)
      sigma_c = 158.4531 erg/cm^2 (42.7505% error)
  rs=5:
      sigma_xc = 115.3093 erg/cm^2 (2.0436% error)
      sigma_x = 27.2333 erg/cm^2 (-52.2222% error)
      sigma_c = 88.0759 erg/cm^2 (57.2784% error)

  XC MAPE 2.8052%
  X MAPE 29.4578%
  C MAPE 38.3986%

---------------------
Jellium cluster:
---------------------
  rs=2:
      sigma_xc = 3255.5159 erg/cm^2 (-4.6142% error)
  rs=3:
      sigma_xc = 752.2523 erg/cm^2 (-3.6809% error)
  rs=3.25:
      sigma_xc = 567.6907 erg/cm^2 (-2.5349% error)
  rs=4:
      sigma_xc = 264.5009 erg/cm^2 (-1.3056% error)
  rs=5:
      sigma_xc = 117.5053 erg/cm^2 (3.9870% error)

  XC MAPE 3.2245%

  rs=2:
      gamma = 2.0027 mhartree/bohr (180.0212% error)
  rs=3.25:
      gamma = 0.6004 mhartree/bohr (253.3746% error)
  rs=4:
      gamma = 0.3178 mhartree/bohr (567.9655% error)

  XC MAPE 333.7871%


NB: percent error defined as:
 100% * (approx/reference - 1)