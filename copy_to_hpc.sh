#!/bin/zsh

local_targ_dir=./tmp_to_copy

mkdir -p tmp_to_copy
cp -r ./atomic_densities $local_targ_dir
cp -r ./data_files $local_targ_dir
cp -r ./dfa_results $local_targ_dir
cp -r ./dft $local_targ_dir
cp -r ./enhance $local_targ_dir
cp -r ./jellium_scf $local_targ_dir
cp constants.py $local_targ_dir
cp jellium_norms.py $local_targ_dir
cp lsq.py $local_targ_dir
cp main.py $local_targ_dir
cp reference_data.py $local_targ_dir
cp runcalc.sh $local_targ_dir
cp settings.py $local_targ_dir
