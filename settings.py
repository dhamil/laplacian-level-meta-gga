# author: Aaron Kaplan
# contact: kaplan@temple.edu

X = "AES" # LSDA, PBE, SCAN, or LLMG
C = "AES" # LSDA, PBE, ACGGA, SCAN, or LLMG

GE = "sol" # O2, O4, simple, qreg
KEMOD = "RPP1"

coeffs = [-.1]#[0.819411,0.201352,0.185020, 1.53804]

npar = len(coeffs)#4
fit_pars = {}
zpp = ['c0','c1','c2','c3']
for iz in range(npar):
    az = zpp[iz]
    fit_pars[az] = coeffs[iz]

# RHF datasets, either CR74 from [CR1974], or BBB93 from [BBB1993], or K99 from [KKWT1991]
dataset = 'BBB93'

routine = {'main': 'calc_norms',
'fit': 'report',
'reopt_r2': False,
'make_tex_table': False,
'vxc': {'atom': ['H','He'], 'js': [2,3,4,5]}}
"""
    Options for routine['main']:
        regen_atomic_dens : remake RHF density files for rare gases, H, and He

        calc_norms : calculate errors/values for appropriate norms
        specific subroutine controlled by "fit" (below)

        fxc : plots the XC enhancement factor for the current DFA and parameters; options set in "fxc_plot_pars" (below)

    Options for routine['fit']:
        lsda_reference : regenerates LSDA Bx, Cx, and Bc coefficients needed for everything else in the routine

        report : gives a scorecard for all appropriate norms using the functional and parameters set here

        filter : tiered search on grids of increasing fineness. Search radius around previous best points are given by throw (below)

        Ax : fits the Ax coefficient using a parameterization of the TF density

        fixed_list: simple grid search on a fixed grid
"""


fxc_plot_pars = {'rs':[4.0], 'zeta': [0.0,1.0], 'wfxc': ['xc','x'], 'aux': [-2.0,-1.0,0.0,2.0] }
"""
    fxc_plot_pars options:
        rs : list of rs values
        zeta : list of spin-polarization values
        wfxc : x or xc
        aux : auxiliary variable. For SCAN, this is alpha. For LLMGA, this is q
"""
target_atom = 'H' # the target atom for calc_coeffs = False

# optional multiprocessing
ncore=6

"""
    Jellium appropriate norms:

    for the planar surface, enter a list of rs values
    for the clusters, enter a dict of rs values; each dict entry is a list of N values
"""
use_jellium_norms = True
js_ref = 'RPA+' # 'DMC' or RPA+
jellium_surface_rs = [2,3,4,5]
jellium_cluster_rs = {2: [2,8,18,20,34,40,58,92,106],#2,8,
3: [2,8,18,20,34,40,58,92,106],
3.25: [2,8,18,20,34,40,58,92,106],
4: [2,8,18,20,34,40,58,92,106],
5: [2,8,18,20,34,40,58,92,106]#,5.62: [2,8,18,20,34,40,58,92,106]
}

search_radius = 3
# weights for the norms. No need to ensure they're normalized, routine does that on its own
norm_wgts = {'SAXC': 1.0, 'Bx': 0.0, 'Cx': 0.0, 'Bc': 0.0, 'H': 0.0,
    'JS': 1.0, 'JC sig': 1.0, 'JC gam': 0.0}
normalize = 0.0
for mod in range(2):
    for akey in norm_wgts:
        if mod == 0:
            normalize += norm_wgts[akey]
        elif mod == 1:
            norm_wgts[akey] /= normalize

# enter bounds as [min, max, step]
parbds = [
[.1,5,0.1],
[0.167,.174,0.1],#[1.e-3,.999,.001],
[-1,1,.001],
[0.1,5.,0.1]
]
hard_bounds = [[False,False],[False,False],[False,False],[False,False]]#[[True,True],[True,True],[True,True],[True,True]]

fxcd = {
'GGA': ['PW86','PW91','PBE','PBEsol','acPBE','ACGGA','B88','LYP'],
'LMGGA': ['LLMG','GEA','LLYP'],
'TMGGA': ['TPSS','revTPSS','SCAN','rSCAN','r2SCAN','R201','TASK','MVS','MLSD','TM','DME']
}
for mgga in fxcd['TMGGA']:
    fxcd['LMGGA'].append(mgga+'-L')
