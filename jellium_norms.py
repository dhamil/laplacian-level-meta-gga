import numpy as np
from os import path
import multiprocessing as mproc

import settings
from constants import pi,Eh_to_eV,au_to_erg_cm_m2
from dft.ex import ex_den_unp
from dft.ec import ec_den,ec_pw92
from lsq import lst_sq_lin
from reference_data import gamma_xc_lsd_ref,jellium_surface_x_ref,dmc_ref_delta_gamma_xc,jellium_cluster_dmc_ref,sigma_xc_reference

sig_xc_ref = {}
for anrs in settings.jellium_surface_rs:
    sig_xc_ref[anrs] = sigma_xc_reference(anrs,pars=settings.js_ref)
for anrs in settings.jellium_cluster_rs:
    if anrs not in sig_xc_ref:
        sig_xc_ref[anrs] = sigma_xc_reference(anrs,pars=settings.js_ref)

def provide_references():
    # a simple helper routine to pass these values to main.py
    return sig_xc_ref

def eps_xc_unif(rs):
    zeta = 0.0
    n = 3.0/(4.0*pi*rs**3)
    eps_x = -3.0/4.0*(3.0/(2*pi))**(2.0/3.0)/rs
    eps_c = ec_pw92(zeta,rs)
    return eps_x+eps_c

def eps_c_dmc_unp(rs,param='PW'):
    """
        [OB1994]
    """
    if param=='PW':
        def g(v,rs):
            q0 = -2.0*v[0]*(1.0 + v[1]*rs)
            q1 = 2.0*v[0]*(v[2]*rs**(0.5) + v[3]*rs + v[4]*rs**(1.5) + v[5]*rs**2)
            return q0*np.log(1.0 + 1.0/q1)
        eps_c = g([0.031091,0.026481,7.5957,3.5876,-0.46647,0.13354],rs)
    elif param == 'PZ':
        gamma = -0.103756
        beta1 = 0.56371
        beta2 = 0.27358
        A = 0.031091
        B = -0.046644
        C = -0.00419
        D = -0.00983
        if hasattr(rs,'__len__'):
            eps_c = np.zeros(rs.shape)
            lm = rs < 1.0
            um = rs >= 1.0
            eps_c[um] = gamma/(1.0 + beta1*rs[um]**(0.5) + beta2*rs[um])
            eps_c[lm] = A*np.log(rs[lm]) + B + C*rs[lm]*np.log(rs[lm]) + D*rs[lm]
        else:
            if rs >= 1.0:
                eps_c = gamma/(1.0 + beta1*rs**(0.5) + beta2*rs)
            else:
                eps_c = A*np.log(rs) + B + C*rs*np.log(rs) + D*rs
    return eps_c

def liquid_drop_model(rs,N_l,eps_xc_diff,sxc_lsda,gxc_lsda):

    """
        See, e.g., [FP1992]
    """

    y = eps_xc_diff*N_l**(-1.0/3.0)
    x = N_l**(1.0/3.0)
    gam_diff_1,sig_diff_1,cdet_1=lst_sq_lin(x,y)
    y = eps_xc_diff*N_l**(-2.0/3.0)
    x = N_l**(-1.0/3.0)
    sig_diff_2,gam_diff_2,cdet_2=lst_sq_lin(x,y)

    if cdet_2 > cdet_1:
        sig_diff = sig_diff_2
        gam_diff = gam_diff_2
    else:
        sig_diff = sig_diff_1
        gam_diff = gam_diff_1
    sigma_xc = sig_diff*au_to_erg_cm_m2/(4*pi*rs**2) + sxc_lsda[rs]#sigma_xc_reference(rs,pars='LSDA')
    gamma_xc = gam_diff*1000.0/(2*pi*rs)+gamma_xc_lsd_ref(rs)#+gxc_lsda[rs]#
    """
    import matplotlib.pyplot as plt
    plt.scatter(N_l**(-1.0/3.0),1000*eps_xc_diff/N_l)
    nl = np.linspace(1.0,1e3,5000)
    plt.plot(nl**(-1.0/3.0),1000*(sig_diff*nl**(-1.0/3.0) + gam_diff*nl**(-2.0/3.0)))
    plt.show()
    #exit()
    """
    return sigma_xc,gamma_xc

def xc_energy(dfa_x,dfa_c,wgt,n,gn,ln,tau,fpars):
    eps_x = ex_den_unp(dfa_x,2*n,2*gn,2*tau,2*ln,fpars)
    eps_c = ec_den(dfa_c,n,n,gn,gn,2*gn,tau,tau,ln,ln,fpars,unp=True)
    return np.sum(wgt*(eps_x+eps_c))

def xc_surface_energy(dfa_x,dfa_c,rs,step,wgt,n,gn,ln,tau,fpars,x_only=False):
    """
        See, e.g., [LK1970] and other references in
        ./jellium_scf/surface/surface_hamiltonian.py
    """
    n0 = 3/(4*pi*rs**3)*np.ones(1)
    dn = np.zeros(1)
    tau0 = 0.3*(3*pi**2)**(2/3)*n0**(5/3)
    eps_x = ex_den_unp(dfa_x,n,gn,tau,ln,fpars)
    x_unif = ex_den_unp(dfa_x,n0,0.0,tau0,0.0,fpars)
    if x_only:
        eps_c = 0.0
        c_unif = 0.0
    else:
        eps_c = ec_den(dfa_c,n/2,n/2,gn/2,gn/2,gn,tau/2,tau/2,ln/2,ln/2,fpars,unp=True)
        c_unif = ec_den(dfa_c,n0/2,n0/2,dn,dn,dn,tau0/2,tau0/2,dn,dn,fpars,unp=True)
    xc_unif = ((x_unif + c_unif))[0]
    xc = eps_x + eps_c
    sigma_xc = np.sum(wgt*(xc - step*xc_unif))
    return sigma_xc*au_to_erg_cm_m2

def source_data_files():
    surface_list = []
    for rs in sig_xc_ref:#settings.jellium_surface_rs:
        prefix = 'rs_'+str(rs)
        tfile = './jellium_scf/data/surface/'+prefix+'/'+prefix+'_density.csv'
        # to compare to old data, uncomment the following line
        #tfile = '/Users/aaronkaplan/Dropbox/phd.nosync/SCAN2/scan2_opt.nosync/LDA'+str(rs)
        if path.isfile(tfile):
            surface_list.append([rs,tfile])
        else:
            print('Cannot source file',tfile)

    cluster_d = {}
    cluster_dict = settings.jellium_cluster_rs
    for rs in cluster_dict:
        cluster_d[rs] = []
        for nelec in cluster_dict[rs]:
            prefix = 'N_'+str(nelec)+'_rs_'+str(rs)
            tfile = './jellium_scf/data/cluster/rs_'+str(rs)+'/'+prefix+'/cluster_density_'+'rs_'+str(rs)+'_N='+str(nelec)+'.csv'
            if path.isfile(tfile):
                cluster_d[rs].append([nelec,tfile])
            else:
                print('Cannot source file',tfile)
    return surface_list,cluster_d

def get_jerr_list(fpars,lsd_cluster,lsd_surf,gxc_lsda,surface_list,cluster_dict,surf_data,clus_data,all_stats):
    zps = {}
    #zps['c0'],zps['c1'],zps['c2'],zps['c3'] = fpars
    parkeys = ['c0','c1','c2','c3']
    for ic in range(settings.npar):
        zps[parkeys[ic]] = fpars[ic]
    nwgt = settings.norm_wgts
    if all_stats:
        tdr = 7#[]
    else:
        tdr = 3
    surf_resid = np.zeros((len(settings.jellium_surface_rs),tdr))
    res = 0.0
    surf_wgt = 1.0/len(surface_list)
    clus_wgt = 1.0/len(cluster_dict.keys())
    for irs,rs in enumerate(settings.jellium_surface_rs):
        step,wgt,n,gn,ln,tau = np.transpose(surf_data[rs])
        sigma_xc = xc_surface_energy(settings.X,settings.C,rs,step,wgt,n,gn,ln,tau,zps)
        err = 100*(sigma_xc/sig_xc_ref[rs]-1.0)
        res += nwgt['JS']*(err)**2*surf_wgt
        if all_stats:
            sig_x_ref = jellium_surface_x_ref[rs]
            sig_c_ref = sig_xc_ref[rs] - sig_x_ref
            sigma_x = xc_surface_energy(settings.X,settings.C,rs,step,wgt,n,gn,ln,tau,zps,x_only=True)
            sigma_c = sigma_xc - sigma_x
            err_x = 100*(sigma_x/sig_x_ref-1.0)
            err_c = 100*(sigma_c/sig_c_ref-1.0)
            #surf_resid.append([rs,sigma_xc,err,sigma_x,err_x,sigma_c,err_c])
            surf_resid[irs] = [rs,sigma_xc,err,sigma_x,err_x,sigma_c,err_c]
        else:
            #surf_resid.append([rs,sigma_xc,err])
            surf_resid[irs] = [rs,sigma_xc,err]

    dmc_dgam = dmc_ref_delta_gamma_xc
    clus_resid = np.zeros((len(cluster_dict.keys()),3))
    gam_resid = np.zeros((len(dmc_dgam.keys()),3))
    gam_wgt = 1.0/len(dmc_dgam.keys())
    jrs = 0
    for irs,rs in enumerate(cluster_dict):
        exc = np.zeros(0)
        n_l= np.zeros(0)
        for ifl,fl in enumerate(cluster_dict[rs]):
            n_l = np.append(n_l,fl[0])
            wgt,nu,gnu,lu,tu = np.transpose(clus_data[rs][ifl])
            exc = np.append(exc,xc_energy(settings.X,settings.C,wgt,nu,gnu,lu,tu,zps))
        ediff = (exc-lsd_cluster[rs])
        sig_xc,dgam_xc = liquid_drop_model(rs,n_l,ediff,lsd_surf,gxc_lsda)
        err = 100*(sig_xc/sig_xc_ref[rs]-1.0)
        res += nwgt['JC sig']*(err)**2*clus_wgt
        #clus_resid.append([rs,sig_xc,err])
        clus_resid[irs] = [rs,sig_xc,err]
        if rs in dmc_dgam:
            gam_ref = dmc_dgam[rs] + gamma_xc_lsd_ref(rs)
            dgam_err = 100*(dgam_xc/gam_ref-1.0)
            #gam_resid.append([rs,dgam_xc,dgam_err])
            gam_resid[jrs] = [rs,dgam_xc,dgam_err]
            res += nwgt['JC gam']*dgam_err**2*gam_wgt
            jrs += 1
    return surf_resid,clus_resid,gam_resid,res

def get_lsda_ref(surface_list,cluster_dict,cdata,sdata):

    sxc_lsda = {}
    for fl in surface_list:
        rs = fl[0]
        step,wgt,n,_,_,_ = np.transpose(sdata[rs])
        trs = (3.0/(4.0*pi*n))**(1.0/3.0)
        xc = eps_xc_unif(trs)
        xc_unif = eps_xc_unif(rs)
        nbulk = 3.0/(4*pi*rs**3)
        #sigma_xc = np.sum(n*wgt*(xc - xc_unif))
        sigma_xc = np.sum(wgt*(n*xc - nbulk*step*xc_unif))
        sxc_lsda[rs]= sigma_xc*au_to_erg_cm_m2

    exc_lsda = {}
    gxc_lsda = {}
    for rs in cluster_dict:
        exc_lsda[rs] = np.zeros(0)
        nl = []
        for ifl,fl in enumerate(cluster_dict[rs]):
            nl.append(fl[0])
            wgt,nu,_,_,_ = np.transpose(cdata[rs][ifl])
            trs = (3.0/(4.0*pi*2*nu))**(1.0/3.0)
            exc = np.sum(2*wgt*nu*eps_xc_unif(trs))
            exc_lsda[rs]= np.append(exc_lsda[rs],exc)
        nl = np.array(nl)
        rhs = nl**(-1/3)
        lhs = (exc_lsda[rs] - eps_xc_unif(rs)*nl)/nl**(2/3)
        p=np.polynomial.polynomial.polyfit(rhs,lhs,2)
        gxc_lsda[rs] = p[1]*1000/(2*pi*rs)
    return exc_lsda,sxc_lsda,gxc_lsda


def init_jellium_calc():

    sl,cd = source_data_files()
    sfl = {}
    for fl in sl:
        rs = fl[0]
        x,n,gn,ln,tau = np.transpose(np.genfromtxt(fl[1],delimiter=',',skip_header=1))
        """
        # to check this using old data, uncomment this block
        dat = np.zeros((0,5))
        with open(fl[1],'r') as infl:
            for iln,ln in enumerate(infl):
                if iln == 0:
                    continue
                aln = filter(None,(ln.strip()).split(' '))
                aln = np.asarray([float(elt) for elt in aln])
                aln[0]*=2*pi*rs/(9*pi/4)**(1.0/3.0)
                dat = np.vstack((dat,aln))
        x,n,gn,tau,ln = np.transpose(dat)
        tau += 3.0/10.0*(9*pi/4)**(2.0/3.0)/rs**2*n
        """
        dx = x[1]-x[0]
        wgt = dx*np.ones(x.shape[0])
        wgt[[0,-1]] *= 0.5
        step = np.ones(x.shape)
        step[x>0.0] = 0.0
        sfl[rs] = np.transpose((step,wgt,n,gn,ln,tau))
    cfl = {}
    for rs in cd:
        cfl[rs] = []
        for fl in cd[rs]:
            dat = np.genfromtxt(fl[1],delimiter=',',skip_header=1)
            cfl[rs].append(np.transpose((dat[:,1],dat[:,2],dat[:,4],dat[:,7],dat[:,9])))

    lsda_ref,sxc_lsda,gxc_lsda = get_lsda_ref(sl,cd,cfl,sfl)

    return sl,cd,sfl,cfl,lsda_ref,sxc_lsda,gxc_lsda

def get_jellium_residuals(pars_l,npars,sl,cd,sfl,cfl,lsda_ref,sxc_lsda,gxc_lsda,all_stats=False):

    surf_err = []
    clus_err = []
    gam_err = []
    res = np.zeros(0)
    if settings.ncore > 1 and npars > 1:
        pool = mproc.Pool(processes=min(settings.ncore,npars))
        to_do = []
        for par in pars_l:
            to_do.append([par,lsda_ref,sxc_lsda,gxc_lsda,sl,cd,sfl,cfl,all_stats])
        olist = pool.starmap(get_jerr_list,to_do)
        pool.close()
        for tlist in olist:
            surf_err.append(tlist[0])
            clus_err.append(tlist[1])
            gam_err.append(tlist[2])
            res = np.append(res,tlist[3])
    else:
        def wrap_errs(fpars,all_stats):
            return get_jerr_list(fpars,lsda_ref,sxc_lsda,gxc_lsda,sl,cd,sfl,cfl,all_stats)
        for pars in pars_l:
            tlist = wrap_errs(pars,all_stats)
            surf_err.append(tlist[0])
            clus_err.append(tlist[1])
            gam_err.append(tlist[2])
            res = np.append(res,tlist[3])
    return np.asarray(surf_err),np.asarray(clus_err),np.asarray(gam_err),res

def get_dmc_ref_vals(use_correction=False):

    dmc_ref = jellium_cluster_dmc_ref
    sigma_xc = {}
    _,cd,_,_,_,sxc_lsda,gxc_lsda = init_jellium_calc()

    sigma_d = {}
    gamma_d = {}
    for rs in dmc_ref:
        nnval = len(cd[rs])
        #exc_lsda = lsda_ref[rs]+eps_no_xc[rs]
        ediff = np.zeros(nnval)
        n_l = np.zeros(nnval)
        eps_unif_diff = eps_c_dmc_unp(rs,param='PW')-ec_pw92(0.0,rs)
        for ind,fl in enumerate(cd[rs]):
            nelec = fl[0]
            n_l[ind] = nelec
            ref = dmc_ref[rs][nelec]/Eh_to_eV
            eps_c_unif_correc = eps_unif_diff
            if use_correction:
                eps_c_unif_correc += eps_unif_diff*(1.0 - (2.0/nelec)**(1.0/3.0))
                """
                    Correction factor eps_c_unif_correc (to ameliorate the DMC fixed
                     node error as N grows large) suggested by [T2008]
                """
            ediff[ind] = (ref-eps_c_unif_correc)*nelec#(ref - exc_lsda[ind] - eps_no_xc[rs][ind]) - (eps_unif_diff+eps_c_unif_correc)*nelec
        sigma_d[rs],gamma_d[rs]=liquid_drop_model(rs,n_l,ediff,sxc_lsda,gxc_lsda)
    return sigma_d,gamma_d


if __name__ == "__main__":

    #print(fit_lsd_gamma_xc())
    #exit()
    #for rs in [2.07,2.65,3.24,3.93,5.62]:
    #    print(rs,gamma_xc_lsd_ref(rs))
    #exit()

    sig_xc_d,gamma_xc_d=get_dmc_ref_vals(use_correction=False)
    for rs in sig_xc_d:
        print(rs,sigma_xc_reference(rs,pars='DMC'),sig_xc_d[rs],gamma_xc_d[rs],gamma_xc_d[rs]-gamma_xc_lsd_ref(rs))
    exit()

    se,ce,res=get_jellium_residuals([[0.1,1.0,2.0,3.0],[0.9,2.0,3.0,0.1],[0.8,3.0,0.1,1.1],[0.7,0.1,1.0,2.0]],4)
    print(res)
    for iset in range(len(se)):
        for row in se[iset]:
            print(row)
        print(ce[iset])
    exit()
    """
    sl,cl=source_data_files()
    nl = []
    cut =[]
    for ifl,fl in enumerate(cl):
        nelec = settings.jellium_cluster_rs[4][ifl]
        nl.append(nelec)
        rc = 4*nelec**(1.0/3.0)
        dat = np.genfromtxt(fl,delimiter=',',skip_header=1)
        ind=np.argmin(np.abs(dat[:,3]-1.e-14))
        cut.append(dat[ind,0]/rc)
        #print(nelec,dat[ind,0]/rc,dat[ind,3])

    b,m,r =lst_sq_lin(np.log(np.asarray(nl)),1.0/np.asarray(cut))
    print(m,b,r)
    for inn,an in enumerate(nl):
        print(an,cut[inn],1.05/(m*np.log(an)+b))
    import matplotlib.pyplot as plt
    plt.scatter(nl,cut)
    nn = np.linspace(min(nl),max(nl),2000)
    plt.plot(nn,1.05/(m*np.log(nn)+b))
    plt.show()
    exit()
    """
    for rs in settings.jellium_surface_rs:
        print(sigma_xc_reference(rs))
