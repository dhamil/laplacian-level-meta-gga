import numpy as np

"""
  Copy of datasets downloaded from http://www2.unb.ca/~ajit/download.htm

  See, in particular,
  Toshikatsu Koga, K. Kanayama, S. Watanabe, and Ajit J. Thakkar,
  Int. J. Quantum Chem. 71, 491 (1991)
  https://doi.org/10.1002/(SICI)1097-461X(1999)71:6<491::AID-QUA6>3.0.CO;2-T

  and other citations in k99light/HFwfn.bib
"""

def k99_dataset_reader(atom):

    shells = {
    'K': {'1s': 2}, 'L': {'2s': 2, '2p': 6},
    'M': {'3s': 2, '3p': 6, '3d': 10},
    'N': {'4s': 2, '4p': 6, '4d': 10, '4f': 14}
    }
    max_occ = {'s': 1, 'p': 3, 'd': 5, 'f': 7}
    config = {}

    zeta_d = {}
    bas_d = {}
    save_orbs = False

    tmpfl = open('./atomic_densities/k99light/neutral/'+atom.lower(),'r')
    for iln,lin in enumerate(tmpfl):
        if iln == 0:
            tmp = lin.strip().split()
            nunpaired = int(tmp[2][0]) - 1
            if tmp[2][1] == 'S':
                symm = 's'
            else:
                symm = 'n'
            config_str = tmp[1].split(')')
            for shell_str in config_str:
                if shell_str[0] in ['1','2','3','4','5','6','7','8','9']:
                    config[shell_str[:2].lower()] = int(shell_str[3:])
                else:
                    if shell_str[0] in shells:
                        wshell = shells[shell_str[0]]
                        for tconf in wshell:
                            config[tconf] =     wshell[tconf]
            nelec = 0
            for tshell in config:
                nelec += config[tshell]
        elif iln == 2:
            tmp = lin.strip().split()
            ke = float(tmp[2])
        elif iln >= 4:
            tmp = lin.strip().split()
            if len(tmp) == 0:
                break
            if tmp[0] in ['BASIS/ORB.ENERGY','CUSP']:
                continue
            elif tmp[0].lower() in ['s','p','d','f']:
                if save_orbs:
                    for orb in orbd:
                        nshell = config[orb]
                        if nshell == 2*max_occ[orb[1]]:
                            nup = max_occ[orb[1]]
                            ndn = nup
                        else:
                            if nunpaired > max_occ[orb[1]]:
                                nup = min(max_occ[orb[1]],nshell)
                                ndn = max(0,nshell - nup)
                            else:
                                navail = (nshell - nunpaired)//2
                                nup = navail + nunpaired
                                ndn = navail
                            nunpaired -= (nup - ndn)
                        tmpl = [nup,ndn]
                        for c in orbd[orb]:
                            tmpl.append(c)
                        bas_d[orb_char].append(tmpl)
                    save_orbs = False

                orb_char = tmp[0].lower()
                orbs = [x.lower() for x in tmp[1:]]
                norb = len(orbs)
                zeta_d[orb_char] = []
                bas_d[orb_char] = []
                orbd = {}
                for orb in orbs:
                    orbd[orb] = []
                continue
            save_orbs = True
            pqn = int(tmp[0][0])
            tz = float(tmp[1])
            zeta_d[orb_char].append([pqn,tz])
            for iorb in range(norb):
                worb = orbs[iorb]
                orbd[worb].append(float(tmp[2+iorb]))

    tmpfl.close()
    for orb in orbd:
        nshell = config[orb]
        if nshell == 2*max_occ[orb[1]]:
            nup = max_occ[orb[1]]
            ndn = nup
        else:
            if nunpaired > max_occ[orb[1]]:
                nup = min(max_occ[orb[1]],nshell)
                ndn = max(0,nshell - nup)
            else:
                navail = (nshell - nunpaired)//2
                nup = navail + nunpaired
                ndn = navail
        tmpl = [nup,ndn]
        for c in orbd[orb]:
            tmpl.append(c)
        bas_d[orb_char].append(tmpl)

    prop = [nelec, ke, symm]

    return bas_d, zeta_d, prop


if __name__=="__main__":

    k99_dataset_reader('Li')
