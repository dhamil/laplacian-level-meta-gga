from __future__ import print_function
import numpy as np
from math import pi,ceil
import matplotlib.pyplot as plt

from atomic_densities.orbs import gauss_legendre
from dft.ex import ex_unif as ex_lda
from dft.ec import ec_pw92
"""
    Adapted from D. Lee, L.A. Constantin, J.P. Perdew, and K. Burke,
    ``Condition on the Kohn–Sham kinetic energy and modern parametrization of
     the Thomas–Fermi density'',
    J. Chem. Phys. 130, 034107 (2009)
    https://doi.org/10.1063/1.3059783
"""
def phimod(x):
    y = x**(0.5)
    bmaj=1.5880710226
    cfa = np.array([-bmaj,4.0/3.0,0.0,-2.0*bmaj/5.0,1.0/3.0,3.0*bmaj**2/70.0,-2.0*bmaj/15.0,2.0/27.0+bmaj**3/252.0])
    cfb = np.array([-0.0144050081,0.0231427314,-0.00617782965,0.0103191718,-0.000154797772,0.0,0.0,0.0])
    numr = 1.0
    denom = 1.0 + cfa[len(cfa)-1]*y**(15)/144.0

    dydx = 0.5/y
    d_numr_dy = 0.0
    d_denom_dy = 15*cfa[len(cfa)-1]*y**(14)/144.0

    for i in range(len(cfa)):
        numr += cfa[i]*y**(i+2)
        denom += cfb[i]*y**(i+10)

        d_numr_dy += (i+2)*cfa[i]*y**(i+1)
        d_denom_dy += (i+10)*cfb[i]*y**(i+9)

    return numr/denom,(d_numr_dy/denom -numr*d_denom_dy/denom**2)*dydx

def n_tf(x,z):
    ac = (0.75*pi)**(2.0/3.0)/2.0
    fm,dfm = phimod(z**(1.0/3.0)*x/ac)
    fm = np.maximum(fm,np.zeros(x.shape))
    dfm *= z**(1.0/3.0)/ac
    return (z*fm/x/ac)**(1.5)/(4.0*pi),1.5*((z/ac)**3*fm/x)**(0.5)*(dfm/x - fm/x**2)/(4.0*pi)

def get_ex_approx(z,r,w):
    return np.sum(ex_lda(n_tf(r,z))*w)
    """
    oint = 1e20
    int = -1e20
    for i in range(2,9):
        r = np.linspace(1.e-8,100.0,10.0**i)
        rstep = r[1]-r[0]
        den = n_tf(r,z)
        exl = ex_lda(den)
        exl[0]/=2.0
        exl[len(exl)-1]/=2.0
        if i == 2:
            oint = 4*pi*np.sum(exl*r**2)*rstep
        else:
            int = 4*pi*np.sum(exl*r**2)*rstep
        if abs(int - oint)/abs(oint) < 1.e-4:
            return int
        else:
            oint = int
        """
def get_ec_approx(z):
    oint = 1e20
    int = -1e20
    for i in range(2,9):
        r = np.linspace(1.e-8,100.0,10.0**i)
        rstep = r[1]-r[0]
        den = n_tf(r,z)
        rs = (3.0/(4.0*pi*den))**(1.0/3.0)
        exl = ec_pw92(0.0,rs)*den
        exl[0]/=2.0
        exl[len(exl)-1]/=2.0
        if i == 2:
            oint = 4*pi*np.sum(exl*r**2)*rstep
        else:
            int = 4*pi*np.sum(exl*r**2)*rstep
        if abs(int - oint)/abs(oint) < 1.e-4:
            return int
        else:
            oint = int

def get_lda_tf():

    #[2.0**i for i in range(5,20)]
    #z_l = np.concatenate((np.array([2.0**i for i in range(5,20)]),np.array([3.0**i for i in range(4,20)]),np.array([4.0**i for i in range(3,20)])))
    z_l = np.arange(100.0,1000.0,10.0)#np.asarray([100.0 + i for i in range(100)])
    r,w,_,_ = gauss_legendre(0.0,1.e2,1000,gen=True)
    r2,w2,_,_ = gauss_legendre(0.0,1.e2,5000,gen=True)
    exlda = []
    eclda = []
    with open('lda_tf.csv','w+') as out:
        out.write('Z,Ex_LDA,Ec_LDA,int Z error (%)\n')
        for zz in z_l:
            rr = r
            ww = w
            if zz > 10.0**5:
                rr = r2
                ww = w2
            den,gden = n_tf(rr,zz)
            zerr = (np.sum(den*ww)-zz)/zz*100
            ext = np.sum(ex_lda(den)*ww)
            rs = (3.0/(4.0*pi*den))**(1.0/3.0)
            ect = np.sum(ec_pw92(0.0,rs)*den*ww)
            exlda.append(ext)
            eclda.append(ect)
            out.write(('{:},{:},{:},{:}\n').format(zz,ext,ect,zerr))
    return z_l,np.array(exlda),np.array(eclda)

def moment(j,p,adj_grid=False,lda_bc=False):

    if adj_grid:
        dev = 1e20
        bint = 0.0
        for lvl in range(1,100):
            r,w,_,_ = gauss_legendre(0.0,1.e2,lvl*10,gen=True)
            fm,_ = phimod(r)
            int = np.sum(w*(fm/r)**(1.5))/(4*pi)
            if abs(int-1.0) < dev:
                dev = abs(int-1.0)
                bint = int
                cl = lvl*10
    else:
        cl = 60
    r,w,_,_ = gauss_legendre(0.0,1.e2,cl,gen=True)
    fm,_ = phimod(r)
    if lda_bc:
        c0 = (1.0 - np.log(2.0))/pi**2
        c1 = 0.046644
        t = (fm/r)**(1.5)
        int = np.sum(t*np.log(t)*w)/(4*pi)
        bc = c0/3.0*(np.log(27*pi**2/128) -int) -c1
        return int,bc,2*c0/3.0
    else:
        return np.sum(w*r**(p-2)*(fm/r)**j)/(4*pi)

if __name__=="__main__":
    print(moment(1.5,2,adj_grid=False,lda_bc=True))
    print(moment(2,2,adj_grid=False)*(9/(2.0*pi**4))**(1.0/3.0))
