from __future__ import print_function # python2 and python3 compatible amigos
import numpy as np
from math import pi,ceil
from scipy.special import factorial
from os import path,system

base = './rhf_atomic_densities/'

# format of the atom catalog (dictionary):
# 'Atom': [orb types] e.g. [[s list], [p list], [d list],...]
# orb list = [[ list of zetas],[basis expansions]]
# [list of zetas] = [ [princpal q #, ang mom q #, zeta1, zeta2, ...], ...]
# [basis expansions] = [[occ. up 1, occ. down 1,coeff 1, coeff 2,...],...]
# these are coefficients for Slater type orbitals
atcat = { 'H': [ [[[1,0,1.0]], [[1,0,1.0]]] ],
'He': [[[[1,0,1.41714,2.37682,4.39628,6.52699,7.94252]],[[1,1,0.76838,0.22346,0.04082,-0.00994,0.00230]]]],
'Li': [[[[1,0,2.47673,4.69873],[2,0,0.38350,0.66055,1.07000,1.63200]],[[1,1,0.89786, 0.11131,-0.00008, 0.00112,-0.00216,0.00884],[1,0,-0.14629,-0.01516,0.00377,0.98053,0.10971,-0.11021]]]],
'Be': [ [[[1,0,3.47116,6.36861],[2,0,0.77820,0.94067,1.48725,2.71830]], [[1,1,0.91796,0.08724,0.00108,-0.00199,0.00176,0.00628],[1,1,-.17092,-.01455,.21186,.62499,.26662,-.09919]]] ],
'B' : [ [[[1,0,4.44561,7.91796],[2,0,0.86709,1.21924,2.07264,3.44332]], [[1,1,0.92705,0.0778,0.00088,-0.002,.00433,.0027],[1,1,-.19484,-.01254,.06941,.75234,.31856,-.12642] ] ], # s orbs
[ [[2,1,0.87481,1.36992,2.32262,5.59481]], [[1,0,0.53622,.4034,.11653,.00821]] ] # p orbs
],
'C' : [
[ [[1,0,5.43599,9.48256],[2,0,1.05749,1.52427,2.68435,4.20096]], [[1,1,.93262,.06931,.00083,-.00176,.00559,.00382], [1,1,-.20814,-.01071,.08099,.75045,.33549,-.14765 ] ] ], # s orbs
[ [[2,1,.98073,1.44361,2.60051,6.51003]], [[2,0,.28241,.54697,.23195,.01025 ]] ] # p orbs
],
'N' : [
[ [[1,0,6.45739,11.172],[2,0,1.36405,1.89734,3.25291,5.08238] ], [[1,1,.9378,.05849,.00093,-.0017,.00574,.00957],[1,1,-.21677,-.00846,.17991,.67416,.31297,-.14497]] ], # s orbs
[ [[2,1,1.16068,1.70472,3.03935,7.17482]], [[3,0,.26639,.52319,.27353,.01292]] ] # p orbs
],
'O': [
[ [[1,0,7.61413,13.7574],[2,0,1.69824,2.48022,4.31196,5.86596]], [[1,1,.94516,.03391,-.00034,.00241,-.00486,.03681],[1,1,-.22157,-.00476,.34844,.60807,.25365,-.19183] ] ], # s orbs
[ [[2,1,1.14394,1.8173,3.44988,7.56484]],[[3,1,.16922,.57974,.32352,.0166]] ] # p orbs
],
'F': [
[ [[1,0,8.5576,14.9766],[2,0,1.82142,2.67295,4.90066,6.57362]], [ [1,1,.9471,.03718,.00013,.00093,.00068,.02602],[1,1,-.22694,-.00530,.23918,.68592,.31489,-.21822 ]] ], # s orbs
[ [[2,1,1.2657,2.05803,3.92853,8.20412]], [[3,2,.1783,.56185,.33658,.01903]] ] # p orbs
],
'Ne' : [
[[[1,0,9.48486,15.56590],[2,0,1.96184,2.86423,4.82530,7.79242]],[[1,1,0.93717,0.04899,0.00058,-0.00064,0.00551,0.01999],[1,1,-0.23093,-0.00635,0.18620,0.66899,0.30910,-0.13871]]], # s orbs
[[[2,1,1.45208,2.38168,4.48489,9.13464]],[[3,3,0.21799,0.53338,0.32933,0.01872]]]],  # p orbs
'Ar' : [
[[[1,0,18.01640],[3,0,22.04650,16.08250,11.63570,7.70365,4.87338,3.32987,2.02791]], [[1,1,0.97349,0.01684,0.02422,-0.00114,0.00123,-0.00039,0.00010,-0.00003],[1,1,0.27635,0.00289,-0.03241,-0.33229,-0.65828,-0.06834,0.00623,-0.00174],[1,1,0.08634,0.00186,-0.01540,-0.10236,-0.27614,-0.11879,0.68436,0.52050]]], # s orbs
[[[2,1,9.05477],[4,1,15.54410,12.39970,8.56120,5.94658,3.42459,1.96709,1.06717]],[[3,3,0.64116,0.00865,0.04186,0.31735,0.09642,0.00003,0.00055,-0.00013],[3,3,-0.17850,-0.00812,0.00520,-0.10986,0.10994,0.56149,0.46314,0.02951]]] # p orbs
],
'Kr' : [
[ [[1,0,32.83510,40.94470],[2,0,27.458,16.0666],[3,0,14.2962,9.10937,6.37181],[4,0,3.84546,2.57902,1.77192]],[ [1,1,0.71521,0.29911,-0.01854,.00897,-.00464,.00190,-.00088,.00026,-.00018,.00006], [1,1,.38139,-.01823,.17175,-1.07160,-.14913,-0.01920,0.00401,-0.00122,.00092,-.00031], [1,1,-.14543,.00181,-.09037,.49528,.25451,-.48504,-.75593,-.01203,.00218,-.00085], [1,1,-.04349,-.00148,-.03219,.16451,.08852,-.16671,-.33291,.46913,.55106,.13572] ]  ], # s orbs
[ [[2,1,17.03660,26.04380],[3,1,15.51,9.49403,6.57275],[4,1,5.38507,3.15603,2.02966,1.42733]], [ [3,3,.72322,.06774,.22056,.04478,-.01672,.00609,-.00195,.00111,-.0004],[3,3,.30185,.02508,.15903,-.28475,-.7644,-.1067,-.00562,.00137,-.00053],[3,3,.08488,.00571,.04169,-.07425,-.26866,.01341,.51241,.42557,.18141] ]  ],  # p orbs
[ [[3,2,5.3065,3.3624,7.94963,10.35430,17.11420]], [[5,5,.50854,.1107,.24778,.20584,.02863]]  ]  # d orbs
],
'Xe' : [
[ [[1,0,55.3072,37.8073],[2,0,27.9297,23.6921],[3,0,15.0353,12.6723],[4,0,7.60195,5.73899],[5,0,4.17583,2.99772,1.98532]], [ [1,1,.87059,.14926,-.06259,.04643,-.01383,.0103,-.00254,.00201,-.00085,.00045,-.00011], [1,1,.02107,.51209,-.01873,-1.18386,-.06502,.03432,-.00469,.00352,-.00136,.00069,-.00017], [1,1,-.00868,-.23044,-.38195,1.12481,.23955,-1.41092,-.06111,.02591,-.00759,.00353,-.00076], [1,1,.00237,.10784,.19149,-.54498,-.35456,1.13006,-.63451,-.58291,-.02272,.00218,-.00092], [1,1,.0007,.03815,.06768,-.19267,-.15274,.44776,-.30543,-.24664,.27675,.59862,.30408] ]  ], # s orbs
[ [[2,1,34.8844,23.3047],[3,1,12.5412,12.023],[4,1,7.7239,5.40562],[5,1,3.32661,2.09341,1.36686]], [[3,3,.13527,.86575,.11362,-.09833,.00123,-.00028,-.00003,.00004,-.00002], [3,3,.02765,.49883,-.48416,-.61656,-.05986,.01605,-.00407,.00238,-.00087], [3,3,-.00908,-.22945,-.34216,1.02476,-.53369,-.67016,-.02313,.00433,-.00136], [3,3,-.00277,-.07054,-.18148,.40692,-.22741,-.21144,.49354,.53529,.13666]]  ], # p orbs
[ [[3,2,20.0824,11.786],[4,2,7.30842,4.884,3.1985]], [[5,5,-.19493,-.80743,-.0683,.02129,-.00536], [5,5,-.08265,-.3486,.40928,.59391,.14481] ]  ], # d orbs
]
}

# format of atom properties dictionary:
# atom : [# electrons, KE energy density, 's' or 'n' ]
# 's' means use spherically-averaged density
# 'n' means non-spherical, use polar angle dependence in spherical harmonic

atprop = {'H':[1.0,0.5,'s'], 'He' : [2.0,2.8617128,'s'], 'Li' : [3.0, 7.4327544,'s'], 'Be': [4.0,14.573036,'s'], 'B': [5.0,24.528956,'n'], 'C': [6.0,37.688357,'n'],'N': [7.0,54.400522,'n'],'O':[8.0,74.809346,'n'],'F':[9.0,99.40914,'n'], 'Ne' : [10.0,128.54681,'s'], 'Ar' : [18.0, 526.8138,'s'], 'Kr' : [36.0, 2752.0481,'s'], 'Xe' : [54.0, 7232.047,'s']
}

def ex_unif(d): # LDA exchange energy
    return -(3.0/(4.0*pi))*(3*pi**2)**(1.0/3.0)*d**(4.0/3.0)

def stor(r,n,zeta): # r is numpy array, n and zeta are scalars; generates Slater-type orbitals
    norm = (2.0*zeta)**(n+0.5)/(1.0*factorial(2*n))**(0.5)
    chi = norm*r**(n-1.0)*np.exp(-zeta*r)
    chi_grad = ((n-1.0)/r - zeta)*chi
    chi_lap = (n*(n-1.0)/r**2 - 2*zeta*n/r + zeta**2)*chi
    return chi, chi_grad, chi_lap

def sphavgharm(l): # NB, density and all other variables are spherically-averaged
    if l == 0: # when using this function
        return 1.0/(4*pi)**(0.5),0.0,0.0
    elif l == 1:
        cons = (3.0/(4.0*pi))**(0.5)
        return  1.0/(4*pi)**(0.5),1.0/(2*pi)**(0.5),-2.0/(4*pi)**(0.5)
    elif l == 2:
        cons = (5.0/(16*pi))**(0.5)
        return 1.0/(4*pi)**(0.5),(3.0/(2*pi))**(0.5),-6.0/(4*pi)**(0.5)

def sphharm(l,th): # explicit angular dependence
    if l == 0: # needed for open shell atoms
        return 1.0/(4*pi)**(0.5),0.0,0.0
    elif l == 1:
        cons = (3.0/(4.0*pi))**(0.5)
        return  cons*th, -cons*(1.0-th**2)**(0.5),-2*cons*th
    elif l == 2:
        cons = (5.0/(16*pi))**(0.5)
        return cons*(3*th**2 - 1.0),-cons*6*th*(1.0-th**2)**(0.5),-6*cons*(3*th**2 - 1.0)


def densvars(r,th,atom,ang=False): # generates spin density variables (n_s, |nabla n_s|, tau_s, nabla**2 n_s)
    dnu = np.zeros(r.shape) # from STO orbitals
    dnd = np.zeros(r.shape)
    gnu = [np.zeros(r.shape),np.zeros(r.shape)]
    gnd = [np.zeros(r.shape),np.zeros(r.shape)]
    tauu = np.zeros(r.shape)
    taud = np.zeros(r.shape)
    lnu = np.zeros(r.shape)
    lnd = np.zeros(r.shape)

    for shell in atcat[atom]:
        [z_l, bs_l] = shell
        phi = []
        gphir = []
        gphith = []
        lphi = []
        for iz,zz in enumerate(z_l):
            for jcz in range(2,len(zz)):
                t,tg,tl = stor(r,zz[0],zz[jcz])
                if ang:
                    ta,tag,tal = sphharm(zz[1],th)
                else:
                    ta,tag,tal = sphavgharm(zz[1])
                phi.append(t*ta)
                gphir.append(tg*ta)
                gphith.append(t*tag/r)
                lphi.append(tl*ta + t*tal/r**2)
        for ib,bb in enumerate(bs_l):
            tphi = np.sum(np.array([bb[iv+2]*np.array(v) for iv,v in enumerate(phi)]),axis = 0)
            tgphir = np.sum(np.array([bb[iv+2]*np.array(v) for iv,v in enumerate(gphir)]),axis = 0)
            tgphith= np.sum(np.array([bb[iv+2]*np.array(v) for iv,v in enumerate(gphith)]),axis = 0)
            tlphi = np.sum(np.array([bb[iv+2]*np.array(v) for iv,v in enumerate(lphi)]),axis = 0)

            dnu += bb[0]*tphi**2 # spin densities
            dnd += bb[1]*tphi**2

            gnu[0] += 2*bb[0]*tphi*tgphir # radial part of gradient of density
            gnd[0] += 2*bb[1]*tphi*tgphir
            if ang:
                gnu[1] += 2*bb[0]*tphi*tgphith # polar angular part of gradient of spin densities
                gnd[1] += 2*bb[1]*tphi*tgphith
            tauu += bb[0]*(tgphir**2+tgphith**2)/2.0  # kinetic energy spin densities, non-negative
            taud += bb[1]*(tgphir**2+tgphith**2)/2.0
            lnu += 2*bb[0]*(tgphir**2 + tgphith**2 + tphi*tlphi)  # laplacian of spin densities
            lnd += 2*bb[1]*(tgphir**2 + tgphith**2 + tphi*tlphi)

    #gu = (gnu[0]**2 + gnu[1]**2)**(0.5)
    #gd = (gnd[0]**2 + gnd[1]**2)**(0.5)
    #gabs = (gu**2 + gd**2 + 2*(gnu[0]*gnd[0]+gnu[1]*gnd[1]))**(0.5)
    return dnu,dnd, gnu, gnd, tauu,taud, lnu,lnd

def gauss_legendre(a,b,lvl,ang=False): # generates integration mesh and weights using Gauss-Legendre quadrature

     # algorithm from Golub and Welsch, Math. Comp. 23, 221 (1969)
    def bet(n):# coefficients from NIST's DLMF, sec. 18.9
        an = (2*n+1.0)/(n+1.0)
        anp1 = (2*n+3.0)/(n+2.0)
        c = (n+1.0)/(n+2.0)
        return (c/an/anp1)**(0.5)
    jac = np.zeros((lvl,lvl))
    jac[0,1] = bet(0)
    jac[lvl-1,lvl-2] = bet(lvl-2)
    for jn in range(1,lvl-1):
        jac[jn,jn+1] = bet(jn)
        jac[jn,jn-1] = bet(jn-1)

    if not path.isfile('./grids/gleg_'+str(lvl)+'.csv') or path.getsize('./grids/gleg_'+str(lvl)+'.csv') == 0:
        grid,v = np.linalg.eigh(jac)
        wg = 2*v[0]**2
        np.savetxt('gleg_'+str(lvl)+'.csv',np.transpose((grid,wg)),delimiter=',',header='point, weight')
    else:
        grid,wg = np.transpose(np.genfromtxt('gleg_'+lvl+'.csv',delimiter=',',skip_header=1))

    rr = grid*(b-a)/2.0+(b+a)/2.0

    if ang:
        rw = 2*pi*wg*(b-a)/2.0*rr**2
        rr,th = np.meshgrid(rr,grid)
        tw = wg
    else:
        rw = 4*pi*wg*(b-a)/2.0*rr**2
        rr,th = np.meshgrid(rr,np.ones(1))
        tw = np.ones(1)

    return rr,rw,th,tw

def write_dens(at,do_all=False):
    rmin = 0.0
    if do_all:
        atoms = [i for i in atcat]
    else:
        atoms = at
    for anat in atoms:
        nn = 0.0
        ts = 0.0
        ln = 0.0
        print('-----------------------------------')
        print('Writing density variables for ',anat)
        with open(base+anat+'.csv','w+') as dout:
            rmax = 100#*atprop[anat][0]**(0.5)
            dout.write(('{:},'*9+'{:}\n').format('Weight','n up', 'n down', '|grad n up|', '|grad n down|', '|grad n|','tau up', 'tau down','lap n up','lap n down'))
            #if atprop[anat][2] == 'n':
            #    ngrpt = 1000#*ceil(atprop[anat][0]**(0.5))
            #    anggr = True
            #elif atprop[anat][2] == 's':
            ngrpt = 1000
            anggr = True
            rgr,wgt,th,twgt = gauss_legendre(rmin,rmax,ngrpt,ang=anggr)
            du,dd,gnu,gnd,tu,td,lu,ld = densvars(rgr,th,anat,ang=anggr)
            if atprop[anat][2] == 's':
                du = np.asarray([np.matmul(twgt,du)/2.0])
                dd = np.asarray([np.matmul(twgt,dd)/2.0])
                gnu[0] = np.asarray([np.matmul(twgt,gnu[0])/2.0])
                gnd[0] = np.asarray([np.matmul(twgt,gnd[0])/2.0])
                gnu[1] = np.asarray([np.matmul(twgt,gnu[1])/2.0])
                gnd[1] = np.asarray([np.matmul(twgt,gnd[1])/2.0])
                tu = np.asarray([np.matmul(twgt,tu)/2.0])
                td = np.asarray([np.matmul(twgt,td)/2.0])
                lu = np.asarray([np.matmul(twgt,lu)/2.0])
                ld = np.asarray([np.matmul(twgt,ld)/2.0])
                twgt = 2*np.ones(1)
            gdu = (gnu[0]**2 + gnu[1]**2)**(0.5)
            gdd = (gnd[0]**2 + gnd[1]**2)**(0.5)
            gt = (gdu**2 + gdd**2 + 2*(gnu[0]*gnd[0]+gnu[1]*gnd[1]))**(0.5)
            nn += np.sum(twgt*np.sum(wgt*(du+dd),axis=1),axis=0)
            ts += np.sum(twgt*np.sum(wgt*(tu+td),axis=1),axis=0)
            ln += np.sum(twgt*np.sum(wgt*(lu+ld),axis=1),axis=0)
            for iwg,wg in enumerate(wgt):
                for itwg,twg in enumerate(twgt):
                    if du[itwg,iwg] + dd[itwg,iwg] > 1.e-14:
                        dout.write(('{:},'*9+'{:}\n').format(wg*twg,du[itwg,iwg],dd[itwg,iwg],gdu[itwg,iwg],gdd[itwg,iwg],gt[itwg,iwg],tu[itwg,iwg],td[itwg,iwg],lu[itwg,iwg],ld[itwg,iwg]))
            print(('N = {:.4f} (error {:.4f}%)').format(nn,100*(nn-atprop[anat][0])/atprop[anat][0]))
            print(('Ts = {:.4f} (error {:.4f}%)').format(ts,100*(ts-atprop[anat][1])/atprop[anat][1]))
            print(('Int(lap n) = {:.4f} (should be zero)').format(ln))
            print('-----------------------------------')

    return

if __name__ == '__main__':

    for dependency in [base,'./grids']:
        if not path.isdir(dependency):
            system('mkdir '+dependency)
    write_dens(['He','Ne','Ar','Kr','Xe'])
    exit()
    write_dens([],do_all=True)
    exit()
    write_dens([],do_all=True)
    with open('lda_asy.csv','w+') as out:
        out.write('Z,Ex_LDA/Z**(5/3)\n')
        for at in atcat:
            wg,nu,nd,gnu,gnd,tu,td,lnu,lnd = np.transpose(np.genfromtxt(at+'.csv',delimiter=',',dtype=None,skip_header=1))
            ex = np.sum(wg*ex_unif(nu+nd))
            out.write(('{:},{:}\n').format(atprop[at][0],ex/atprop[at][0]**(5.0/3.0)))
