from __future__ import print_function # python2 and python3 compatible amigos
import numpy as np
from math import pi,ceil
from scipy.special import factorial
from scipy.linalg import eigh_tridiagonal as eigtri
from os import system,path

from atomic_densities.data_sets import atcat,atprop

out_dir = './data_files/'
for adir in [out_dir]:
    if not path.isdir(adir):
        system(('mkdir {:}').format(adir))

ml_d = {'s': 0.0, 'p': 1.0, 'd': 2.0, 'f': 3.0}

def ex_unif(d): # LDA exchange energy density
    return -(3.0/(4.0*pi))*(3*pi**2)**(1.0/3.0)*d**(4.0/3.0)

def sto(r,n,zeta): # r is numpy array, n and zeta are scalars; generates Slater-type orbitals
    norm = (2.0*zeta)**(n+0.5)/(1.0*factorial(2*n))**(0.5)
    chi = norm*r**(n-1.0)*np.exp(-zeta*r)
    chi_grad = ((n-1.0)/r - zeta)*chi
    chi_lap = (n*(n-1.0)/r**2 - 2*zeta*n/r + zeta**2)*chi
    return chi, chi_grad, chi_lap

def stok(q,n,zeta,l):
    """
        [BT1989]
    """
    norm = (2.0*zeta)**(n+0.5)/(1.0*factorial(2*n))**(0.5)
    def omega_sum(): # Eq. 31
        fac = -1.0/(4.0*zeta**2)
        sum = 0.0
        for k in range(int(np.floor((n-l)/2.0))+1):
            if k == 0:
                omega = (1.0*factorial(n))/(1.0*factorial(n-l))
            else:
                omega *= fac*(n-l-2*k+1.0)*(n-l-2*k+2.0)/(k*(n-k+1.0))
            sum += omega/(q**2 + zeta**2)**(n+1-k)
        return sum

    chiq = 2.0**(n-1)*factorial(n-l)/pi**2*(1.j*q)**l
    chiq *= zeta**(n-l)*norm
    chiq *= omega_sum() # Eq. 30
    return chiq/(4.0*pi)**(0.5)

def sph_avg_harm(l): # NB, density and all other variables are spherically-averaged
    if l == 0: # when using this function
        return 1.0/(4*pi)**(0.5),0.0,0.0
    elif l == 1:
        cons = (3.0/(4.0*pi))**(0.5)
        return  1.0/(4*pi)**(0.5),1.0/(2*pi)**(0.5),-2.0/(4*pi)**(0.5)
    elif l == 2:
        cons = (5.0/(16*pi))**(0.5)
        return 1.0/(4*pi)**(0.5),(3.0/(2*pi))**(0.5),-6.0/(4*pi)**(0.5)

def sph_harm(l,th): # explicit angular dependence
    if l == 0: # needed for open shell atoms
        return 1.0/(4*pi)**(0.5),0.0,0.0
    elif l == 1:
        cons = (3.0/(4.0*pi))**(0.5)
        return  cons*th, -cons*(1.0-th**2)**(0.5),-2*cons*th
    elif l == 2:
        cons = (5.0/(16*pi))**(0.5)
        return cons*(3*th**2 - 1.0),-cons*6*th*(1.0-th**2)**(0.5),-6*cons*(3*th**2 - 1.0)

def nshell(atom,dset):
    nshell = 0
    z_d,b_d = atcat(atom,wset=dset)
    for ashell in ml_d:
        nshell += len(b_d[ashell])
    return nshell

def generate_orbital_ffts(atom,dset,shell_by_shell=False):

    hbc = 1.973269804 # keV.Angstrom, NIST CODATA 2018
    bohr_to_ang = 0.529177210903 # also NIST CODATA 2018
    pscl = hbc/bohr_to_ang # need p in keV/c

    kmin = 0.0
    kmax = 300/pscl
    ngrpt = 10000
    k_l = np.linspace(kmin,kmax,ngrpt)
    p = pscl*k_l
    p_wg = (p[1]-p[0])*np.ones(p.shape) # trapezoidal weights
    p_wg[0] *= 0.5
    p_wg[1] *= 0.5

    n_unp = np.zeros(p.shape)
    n_pol = np.zeros(p.shape)

    if shell_by_shell:
        nshells = nshell(atom,dset)
        shell_data = np.zeros((p.shape[0],nshells),dtype='complex')
        shell_data[:,0] = p
        shell_data[:,1] = p_wg
        aux_header='p (keV/c), p weight'
        ishell = -1

    z_d,bs_d = atcat(atom,dset)

    for shell in z_d:
        z_l = z_d[shell]
        bs_l = bs_d[shell]
        phi = []
        for iz,zz in enumerate(z_l):
            for jcz in range(1,len(zz)):
                t = stok(k_l,zz[0],zz[jcz],ml_d[shell])
                phi.append(t)
        for ib,bb in enumerate(bs_l):
            tphi = np.sum(np.array([bb[iv+2]*np.array(v) for iv,v in enumerate(phi)]),axis = 0)

            if bb[0] >= bb[1]:
                occup_unp = 2*bb[1]
                occup_pol = bb[0]-bb[1]
            else:
                occup_unp = 2*bb[0]
                occup_pol = bb[1]-bb[0]

            n_unp += 4*pi*occup_unp*p**2*np.abs(tphi)**2 # add orbitals to momentum density
            n_pol += 4*pi*occup_pol*p**2*np.abs(tphi)**2
            if shell_by_shell:
                ishell+=1
                shell_data[:,ishell] = tphi
                aux_header += ', '+str(occup_unp)+':'+str(occup_pol)

    n_unp /= np.sum( p_wg*n_unp)
    n_pol /= np.sum( p_wg*n_pol)

    np.savetxt(out_dir+atom+'_'+dset+'_fft_sph.csv',np.transpose((p,n_unp,n_pol)),delimiter=',',header='p (keV/c),<n_unp(p)>,<n_pol(p)>')
    if shell_by_shell:
        with open(out_dir+atom+'_'+dset+'_shell_decomposition.csv','w+') as ofl:
            ofl.write(aux_header+'\n')
            for ip,ap in enumerate(p):
                ofl.write(('{:},'*(nshells+1)+'{:}\n').format(ap,p_wg[ip],*shell_data[ip,:]))

    def cdf(func):
        f2 = np.zeros(func.shape)
        for ind in range(func.shape[0]):
            f2[ind] = np.sum(func[:ind+1])
        return f2

    np.savetxt(out_dir+atom+'_'+dset+'_fft_sph_cdf.csv',np.transpose((p,cdf(p_wg*n_unp),cdf(p_wg*n_pol))),delimiter=',',header='p (keV/c),<n_unp(p)>,<n_pol(p)>')

    return


def densvars(r,th,atom,dset,ang=False):
    # generates spin density variables (n_s, |nabla n_s|, tau_s, nabla**2 n_s)
    # from STO orbitals
    dnu = np.zeros(r.shape)
    dnd = np.zeros(r.shape)
    # NB, splat the r shape to ensure this works regardless of the shape of r
    gnu = np.zeros((2,*r.shape))
    gnd = np.zeros((2,*r.shape))
    tauu = np.zeros(r.shape)
    taud = np.zeros(r.shape)
    lnu = np.zeros(r.shape)
    lnd = np.zeros(r.shape)

    z_d,bs_d = atcat(atom,dset)
    for shell in z_d:
        z_l = z_d[shell]
        nzt = 0
        for iz in range(len(z_l)):
            for jcz in range(1,len(z_l[iz])):
                nzt+=1
        bs_l = np.asarray(bs_d[shell])


        phi = np.zeros((nzt,*r.shape))
        gphir = np.zeros((nzt,*r.shape))
        gphith = np.zeros((nzt,*r.shape))
        lphi = np.zeros((nzt,*r.shape))
        kz = 0
        for iz,zz in enumerate(z_l):
            for jcz in range(1,len(zz)):
                t,tg,tl = sto(r,zz[0],zz[jcz])
                if ang:
                    ta,tag,tal = sph_harm(ml_d[shell],th)
                else:
                    ta,tag,tal = sph_avg_harm(ml_d[shell])
                """
                phi.append(t*ta)
                gphir.append(tg*ta)
                gphith.append(t*tag/r)
                lphi.append(tl*ta + t*tal/r**2)
                """
                phi[kz] = t*ta
                gphir[kz] = tg*ta
                gphith[kz] = t*tag/r
                lphi[kz] = tl*ta + t*tal/r**2
                kz += 1

        for ib,bb in enumerate(bs_l):

            """
            tphi = np.sum(np.array([bb[iv+2]*np.array(v) for iv,v in enumerate(phi)]),axis = 0)
            tgphir = np.sum(np.array([bb[iv+2]*np.array(v) for iv,v in enumerate(gphir)]),axis = 0)
            tgphith= np.sum(np.array([bb[iv+2]*np.array(v) for iv,v in enumerate(gphith)]),axis = 0)
            tlphi = np.sum(np.array([bb[iv+2]*np.array(v) for iv,v in enumerate(lphi)]),axis = 0)
            """
            tphi = np.einsum('i,ijk->jk',bb[2:],phi)
            tgphir = np.einsum('i,ijk->jk',bb[2:],gphir)
            tgphith = np.einsum('i,ijk->jk',bb[2:],gphith)
            tlphi = np.einsum('i,ijk->jk',bb[2:],lphi)

            dnu += bb[0]*tphi**2 # spin densities
            dnd += bb[1]*tphi**2

            gnu[0] += 2*bb[0]*tphi*tgphir # radial part of gradient of density
            gnd[0] += 2*bb[1]*tphi*tgphir
            if ang:
                gnu[1] += 2*bb[0]*tphi*tgphith # polar angular part of gradient of spin densities
                gnd[1] += 2*bb[1]*tphi*tgphith
            tauu += bb[0]*(tgphir**2+tgphith**2)/2.0  # kinetic energy spin densities, non-negative
            taud += bb[1]*(tgphir**2+tgphith**2)/2.0
            lnu += 2*bb[0]*(tgphir**2 + tgphith**2 + tphi*tlphi)  # laplacian of spin densities
            lnd += 2*bb[1]*(tgphir**2 + tgphith**2 + tphi*tlphi)

    return dnu,dnd, gnu, gnd, tauu,taud, lnu,lnd

def gauss_legendre(lvl):

    # generates integration mesh and weights using Gauss-Legendre quadrature
    """
        algorithm from [GW1969]
    """
    def bet(n):
        # coefficients from NIST's DLMF, sec. 18.9
        an = (2*n+1.0)/(n+1.0)
        anp1 = (2*n+3.0)/(n+2.0)
        cnp1 = (n+1.0)/(n+2.0)
        return (cnp1/an/anp1)**(0.5)

    a = np.zeros(lvl)
    b = bet(np.arange(0,lvl-1,1))
    grid,v = eigtri(a,b)
    wg = 2*v[0]**2

    return grid, wg

def gauss_legendre_r_grid(a,b,lvl,ang=False):

    grid, wg = gauss_legendre(lvl)

    if b == 'inf':
        cp = 0.1
        rmax = 4.0
        ngrid = 0.5*(grid+1.0)
        rl = rmax*ngrid
        rg = cp*(2.0/(1.0 + grid)-1.0)+rmax
        rr = np.append(rl,rg[::-1])
    else:
        rr = grid*(b-a)/2.0+(b+a)/2.0
    if ang:
        if b == 'inf':
            rw = 2*pi*wg*0.5*rmax*rl**2
            tmpwg = 2*pi*wg*2*cp/(1.0 + grid)**2*rg**2
            rw = np.append(rw,tmpwg[::-1])
        else:
            rw = 2*pi*wg*(b-a)/2.0*rr**2
        rr,th = np.meshgrid(rr,grid)
        tw = wg
    else:
        if b == 'inf':
            rw = 4*pi*wg*0.5*rmax*rl**2
            tmpwg = 4*pi*wg*2*cp/(1.0 + grid)**2*rg**2
            rw = np.append(rw,tmpwg[::-1])
        else:
            rw = 4*pi*wg*(b-a)/2.0*rr**2
        rr,th = np.meshgrid(rr,np.ones(1))
        tw = np.ones(1)
    return rr,rw,th,tw

def rect_r_grid(rmin,rmax,ngrpt,nang=1):
    # rectangular grid in r, Gauss-Legendre grid in theta
    r_1d, dr = np.linspace(rmin,rmax,ngrpt,retstep=True)
    if nang > 1:
        angfac = 2*pi
        th_1d,tw = gauss_legendre(nang)
    else:
        angfac = 4*pi
        th_1d = np.ones(1)
        tw = np.ones(1)
    rr,th = np.meshgrid(r_1d,th_1d)
    rw = angfac*r_1d**2*dr
    rw[[0,-1]] /= 2
    return rr,rw,th,tw

def write_dens(at,dset,do_all=False,save=True,rect_grid=False, anggr=True, \
    rmax = 50, filenames=[],nodmask=False):

    if do_all:
        atoms = atcat(None,dset,return_keys=True)
    else:
        atoms = at
    for iat,anat in enumerate(atoms):
        nn = 0.0
        ts = 0.0
        lan = 0.0
        props = atprop(anat,dset)
        print('-----------------------------------')

        print('Generating density variables for ',anat)
        if save:
            addn = ''
            if rect_grid:
                addn = '_rect'

            if len(filenames) > 0:
                fstr = filenames[iat]
            else:
                fstr = out_dir+anat+'_'+dset+addn+'.csv'
            dout = open(fstr,'w+')

        if rect_grid:
            dr = 1.e-2/props[0]
            rmin = 1.e-2/props[0]
            ngrpt = 20000#1 + int(np.ceil((rmax - rmin))/dr)
            if anggr:
                nangpt = 200
            else:
                nangpt = 1
            rgr,wgt,th,twgt = rect_r_grid(rmin,rmax,ngrpt,nang=nangpt)
        else:
            rmin = 0.0
            #rmax = 10
            ngrpt = 100
            rgr,wgt,th,twgt = gauss_legendre_r_grid(rmin,'inf',ngrpt,ang=anggr)
        du,dd,gnu,gnd,tu,td,lu,ld = densvars(rgr,th,anat,dset,ang=anggr)
        if props[2] == 's':
            du = np.asarray([np.matmul(twgt,du)/2.0])
            dd = np.asarray([np.matmul(twgt,dd)/2.0])
            gnu[0] = np.asarray([np.matmul(twgt,gnu[0])/2.0])
            gnd[0] = np.asarray([np.matmul(twgt,gnd[0])/2.0])
            gnu[1] = np.asarray([np.matmul(twgt,gnu[1])/2.0])
            gnd[1] = np.asarray([np.matmul(twgt,gnd[1])/2.0])
            tu = np.asarray([np.matmul(twgt,tu)/2.0])
            td = np.asarray([np.matmul(twgt,td)/2.0])
            lu = np.asarray([np.matmul(twgt,lu)/2.0])
            ld = np.asarray([np.matmul(twgt,ld)/2.0])
            twgt = 2*np.ones(1)
        gdu = (gnu[0]**2 + gnu[1]**2)**(0.5)
        gdd = (gnd[0]**2 + gnd[1]**2)**(0.5)
        gt = (gdu**2 + gdd**2 + 2*(gnu[0]*gnd[0]+gnu[1]*gnd[1]))**(0.5)
        nn += np.sum(twgt*np.sum(wgt*(du+dd),axis=1),axis=0)
        ts += np.sum(twgt*np.sum(wgt*(tu+td),axis=1),axis=0)
        lan += np.sum(twgt*np.sum(wgt*(lu+ld),axis=1),axis=0)
        if save:
            dout.write(('{:},'*10+'{:}\n').format('r', 'Weight','n up', 'n down', '|grad n up|', '|grad n down|', '|grad n|','tau up', 'tau down','lap n up','lap n down (all a.u.)'))
            for iwg,wg in enumerate(wgt):
                for itwg,twg in enumerate(twgt):
                    if du[itwg,iwg] + dd[itwg,iwg] > 1.e-14 or nodmask:
                        dout.write(('{:},'*10+'{:}\n').format(rgr[0,iwg],wg*twg,
                            du[itwg,iwg],dd[itwg,iwg],gdu[itwg,iwg],gdd[itwg,iwg],
                            gt[itwg,iwg],tu[itwg,iwg],td[itwg,iwg],lu[itwg,iwg],
                            ld[itwg,iwg]))
            dout.close()
        print(('N = {:.6f} (difference {:.6e})').format(nn,nn-props[0]))#100*(nn/props[0]-1.0)))
        print(('Ts = {:.6f} (difference {:.6e})').format(ts,ts-props[1]))#100*(ts/props[1]-1.0)))
        print(('Int(lap n) = {:.6e} (should be zero)').format(lan))
        print('-----------------------------------')

    return

if __name__ == '__main__':

    #write_dens(['Ne'],'BBB93')
    #exit()
    for dset in ['CR74','BBB93']:
        write_dens(['H','He','Ne','Ar','Kr','Xe'],dset)
    exit()
    for dset in ['CR74','BBB93']:
        generate_orbital_ffts('Fe',dset,shell_by_shell=True)
