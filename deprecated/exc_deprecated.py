        elif settings.GE4 == "CSQ":
            #c0=zps['p2']*2
            #gex = (1.0-zps['a4'])*(1.0 + c0)*uak*p/(1.0 + c0*np.exp(-(1.0 + c0)*p/zps['p4']))
            #cpp = (25.0/16.0*cge4+c0*uak*(1.0-zps['a4'])/zps['p4'])/(uak*zps['a4']) +1.0
            gex = (1.0-zps['a4'])*uak*p*(1.0 + p/zps['p2'])
            cpp = (25.0/16.0*cge4+ (1.0-zps['a4'])*uak/zps['p2'])/(uak*zps['a4'])+1.0
            gex += zps['a4']*uak*p*(1.0 + p)/(1.0 + cpp*p)
            gex += cge4*(q - 5.0*p/4.0)**2 + q**4/zps['p4']
            return (1.0 + kappa*np.tanh(gex/kappa))*ex_unif(d)
        elif settings.GE4 == "bare":
            k1 = kappa*zps['p2']
            k2 = kappa - k1
            cpp = 25.0/16.0*cge4/uak
            gex = (uak*p + p**4/zps['p4'])/(1.0 + cpp*p + p**2/zps['a4'])
            gex4 = cge4*(q - 5.0*p/4.0)**2
            delta = k1*np.tanh(gex/k1) + k2*np.tanh(gex4/k2)
            return (1.0 + delta)*ex_unif(d)
            #return (1.0 + kappa*np.tanh((gex+gex4)/kappa))*ex_unif(d)
        elif settings.GE4 == "REG" or settings.GE4 == "REGSQ":
            k1 = kappa*zps['p2']
            k2 = (kappa-k1)*zps['p4']
            k3 = kappa-k1-k2
            dcor = p**3/zps['a4']
            if settings.GE4 == "REG":
                gex4 = cge4*q*(q - 5.0*p/2.0)
            elif settings.GE4 == "REGSQ":
                gex4 = cge4*(q - 5.0*p/4.0)**2
                cpp = 25.0/16.0*cge4/uak
                gex = uak*p
                dcor -= cpp*p**2
            d2 = k1*np.tanh(gex/k1)
            d4 = k2*np.tanh(gex4/k2)
            dc = k3*np.tanh(dcor/k3)
            return (1.0 + d2 + d4 + dc)*ex_unif(d)
        elif settings.GE4 == "SREG":
            k1 = kappa*zps['p2']
            k2 = kappa-k1
            gex4 = cge4*(q - 5.0*p/4.0)**2
            cpp = 25.0/16.0*cge4/uak+1.0/zps['p4']
            gex = uak*p*(1.0 + p/zps['p4']+p**2/zps['a4'])/(1.0 + cpp*p)
            d2 = k1*np.tanh(gex/k1)
            d4 = k2*np.tanh(gex4/k2)
            return (1.0 + d2 + d4)*ex_unif(d)


def ex_modscan(d,gd,tau,zps):

    hx0 = 1.174
    a1 = 4.9479

    k1 = 0.065
    uak = 10.0/81.0
    a4 = 146.0/2025.0
    b2 = (5913.0/405000.0)**(0.5)
    b1 = (511.0/13500.0)/(2*b2)
    b4 = uak**2/k1 - 1606.0/18225.0-b1**2
    c1 = 2.0/3.0
    c2 = 0.8
    d1 = 1.24

    p = (gd/(2.0*(3*pi**2)**(1.0/3.0)*d**(4.0/3.0)))**2

    tauw = gd**2/(8.0*d)
    tauunif = 3.0/10.0*(3*pi**2)**(2.0/3.0)*d**(5.0/3.0)
    alpha = np.maximum((tau - tauw)/tauunif,np.zeros(tau.shape))

    q = 9.0/20.0*(alpha - 1.0) + 2.0/3.0*p

    if settings.GE4 == 'exp':
        xf = uak*p*(1.0 + zps['p2']*p*np.exp(-abs(zps['p2'])*p))
        xf += (zps['p4']*p + zps['a4']*(1.0 - alpha)*np.exp(-zps['ad']*(1.0-alpha)**2))**2
    elif settings.GE4 == 'tanh':
        #xf = uak*p + zps['p2']*np.tanh(p**2) + zps['a4']*np.tanh((1.0 - alpha)**2) + zps['p4']*np.tanh(p*(alpha-1.0))
        #xf = uak*p + zps['p2']*np.tanh( (zps['p4']*p + zps['a4']*(1.0-alpha))**2 + zps['ad']*p**2)
        xf = uak*p*(1.0 + zps['p2']*np.tanh( zps['p4']*p + zps['ad']*(1.0-alpha)))  + zps['a4']*(1.0-alpha)**2#np.tanh()
        #xf = uak*p  + k1*zps['p2']*np.tanh(zps['p4']*p**2 + zps['a4']*(1.0-alpha)**2 + zps['ad']*p*(1.0-alpha))
    elif settings.GE4 == 'poly':
        xf = uak*p  + zps['p2']*p**2 + (zps['a4']*(1.0-alpha) + zps['p4']*p)**2

    hx1 = 1.0 + k1 - k1/(1.0 + xf/k1)

    ief = np.ones(alpha.shape)
    oma = 1.0 - alpha
    ief[alpha < 1.0] = np.exp(-c1*alpha[alpha < 1.0]/oma[alpha < 1.0])
    ief[alpha > 1.0] = -d1*np.exp(c2/oma[alpha > 1.0])

    gx = np.ones(p.shape)
    gx[p > 1.e-16] = 1.0 - np.exp(-a1/p[p > 1.e-16]**(0.25))

    fx = (hx1 + ief*(hx0 - hx1))*gx

    return fx*ex_unif(d)

def ex_poc(d,gd,tau,zps):
    hx0 = 1.174
    a1 = 4.94869866704

    k1 = 0.174*.2#*0.2115505820
    uak = 10.0/81.0
    eta = 0.01
    nu = 0.005

    p = (gd/(2.0*(3*pi**2)**(1.0/3.0)*d**(4.0/3.0)))**2

    tauw = gd**2/(8.0*d)
    tauunif = 3.0/10.0*(3*pi**2)**(2.0/3.0)*d**(5.0/3.0)

    bb = np.maximum((tau - tauw)/(tau + (eta-1.0)*tauw + tauunif),np.zeros(tau.shape))
    bf = bb*(1.0 + ((1.0-2*bb) + (1.0-2*bb)**2)/(1.0 + ((1.0-2*bb)**2 + p**2)**2/nu**2) )
    z = 1.0-2*bf
    ief = 2*z - 13.0/8.0*z**3 + 3.0/4.0*z**5 - z**7/8.0

    #xf = uak*p
    xf = uak*p*(1.0 + zps['p2']*np.tanh( zps['p4']*p + zps['ad']*(1.0-2*bb) ))  + zps['a4']*(1.0-2*bb)**2
    hx1 = 1.0 + k1 - k1/(1.0 + xf/k1)

    gx = np.ones(p.shape)
    gx[p > 1.e-16] = 1.0 - np.exp(-a1/p[p > 1.e-16]**(0.25))

    fx = (hx1 + ief*(hx0 - hx1))*gx

    return fx*ex_unif(d)

def ex_modpbe(d,gd,tau,zps):
    a4 = 146.0/2025.0
    upbe = 0.066724550603149220*pi**2/3.0
    uak = 10.0/81.0
    kappa = 0.804

    p = (gd/(2.0*(3*pi**2)**(1.0/3.0)*d**(4.0/3.0)))**2

    tauw = gd**2/(8.0*d)
    tauunif = 3.0/10.0*(3*pi**2)**(2.0/3.0)*d**(5.0/3.0)
    alpha = np.maximum((tau - tauw)/tauunif,np.zeros(tau.shape))

    q = 9.0/20.0*(alpha - 1.0) + 2.0/3.0*p

    x = zps['p2']*p + (zps['a4']*(1.0 - alpha) + zps['p4']*p)**2
    fx = 1.0 + kappa - kappa/(1.0 + x/kappa)
    return fx*ex_unif(d)

def ex_mgga(d,gd,ld,tau,zps):

    kappa = 0.174
    uak = 10.0/81.0
    cge4 = 146.0/2025.0
    cpp = 25.0/16.0*cge4
    p = (gd/(2.0*(3*pi**2*d)**(1.0/3.0)*d))**2
    q = ld/(4.0*(3*pi**2*d)**(2.0/3.0)*d)

    delta = uak*p + cge4*(q**2 - 5.0*p*q/2.0) + cpp*p**2*np.tanh(p/kappa)

    hx1 = 1.0 + kappa*np.tanh(delta/kappa)
    hx0 = 1.174

    atil = ke_appr(d,gd,ld,zps,atil=True)
    fa = (1.0 - atil)**3/(1.0 + atil**3/zps['p2'])

    return (hx1 + fa*(hx0 - hx1))*ex_unif(d)

def ec_mgga(z,n,gn,gu,gd,lu,ld,zps):

    p = (gn/(2.0*(3.0*pi**2*n)**(1.0/3.0)*n))**2
    rs = (3.0/(4.0*pi*n))**(1.0/3.0)

    phi = spinf(z,2.0/3.0)
    gp3 = gamma*phi**3
    t2 = (3.0*pi**2/16.0)**(2.0/3.0)*p/(phi**2*rs)

    # ec_lsda_0 from J. Sun, J. P. Perdew, Z. Yang, and H. Peng,
    # J. Chem. Phys. 144,
    # 191101 (2016)
    # doi:10.1063/1.4950845
    b1c = 0.0233504
    b2c = 0.1018
    b3c = 0.102582
    ec0_lda = -b1c/(1.0 + b2c*rs**(0.5) + b3c*rs)

    h0 = 1.174
    dx_zeta = spinf(z,4.0/3.0)
    bfac = h0*(3.0/4.0*(3.0/(2.0*pi))**(2.0/3.0))*b3c/b1c
    gc_zeta = (1.0 - bfac*(dx_zeta - 1.0)) * (1.0 - z**(12))

    ec0 = gc_zeta*ec0_lda

    ecl = ec_pw92(z,n)
    brs = beta0*(1.0 + 0.1*rs)/(1.0 + 0.1778*rs)

    w = np.expm1((ec0-ecl)/gp3)
    aa = brs/gamma/w
    y = aa*t2
    gt = np.tanh(y)#1.0 - 1.0/(1.0 + 4*y)**(0.25)#
    h = gp3*np.log(1.0 + w*gt)

    ec1 = ecl + h

    nu = (1.0 + z)*n/2.0
    nd = (1.0 - z)*n/2.0

    tu = ke_appr(2*nu,2*gu,2*lu,zps)
    td = ke_appr(2*nd,2*gd,2*ld,zps)
    ds_zeta = spinf(z,5.0/3.0)
    t0 = 3.0/10.0*(3*pi**2*n)**(2.0/3.0)*n*ds_zeta
    tw = gn**2/(8.0*n)

    atil = (0.5*(tu+td)-tw)/t0
    fa = (1.0 - atil)**3/(1.0 + atil**3/zps['p4'])

    return ec1 + fa*(ec0 - ec1)
