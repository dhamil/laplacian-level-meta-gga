import numpy as np
from scipy import optimize

def get_lz_x(exl):
    zl = np.array([10.0, 18.0, 36.0, 54.0])
    #xlda =np.array([-10.9668, -27.8122, -88.5356, -170.5124])
    xlda = np.array([-11.51416921317595, -29.438915776102505, -95.21956089279242, -184.6863065741484])

    fn = (np.array(exl)- xlda)/zl
    fitfn = lambda p, x: p[0] + p[1]*x
    res = lambda p, x, y: fitfn(p,x) - y
    x0 = [0.2,0.2]
    [cbx,ccx],sux = optimize.leastsq(res,x0[:],args=(zl**(-1.0/3.0),fn))

    return cbx,ccx


def get_lz_c(ecl):
    zl = np.array([10.0, 18.0, 36.0, 54.0])
    tf_c0 = -0.02073*zl*np.log(zl)

    fn = (np.array(ecl) - tf_c0)/zlist
    fitfn = lambda p, x: p[0] + p[1]*x
    res = lambda p, x, y: fitfn(p,x) - y
    x0 = [0.2,0.2]
    [cbc,_],sux = optimize.leastsq(res,x0[:],args=(1.0/zl,fn))

    return cbc
