
    #a,b = np.transpose(np.asarray([[1,0.3125],[2, 0.6250],[3,0.7221],[7,1.2836],[8,1.4278],[9,1.5826],[10,1.7479],[11,1.8038]]))
    #print('Z>0',lst_sq_lin(a**(2.0/3.0),b))
    #a,b = np.transpose(np.asarray([[2, 0.6250],[3,0.7221],[7,1.2836],[8,1.4278],[9,1.5826],[10,1.7479],[11,1.8038]]))
    #print('x',lst_sq_lin(a**(2.0/3.0),b))
    #a,b = np.transpose(np.asarray([[2, 0.0467],[3,0.0537],[7,0.2373],[8,0.3064],[9,0.3694],[10,0.4278],[11,0.4577]]))
    #print('c',lst_sq_lin(a*np.log(a),b))
    #exit()
    """
    var =np.arange(0.0,1.0,0.0001)
    dend = {}
    for at in lzas:
        dend[at] = np.genfromtxt(at+'.csv',delimiter=',',dtype=None,skip_header=1)
    errl=[]
    for zps['ad'] in tqdm(var,total=var.shape[0]):
        ec_l = np.asarray([int_ec(dend[at],zps) for at in lzas])
        errl.append(100*np.abs((get_lz_c(ec_l)-settings.lzc1)/settings.lzc1))
    inds = np.asarray(errl).argmin()
    print(var[inds],errl[inds])
    exit()
    """
    """
    bb = [1e20,1e20]
    tlist = ['H','He','Li','Be','B','C','N','O','F','Ne','Ar','Kr','Xe']
    avg = 0.0
    for at in tlist:
        settings.X='ke'
        att = np.genfromtxt(at+'.csv',delimiter=',',dtype=None,skip_header=1)
        wg,nu,nd,gnu,gnd,gt,tu,td,lnu,lnd = np.transpose(att)
        do_oe = False
        if at == 'H':
            do_oe=True
        tapp = ex_den(nu,nd,gnu,gnd,tu,td,lnu,lnd,zps,oes=do_oe)#int_ex(att,zps,qoe=do_oe)
        texact = tu+td#np.sum(wg*(tu+td))
        res = np.sum(wg*np.abs(tapp-texact))/np.sum(wg*texact)
        avg+=res
        print(at,res)
    print('avg',avg/len(tlist))
    exit()
    """
    """
    texact = []
    abserr = 1e3
    dd = {}
    dd['H'] = np.genfromtxt('H.csv',delimiter=',',dtype=None,skip_header=1)
    for at in lzas:
        dd[at]= np.genfromtxt(at+'.csv',delimiter=',',dtype=None,skip_header=1)
        wg,nu,nd,gnu,gnd,gt,tu,td,lnu,lnd = np.transpose(dd[at])
        texact.append(np.sum(wg*(tu+td)))
    zl = np.asarray([10.0, 18.0, 36.0, 54.0])
    _,bex,_=lst_sq_lin(zl**(-2.0/3.0),np.asarray(texact)/zl**(7.0/3.0))
    ll = np.arange(0.001,0.601,0.0001)
    def min_h(par,dd):
        zps['p2']=par
        hex = int_ex(dd['H'],zps,qoe=True)
        return abs(hex-0.5)/0.5*100
    for itt in tqdm(ll,total=ll.shape[0]):
        zps['p4']=itt
        rr=optimize.minimize_scalar(min_h,bounds=(0.001,0.5),args=dd,method='bounded',tol=1.e-6)
        zps['p2']=rr.x
        #,zps['p4']=itt
        tapp = [int_ex(dd[at],zps) for at in lzas]
        _,ba,_=lst_sq_lin(zl**(-2.0/3.0),np.asarray(tapp)/zl**(7.0/3.0))
        err = (ba-bex)/bex*100
        hex = int_ex(dd['H'],zps,qoe=True)
        herr = (hex-0.5)/0.5*100
        res = (err**2 + herr**2)**(0.5)
        if res < abserr:
            abserr = res
            bherr = herr
            berr = err
            bp = zps['p2']
            bp2 = zps['p4']
    print(bp,bp2,abserr,berr,bherr)
    """
