from __future__ import print_function # python2 and python3 compatible amigos
import numpy as np
from scipy import optimize
from math import pi
import multiprocessing as mp
from itertools import product
from tqdm import tqdm

from atomic_densities.tf_den import get_lda_tf
from dft.ex import ex_den
from dft.ec import ec_den
import settings


def min_lz_errs(par_space,zps,simul=False):

    def min_h_err(cap,ws,nu,nd,gnu,gnd,gt,tu,td,lnu,lnd,zps):
        zps['a4'] = cap
        exd=ex_den(nu,nd,gnu,gnd,tu,td,lnu,lnd,zps,oes=True)
        ecd=ec_den(nu,nd,gnu,gnd,gt,tu,td,lnu,lnd,zps)
        exd[nu<1.0e-14] = 0.0
        ecd[nu<1.0e-14] = 0.0
        return abs(np.sum(ws*(exd+ecd)) + 0.3125)/0.3125


    if settings.X == "modSCAN":
        #wg = [6.446908804131819,15.11976346573394]#[0.06207904213112146,4.589273352382888]
        wg =15.11976346573394/6.446908804131819
    elif settings.X == "modPBE":
        #wg = [14.800264069241619,24.458485119785227]
        wg = 24.458485119785227/14.800264069241619
    elif settings.X == "POC" or settings.X == "MGGA" or settings.X == "GGA":
        wg = 1.0

    lzas = ['Ne','Ar','Kr','Xe']
    dvar={}
    if settings.X == 'EXX':
        for att in lzas:
            dvar[att] =att
    else:
        for att in lzas:
            dvar[att] =np.genfromtxt(att+'.csv',delimiter=',',dtype=None,skip_header=1)
        dvar['H'] =np.genfromtxt('H.csv',delimiter=',',dtype=None,skip_header=1)

    sigma = 1.e20
    zps['p4'] = 0.0
    zps['a4'] = 0.0
    zps['ad'] = 0.0
    print('Fitting Large-Z coefficient errors')
    #p2l = np.arange(-2.0,2.001,0.05)

    bpar = []
    if settings.use_a4:
        tl = np.arange(settings.p2_min,settings.p2_max,settings.p2_step)
        p2l =tl[tl>0.0]
        p4l = tl[tl>0.0]
        a4l=tl[tl>0.0]
    else:
        tl = np.arange(settings.p2_min,settings.p2_max,settings.p2_step)
        p2l = tl[tl>0.0]
        p4l= tl[tl>0.0]
        a4l = np.zeros(1)

    #p2l = np.arange(0.73,0.831,0.005)
    #p4l = np.arange(0.05,0.151,0.005)
    #a4l = np.arange(0.25,0.361,0.005)

    if settings.bx_only:
        #for zps['p2'] in tqdm(p2l, total=p2l.shape[0],ascii=True):
        for tlist in tqdm(product(p2l,a4l,p4l), total=p2l.shape[0]*a4l.shape[0]*p4l.shape[0],ascii=True,disable=False):
            zps['p2'],zps['a4'],zps['p4'] = tlist
            hwg,hnu,hnd,hgnu,hgnd,hgt,htu,htd,hlnu,hlnd = np.transpose(dvar['H'])
            if settings.fit_h_atom:
                res = optimize.minimize_scalar(min_h_err,bounds=(0.001,10.01),args=(hwg,hnu,hnd,hgnu,hgnd,hgt,htu,htd,hlnu,hlnd,zps),method='bounded')#,tol=5.0e-2)
                zps['a4'] = res.x
                if not res.success or res.fun > 5.e-2:
                    continue
            ex_l = np.asarray([int_ex(dvar[atom],zps) for atom in lzas])
            ec_l = np.asarray([int_ec(dvar[atom],zps) for atom in lzas])

            bx,cx = get_lz_x(ex_l)
            bc = get_lz_c(ec_l)
            #bxc,_=get_lz_x(ex_l+ec_l,xc=True)
            err_bx = 100.0*(bx - settings.lzx1)/settings.lzx1
            err_bc = 100.0*(bc - settings.lzc1)/settings.lzc1
            #err_bxc = 100.0*(bxc - settings.lzxc1)/settings.lzxc1
            if abs(err_bx) > 5.0:
                continue
            err_cx = 100.0*(cx - settings.lzx2)/settings.lzx2
            #lzres = np.abs(err_bx)
            #absres = (((err_bx**2 - err_cx**2)**2 + (err_bx**2 + err_cx**2)**2)/4.0e8)**(0.25)
            hexc=int_exc(dvar['H'],zps,qoe=True)
            herr = (hexc+.3125)/(-.3125)*100.0
            if settings.GE4 =="BX":
                absres = abs(err_bx)
            else:
                absres = (err_bx**2 + err_cx**2 + err_bc**2 + herr**2)**(0.5)#+err_bxc**2+err_bc**2
            if absres < sigma:
                sigma =absres
                bpar = [zps['p2'],zps['p4'],zps['a4'],bx,err_bx,cx,err_cx,bc,err_bc,hexc,herr]
                #if lzres < 1.0 and abs(err_cx) < 1.0:
                #    break
            #p2l = np.arange(bpar[0]-settings.throw*adj,bpar[0]+settings.throw*adj+adj/20.0,adj/10.0)
            #p2l = p2l[p2l>=0]
            #if settings.GE4 == "tanh" or settings.X == "MGGA" or settings.X == "GGA":
            #    p2l = p2l[p2l<= 1.0]
        if len(bpar) == 0:
            print('Found no satisfactory parameter sets')
            return
        hstring = 'p2,p4,a4,Bx,Bx err (%), Cx, Cx err (%),Bc, Bc error (%), H Exc,H Exc err (%)\n'
        if settings.X == "modSCAN":
            fname = 'modscan_bx_only.csv'
        elif settings.X == "modPBE":
            fname = 'modpbe_bx_only.csv'
        elif settings.X == "POC":
            fname = 'poc_bx_only.csv'
        elif settings.X == "MGGA":
            fname = 'mgga_bx_only.csv'
        elif settings.X == "GGA":
            fname = 'gga_x_'+(settings.C).lower()+'_c_'+(settings.GE4).lower()+'_bx_only.csv'
            #hstring = 'p2,p4,Bx,Bx err (%), Cx, Cx err (%),sigma,H Ex err\n'
        with open(fname,'w+') as outf:
            outf.write(hstring)
            outf.write(('{:},'*(len(bpar)-1)+'{:} \n').format(*bpar))
        return


    if settings.X == "modSCAN":
        fname = 'modscan_'+settings.GE4+'_simul.csv'
    elif settings.X == "modPBE":
        fname = 'modpbe_simul.csv'
    elif settings.X == "POC":
        fname = 'poc_simul.csv'
    elif settings.X == "MGGA":
        fname = 'mgga_simul.csv'
    elif settings.X == "GGA":
        fname = 'gga_simul.csv'

    with open(fname,'w+') as outf:
        cadj = 2.0
        tsig = 1e20
        abs_best = []
        par_l = []
        outf.write('fling,wcx,p2,p4,a4,ad,Bx,Bx err (%), Cx, Cx err (%),sigma\n')
        if settings.adj_throw:
            fling_l = np.arange(0.5,settings.throw+0.1,0.5)
        else:
            fling_l = np.asarray([settings.throw])
        if settings.adj_weight:
            w_l = np.arange(settings.weight_min,settings.weight_max+1.0e-5,settings.weight_step)
        else:
            w_l =  np.asarray([wg])
        wgs_l = product(fling_l,w_l)
        ofling = 0.0
        for wgs_i in tqdm(wgs_l,total = fling_l.shape[0]*w_l.shape[0],ascii=True,disable=False,desc='Flings/weights'):
            fling,wcx = wgs_i
            sigma = 1e20
            bpar = []
            p2l = np.arange(settings.p2_min,settings.p2_max,settings.p2_step)
            p4l = np.arange(settings.p4_min,settings.p4_max,settings.p4_step)
            a4l = np.arange(settings.a4_min,settings.a4_max,settings.a4_step)
            if settings.GE4 == 'poly':
                adl = np.asarray([None])
            else:
                adl = a4l

            if settings.GE4 == "tanh":
                p2l = p2l[p2l<=1.0]
                a4l = a4l[a4l>=0.0]
            elif settings.GE4 == "exp":
                adl = adl[adl >=0.0]
            if settings.X == "MGGA":
                p2l = p2l[0.0 < p2l]
                p2l = p2l[ p2l<=1.0]
                p4l = p4l[0.0 < p4l]
                adl = np.asarray([None])
                #a4l = np.asarray([None])

            for adj in np.asarray([1.0/cadj**i for i in range(9)]):

                for u in tqdm(product(p2l,p4l,a4l,adl), total=p2l.shape[0]*p4l.shape[0]*a4l.shape[0]*adl.shape[0], ascii=True,disable=False,desc='Coeffs'):
                    zps['p2'],zps['p4'],zps['a4'],zps['ad'] = u

                    ex_l = []
                    for atom in lzas:
                        ex_l.append(int_ex(atom,zps))

                    bx,cx = get_lz_x(ex_l)
                    err_bx = 100.0*(bx - settings.lzx1)/settings.lzx1
                    err_cx = 100.0*(cx - settings.lzx2)/settings.lzx2
                    res = np.sqrt((err_bx**2 - err_cx**2)**2 + (err_bx**2 + err_cx**2)**2)#np.sqrt((err_bx)**2 + (err_cx/wcx)**2)
                    absres = np.sqrt((err_bx**2 - err_cx**2)**2 + (err_bx**2 + err_cx**2)**2)
                    if res < sigma:
                        sigma = res
                        bpar = [fling,wcx,zps['p2'],zps['p4'],zps['a4'],zps['ad'],bx,err_bx,cx,err_cx,absres]

                p2l = np.arange(bpar[0]-fling*adj,bpar[0]+fling*adj + 0.01,adj/cadj)
                #p2l = np.arange(bpar[0]-fling*adj,bpar[0]-ofling*adj,adj/cadj)
                #p2l = np.append(p2l,np.arange(bpar[0]+ofling*adj,bpar[0]+fling*adj+1.0e-6,adj/cadj))
                p2l = p2l[p2l>=0]

                p4l = np.arange(bpar[1]-fling*adj,bpar[1]+fling*adj+ 0.01,adj/cadj)
                #p4l = np.arange(bpar[1]-fling*adj,bpar[1]-ofling*adj,adj/cadj)
                #p4l = np.append(p2l,np.arange(bpar[1]+ofling*adj,bpar[1]+fling*adj+1.0e-6,adj/cadj))
                p4l = p4l[p4l>=0]

                a4l = np.arange(bpar[2]-fling*adj,bpar[2]+fling*adj+ 0.01,adj/cadj)
                #a4l = np.arange(bpar[2]-fling*adj,bpar[2]-ofling*adj,adj/cadj)
                a4l = np.append(p2l,np.arange(bpar[2]+ofling*adj,bpar[2]+fling*adj+1.0e-6,adj/cadj))

                if settings.X == "MGGA":
                    p2l = p2l[0.0 < p2l]
                    p2l = p2l[ p2l<=1.0]
                    p4l = p4l[0.0 <p4l]
                    adl = np.asarray([None])
                    #a4l = np.asarray([None])
                if settings.GE4 == "exp":
                    adl = np.arange(bpar[3]-fling*adj,bpar[3]+fling*adj+ 0.01,adj/cadj)
                    adl = adl[adl>=0]
                elif settings.GE4 == "tanh":
                    a4l = a4l[a4l>=0.0]
                    p2l = p2l[p2l<=1.0]
                    adl = np.arange(bpar[3]-fling*adj,bpar[3]+fling*adj+ 0.01,adj/cadj)
                elif settings.GE4 == "poly":
                    adl = np.asarray([None])
            par_l.append(bpar)
            outf.write(('{:},'*10+'{:}\n').format(*bpar))
            ofling = fling
        for u in par_l:
            if u[10] < tsig:
                tsig = u[10]
                abs_best = u
        outf.write('--,--,--,--,Absolute, best,--,--,--,--\n')
        outf.write(('{:},'*10+'{:}\n').format(*abs_best))

    return
