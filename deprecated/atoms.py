import numpy as np

import settings
from constants import pi
from lsq import lst_sq_lin
from dft.ex import ex_den
from dft.ec import ec_den
from reference_data import lzx1,lzx2,lzc1,lzxc1,lda_ref_x,exx,ecc,atom_xc


def get_lz_x(exl,xlda):
    """
        see [SP2019]
    """
    zl = np.asarray([10.0, 18.0, 36.0, 54.0])
    fn = (np.asarray(exl) - xlda)/zl
    fitfn = lambda p, x: p[0] + p[1]*x
    res = lambda p, x, y: fitfn(p,x) - y

    cbx,ccx,_ = lst_sq_lin(zl**(-1.0/3.0),fn)
    return cbx,ccx

def get_lz_c(ecl):
    """
        See [BCGP2016]
    """

    zl = np.asarray([10.0, 18.0, 36.0, 54.0])
    tf_c0 = -2.0/3.0*(1.0-np.log(2.0))/pi**2*zl*np.log(zl)
    fn = (np.array(ecl) - tf_c0)/zl
    n_homo = np.arange(2.0,6.0,1.0)
    cbc,ccc,q = lst_sq_lin(1.0/n_homo,fn)
    return cbc#,ccc,q

def int_ex(dfa,at,zps,qoe=False,readf=False,unp=False):

    if readf:
        att = np.genfromtxt(base+at+'_'+settings.dataset+'.csv',delimiter=',',dtype=None,skip_header=1)
    else:
        att=at
    wg,nu,nd,gnu,gnd,gt,tu,td,lnu,lnd = np.transpose(att)
    exd = ex_den(dfa,nu,nd,gnu,gnd,tu,td,lnu,lnd,zps,oes=qoe,unp=unp)
    exd[nu+nd<1.e-14] = 0.0
    return np.sum(exd*wg)

def int_ec(dfa,at,zps,readf=False,unp=False):

    if readf:
        att = np.genfromtxt(base+at+'_'+settings.dataset+'.csv',delimiter=',',dtype=None,skip_header=1)
    else:
        att=at
    wg,nu,nd,gnu,gnd,gt,tu,td,lnu,lnd = np.transpose(att)
    ecd = ec_den(dfa,nu,nd,gnu,gnd,gt,tu,td,lnu,lnd,zps,unp=unp)
    ecd[nu+nd<1.e-14] = 0.0
    return np.sum(ecd*wg)

def int_exc(dfa_x,dfa_c,at,zps,qoe=False,unp=False):

    wg,nu,nd,gnu,gnd,gt,tu,td,lnu,lnd = np.transpose(at)
    exd = ex_den(dfa_x,nu,nd,gnu,gnd,tu,td,lnu,lnd,zps,oes=qoe,unp=unp)
    exd[nu+nd<1.e-14] = 0.0
    ecd = ec_den(dfa_c,nu,nd,gnu,gnd,gt,tu,td,lnu,lnd,zps,oes=qoe,unp=unp)
    ecd[nu+nd<1.e-14] = 0.0
    return np.sum((exd+ecd)*wg)

def get_atom_errs(tlist,atom_dvar):

    nwgt = settings.norm_wgts
    lzas = ['Ne','Ar','Kr','Xe']
    zps = {}
    zps['c0'],zps['c1'],zps['c2'],zps['c3'] = tlist
    ex_l = np.asarray([int_ex(settings.X,atom_dvar[atom],zps,unp=True) for atom in lzas])
    ec_l = np.asarray([int_ec(settings.C,atom_dvar[atom],zps,unp=True) for atom in lzas])

    ex_l = np.asarray([int_ex(settings.X,atom_dvar[atom],zps,unp=True) for atom in lzas])
    ec_l = np.asarray([int_ec(settings.C,atom_dvar[atom],zps,unp=True) for atom in lzas])

    bx,cx = get_lz_x(ex_l,lda_ref_x)
    bc = get_lz_c(ec_l)
    err_bx = 100.0*(bx - lzx1)/lzx1
    err_bc = 100.0*(bc - lzc1)/lzc1
    err_cx = 100.0*(cx - lzx2)/lzx2
    hexc=int_exc(settings.X,settings.C,atom_dvar['H'],zps,qoe=True)
    herr = (hexc-atom_xc['H'])/atom_xc['H']*100.0
    absres = (nwgt['Bx']*err_bx**2 + nwgt['Cx']*err_cx**2 + nwgt['Bc']*err_bc**2 + nwgt['H']*herr**2)#**(0.5)
    return tlist[0],tlist[1],tlist[2],tlist[3],bx,err_bx,cx,err_cx,bc,err_bc,hexc,herr,absres
