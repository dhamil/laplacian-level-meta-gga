import matplotlib.pyplot as plt
import numpy as np


def new_fs(p,q):

    x0 = 0.5583

    x = 1 -40*p/27 + 20*q/9
    ft = np.zeros(x.shape)

    mask = (0 <= x) & (x <= x0)
    xm = x[mask]

    ca =  20/x0**3
    cb =  -45/x0**4
    cc = 36./x0**5
    cd = -10./x0**6
    ft[mask] = xm**4*(ca + xm*(cb + xm*(cc + xm*cd)))

    mask = x>x0
    ft[mask] = x[mask]
    return 5*p/3 + ft

def fs_gea2(p,q):
    return 1 + 5*p/27 + 20*q/9

if __name__ == "__main__":

    fig,ax = plt.subplots(figsize=(5,4))
    p = np.linspace(0.0,5,5000)
    colors = ['darkblue','darkorange','darkgreen','darkred','brown']
    linestyles = ['-','--','-.',':','-']

    fw = 5*p/3
    ax.plot(p,fw,linewidth=2,color='black',label='$F_\\mathrm{W}$')
    ax.annotate('$F_\\mathrm{W}$',(p[-1]/2,5*p[-1]/7),fontsize=14)

    q_list = [-0.25,0,1,3]
    for iq,q in enumerate(q_list):
        fs = new_fs(p,q)
        tp = p[fs>fw]
        tfs = fs[fs>fw]
        nmid = (len(tp)-len(tp)%2)//2
        ax.plot(tp,tfs,linewidth=2,color=colors[iq],linestyle=linestyles[iq],label='q={:}'.format(q))
        if nmid > 2:
            if q < 0:
                ax.annotate('q={:}'.format(q),(1.15*tp[nmid],-.4+tfs[nmid]),fontsize=14,color=colors[iq])
            else:
                ax.annotate('q={:}'.format(q),(0.9*tp[nmid],.3+tfs[nmid]),fontsize=14,color=colors[iq])

    ax.set_xlim(p[0],p[-1])
    #ax.set_ylim(0,5/3*p[-1])
    ax.set_xlabel('$p$',fontsize=16)
    ax.set_ylabel('$F_\\mathrm{s}(p,q)$',fontsize=16)
    ax.tick_params(axis='both',labelsize=14)
    #ax.legend(fontsize=14)
    #plt.show() ; exit()
    plt.savefig('./f_s_plot.pdf',dpi=600,bbox_inches='tight')
