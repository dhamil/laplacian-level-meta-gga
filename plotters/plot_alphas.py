import matplotlib.pyplot as plt
import numpy as np

def alphas_c2(x):
    x0 = 0.5245
    alps = np.zeros(x.shape)
    ca = 6/x0**2
    cb = -8/x0**3
    cc = 3/x0**4

    mask = (0 <= x) & (x <= x0)
    xm = x[mask]

    alps[mask] = xm**3*(ca + xm*(cb + xm*cc))

    mask = x>x0
    alps[mask] = x[mask]
    return alps,x0

def alphas_c3(x,x0):
    #x0 = 0.819411#0.5583
    alps = np.zeros(x.shape)
    ca =  20/x0**3
    cb =  -45/x0**4
    cc = 36./x0**5
    cd = -10./x0**6

    mask = (0 <= x) & (x <= x0)
    xm = x[mask]

    alps[mask] = xm**4*(ca + xm*(cb + xm*(cc + xm*cd)))

    mask = x>x0
    alps[mask] = x[mask]
    return alps

def alpha_cr(z):
    apar = 4
    iz = np.ones_like(z)
    zmk = z < 0
    iz[zmk] = (1 - np.exp(-1/np.abs(z[zmk])**apar) )**(1/apar)
    return 1 + z*iz

def pc_interpolation(z,apar,bpar):
    theta = np.zeros(z.shape)
    # theta function is pretty badly-behaved. Introduce regularization to deal with it
    eps = 1.e-2
    apar_fuzzy = apar-eps
    mid_mask = (eps < z) & (z < apar_fuzzy)
    zm = z[mid_mask]
    thmid = ((1.0 + np.exp(apar/(apar-zm)))/(np.exp(apar/zm)+ np.exp(apar/(apar-zm))))**bpar
    theta[mid_mask] = thmid
    theta[z >= apar_fuzzy] = 1.0
    return theta

if __name__ == "__main__":

    fig,ax = plt.subplots(figsize=(6,4.5))
    x = np.linspace(-0.5,2,5000)
    x0 = 0.819411
    alp = alphas_c3(x,x0)
    ax.plot(x,alp,linewidth=2,color='darkblue',label='RPP')
    ax.plot(x,alpha_cr(x-1),linewidth=2,color='darkorange',linestyle='--',label='CR')
    ax.plot(x,x*pc_interpolation(x,0.5389,3.0),color='darkgreen',linestyle=':',linewidth=2,label='PC')
    ax.plot(x,x*pc_interpolation(x,1.784720,0.258304),color='darkred',linestyle='-.',linewidth=2,label='MR')

    ax.set_xlim(x[0],x[-1])
    ax.set_ylim(-1.e-2,x[-1])
    ax.set_xlabel('$x$',fontsize=16)
    ax.set_ylabel('$\\alpha_\\mathrm{s}(x)$',fontsize=16)
    ax.vlines(x0,ax.get_ylim()[0],ax.get_ylim()[1],color='gray',linewidth=1,linestyle='--')
    ax.set_xticks([-0.5,0.0,x0,1,1.5,2.0])
    ax.set_xticklabels(['-0.5','0.0','$x_0$','1.0','1.5','2.0'])
    ax.tick_params(axis='both',labelsize=14)
    ax.legend(fontsize=14)
    #plt.show() ;exit()
    plt.savefig('./alpha_s_plot.pdf',dpi=600,bbox_inches='tight')
