import numpy as np
from scipy.optimize import least_squares, minimize
import h5py
import matplotlib.pyplot as plt
from matplotlib import cm
from itertools import product

def theta_model_rp(z,c,log_z=False):
    npar = len(c)
    theta = np.zeros_like(z)
    zmsk = z >= 0.0
    if log_z:
        zm = np.log(1 + z[zmsk])/np.log(2.0)
    else:
        zm = z[zmsk]
    b1 = 3
    b2 = 0.0
    for i in range(npar):
        b1 += (5-i-1)*c[i]
        b2 += c[i]
    b2 -= b1
    tden = np.ones_like(zm)
    for i in range(len(c)):
        tden += c[i]*zm**(1+i)
    theta[zmsk] = zm**3*(1 + b1*zm + b2*zm**2)/tden
    return theta

def theta_model_lin(z,c,reg=True):
    if reg:
        zr = (z-1)/(z+1)
    else:
        zr = z

    theta = np.ones_like(z)
    cnp1 = 1
    npar = len(c)
    for ic in range(npar):
        theta += c[ic]*zr**(ic+2)
        cnp1 += c[ic]*(-1)**(ic+2)
    theta -= cnp1*(-zr)**(npar+2)

    return theta

def fit_theta():

    log_z = False

    ofl = h5py.File('./ked_data/all_norm_ked.hdf5','r')
    kdat = ofl['BINNED']
    obj = lambda c : (theta_model_rp(kdat[:,0],c,log_z=log_z) - kdat[:,1])
    obj_scl = lambda c: np.sum(obj(c)**2)

    zl = np.arange(0.0,1.0,0.01)
    zl = zl/(1 - zl)

    accps = []
    for ips in range(2,21):
        if log_z:
            lsfp = minimize(obj_scl,np.zeros(ips),method='BFGS')
        else:
            lsfp = least_squares(obj,np.zeros(ips))

        tps = [1]
        for tx in lsfp.x:
            tps.append(tx)

        #print(lsfp.x,tps)
        # pole-free denominator?
        prts = np.polynomial.polynomial.polyroots(tps)
        accept = True
        for prt in prts:
            if prt.imag == 0 and prt > 0.0:
                accept = False
                break

        # theta is non-negative?
        if accept:
            th = theta_model_rp(zl,lsfp.x,log_z=log_z)
            if np.any(th < 0.0):
                accept = False

        if accept:
            accps.append(lsfp.x)

    clist = ['darkblue','darkorange','darkgreen','tab:red']#cm.jet(np.linspace(0,1,len(accps)))
    lsl = ['-','--','-.',':']

    fig,ax = plt.subplots(figsize=(6,4))

    z = 10**(np.linspace(np.log10(5.e-2),np.log10(5.e1),5000))
    bds = [1e20,-1e20]
    jtx = 0
    for itx,tx in enumerate(accps):
        tstr = '['
        for ipar, apar in enumerate(tx):
            fchar = ', '
            if ipar == 0:
                fchar = ''
            tstr += '{:}{:}'.format(fchar,apar)
        tstr +=']'
        print(len(tx),tstr,obj_scl(tx))

        tm = theta_model_rp(z,tx,log_z=log_z)
        bds[0] = min([bds[0],tm.min()])
        bds[1] = max([bds[1],tm.max()])

        ax.plot(z,tm,color=clist[itx%len(clist)],label='$M={:}$'.format(len(tx)),\
            linestyle=lsl[jtx%len(lsl)])

        if itx >= len(clist)-1:
            jtx += 1

    ax.scatter(kdat[:,0],kdat[:,1],color='k')

    ax.set_xlim(z[0],z[-1])
    ax.set_xscale('log')

    #ax.set_ylim(0.0,1.5)
    ax.set_ylim(bds[0],1.01*max(kdat[:,1].max(),min(1.5,bds[1])))

    ax.hlines(1.0,*ax.get_xlim(),color='gray',linestyle=':')
    ax.vlines(1.0,*ax.get_ylim(),color='gray',linestyle=':')

    ax.set_xlabel('$z$',fontsize=12)
    ax.set_ylabel('$\\theta(z)$',fontsize=12)

    ax.legend(fontsize=12)

    #plt.show() ; exit()
    plt.savefig('./figs/human_learn_ked.pdf',dpi=600,bbox_inches='tight')

    return

def qfn(q,c,mq):
    fq = 1 + 20/9*q
    for iq in range(mq):
        fq += c[iq]*q**(iq+2)
    return fq

def pfn(p,c,mp):
    fp = 1 - 40/27*p
    for ip in range(mp):
        fp += c[ip]*p**(ip+2)
    return fp

def alpha_emp(p,q,c,mp,mq):
    qr = np.tanh(q)
    pr = np.tanh(p)
    fq = qfn(qr,c[:mq],mq)
    fp = pfn(pr,c[mq:],mp)
    alp = fq*fp
    #alp[alp<0.0] = 0.0
    return alp

def fit_alpha():

    ofl = h5py.File('./ked_data/all_norm_ked.hdf5','r')
    kdat = np.asarray(ofl['RAW'])
    alp_ex = kdat[:,2] - 5/3*kdat[:,0]
    alp_ex[alp_ex<0.0] = 0.0

    Npts = kdat.shape[0]

    binwidths = np.arange(0.01,1.01,0.01)
    p_max = 10.0
    q_max = 10.0
    tmsk = (kdat[:,0] <= p_max) & (np.abs(kdat[:,1])<=q_max)
    alp_ex_w = alp_ex[tmsk]
    p_w = kdat[tmsk,0]
    q_w = kdat[tmsk,1]

    p_max = min(p_max,p_w.max())
    q_max = min(q_max,q_w.max())
    q_min = max(-q_max,q_w.min())
    tmp = np.arange(0,q_w.shape[0],1)
    print(tmp.shape,q_w.shape)
    plt.scatter(tmp,q_w)
    plt.show()
    exit()

    for bw in binwidths:

        p_max_r = p_max + bw - p_max%bw
        q_max_r = q_max + bw - q_max%bw
        q_min_r = q_min - q_min%bw

        Np = int(np.ceil(p_max_r/bw)) + 1
        Nq = int(np.ceil((q_max_r-q_min_r)/bw)) + 1
        if Np*Nq > Npts:
            # can't be more bins than points
            continue
        p_bin = np.linspace(0.0,p_max_r,Np)
        q_bin = np.linspace(-q_max_r,q_max_r,Nq)

        #Np = p_bin.shape[0]
        #Nq = q_bin.shape[0]
        nbin = np.zeros((Np,Nq),dtype=int)

        alp_bin = np.zeros((Np,Nq))

        ip_l = np.searchsorted(p_bin,p_w)
        iq_l = np.searchsorted(q_bin,q_w)

        for ip,jp in enumerate(ip_l):
            jq = iq_l[ip]
            alp_bin[jp,jq] += alp_ex_w[ip]
            nbin[jp,jq] += 1

        #print(nbin[nbin==0].shape,np.sum(nbin))
        gtg = np.all(nbin>0)
        print(nbin[nbin==0].shape)
        for ip in range(Np):
            for jp in range(Nq):
                if nbin[ip,jp]==0:
                    print(p_bin[ip],q_bin[jp])
        exit()
        if gtg:
            alp_bin /= 1.0*nbin
            break
    exit()

    bet_ex = alp_ex/(1 + alp_ex + 5/3*kdat[:,0])

    accsol = {}

    tmp = range(2,5)
    tlist = product(tmp,tmp)

    qrl = np.linspace(-1.0,1.0,1000)
    prl = np.linspace(0.0,1.0,1000)

    for pv in tlist:

        mp,mq = pv
        npar = mp + mq

        def obj(c):
            alpemp = alpha_emp(kdat[:,0],kdat[:,1],c,mp,mq)
            alpemp[alpemp<0.0] = 0.0
            bet_emp = alpemp/(1 + alpemp + 5/3*kdat[:,0])
            tobj = np.sum((bet_emp - bet_ex)**2) #bet_emp - bet_ex
            return tobj

        lsfp = least_squares(obj,np.ones(npar))

        #bds = [(-10,10) for i in range(mp + mq)]
        #lsfp = minimize(obj,np.ones(npar))

        # alpha(p,q) is non-negative?
        accept_pos = True
        fqtmp = qfn(qrl,lsfp.x[:mq],mq)
        fptmp = pfn(prl,lsfp.x[mq:],mp)

        if np.any(fqtmp*fptmp < 0.0):
            accept_pos = False

        print(pv,lsfp.fun,accept_pos)

        if accept_pos:
            tmpkey = '{:}_{:}_{:}_{:}'.format(mp1,mp2,mq1,mq2)
            accsol[tmpkey] = {
                'MQ1': mq1, 'MQ2': mq2, 'MP1': mp1, 'MP2': mp2,
                'PARS': lsfp.x, 'RES': lsfp.cost
            }
            print(tmpkey,lsfp.cost)

    return

if __name__=="__main__":

    #fit_alpha()
    #exit()

    fit_theta()
