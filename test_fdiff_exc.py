import numpy as np
from os import sys
import settings

from dft.tau_w_derivs import lap_lev_ke_unp_w_derivs
from dft.scan_xc_w_derivs import ex_scan_unpol_w_derivs,ec_scan_w_derivs
from dft.pbe_xc_w_derivs import ex_pbe_unpol,ec_pbe
from dft.ec_pw92_w_derivs import ec_pw92,ex_lsda
from dft.pbesol_plus_xc_w_derivs import ex_pbesol_plus_unpol,ec_pbesol_plus

pi = np.pi

def test_derivs(wfxc,waux=None):

    zps = settings.fit_pars
    nrs = 20
    nzeta = 40
    naux = 20

    rsl = np.linspace(0.1,20,nrs)
    zl = np.linspace(-0.9,0.9,nzeta)

    ex_n = 0.0
    ec_nu = 0.0
    ec_nd = 0.0

    ex_gn = 0.0
    ec_gn = 0.0

    ex_aux = 0.0
    ec_aux = 0.0

    nl = 3/(4*pi*rsl**3)

    xnorm = nrs**2*naux
    cnorm = nrs**2*nzeta*naux

    xvar = np.zeros((xnorm,3))
    cvar = np.zeros((cnorm,4))

    ixvar = 0
    icvar = 0

    gga = ['PBE','PBEsol']
    l_meta = ['SCAN-L','r2SCAN-L','LLMG']
    t_meta = ['SCAN','r2SCAN']

    for n in nl:

        # 1.e-6 <= s <= 3
        gnl = 2*(3*pi**2*n)**(1/3)*n*np.linspace(1.e-6,3.0,nrs)

        for gn in gnl:

            if wfxc in t_meta:
                # 1.e-6 <= alpha <= 5
                tw_tmp = gn**2/(8*n)
                t0_tmp = 0.3*(3*pi**2*n)**(2/3)*n
                auxl = tw_tmp + t0_tmp*np.linspace(1.e-6,5.0,naux)
            else:
                # |q| <= 5
                auxl = 4*(3*pi**2*n)**(2/3)*n*np.linspace(-5.0,5.0,naux)

            for aux in auxl:

                xvar[ixvar,0] = n
                xvar[ixvar,1] = gn
                xvar[ixvar,2] = aux
                ixvar += 1

                for z in zl:

                    nup = n/2*(1 + z)
                    ndn = n/2*(1 - z)

                    cvar[icvar,0] = nup
                    cvar[icvar,1] = ndn
                    cvar[icvar,2] = gn
                    cvar[icvar,3] = aux
                    icvar += 1

    n_step = max(1.e-10,xvar[:,0].min()/10)
    nu_step = max(1.e-10,cvar[:,0].min()/10)
    nd_step = max(1.e-10,cvar[:,1].min()/10)
    gn_step = max(1.e-10,xvar[:,1].min()/10)
    aux_step = max(1.e-10,np.abs(xvar[:,2]).min()/10)

    if wfxc == 'KE':
        ex,dexdn,dexdgn,dexdaux = lap_lev_ke_unp_w_derivs(waux,xvar[:,0],\
            xvar[:,1],xvar[:,2],zps)
        ex_n1,_,_,_ = lap_lev_ke_unp_w_derivs(waux,xvar[:,0]+n_step,xvar[:,1],\
            xvar[:,2],zps)
        ex_gn1,_,_,_ = lap_lev_ke_unp_w_derivs(waux,xvar[:,0],xvar[:,1]+gn_step,\
            xvar[:,2],zps)
        ex_aux1,_,_,_ = lap_lev_ke_unp_w_derivs(waux,xvar[:,0],xvar[:,1],\
            xvar[:,2]+aux_step,zps)

        ex_n = np.sum(np.abs(dexdn - (ex_n1-ex)/n_step))/xnorm
        ex_gn = np.sum(np.abs(dexdgn - (ex_gn1 - ex)/gn_step))/xnorm
        ex_aux = np.sum(np.abs(dexdaux - (ex_aux1 - ex)/aux_step))/xnorm

    elif wfxc == 'LSDA':
        ex,dexdn, _ = ex_lsda(xvar[:,0]/2,xvar[:,0]/2)
        ex_n1,_,_ = ex_lsda((xvar[:,0]+n_step)/2,(xvar[:,0]+n_step)/2)

        ex_n = np.sum(np.abs(dexdn - (ex_n1-ex)/n_step))/xnorm

        ec, decdnup, decdndn = ec_pw92(cvar[:,0],cvar[:,1])
        ec_nup1,_,_ = ec_pw92(cvar[:,0]+nu_step,cvar[:,1])
        ec_ndn1,_,_ = ec_pw92(cvar[:,0],cvar[:,1]+nd_step)

        ec_nu = np.sum(np.abs(decdnup - (ec_nup1-ec)/nu_step))/cnorm
        ec_nd = np.sum(np.abs(decdndn - (ec_ndn1-ec)/nd_step))/cnorm

    elif wfxc in ['PBE','PBEsol']:

        ex,dexdn,dexdgn = ex_pbe_unpol(xvar[:,0],xvar[:,1],var=wfxc)
        ex_n1,_,_ = ex_pbe_unpol(xvar[:,0]+n_step,xvar[:,1],var=wfxc)
        ex_gn1,_,_ = ex_pbe_unpol(xvar[:,0],xvar[:,1]+gn_step,var=wfxc)

        ex_n = np.sum(np.abs(dexdn - (ex_n1-ex)/n_step))/xnorm
        ex_gn = np.sum(np.abs(dexdgn - (ex_gn1 - ex)/gn_step))/xnorm

        ec, decdnup, decdndn, decdgn = ec_pbe(cvar[:,0],cvar[:,1],cvar[:,2],var=wfxc)
        ec_nup1, _,_,_ = ec_pbe(cvar[:,0]+nu_step,cvar[:,1],cvar[:,2],var=wfxc)
        ec_ndn1, _,_,_ = ec_pbe(cvar[:,0],cvar[:,1]+nd_step,cvar[:,2],var=wfxc)
        ec_gn1, _,_,_ = ec_pbe(cvar[:,0],cvar[:,1],cvar[:,2]+gn_step,var=wfxc)

        ec_nu = np.sum(np.abs(decdnup - (ec_nup1-ec)/nu_step))/cnorm
        ec_nd = np.sum(np.abs(decdndn - (ec_ndn1-ec)/nd_step))/cnorm
        ec_gn = np.sum(np.abs(decdgn - (ec_gn1 - ec)/gn_step))/cnorm

    elif wfxc == 'LLMG':
        ex,dexdn,dexdgn,dexdaux = ex_pbesol_plus_unpol(xvar[:,0],xvar[:,1],xvar[:,2],zps)
        ex_n1,_,_,_ = ex_pbesol_plus_unpol(xvar[:,0]+n_step,xvar[:,1],xvar[:,2],zps)
        ex_gn1,_,_,_ = ex_pbesol_plus_unpol(xvar[:,0],xvar[:,1]+gn_step,xvar[:,2],zps)
        ex_aux1,_,_,_ = ex_pbesol_plus_unpol(xvar[:,0],xvar[:,1],xvar[:,2]+aux_step,zps)

        ex_n = np.sum(np.abs(dexdn - (ex_n1-ex)/n_step))/xnorm
        ex_gn = np.sum(np.abs(dexdgn - (ex_gn1 - ex)/gn_step))/xnorm
        ex_aux = np.sum(np.abs(dexdaux - (ex_aux1 - ex)/aux_step))/xnorm

        ec, decdnup, decdndn, decdgn = ec_pbesol_plus(cvar[:,0],cvar[:,1],cvar[:,2])
        ec_nup1, _,_,_ = ec_pbesol_plus(cvar[:,0]+nu_step,cvar[:,1],cvar[:,2])
        ec_ndn1, _,_,_ = ec_pbesol_plus(cvar[:,0],cvar[:,1]+nd_step,cvar[:,2])
        ec_gn1, _,_,_ = ec_pbesol_plus(cvar[:,0],cvar[:,1],cvar[:,2]+gn_step)

        ec_nu = np.sum(np.abs(decdnup - (ec_nup1-ec)/nu_step))/cnorm
        ec_nd = np.sum(np.abs(decdndn - (ec_ndn1-ec)/nd_step))/cnorm
        ec_gn = np.sum(np.abs(decdgn - (ec_gn1 - ec)/gn_step))/cnorm

    elif wfxc in ['SCAN','r2SCAN']:
        ex,dexdn,dexdgn, dexdaux = ex_scan_unpol_w_derivs(xvar[:,0],xvar[:,1],\
            xvar[:,2],zps,wfxc)
        ex_n1,_,_,_ = ex_scan_unpol_w_derivs(xvar[:,0]+n_step,xvar[:,1],\
            xvar[:,2],zps,wfxc)
        ex_gn1,_,_,_ = ex_scan_unpol_w_derivs(xvar[:,0],xvar[:,1]-gn_step,\
            xvar[:,2],zps,wfxc)
        ex_aux1,_,_,_ = ex_scan_unpol_w_derivs(xvar[:,0],xvar[:,1],\
            xvar[:,2]+aux_step,zps,wfxc)

        ex_n = np.sum(np.abs(dexdn - (ex_n1-ex)/n_step))/xnorm
        ex_gn = np.sum(np.abs(dexdgn - (ex - ex_gn1)/gn_step))/xnorm
        ex_aux = np.sum(np.abs(dexdaux - (ex_aux1 - ex)/aux_step))/xnorm

        ec, decdnup, decdndn, decdgn, decdaux = ec_scan_w_derivs(cvar[:,0],\
            cvar[:,1],cvar[:,2],cvar[:,3],zps,wfxc)
        ec_nup1,_,_,_,_ = ec_scan_w_derivs(cvar[:,0]+nu_step,cvar[:,1],cvar[:,2],\
            cvar[:,3],zps,wfxc)
        ec_ndn1,_,_,_,_ = ec_scan_w_derivs(cvar[:,0],cvar[:,1]+nd_step,cvar[:,2],\
            cvar[:,3],zps,wfxc)
        ec_gn1,_,_,_,_ = ec_scan_w_derivs(cvar[:,0],cvar[:,1],cvar[:,2]-gn_step,\
            cvar[:,3],zps,wfxc)
        ec_aux1,_,_,_,_ = ec_scan_w_derivs(cvar[:,0],cvar[:,1],cvar[:,2],\
            cvar[:,3]+aux_step,zps,wfxc)

        ec_nu = np.sum(np.abs(decdnup - (ec_nup1-ec)/nu_step))/cnorm
        ec_nd = np.sum(np.abs(decdndn - (ec_ndn1-ec)/nd_step))/cnorm
        ec_gn = np.sum(np.abs(decdgn - (ec - ec_gn1)/gn_step))/cnorm
        ec_aux = np.sum(np.abs(decdaux - (ec_aux1 - ec)/aux_step))/cnorm

    else:
        raise SystemExit('Derivatives for {:} not implemented yet'.format(wfxc))

    return ex_n,ex_gn,ex_aux,ec_nu,ec_nd,ec_gn,ec_aux

if __name__=="__main__":

    aux = None
    if len(sys.argv) == 1:
        raise SystemExit('Too few arguments to finite diff calc')
    if len(sys.argv) > 1:
        wfnl = sys.argv[1]
    if len(sys.argv) > 2:
        aux = sys.argv[2]

    ex_n,ex_gn,ex_aux,ec_nu,ec_nd,ec_gn,ec_aux = test_derivs(wfnl,waux=aux)
    print('FUNCTIONAL = {:}'.format(wfnl))
    if aux is not None:
        print('AUX = {:}'.format(aux))
    print('d ex / d n = {:}'.format(ex_n))
    print('d ex / d gn = {:}'.format(ex_gn))
    print('d ex / d aux = {:}'.format(ex_aux))

    print('\nd ec / d n_up = {:}'.format(ec_nu))
    print('d ec / d n_dn = {:}'.format(ec_nd))
    print('d ec / d gn = {:}'.format(ec_gn))
    print('d ec / d aux = {:}'.format(ec_aux))
