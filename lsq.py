import numpy as np

def lst_sq_lin(x,y):
    mat = np.asarray([np.sum(x**j) for j in range(3)])
    matinv = 1.0/(mat[0]*mat[2] - mat[1]**2)*np.asarray([[mat[2],-mat[1]],[-mat[1],mat[0]]])
    f = np.asarray([[np.sum(y)],[np.sum(y*x)]])
    fit = np.matmul(matinv,f)
    varp = np.sum((fit[0,0]+fit[1,0]*x-f[0,0]/len(y))**2)
    vart = max(1.e-20,np.sum((y-f[0,0]/len(y))**2))
    return fit[0,0],fit[1,0],varp/vart
