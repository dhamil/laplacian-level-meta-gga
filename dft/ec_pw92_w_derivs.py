import numpy as np

from constants import pi

def ex_lsda(nu,nd):

    exu, dexdnu = ex_lsda_unp(2*nu)
    exd, dexdnd = ex_lsda_unp(2*nd)
    return (exu + exd)/2, dexdnu,dexdnd

def ex_lsda_unp(n):
    kf = (3*pi**2*n)**(1/3)
    exlda = -3/(4*pi)*kf*n
    d_exlda_dn = -kf/pi
    return exlda, d_exlda_dn

def ec_pw92(nu,nd):

    n = nu + nd
    rs = (3/(4*pi*n))**(1/3)
    d_rs_dn = -rs/(3*n)

    z = np.minimum(np.maximum((nu - nd)/n,-0.99999999999990),0.99999999999990)
    d_z_dnu = 2*nd/n**2
    d_z_dnd = -2*nu/n**2

    ecl,d_ecl_drs,d_ecl_dz,_, _ = ec_pw92_w_derivs(z,rs)

    ec = n*ecl

    vcn = ecl + n*d_ecl_drs*d_rs_dn
    vcu = vcn + n*d_ecl_dz*d_z_dnu
    vcd = vcn + n*d_ecl_dz*d_z_dnd

    return ec, vcu,vcd

def spinf_w_deriv(z,n,oderiv=1):
    opz = np.minimum(2,np.maximum(0.0,1+z))
    omz = np.minimum(2,np.maximum(0.0,1-z))
    f = (opz**n + omz**n)/2.0
    df = n*(opz**(n-1) - omz**(n-1))/2.0
    if oderiv == 0:
        return f
    elif oderiv == 1:
        return f, df
    elif oderiv == 2:
        ddf = n*(n-1)*(opz**(n-2) + omz**(n-2))/2.0
        return f, df, ddf

def ec_pw92_w_derivs(z,rs):

    """
        J.P. Perdew and Y. Wang,
        ``Accurate and simple analytic representation of the electron-gas correlation energy'',
        Phys. Rev. B 45, 13244 (1992).
        https://doi.org/10.1103/PhysRevB.45.13244
    """

    rsh = rs**(0.5)
    def g(v):

        q0 = -2*v[0]*(1 + v[1]*rs)
        dq0 = -2*v[0]*v[1]

        q1 = 2*v[0]*(v[2]*rsh + v[3]*rs + v[4]*rs*rsh + v[5]*rs*rs)
        dq1 = v[0]*(v[2]/rsh + 2*v[3] + 3*v[4]*rsh + 4*v[5]*rs)
        ddq1 = v[0]*(-0.5*v[2]/rsh**3 + 3/2*v[4]/rsh + 4*v[5])

        q2 = np.log(1 + 1/q1)
        dq2 = -dq1/(q1**2 + q1)
        ddq2 = (dq1**2*(1 + 2*q1)/(q1**2 + q1) - ddq1)/(q1**2 + q1)

        g = q0*q2
        dg = dq0*q2 + q0*dq2
        ddg = 2*dq0*dq2 + q0*ddq2

        return g,dg,ddg

    unp_pars = [0.031091,0.21370,7.5957,3.5876,1.6382,0.49294]
    pol_pars = [0.015545,0.20548,14.1189,6.1977,3.3662,0.62517]
    alp_pars = [0.016887,0.11125,10.357,3.6231,0.88026,0.49671]

    fz_den = 0.5198420997897464#(2**(4/3)-2)
    fdd0 = 1.7099209341613653#8/9/fz_den
    dxz,d_dxz_dz = spinf_w_deriv(z,4/3)
    fz = 2*(dxz - 1)/fz_den
    d_fz_dz = 2*d_dxz_dz/fz_den

    ec0,d_ec0_drs,d_ec0_drs2 = g(unp_pars)
    ec1,d_ec1_drs,d_ec1_drs2 = g(pol_pars)
    ac,d_ac_drs,d_ac_drs2 = g(alp_pars)
    z4 = z**4
    fzz4 = fz*z4
    ec = ec0 - ac/fdd0*(fz - fzz4) + (ec1 - ec0)*fzz4

    d_ec_drs = d_ec0_drs*(1 - fzz4) + d_ec1_drs*fzz4 - d_ac_drs/fdd0*(fz - fzz4)

    d_ec_dz = -ac*d_fz_dz/fdd0 + (fz*4*z**3 + d_fz_dz*z4)*(ac/fdd0 + ec1 - ec0)

    d_ec_drs2 = d_ec0_drs2*(1 - fzz4) + d_ec1_drs2*fzz4 - d_ac_drs2/fdd0*(fz - fzz4)

    d_ec_drsdz = -d_ac_drs*d_fz_dz/fdd0 + (fz*4*z**3 + d_fz_dz*z4)*(d_ac_drs/fdd0\
        + d_ec1_drs - d_ec0_drs)

    return ec, d_ec_drs, d_ec_dz, d_ec_drs2,  d_ec_drsdz
