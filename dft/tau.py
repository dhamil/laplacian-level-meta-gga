import numpy as np

from constants import pi
#pi = 3.141592653589793238462643383279502884197169399375105820974944592307816406286198

def lap_lev_ke(wke,du,dd,gdu,gdd,ldu,ldd,zps,oes=False,unp=False):
    if unp:
        return lap_lev_ke_unp(wke,2*du,2*gdu,2*ldu,zps)
    tauu = lap_lev_ke_unp(wke,2*du,2*gdu,2*ldu,zps)
    if oes:
        taud = 0.0
    else:
        taud = lap_lev_ke_unp(wke,2*dd,2*gdd,2*ldd,zps,pars=pars)
    return (tauu+taud)/2.0


def lap_lev_ke_unp(wke,d,gd,ld,zps):

    kf = (3*pi**2*d)**(1/3)
    tau0 = 0.3*kf**2*d

    p = (gd/(2*kf*d))**2
    q = ld/(4*kf**2*d)

    ft = lap_lev_ft(wke,p,q,zps)

    return ft*tau0


def lap_lev_ft(wke,p,q,zps):

    if wke == 'GEA2':
        ft = 1 + 5/27*p + 20/9*q
    elif wke == 'GEA4':
        ft = 1 + 5/27*p + 20/9*q + 8/81*q**2 - p*q/9 + 8/243*p**2

    elif wke == 'PC':
        # [PC2007]
        ft = ft_pc(p,q,0.5389,3.0)

    elif wke == 'MRT':
        # [MRT2017]
        ft = ft_pc(p,q,1.784720,0.258304)

    elif wke == 'CR':
        ft = ft_cr(p,q)

    elif wke in ['RPP1','RPP2']:
        ft = ft_rpp(p,q,int(wke[-1]),zps)

    elif wke in ['RPE1','RPE2']:
        ft = ft_rpe(p,q,int(wke[-1]),zps)

    elif wke == 'RAE':
        ft = ft_rae(p,q,zps)

    elif wke[:2] == 'HL':
        ft = ft_hl(p,q,int(wke[2:]))

    else:
        raise ValueError('Unknown approximate tau, ',wke)

    return ft

def ft_pc(p,q,apar,bpar):
    ftw = 5*p/3
    ft2 = 5/27*p + 20/9*q
    ft4 = 8/81*q**2 - p*q/9 + 8/243*p**2

    freg = (1.0 + (ft4/(1.0 + ftw))**2)**(0.5)
    ftge4 = (1.0 + ft2 + ft4)/freg
    z = ftge4 - ftw
    theta = pc_interpolation(z,apar,bpar)

    ft = ftw + z*theta
    return ft

def pc_interpolation(z,apar,bpar):
    theta = np.zeros(z.shape)
    # theta function is pretty badly-behaved. Introduce regularization to deal with it
    eps = 1.e-2
    apar_fuzzy = apar-eps
    mid_mask = (eps < z) & (z < apar_fuzzy)
    zm = z[mid_mask]
    thmid = ((1.0 + np.exp(apar/(apar-zm)))/(np.exp(apar/zm)+ np.exp(apar/(apar-zm))))**bpar
    theta[mid_mask] = thmid
    theta[z >= apar_fuzzy] = 1.0
    return theta

def ft_cr(p,q):

    """ [CR2017] """

    apar = 4

    z = 20*q/9 - 40*p/27
    iz = np.ones_like(z)
    zmk = z < 0
    iz[zmk] = (1 - np.exp(-1/np.abs(z[zmk])**apar) )**(1/apar)

    fw = 5*p/3
    ft = fw + 1 + z*iz
    return ft

def ft_rpp(p,q,rev,zps):

    """
    c2 = 9/16*(20*(zps['c1'] + 40/27) - 1)
    c3 =  8/81*zps['c1']**2 + 1/2*(zps['c1'] + 40/27)*(zps['c1'] + 40/9) - 8/243
    #c3 = 8/243*(3*zps['c1']**2 + 45*(40/27 + zps['c1']) - 1)
    x = (1 + zps['c1']*p + 20*q/9 + 8/81*(q + c2*p)**2) \
        /(1 + 2*(zps['c1'] + 40/27)*p + 2*c3*p**2)**(0.5)
    """
    cqq,cpq,cpp = get_r2_ge4_corr()

    f4 = (cpp-zps['c3'])*p**2 + cpq*p*q + cqq*q**2
    f4damp = np.exp(-(p/zps['c1'])**2 - (q/zps['c2'])**2)
    f4c = zps['c3']*p**2*np.exp(-zps['c3']*p)

    x = 1 - 40/27*p + 20/9*q + f4c + f4*f4damp
    """
    #f4 = (cqq*q**2 + cpq*p*q)*np.exp(-cqq*q**2) + cpp*p**2
    x = 1 - 40/27*p + 20*q/9 + zps['c1']*p**2*np.exp(-abs(zps['c1'])*p)#f4*np.exp(-cpp*p**2)
    """
    alp = np.zeros(x.shape)

    if rev == 1:

        x0 = zps['c0']#0.5583#0.55821
        ca =  20/x0**3
        cb =  -45/x0**4
        cc = 36./x0**5
        cd = -10./x0**6

        mask = (0 <= x) & (x <= x0)
        xm = x[mask]

        alp[mask] = xm**4*(ca + xm*(cb + xm*(cc + xm*cd)))

    elif rev == 2:

        x0 = 0.5245
        ca = 6/x0**2
        cb = -8/x0**3
        cc = 3/x0**4
        mask = (0 <= x) & (x <= x0)
        xm = x[mask]

        alp[mask] = xm**3*(ca + xm*(cb + xm*cc))

    mask = x>x0
    alp[mask] = x[mask]
    return 5*p/3 + alp

def get_r2_ge4_corr():
    eta = 1.e-3
    hx0 = 1.174
    k0 = 0.174
    k1 = 0.065

    uak = 10/81
    cqq = 146/2025
    cpq = -5/2*cqq

    epne = (8+9*eta)

    cxv = [1.0,-0.667,-0.4445555,-0.663086601049, 1.451297044490, -0.887998041597, 0.234528941479,-0.023185843322]

    f1 = 0
    f2 = 0
    for i in range(1,8):
        f1 += i*cxv[i]
        f2 += i*(i-1)*cxv[i]

    h1 = 5*(4 + 9*eta)*k0*f1/27 + uak
    h2 = -2*h1**2/k1

    tqq = (146/2025/k0 - 200/81*f2)/f1
    tpq = 100/243*epne*f2/f1 + 20/9*h1/k0 + 100*eta/27 - 73/405/(k0*f1)
    tpp = -(h2/2 + 5/27*epne*h1*f1 + 25/1458*epne**2*k0*f2 )/(k0* f1) \
        - 25*eta**2/9 - 200*eta/81

    return tqq,tpq,tpp

def ke_g_poly(y,k):
    g = np.zeros_like(y)
    for i in range(1,k+1):
        g += (-1)**(i+1)*y**i/i
    return g

def ke_h_poly(y,k):
    h = ( ke_g_poly(y,2*k) + 2*(k+1)/(2*k+1)*y**(2*k+1) )/(1 +  y**(2*k))
    return h

def ft_rpe(p,q,rev,zps):
    x = -40*p/27 + 20*q/9
    x0 = zps['c0']
    nn = int(zps['c1'])

    mask = x <= x0
    alp = np.zeros_like(x)
    y = (x - x0)/(1 + x0)
    if rev == 1:
        alp[mask] = (1+x0)*np.exp(ke_g_poly(y[mask],nn))
    elif rev == 2:
        alp[mask] = (1+x0)*np.exp(ke_h_poly(y[mask],nn))
    mask = x > x0
    alp[mask] = 1 + x[mask]

    return 5*p/3 + alp

def ft_rae(p,q,zps):

    x = -40*p/27 + 20*q/9
    alp = np.zeros(x.shape)
    # doing this switching to avoid overflows
    xmask = x>0
    xm = x[xmask]
    alp[xmask] = (1 + xm + np.exp(-4*xm) )/(1 + np.exp(-5*xm))

    xmask = x<= 0
    xm = x[xmask]
    alp[xmask] = (np.exp(xm) + (1 + xm)*np.exp(5*xm))/(1 + np.exp(5*xm))

    return 5*p/3 + alp

def ke_non_emp_ft_jpp2(p,q,zps):

    x0 = zps['c0']
    x1 = zps['c1']
    if (x0 >= x1):
        return 1e20*np.ones(p.shape)
    c0 = 2*( 5 + 2*x0 + 3*x1 )
    c1 = -( 15 + 7*x0 + 8*x1 )
    c2 = 3*( 2 + x0 + x1 )

    x = -40*p/27 + 20*q/9
    y = (x - x0)/(x1 - x0)

    alp = np.zeros(x.shape)
    mask = (x0 <= x) & (x <= x1)
    ym = y[mask]
    alp[mask] = ym**3*(c0 + ym*(c1 + ym*c2))

    yt = np.linspace(0,1,1000)
    if np.any(yt**3*(c0 + yt*(c1 + yt*c2))<0.0):
        return 1e20*np.ones(p.shape)

    mask = x > x1
    alp[mask] = 1 + x[mask]
    return 5*p/3 + alp

def ft_hl(p,q,npar):
    pard = {
        # these use GE2 ingredients
        3 : [-2.4153571096515076, 1.9965618896773247, 0.24130250873900594],
        4 : [-5.625916373547513, 10.802436076371524, -7.770489376090032, \
            3.832601020621118],
        6 : [0.41734330440209316, -26.865089294441745, 75.20427919702495, \
            -83.96867704976964, 52.17930398327887, 0.6775117229296242],
        11 : [-1.2671660247767116, -11.922250743093565, 22.386020165089914, \
            10.69242899231974, -35.98119268664415, 34.89146499613219, \
            1.0511772339901142, -4.812118332316358, 1.3178981129806067, \
            -0.13854891212453904, 0.005117342685216863]
    }

    fw = 5*p/3
    z = 1 + 20*q/9 - 40*p/27

    ft = fw + z*theta_model(z,pard[npar])
    return ft

def theta_model(z,c):
    npar = len(c)
    theta = np.zeros_like(z)
    zmsk = z >= 0.0
    zm = z[zmsk]#np.log(1+z[zmsk])/np.log(2)#
    b1 = 3
    b2 = 0.0
    for i in range(npar):
        b1 += (5-i-1)*c[i]
        b2 += c[i]
    b2 -= b1
    tden = np.ones_like(zm)
    for i in range(len(c)):
        tden += c[i]*zm**(1+i)
    theta[zmsk] = zm**3*(1 + b1*zm + b2*zm**2)/tden
    return theta

def theta(z,pars):
    f = np.zeros_like(z)
    reg1 = z>0.0
    zr = z[reg1]
    f[reg1] = 1 - (1 - zr)**3*(1 + pars[0]*zr)/(1 + zr**4*(pars[1] + pars[2]*np.log(1 + zr)) )
    zt = np.linspace(0.0,10.0,1000)
    ftest1 = 1 + zt**4*(pars[1] + pars[2]*np.log(1 + zt))
    ftest2 = 1 - (1 - zt)**3*(1 + pars[0]*zt)/(1 + zt**4*(pars[1] + pars[2]*np.log(1 + zt)) )
    if np.any(ftest1 < 0.0) or np.any(ftest2 < 0.0) :
        return 1e20
    """
    f[reg1] = 1 - (1 - zr)**3*(1 + pars[0]*zr)/(1 + zr**4*(pars[1] + pars[2]*np.log(1 + zr)) )
    zt = np.linspace(0.0,10.0,1000)
    ftest1 = 1 + zt**4*(pars[1] + pars[2]*np.log(1 + zt))
    if np.any(ftest1 < 0.0):
        return 1e20
    ftest2 = 1 - (1 - zt)**3*(1 + pars[0]*zt)/(1 + zt**4*(pars[1] + pars[2]*np.log(1 + zt)) )
    if np.any(ftest2 < 0.0):
        return 1e20
    """
    return f

def scan_interp(z,c1,c2,d):
    f = np.ones_like(z)
    reg1 = (z >= 0) & (z < 1)
    reg2 = z > 1
    f[reg1] = np.exp(-c1*z[reg1]/(1 - z[reg1]))
    f[reg2] = -d*np.exp(c2/(1 - z[reg2]))
    return 1 - f

def unit_step(x):
    ox = np.zeros(x.shape)
    ox[x>0] = 1.0
    return ox

if __name__ == "__main__":

    tw = gn**2/(8*n)
    tu0 = 0.3*(3*pi**2)**(2/3)*(3/(4*pi*rs**3))**(5/3)
    kf = (3*pi**2*n)**(1/3)
    lf = 2*pi*rs/(9*pi/4)**(1/3)
    tu = 0.3*kf**2*n
    p = (gn/(2*kf*n))**2

    alpha = (tau - tw)/tu
    alpha_bar = (tau - tw)/(tu + 0.001*tw)
    tau_appr = pc_tau_unp(n,gn,ln,pars='MRT')

    alpha_appr = (tau_appr - tw)/tu
    alpha_bar_appr = (tau_appr - tw)/(tu + 0.001*tw)
    import matplotlib.pyplot as plt
    scan=fx_scan(p,alpha,wfun='SCAN')
    scanl=fx_scan(p,alpha_appr,wfun='SCAN')
    r2scan=fx_scan(p,alpha_bar,wfun='r2SCAN')
    r2scanl=fx_scan(p,alpha_bar_appr,wfun='r2SCAN')
    plt.plot(x/lf,scan)
    plt.plot(x/lf,scanl)
    #plt.plot(x/lf,r2scan)
    #plt.plot(x/lf,r2scanl)
    plt.xlabel('$x/\lambda_F$')
    #plt.title('r$^2$SCAN $f_x(\\bar{\\alpha})$ (blue) and r$^2$SCAN-L $f_x(\\bar{\\alpha})$ (orange), $r_s=4$')
    plt.title('SCAN $f_x(\\alpha)$ (blue) and SCAN-L $f_x(\\alpha)$ (orange), $r_s=4$')
    plt.show()
    np.savetxt('./deorb_test_rs_4.csv',np.transpose((x,scan,scanl,r2scan,r2scanl)),delimiter=',',header='x,Fx SCAN,SCAN-L,r2 SCAN,r2 SCAN-L')
    exit()

    ap = 1.784720
    bp = 0.258304
    f1 = pc_interpolation(np.ones(1),ap,bp)[0]
    exp1 = np.exp(ap/(ap-1.0))
    exp2 = np.exp(ap)
    df1 = ap*bp*f1/(1.0 + exp1)/(exp2 + exp1) *(1.0/(ap-1)**2*exp1*(exp2 + exp1) \
        - (1 + exp1)*(1/(ap-1)**2*exp1 - exp2))
    print(f1,df1)
    exit()

    import matplotlib.pyplot as plt
    z = np.linspace(0.0,1.1,2000)
    theta = np.zeros(z.shape)
    eps = 1.e-2
    apar_fuzzy = 1.0-eps
    mid_mask = (eps < z) & (z < apar_fuzzy)
    zm = z[mid_mask]
    thmid = ((1.0 + np.exp(1.0/(1.0-zm)))/(np.exp(1.0/zm)+ np.exp(1.0/(1.0-zm))))**3
    theta[mid_mask] = thmid
    theta[z >= apar_fuzzy] = 1.0
    plt.plot(z,theta)
    plt.show()
