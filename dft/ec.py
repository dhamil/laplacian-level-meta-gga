import numpy as np

from constants import pi
import settings
from dft.tau import lap_lev_ke

gamma = (1.0 - np.log(2.0))/pi**2
beta0 = 0.066725

def spinf(z,pow):
    opz = np.minimum(2,np.maximum(0.0,1+z))
    omz = np.minimum(2,np.maximum(0.0,1-z))
    return (opz**pow + omz**pow)/2.0


def ec_den(wdfa,du,dd,gdu,gdd,gdt,tauu,taud,ldu,ldd,zps,ret_fc=False,oes=False,unp=False):

    n = du + dd
    rs = (3.0/(4.0*pi*n))**(1.0/3.0)
    if unp:
        z = 0.0
    else:
        z = np.minimum(np.maximum((du - dd)/n,-1.0),1.0)
    kf = (3*pi**2*n)**(1/3)
    p = (gdt/(2*kf*n))**2

    if wdfa[-2:]=='-L':
        """
            Deorbitalized functionals
        """
        tau = lap_lev_ke(settings.KEMOD,du,dd,gdu,gdd,ldu,ldd,zps,oes=oes,unp=unp)
        dfa = wdfa[:-2]
        """
        import matplotlib.pyplot as plt
        plt.plot(tau)
        plt.plot(tauu+taud)
        plt.show()
        plt.close()
        exit()
        """
    else:
        tau = tauu+taud
        dfa = wdfa

    tpss_regex = ['TPSS','revTPSS','TM']
    scan_regex = ['SCAN','rSCAN','r2SCAN','SCAN-L','rSCAN-L','r2SCAN-L','R201','AES']
    deorb_regex = ['SCAN-L','rSCAN-L','r2SCAN-L']
    needs_laplacian = ['LLMG']

    if dfa == "LSDA":
        eps_c = ec_pw92(z,rs)

    elif dfa == 'PW91':
        eps_c = ec_pw91(z,rs,p)

    elif dfa in ["PBE","PBEsol"]:
        eps_c = ec_pbe(z,rs,p,var=dfa)

    elif dfa == "ACGGA":
        eps_c = ec_acgga(z,rs,p)

    elif dfa == 'LYP':
        eps_c = ec_lyp_gga(du,dd,gdu,gdd,gdt)

    elif dfa == 'LLYP':
        eps_c = ec_lyp_lap_lev(du,dd,gdu,gdd,gdt,ldu,ldd,oes=oes)

    elif dfa in needs_laplacian:
        #q = (ldu+ldd)/(4*kf**2*n)
        eps_c = ec_pbe(z,rs,p,var='ACGGA')

    elif dfa in tpss_regex:

        eps_c = ec_tpss(du,dd,gdu,gdd,gdt,tauu+taud,rev=dfa,unp=unp,oes=oes)

    elif dfa in scan_regex:
        ds_zeta = spinf(z,5.0/3.0)
        t0 = 0.3*kf**2*n*ds_zeta
        tauw = gdt**2/(8*n)

        if dfa == 'SCAN':
            alpha = np.maximum((tau - tauw)/t0,0.0)
        elif dfa == 'rSCAN':
            alpha_r = 1.e-3
            tau_r = 1.e-4
            alpha = np.maximum((tau - tauw)/(t0 + tau_r*ds_zeta),0.0)
            alpha = alpha**3/(alpha**2 + alpha_r)
        elif dfa == 'r2SCAN':
            if settings.routine['reopt_r2']:
                eta = zps['c1']
            else:
                eta = 1.e-3
            alpha = np.maximum((tau - tauw)/(t0 + eta*tauw),0.0)
        elif dfa in ['R201','AES']:
            eta = 1.e-2
            alpha = np.maximum((tau - tauw)/(t0 + eta*tauw),0.0)
        eps_c = ec_scan(z,rs,p,alpha,zps,wfun=(dfa).split('-')[0])

    else:
        raise ValueError(("Invalid correlation functional {:}").format(dfa))

    if ret_fc:
        return eps_c/eps_x_unif(rs)
    else:
        return eps_c*n

def eps_x_unif(rs):
    ax = -3.0/(4.0*pi)
    kf = (9*pi/4.0)**(1.0/3.0)/rs
    return ax*kf

def ec_pw92(z,rs,ret_derivs=False):
    """
        [PW1992]
    """
    def g(v,rs):
        q0 = -2.0*v[0]*(1.0 + v[1]*rs)
        q1 = 2.0*v[0]*(v[2]*rs**(0.5) + v[3]*rs + v[4]*rs**(1.5) + v[5]*rs**2)
        q1p = v[0]*(v[2]*rs**(-0.5) + 2*v[3] + 3*v[4]*rs**(0.5) + 4*v[5]*rs)
        dg = -2*v[0]*v[1]*np.log(1.0 + 1.0/q1) - q0*q1p/(q1**2 + q1)
        return q0*np.log(1.0 + 1.0/q1),dg

    if not hasattr(z,'__len__') and z == 0.0:
        ec,d_ec_drs = g([0.031091,0.21370,7.5957,3.5876,1.6382,0.49294],rs)
        if ret_derivs:
            return ec,d_ec_drs
        else:
            return ec

    fz_den = (2.0**(4.0/3.0)-2.0)
    fdd0 = 8.0/9.0/fz_den
    fz = 2*(spinf(z,4/3) - 1)/fz_den

    ec0,d_ec_drs_0 = g([0.031091,0.21370,7.5957,3.5876,1.6382,0.49294],rs)
    ec1,d_ec_drs_1 = g([0.015545,0.20548,14.1189,6.1977,3.3662,0.62517],rs)
    ac,d_ac_drs = g([0.016887,0.11125,10.357,3.6231,0.88026,0.49671],rs)
    ec = ec0 - ac*fz/fdd0*(1.0 - z**4) + (ec1 - ec0)*fz*z**4

    if ret_derivs:
        d_ec_drs = d_ec_drs_0*(1.0 - fz*z**4) + d_ec_drs_1*fz*z**4
        d_ec_drs -= d_ac_drs*fz/fdd0*(1.0 - z**4)
        return ec,d_ec_drs
    else:
        return ec

def ec_pw91(z,rs,p):
    """ [P91] """
    def crs():
        """
            J.P. Perdew, Phys. Rev. B 33, 8822 (1986),
                https://doi.org/10.1103/PhysRevB.33.8822
        and
            M. Rasolt and D.J.W. Geldart, Phys. Rev. B 34, 1325 (1986),
                https://doi.org/10.1103/PhysRevB.34.1325
        """
        ca = 0.023266
        cb = 7.389e-6
        cbd = 7.389e-2
        cc = 8.723
        cd = 0.472
        mcx = 0.001667
        cnum = 0.002568 + rs*(ca + rs*cb)
        cden = 1 + rs*(cc + rs*(cd + rs*cbd))
        return mcx + cnum/cden

    alpha = 0.09
    c0 = 0.004235
    vv = 16*(3/pi)**(1/3)
    beta = vv*c0
    gamma = 2*alpha/beta

    phi = spinf(z,2/3)
    phi3 = phi**3
    eclsda = ec_pw92(z,rs)

    t2 = (3*pi**2/16)**(2/3)*p/(phi**2*rs)
    fa = gamma/np.expm1(-gamma*eclsda/(phi3*beta))
    fti = t2*(1 + fa*t2)/(1 + fa*(t2 + fa*t2))
    h0 = phi3*beta/gamma*np.log(1 + gamma*fti)

    kf = (9*pi/4)**(1/3)/rs
    sk2 = 4/(pi*kf) # = (ks/kf)**2
    h1 = vv*(crs() - c0)*phi3*t2*np.exp(-100*phi*phi3*sk2*t2)

    return eclsda + h0 + h1

def ec_pbe(z,rs,p,var='PBE'):
    """
        PBE: [PBE1996]
        PBEsol: [P2008]
    """
    phi = spinf(z,2/3)
    gp3 = gamma*phi**3
    t2 = (3.0*pi**2/16.0)**(2.0/3.0)*p/(phi**2*rs)

    ecl = ec_pw92(z,rs)
    beta = {'PBE': beta0, 'PBEsol': 0.046,
    'revTPSS': beta0*(1 + 0.1*rs)/(1 + 0.1778*rs),
    'ACGGA': beta0*(1 + 0.5*rs + 0.083335*rs**2)/(1 + 0.5*rs + 0.148165*rs**2)
    }

    aa = beta[var]/gamma/np.expm1(-ecl/gp3)
    v = aa*t2
    v2 = v**2

    gt = t2*(1.0 + v)/(1.0 + v + v2)
    hc = 1.0 + beta[var]/gamma*gt

    ec = ecl + gp3*np.log(hc)

    return ec

def ec_acgga(z,rs,p):
    """
        [CCKB2018]
    """

    ca = 0.5
    cb = 1.0
    cc = 0.16667
    cd = 0.29633

    eta = 1.0/4.5
    ctil = 1.467

    phi = spinf(z,2/3)
    gp3 = gamma*phi**3
    t2 = (3.0*pi**2/16.0)**(2.0/3.0)*p/(phi**2*rs)
    t = t2**(0.5)
    pt = (1.0 + eta*t)/(1.0 + ctil*eta*t)

    brs = beta0*(1.0 + ca*rs*(cb + cc*rs))/(1.0 + ca*rs*(1.0 + cd*rs))

    ttil2 = t2*pt*brs/beta0

    ecl = ec_pw92(z,rs)

    aa = beta0/gamma/np.expm1(-ecl/gp3)
    v = aa*ttil2
    v2 = v**2
    gt = ttil2*(1.0 + v)/(1.0 + v + v2)
    hc = 1.0 + beta0/gamma*gt
    ec = ecl + gp3*np.log(hc)

    return ec

def ec_scan(z,rs,p,alpha,zps,wfun='SCAN'):
    """
        SCAN: [SRP2015]
        rSCAN: [BY2019]
        r2 SCAN: [F2020]
    """

    phi = spinf(z,2.0/3.0)
    gp3 = gamma*phi**3
    t2 = (3.0*pi**2/16.0)**(2.0/3.0)*p/(phi**2*rs)

    b1c = 0.0285764
    b2c = 0.0889
    b3c = 0.125541
    ec0_lda = -b1c/(1.0 + b2c*rs**(0.5) + b3c*rs)

    hx0 = 1.174
    dx_zeta = spinf(z,4.0/3.0)
    bfac = hx0*(3.0/4.0*(3.0/(2.0*pi))**(2.0/3.0))*b3c/b1c
    gc_zeta = (1.0 - bfac*(dx_zeta - 1.0)) * (1.0 - z**(12))

    ec0 = gc_zeta*ec0_lda
    if wfun in ['r2SCAN','R201','AES']:
        d_ec0_drs = gc_zeta*b1c/(1.0 + b2c*rs**(0.5) + b3c*rs)**2
        d_ec0_drs *= 0.5*b2c/rs**(0.5) + b3c
        ecl, d_ecl_drs = ec_pw92(z,rs,ret_derivs=True)
    else:
        ecl = ec_pw92(z,rs)

    chi_inf = 0.12802585262625818#(3*pi**2/16.0)**(2.0/3.0)*beta0/(1.778*(0.9 - 3*(3/(16.0*pi))**(2.0/3.0)))
    g_inf = 1.0 - 1.0/(1.0 + 4*chi_inf*p)**(0.25)
    w0 = np.expm1(-ec0_lda/b1c)
    h0 = b1c*gc_zeta*np.log(1.0 + w0*g_inf)

    eps0 = ec0 + h0

    brs = beta0*(1.0 + 0.1*rs)/(1.0 + 0.1778*rs)

    w = np.expm1(-ecl/gp3)
    aa = brs/gamma/w
    y = aa*t2

    if wfun in ['rSCAN','r2SCAN','R201','AES']:
        cfs = np.asarray([1.0,-0.64,-0.4352,-1.535685604549,3.061560252175,
            -1.915710236206,0.516884468372,-0.051848879792])

    if wfun in ['SCAN','rSCAN']:
        gt = 1.0 - 1.0/(1.0 + 4*y)**(0.25)
    if wfun in ['r2SCAN','R201','AES']:
        dp2 = 0.361
        if wfun == 'r2SCAN':
            if settings.routine['reopt_r2']:
                dp2 = zps['c2']
                eta = zps['c1']
            else:
                eta = 1.e-3
        elif wfun in ['R201','AES']:
            eta = 1.e-2
        ds_zeta = spinf(z,5.0/3.0)
        delta_f_c2 = -0.7114023342889984#np.dot([i for i in range(1,8)],cfs[1:])
        delta_y = delta_f_c2/(27.0*ds_zeta*gp3*w)
        delta_y *= 20*rs*(d_ec0_drs - d_ecl_drs) - 45*eta*(ec0 - ecl)
        delta_y *= p*np.exp(-(p/dp2**2)**2)
        tmp = y-delta_y
        if len(tmp[tmp<0]) > 0:
            #print('WARNING, negative y - delta_y in r2 SCAN C!')
            return 1e20
        gt = 1.0 - 1.0/(1.0 + 4*(y - delta_y))**(0.25)

    h = gp3*np.log(1.0 + w*gt)

    eps1 = ecl + h

    ief = np.zeros(alpha.shape)
    c1 = 0.64
    c2 = 1.5
    d1 = 0.7
    oma = 1.0 - alpha
    if wfun == 'SCAN':
        msk = alpha < 1.0
        ief[msk] = np.exp(-c1*alpha[msk]/oma[msk])
        msk = alpha > 1.0
        ief[msk] = -d1*np.exp(c2/oma[msk])
    elif wfun in ['rSCAN','r2SCAN','R201','AES']:
        msk = alpha <= 2.5
        am = alpha[msk]
        for i in range(len(cfs)):
            ief[msk] += cfs[i]*am**i

        msk = alpha > 2.5
        ief[msk] = -d1*np.exp(c2/oma[msk])

    epsc = eps1 + ief*(eps0 - eps1)

    if wfun == 'AES':
        fscl = np.zeros_like(alpha)
        amsk = alpha < 1.0
        #fscl[amsk] = np.exp(-zps['c0']*alpha[amsk]**2/(1 - alpha[amsk]))
        fscl[amsk] = ((1-alpha[amsk])/(1+zps['c0']*alpha[amsk]))**3
        epsc = (1 - fscl)*epsc + fscl*ecl

    return epsc

def ec_tpss(nu,nd,gnu,gnd,gn,tau,rev='TPSS',unp=False,oes=False):

    n = nu + nd
    rs = (3.0/(4.0*pi*n))**(1.0/3.0)
    # z = zeta = relative spin polarization
    if unp:
        z = 0.0
        zr = 0.0
    else:
        z = np.minimum(np.maximum((nu - nd)/n,-1.0),1.0)
        zr = np.minimum(np.maximum((nu - nd)/n,-0.99999999999990),0.99999999999990)
    kf = (3*pi**2*n)**(1/3)
    p = (gn/(2*kf*n))**2

    if unp or oes:
        # speed things up: for a fully spin-polarzed or spin-unpolarized density, |grad zeta| = 0
        ksi = np.zeros_like(p)
    else:
        isect = gn**2 - gnu**2 - gnd**2
        # gz = |grad zeta|
        gz = 2/n**2*((nd**2*gnu**2 + nu**2*gnd**2 - nu*nd*isect)**2)**(0.25)
        ksi = gz/(2*kf)

    # zz = tau_W/tau
    zz = gn**2/(8*n)/tau

    z2 = z**2
    if rev == 'TPSS':
        cz0 = 0.53 + z2*(0.87 + z2*(0.5 + 2.26*z2))
        ecpbe = ec_pbe(z,rs,p,var='PBE')
    elif rev == 'revTPSS':
        cz0 = 0.59 + z2*(0.9269 + z2*(0.6225 + 2.154*z2))
        ecpbe = ec_pbe(z,rs,p,var='revTPSS')
    elif rev == 'TM':
        cz0 = z2*(0.1 + 0.32*z2)
        ecpbe = ec_pbe(z,rs,p,var='PBE')
    czz = cz0/(1 + ksi**2*spinf(zr,-4/3))**4

    rsu = (3/(4*pi*nu))**(1/3)
    pu = (gnu/(2*(3*pi**2)**(1/3)*nu**(4/3)))**2

    if not oes:
        rsd = (3/(4*pi*nd))**(1/3)
        pd = (gnd/(2*(3*pi**2)**(1/3)*nd**(4/3)))**2

    if rev in ['TPSS','TM']:
        ecu = ec_pbe(1,rsu,pu,var='PBE')
        if oes:
            ecd = 0.0
        else:
            ecd = ec_pbe(1,rsd,pd,var='PBE')
    elif rev == 'revTPSS':
        ecu = ec_pbe(1,rsu,pu,var='revTPSS')
        if oes:
            ecd = 0.0
        else:
            ecd = ec_pbe(1,rsd,pd,var='revTPSS')
    ecu = np.maximum(ecu,ecpbe)
    ecd = np.maximum(ecd,ecpbe)

    ec = ecpbe*(1 + czz*zz**2) - (1 + czz)*zz**2*(nu*ecu + nd*ecd)/n

    ecm = ec*(1 + 2.8*ec*zz**3)

    return ecm

def ec_lyp_lap_lev(n0,n1,gn0,gn1,gn,l0,l1,oes=False):

    """
        C. Lee, W. Yang, and R. Parr,
            ``Development of the Colle-Salvetti correlation-energy formula
            into a functional of the electron density''
            Phys. Rev. B 37, 785 (1988),
            doi: 10.1103/PhysRevB.37.785
    """
    a = 0.04918
    b = 0.132
    c = 0.2533
    d = 0.349

    n = n0 + n1
    lan = l0 + l1
    # Not the Weizsacker KED, Eq. 8
    tw_mod = gn**2/(8*n) - lan/8
    tw0 = gn0**2/(8*n0) - l0/8
    if oes:
        tw1 = np.zeros_like(n0)
    else:
        tw1 = gn1**2/(8*n1) - l1/8

    # Eq. 13
    gam = 2*(1 - (n0**2 + n1**2)/n**2)

    cfsr = 3/10*(6*pi**2)**(2/3)

    # Eq. 22
    t1 = cfsr*(n0**(8/3) + n1**(8/3))
    t2 = (n0*tw0 + n1*tw1 )/9 - n*tw_mod
    t3 = (n0*l0 + n1*l1)/18
    t4 = 2*b*n**(-8/3)*(t1 + t2 + t3)*np.exp(-c*n**(-1/3))

    return -a*gam*( 1 + t4 )/(1 + d*n**(-1/3))

def ec_lyp_gga(n0,n1,gn0,gn1,gn):

    """
        C. Lee, W. Yang, and R. Parr,
            ``Development of the Colle-Salvetti correlation-energy formula
            into a functional of the electron density''
            Phys. Rev. B 37, 785 (1988),
            doi: 10.1103/PhysRevB.37.785

        and

        B. Miehlich, A. Savin, H. Stoll, and H. Preuss,
            ``Results obtained with the correlation energy density
            functionals of Becke and Lee, Yang, and Parr''
            Chem. Phys. Lett. 157, 200 (1989),
            doi: 10.1016/0009-2614(89)87234-3
    """
    a = 0.04918
    b = 0.132
    c = 0.2533
    d = 0.349
    cf = 3/10*(3*pi**2)**(2/3)

    n = n0 + n1
    nmt = n**(-1/3)
    t1 = 2**(11/3)*cf*( n0**(8/3) + n1**(8/3) )

    delta = c*nmt + d*nmt/(1 + d*nmt)
    t2 = (47 - 7*delta)*gn**2/18
    t3 = -(5/2 - delta/18)*(gn0**2 + gn1**2)
    t4 = -(delta - 11)*(n0*gn0**2 + n1*gn1**2)/(9*n)

    t5 = -2/3*(n*gn)**2 + (2/3*n**2 - n0**2)*gn1**2 + (2/3*n**2 - n1**2)*gn0**2

    u1 = -4*a*n0*n1/(1 + d*nmt)/n

    omega = np.exp(-c*nmt)/(1 + d*nmt)*nmt**(11)
    u2 = -a*b*omega*(n0*n1*(t1 + t2 + t3 + t4) + t5 )
    ec = u1 + u2

    return ec/n
