import numpy as np

from constants import pi
from dft.tau import get_r2_ge4_corr

def lap_lev_ke_unp_w_derivs(wke,n,gn,lap,zps):

    kf = (3*pi**2*n)**(1/3)
    kf2 = kf*kf

    tau0 = 0.3*kf2*n
    d_tau0_dn = kf2/2

    p = (gn/(2*kf*n))**2
    d_p_dn = -8/3*p/n
    d_p_dgn = gn/(2*kf2*n**2)

    q = lap/(4*kf2*n)
    d_q_dn = -5/3*q/n
    d_q_dlap = 1/(4*kf2*n)

    if wke == 'GEA2':
        ft = 1 + 5/27*p + 20/9*q
        d_ft_dp = 5/27
        d_ft_dq = 20/9

    elif wke == 'PC':
        # [PC2007]
        ft, d_ft_dp, d_ft_dq = ft_pc_w_derivs(p,q,0.5389,3.0)

    elif wke == 'MRT':
        # [MRT2017]
        ft, d_ft_dp, d_ft_dq = ft_pc_w_derivs(p,q,1.784720,0.258304)

    elif wke == 'RPP1':

        ft, d_ft_dp, d_ft_dq = ft_rpp_w_derivs(p,q,zps)

    elif wke[:2] == 'HL':
        ft, d_ft_dp, d_ft_dq = ft_hl_w_derivs(p,q,int(wke[2:]))

    else:
        raise SystemExit('KED functional {:} not implemented yet'.format(wke))

    tau = tau0*ft

    dtdn = ft*d_tau0_dn + tau0*(d_ft_dp*d_p_dn + d_ft_dq*d_q_dn)
    # NB: tau0*d_p_dgn = 3/20 * |grad n|/n = 0.15 * |grad n|/n
    dtdgn = 0.15*gn/n*d_ft_dp
    # NB: tau0*d_q_dln = 3/40 = 0.075
    dtdlap = 0.075*d_ft_dq

    return tau, dtdn, dtdgn, dtdlap

def ft_pc_w_derivs(p,q,ca,cb):

    ftw = 5*p/3
    d_ftw_dp = 5/3
    opftw = 1 + ftw

    ft2 = 5/27*p + 20/9*q
    d_ft2_dp = 5/27
    d_ft2_dq = 20/9

    ft4 = 8/81*q**2 - p*q/9 + 8/243*p**2
    d_ft4_dp = -q/9 + 16/243*p
    d_ft4_dq = 16/81*q -p/9

    freg = (1 + (ft4/opftw)**2)**(0.5)
    d_freg_dp = ft4*(d_ft4_dp - ft4*d_ftw_dp/opftw)/(freg*opftw**2)
    d_freg_dq = ft4*d_ft4_dq/(freg*opftw**2)

    ftge4 = (1 + ft2 + ft4)/freg
    d_ftge4_dp = (d_ft2_dp + d_ft4_dp - ftge4*d_freg_dp)/freg
    d_ftge4_dq = (d_ft2_dq + d_ft4_dq - ftge4*d_freg_dq)/freg

    z = ftge4 - ftw
    d_z_dp = d_ftge4_dp - d_ftw_dp
    d_z_dq = d_ftge4_dq

    theta, d_theta_dz = pc_interp_w_derivs(z,ca,cb)

    ft = ftw + z*theta
    d_ft_dp = d_ftw_dp + (d_theta_dz*z + theta)*d_z_dp
    d_ft_dq = (d_theta_dz*z + theta)*d_z_dq

    return ft, d_ft_dp, d_ft_dq


def pc_interp_w_derivs(z,ca,cb):

    eps = 1.e-2
    a_fuzzy = ca-eps
    theta = np.zeros_like(z)
    d_theta_dz = np.zeros_like(z)

    zmsk = (eps < z) & (z < a_fuzzy)
    zm = z[zmsk]

    fac1 = np.exp(ca/(ca - zm))
    dfac1 = ca/(ca - zm)**2*fac1

    fac2 = np.exp(ca/zm)
    dfac2 = -ca/zm**2*fac2

    thetac = (1 + fac1)/(fac2 + fac1)
    theta[zmsk] = thetac**cb
    d_theta_dz[zmsk] = cb*thetac**(cb - 1) * ( dfac1*(fac2 - 1)  \
        - dfac2*(1 + fac1) )/(fac2 + fac1)**2

    theta[z >= a_fuzzy] = 1.0

    return theta, d_theta_dz


def ft_rpp_w_derivs(p,q,zps):

    cqq,cpq,cpp = get_r2_ge4_corr()

    f4 = (cpp-zps['c3'])*p**2 + cpq*p*q + cqq*q**2
    d_f4_dp = 2*(cpp-zps['c3'])*p + cpq*q
    d_f4_dq = cpq*p + 2*cqq*q

    f4damp = np.exp(-(p/zps['c1'])**2 - (q/zps['c2'])**2)
    d_f4damp_dp = -2*p/zps['c1']**2*f4damp
    d_f4damp_dq = -2*q/zps['c2']**2*f4damp

    f4c_exp = np.exp(-zps['c3']*p)
    f4c = zps['c3']*p**2*f4c_exp
    d_f4c_dp = zps['c3']*p*(2 - zps['c3']*p)*f4c_exp

    x = 1 - 40/27*p + 20/9*q + f4c + f4*f4damp
    d_x_dp = -40/27 + d_f4c_dp + d_f4_dp*f4damp + f4*d_f4damp_dp
    d_x_dq = 20/9 + d_f4_dq*f4damp + f4*d_f4damp_dq

    alpha = np.zeros_like(p)
    d_alpha_dx = np.zeros_like(p)

    x0 = zps['c0']
    xmsk = (0 <= x) & (x <= x0)
    xm = x[xmsk]

    ca = 20/x0**3
    cb = -45/x0**4
    cc = 36./x0**5
    cd = -10./x0**6

    alpha[xmsk] = xm**4*(ca + xm*(cb + xm*(cc + xm*cd)))
    d_alpha_dx[xmsk] = xm**3*(4*ca + xm*(5*cb + xm*(6*cc + 7*xm*cd)))

    xmsk = x > x0
    alpha[xmsk] = x[xmsk]
    d_alpha_dx[xmsk] = 1.0

    ft = 5*p/3 + alpha
    d_ft_dp = 5/3 + d_alpha_dx*d_x_dp
    d_ft_dq = d_alpha_dx*d_x_dq

    return ft, d_ft_dp, d_ft_dq

def ft_hl_w_derivs(p,q,npar):
    pard = {
        # these use GE2 ingredients
        3 : [-2.4153571096515076, 1.9965618896773247, 0.24130250873900594],
        4 : [-5.625916373547513, 10.802436076371524, -7.770489376090032, \
            3.832601020621118],
        6 : [0.41734330440209316, -26.865089294441745, 75.20427919702495, \
            -83.96867704976964, 52.17930398327887, 0.6775117229296242],
        11 : [-1.2671660247767116, -11.922250743093565, 22.386020165089914, \
            10.69242899231974, -35.98119268664415, 34.89146499613219, \
            1.0511772339901142, -4.812118332316358, 1.3178981129806067, \
            -0.13854891212453904, 0.005117342685216863]
    }

    fw = 5*p/3
    d_fw_dp = 5/3

    z = 1 + 20*q/9 - 40*p/27
    d_z_dp = -40/27
    d_z_dq = 20/9

    th,dthdz = theta_model_w_derivs(z,pard[npar])

    ft = fw + z*th
    d_ft_dp = d_fw_dp + (th + z*dthdz)*d_z_dp
    d_ft_dq = (th + z*dthdz)*d_z_dq
    return ft, d_ft_dp, d_ft_dq


def theta_model_w_derivs(z,c):
    npar = len(c)
    theta = np.zeros_like(z)
    d_theta_dz = np.zeros_like(z)
    zmsk = z >= 0.0
    zm = z[zmsk]
    b1 = 3
    b2 = 0.0
    for i in range(npar):
        b1 += (5-i-1)*c[i]
        b2 += c[i]
    b2 -= b1
    tden = np.ones_like(zm)
    d_tden_dz = np.zeros_like(zm)
    for i in range(len(c)):
        tden += c[i]*zm**(1+i)
        d_tden_dz += (1+i)*c[i]*zm**i

    theta[zmsk] = zm**3*(1 + b1*zm + b2*zm**2)/tden
    d_theta_dz[zmsk] = ( zm**2*(3 + 4*b1*zm + 5*b2*zm**2) - theta[zmsk]*d_tden_dz )/tden

    return theta, d_theta_dz
