import numpy as np

pi = 3.141592653589793238462643383279502884197169399375105820974944592307816406286198
scan_x_pars = {'C1':  0.667, 'C2': 0.8,'D': 1.24}
scan_c_pars = {'C1':  0.64, 'C2': 1.5,'D': .7}
match_pt = 2.5
gamma = (1.0 - np.log(2.0))/pi**2


def scan2_x_pars(eta):
    K0 = 0.174
    fa = 100*(4 + 9*eta)*K0/243
    fb = (200 + 6*K0*(1 + 441*eta))/729
    fc = (657*eta - 511)/6075

    f1 = (-fb - (fb**2 - 4*fa*fc)**(0.5) )/(2*fa)
    f2 = 73/(2500*K0) - f1/50
    h1 = 10/81 + 5*(4 + 9*eta)*K0*f1/27
    h2 = -2*(K0*f1*(6 + 75*eta*(8 + 9*eta))/243 + f1*h1*5*(8 + 9*eta)/27 + K0*f2*25/1458*(8 + 9*eta)**2)
    return f1,f2,h1,h2

def scan_ief_lower(x,sps):
    ief = np.exp(-sps['C1']*x/(1-x))
    dief = -ief*sps['C1']/(1-x)**2
    ddief = ief*sps['C1']/(1-x)**4*(sps['C1'] - 2 + 2*x)
    return ief,dief,ddief

def scan_ief_upper(x,sps):
    omx = 1-x
    ief = -sps['D']*np.exp(sps['C2']/omx)
    d1 = ief*sps['C2']/omx**2
    d2 = (d1 + 2*ief/omx )*sps['C2']/omx**2
    d3 = (d2 + 2*d1/omx + 2*ief/omx**2)*sps['C2']/omx**2 \
        + 2*d2/omx
    d4 = (d3 + 2*d2/omx + 4*d1/omx**2 + 4*ief/omx**3)*sps['C2']/omx**2 \
        + 2*(d2 + 2*d1/omx + 2*ief/omx**2)*sps['C2']/omx**3 \
        + 2*d3/omx + 2*d2/omx**2
    return ief,d1,d2,d3,d4

def scan_interp(x,sps):
    ief = np.zeros(x.shape)
    ief[x < 1],_,_ = scan_ief_lower(x[x < 1],sps)
    ief[x > 1],_,_,_,_ = scan_ief_upper(x[x > 1],sps)
    return ief

def rscan_interp(x,wief):
    if wief.lower() == 'x':
        cl = np.asarray([1.0,-0.667,-0.4445555,-0.663086601049, 1.451297044490, \
            -0.887998041597, 0.234528941479,-0.023185843322])
        sps = scan_x_pars
    elif wief.lower() == 'c':
        cl = np.asarray([1.0,-0.64,-0.4352,-1.535685604549,3.061560252175,\
            -1.915710236206,0.516884468372,-0.051848879792])
        sps = scan_c_pars

    ief = np.zeros(x.shape)
    lmask = x <= 2.5
    umask = x > 2.5

    xm = x[lmask]
    for i in range(8):
        ief[lmask] += cl[i]*xm**i

    ief[umask] = -sps['D']*np.exp(sps['C2']/(1-x[umask]))
    return ief

def dpoly(x,n):
    df = np.zeros(n.shape)
    nmask = n>0
    df[nmask] = n[nmask]*x**(n[nmask]-1)
    return df

def poly_interp_mat_coeff(x,nc):

    i = np.arange(1,nc,1)
    f = (1 - x)*x**i

    df1 = dpoly(x,i) - dpoly(x,i+1)
    df2 = i*dpoly(x,i-1) - (i+1)*dpoly(x,i)
    df3 = i* ( (i-1)*dpoly(x,i-2) - (i+1)*dpoly(x,i-1) )
    df4 = i*(i-1)*( (i-2)*dpoly(x,i-3) - (i+1)*dpoly(x,i-2) )

    return f,df1,df2,df3,df4

def get_coeff_mat(eta,wex):

    if wex.lower() == 'x':
        nc = 10
        spars = scan_x_pars
    elif wex.lower() == 'c':
        nc = 8
        spars = scan_c_pars

    ind = 0
    cmat = np.zeros((nc-1,nc-1))
    res = np.zeros(nc-1)

    # match SCAN X first and second derivs for alpha = 0

    _,cmat[ind],cmat[ind+1],_,_ = poly_interp_mat_coeff(0.0,nc)
    _,res[ind],res[ind+1] = scan_ief_lower(0.0,spars)
    res[ind] += 1

    ind += 2
    if wex.lower() == 'x':
        # fix first and second derivs at alpha = 1
        _,cmat[ind],cmat[ind+1],_,_ = poly_interp_mat_coeff(1.0,nc)
        res[ind],res[ind+1],_,_ = scan2_x_pars(eta)
        res[ind] += 1
        ind += 2

    # continuity up to fourth derivs with SCAN X alpha > 1 interpolation function
    # at alpha = 2.5, for consistency with r2 SCAN C
    cmat[ind],cmat[ind+1],cmat[ind+2],cmat[ind+3],cmat[ind+4] = \
        poly_interp_mat_coeff(match_pt,nc)
    res[ind],res[ind+1],res[ind+2],res[ind+3],res[ind+4] = \
        scan_ief_upper(match_pt,spars)
    res[ind] += match_pt-1
    res[ind+1] += 1.0

    return cmat,res

def get_coeffs(eta,wex):

    cmat,res = get_coeff_mat(eta,wex)
    #print('Coefficient matrix:')
    #for i in range(cmat.shape[0]):
    #    print(cmat[i])

    pars = np.zeros(res.shape[0]+1)
    pars[0] = 1.0
    pars[1:] = np.linalg.solve(cmat,res)

    #print(np.dot(cmat,pars)-res)
    #exit()

    str = '{:}: ['.format(wex.upper())
    for i in range(pars.shape[0]):
        str += '{:},'.format(pars[i])
    str = str[:-1] +']'
    #print(str)

    return pars

def new_interp(x,c,wex):
    if wex.lower() == 'x':
        sps = scan_x_pars
    elif wex.lower() == 'c':
        sps = scan_c_pars

    ief = np.zeros(x.shape)

    lmask = x <= match_pt
    umask = x > match_pt
    xm = x[lmask]
    for i in range(c.shape[0]):
        ief[lmask] += c[i]*xm**i
    ief[lmask] *= (1-xm)

    ief[umask] = -sps['D']*np.exp(sps['C2']/(1-x[umask]))
    return ief


def ex_r4scan(nup,ndn,gnup,gndn,tauup,taudn,xps,unp=False):

    if unp:
        return ex_r4scan_unp(2*nup,2*gnup,2*tauup,xps)
    ex_up = ex_r4scan_unp(2*nup,2*gnup,2*tauup,xps)
    ex_dn = ex_r4scan_unp(2*ndn,2*gndn,2*taudn,xps)
    return (ex_up + ex_dn)/2

def ex_r4scan_unp(n,gn,tau,xps):

    kf = (3*pi**2*n)**(1/3)
    p = (gn/(2*kf*n))**2
    tauw = gn**2/(8*n)
    tau0 = 0.3*kf**2*n

    alpha = (tau - tauw)/(tau0 + xps['eta']*tauw)
    alpha[alpha < 0.0] = 0.0

    ex0 = -3/(4*pi)*kf*n
    fx = fx_r4scan(p,alpha,xps)
    return fx*ex0


def fx_r4scan(p,alpha,xps):

    hx0 = 1.174
    k0 = 0.174
    uak = 10/81

    f1,f2,h1,h2 = scan2_x_pars(xps['eta'])

    #hx1 = (1 + uak*p + uak* (uak - h1)*p**2 + (1 + xps['k1'])*(p/xps['dp'])**3)\
    #    /(1 + (uak - h1)*p  + ( (uak - h2)**2 - h2/2)*p**2 + (p/xps['dp'])**3)
    #hx1 = (1 + h1*p + h2*p**2/2 + (1+xps['k1'])*(p/xps['dp'])**3)/(1 + (p/xps['dp'])**3)
    #x = ( (h1 - uak + (h2/2 + h1**2/xps['k1'])*p)*np.exp(-p**2/xps['dp']**4) + uak)*p
    #hx1 = 1 + xps['k1'] - xps['k1']/(1 + x/xps['k1'])
    hx1 = (1 + h1*p + (1 + h2/2)*p**2)/(1 + p**2)

    """
    alpha2 = alpha*alpha
    alpha3 = alpha*alpha2
    alpha4 = alpha2*alpha2

    # interpolation function without control of fx'(0)
    c1 = -2 + f2 + xps['dx']
    c2 = 1 - 2*f1 - f2 - 2*xps['dx']
    ief = (1-alpha)*(1 + c1*alpha + c2*alpha2 + xps['dx']*alpha*alpha2)/(1 + alpha4)
    """
    """
    # interpolation function with control of fx'(0)
    c1 = 1 + xps['fx0']
    c2 = -5 - 2*xps['fx0'] - f1 + f2 + xps['dx']
    c3 = 3 + xps['fx0'] - f1 - f2 - 2*xps['dx']
    ief = (1-alpha)*(1 + c1*alpha + c2*alpha2 + c3*alpha3 + xps['dx']*alpha4)/(1 + alpha4*alpha)
    """
    """
    c1 = 1 + xps['fx0']
    c4 = .1
    c2 = -5 + c4*xps['dx'] - 2*xps['fx0'] + (2*c4-3)*f1 + (1 + c4)*f2/2
    c3 = 3 - 2*c4*xps['dx'] + xps['fx0'] + (2 - 3*c4)*f1 - (1 + c4)*f2/2
    ief = (1 - alpha)*(1 + c1*alpha + c2*alpha2 + c3*alpha3 + xps['dx']*c4*alpha4)/(1 + alpha*alpha4*c4)
    """
    #cl = get_coeffs(xps['eta'],'x')
    #ief = new_interp(alpha,cl,'x')
    c1 = (f2 - f1 - 2)
    c2 = (-f2 - f1 + 1)
    ief = (1 - alpha)*(1 + c1*alpha + c2*alpha**2)/(1 + alpha**3)

    gx = np.ones(p.shape)
    gx[p > 1.e-16] = 1.0 - np.exp(-xps['ax']/p[p > 1.e-16]**(0.25))
    #gx = 1/(1 + (p/3.9533280025015105)**4)**(1/16)

    fx = (hx1 + ief*(hx0 - hx1))*gx

    return fx

def spinf(z,pow):
    opz = np.minimum(2,np.maximum(0.0,1+z))
    omz = np.minimum(2,np.maximum(0.0,1-z))
    return (opz**pow + omz**pow)/2.0

def ec_den_r4scan(nu,nd,gnu,gnd,gn,tau,cps):

    n = nu + nd
    zeta = np.minimum(1.0,np.maximum(-1.0,(nu-nd)/n))
    rs = (3/(4*pi*n))**(1/3)
    kf = (3*pi**2*n)**(1/3)
    p = (gn/(2*kf*n))**2

    tauw = gn**2/(8*n)
    #tauw = np.zeros(nu.shape)
    #tauw[nu>0.0] = gnu[nu>0.0]**2/(8*nu[nu>0.0])
    #tauw[nd>0.0] += gnd[nd>0.0]**2/(8*nd[nd>0.0])
    tau0 = 0.3*kf**2*n*spinf(zeta,5/3)

    alpha = (tau - tauw)/(tau0 + cps['eta']*tauw)
    alpha[alpha < 0.0] = 0.0

    ec = ec_r4scan(zeta,rs,p,alpha,cps)

    return n*ec

def ec_r4scan(z,rs,p,alpha,cps):

    beta0 = 0.066724526517184

    beta1 = 0.5
    beta2 = 0.083335
    beta3 = 0.148165
    brs = beta0*(1 + beta1*rs + beta2*rs**2)/(1 + beta1*rs + beta3*rs**2)
    beta_inf = beta0*beta2/beta3
    """
    beta1 = 0.1
    beta2 = 0.1778
    brs = beta0*(1 + beta1*rs)/(1 + beta2*rs)
    beta_inf = beta0*beta1/beta2
    """

    gamma = 0.0310906908696549#(1- np.log(2))/pi**2
    phi = spinf(z,2/3)
    gp3 = gamma*phi**3
    t2 = (3*pi**2/16)**(2/3)*p/(phi**2*rs)

    b1c = 0.028576417226272532#0.0285764
    b2c = 0.10754700015884122#0.0889
    b3c = 0.12554127064929602#0.125541
    ec0_lda = -b1c/(1.0 + b2c*rs**(0.5) + b3c*rs)

    dx_zeta = spinf(z,4/3)
    bfac = 1.174*(3/4*(3/(2*pi))**(2/3))*b3c/b1c
    gc_zeta = (1 - bfac*(dx_zeta - 1)) * (1 - z**(12))

    chi_inf = (3*pi**2/16)**(2/3)*beta_inf/(0.9 - 3*(3/(16*pi))**(2/3))

    g_inf = 1/(1 + 4*chi_inf*p)**(0.25)
    w0 = np.expm1(-ec0_lda/b1c)
    h0 = b1c*gc_zeta*np.log(1 + w0*(1-g_inf))

    eps0 = (ec0_lda + h0)*gc_zeta

    ecl = ec_pw92(z,rs)
    w1 = np.expm1(-ecl/gp3)
    aa = brs/gamma/w1
    y = aa*t2

    gt = 1/(1 + 4*y + 10*y**2)**(0.25)

    h = gp3*np.log(1 + w1*(1 - gt))

    eps1 = ecl + h

    # interpolation function without control of fc'(0)
    #ief = (1 - alpha)**3/(1 + alpha**3/cps['dc'])

    # interpolation function with control of fc'(0)
    #alpha2 = alpha*alpha
    #ief = (1 - alpha)**3*(1 + (3 + cps['fc0'])*alpha + cps['dc']*alpha2)/(1 + alpha**5)
    f1,f2,_,_ = scan2_x_pars(cps['eta'])
    ief = (1 - alpha)**3*(1 + (f2 - f1)*alpha +  (-f2 - f1 + 1)*alpha**2)/(1 + alpha**5)

    epsc = eps1*(1 - ief) + ief*eps0
    return epsc

def ec_r2scan_mod(z,rs,p,alpha,cps):
    """
        SCAN: [SRP2015]
        rSCAN: [BY2019]
        r2 SCAN: [F2020]
    """

    phi = spinf(z,2.0/3.0)
    gp3 = gamma*phi**3
    t2 = (3.0*pi**2/16.0)**(2.0/3.0)*p/(phi**2*rs)

    beta0 = 0.066724526517184
    ca = 0.5
    cb = 1.0
    cc = 0.16667
    cd = 0.29633
    BETA_INF = beta0*cc/cd

    brs = beta0*(1.0 + ca*rs*(cb + cc*rs))/(1.0 + ca*rs*(1.0 + cd*rs))

    b1c = 0.028576417228595823
    b2c = 0.10754700025278864
    b3c = 0.12554127065950266
    ec0_lda = -b1c/(1.0 + b2c*rs**(0.5) + b3c*rs)

    hx0 = 1.174
    dx_zeta = spinf(z,4.0/3.0)
    bfac = hx0*(3.0/4.0*(3.0/(2.0*pi))**(2.0/3.0))*b3c/b1c
    gc_zeta = (1.0 - bfac*(dx_zeta - 1.0)) * (1.0 - z**(12))

    ec0 = gc_zeta*ec0_lda
    d_ec0_drs = gc_zeta*b1c/(1.0 + b2c*rs**(0.5) + b3c*rs)**2
    d_ec0_drs *= 0.5*b2c/rs**(0.5) + b3c
    ecl, d_ecl_drs = ec_pw92(z,rs,ret_derivs=True)

    chi_inf = (3*pi**2/16)**(2/3)*BETA_INF/(0.9 - 3*(3/(16*pi))**(2/3))
    g_inf = 1.0 - 1.0/(1.0 + 4*chi_inf*p)**(0.25)
    w0 = np.expm1(-ec0_lda/b1c)
    h0 = b1c*gc_zeta*np.log(1.0 + w0*g_inf)

    eps0 = ec0 + h0

    w = np.expm1(-ecl/gp3)
    aa = brs/gamma/w
    y = aa*t2

    dp2 = cps['dp']
    eta = cps['eta']
    ds_zeta = spinf(z,5.0/3.0)
    cl = get_coeffs(cps['eta'],'c')
    delta_f_c2 = -np.sum(cl)
    delta_y = delta_f_c2/(27.0*ds_zeta*gp3*w)
    delta_y *= 20*rs*(d_ec0_drs - d_ecl_drs) - 45*eta*(ec0 - ecl)
    delta_y *= p*np.exp(-p**2/dp2**4)
    gt = 1.0 - 1.0/(1.0 + 4*(y - delta_y))**(0.25)

    h = gp3*np.log(1.0 + w*gt)

    eps1 = ecl + h

    ief = new_interp(alpha,cl,'c')

    return eps1 + ief*(eps0 - eps1)


def ec_pw92(z,rs,ret_derivs=False):
    """
        [PW1992]
    """
    def g(v,rs):
        q0 = -2.0*v[0]*(1.0 + v[1]*rs)
        q1 = 2.0*v[0]*(v[2]*rs**(0.5) + v[3]*rs + v[4]*rs**(1.5) + v[5]*rs**2)
        q1p = v[0]*(v[2]*rs**(-0.5) + 2*v[3] + 3*v[4]*rs**(0.5) + 4*v[5]*rs)
        dg = -2*v[0]*v[1]*np.log(1.0 + 1.0/q1) - q0*q1p/(q1**2 + q1)
        return q0*np.log(1.0 + 1.0/q1),dg

    if not hasattr(z,'__len__') and z == 0.0:
        ec,d_ec_drs = g([0.031091,0.21370,7.5957,3.5876,1.6382,0.49294],rs)
        if ret_derivs:
            return ec,d_ec_drs
        else:
            return ec

    fz_den = (2.0**(4.0/3.0)-2.0)
    fdd0 = 8.0/9.0/fz_den
    fz = 2*(spinf(z,4/3) - 1)/fz_den

    ec0,d_ec_drs_0 = g([0.031091,0.21370,7.5957,3.5876,1.6382,0.49294],rs)
    ec1,d_ec_drs_1 = g([0.015545,0.20548,14.1189,6.1977,3.3662,0.62517],rs)
    ac,d_ac_drs = g([0.016887,0.11125,10.357,3.6231,0.88026,0.49671],rs)
    ec = ec0 - ac*fz/fdd0*(1.0 - z**4) + (ec1 - ec0)*fz*z**4

    if ret_derivs:
        d_ec_drs = d_ec_drs_0*(1.0 - fz*z**4) + d_ec_drs_1*fz*z**4
        d_ec_drs -= d_ac_drs*fz/fdd0*(1.0 - z**4)
        return ec,d_ec_drs
    else:
        return ec
