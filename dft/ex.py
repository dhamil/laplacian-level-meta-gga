import numpy as np

from constants import pi
import settings
from dft.tau import lap_lev_ke_unp,lap_lev_ft

def ex_den(dfa,du,dd,gdu,gdd,tauu,taud,ldu,ldd,zps,oes=False,unp=False):
    if unp:
        return ex_den_unp(dfa,2*du,2*gdu,2*tauu,2*ldu,zps)
    e_x_up = ex_den_unp(dfa,2*du,2*gdu,2*tauu,2*ldu,zps)
    if oes:
        e_x_dn = 0.0
    else:
        e_x_dn = ex_den_unp(dfa,2*dd,2*gdd,2*taud,2*ldd,zps)
    return (e_x_up + e_x_dn)/2.0

def ex_den_unp(wdfa,d,gd,t,lap,zps,fx_only=False):

    pbe_regex = ['PBE','PBEsol','acPBE']
    scan_regex = ['SCAN','rSCAN','r2SCAN','SCAN-L','rSCAN-L','r2SCAN-L','R201','AES']
    tpss_regex = ['TPSS','revTPSS']
    #deorb_regex = ['SCAN-L','rSCAN-L','r2SCAN-L']
    needs_laplacian = ['LLMG']

    kf = (3*pi**2*d)**(1/3)
    p = (gd/(2*kf*d))**2

    if wdfa[-2:]=='-L':
        dfa = wdfa[:-2]
        """
            Deorbitalized functionals
        """
        #q = lap/(4*kf**2*d)
        tau = lap_lev_ke_unp(settings.KEMOD,d,gd,lap,zps)
    else:
        dfa = wdfa
        tau = t


    if dfa=="LSDA":
        fx = 1.0
    elif dfa in pbe_regex:
        fx = fx_pbe(p,var=dfa)
    elif dfa == 'PBEnu':
        fx = fx_pbe_nu(p)
    elif dfa == 'PW86':
        fx = (1 + p*( 35/27 + p*( 14 + 0.2*p)) )**(1/15)
    elif dfa == 'B88':
        fx = fx_b88(p)
    elif dfa == 'PW91':
        fx = fx_pw91(p)
    elif dfa == 'MVS':
        tauw = gd**2/(8*d)
        tauunif = 0.3*kf**2*d
        alpha = np.maximum((t - tauw)/tauunif,0.0)
        fx = fx_mvs(p,alpha)
    elif dfa == 'MVSb':
        tauw = gd**2/(8*d)
        tauunif = 0.3*kf**2*d
        beta = (t - tauw)/(t + tauunif)
        beta[beta < 0.0] = 0.0
        beta[beta > 1.0] = 1.0
        fx = fx_mvsb(p,beta,zps)
    elif dfa == 'model':
        eta = 1.e-3
        tauw = gd**2/(8*d)
        tauunif = 0.3*kf**2*d
        alpha = np.maximum((tau - tauw)/(tauunif + eta*tauw),0.0)
        fx = fx_simple(p,alpha,zps)
    elif dfa == 'MLSD':
        mix = 1/6
        t0 = 0.3*kf**2*d
        fx = (t/t0)**(4*mix/5)
    elif dfa in scan_regex:
        tauw = gd**2/(8*d)
        tauunif = 0.3*kf**2*d

        if dfa == 'SCAN':
            alpha = np.maximum((tau - tauw)/tauunif,0.0)
        elif dfa == 'rSCAN':
            alpha_r = 1.e-3
            tau_r = 1.e-4
            alpha = np.maximum((tau - tauw)/(tauunif+tau_r),0.0)
            alpha = alpha**3/(alpha**2 + alpha_r)

        elif dfa == 'r2SCAN':
            if settings.routine['reopt_r2']:
                eta = zps['c1']
            else:
                eta = 1.e-3
            alpha = np.maximum((tau - tauw)/(tauunif + eta*tauw),0.0)
        elif dfa in ['R201','AES']:
            eta = 1.e-2
            alpha = np.maximum((tau - tauw)/(tauunif + eta*tauw),0.0)
        fx = fx_scan(p,alpha,zps,wfun=(dfa).split('-')[0])

    elif dfa in tpss_regex:

        tauw = gd**2/(8*d)
        tauunif = 0.3*kf**2*d
        alpha = np.maximum((tau - tauw)/tauunif,0.0)
        fx = fx_tpss(p,alpha,rev=dfa)

    elif dfa in ['TPSSb','rTPSSb']:

        tauw = gd**2/(8*d)
        tauunif = 0.3*kf**2*d
        beta = (t - tauw)/(t + tauunif)
        beta[beta < 0.0] = 0.0
        beta[beta > 1.0] = 1.0
        fx = fx_tpss(p,beta,rev=dfa)

    elif dfa == 'TASK':
        tauw = gd**2/(8*d)
        tauunif = 0.3*kf**2*d
        alpha = np.maximum((t - tauw)/tauunif,0.0)
        fx = fx_task(p,alpha)

    elif dfa == "LLMG":
        q = lap/(4*kf**2*d)
        fx = fx_llmgga(p,q,zps)

    elif dfa == 'TM':
        tauw = gd**2/(8*d)
        tauunif = 0.3*kf**2*d
        alpha = np.maximum((t - tauw)/tauunif,0.0)

        fx = fx_tao_mo(p,alpha)

    elif dfa == 'DME':

        tauw = gd**2/(8*d)
        tauunif = 0.3*kf**2*d
        alpha = (t - tauw)/tauunif
        alpha[alpha < 0.0] = 0.0

        q = lap/(4*kf**2*d)

        fx = fx_dme(p,q,alpha)

    elif dfa == 'DMEreg':

        tauw = gd**2/(8*d)
        tauunif = 0.3*kf**2*d
        alpha = (t - tauw)/tauunif
        alpha[alpha < 0.0] = 0.0

        fx = fx_dme_reg(p,alpha)

    elif dfa == 'test':

        u = 0.21951#10/81
        k = 0.804
        cl = np.array([0.804, 0.9421139665104077, -2.6863967778980387, 1.744282811387631])
        fx = np.ones_like(p)
        x = u*p/k
        y = x/(1+x)#np.tanh(p)
        tmp = np.zeros_like(p)
        tmp[:] = y
        for i in range(len(cl)):
            fx += cl[i]*tmp
            tmp *= y

    else:
        raise ValueError(("Invalid exchange functional {:}").format(dfa))

    if fx_only:
        return fx
    else:
        return fx*ex_unif(d)

def ex_unif(d):
    return -(3.0/(4.0*pi))*(3*pi**2)**(1.0/3.0)*d**(4.0/3.0)

def fx_pw91(p):

    """ [P91] """
    s = p**(0.5)
    asinhf = 0.19645*s*np.arcsinh(7.7956*s)
    fx_num = 1 + asinhf + (0.2743 - 0.1508*np.exp(-100*p))*p
    fx_den = 1 + asinhf + 0.004*p**2

    return fx_num/fx_den

def fx_pbe(p,var='PBE'):
    """
        PBE: [PBE1996]
        PBEsol: [P2008]
        acPBE: [CCKB2018]
    """
    kappa = 0.804
    upbe = {'PBE': 0.21951,#0.066725*pi**2/3.0
    'PBEsol': 10.0/81.0, 'acPBE': 0.249 }
    fx = 1.0 + kappa - kappa/(1.0 + upbe[var]*p/kappa)
    return fx

def fx_pbe_nu(p):

    u_pbe = 0.21951
    u2 = 2.109*10/81#0.2603
    u4 = -0.125
    k0 = -u2**2/u4

    #c = 4*(-u2 + (u2**2 - 2*u4/5)**(0.5) )
    #fx = (1 + (u2 + 5/4*c)*p)/(1 + c*p)**(5/4)

    x = u2*p #+ (u2**2/k0 + u4)*p**2
    fx = 1 + k0 - k0/(1 + x/k0)
    c = 6.80252618885021
    gx = np.ones_like(p)
    pmsk = p > 1.e-14
    gx[pmsk] = -np.expm1(-c/p[pmsk]**(0.25))

    #c = 8*u2**2/(5*k0)*( (3 - 10*k0)/(10*k0) + 1)
    #fx = (1 + (1 + k0)*u2*p/k0)/(1 + 8/5*u2*p/k0 + c*p**2)**(5/8)

    return fx*gx

def fx_dme(p,q,alpha):
    fx = 1 + 35/27*(q - p) + 7/9*(1-alpha)
    #fx = 1 - 70/81*p - 7/9*(alpha-1)
    return fx

def fx_llmgga(p,q,zps):
    kappa = 0.174
    uak = 10/81
    ussb = 146/2025

    if settings.GE == "O2":
        delta = uak*p
        fx = 1 + kappa*np.tanh(delta/kappa)
    elif settings.GE == "O4":
        delta = uak*p + ussb*(q**2 - 5*p*q/2)
        fx = 1 + kappa*np.tanh(delta/kappa)

    elif settings.GE =="sol":


        #delta = uak*(1 + b1*p*np.exp(-zps['c0']*abs(b1)*p))*p + ussb*(q - 5/4*p)**2
        #delta = uak*p*(1 + b1*p*np.exp(b1*p))**2 + ussb*(np.tanh(q)-5*p/4)**2#ussb*(q*np.exp(-5*ussb*p/2) - 5/4*p)**2
        """
        k = 0.804
        b1 = 0.5*(uak/k - 25*ussb/(16*uak))
        delta = uak*p + uak**2/k*p**2 + ussb*np.tanh(q**2)-ussb*np.tanh(5/2*p*q)
        fx = 1 + k - k/(1 + delta/k)
        """
        k = 0.804
        """
        wgs = [1/uak, (1/ussb)**(0.5), (2/(5*ussb))**(0.5)]
        wgt = np.sum(wgs)
        k1 = k*wgs[0]/wgt#zps['c0']*k#16*uak**2/(25*ussb)
        k2 = k*wgs[1]/wgt#(k - k1)*zps['c1']
        k3 = k*wgs[2]/wgt#k - k1 - k2
        d1 = uak*p
        d2 = ussb*(q-5*p/4)**2
        fx = 1 + k1*np.tanh(d1/k1) + k2*np.tanh(ussb*q**2/k2) - k3*np.tanh(ussb*5/2*p*q/k3)
        """
        #delta = (d1 + d2 )/(1 + 25*ussb/(16*uak) - uak*p)
        #hx = 1 + k-k/(1 + (uak*p + ussb*(q-5*p/4)**2)/k)
        #gx = 1/(1 + (25*ussb - 16*uak**2/k)*p**2 + zps['c0']*q**4)**(1/16)
        #fx = hx*gx
        #
        #fx = 1 + k1 - k1/(1 + d1/k1) + k2 - k2/(1 + d2/k2)
        #fx = 1 + k1*np.tanh(d1/k1) + k2*np.tanh(d2/k2)

        c = 1
        #fp = (1 + (uak + 17/16*c)*p + 17/16*c*(uak + 17/16*c)*p**2)/(1 + c*p + 33/32*p**2)**(17/16)
        #fq = (1 + ussb*q**2)/(1 + 40/17*ussb/uak*q + 33/32*(40/17*ussb/uak*q)**2)**(17/16)
        fp = (1 + (uak + 9/8*c)*p + 9/8*c*(uak + 9/8*c)*p**2)/(1 + c*p \
            + (17/16*c**2 + 25/18*ussb)*p**2 )**(9/8)
        fx = fp*fq

    elif settings.GE == 'interp':

        k0 = 0.174

        fa = 400/243*k0
        fb = 200/729
        fc = -511/6075

        f2 = 81/200*(146/2025)/k0
        f1 = (-fb - (fb**2 - 4*fa*fc)**(0.5))/(2*fa)
        h1 = 10/81 + 20/27*k0*f1
        h2 = -2*( 800/729*k0*f2 + 40/27*f1*h1)

        ft = lap_lev_ft(settings.KEMOD,p,q,zps)
        alpha = ft - 5*p/3
        ief = (1-alpha)*(1 + (f2-f1 -2)*alpha + (-f2 -f1+1)*alpha**2)/(1 + alpha**3)
        #c1 = -2 + f2 + zps['c1']
        #c2 = 1 - 2*f1 - f2 - 2*zps['c1']
        #ief = (1-alpha)*(1 + c1*alpha + c2*alpha**2 + zps['c1']*alpha**3)/(1 + alpha**4)
        #ief = (1-alpha)* (1 - (1 - 1/(3*k0))*alpha)/(1 + alpha**2)
        fx0 = 1 + k0
        fx1 = 1 + (h1*p + h2/2*p**2 + zps['c0']*p**3)/(1 + p**3)

        fx = ( fx1 + ief*(fx0 - fx1) )/(1 + (p/zps['c1'])**4)**(1/16)

    elif settings.GE == 'simple':

        kappa = 0.174
        uak = 10/81
        cqq = 146/2025
        a = 25/32*cqq/zps['c0']

        qr = zps['c0']*np.tanh(q/zps['c0'])
        delta = (uak - zps['c0']*a**2)*p + cqq*(qr - 5*p/4)**2 + zps['c0']*p*(p-a)**2
        fx = 1 + kappa*np.tanh(delta/kappa)

    elif settings.GE == 'pade':

        kappa = 0.804
        uak = 10/81
        cqq = 146/2025
        #qr = q/(1 + q**2)
        #fx_num = uak*p + 4*uak**2/kappa*p**2
        #fx_denom = (1 + 2*uak/kappa*p)**2
        k2 = kappa/2
        fx2 = uak*p/(1 + 25*cqq/(8*uak)*p + (uak*p/k2)**2)**(0.5)
        qmp2 = (q - 5*p/4)**2
        fx4 = cqq*qmp2/(1 + (cqq*qmp2/k2)**2)**(0.5)
        fx = 1 + fx2 + fx4

    elif settings.GE =='scan':

        kappa = 0.065
        uak = 10/81
        cqq = 146/2025

        xf = uak*p + uak**2/kappa*p**2 + cqq*(q**2 - 5*p*q/2)*np.exp(-0.5*q**2)

        hx1 = 1 + kappa - kappa/(1 + xf/kappa)
        alpha = (1 + 40/9*(q - 2*p/3)*(1 + 10*q/9 - 20*p/27))**(0.5)

        def fsmooth(a,c):
            a2 = a**2
            a3 = a2*a
            ief = (1-a)**3*(1 + a*c[0] + c[1]*a2 + c[2]*a3)/(1 + c[3]*a2 + c[2]/c[4]*a3*a3)
            return ief
        ief = fsmooth(alpha,[2.737365,  1.728020,  0.764758, -1.093667,1.24])

        a1 = 4.9479
        gx = np.ones(p.shape)
        gx[p > 1.e-16] = 1.0 - np.exp(-a1/p[p > 1.e-16]**(0.25))

        fx = (hx1 + ief*(1.174 - hx1))*gx

    return fx

def fx_mvs(p,alpha):

    """ [SRP2015a] """

    hx0 = 1.174
    k0 = 0.174
    hx1 = 1.0
    uak = 10/81

    e1 = -1.6665
    c1 = (20*k0/(27*uak))**4 - (1 + e1)**2
    ief = (1 - alpha)/((1 + e1*alpha**2)**2 + c1*alpha**4)**(1/4)

    b = 0.0233
    gx = 1/(1 + b*p**2)**(1/8)

    fx = (hx1 + ief*(hx0 - hx1))*gx
    return fx

def fx_scan(p,alpha,zps,wfun='SCAN'):
    """
        SCAN: [SRP2015]
        rSCAN: [BY2019]
        r2 SCAN: [F2020]
    """

    hx0 = 1.174
    a1 = 4.9479

    k1 = 0.065
    uak = 10.0/81.0

    c1 = 0.667
    c2 = 0.8
    d1 = 1.24
    if wfun in ['rSCAN','r2SCAN','R201','AES']:
        cfs = np.asarray([1.0,-0.667,-0.4445555,-0.663086601049, 1.451297044490,
            -0.887998041597, 0.234528941479,-0.023185843322])

    if wfun in ['SCAN','rSCAN']:

        b2 = (5913.0/405000.0)**(0.5)
        b1 = (511.0/13500.0)/(2*b2)
        b3 = 0.5
        b4 = uak**2/k1 - 1606.0/18225.0-b1**2

        xf = uak*p*(1.0 + (b4*p/uak)*np.exp(-abs(b4)*p/uak))
        xf += (b1*p + b2*(1.0 - alpha)*np.exp(-b3*(1.0-alpha)**2))**2

    elif wfun in ['r2SCAN','R201','AES']:
        dp2 = 0.361
        if wfun == 'r2SCAN':
            if settings.routine['reopt_r2']:
                dp2 = zps['c2']
                eta = zps['c1']
            else:
                eta = 1.e-3
        elif wfun in ['R201','AES']:
            eta = 1.e-2
        c_eta = 20.0/27.0 + 5.0/3.0*eta#4009.0/5400.0#
        c_2 = -0.16274221523404786#(hx0 - 1.0)*np.dot([i for i in range(1,8)],cfs[1:])
        xf = (c_eta*c_2*np.exp(-(p/dp2**2)**2) + uak)*p

    hx1 = 1.0 + k1 - k1/(1.0 + xf/k1)

    ief = np.zeros(alpha.shape)
    oma = 1.0 - alpha
    if wfun == 'SCAN':
        msk = alpha < 1.0
        ief[msk] = np.exp(-c1*alpha[msk]/oma[msk])
        msk = alpha > 1.0
        ief[msk] = -d1*np.exp(c2/oma[msk])
    elif wfun in ['rSCAN','r2SCAN','R201','AES']:
        msk = alpha <= 2.5
        am = alpha[msk]
        for i in range(len(cfs)):
            ief[msk] += cfs[i]*am**i

        msk = alpha > 2.5
        ief[msk] = -d1*np.exp(c2/oma[msk])

    gx = np.ones(p.shape)
    gx[p > 1.e-16] = 1.0 - np.exp(-a1/p[p > 1.e-16]**(0.25))

    fx = (hx1 + ief*(hx0 - hx1))*gx

    if wfun == 'AES':
        fscl = np.zeros_like(alpha)
        amsk = alpha < 1.0
        #fscl[amsk] = np.exp(-zps['c0']*alpha[amsk]**2/(1 - alpha[amsk]))
        fscl[amsk] = ((1-alpha[amsk])/(1+zps['c0']*alpha[amsk]))**3
        fx = (1 - fscl)*fx + fscl

    return fx

def fx_tpss(p,alpha,rev='TPSS'):

    kappa = 0.804
    uak = 10/81
    cb = 0.4

    if rev in ['TPSSb','rTPSSb']:
        yb = 1 - 5*alpha + 8*alpha**2 - 4*alpha**3
        pf = 10*p/(3 + 10*p)
        z = (pf + yb*(1 - pf))*(1-alpha)
        qb = 9/20*(2*alpha - 1)/( (1-alpha)**2 + cb*alpha*(2*alpha-1))**(0.5) + 17/12*p
    else:
        z = 5*p/(5*p + 3*alpha)
        z[z > 1.0] = 1.0
        qb = 9/20*(alpha-1)/(1 + cb*alpha*(alpha-1))**(0.5) + 2*p/3

    if rev == 'TPSS':
        cc = 1.59096
        ce = 1.537
        cmu = 0.21951
        zfac = cc*z**2/(1 + z**2)**2
    elif rev == 'revTPSS':
        cc = 2.35204
        ce = 2.1677
        cmu = 0.14
        #cb = 0.4
        zfac = cc*z**3/(1 + z**2)**2
    elif rev == 'TPSSb':
        cc = 1.501473670500855#1.0203440289202264
        ce = 0.465650621477107#0.19509166818315868
        cmu = 0.21951
        zfac = cc*z**2/(1 + z**2)**2
    elif rev == 'rTPSSb':
        cc = 1.8826952120594809#1.2805913031901586
        ce = 0.6952197859223813#0.3313760979435756
        cmu = 0.14
        zfac = cc*z**3/(1 + z**2)**2

    ceh = ce**(0.5)

    pz = 3*z/5

    x = ( (uak + zfac)*p + 146/2025*qb**2 \
        -73/405*qb*(0.5*(pz**2 + p**2))**(0.5) + uak**2*p**2/kappa \
        + 2*ceh*uak*pz**2 + ce*cmu*p**3 )/(1 + ceh*p)**2
    fx = 1 + kappa - kappa/(1 + x/kappa)

    return fx

def fx_b88(p):

    b88 = 0.005291668409558467#0.0042*2**(1/3)
    x2 = 4*(3*pi**2)**(2/3)*p
    x = x2**(0.5)

    fx = 1 + 4/3*(pi/3)**(1/3)*b88*x2/(1 + 6*b88*x*np.arcsinh(2**(1/3)*x))
    return fx

def fx_task(p,alpha):

    """ [AK2019] """

    anu = [0.938719,-0.076371,-0.0150899]
    bnu = [-0.628591,-2.10315,-0.5,0.103153,0.128591]
    hx1 = np.zeros(p.shape)
    for i in range(3):
        hx1 += anu[i]*cheb_rtnl_poly(p,i)

    ief = np.zeros(alpha.shape)
    for i in range(5):
        ief += bnu[i]*cheb_rtnl_poly(alpha,i)

    ch = 4.9479
    gx = np.ones(p.shape)
    pm = p > 1.e-16
    gx[pm] = -np.expm1(-ch/p[pm]**(0.25))

    cd = 10
    hx0 = 1.174
    fx = hx0*gx + (1 - ief)*(hx1 - hx0)*gx**cd

    return fx

def cheb_rtnl_poly(x,n):
    """
        See [B1987] and [NRF77]

        Chebyshev polynomial T_n(x) = cos(n * arccos(x))

        Rational Chebyshev polynomial R_n(x) = T_n((x-1)/(x+1))
    """
    if n == 0:
        return 1
    elif n == 1:
        return (x - 1)/(x + 1)
    elif n == 2:
        return (x**2 - 6*x + 1)/(x + 1)**2
    elif n == 3:
        return (x**3 - 15*x**2 + 15*x - 1)/(x + 1)**3
    elif n == 4:
        return (x**4 - 28*x**3 + 70*x**2 - 28*x + 1)/(x + 1)**4
    else:
        return np.cos(n*np.arccos((x-1)/(x+1)))

def fx_tao_mo(p,alpha):

    """
        [TM2016]
    """

    lam = 0.6866
    beta = 79.873

    y = (2*lam - 1)**2*p
    f = (1 + y*(700/27 + beta*y))**(0.1)
    of2 = 1/(f*f)

    """
        NB: tau/tau0 = alpha + 5*p/3. Making use of that here to replace
            R = (tau - 3*(lam**2 - lam + 0.5)*(tau - tau0 + tauw/9))
        with...
    """
    R = 1 + 595*y/54 - (alpha + 5*p/3 - 3*(lam**2 - lam + 0.5)*(alpha - 1 + 40*p/27))
    fx_dme = (1 + 7*R*of2/9)*of2

    z = 5*p/(5*p + 3*alpha)
    z[z > 1.0] = 1.0
    qt = 9/20*(alpha - 1) + 2*p/3

    fx1 = (10/81 + 50*p/729)*p
    fx2 = 146/2025*qt**2 - 73/405*qt*(3*z/5)*(1 - z)
    fx_svl = (1 + 10*(fx1 + fx2) )**(0.1)

    z2 = z*z
    z3 = z2*z
    w = (z2 + 3*z3)/(1 + z3)**2

    fx = fx_svl + w*(fx_dme - fx_svl)
    return fx

def fx_dme_reg(p,alpha):

    lam = 0.30429295745428747#0.33688023088098595
    y = (2*lam - 1)**2*p
    f = ( (1 + (15*70/27 - 70/3)*y)**(1/3) + 70/9*y )**(1/5)
    of2 = 1/(f*f)

    R = 1 + 595*y/54 - (alpha + 5*p/3 - 3*(lam**2 - lam + 0.5)*(alpha - 1 + 40*p/27))
    #R = 1 + 595*y/54 - ((b + 5*p/3)/(1 - b) - 3*(lam**2 - lam + 0.5)*( (2*b - 1 + 5*p/3)/(1 - b) - 5*p/27))
    fx_dme = (1 + 7*R*of2/9)*of2

    k0 = 0.804
    fx_dme_reg = 1 + k0*np.tanh((fx_dme-1)/k0)

    """
    zb = 1 + b*(-5 + b*(8 - 4*b))
    w = zb**2*(1 + 3*zb)/(1 + zb**3)**2

    mu2 = 10/81
    mu4 = 146/2025
    k1 = 16*mu2**2/(25*mu4)
    qt = 9/10*(2*b - 1) + 17/12*p
    x = mu2*p + mu4*(qt-5*p/4)**2

    fx_svl = 1 + k1 - k1/(1 + x/k1)
    fx = fx_svl + w*(fx_dme_reg - fx_svl)
    """

    return fx_dme_reg
