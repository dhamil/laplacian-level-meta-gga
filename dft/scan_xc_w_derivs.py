import numpy as np

from settings import KEMOD,routine
from constants import pi
from dft.tau_w_derivs import lap_lev_ke_unp_w_derivs
from dft.ec_pw92_w_derivs import spinf_w_deriv,ec_pw92_w_derivs


def exc_scan_w_derivs(nup,ndn,gnup,gndn,gn,ttup,ttdn,lup,ldn,zps,wdfa,oes=False,unp=False):

    if wdfa[-2:]=='-L':
        lap_derivs = True
        dfa = wdfa[:-2]
        """
            Deorbitalized functionals
        """
        tup, d_tup_dnup, d_tup_dgnup, d_tup_dlup = lap_lev_ke_unp_w_derivs(
            KEMOD,2*nup,2*gnup,2*lup,zps)
        # local spin scaling relation
        tup *= 0.5
        if oes:
            tdn = 0.0
            d_tdn_dndn = 0.0
            d_tdn_dgndn = 0.0
            d_tdn_dldn = 0.0
        elif unp:
            tdn = tup[:]
            d_tdn_dndn = d_tup_dnup[:]
            d_tdn_dgndn = d_tup_dgnup[:]
            d_tdn_dldn = d_tup_dlup[:]
        else:
            tdn, d_tdn_dndn, d_tdn_dgndn, d_tdn_dldn = lap_lev_ke_unp_w_derivs(
                KEMOD,2*ndn,2*gndn,2*ldn,zps)
            tdn *= 0.5

    else:
        lap_derivs = False
        """
            use exact KED
        """
        dfa = wdfa
        tup = ttup
        tdn = ttdn

    exc_out = np.zeros((nup.shape[0],18))
    """
    exc_out indexes:
        0 = ex density     6 = d_ex_dgnup     11 = d_ex_dlup    15 = d_ex_dtup
        1 = ec density     7 = d_ec_dgnup     12 = d_ec_dlup    16 = d_ex_dtdn
        2 = d_ex_dnup      8 = d_ex_dgndn     13 = d_ex_dldn    17 = d_ec_dtau
        3 = d_ec_dnup      9 = d_ec_dgndn     14 = d_ec_dldn
        4 = d_ex_dndn      10 = d_ec_dgn
        5 = d_ec_dndn
    """

    exc_out[:,0], exc_out[:,2], exc_out[:,4], exc_out[:,6], exc_out[:,8], exc_out[:,15], \
        exc_out[:,16] = ex_scan_w_derivs(nup,ndn,gnup,gndn,tup,tdn,zps,dfa,oes=oes,unp=unp)

    exc_out[:,1], exc_out[:,3], exc_out[:,5], exc_out[:,10], exc_out[:,17] = \
        ec_scan_w_derivs(nup,ndn,gn,tup+tdn,zps,dfa)

    if lap_derivs:

        exc_out[:,2] += exc_out[:,15]*d_tup_dnup
        exc_out[:,4] += exc_out[:,16]*d_tdn_dndn

        exc_out[:,6] += exc_out[:,15]*d_tup_dgnup
        exc_out[:,8] += exc_out[:,16]*d_tdn_dgndn

        exc_out[:,11] = exc_out[:,15]*d_tup_dlup
        exc_out[:,13] = exc_out[:,16]*d_tdn_dldn

        exc_out[:,15] = 0.0
        exc_out[:,16] = 0.0

        exc_out[:,3] += exc_out[:,17]*d_tup_dnup
        exc_out[:,5] += exc_out[:,17]*d_tdn_dndn

        exc_out[:,7] = exc_out[:,17]*d_tup_dgnup
        exc_out[:,9] = exc_out[:,17]*d_tdn_dgndn

        exc_out[:,12] = exc_out[:,17]*d_tup_dlup
        exc_out[:,14] = exc_out[:,17]*d_tdn_dldn

        exc_out[:,17] = 0.0

    return exc_out


def scan_ief_w_derivs(alpha,c1,c2,d):

    ief = np.zeros_like(alpha)
    d_ief_da = np.zeros_like(alpha)

    msk = alpha < 1.0
    am = alpha[msk]
    omam = 1-am
    ief[msk] = np.exp(-c1*am/omam)
    d_ief_da[msk] = -c1/omam**2*ief[msk]

    msk = alpha > 1.0
    omam = 1-alpha[msk]
    ief[msk] = -d*np.exp(c2/omam)
    d_ief_da[msk] = c2*ief[msk]/omam**2

    return ief, d_ief_da

def rscan_ief_w_derivs(alpha,cfs,c1,c2,d):

    msk = alpha <= 2.5
    am = alpha[msk]

    ief = np.zeros_like(alpha)
    d_ief_da = np.zeros_like(alpha)

    ief[msk] = cfs[0]
    for i in range(1,len(cfs)):
        ief[msk] += cfs[i]*am**i
        d_ief_da[msk] += i*cfs[i]*am**(i-1)

    msk = alpha > 2.5
    omam = 1-alpha[msk]
    ief[msk] = -d*np.exp(c2/omam)
    d_ief_da[msk] = c2*ief[msk]/omam**2

    return ief, d_ief_da

def ex_scan_w_derivs(nup,ndn,gnup,gndn,tup,tdn,zps,wfun,oes=False,unp=False):

    exup, d_ex_dnup, d_ex_dgnup, d_ex_dtup = ex_scan_unpol_w_derivs(2*nup,2*gnup,
        2*tup,zps,wfun)

    if oes:
        tmp = np.zeros(nup.shape)
        return exup/2,d_ex_dnup,tmp,d_ex_dgnup,tmp,d_ex_dtup,tmp
    elif unp:
        return exup, d_ex_dnup, d_ex_dnup, d_ex_dgnup, d_ex_dgnup, d_ex_dtup, d_ex_dtup
    else:
        exdn, d_ex_dndn, d_ex_dgndn, d_ex_dtdn = ex_scan_unpol_w_derivs(2*ndn,2*gndn,
            2*tdn,zps,wfun)

        return (exup + exdn)/2, d_ex_dnup, d_ex_dndn, d_ex_dgnup, d_ex_dgndn, \
            d_ex_dtup, d_ex_dtdn

def ex_scan_unpol_w_derivs(n,gn,tau,zps,wfun):

    kf = (3*pi**2*n)**(1/3)
    exlda = -3/(4*pi)*kf*n
    d_exlda_dn = -kf/pi

    p = (gn/(2*kf*n))**2
    d_p_dn = -8/3*p/n
    d_p_dgn = gn/(2*(kf*n)**2)

    tau0 = 0.3*kf**2*n
    d_tau0_dn = kf**2/2

    tauw = gn**2/(8*n)
    d_tauw_dn = -tauw/n
    d_tauw_dgn = gn/(4*n)

    if wfun == 'SCAN':

        alpha = (tau - tauw)/tau0
        alpha[alpha < 0.0] = 0.0

        d_alpha_dn = -(d_tauw_dn + d_tau0_dn*alpha)/tau0
        d_alpha_dgn = -d_tauw_dgn/tau0
        d_alpha_dtau = 1/tau0

    elif wfun == 'r2SCAN':

        if routine['reopt_r2']:
            eta = zps['c1']
        else:
            eta = 1.e-3

        alpha_den = tau0 + eta*tauw
        alpha = (tau - tauw)/alpha_den
        alpha[alpha < 0.0] = 0.0

        d_alpha_dn = -(d_tauw_dn + (d_tau0_dn + eta*d_tauw_dn)*alpha )/alpha_den
        d_alpha_dgn = -d_tauw_dgn*(1 + eta*alpha )/alpha_den
        d_alpha_dtau = 1/alpha_den

    else:
        raise ValueError('Unknown SCAN family functional, ',wfun)

    fx, d_fx_dp, d_fx_da = fx_scan_w_derivs(p,alpha,wfun)

    ex = fx*exlda
    d_ex_dn = d_exlda_dn*fx + exlda*(d_fx_dp*d_p_dn + d_fx_da*d_alpha_dn)
    d_ex_dgn = exlda*(d_fx_dp*d_p_dgn + d_fx_da*d_alpha_dgn)
    d_ex_dtau =  exlda*d_fx_da*d_alpha_dtau

    return ex, d_ex_dn, d_ex_dgn, d_ex_dtau


def fx_scan_w_derivs(p,alpha,wfun):

    """
        SCAN: [SRP2015]
        r2 SCAN: [F2020]
    """

    hx0 = 1.174
    a1 = 4.9479

    k1 = 0.065
    uak = 10.0/81.0

    c1x = 0.667
    c2x = 0.8
    dx = 1.24
    if wfun == 'rSCAN' or wfun == 'r2SCAN':
        cfx = np.asarray([1.0,-0.667,-0.4445555,-0.663086601049, 1.451297044490,
            -0.887998041597, 0.234528941479,-0.023185843322])

    if wfun == 'SCAN' or wfun == 'rSCAN':

        b2 = (5913.0/405000.0)**(0.5)
        b1 = (511.0/13500.0)/(2*b2)
        b3 = 0.5
        b4 = uak**2/k1 - 1606.0/18225.0-b1**2

        oma = 1.0 - alpha

        pexp = np.exp(-abs(b4)*p/uak)
        d_pexp_dp = -abs(b4)/uak*pexp
        aexp = np.exp(-b3*oma**2)
        d_aexp_da = 2*b3*oma*aexp

        xf4 = b1*p + b2*oma*aexp
        d_xf4_dp = b1
        d_xf4_da = b2*(-aexp + oma*d_aexp_da)

        xf = uak*p + b4*p**2*pexp + xf4**2
        d_xf_dp = uak + 2*b4*p*pexp + b4*p**2*d_pexp_dp + 2*xf4*d_xf4_dp
        d_xf_da =  2*xf4*d_xf4_da

    elif wfun == 'r2SCAN':

        if routine['reopt_r2']:
            dp2 = zps['c2']
            eta = zps['c1']
        else:
            dp2 = 0.361
            eta = 1.e-3

        c_eta = 20.0/27.0 + 5.0/3.0*eta#4009.0/5400.0#
        c_2 = -0.16274221523404786#(hx0 - 1.0)*np.dot([i for i in range(1,8)],cfx[1:])
        cr2 = c_eta*c_2

        pdamp = np.exp(-p**2/dp2**4)
        d_pdamp_dp = -2*p/dp2**4*pdamp

        xf = (cr2*pdamp + uak)*p
        d_xf_dp = cr2*pdamp + uak + cr2*d_pdamp_dp*p
        d_xf_da = np.zeros_like(p)

    hx1 = 1.0 + k1 - k1/(1.0 + xf/k1)
    dhx1 = 1.0/(1.0 + xf/k1)**2
    d_hx1_dp = dhx1*d_xf_dp
    d_hx1_da = dhx1*d_xf_da

    if wfun == 'SCAN':

        ief, d_ief_da =  scan_ief_w_derivs(alpha,c1x,c2x,dx)

    elif wfun == 'rSCAN' or wfun == 'r2SCAN':

        ief, d_ief_da = rscan_ief_w_derivs(alpha,cfx,c1x,c2x,dx)

    gx = np.ones_like(p)
    d_gx_dp = np.zeros_like(p)
    pmsk = p > 1.e-16
    pm = p[pmsk]
    gexp = np.exp(-a1/pm**(0.25))
    gx[pmsk] = 1.0 - gexp
    d_gx_dp[pmsk] = -0.25*a1/pm**(1.25)*gexp

    fx_core = hx1 + ief*(hx0 - hx1)
    fx = fx_core*gx

    omief = 1-ief
    d_fx_dp = fx_core*d_gx_dp + gx*omief*d_hx1_dp
    d_fx_da = gx*(omief*d_hx1_da + d_ief_da*(hx0-hx1))

    return fx, d_fx_dp, d_fx_da


def ec_scan_w_derivs(nup,ndn,gn,tau,zps,wfun):

    """
        SCAN: [SRP2015]
        r2 SCAN: [F2020]
    """

    n = nup + ndn
    rs = (3/(4*pi*n))**(1/3)
    kf = (3*pi**2*n)**(1/3)
    d_rs_dn = -rs/(3*n)

    #z = np.minimum(np.maximum((nup - ndn)/n,-1.0),1.0)
    z = np.minimum(np.maximum((nup - ndn)/n,-0.99999999999990),0.99999999999990)
    d_z_dnup = 2*ndn/n**2
    d_z_dndn = -2*nup/n**2

    p = (gn/(2*kf*n))**2
    d_p_dn = -8/3*p/n
    d_p_dgn = gn/(2*(kf*n)**2)

    dsz,d_dsz_dz = spinf_w_deriv(z,5/3)
    tau0 = 0.3*kf**2*n*dsz
    d_tau0_dn = kf**2*dsz/2
    d_tau0_dz = 0.3*kf**2*n*d_dsz_dz

    tauw = gn**2/(8*n)
    d_tauw_dn = -tauw/n
    d_tauw_dgn = gn/(4*n)

    if wfun == 'SCAN':

        alpha = (tau - tauw)/tau0
        alpha[alpha < 0.0] = 0.0

        d_alpha_dn = -(d_tauw_dn + d_tau0_dn*alpha)/tau0
        d_alpha_dz = -d_tau0_dz*alpha/tau0
        d_alpha_dgn = -d_tauw_dgn/tau0
        d_alpha_dtau = 1/tau0

    elif wfun == 'r2SCAN':

        if routine['reopt_r2']:
            eta = zps['c1']
        else:
            eta = 1.e-3

        alpha_den = tau0 + eta*tauw
        alpha = (tau - tauw)/alpha_den
        alpha[alpha < 0.0] = 0.0

        d_alpha_dn = -(d_tauw_dn + (d_tau0_dn + eta*d_tauw_dn)*alpha )/alpha_den
        d_alpha_dz = -d_tau0_dz*alpha/alpha_den
        d_alpha_dgn = -d_tauw_dgn*(1 + eta*alpha )/alpha_den
        d_alpha_dtau = 1/alpha_den

    else:
        raise ValueError('Unknown SCAN family functional, ',wfun)

    eps, d_eps_drs, d_eps_dz, d_eps_dp, d_eps_da = \
        ec_scan_core_w_derivs(rs,p,z,alpha,zps,wfun)

    ec = eps*n

    d_ec_dn = eps + n*(d_eps_drs*d_rs_dn + d_eps_dp*d_p_dn + d_eps_da*d_alpha_dn)

    d_ec_dz = n*(d_eps_dz + d_eps_da*d_alpha_dz)
    d_ec_dnup = d_ec_dn + d_ec_dz*d_z_dnup
    d_ec_dndn = d_ec_dn + d_ec_dz*d_z_dndn

    d_ec_dgn = n*(d_eps_dp*d_p_dgn + d_eps_da*d_alpha_dgn)
    d_ec_dtau = n*d_eps_da*d_alpha_dtau

    return ec, d_ec_dnup, d_ec_dndn, d_ec_dgn, d_ec_dtau


def ec_scan_core_w_derivs(rs,p,z,alpha,zps,wfun):

    gamma = (1-np.log(2))/pi**2
    beta0 = 0.066725
    beta_a = 0.1
    beta_b = 0.1778
    beta_inf = beta0*beta_a/beta_b


    """
        Interpolation functions
    """

    c1c = 0.64
    c2c = 1.5
    dc = 0.7

    if wfun == 'SCAN':

        ief, d_ief_da =  scan_ief_w_derivs(alpha,c1c,c2c,dc)

    elif wfun == 'rSCAN' or wfun == 'r2SCAN':

        cfc = np.asarray([1.0,-0.64,-0.4352,-1.535685604549,3.061560252175,
            -1.915710236206,0.516884468372,-0.051848879792])

        ief, d_ief_da = rscan_ief_w_derivs(alpha,cfc,c1c,c2c,dc)


    """
        alpha = 0 energy density
    """

    sqrt_rs = np.sqrt(rs)

    b1c = 0.0285764
    b2c = 0.0889
    b3c = 0.125541

    f1 = 1.0 + b2c*sqrt_rs + b3c*rs
    d_f1_drs = b2c/(2*sqrt_rs) + b3c
    ec0_lda = -b1c/f1
    d_ec0lda_drs = b1c/f1**2*d_f1_drs
    d_ec0lda_drs2 = -2*d_ec0lda_drs*d_f1_drs/f1 - b1c*b2c/(4*sqrt_rs**3*f1**2)

    dxz,d_dxz_dz = spinf_w_deriv(z,4/3)
    bfac = 1.174*(3/4*(3/(2*pi))**(2/3))*b3c/b1c
    gc_zeta = (1.0 - bfac*(dxz - 1.0)) * (1 - z**(12))
    d_gcz_dz = - bfac*d_dxz_dz*(1 - z**(12)) - 12*z**(11)*(1.0 - bfac*(dxz - 1.0))

    ecl0 = ec0_lda*gc_zeta
    d_ecl0_drs = d_ec0lda_drs*gc_zeta
    d_ecl0_dz = ec0_lda*d_gcz_dz
    d_ecl0_drs2 = d_ec0lda_drs2*gc_zeta
    d_ecl0_drsdz = d_ec0lda_drs*d_gcz_dz

    w0 = np.expm1(-ec0_lda/b1c)
    d_w0_drs = -d_ec0lda_drs/b1c*(w0 + 1)

    chi_inf = (3*pi**2/16)**(2/3)*beta_inf/(0.9 - 3*(3/(16*pi))**(2/3))
    gf_inf = 1.0/(1.0 + 4.0*chi_inf*p)**(1.0/4.0)
    d_gf_inf_dp = -chi_inf*gf_inf**5

    hcore0 = 1.0 + w0*(1.0 - gf_inf)
    h0 = b1c*np.log(hcore0)
    d_h0_drs = b1c*d_w0_drs*(1.0 - gf_inf)/hcore0
    d_h0_dp = -b1c*w0*d_gf_inf_dp/hcore0

    eps0 = (ec0_lda + h0)*gc_zeta
    d_eps0_drs = (d_ec0lda_drs + d_h0_drs)*gc_zeta
    d_eps0_dz = (ec0_lda + h0)*d_gcz_dz
    d_eps0_dp = d_h0_dp*gc_zeta


    """
        alpha = 1 energy density
    """
    brs_den = 1 + beta_b*rs
    brs = beta0*(1 + beta_a*rs)/brs_den
    d_brs_drs = beta0*( beta_a - beta_b)/brs_den**2

    phi, d_phi_dz = spinf_w_deriv(z,2/3,oderiv = 1)

    gp3 = gamma*phi**3
    t2 = (3*pi**2/16)**(2/3)*p/(phi**2*rs)
    d_t2_drs = -t2/rs
    d_t2_dz = -2*t2*d_phi_dz/phi
    d_t2_dp = (3*pi**2/16)**(2/3)/(phi**2*rs)

    ecl,d_ecl_drs,d_ecl_dz,d_ecl_drs2, d_ecl_drsdz = ec_pw92_w_derivs(z,rs)

    w1 = np.expm1(-ecl/gp3)
    opw1 = 1 + w1
    d_w1_drs = -d_ecl_drs/gp3*opw1
    d_w1_dz = (-d_ecl_dz + 3*ecl*d_phi_dz/phi)*opw1/gp3

    aa = brs/(gamma*w1)
    y = aa*t2
    d_y_drs = ( d_brs_drs/(gamma*w1) - d_w1_drs*aa/w1 )*t2 + aa*d_t2_drs
    d_y_dz = -d_w1_dz*aa*t2/w1 + aa*d_t2_dz
    d_y_dp = aa*d_t2_dp

    if wfun == 'r2SCAN':

        if routine['reopt_r2']:
            dp2 = zps['c2']
            eta = zps['c1']
        else:
            dp2 = 0.361
            eta = 1.e-3
        ds_zeta,d_ds_zeta_dz = spinf_w_deriv(z,5/3)

        df1 = -0.7114023342889984
        dyfac = df1/(27.0*ds_zeta*gp3*w1)
        d_dyfac_drs = -dyfac*d_w1_drs/w1
        d_dyfac_dz = -dyfac*(d_ds_zeta_dz/ds_zeta + 3*d_phi_dz/phi + d_w1_dz/w1)

        dyc = 20*rs*(d_ecl0_drs - d_ecl_drs) - 45*eta*(ecl0 - ecl)
        d_dyc_drs = (20 - 45*eta)*(d_ecl0_drs - d_ecl_drs) + 20*rs*(d_ecl0_drs2 - d_ecl_drs2)
        d_dyc_dz = 20*rs*(d_ecl0_drsdz - d_ecl_drsdz) - 45*eta*(d_ecl0_dz - d_ecl_dz)

        damp = np.exp(-p**2/dp2**4)
        dydamp = p*damp
        d_dydamp_dp = (1 - 2*p**3/dp2**4)*damp

        delta_y = dyfac*dyc*dydamp
        d_dy_drs = d_dyfac_drs*dyc*dydamp + dyfac*d_dyc_drs*dydamp
        d_dy_dz = d_dyfac_dz*dyc*dydamp + dyfac*d_dyc_dz*dydamp
        d_dy_dp = dyfac*dyc*d_dydamp_dp
    else:
        delta_y = 0.0
        d_dy_drs = 0.0
        d_dy_dz = 0.0
        d_dy_dp = 0.0

    gt = 1.0/(1.0 + 4*(y - delta_y))**(0.25)
    dgt = -gt**5
    d_gt_drs = dgt*(d_y_drs - d_dy_drs)
    d_gt_dz = dgt*(d_y_dz - d_dy_dz)
    d_gt_dp = dgt*(d_y_dp - d_dy_dp)

    hcore1 = 1 + w1*(1 - gt)
    h = gp3*np.log(hcore1)
    d_h_drs = gp3*(d_w1_drs*(1-gt) - w1*d_gt_drs)/hcore1
    d_h_dz = 3*h*d_phi_dz/phi + gp3*(d_w1_dz*(1-gt) -w1*d_gt_dz)/hcore1
    d_h_dp = -gp3*w1*d_gt_dp/hcore1

    eps1 = ecl + h
    d_eps1_drs = d_ecl_drs + d_h_drs
    d_eps1_dz = d_ecl_dz + d_h_dz
    d_eps1_dp = d_h_dp

    omief = 1 - ief
    eps = eps1*omief + ief*eps0

    d_eps_drs = d_eps1_drs*omief + d_eps0_drs*ief
    d_eps_dz = d_eps1_dz*omief + d_eps0_dz*ief
    d_eps_dp = d_eps1_dp*omief + d_eps0_dp*ief
    d_eps_da = d_ief_da*(eps0-eps1)

    return eps, d_eps_drs, d_eps_dz, d_eps_dp, d_eps_da


if __name__ == "__main__":

    exit()
