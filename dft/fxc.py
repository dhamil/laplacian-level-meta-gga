import numpy as np
from math import pi
import matplotlib.pyplot as plt
from matplotlib import cm,colors
from itertools import product
import os

from dft.ex import fx_pbe,fx_scan,fx_llmgga
from dft.ec import ec_pbe,ec_scan,ec_llmgga
import settings


bpars = settings.fit_pars

def set_up_dir():
    if not os.path.isdir('./enhance'):
        os.system('mkdir ./enhance')
    return


def eps_x_unif(rs):
    ax = -3.0/(4.0*pi)
    kf = (9*pi/4.0)**(1.0/3.0)/rs
    return ax*kf

def fx_unpol(p,aux,pars={}):

    if settings.X == 'PBE':
        return fx_pbe(p)
    elif settings.X == 'SCAN':
        return fx_scan(p,aux)
    elif settings.X == 'LLMG':
        return fx_llmgga(p,aux,pars)

def fx_spin_resolved(zeta,rs,p,aux,pars={}):

    ex_up = 0.0
    ex_dn = 0.0

    # assuming | grad zeta | = 0
    if zeta > -1.0:
        zfac_up = (1.0/(1.0 + zeta))**(1.0/3.0)
        ex_up = fx_unpol(zfac_up**2*p,zfac_up**2*aux,pars=pars)*eps_x_unif(rs*zfac_up)*(1.0 + zeta)

    if zeta < 1.0:
        zfac_dn = (1.0/(1.0 - zeta))**(1.0/3.0)
        ex_dn = fx_unpol(zfac_dn**2*p,zfac_dn**2*aux,pars=pars)*eps_x_unif(rs*zfac_dn)*(1.0 - zeta)

    return (ex_up + ex_dn)/(2.0*eps_x_unif(rs))

def fc_spin_resolved(zeta,rs,p,aux,pars={}):

    if settings.X == 'PBE':
        eps_c = ec_pbe(zeta,rs,p)
    elif settings.X == 'SCAN':
        eps_c = ec_scan(zeta,rs,p,aux)
    elif settings.X == 'LLMG':
        eps_c = ec_llmgga(zeta,rs,p,aux,pars)

    return eps_c/eps_x_unif(rs)

def fxc(zeta,rs,p,aux,pars={}):

    fx = fx_spin_resolved(zeta,rs,p,aux,pars=pars)
    fc = fc_spin_resolved(zeta,rs,p,aux,pars=pars)
    return fx + fc

def contours(rs_l,z_l,which_l):

    set_up_dir()

    fsz = 20

    p_l = np.linspace(0.0,5.0,300)
    if settings.X == 'SCAN':
        aux_l = np.linspace(0.0,5.0,300)
    elif settings.X == 'LLMG':
        aux_l = np.linspace(-5.0,5.0,600)

    p,aux = np.meshgrid(p_l,aux_l)

    for vars in product(rs_l,z_l,which_l):

        rs,z,which = vars

        if which.lower() == 'x':
            f = fx_spin_resolved(z,rs,p,aux,pars=bpars)
        elif which.lower() == 'c':
            f = fc_spin_resolved(z,rs,p,aux,pars=bpars)
        elif which.lower() == 'xc':
            f = fxc(z,rs,p,aux,pars=bpars)
        else:
            raise ValueError(('Not a recognized enhancement factor, {:}').format(which))

        fig,ax = plt.subplots(figsize=(8,6))

        cts = np.linspace(f.min(),f.max(),20)

        cml = cm.get_cmap('Spectral')
        ccl = cml(np.linspace(.95,0.0,cts.shape[0]))
        ncml = colors.ListedColormap(ccl)
        ncml.set_over('white')
        ncml.set_under('purple')

        cplt=ax.contourf(p,aux,f,cts,cmap=ncml,extend='both')
        ax.set_xlabel('$p$',fontsize=fsz)
        if settings.X == 'SCAN':
            ax.set_ylabel('$\\alpha$',fontsize=fsz)
        elif settings.X == 'LLMG':
            ax.set_ylabel('$q$',fontsize=fsz)
        ax.tick_params(axis='both',labelsize=fsz*.8)
        fig.colorbar(cplt,extend='both',shrink=0.8)
        plt.title('$F^{\\mathrm{'+settings.X+'}}_{\\mathrm{'+which.lower()+'}}$, for '+'$r_s=$'+str(rs)+', $\\zeta=$'+str(z),fontsize=fsz)


        fname = (settings.X).lower()+'_F_'+which.lower()+'_rs_'+str(rs)+'_zeta_'+str(z)+'.pdf'
        plt.savefig('./enhance/'+fname,dpi=600,bbox_inches='tight')

        plt.cla()
        plt.clf()

    return

def plot_2d(rs_l,z_l,which_l,aux_l):

    set_up_dir()
    fsz = 24
    s_l = np.linspace(0.0,5.0,2000)
    p_l = s_l**2

    cml = cm.get_cmap('Set1')
    ccl = cml(np.linspace(0.0,0.9,len(rs_l)*len(aux_l)))

    for vars in product(z_l,which_l):

        z,which = vars

        fig,ax = plt.subplots(figsize=(12,6))

        for ivar,var in enumerate(product(rs_l,aux_l)):

            rs,aux = var

            aux_nm = ''
            if settings.X == 'SCAN':
                aux_nm = '$\\alpha$'
            elif settings.X == 'LLMG':
                aux_nm = '$q$'
            if len(rs_l) > 1 and len(aux_l) > 1:
                lbl = '$r_s=$'+str(rs)+', '+aux_nm+'='+str(aux)
                ptitle = '$F^{\\mathrm{'+settings.X+'}}_{\\mathrm{'+which.lower()+'}}$, for $\\zeta=$'+str(z)
                fname = (settings.X).lower()+'_F_'+which.lower()+'_zeta_'+str(z)+'.pdf'
            elif len(rs_l) > 1:
                lbl = '$r_s=$'+str(rs)
                ptitle = '$F^{\\mathrm{'+settings.X+'}}_{\\mathrm{'+which.lower()+'}}$, for $\\zeta=$'+str(z)+', '+aux_nm+'='+str(aux)
                fname = (settings.X).lower()+'_F_'+which.lower()+'_'+aux_nm+'_'+str(aux)+'_zeta_'+str(z)+'.pdf'
            elif len(aux_l) > 1:
                lbl = aux_nm+'='+str(aux)
                ptitle = '$F^{\\mathrm{'+settings.X+'}}_{\\mathrm{'+which.lower()+'}}$, for $\\zeta=$'+str(z)+', $r_s=$'+str(rs)
                fname = (settings.X).lower()+'_F_'+which.lower()+'_rs_'+str(rs)+'_zeta_'+str(z)+'.pdf'

            if which.lower() == 'x':
                f = fx_spin_resolved(z,rs,p_l,aux,pars=bpars)
            elif which.lower() == 'c':
                f = fc_spin_resolved(z,rs,p_l,aux,pars=bpars)
            elif which.lower() == 'xc':
                f = fxc(z,rs,p_l,aux,pars=bpars)
            else:
                raise ValueError(('Not a recognized enhancement factor, {:}').format(which))

            mid = 0.5*(f.max()-f.min()) + f.min()
            min_ind = np.argmin(abs(f-mid))
            t_deriv = (f[min_ind]-f[min_ind-1])/(s_l[min_ind]-s_l[min_ind-1])
            t_deriv_2 = (f[min_ind]-2*f[min_ind-1]+f[min_ind-2])/(s_l[min_ind]-s_l[min_ind-1])**2
            scl_y = fsz*.6/72.0*(f.max()-f.min())/6.0
            scl_x = fsz*.6/72.0*(s_l.max()-s_l.min())/10.0
            """
            if t_deriv > 0.0:
                if t_deriv_2 >= 0.0:
                    t_lbl_pos = (p_l[min_ind]+scl_x,f[min_ind]+scl_y)
                else:
                    t_lbl_pos = (p_l[min_ind]-scl_x,f[min_ind]+scl_y)
            else:
                if t_deriv_2 >= 0.0:
                    t_lbl_pos = (p_l[min_ind]+scl_x,f[min_ind]+scl_y)
                else:
                    t_lbl_pos = (p_l[min_ind]-scl_x,f[min_ind]-scl_y)
            """
            t_lbl_pos = (max(s_l[min_ind]-scl_x,0.0),f[min_ind])
            ax.annotate(lbl,t_lbl_pos,color=ccl[ivar],fontsize=fsz*.6,bbox={'boxstyle':'round,pad=0.05,rounding_size=1','facecolor':'white','edgecolor':'none'})

            ax.plot(s_l,f,color=ccl[ivar],linewidth=2)

        #ax.legend(fontsize=fsz*.8)
        ax.tick_params(axis='both',labelsize=fsz*.8)

        ax.set_xlabel('$s$',fontsize=fsz)
        ax.set_xlim([s_l.min(),s_l.max()])
        ax.set_ylabel('$F^{\\mathrm{'+settings.X+'}}_{\\mathrm{'+which.lower()+'}}$',fontsize=fsz)
        plt.title(ptitle,fontsize=fsz)

        plt.savefig('./enhance/'+fname,dpi=600,bbox_inches='tight')

        plt.cla()
        plt.clf()


################################################################################


if __name__ == "__main__":
    plot_2d([4.0],[0.0,1.0],'x',np.arange(-2.0,2.1,1.0))
    #plot_2d([1.0,2.0,4.0,10.0,100.0],[0.0,1.0],'c',[0.0])
    #contours([4.0],[0.0,1.0],['x','c','xc'])
