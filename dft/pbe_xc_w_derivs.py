import numpy as np

from constants import pi
from dft.ec_pw92_w_derivs import spinf_w_deriv,ec_pw92_w_derivs

"""
    author: Aaron Kaplan
    contact: kaplan@temple.edu

    adapted from:

        PW92: J.P. Perdew and Y. Wang,
            ``Accurate and simple analytic representation of the electron-gas correlation energy'',
            Phys. Rev. B 45, 13244 (1992).
            https://doi.org/10.1103/PhysRevB.45.13244

        PBE: J.P. Perdew, M. Ernzerhof, and K. Burke,
            ``Generalized Gradient Approximation Made Simple'',
            Phys. Rev. Lett. 77, 3865 (1996).
            https://doi.org/10.1103/PhysRevLett.77.3865

        PBEsol: J.P. Perdew, A. Ruzsinszky, G.I. Csonka, O.A. Vydrov,
                G.E. Scuseria, L.A. Constantin, X. Zhou, and K. Burke,
            ``Restoring the Density-Gradient Expansion for Exchange in
                Solids and Surfaces'',
            Phys. Rev. Lett. 100, 136406 (2008).
            http://dx.doi.org/10.1103/PhysRevLett.100.136406
"""

def ex_pbe(nu,nd,gnu,gnd,var='PBE',oes=False,unp=False):

    """
        Inputs:
            nu (up-spin density)
            nd (down-spin density)
            gnu (|grad nu|)
            gnd (|grad nd|)
        Outputs:
            ex (exchange energy density, integrate to yield exchange energy)
            vxu (d ex / d nu)
            vxd (d ex / d nd)
            vxgu (d ex / d gnu)
            vxgd (d ex / d gnd)

        Optional arguments:
            var : string, 'PBE' or 'PBEsol'
            oes : Boolean, True for one-electron system
            unp : Boolean, True for spin-unpolarized system
                    Speeds calculation up by not wasting repeated computation
    """

    exu,vxu,vxgu = ex_pbe_unpol(2*nu,2*gnu,var=var)
    if oes:
        exd = 0.0
        vxd = np.zeros_like(nu)
        vxgd = vxd[:]
    elif unp:
        return exu, vxu, vxu, vxgu, vxgu
    else:
        exd,vxd,vxgd = ex_pbe_unpol(2*nd,2*gnd,var=var)

    return (exu + exd)/2, vxu, vxd, vxgu, vxgd


def ex_pbe_unpol(n,gn,var='PBE'):

    kf = (3*pi**2*n)**(1/3)
    exlda = -3/(4*pi)*kf*n
    d_exlda_dn = -kf/pi

    p = (gn/(2*kf*n))**2
    d_p_dn = -8/3*p/n
    d_p_dgn = gn/(2*(kf*n)**2)

    kappa = 0.804
    upbe = {'PBE': 0.21951, 'PBEsol': 10.0/81.0}#, 'acPBE': 0.249 }
    fx_den = 1 + upbe[var]*p/kappa
    fx = 1 + kappa - kappa/fx_den

    d_fx_dp = upbe[var]/fx_den**2

    ex = exlda*fx
    d_ex_dn = d_exlda_dn*fx + exlda*d_fx_dp*d_p_dn
    d_ex_dgn = exlda*d_fx_dp*d_p_dgn

    return ex, d_ex_dn, d_ex_dgn


def ec_pbe(nu,nd,gn,var='PBE'):

    """
        Inputs:
            nu (up-spin density)
            nd (down-spin density)
            gn (|grad n|, n = nu + nd)
        Outputs:
            ec (correlation energy density, integrate to yield correlation energy)
            d_ec_dnu (d ec / d nu)
            d_ec_dnd (d ec / d nd)
            d_ec_dgn (d ec / d gn)

        Optional arguments:
            var : string, 'PBE' or 'PBEsol'
    """

    gamma = (1-np.log(2))/pi**2
    beta0 = 0.066725

    n = nu + nd
    rs = (3/(4*pi*n))**(1/3)
    kf = (3*pi**2*n)**(1/3)
    d_rs_dn = -rs/(3*n)

    z = np.minimum(np.maximum((nu - nd)/n,-0.99999999999990),0.99999999999990)
    d_z_dnu = 2*nd/n**2
    d_z_dnd = -2*nu/n**2

    p = (gn/(2*kf*n))**2
    d_p_dn = -8/3*p/n
    d_p_dgn = gn/(2*(kf*n)**2)

    if var == 'PBE':
        beta = beta0
    elif var == 'PBEsol':
        beta = 0.046
    """
    elif var == 'revTPSS':
        beta_a = 0.1
        beta_b = 0.1778
        brs_den = 1 + beta_b*rs
        beta = beta0*(1 + beta_a*rs)/brs_den
        d_beta_drs = beta0*( beta_a - beta_b)/brs_den**2
    """

    phi, d_phi_dz = spinf_w_deriv(z,2/3,oderiv = 1)
    gp3 = gamma*phi**3
    t2 = (3*pi**2/16)**(2/3)*p/(phi**2*rs)
    d_t2_drs = -t2/rs
    d_t2_dz = -2*t2*d_phi_dz/phi
    d_t2_dp = (3*pi**2/16)**(2/3)/(phi**2*rs)

    ecl,d_ecl_drs,d_ecl_dz,_, _ = ec_pw92_w_derivs(z,rs)

    w = np.expm1(-ecl/gp3)
    opw = 1 + w
    d_w_drs = -d_ecl_drs/gp3*opw
    d_w_dz = (-d_ecl_dz + 3*ecl*d_phi_dz/phi)*opw/gp3

    aa = beta/(gamma*w)
    d_aa_drs = -d_w_drs*aa/w
    d_aa_dz = -d_w_dz*aa/w

    y = aa*t2
    y2 = y**2

    d_y_drs = d_aa_drs*t2 + aa*d_t2_drs
    d_y_dz = d_aa_dz*t2 + aa*d_t2_dz
    d_y_dp = aa*d_t2_dp

    gy = (1 + y)/(1 + y + y2)
    dgy = -y*(2 + y)/(1 + y + y2)**2
    d_gy_drs = dgy*d_y_drs
    d_gy_dz = dgy*d_y_dz
    d_gy_dp = dgy*d_y_dp

    bog = beta/gamma
    hc = 1 + bog* t2*gy
    d_hc_drs = bog * (d_t2_drs*gy + t2*d_gy_drs)
    d_hc_dz = bog * (d_t2_dz*gy + t2*d_gy_dz)
    d_hc_dp = bog * (d_t2_dp*gy + t2*d_gy_dp)

    h = gp3*np.log(hc)
    d_h_drs = gp3*d_hc_drs/hc
    d_h_dz = 3*gamma*phi**2*d_phi_dz*np.log(hc) + gp3*d_hc_dz/hc
    d_h_dp = gp3*d_hc_dp/hc

    ec = n*(ecl + h)
    d_eps_drs = d_ecl_drs + d_h_drs
    d_eps_dz = d_ecl_dz + d_h_dz
    d_eps_dp = d_h_dp

    d_ec_dn = ecl + h + n*(d_eps_drs*d_rs_dn + d_eps_dp*d_p_dn)
    d_ec_dnu = d_ec_dn + n*d_eps_dz*d_z_dnu
    d_ec_dnd = d_ec_dn + n*d_eps_dz*d_z_dnd

    d_ec_dgn = n*d_eps_dp*d_p_dgn

    return ec, d_ec_dnu, d_ec_dnd, d_ec_dgn
