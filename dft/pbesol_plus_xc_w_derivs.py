import numpy as np

from constants import pi
from dft.ec_pw92_w_derivs import spinf_w_deriv,ec_pw92_w_derivs


def ex_pbesol_plus(nu,nd,gnu,gnd,lu,ld,zps,oes=False,unp=False):

    exu,vxu,vxgu,vxlu = ex_pbesol_plus_unpol(2*nu,2*gnu,2*lu,zps)
    if oes:
        exd = 0.0
        vxd = np.zeros_like(nu)
        vxgd = vxd[:]
        vxld = vxd[:]
    elif unp:
        return exu, vxu, vxu, vxgu, vxgu, vxlu, vxlu
    else:
        exd,vxd,vxgd,vxld = ex_pbesol_plus_unpol(2*nd,2*gnd,2*ld,zps)

    return (exu + exd)/2, vxu, vxd, vxgu, vxgd, vxlu, vxld


def ex_pbesol_plus_unpol(n,gn,lap,zps):

    kf = (3*pi**2*n)**(1/3)
    exlda = -3/(4*pi)*kf*n
    d_exlda_dn = -kf/pi

    p = (gn/(2*kf*n))**2
    d_p_dn = -8/3*p/n
    d_p_dgn = gn/(2*(kf*n)**2)

    kappa = 0.804
    uak = 10/81
    ussb = 146/2025

    q = lap/(4*kf**2*n)
    d_q_dn = -5/3*q/n
    d_q_dlap = 1/(4*kf**2*n)

    b1 = 0.5*(uak/kappa - 25*ussb/(16*uak))

    """
    delta2_damp = np.exp(-zps['c0']*abs(b1)*p)
    delta2 = b1*p*delta2_damp
    d_delta2_dp = b1*(1 - zps['c0']*abs(b1)*p)*delta2_damp

    delta = uak*(1 + delta2)*p + ussb*(q - 5/4*p)**2
    d_delta_dp = uak*(1 + delta2 + d_delta2_dp*p) - 5/2*ussb*(q - 5/4*p)
    d_delta_dq = 2*ussb*(q - 5/4*p)
    """

    """
    delta2_damp = np.exp(b1*p)
    delta2 = b1*p*delta2_damp
    d_delta2_dp = b1*(1 + b1*p)*delta2_damp

    qr_damp = np.exp(-5*ussb*p/2)
    qr = q*qr_damp
    d_qr_dq = qr_damp
    d_qr_dp = -5*ussb/2*qr

    delta = uak*p*(1 + delta2)**2 + ussb*(qr - 5/4*p)**2
    d_delta_dp = uak*(1 + delta2)**2 + 2*uak*p*(1 + delta2)*d_delta2_dp \
        + ussb*(qr - 5/4*p)*(2*d_qr_dp - 5/2)
    d_delta_dq = 2*ussb*(qr - 5/4*p)*d_qr_dq
    """
    delta = uak*p + uak**2/kappa*p**2 + ussb*np.tanh(q**2)-ussb*np.tanh(5/2*p*q)
    fac = np.zeros_like(p)
    tmp = 5/2*p*q
    idr = tmp >= 0.0
    fac[idr] = 4*np.exp(-2*tmp[idr])/(1 + np.exp(-2*tmp[idr]))**2
    idr = tmp < 0.0
    fac[idr] = 4*np.exp(2*tmp[idr])/(1 + np.exp(2*tmp[idr]))**2
    d_delta_dp = uak + 2*uak**2/kappa*p - 5/2*q*ussb*fac

    fac2 = 4*np.exp(-2*q**2)/(1 + np.exp(-2*q**2))**2
    d_delta_dq = 2*q*ussb*fac2 - 5/2*p*ussb*fac

    fx_den = 1 + delta/kappa
    fx = 1 + kappa - kappa/fx_den

    d_fx_dp = d_delta_dp/fx_den**2
    d_fx_dq = d_delta_dq/fx_den**2

    ex = exlda*fx
    d_ex_dn = d_exlda_dn*fx + exlda*(d_fx_dp*d_p_dn + d_fx_dq*d_q_dn)
    d_ex_dgn = exlda*d_fx_dp*d_p_dgn
    d_ex_dlap = exlda*d_fx_dq*d_q_dlap

    return ex, d_ex_dn, d_ex_dgn, d_ex_dlap


def ec_pbesol_plus(nu,nd,gn):

    gamma = (1-np.log(2))/pi**2
    beta0 = 0.066725

    n = nu + nd
    rs = (3/(4*pi*n))**(1/3)
    kf = (3*pi**2*n)**(1/3)
    d_rs_dn = -rs/(3*n)

    z = np.minimum(np.maximum((nu - nd)/n,-0.99999999999990),0.99999999999990)
    d_z_dnu = 2*nd/n**2
    d_z_dnd = -2*nu/n**2

    p = (gn/(2*kf*n))**2
    d_p_dn = -8/3*p/n
    d_p_dgn = gn/(2*(kf*n)**2)

    beta_a = 0.5
    beta_b = 0.083335
    beta_c = 0.148165

    brs_den = 1 + beta_a*rs + beta_c*rs**2
    d_brs_den_drs = beta_a + 2*beta_c*rs

    beta = beta0*(1 + beta_a*rs + beta_b*rs**2)/brs_den
    d_beta_drs = (beta0*(beta_a + 2*beta_b*rs) - d_brs_den_drs*beta )/brs_den

    phi, d_phi_dz = spinf_w_deriv(z,2/3,oderiv = 1)
    gp3 = gamma*phi**3
    t2 = (3*pi**2/16)**(2/3)*p/(phi**2*rs)
    d_t2_drs = -t2/rs
    d_t2_dz = -2*t2*d_phi_dz/phi
    d_t2_dp = (3*pi**2/16)**(2/3)/(phi**2*rs)

    ecl,d_ecl_drs,d_ecl_dz,_, _ = ec_pw92_w_derivs(z,rs)

    w = np.expm1(-ecl/gp3)
    opw = 1 + w
    d_w_drs = -d_ecl_drs/gp3*opw
    d_w_dz = (-d_ecl_dz + 3*ecl*d_phi_dz/phi)*opw/gp3

    aa = beta/(gamma*w)
    d_aa_drs = d_beta_drs/(gamma*w) - d_w_drs*aa/w
    d_aa_dz = -d_w_dz*aa/w

    y = aa*t2
    y2 = y**2

    d_y_drs = d_aa_drs*t2 + aa*d_t2_drs
    d_y_dz = d_aa_dz*t2 + aa*d_t2_dz
    d_y_dp = aa*d_t2_dp

    gy = (1 + y)/(1 + y + y2)
    dgy = -y*(2 + y)/(1 + y + y2)**2
    d_gy_drs = dgy*d_y_drs
    d_gy_dz = dgy*d_y_dz
    d_gy_dp = dgy*d_y_dp

    bog = beta/gamma
    d_bog_drs = d_beta_drs/gamma

    hc = 1 + bog* t2*gy
    d_hc_drs = d_bog_drs*t2*gy + bog * (d_t2_drs*gy + t2*d_gy_drs)
    d_hc_dz = bog * (d_t2_dz*gy + t2*d_gy_dz)
    d_hc_dp = bog * (d_t2_dp*gy + t2*d_gy_dp)

    h = gp3*np.log(hc)
    d_h_drs = gp3*d_hc_drs/hc
    d_h_dz = 3*gamma*phi**2*d_phi_dz*np.log(hc) + gp3*d_hc_dz/hc
    d_h_dp = gp3*d_hc_dp/hc

    ec = n*(ecl + h)
    d_eps_drs = d_ecl_drs + d_h_drs
    d_eps_dz = d_ecl_dz + d_h_dz
    d_eps_dp = d_h_dp

    d_ec_dn = ecl + h + n*(d_eps_drs*d_rs_dn + d_eps_dp*d_p_dn)
    d_ec_dnu = d_ec_dn + n*d_eps_dz*d_z_dnu
    d_ec_dnd = d_ec_dn + n*d_eps_dz*d_z_dnd

    d_ec_dgn = n*d_eps_dp*d_p_dgn

    return ec, d_ec_dnu, d_ec_dnd, d_ec_dgn
