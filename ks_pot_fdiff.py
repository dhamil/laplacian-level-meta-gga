import numpy as np
from os import system,path
import matplotlib.pyplot as plt

import settings
from constants import pi
from dft.scan_xc_w_derivs import exc_scan_w_derivs
from dft.pbe_xc_w_derivs import ex_pbe,ec_pbe
from dft.ec_pw92_w_derivs import ec_pw92,ex_lsda
from dft.pbesol_plus_xc_w_derivs import ex_pbesol_plus,ec_pbesol_plus

base = './dfa_results/ks_pot/'
dat_base = base + 'pot_data/'
if not path.isdir(dat_base):
    system('mkdir -p '+dat_base)

def fdiff(dr,oderiv,funtab):

    if oderiv == 1:
        fdc = np.asarray([-1.0/60.0, 3.0/20.0, -3.0/4.0, 0.0, 3.0/4.0,
            -3.0/20.0, 1.0/60.0])
        fdf = np.asarray([-49.0/20.0,6.0,-15.0/2.0,20.0/3.0, -15.0/4.0,
            6.0/5.0,-1.0/6.0 ])
    elif oderiv == 2:
        fdc = np.asarray([1.0/90.0, -3.0/20.0, 3.0/2.0, -49.0/18.0, 3.0/2.0,
            -3.0/20.0, 1.0/90.0])
        fdf = np.asarray([469.0/90.0,-223.0/10.0, 879.0/20.0, -949.0/18.0, 41.0,
            -201.0/10.0,1019.0/180.0,-7.0/10.0])

    nfdf = 7 + oderiv -1
    nr = len(funtab)
    dfun = np.zeros(nr)
    for i in range(nr):
        if i < 3:
            dfun[i] = np.dot(fdf,funtab[i:i+nfdf])
        elif i > nr-4:
            dfun[i] = -np.dot(fdf,funtab[i-nfdf:i])
        else:
            dfun[i] = np.dot(fdc,funtab[i-3:i+4])
    dfun /= dr**oderiv

    return dfun

def sph_div(dr,r,fun):
    # for a grid evenly spaced in r, spacing dr
    #r2 = r**2
    fun_div = fdiff(dr,1,fun)
    return fun_div + 2*fun/r

def sph_div_log_rad(drho,r,fun):
    # for a grid evenly spaced in rho = ln(r), spacing drho
    fun_div = fdiff(drho,1,fun)
    return (fun_div + 2*fun)/r

def sph_lap(dr,r,fun):
    # for a grid evenly spaced in r, spacing dr
    fun_lap = fdiff(dr,2,r*fun)
    return fun_lap/r

def sph_lap_log_rad(drho,r,fun):
    # for a grid evenly spaced in rho = ln(r), spacing drho
    tmp1 = fdiff(drho,1,fun)
    tmp2 = fdiff(drho,2,fun)
    return (tmp1 + tmp2)/r**2

def exc_selec(wfxc,n0,n1,agn0,agn1,agnt,l0,l1,tau0,tau1,zps,oes=False,unp=False):
    """
    exc_dat indexes:
        0 = ex density     6 = d_ex_dgnup     11 = d_ex_dlup    15 = d_ex_dtup
        1 = ec density     7 = d_ec_dgnup     12 = d_ec_dlup    16 = d_ex_dtdn
        2 = d_ex_dnup      8 = d_ex_dgndn     13 = d_ex_dldn    17 = d_ec_dtau
        3 = d_ec_dnup      9 = d_ec_dgndn     14 = d_ec_dldn
        4 = d_ex_dndn      10 = d_ec_dgn
        5 = d_ec_dndn
    """

    if wfxc == 'LSDA':
        exc_dat = np.zeros((n0.shape[0],18))
        exc_dat[:,0], exc_dat[:,2], exc_dat[:,4] = ex_lsda(n0,n1)
        exc_dat[:,1], exc_dat[:,3], exc_dat[:,5] = ec_pw92(n0,n1)

    elif wfxc in ['PBE','PBEsol']:
        exc_dat = np.zeros((n0.shape[0],18))
        exc_dat[:,0], exc_dat[:,2], exc_dat[:,4], exc_dat[:,6], exc_dat[:,8] = \
            ex_pbe(n0,n1,agn0,agn1,var=wfxc,oes=oes,unp=unp)

        exc_dat[:,1], exc_dat[:,3], exc_dat[:,5], exc_dat[:,10] = \
            ec_pbe(n0,n1,agnt,var=wfxc)

    elif wfxc == 'LLMG':
        exc_dat = np.zeros((n0.shape[0],18))
        exc_dat[:,0], exc_dat[:,2], exc_dat[:,4], exc_dat[:,6], exc_dat[:,8], \
            exc_dat[:,11], exc_dat[:,13] = ex_pbesol_plus(n0,n1,agn0,agn1,l0,l1,\
            zps,oes=oes,unp=unp)
        exc_dat[:,1], exc_dat[:,3], exc_dat[:,5], exc_dat[:,10] = \
            ec_pbesol_plus(n0,n1,agnt)

    elif wfxc in ['SCAN','r2SCAN','SCAN-L','r2SCAN-L']:
        exc_dat = exc_scan_w_derivs(n0,n1,agn0,agn1,agnt,tau0,tau1,l0,l1,zps,\
            wfxc,oes=oes,unp=unp)
    else:
        raise SystemExit('Derivatives for {:} not implemented yet'.format(wfxc))

    return exc_dat

def vxc_rad(wfxc,drho,r,n0,n1,gn0,gn1,gnt,l0,l1,tau0,tau1,zps,oes=False):

    agn0 = np.abs(gn0)
    agn1 = np.abs(gn1)
    agnt = np.abs(gnt)

    exc_dat = exc_selec(wfxc,n0,n1,agn0,agn1,agnt,l0,l1,tau0,tau1,zps,oes=oes)

    nr = r.shape[0]
    vx_grad_0 = sph_div(drho,r,exc_dat[:,6]*gn0/agn0)
    vx_lap_0 = sph_lap(drho,r,exc_dat[:,11])
    vx0 = exc_dat[:,2] - vx_grad_0 + vx_lap_0

    if oes:
        vx1 = np.zeros(nr)
    else:
        vx_grad_1 = sph_div(drho,r,exc_dat[:,8]*gn1/agn1)
        vx_lap_1 = sph_lap(drho,r,exc_dat[:,13])
        vx1 = exc_dat[:,4] - vx_lap_1 + vx_lap_0

    vc_grad_01 = sph_div(drho,r,(gn0 + gn1)*exc_dat[:,10]/agnt)
    if wfxc in ['SCAN-L','r2SCAN-L']:
        vc_grad_0 = sph_div(drho,r,gn0*exc_dat[:,7]/agn0)
    else:
        vc_grad_0 = 0.0
    vc_lap_0 = sph_lap(drho,r,exc_dat[:,12])
    vc0 = exc_dat[:,3] - vc_grad_01 - vc_grad_0 + vc_lap_0

    if oes:
        vc1 = np.zeros(nr)
    else:
        if wfxc in ['SCAN-L','r2SCAN-L']:
            vc_grad_1 = sph_div(drho,r,gn1*exc_dat[:,9]/agn1)
        else:
            vc_grad_1 = 0.0
        vc_lap_1 = sph_lap(drho,r,exc_dat[:,14])
        vc1 = exc_dat[:,5] - vc_grad_01 - vc_grad_1 + vc_lap_1

    return exc_dat[:,0], exc_dat[:,1], vx0, vx1, vc0, vc1

def h_atom_vh(r):
    tmp = np.exp(-2*r)
    return (1-tmp)/r - tmp

def vh_rad(wg,r,n):
    nr = r.shape[0]
    vh = np.zeros(nr)
    """
        wg includes a factor of 4*pi*r**2
    """
    itgrd1 = n*wg
    itgrd2 = n*wg/r
    for ir in range(nr):
        vh[ir] = np.sum(itgrd1[:ir])/r[ir] + np.sum(itgrd2[ir:])
    return vh

def rad_pot(atdens,at,dr,zps):

    ex, ec, vx_up, vx_dn, vc_up, vc_dn = vxc_rad(settings.X,dr,atdens[:,0], \
        atdens[:,2],atdens[:,3],atdens[:,4],atdens[:,5],atdens[:,6],\
        atdens[:,9],atdens[:,10],atdens[:,7],atdens[:,8],zps,oes=(at == 'H'))

    print('System: atom {:}'.format(at))
    int_ex = np.sum(atdens[:,1]*ex)
    int_ec = np.sum(atdens[:,1]*ec)
    print('Ex = {:.12f} Eh'.format(int_ex))
    print('Ec = {:.12f} Eh'.format(int_ec))
    print('Exc = {:.12f} Eh'.format(int_ex+int_ec))

    nelec = int(round(np.sum(atdens[:,1]*(atdens[:,2]+atdens[:,3])),0))
    vext = -nelec/atdens[:,0]

    vh_up = vh_rad(atdens[:,1],atdens[:,0],atdens[:,2])
    vh_dn = vh_rad(atdens[:,1],atdens[:,0],atdens[:,3])
    vks_up = vx_up + vc_up + vh_up + vext
    vks_dn = vx_dn + vc_dn + vh_dn + vext

    modstr = ''
    pmodstr = ''
    if settings.X[-2:] == '-L':
        modstr = '_'+settings.KEMOD
        pmodstr = ', '+settings.KEMOD

    base2 = base + settings.X+modstr+'/'
    dat_base2 = dat_base + settings.X+modstr+'/'
    for tmp in [base2,dat_base2]:
        if not path.isdir(tmp):
            system('mkdir -p '+tmp)

    np.savetxt(dat_base2+'atom_'+at+'_ks_pot_XC='+settings.X+modstr+'.csv', \
        np.transpose((atdens[:,0],vx_up,vx_dn,vc_up,vc_dn,vks_up,vks_dn)),delimiter=',', \
            header='r, vx up, vx dn, vc up, vc dn, vks_up, vks_dn')

    clist = ['tab:blue','tab:orange','tab:green','tab:red']

    fig,ax = plt.subplots(figsize=(8,6))

    ax.plot(atdens[:,0],vext,color=clist[0],label='$-{:}/r$'.format(nelec))
    ax.plot(atdens[:,0],vx_up,color=clist[1],label='$v^{\\uparrow}_{\\mathrm{x}}(r)$')
    ax.plot(atdens[:,0],vc_up,color=clist[2],label='$v^{\\uparrow}_{\\mathrm{c}}(r)$')
    ax.plot(atdens[:,0],vks_up,color=clist[3],label='$v^{\\uparrow}_{\\mathrm{KS}}(r)$')

    ax.set_xlabel('$r$ (bohr)',fontsize=18)
    ax.set_xlim([0.0,6.0])
    ax.set_ylim([-5.0,1.5])
    ax.hlines(0.0,0.0,6.0,color='gray')
    ax.legend(loc='lower center',fontsize=18)
    ax.tick_params(axis='both',labelsize=18)

    if settings.X == 'r2SCAN-L' and settings.KEMOD == 'RPP1':
        pstr = 'OFR2'
    else:
        pstr = settings.X+pmodstr
    ax.annotate(pstr,(4.,-4.8),fontsize=18)
    plt.savefig(base2+'{:}_atom_vks_fdiff_XC='.format(at)+settings.X+modstr+'.pdf',\
        dpi=600,bbox_inches='tight')

    plt.cla()
    plt.clf()

    return

def cart_pot_1d_unp(wfxc,rs0,dx,x,n,gn,lap,tau,zps,tmpd):

    n0 = 3/(4*pi*rs0**3)
    kf0 = (9*pi/4)**(1/3)/rs0
    lf0 = 2*pi/kf0

    vx_bulk = -kf0/pi
    _,vc_bulk,_ = ec_pw92(n0/2,n0/2)

    nh = n/2
    agn = np.abs(gn)
    agnh = agn/2
    lh = lap/2
    th = tau/2

    exc_dat = exc_selec(wfxc,nh,nh,agnh,agnh,agn,lh,lh,th,th,zps,oes=False,unp=True)
    lsd_dat = exc_selec('LSDA',nh,nh,agnh,agnh,agn,lh,lh,th,th,zps,oes=False,unp=True)

    nx = x.shape[0]
    vx_grad_0 = fdiff(dx,1,exc_dat[:,6]*gn/2/agnh)
    vx_lap_0 = fdiff(dx,2,exc_dat[:,11])
    vx0 = exc_dat[:,2] - vx_grad_0 + vx_lap_0

    vc_grad_01 = fdiff(dx,1,gn*exc_dat[:,10]/agn)
    if wfxc in ['SCAN-L','r2SCAN-L']:
        vc_grad_0 = fdiff(dx,1,gn/2*exc_dat[:,7]/agnh)
    else:
        vc_grad_0 = 0.0
    vc_lap_0 = fdiff(dx,2,exc_dat[:,12])
    vc0 = exc_dat[:,3] - vc_grad_01 - vc_grad_0 + vc_lap_0

    vxd = vx0/vx_bulk
    vcd = vc0/vc_bulk
    vxc_bulk = vx_bulk + vc_bulk
    vxcd = (vx0+vc0)/vxc_bulk
    xd = x/lf0
    xmsk = (-4 <= xd) & (xd <= 2)

    modstr = ''
    pmodstr = ''
    if settings.X[-2:] == '-L':
        modstr = '_'+settings.KEMOD
        pmodstr = ', '+settings.KEMOD

    base2 = base + settings.X+modstr+'/'
    dat_base2 = dat_base + settings.X+modstr+'/'
    for tmp in [base2,dat_base2]:
        if not path.isdir(tmp):
            system('mkdir -p '+tmp)

    np.savetxt(dat_base2+'js_rs0={:}'.format(rs0)+'_xc_pot_XC='+settings.X+modstr+'.csv', \
        np.transpose((xd,vxd,vcd)),delimiter=',', \
            header='x/lambda_F0, vx/vx_0, vc/vc_0')

    fig,ax = plt.subplots(figsize=(8,6))

    xdiff = vxd - lsd_dat[:,2]/vx_bulk
    cdiff = vcd - lsd_dat[:,3]/vc_bulk

    ax.plot(xd[xmsk],xdiff[xmsk],color='darkblue',label= \
        '$[v_{\\mathrm{x}}(x)-v^{\\mathrm{LDA}}_{\\mathrm{x}}(x)]/\\overline{v_\\mathrm{x}}$',linestyle='--')
    ax.plot(xd[xmsk],cdiff[xmsk],color='darkorange',label=\
        '$[v_{\\mathrm{c}}(x)-v^{\\mathrm{LDA}}_{\\mathrm{c}}(x)]/\\overline{v_\\mathrm{c}}$',linestyle='-.')
    ax.plot(xd[xmsk],vxcd[xmsk],color='tab:green',label=\
        '$v_{\\mathrm{xc}}(x)/\\overline{v_\\mathrm{xc}}$',linestyle='-')

    tind = np.argmin(np.abs(xd + 3.95))
    xlbl = xd[tind]

    vxlbl = xdiff[tind]
    sgnvxlbl = np.sign(vxlbl)
    vclbl = cdiff[tind]
    sgnvclbl = np.sign(vclbl)

    #xfac = 1 + .1/abs(vxlbl)
    #if sgnvxlbl == sgnvclbl :
    #    cfac = -(1 + .15/abs(vclbl))
    #else:
    #    cfac = (1 + .15/abs(vclbl))
    ax.annotate('$[v_{\\mathrm{x}}(x)-v^{\\mathrm{LDA}}_{\\mathrm{x}}(x)]/\\overline{v_\\mathrm{x}}$',
        (xlbl,.15),color='darkblue',fontsize=18)
    ax.annotate('$[v_{\\mathrm{c}}(x)-v^{\\mathrm{LDA}}_{\\mathrm{c}}(x)]/\\overline{v_\\mathrm{c}}$',
        (xlbl,-.2),color='darkorange',fontsize=18)
    ax.annotate('$v_{\\mathrm{xc}}(x)/\\overline{v_\\mathrm{xc}}$',
        (xlbl,vxcd[tind]*(1 + np.sign(vxcd[tind])*.05)),color='tab:green',fontsize=18)


    tmpd[rs0] = np.transpose((xd[xmsk],vxcd[xmsk] - (lsd_dat[xmsk,2]+lsd_dat[xmsk,3])/vxc_bulk ))

    ax.set_xlabel('$x/\\overline{\\lambda_\\mathrm{F}}$',fontsize=18)
    ax.set_xlim([-4,2])
    ax.set_ylim([-.6,1.5])
    ax.vlines(0.0,*ax.get_ylim(),color='gray')
    ax.hlines(0.0,*ax.get_xlim(),color='gray')
    #ax.legend(loc=(0.35,0.05),fontsize=18,title=\
    #    '$\\overline{{r_\\mathrm{{s}} }}={:}$'.format(rs0),title_fontsize=18)
    ax.tick_params(axis='both',labelsize=18)

    if settings.X == 'r2SCAN-L' and settings.KEMOD == 'RPP1':
        pstr = 'OFR2'
    else:
        pstr = settings.X+pmodstr
    ax.annotate(pstr,(-3.95,-.55),fontsize=18)
    ax.annotate('$\\overline{{r_\\mathrm{{s}} }}={:}$'.format(rs0),(1.2,-0.55),fontsize=18)
    #plt.show() ; exit()
    plt.savefig(base2+'rs0={:}_js_vxc_fdiff_XC='.format(rs0)+settings.X+modstr+'.pdf',\
        dpi=600,bbox_inches='tight')

    plt.cla()
    plt.clf()

    return tmpd

def pot_calc(sysd,zps):

    if settings.X in settings.fxcd['TMGGA'] or settings.C in settings.fxcd['TMGGA']:
        if settings.routine['vxc'] != 'H':
            print('WARNING: I can only compute the KS potential for LSDAs, GGAs, and LLMGGAs')
            print('You have selectd a TMGGA; computing only the semilocal KS potential')
    if settings.X != settings.C:
        print('WARNING: At this time, I can only compute the potential for the same X and C functional')
        print('Ignoring your choice of correlation functional')

    for sys in sysd:
        if sys == 'atom':
            for at in sysd[sys]:
                atdens = np.genfromtxt('./data_files/'+at+'_'+settings.dataset+'_rect.csv',delimiter=',',dtype=None,skip_header=1)
                """
                atdens indices:
                0 = r                       4 = |grad n up|         7 = tau up
                1 = integration weight      5 = |grad n down|       8 = tau down
                2 = n up                    6 = |grad n|            9 = lap n up
                3 = n down                                         10 = lap n down
                """
                # density file only saves absolute value of gradient, we need the sign
                dr = atdens[1,0]-atdens[0,0]
                atdens[:,4] = fdiff(dr,1,atdens[:,2])
                atdens[:,5] = fdiff(dr,1,atdens[:,3])
                atdens[:,6] = atdens[:,4] + atdens[:,5]

                rad_pot(atdens,at,dr,zps)

        elif sys == 'js':
            rsd = {}
            for rs in sysd[sys]:
                prefix = 'rs_'+str(rs)
                tfile = './jellium_scf/data/surface/'+prefix+'/'+prefix+'_density.csv'
                x,n,_,lap,tau = np.transpose(np.genfromtxt(tfile,delimiter=',',skip_header=1))
                dx = x[1]-x[0]
                # density file only saves absolute value of gradient, we need the sign
                gn = fdiff(dx,1,n)
                rsd = cart_pot_1d_unp(settings.X,rs,dx,x,n,gn,lap,tau,zps,rsd)

            """
            fig,ax = plt.subplots(figsize=(8,6))
            lsls = ['-','--','-.',':']
            cls = ['darkblue','darkorange','darkgreen','tab:red']
            for irs,rs in enumerate(sysd[sys]):
                plt.plot(rsd[rs][:,0],rsd[rs][:,1],linestyle=lsls[irs],color=cls[irs])

            ax.set_xlabel('$x/\\overline{\\lambda_\\mathrm{F}}$',fontsize=18)
            ax.set_xlim([-4,2])
            ax.set_ylim([0,1.1])
            ax.vlines(0.0,*ax.get_ylim(),color='gray')
            #ax.legend(loc=(0.35,0.05),fontsize=18,title=\
            #    '$\\overline{{r_\\mathrm{{s}} }}={:}$'.format(rs0),title_fontsize=18)
            ax.tick_params(axis='both',labelsize=18)

            plt.show() ; exit()
            plt.savefig(base2+'rs0={:}_js_vxc_fdiff_XC='.format(rs0)+settings.X+modstr+'.pdf',\
                dpi=600,bbox_inches='tight')
            """
    return


if __name__=='__main__':

    pot_calc({'js': [4]},settings.fit_pars)
