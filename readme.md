***
# Laplacian-level meta-GGA
***
## Aaron D. Kaplan and John P. Perdew
***

This code supports the Laplacian-level meta-GGA (LLMGGA) designed for solids

***
# Primary files:

`constants.py` : contains global constants (pi, conversion between Hartree and eV, or Hartree/bohr^2 and erg/cm^2)

`main.py` : main executable for fitting the LLMGGA, plotting the XC enhancement factor of various functionals, and getting error scores for the appropriate norms used in the fitting (again for various functionals)

`jellium_norms.py` : calculates surface formation and curvature energies for jellium surfaces and clusters

`lsq.py` : lightweight least-squares routine, needed for extrapolating large-Z atomic coefficients and surface/curvature energies from jellium clusters

`settings.py` : settings file, controls the reference values in the fit, what routine is performed (fitting, calculating error scores, plotting, regenerating reference densities, etc.)

***
# Some important subdirectories:

`atomic_densites` : contains subroutines for generating Roothaan-Hartree-Fock reference densities from established data sets

`dfa_results` : stores data on the functionals available in `dft`. Ex. possible parameterizations for the LLMGGA, or error readouts for a functional

`dft` : `ex.py` and `ec.py` contain all X and C functional options (can be combined in whatever way, say SCAN X with LSDA C). `fxc.py` plots the XC enhancement factor, plots are stored in `enhance`
| X | C |
|:---:|:---:|
| LLMGGA | LLGMA |
| LSDA | PW92 |
| PBE  | PBE  |
| SCAN | SCAN |
| rSCAN | rSCAN |
| r<sup>2</sup>SCAN | r<sup>2</sup>SCAN |
| SCAN-L | SCAN-L (deorbitalized SCAN)|
| rSCAN-L | rSCAN-L |
| r<sup>2</sup>SCAN-L | r<sup>2</sup>SCAN-L |
| | ACGGA |

(There are many more DFAs available in these subroutines.)

`jellium_scf` : contains subroutines for calculating LSDA reference densities for planar jellium surfaces and jellium clusters

`ks_potential` : contains subroutines for calculating the KS potential of the LLMGGA using finite differences for the gradient and Laplacian terms

`notes` : some notes detailing the methods used
