import numpy as np
from os import path,system
import multiprocessing as mproc
from itertools import product
from time import time
from math import ceil

import settings
from constants import pi
from lsq import lst_sq_lin
from dft.ex import ex_den,ex_den_unp
from dft.ec import ec_den
from reference_data import lzx1,lzx2,lzc1,lzxc1,lda_ref_x,exx,ecc,excx,atom_xc,dmc_ref_delta_gamma_xc
from jellium_norms import get_jerr_list, init_jellium_calc, get_jellium_residuals

from ks_pot_fdiff import pot_calc

from functools import partial
from scipy.optimize import minimize

#from guppy import hpy

base = './data_files/'

def get_atomic_norm_densities():
    atom_dvar = {}
    atlist = ['Ne','Ar','Kr','Xe','H']
    if settings.routine['fit'] in ['report','filter','fixed_list','tau_fit']:
        atlist.append('He')
    for att in atlist:
        dat = np.genfromtxt(base+att+'_'+settings.dataset+'.csv',delimiter=',',dtype=None,skip_header=1)
        # integration weight, n_up, |grad n_up|, tau_up, lap n_up
        atom_dvar[att] = np.transpose((dat[:,1],dat[:,2],dat[:,4],dat[:,7],dat[:,9]))
        #print(att,np.sum(dat[:,1]*(dat[:,2]+dat[:,3])))
    return atom_dvar

def get_lz_x(exl,xlda):
    """
        see [SP2019]
    """
    zl = np.asarray([10.0, 18.0, 36.0, 54.0])
    fn = (np.asarray(exl) - xlda)/zl
    fitfn = lambda p, x: p[0] + p[1]*x
    res = lambda p, x, y: fitfn(p,x) - y

    cbx,ccx,_ = lst_sq_lin(zl**(-1.0/3.0),fn)
    return cbx,ccx

def get_lz_c(ecl):
    """
        See [BCGP2016]
    """

    zl = np.asarray([10.0, 18.0, 36.0, 54.0])
    tf_c0 = -2.0/3.0*(1.0-np.log(2.0))/pi**2*zl*np.log(zl)
    fn = (np.array(ecl) - tf_c0)/zl
    n_homo = np.arange(2.0,6.0,1.0)
    cbc,ccc,q = lst_sq_lin(1.0/n_homo,fn)
    return cbc#,ccc,q

def int_ex(dfa,att,zps,qoe=False,unp=False):
    wg,nu,nd,gnu,gnd,gt,tu,td,lnu,lnd = np.transpose(att)
    exd = ex_den(dfa,nu,nd,gnu,gnd,tu,td,lnu,lnd,zps,oes=qoe,unp=unp)
    exd[nu+nd<1.e-14] = 0.0
    return np.sum(exd*wg)

def int_ec(dfa,att,zps,unp=False):
    wg,nu,nd,gnu,gnd,gt,tu,td,lnu,lnd = np.transpose(att)
    ecd = ec_den(dfa,nu,nd,gnu,gnd,gt,tu,td,lnu,lnd,zps,unp=unp)
    ecd[nu+nd<1.e-14] = 0.0
    return np.sum(ecd*wg)

def int_exc(dfa_x,dfa_c,at,zps,qoe=False,unp=False):
    wg,nu,nd,gnu,gnd,gt,tu,td,lnu,lnd = np.transpose(at)
    exd = ex_den(dfa_x,nu,nd,gnu,gnd,tu,td,lnu,lnd,zps,oes=qoe,unp=unp)
    exd[nu+nd<1.e-14] = 0.0
    ecd = ec_den(dfa_c,nu,nd,gnu,gnd,gt,tu,td,lnu,lnd,zps,oes=qoe,unp=unp)
    ecd[nu+nd<1.e-14] = 0.0
    return np.sum((exd+ecd)*wg)


def int_exc_unp(dfa_x,dfa_c,att,zps):

    wg,nu,gnu,tu,lnu = np.transpose(att)
    exd = ex_den_unp(dfa_x,2*nu,2*gnu,2*tu,2*lnu,zps)
    exd[2*nu<1.e-14] = 0.0
    exint = np.sum(exd*wg)

    ecd = ec_den(dfa_c,nu,nu,gnu,gnu,2*gnu,tu,tu,lnu,lnu,zps,unp=True)
    ecd[2*nu<1.e-14] = 0.0
    ecint = np.sum(ecd*wg)

    return exint,ecint,exint+ecint

def int_exc_one_elec(dfa_x,dfa_c,att,zps):

    wg,nu,gnu,tu,lnu = np.transpose(att)
    tmp = np.zeros(nu.shape)
    exd = 0.5*ex_den_unp(dfa_x,2*nu,2*gnu,2*tu,2*lnu,zps)
    exd[nu<1.e-14] = 0.0
    exint = np.sum(exd*wg)

    ecd = ec_den(dfa_c,nu,tmp,gnu,tmp,gnu,tu,tmp,lnu,tmp,zps,unp=False,oes=True)
    ecd[2*nu<1.e-14] = 0.0
    ecint = np.sum(ecd*wg)

    return exint,ecint,exint+ecint

def get_atom_errs(tmp,dens):

    nwgt = settings.norm_wgts
    lzas = ['Ne','Ar','Kr','Xe']
    nzas = len(lzas)
    zps = {}
    #zps['c0'],zps['c1'],zps['c2'],zps['c3'] = tmp
    parkeys = ['c0','c1','c2','c3']
    for ic in range(settings.npar):
        zps[parkeys[ic]] = tmp[ic]
    for ic in range(settings.npar,4):
        zps[parkeys[ic]] = 0.0
    ex_l = np.zeros(4)
    ec_l = np.zeros(4)
    aresx = 0.0
    aresc = 0.0
    aresxc = 0.0
    for iatom,atom in enumerate(lzas):
        ex_l[iatom],ec_l[iatom],_ = int_exc_unp(settings.X,settings.C,dens[atom],zps)
        aresx += 100*abs(ex_l[iatom]/exx[atom]-1)/nzas
        aresc += 100*abs(ec_l[iatom]/ecc[atom]-1)/nzas
        aresxc += 100*abs((ex_l[iatom]+ec_l[iatom])/excx[atom]-1)/nzas
    bx,cx = get_lz_x(ex_l,lda_ref_x)
    bc = get_lz_c(ec_l)
    err_bx = 100.0*(bx - lzx1)/lzx1
    err_bc = 100.0*(bc - lzc1)/lzc1
    err_cx = 100.0*(cx - lzx2)/lzx2
    _,_,hexc = int_exc_one_elec(settings.X,settings.C,dens['H'],zps)
    herr = (hexc-atom_xc['H'])/atom_xc['H']*100.0
    absres = (nwgt['SAXC']*aresxc**2 +nwgt['Bx']*err_bx**2 + nwgt['Cx']*err_cx**2 \
    + nwgt['Bc']*err_bc**2 + nwgt['H']*herr**2)#**(0.5)
    return zps['c0'],zps['c1'],zps['c2'],zps['c3'],aresx,aresc,aresxc,bx,err_bx,cx,err_cx,bc,err_bc,hexc,herr,absres

def get_atom_errs_list(tmpl,dens,resl):

    nwgt = settings.norm_wgts
    for tmp in tmpl:
        lzas = ['Ne','Ar','Kr','Xe']
        zps = {}
        zps['c0'], zps['c1'], zps['c2'],zps['c3'] = tmp
        ex_l = np.zeros(4)
        ec_l = np.zeros(4)
        for iatom,atom in enumerate(lzas):
            ex_l[iatom],ec_l[iatom],_ = int_exc_unp(settings.X,settings.C,dens[atom],zps)

        bx,cx = get_lz_x(ex_l,lda_ref_x)
        bc = get_lz_c(ec_l)
        err_bx = 100.0*(bx - lzx1)/lzx1
        err_bc = 100.0*(bc - lzc1)/lzc1
        err_cx = 100.0*(cx - lzx2)/lzx2
        _,_,hexc = int_exc_one_elec(settings.X,settings.C,dens['H'],zps)
        herr = (hexc-atom_xc['H'])/atom_xc['H']*100.0
        absres = (nwgt['Bx']*err_bx**2 + nwgt['Cx']*err_cx**2 + nwgt['Bc']*err_bc**2 + nwgt['H']*herr**2)#**(0.5)
        resl.append([zps['c0'], zps['c1'], zps['c2'],zps['c3'],bx,err_bx,cx,err_cx,bc,err_bc,hexc,herr,absres])

def scorecard(pars,atom_dvar):
    """
        Gives an error scorecard for the X and C combo currently listed in the settings
    """

    if (settings.X in settings.fxcd['TMGGA']) or (settings.C in settings.fxcd['TMGGA']):
        base_dir = 'dfa_results/TMGGA/'
    elif (settings.X in settings.fxcd['LMGGA']) or (settings.C in settings.fxcd['LMGGA']):
        base_dir = 'dfa_results/LMGGA/'
    elif (settings.X[-2:]=='-L') or (settings.X[-2:]=='-L'):
        base_dir = 'dfa_results/LMGGA/'
    elif (settings.X in settings.fxcd['GGA']) or (settings.C in settings.fxcd['GGA']):
        base_dir = 'dfa_results/GGA/'
    else:
        base_dir = 'dfa_results/'

    if settings.X == settings.C:
        ofl = base_dir + settings.X
    else:
        ofl = base_dir+settings.X+'_X_'+settings.C+'_C'

    if settings.X in ['LLMG','GEA']:
        ofl += '_'+settings.GE

    deorb_regex = ['SCAN-L','rSCAN-L','r2SCAN-L']
    if settings.X in deorb_regex or settings.C in deorb_regex:
        ofl += '_'+settings.KEMOD
    elif (settings.X[-2:]=='-L') or (settings.X[-2:]=='-L'):
        ofl += '_'+settings.KEMOD

    ofl += '_scorecard.txt'

    if settings.routine['make_tex_table']:
        tofl = ofl[:-3]+'tex'
        tostr = 'Atomic Norm & Reference (hartree) & OFR2 (hartree) & Percent error \\\\ \\hline \n'

    hex,hec,hexc = int_exc_one_elec(settings.X,settings.C,atom_dvar['H'],pars)
    he_ex,he_ec,he_xc = int_exc_unp(settings.X,settings.C,atom_dvar['He'],pars)

    ostring = ('Error breakdown for {:} X with {:} C\n\n').format(settings.X,settings.C)
    if settings.X in settings.fxcd['LMGGA'] or settings.C in settings.fxcd['LMGGA']:
        ostring += 'LLMG parameters:'
        for apar in pars:
            ostring += '  {:} = {:},'.format(apar,pars[apar])
        ostring += '\n\n'
    r2l = ['r2SCAN','r2SCAN-L']
    if (settings.X in r2l or settings.C in r2l) and settings.routine['reopt_r2']:
        ostring += 'r2 SCAN parameters reoptimized; eta = c1, dp = c2\n\n'
    ostring += ('  H atom Exc={:.4f} Eh ({:.4f}% error); Ex = {:.4f} Eh, Ec = {:.4f} Eh\n').format(hexc,100*(hexc/atom_xc['H']-1.0),hex,hec)
    ostring += ('  He atom Exc={:.4f} Eh ({:.4f}% error); Ex = {:.4f} Eh, Ec = {:.4f} Eh ; not used as a norm\n').format(he_xc,100*(he_xc/atom_xc['He']-1.0),he_ex,he_ec)
    ostring += '\n------------------------------------------\n'
    ostring += 'Rare gas atoms ({:} dataset):'.format(settings.dataset)
    ostring += '\n------------------------------------------\n'
    lzas = ['Ne','Ar','Kr','Xe']
    ex_l = np.zeros(4)
    ec_l = np.zeros(4)
    exc_l = np.zeros(4)
    err_x = np.zeros(4)
    err_c = np.zeros(4)
    err_xc = np.zeros(4)
    for iat,atom in enumerate(lzas):
        ex_l[iat], ec_l[iat], exc_l[iat] = int_exc_unp(settings.X,settings.C,atom_dvar[atom],pars)
        err_x[iat] = 100*(ex_l[iat]/exx[atom] - 1)
        err_c[iat] = 100*(ec_l[iat]/ecc[atom] - 1)
        err_xc[iat] = 100*(exc_l[iat]/(ecc[atom]+exx[atom]) - 1)
        ostring += ('  {:}:\n      Ex={:.4f} Eh, {:.4f}% error\n').format(atom,ex_l[iat],err_x[iat])
        ostring += ('      Ec={:.4f} Eh, {:.4f}% error\n').format(ec_l[iat],err_c[iat])
        ostring += ('      Exc={:.4f} Eh, {:.4f}% error\n').format(ex_l[iat]+ec_l[iat],err_xc[iat])
        if settings.routine['make_tex_table']:
            tostr += '{:} & {:.3f} & {:.3f} & {:.2f}\\% \\\\ \n'.format(atom,ecc[atom]+exx[atom],exc_l[iat],err_xc[iat])
    saxcmape=np.sum(np.abs(err_xc))/len(err_xc)
    ostring += ('\n'+'XC MAPE {:.4f}%\n').format(saxcmape)
    if settings.routine['make_tex_table']:
        tostr += '& & MAPE & {:.2f}\\% \\\\ \\hline \n'.format(saxcmape)

    ostring += (' X MAPE {:.4f}%\n C MAPE {:.4f}%\n\n').format( \
        np.sum(np.abs(err_x))/len(err_x),np.sum(np.abs(err_c))/len(err_c))
    bx,cx = get_lz_x(ex_l,lda_ref_x)
    bc = get_lz_c(ec_l)
    ostring += ('    Bx={:.4f}, err={:.4f}%\n').format(bx,100*(bx-lzx1)/lzx1)
    ostring += ('    Cx={:.4f}, err={:.4f}%\n').format(cx,100*(cx-lzx2)/lzx2)
    ostring += ('    Bc={:.4f}, err={:.4f}%\n').format(bc,100*(bc-lzc1)/lzc1)
    ostring += ('    Bxc={:.4f}, err={:.4f}%\n').format(bx+bc,100*(bx+bc-lzxc1)/lzxc1)
    if settings.use_jellium_norms:
        wpars = []
        for azp in pars:
            wpars.append(pars[azp])
        sl,cd,sfl,cfl,lsda_ref,sxc_lsda,gxc_lsda = init_jellium_calc()
        surf_res,clus_res,gam_res,_ = get_jellium_residuals([wpars],1,sl,cd,sfl,cfl,lsda_ref,sxc_lsda,gxc_lsda,all_stats=True)
        surf_res = surf_res[0]
        clus_res = clus_res[0]
        gam_res = gam_res[0]
        ostring += '\n---------------------\n'+'Jellium Surface:\n'+'---------------------\n'
        if settings.routine['make_tex_table']:
            from jellium_norms import provide_references
            sigxcd = provide_references()
            tostr += 'Jellium surface $\\rs$ (bohr) & Reference (erg/cm$^{-2}$) & OFR2 (erg/cm$^{-2}$) & Percent error \\\\ \\hline \n'
        avg = 0.0
        avg_x = 0.0
        avg_c = 0.0
        for irs,rs in enumerate(settings.jellium_surface_rs):
            perr = surf_res[irs,2]
            avg += abs(perr)
            avg_x += abs(surf_res[irs,4])
            avg_c += abs(surf_res[irs,6])
            ostring += ('  rs={:}:\n      sigma_xc = {:.4f} erg/cm^2 ({:.4f}% error)\n').format(rs,surf_res[irs,1],perr)
            ostring += ('      sigma_x = {:.4f} erg/cm^2 ({:.4f}% error)\n').format(surf_res[irs,3],surf_res[irs,4])
            ostring += ('      sigma_c = {:.4f} erg/cm^2 ({:.4f}% error)\n').format(surf_res[irs,5],surf_res[irs,6])
            if settings.routine['make_tex_table']:
                tostr += '{:} & {:.0f} & {:.0f} & {:.2f}\\% \\\\ \n'.format(rs,sigxcd[rs],surf_res[irs,1],perr)

        xcmape = avg/(irs+1.0)
        ostring += ('\n  XC MAPE {:.4f}%\n  X MAPE {:.4f}%\n  C MAPE {:.4f}%\n').format(xcmape,avg_x/(irs+1.0),avg_c/(irs+1.0))
        if settings.routine['make_tex_table']:
            tostr += '& & MAPE & {:.2f}\\% \\\\ \\hline \n'.format(xcmape)

        ostring += '\n---------------------\n'+'Jellium cluster:\n'+'---------------------\n'
        if settings.routine['make_tex_table']:
            tostr += 'Jellium cluster $\\rs$ (bohr) & Reference (erg/cm$^{-2}$) & OFR2 (erg/cm$^{-2}$) & Percent error \\\\ \\hline \n'

        avg = 0.0
        for irs,rs in enumerate(settings.jellium_cluster_rs):
            perr = clus_res[irs,2]
            avg += abs(perr)
            ostring += ('  rs={:}:\n      sigma_xc = {:.4f} erg/cm^2 ({:.4f}% error)\n').format(rs,clus_res[irs,1],perr)
            if settings.routine['make_tex_table']:
                tostr += '{:} & {:.0f} & {:.0f} & {:.2f}\\% \\\\ \n'.format(rs,sigxcd[rs],clus_res[irs,1],perr)

        xcmape = avg/(irs+1.0)
        ostring += ('\n  XC MAPE {:.4f}%\n\n').format(xcmape)
        if settings.routine['make_tex_table']:
            tostr += '& & MAPE & {:.2f}\\% \\\\ '.format(xcmape)
            thefl = open(tofl,'w+')
            thefl.write(tostr)
            thefl.close()

        avg = 0.0

        wrs = []
        for rs in settings.jellium_cluster_rs:
            if rs in dmc_ref_delta_gamma_xc:
                wrs.append(rs)
        for irs,rs in enumerate(wrs):
            perr = gam_res[irs,2]
            avg += abs(perr)
            ostring += ('  rs={:}:\n      gamma = {:.4f} mhartree/bohr ({:.4f}% error)\n').format(rs,gam_res[irs,1],perr)
        ostring += ('\n  XC MAPE {:.4f}%\n').format(avg/(irs+1.0))
    ostring += '\n\n'+'NB: percent error defined as:\n 100% * (approx/reference - 1)'
    with open(ofl,'w+') as thefl:
        thefl.write(ostring)
    return


def filter_search():

    """
        Does a tiered search through filters of increasingly fine grid spacings. Ideal for large parameter sets
        Leverages multiprocessing
    """

    nconv = 0
    srch_rad = settings.search_radius

    atom_dvar = get_atomic_norm_densities()
    if settings.use_jellium_norms:
        sl,cd,sfl,cfl,lsda_ref,sxc_lsda,gxc_lsda = init_jellium_calc()

    stime = time()

    if settings.routine['fit'] == 'filter':
        incr = 0.8
        nstep = 50#ceil(np.log(0.001/settings.parbds[0][2])/np.log(incr))+1
        step_l = [settings.parbds[0][2]*incr**i for i in range(nstep)]
    elif settings.routine['fit'] == 'fixed_list':
        step_l = [1.0]

    ores = 1e20

    wrap_atom_errs = partial(get_atom_errs,dens=atom_dvar)
    if settings.use_jellium_norms:
        wrap_jerrs = partial(get_jerr_list,lsd_cluster=lsda_ref,lsd_surf=sxc_lsda,
        gxc_lsda=gxc_lsda,surface_list=sl,cluster_dict=cd,surf_data=sfl,clus_data=cfl,all_stats=False)

    for istep,step in enumerate(step_l):

        pd = {}
        if istep == 0:
            if settings.routine['fit'] == 'filter':
                def scalar_res(pars):
                    _,_,_,_,_,_,_,_,_,_,_,_,_,_,_,ares = wrap_atom_errs(pars)
                    _,_,_,jres = wrap_jerrs(pars)
                    return (ares + jres)**(0.5)
                #for ic in range(settings.npar):
                #    pd[ic] = np.arange(settings.parbds[ic][0],settings.parbds[ic][1],step)
                iset = [0.5*(settings.parbds[ic][0]+settings.parbds[ic][1]) for ic in range(settings.npar)]
                bdl = []
                for ic in range(settings.npar):
                    if settings.hard_bounds[ic][0]:
                        tbd0 = settings.parbds[ic][0]
                    else:
                        tbd0 = None#-10.0
                    if settings.hard_bounds[ic][1]:
                        tbd1 = settings.parbds[ic][1]
                    else:
                        tbd1 = None#10.0
                    bdl.append((tbd0,tbd1))
                nd_fit = minimize(scalar_res,iset,method='Nelder-Mead',bounds=bdl)
                cb = np.zeros(4)#nd_fit.x
                cb[:settings.npar] = nd_fit.x
                _,_,_,_,amapex,amapec,amapexc,bx,err_bx,cx,err_cx,bc,err_bc,hexc,herr,cres = wrap_atom_errs(cb)
                if settings.use_jellium_norms:
                    surf_res,clus_res,gam_res,jres =  wrap_jerrs(cb)
                    ores = (cres + jres)**(0.5)
                    prev_best = [amapex,amapec,amapexc,bx,err_bx,cx,err_cx,bc,err_bc,hexc,
                    herr,ores,surf_res,clus_res,gam_res]
                else:
                    ores = cres**(0.5)
                    prev_best = [amapex,amapec,amapexc,bx,err_bx,cx,err_cx,bc,
                        err_bc,hexc,herr,ores]
                print(('Stage {:} of {:} completed, best params: ' \
                    + '{:.6f}, '*(settings.npar-1) + '{:.6f}; ' \
                    + 'res = {:.6f}').format(istep,nstep,*cb[:settings.npar],ores))
                continue

            elif settings.routine['fit'] == 'fixed_list':
                for ic in range(settings.npar):
                    pd[ic] = np.arange(settings.parbds[ic][0],settings.parbds[ic][1],settings.parbds[ic][2])
        else:
            for ic in range(settings.npar):
                cm = [0.,0.]
                adj = 0.75**(istep+1)
                decr = 1 - adj
                incr = 1 + adj
                if cb[ic] > 0:
                    cm[0] = cb[ic]*decr
                    cm[1] = cb[ic]*incr
                elif cb[ic] == 0.0:
                    cm[0] = decr
                    cm[1] = incr
                else:
                    cm[0] = cb[ic]*incr
                    cm[1] = cb[ic]*decr
                if settings.hard_bounds[ic][0]:
                    cm[0] = max([settings.parbds[ic][0],cm[0]]) #- srch_rad*step_l[istep-1]])
                if settings.hard_bounds[ic][1]:
                    cm[1] = min([settings.parbds[ic][1],cm[1]]) #+ srch_rad*step_l[istep-1]])
                #pd[ic] = np.arange(cm[0],cm[1],step)
                pd[ic] = np.linspace(cm[0],cm[1],5)

        if settings.npar < 4:
            for ic in range(settings.npar,4):
                pd[ic] = np.zeros(1)

        tlen = pd[0].shape[0]*pd[1].shape[0]*pd[2].shape[0]*pd[3].shape[0]

        #if settings.ncore > 1 and tlen > 1:
        par_space = product(pd[0],pd[1],pd[2],pd[3])#,[atom_dvar])
        pool = mproc.Pool(processes=min(settings.ncore,tlen))
        res_l = pool.map(wrap_atom_errs,par_space)
        if settings.use_jellium_norms:
            olist = pool.map(wrap_jerrs,product(pd[0],pd[1],pd[2],pd[3]))
            pool.close()
            surf_res = np.zeros((tlen,len(settings.jellium_surface_rs),3))
            clus_res = np.zeros((tlen,len(settings.jellium_cluster_rs.keys()),3))
            gam_res = np.zeros((tlen,len(dmc_ref_delta_gamma_xc.keys()),3))
            jres = np.zeros(tlen)
            # can't transpose olist, as this makes a ragged numpy array
            for ilist,tlist in enumerate(olist):
                surf_res[ilist] = tlist[0]
                clus_res[ilist] = tlist[1]
                gam_res[ilist] = tlist[2]
                jres[ilist] = tlist[3]
        else:
            jres = 0.0
            pool.close()
            """
            with mproc.Manager() as manman:
                res_l = manman.list()
                dvars = manman.dict(atom_dvar)
                nrem = tlen%settings.ncore
                chunklen = (tlen - nrem)//settings.ncore
                chunk_inds = [[i*chunklen,(i+1)*chunklen] for i in range(settings.ncore)]
                chunk_inds[settings.ncore-1][1] = tlen
                obj = list(product(pd[0],pd[1],pd[2],pd[3]))
                for i in range(settings.ncore):
                    tmpinds = chunk_inds[i]
                    tmpproc = mproc.Process(target=get_atom_errs_list,args=(obj[tmpinds[0]:tmpinds[1]],dvars,res_l))
                    tmpproc.start()
                tmpproc.join()
                res_l = np.asarray(res_l)
            """

        #else:
        #    res_l = np.zeros((tlen,13))
        #    for ipar,pars in enumerate(product(pd[0],pd[1],pd[2],pd[3])):
        #        res_l[ipar] = get_atom_errs(*pars,atom_dvar)

        t0,t1,t2,t3,amapexl,amapecl,amapexcl,bx,err_bx,cx,err_cx,bc,err_bc,hexc,herr,absres = np.transpose(res_l)
        absres += jres

        """
        if settings.use_jellium_norms:
            par_space = product(pd[0],pd[1],pd[2],pd[3])
            surf_res,clus_res,gam_res,newres = get_jellium_residuals(par_space,tlen,sl,cd,sfl,cfl,lsda_ref,sxc_lsda,gxc_lsda)
            absres += newres
        """

        absres = absres**(0.5)
        if absres.min() > ores:
            iworst = np.argmax(absres)
            t0[iworst] = cb[0]
            t1[iworst] = cb[1]
            t2[iworst] = cb[2]
            t3[iworst] = cb[3]
            amapexl[iworst] = prev_best[0]
            amapecl[iworst] = prev_best[1]
            amapexcl[iworst] = prev_best[2]
            bx[iworst] = prev_best[3]
            err_bx[iworst] = prev_best[4]
            cx[iworst] = prev_best[5]
            err_cx[iworst] = prev_best[6]
            bc[iworst] = prev_best[7]
            err_bc[iworst] = prev_best[8]
            hexc[iworst] = prev_best[9]
            herr[iworst] = prev_best[10]
            absres[iworst] = prev_best[11]
            if settings.use_jellium_norms:
                surf_res[iworst] = prev_best[12]
                clus_res[iworst] = prev_best[13]
                gam_res[iworst] = prev_best[14]

        low_err = np.argmin(absres)
        cres = absres[low_err]
        cb = [t0[low_err],t1[low_err],t2[low_err],t3[low_err]]
        if settings.use_jellium_norms:
            prev_best = [amapexl[low_err],amapecl[low_err],amapexcl[low_err],
            bx[low_err], err_bx[low_err], cx[low_err],
            err_cx[low_err], bc[low_err], err_bc[low_err],
            hexc[low_err], herr[low_err], cres,surf_res[low_err],clus_res[low_err],
            gam_res[low_err]]
        else:
            prev_best = [amapexl[low_err],amapecl[low_err],amapexcl[low_err],
            bx[low_err], err_bx[low_err], cx[low_err],
            err_cx[low_err], bc[low_err], err_bc[low_err],
            hexc[low_err], herr[low_err], cres]

        if abs(ores - cres) < 1.e-6*ores and istep > 3:
            nconv += 1
        else:
            ores = min([cres,ores])
            nconv = min([nconv,0])

        if settings.routine['fit'] == 'filter':
            if (nconv >= nstep/3) or (nconv >= 3 and istep >= nstep/2):
                print('Stopping early, residuals have appeared to converge')
                print('Current {:.6f}; last {:.6f}'.format(cres,ores))
                break


        if settings.routine['fit'] == 'filter':
            if istep == 0:
                print(('Stage {:} of {:} completed, best params: ' \
                    + '{:.6f}, '*(settings.npar-1) + '{:.6f} ; res = {:.6f}'\
                    ).format(istep,nstep,*cb[:settings.npar],ores))
            else:
                print(('Stage {:} best params: ' + '{:.6f}, '*(settings.npar-1) \
                    + '{:.6f} ; res = {:.6f}').format(istep,*cb[:settings.npar],ores))

    #print(hpy().heap())
    nbest = min([40,len(absres)-1])
    ind_l = np.argpartition(absres,nbest)[:nbest]
    bpar = []
    for ind in ind_l:
        tolist = [t0[ind],t1[ind],t2[ind],t3[ind],absres[ind],
        amapexl[ind],amapecl[ind],amapexcl[ind],
        bx[ind],err_bx[ind],cx[ind],err_cx[ind],bc[ind],
        err_bc[ind],hexc[ind],herr[ind]]

        if settings.use_jellium_norms:
            avg = 0.0
            nsurf =len(settings.jellium_surface_rs)
            for irs in range(nsurf):
                perr = surf_res[ind,irs,2]
                avg += abs(perr)
                tolist.append(surf_res[ind,irs,1])
                tolist.append(perr)
            tolist.append(avg/nsurf)

            nclus = len(settings.jellium_cluster_rs.keys())
            avg = 0.0
            for irs in range(nclus):
                perr = clus_res[ind,irs,2]
                avg += abs(perr)
                tolist.append(clus_res[ind,irs,1])
                tolist.append(perr)
            tolist.append(avg/nclus)

            ngam = len(dmc_ref_delta_gamma_xc.keys())
            avg = 0.0
            for irs in range(ngam):
                perr = gam_res[ind,irs,2]
                avg += abs(perr)
                tolist.append(gam_res[ind,irs,1])
                tolist.append(perr)
            tolist.append(avg/ngam)

        bpar.append(tolist)

    bpar = np.asarray(bpar)
    ind_l = np.argsort(bpar[:,4])
    bpar = bpar[ind_l]

    if settings.use_jellium_norms:
        hstring = 'c0,c1,c2,c3,sigma, SA MAPE X, SA MAPE C, SA MAPE XC,Bx,Bx err (%), Cx, Cx err (%),Bc, Bc error (%), H Exc,H Exc err (%),'
        for rs in settings.jellium_surface_rs:
            hstring += 'J.S. sigxc rs={:}, % err,'.format(rs)
        hstring += 'J.S. MAPE,'
        for rs in settings.jellium_cluster_rs:
            hstring += 'J.C. sigxc rs={:}, % err,'.format(rs)
        hstring += 'J.C. MAPE,'
        for rs in dmc_ref_delta_gamma_xc:
            hstring += 'gamma_xc rs={:}, % err,'.format(rs)
        hstring += 'gamma_xc MAPE \n'

        poststr = '_'+settings.routine['fit']+'_all_norms.csv'
        if settings.X == 'LLMG' and settings.C == 'LLMG':
            fname = 'llmgga_xc_'+(settings.GE).lower()+poststr
        else:
            if settings.X == settings.C:
                fname = settings.X+'_xc_'+(settings.KEMOD).lower()+poststr
            else:
                fname = settings.X+'_x_'+settings.C+'_c_'+(settings.KEMOD).lower()+poststr
    else:
        poststr = '_'+settings.routine['fit']+'_atom_norms.csv'
        hstring = 'c0,c1,c2,c3,sigma,SA MAPE X, SA MAPE C, SA MAPE XC,Bx,Bx err (%), Cx, Cx err (%),Bc, Bc error (%), H Exc,H Exc err (%)\n'
        if settings.X == 'LLMG' and settings.C == 'LLMG':
            fname = 'llmgga_xc_'+(settings.GE).lower()+poststr
        else:
            if settings.X == settings.C:
                fname = settings.X+'_xc_'+(settings.KEMOD).lower()+poststr
            else:
                fname = settings.X+'_x_'+settings.C+'_c_'+(settings.KEMOD).lower()+poststr


    with open('dfa_results/fits/'+fname,'w+') as outf:
        outf.write(hstring)
        for abpar in bpar:
            outf.write(('{:},'*4 +'{:},'*(len(abpar)-5)+'{:} \n').format(*abpar))
        outf.write('\n norm weights \n')
        tstr1 = ''
        tstr2 = ''
        for asys in settings.norm_wgts:
            tstr1 += asys+', '
            tstr2 += '{:}, '.format(settings.norm_wgts[asys])
        outf.write(tstr1+'\n'+tstr2+'\n')

    rtime = time() - stime
    if rtime > 60:
        rem = rtime%60
        print(('Runtime {:.1f} minutes, {:.4f} seconds').format((rtime-rem)/60.0,rem))
    else:
        print(('Runtime {:.4f} seconds').format(rtime))

    bps = bpar[0,:4]
    fps = {}
    coeffs = ['c0','c1','c2','c3']
    for ip,ap in enumerate(coeffs):
        fps[ap] = round(bps[ip],6)
    scorecard(fps,atom_dvar)

    return

def wrap_jnorms(c0,sl,cd,sfl,cfl,lsda_ref,sxc_lsda,gxc_lsda):
    surf_res,clus_res,gam_res,_ = get_jellium_residuals([[c0, 0.0,0.0,0.0]],1,sl,cd,sfl,cfl,lsda_ref,sxc_lsda,gxc_lsda,all_stats=True)
    avg_x = 0.0
    avg_c = 0.0
    for irs,rs in enumerate(settings.jellium_surface_rs):
        avg_x += abs(surf_res[0,irs,4])/len(settings.jellium_surface_rs)
        avg_c += abs(surf_res[0,irs,6])/len(settings.jellium_surface_rs)
    return c0, avg_x, avg_c

def r2scan_of_jnorms():

    c0l = np.arange(0.001,5.001,0.001)
    sl,cd,sfl,cfl,lsda_ref,sxc_lsda,gxc_lsda = init_jellium_calc()

    pool = mproc.Pool(processes=settings.ncore)
    par_space = product(c0l,[sl],[cd],[sfl],[cfl],[lsda_ref],[sxc_lsda],[gxc_lsda])
    res_l = pool.starmap(wrap_jnorms,par_space)
    pool.close()

    tmpfl = open('./dfa_results/fits/r2scan_of_fit.csv','w+')
    tmpfl.write('c0, X MAPE, C MAPE\n')
    for ic0,c0 in enumerate(c0l):
        tmpfl.write(('{:}, {:}, {:}\n').format(*res_l[ic0]))
    tmpfl.close()

    return

def tau_ingredients(d,gd,ld,tau):
    kf = (3*pi**2*d)**(1/3)
    t0 = 0.3*kf**2*d
    p = (gd/(2*kf*d))**2
    q = ld/(4*kf**2*d)
    fw = 5*p/3
    f2 = 5*p/27 + 20*q/9
    f4 = 8/81*q**2 - p*q/9 + 8/243*p**2
    #fge = (1 + f2 + f4)/(1 + (f4/(1 + fw))**2)**(0.5)
    fge = 1 + f2
    z = fge - fw
    theta = (tau/t0 - fw)/z
    theta[theta < 0.0] = 0.0
    ft = tau/t0
    return p, q, z, theta, ft


def fit_tau():

    nz = 100
    maxz = 10.0
    zbin = np.linspace(0.0,maxz,nz)
    thbin = np.zeros(nz)
    thcut = 2.0
    ncount = np.zeros(nz,dtype=int)

    atoms = ['Ne', 'Ar', 'Kr', 'Xe']
    atdv = get_atomic_norm_densities()
    import h5py
    import matplotlib.pyplot as plt

    if not path.isdir('./ked_data/'):
        system('mkdir ./ked_data/')
    ofl = h5py.File('./ked_data/all_norm_ked.hdf5','w')

    fdat = []
    for atom in atdv:
        _,nu,gnu,tu,lnu = np.transpose(atdv[atom])
        p, q, z, theta, ft = tau_ingredients(2*nu,2*gnu,2*lnu,2*tu)
        for ipq in range(p.shape[0]):
            fdat.append([p[ipq],q[ipq],ft[ipq]])
        for iaz,az in enumerate(z):
            if az > maxz:
                continue
            for jz in range(nz-1):
                if zbin[jz] <= az and az < zbin[jz+1]:
                    thbin[jz] += theta[iaz]
                    ncount[jz] += 1
                    break

        ofl.create_dataset(atom,data=np.transpose((z,theta)))

        #plt.scatter(z,theta,label=atom)
    #plt.legend()
    #plt.xlim([0,10])
    #plt.ylim([0,5])
    #plt.show()

    #plt.cla()
    #plt.clf()

    if settings.use_jellium_norms:
        _,_,sfl,cfl,_,_,_ = init_jellium_calc()
        for rs in settings.jellium_surface_rs:
            _,_,n,gn,ln,tau = np.transpose(sfl[rs])
            p, q, z, theta, ft = tau_ingredients(n,gn,ln,tau)
            for ipq in range(p.shape[0]):
                fdat.append([p[ipq],q[ipq],ft[ipq]])
            ofl.create_dataset('JS '+str(rs),data=np.transpose((z,theta)))
            for iaz,az in enumerate(z):
                if az > maxz:
                    continue
                for jz in range(nz-1):
                    if zbin[jz] <= az and az < zbin[jz+1]:
                        thbin[jz] += theta[iaz]
                        ncount[jz] += 1
                        break
            #plt.scatter(z,theta,label=str(rs))
        mask = ncount>0
        zbin = zbin[mask]
        thbin = thbin[mask]/ncount[mask]

        mask = thbin < thcut
        zbin = zbin[mask]
        thbin = thbin[mask]
        #plt.plot(zbin,thbin)
        #plt.legend()
        #plt.xlim([0,10])
        #plt.ylim([0,5])
        #plt.show()
        ofl.create_dataset('BINNED',data=np.transpose((zbin,thbin)))
        ofl.create_dataset('RAW',data=np.asarray(fdat))

    return


def run_main():

    directory_l = ['dfa_results/fits']
    for adir in directory_l:
        if not path.isdir(adir):
            system(('mkdir -p {:}').format(adir))
    for fnltype in settings.fxcd:
        system(('mkdir -p dfa_results/{:}').format(fnltype))

    lzas = ['Ne','Ar','Kr','Xe']

    zps = settings.fit_pars

    if settings.routine['main'] == 'regen_atomic_dens':

        from atomic_densities.orbs import write_dens
        fls = lzas
        fls.append('H')
        fls.append('He')
        #for at in ['Li','N','Na','K','Cr','Cu','As']:
        #    fls.append(at)
        for lrec in [False,True]:
            write_dens(fls,settings.dataset,rect_grid=lrec)

    elif settings.routine['main'] == 'fxc':

        from dft.fxc import plot_2d as fxc_plotter

        inps = settings.fxc_plot_pars
        fxc_plotter(inps['rs'],inps['zeta'],inps['wfxc'],inps['aux'])

    elif settings.routine['main'] == 'calc_norms':

        if settings.routine['fit'] == 'lsda_reference':

            atom_dvar = get_atomic_norm_densities()
            lda_ex_l = np.zeros(4)
            for iat,atom in enumerate(lzas):
                lda_ex_l[iat],_,_ = int_exc_unp('LSDA','LSDA',atom_dvar[atom],zps)
            print('LSDA reference X energies:')
            ostr = '['
            for tmp in lda_ex_l:
                ostr += '{:}, '.format(tmp)
            ostr = ostr[:-2]+']'
            print(ostr)
            ex_l = []
            ec_l = []
            for iat,atom in enumerate(lzas):
                ex_l.append(exx[atom])
                ec_l.append(ecc[atom])
            bx,cx = get_lz_x(ex_l,lda_ex_l)
            bc = get_lz_c(ec_l)
            print('Bx=',bx)
            print('Cx=',cx)
            print('Bc=',bc)

        elif settings.routine['fit'] == 'report':
            scorecard(zps,get_atomic_norm_densities())

        elif settings.routine['fit'] == 'jnorms':
            r2scan_of_jnorms()

        elif settings.routine['fit'] == 'Ax':

            from atomic_densities.tf_den import get_lda_tf

            z_l,ex_l,ec_l = get_lda_tf()
            bx,ax,_ =lst_sq_lin(z_l**(2.0/3.0),ex_l/z_l)
            bc,ac,_ = lst_sq_lin(np.log(z_l),ec_l/z_l)
            print(('Ax = {:}, Bx = {:}').format(-ax,bx))
            print(('Ac = {:}, Bc = {:}').format(-ac,bc))

        elif settings.routine['fit'] == 'filter' or settings.routine['fit'] == 'fixed_list':
            filter_search()
        elif settings.routine['fit'] == 'tau_fit':
            fit_tau()
        else:
            raise ValueError('Unkown fitting option ',settings.routine['fit'])

    elif settings.routine['main'] == 'plot_js_ke':


        import matplotlib.pyplot as plt
        from dft.tau import lap_lev_ft

        if not path.isdir('./jell_surf_figs/'):
            system('mkdir -p ./jell_surf_figs/')

        for rs0 in [2,3,3.25,4,5]:
            prefix = 'rs_'+str(rs0)
            tfile = './jellium_scf/data/surface/'+prefix+'/'+prefix+'_density.csv'
            x,n,gn,ln,tau = np.transpose(np.genfromtxt(tfile,delimiter=',',skip_header=1))

            kf0 = (9*pi/4)**(1/3)/rs0
            lf0 = 2*pi/kf0
            n_bulk = 3/(4*pi*rs0**3)
            tau0_bulk = 0.3*kf0**2*n_bulk

            kf = (3*pi**2*n)**(1/3)
            tau0 = 0.3*kf**2*n
            p = (gn/(2*kf*n))**2
            q = ln/(4*kf**2*n)

            xd = x/lf0
            taud = tau/tau0_bulk
            tau0d = tau0/tau0_bulk

            tauw = 5*p/3*tau0d
            tau_ge2 = lap_lev_ft('GEA2',p,q,{})*tau0d
            tau_ofr2 = lap_lev_ft('RPP1',p,q,zps)*tau0d
            yubd = 1.05*max([taud.max(),tauw.max(),tau_ge2.max(),tau_ofr2.max()])

            fig,ax = plt.subplots(figsize=(5,4))

            ax.plot(xd,taud,linewidth=2,color='gray',label='Exact')
            ax.plot(xd,tauw,linewidth=2,color='darkorange',linestyle='-.',label='Weizs$\\mathrm{\\"a}$cker')
            ax.plot(xd,tau_ge2,linewidth=2,color='tab:green',label='GEA2')
            ax.plot(xd,tau_ofr2,linewidth=2,color='darkblue',linestyle='--',label='RPP')
            #ax.plot(xd,(1 + ft2 + ft4)*tau0/tau0_bulk,linewidth=2,color='darkred',label='GEA4')
            #ax.set_ylim([0,3])
            ax.legend(title='$\\overline{r_\\mathrm{s}}='+str(rs0)+'$',
                fontsize=14,title_fontsize=16)
            ax.set_xlabel('$x/\\overline{\\lambda_{\\mathrm{F}}}$',fontsize=16)
            ax.set_ylabel('$\\tau/\\overline{\\tau_\\mathrm{UEG}}$',fontsize=16)
            ax.set_xlim(xd[0],xd[-1])
            ax.set_ylim(0.0,yubd)
            ax.tick_params(axis='both',labelsize=14)
            #ax.annotate('$r_\\mathrm{s}^\\mathrm{bulk}='+str(rs0)+'$',(0.1*xd[-1],0.9*yubd),fontsize=20)
            #plt.title('$r_\\mathrm{s}='+str(rs0)+'$ jellium surface KED',fontsize=14)
            #plt.show() ; exit()
            plt.savefig('./jell_surf_figs/rs_{:}_js_ked.pdf'.format(rs0),dpi=600,bbox_inches='tight')
        exit()

    elif settings.routine['main'] == 'plot_alphas':

        import matplotlib.pyplot as plt
        from dft.tau import pc_interpolation, theta_model

        if not path.isdir('./figs/'):
            system('mkdir -p ./figs/')

        def alpha_rpp(x,x0):
            #x0 = 0.819411#0.5583
            alps = np.zeros(x.shape)
            ca =  20/x0**3
            cb =  -45/x0**4
            cc = 36./x0**5
            cd = -10./x0**6

            mask = (0 <= x) & (x <= x0)
            xm = x[mask]

            alps[mask] = xm**4*(ca + xm*(cb + xm*(cc + xm*cd)))

            mask = x>x0
            alps[mask] = x[mask]
            return alps

        def alpha_cr(z):
            apar = 4
            iz = np.ones_like(z)
            zmk = z < 0
            iz[zmk] = (1 - np.exp(-1/np.abs(z[zmk])**apar) )**(1/apar)
            return 1 + z*iz

        pard = {
            # these use GE2 ingredients
            3 : [-2.4153571096515076, 1.9965618896773247, 0.24130250873900594],
            4 : [-5.625916373547513, 10.802436076371524, -7.770489376090032, \
                3.832601020621118],
            6 : [0.41734330440209316, -26.865089294441745, 75.20427919702495, \
                -83.96867704976964, 52.17930398327887, 0.6775117229296242],
            11 : [-1.2671660247767116, -11.922250743093565, 22.386020165089914, \
                10.69242899231974, -35.98119268664415, 34.89146499613219, \
                1.0511772339901142, -4.812118332316358, 1.3178981129806067, \
                -0.13854891212453904, 0.005117342685216863]
        }


        fig,ax = plt.subplots(figsize=(6,4.5))
        x = np.linspace(-0.5,2,5000)
        x0 = zps['c0']
        ax.plot(x,alpha_rpp(x,x0),linewidth=2,color='darkblue',label='RPP')
        ax.plot(x,alpha_cr(x-1),linewidth=2,color='darkorange',linestyle='--',label='CR')
        ax.plot(x,x*pc_interpolation(x,0.5389,3.0),color='darkgreen',linestyle=':',linewidth=2,label='PC')
        ax.plot(x,x*pc_interpolation(x,1.784720,0.258304),color='darkred',linestyle='-.',linewidth=2,label='MRT')
        #ax.plot(x,x*theta_model(x,pard[6]),color='black',linestyle='-.',linewidth=2,label='HL6')

        ax.set_xlim(x[0],x[-1])
        ax.set_ylim(-1.e-2,x[-1])
        ax.set_xlabel('$x$',fontsize=16)
        ax.set_ylabel('$\\widetilde{\\alpha}(x)$',fontsize=16)
        ax.vlines(x0,ax.get_ylim()[0],ax.get_ylim()[1],color='gray',linewidth=1,linestyle='--')
        ax.set_xticks([-0.5,0.0,x0,1,1.5,2.0])
        ax.set_xticklabels(['-0.5','0.0','$x_0$','1.0','1.5','2.0'])
        ax.tick_params(axis='both',labelsize=14)
        ax.legend(fontsize=14)
        #plt.show() ;exit()
        plt.savefig('./figs/alpha_s_plot.pdf',dpi=600,bbox_inches='tight')

    elif settings.routine['main'] == 'plot_fs':

        import matplotlib.pyplot as plt
        from dft.tau import ft_rpp

        if not path.isdir('./figs/'):
            system('mkdir -p ./figs/')

        fig,ax = plt.subplots(figsize=(5,4))
        p = np.linspace(0.0,5,5000)
        colors = ['darkblue','darkorange','darkgreen','darkred','brown']
        linestyles = ['-','--','-.',':','-']

        fw = 5*p/3
        ax.plot(p,fw,linewidth=2,color='black',label='$F_\\mathrm{W}$')
        ax.annotate('$F_\\mathrm{W}$',(p[-1]/2,5*p[-1]/7),fontsize=14)

        q_list = [-0.25,0,1,3]
        for iq,q in enumerate(q_list):
            fs = ft_rpp(p,q,1,zps)
            tp = p[fs>fw]
            tfs = fs[fs>fw]
            nmid = (len(tp)-len(tp)%2)//2
            ax.plot(tp,tfs,linewidth=2,color=colors[iq],linestyle=linestyles[iq],label='q={:}'.format(q))
            if nmid > 2:
                if q < 0 or q > 1:
                    ax.annotate('q={:}'.format(q),(1.15*tp[nmid],-.5+tfs[nmid]),fontsize=14,color=colors[iq])
                else:
                    ax.annotate('q={:}'.format(q),(0.9*tp[nmid],.3+tfs[nmid]),fontsize=14,color=colors[iq])

        ax.set_xlim(p[0],p[-1])
        #ax.set_ylim(0,5/3*p[-1])
        ax.set_xlabel('$p$',fontsize=16)
        ax.set_ylabel('$F_\\mathrm{s}(p,q)$',fontsize=16)
        ax.tick_params(axis='both',labelsize=14)
        #ax.legend(fontsize=14)
        #plt.show() ; exit()
        plt.savefig('./figs/f_s_plot.pdf',dpi=600,bbox_inches='tight')

    elif settings.routine['main'] == 'testing':

        import matplotlib.pyplot as plt
        from matplotlib.animation import FuncAnimation
        from dft.tau import lap_lev_ft

        p_l = np.linspace(0,10,500)
        q_l = [-10,-5,-1,0,1,5,10]
        fig,ax = plt.subplots(figsize=(8,6))

        tmp1, = plt.plot([], [], 'darkblue',linewidth=2,label='GEA4')
        tmp2, = plt.plot([], [], 'darkorange',linewidth=2,label='MRT')
        tmp3, = plt.plot([], [], 'darkgreen',linewidth=2,label='New')

        def anim_update_1(q):
            tmp1.set_data(p_l,lap_lev_ft('GEA4',p_l,q,{}))
            tmp2.set_data(p_l,lap_lev_ft('MRT',p_l,q,{}))
            tmp3.set_data(p_l,lap_lev_ft(settings.KEMOD,p_l,q,zps))
            plt.title('$q={:.4f}$'.format(q),fontsize=16)
            return tmp1,tmp2,tmp3
            #plt.plot(p_l,ft_gea4(p_l,q),label='$F_s^{\\mathrm{GEA4}}$',color='darkblue',linewidth=2)
            #plt.plot(p_l,pc_ft(p_l,q,1.784720,0.258304),label='$F_s^{\\mathrm{MRT}}$',color='darkorange',linewidth=2)
            #plt.plot(p_l,ke_non_emp_ft(p_l,q,zps),label='$F_s^{\\mathrm{new}}$',color='darkgreen',linewidth=2)
            #return tmp,

        ax.plot(p_l,5*p_l/3,label='Weiz.',color='black',linewidth=2)

        frml = np.linspace(-10, 10, 210)
        ax.set_xlim([p_l[0],p_l[-1]])
        ax.set_ylim([0.0,5*p_l[-1]/3])
        ax.set_xlabel('$p$',fontsize=14)
        ax.set_ylabel('$F_s(p,q)$',fontsize=14)
        ax.tick_params(axis='both',labelsize=14)

        ax.legend(loc='lower center',fontsize=14)
        anim = FuncAnimation(fig, anim_update_1, frames=frml,blit=True)

        anim.save('./fs_comp.gif',writer='Pillow',fps=30)
        #plt.show()

        exit()
        def fscan(a,c1,c2,d):
            ief = np.zeros(a.shape)
            oma = 1.0 - a
            ief[a < 1.0] = np.exp(-c1*a[a < 1.0]/oma[a < 1.0])
            ief[a > 1.0] = -d*np.exp(c2/oma[a > 1.0])
            return ief
        def fsmooth(a,c):
            ief = (1-a)**3*(1 + a*(c[0] + a*(c[1] + c[2]*a)) )/(1 + c[3]*a**2 + c[2]/c[4]*a**6)
            return ief
        at = np.linspace(0,10,10000)
        def resx(c):
            return (fscan(at,0.667,.8,1.24) - fsmooth(at,[c[0],c[1],c[2],c[3],1.24]))**2
        def resc(c):
            return (fscan(at,0.64,1.5,0.7) - fsmooth(at,[c[0],c[1],c[2],c[3],0.7]))**2
        from scipy.optimize import least_squares
        resyx = least_squares(resx,[3,2,1,1])
        print(resyx)
        resyc = least_squares(resc,[3,2,1,1])
        print(resyc)
        return

    elif settings.routine['main'] == 'xcpot':
        pot_calc(settings.routine['vxc'],zps)

    else:
        raise ValueError('Unknown routine, ', settings.routine['main'])

if __name__=='__main__':

    run_main()
