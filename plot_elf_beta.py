import numpy as np
import matplotlib.pyplot as plt
from os import system,path
from scipy.interpolate import splrep,splev
from scipy.optimize import bisect

from dft.tau import ft_hl,ft_rpp,ft_pc

if not path.isdir('./iso_orb_figs/rg_atoms'):
    system('mkdir -p ./iso_orb_figs/rg_atoms')

if not path.isdir('./iso_orb_figs/sph_atoms'):
    system('mkdir -p ./iso_orb_figs/sph_atoms')

if not path.isdir('./iso_orb_figs/js'):
    system('mkdir -p ./iso_orb_figs/js')

do_rg_at = False
do_sph_at = False
do_js = False

if do_rg_at:
    for atom in ['Ne','Kr','Ar','Xe']:
        atdens = np.genfromtxt('./data_files/'+atom+'_K99_rect.csv',\
            delimiter=',',dtype=None,skip_header=1)
        r = atdens[:,0]
        n = atdens[:,2] + atdens[:,3]
        nelec_f = 4*np.pi*(r[1]-r[0])*np.sum(r**2*n)

        gn = atdens[:,6]
        tau = atdens[:,7] +  atdens[:,8]

        tauw = gn**2/(8*n)
        tau0 = 3/10*(3*np.pi**2*n)**(2/3)*n

        tmtw = tau - tauw
        tmtw[tmtw<0.0] = 0.0

        alpha = tmtw/tau0
        elf = 1/(1 + alpha**2)

        beta = tmtw/(tau + tau0)

        fig,ax = plt.subplots(figsize=(6,4))
        ax.plot(r,elf,color='darkblue')
        ax.plot(r,1-beta,color='darkorange',linestyle='--')

        ax.set_ylim([-.01,1.01])
        ax.set_xlim([1.e-2,r.max()])
        ax.set_xscale('log')

        ax.set_xlabel('$r$ (bohr)',fontsize=12)
        plt.title('ELF (blue) and $1-\\beta$ (orange); {:} atom'.format(atom),fontsize=12)

        plt.savefig('./iso_orb_figs/rg_atoms/{:}_atom.pdf'.format(atom),dpi=600,bbox_inches='tight')

        plt.cla()
        plt.clf()
        plt.close()

if do_sph_at:
    for atom in ['Li','N','Na','K','Cr','Cu','As']:
        atdens = np.genfromtxt('./data_files/'+atom+'_K99_rect.csv',\
            delimiter=',',dtype=None,skip_header=1)
        r = atdens[:,0]
        nelec_f = 4*np.pi*(r[1]-r[0])*np.sum(r**2*(atdens[:,2]+atdens[:,3]))

        fig,ax = plt.subplots(2,1,figsize=(6,6))

        for i in range(2):
            n = atdens[:,2+i]

            gn = atdens[:,4+i]
            tau = atdens[:,7+i]

            tauw = gn**2/(8*n)
            tau0 = 3/10*(3*np.pi**2*n)**(2/3)*n

            tmtw = tau - tauw
            tmtw[tmtw<0.0] = 0.0

            alpha = tmtw/tau0
            elf = 1/(1 + alpha**2)

            beta = tmtw/(tau + tau0)

            #elf2 = (1 - beta)**2/((1 - beta)**2 + beta**2)
            if i == 0:
                modstr = '\\uparrow'
            elif i == 1:
                modstr = '\\downarrow'

            ax[i].plot(r,elf,color='darkblue',linestyle='-',\
                label='ELF$_{:}$'.format(modstr))
            ax[i].plot(r,1-beta,color='darkorange',linestyle='--',\
                label='$(1-\\beta_{:})$'.format(modstr))
            #ax[i].plot(r,elf2,color='darkgreen',linestyle='-.',\
            #    label='$(1-\\beta_{:})$'.format(modstr))
            ax[i].set_ylabel('${:}$-spin'.format(modstr),fontsize=12)

            ax[i].set_ylim([-.01,1.01])
            ax[i].set_xlim([1.e-2,r.max()])
            ax[i].set_xscale('log')

        #ax.legend(fontsize=12,loc='lower left')
        ax[1].set_xlabel('$r$ (bohr)',fontsize=12)
        plt.suptitle('ELF (blue) and $1-\\beta$ (orange); {:} atom'.format(atom),fontsize=12)

        #plt.show() ; exit()
        plt.savefig('./iso_orb_figs/sph_atoms/{:}_atom.pdf'.format(atom),dpi=600,bbox_inches='tight')

        plt.cla()
        plt.clf()
        plt.close()


if do_js:
    for rs0 in [2,3,4,5]:
        prefix = 'rs_'+str(rs0)
        tfile = './jellium_scf/data/surface/'+prefix+'/'+prefix+'_density.csv'
        z,n,gn,lap,tau = np.transpose(np.genfromtxt(tfile,delimiter=',',skip_header=1))

        tauw = gn**2/(8*n)
        tau0 = 3/10*(3*np.pi**2*n)**(2/3)*n

        tmtw = tau - tauw
        tmtw[tmtw<0.0] = 0.0

        alpha = tmtw/tau0
        elf = 1/(1 + alpha**2)

        beta = tmtw/(tau + tau0)

        fig,ax = plt.subplots(figsize=(6,4))

        kf0 = (9*np.pi/4)**(1/3)/rs0
        lf0 = 2*np.pi/kf0

        ax.plot(z/lf0,elf,color='darkblue')
        ax.plot(z/lf0,1-beta,color='darkorange',linestyle='--')

        ax.set_ylim([0,1])
        ax.set_xlim([-4.5,2])

        ax.set_xlabel('$z/\\overline{\\lambda}_\\mathrm{F}$',fontsize=12)
        plt.title('ELF (blue) and $1-\\beta$ (orange); $r_\\mathrm{s}'+'={:}$ jellium surface'.format(rs0),fontsize=12)

        #plt.show() ; exit()
        plt.savefig('./iso_orb_figs/js/rs_{:}_js.pdf'.format(rs0),dpi=600,bbox_inches='tight')

        plt.cla()
        plt.clf()
        plt.close()

if __name__ == "__main__":

    atom = 'Cr'
    rpp_pars = {'c0': 0.819411, 'c1': 0.201352, 'c2': 0.185020 , 'c3': 1.53804}
    mrt_pars = [1.784720,0.258304]

    atdens = np.genfromtxt('./data_files/'+atom+'_K99_rect.csv',\
        delimiter=',',dtype=None,skip_header=1)
    r = atdens[:,0]

    """
        First spin-resolved
    """
    fig,ax = plt.subplots(2,2,figsize=(8,6))

    for isp in range(2):
        n = 2*atdens[:,2+isp]

        gn = 2*atdens[:,4+isp]
        tau = 2*atdens[:,7+isp]
        lap = 2*atdens[:,9+isp]

        tauw = gn**2/(8*n)
        tau0 = 3/10*(3*np.pi**2*n)**(2/3)*n

        kf = (3*np.pi**2*n)**(1/3)
        p = (gn/(2*kf*n))**2
        q = lap/(4*kf**2*n)

        tmtw = tau - tauw
        tmtw[tmtw<0.0] = 0.0

        alpha = tmtw/tau0
        osign = np.sign(alpha[0]-1)
        pot_cross = []
        for ia,aa in enumerate(alpha):
            csign = np.sign(alpha[ia]-1)
            if csign*osign < 0.0:
                ind0 = max(ia-1,0)
                ind1 = min(ia+1,alpha.shape[0]-1)
                pot_cross.append((r[ind0],r[ind1]))
            osign = csign

        alp_spl_par = splrep(r, alpha-1, s=0)
        ainterp = lambda x : splev(x, alp_spl_par, der=0)

        amo_cross = []
        for ia in range(len(pot_cross)):

            minpars = bisect(ainterp,pot_cross[ia][0],pot_cross[ia][1],\
                full_output=True,maxiter=1000)

            if minpars[1].converged:
                amo_cross.append(minpars[0])

        alpha_ge2 = 1 + 20/9*q - 40*p/27
        alpha_rpp = ft_rpp(p,q,1,rpp_pars) - 5*p/3
        alpha_mrt = ft_pc(p,q,mrt_pars[0],mrt_pars[1]) - 5*p/3

        if isp == 0:
            modstr = '\\uparrow'
        elif isp == 1:
            modstr = '\\downarrow'

        ax[0,isp].plot(r,p,color='darkblue')
        ax[0,isp].annotate('$p_{:}$'.format(modstr),(.014,.6),color='darkblue',fontsize=14)

        ax[0,isp].plot(r,q,color='darkorange',linestyle='--')
        ax[0,isp].annotate('$q_{:}$'.format(modstr),(1.04,-.39),color='darkorange',fontsize=14)

        ax[0,isp].plot(r,alpha,color='darkgreen',linestyle='-.')
        ax[0,isp].annotate('$\\alpha_{:}$'.format(modstr),(.088,1.74),color='darkgreen',fontsize=14)

        ax[1,isp].plot(r,100*(alpha_mrt/alpha - 1),color='darkblue',linestyle='-')
        #ax[1,isp].plot(r,alpha,color='black',linestyle='-')
        #ax[1,isp].plot(r,alpha_mrt,color='darkblue',linestyle='--')
        ax[1,isp].annotate('MRT',(5.6,-69),color='darkblue',fontsize=14)

        ax[1,isp].plot(r,100*(alpha_rpp/alpha - 1),color='darkorange',linestyle='--')
        #ax[1,isp].plot(r,alpha_rpp,color='darkorange',linestyle='-.')
        ax[1,isp].annotate('RPP',(.066,87.5),color='darkorange',fontsize=14)

        ax[1,isp].plot(r,100*(alpha_ge2/alpha - 1),color='darkgreen',linestyle='-.')

        ax[1,isp].annotate('GE2',(.02,-56),xytext=(.05,-75),\
            color='darkgreen',fontsize=14, arrowprops={ 'lw':1, 'arrowstyle':'->',\
            'color': 'darkgreen'})

        iloc = np.argmin(np.abs(r-1.e-2))
        ax[0,isp].set_ylim([q[iloc],5.0])
        ax[1,isp].set_ylim([-100,150])
        for i in range(2):
            ax[i,isp].set_xlim([1.e-2,r.max()])
            ax[i,isp].set_xscale('log')
            ax[i,isp].hlines(0,*ax[i,isp].get_xlim(),color='black',linestyle='-',linewidth=1)

        ax[0,isp].hlines(1,*ax[0,isp].get_xlim(),color='gray',linestyle=':')

        """
        for ia in range(len(amo_cross)):
            for i in range(2):
                ax[i,isp].vlines(amo_cross[ia],*ax[i,isp].get_ylim(),color='gray',\
                    linestyle=':')
        """

        ax[1,isp].set_xlabel('$r$ (bohr)',fontsize=12)

    ax[0,0].annotate('(a)',(1.1e-2,4.45),fontsize=16)
    ax[0,1].annotate('(b)',(1.1e-2,4.45),fontsize=16)
    ax[1,0].annotate('(c)',(1.e-2,122),fontsize=16)
    ax[1,1].annotate('(d)',(1.e-2,122),fontsize=16)

    #ax[1,0].annotate('{:}$\\uparrow$'.format(atom),(1.1e-2,122),fontsize=20)
    #ax[1,1].annotate('{:}$\\downarrow$'.format(atom),(1.1e-2,122),fontsize=20)
    ax[1,0].set_ylabel('$(100\\%)(\\alpha_\\mathrm{approx}/\\alpha - 1)$',fontsize=12)
    #plt.title('ELF (blue) and $1-\\beta$ (orange); {:} atom'.format(atom),fontsize=12)

    #plt.show() ; exit()
    plt.savefig('./notes/manuscript/figs/{:}_atom.pdf'.format(atom),dpi=600,bbox_inches='tight')

    plt.cla()
    plt.clf()
    plt.close()

    exit()
    """
        Now both spin channels/total tau
    """

    fig,ax = plt.subplots(1,figsize=(6,4))

    tau = atdens[:,7] + atdens[:,8]

    tau_rpp = np.zeros(atdens.shape[0])
    tau_mrt = np.zeros(atdens.shape[0])

    for isp in range(2):

        n = 2*atdens[:,2+isp]

        gn = 2*atdens[:,4+isp]
        lap = 2*atdens[:,9+isp]

        tauw = gn**2/(8*n)
        tau0 = 3/10*(3*np.pi**2*n)**(2/3)*n

        kf = (3*np.pi**2*n)**(1/3)
        p = (gn/(2*kf*n))**2
        q = lap/(4*kf**2*n)

        tau_rpp += 0.5*ft_rpp(p,q,1,rpp_pars)*tau0
        tau_mrt += 0.5*ft_pc(p,q,mrt_pars[0],mrt_pars[1])*tau0

    ntot = atdens[:,2] + atdens[:,3]
    z = np.minimum(np.maximum((atdens[:,2] - atdens[:,3])/ntot,-1.0),1.0)
    dsz = ( (1 + z)**(5/3) + (1-z)**(5/3))/2
    tau0_sr = 3/10*(3*np.pi**2*ntot)**(2/3)*ntot*dsz
    tauw_nsr = atdens[:,6]**2/(8*ntot)

    alpha = (tau - tauw_nsr)/tau0_sr
    alpha_rpp = (tau_rpp - tauw_nsr)/tau0_sr
    alpha_mrt = (tau_mrt - tauw_nsr)/tau0_sr

    ax.plot(r,alpha,color='black')
    ax.plot(r,alpha_mrt,color='darkblue',linestyle='--')
    ax.plot(r,alpha_rpp,color='darkorange',linestyle='-.')

    ax.set_xlim(1.e-2,r.max())
    ax.set_ylim(0.0,5.0)
    ax.set_xscale('log')
    ax.hlines(1.0,*ax.get_xlim(),color='gray',linewidth=1,linestyle=':')
    ax.set_xlabel('$r$ (bohr)',fontsize=12)
    ax.set_ylabel('$\\alpha(r)$',fontsize=12)

    plt.show()
