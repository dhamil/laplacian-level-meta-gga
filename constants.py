

pi = 3.141592653589793238462643383279502884197169399375105820974944592307816406286198
#from julia BigFloat(pi)

Eh_to_eV = 27.211386245988 # https://physics.nist.gov/cgi-bin/cuu/Value?hr

bohr_to_ang = 5.29177210903 # https://physics.nist.gov/cgi-bin/cuu/Value?bohrrada0
au_to_erg_cm_m2 = 4.3597447222071/bohr_to_ang**2*10**7
