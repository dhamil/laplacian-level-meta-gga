import numpy as np
from math import ceil

rs = 4
system = 'surface' # surface, cluster, atom
N = 106# only used for cluster
#[2,8,18,20,34,40,58,92,106]

N_k_pts = 100
x_min = -4.5
x_max = 2
x_step = 0.00125
N_x_pts = ceil((x_max - x_min)/x_step)
#N_x_pts += 5-N_x_pts%6

method='numerov' # numerov or STO
if method == 'numerov':
    rgrid = (3000,0)
elif method == 'STO':
    rgrid = (300,20)
minimal_basis = True

enforce_order = 'near_degen' # always or near_degen
ell_order = [0,1,2,0,3,1,4,2,5,0,3]

density_mixing = 'linear' # none, linear, or mod_broyden

surface_charge = 0.0 # 0 for neutral surface, can have arbitrary surface charge density
kscr_default = {5: 0.9, 4: 0.9, 3:0.8, 3.25:0.8, 2:0.8}
if rs in kscr_default:
    kscr = kscr_default[rs]
else:
    kscr = 1.1283791670955126 # TF screening wavevector

max_scf_steps = {'HKS': 100, 'ESP': 1}
scf_conv = {'HKS': 1.e-6, 'CHG': 1.e-5, 'ESP': 1.e-2, 'BV':1.e-6}
min_scf = {'HKS': 5, 'ESP': 2}

save_orbitals = False

nproc = 4

finite_diff_order = 6

int_wgts = np.ones(N_x_pts)
int_wgts[[0,-1]]=0.5

"""
    From Abramowitz and Stegun 25.4.15, an O(h**7) Newton-Cotes rule

int_wgts = 5/288*np.ones(N_x_pts)
int_wgts[0::5] *= 2*19
int_wgts[1::5] *= 75
int_wgts[2::5] *= 50
int_wgts[3::5] *= 50
int_wgts[4::5] *= 75
"""
