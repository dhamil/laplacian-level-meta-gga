import settings
from surface.surface_scf import scf as surf_scf
from cluster.cluster_scf import scf as clus_scf


def write_settings_log():

    if settings.system == 'surface':
        desired_attrs = ['N_k_pts','density_mixing','finite_diff_order','kscr','max_scf_steps','min_scf','nproc','rs','scf_conv','surface_charge','x_max','x_min','x_step']
    elif settings.system == 'cluster' or settings.system == 'atom':
        desired_attrs = ['N','density_mixing','max_scf_steps','min_scf','nproc','rs','scf_conv','rgrid','minimal_basis','enforce_order','ell_order']

    if settings.system == 'surface':
        wdir = './data/'+settings.system+'/rs_'+str(settings.rs)
    elif settings.system == 'cluster':
        wdir = './data/'+settings.system+'/rs_'+str(settings.rs)+'/N_'+str(settings.N)+'_rs_'+str(settings.rs)
    elif settings.system == 'atom':
        wdir = './data/'+settings.system+'/N_'+str(settings.N)
    ofl = open(wdir+'/rs_'+str(settings.rs)+'_params.csv','w+')

    for entity in dir(settings):
        if entity in desired_attrs:
            val = getattr(settings,entity)
            if isinstance(val,dict):
                for elt in val.keys():
                    ofl.write(('{:},{:},{:}\n').format(entity,elt,val[elt]))
            else:
                ofl.write(('{:},{:}\n').format(entity,val))
    ofl.close()

    return


if __name__=="__main__":

    if settings.system == 'surface':
        surf_scf()
    elif settings.system == 'cluster' or settings.system == 'atom':
        clus_scf()
    else:
        raise ValueError('Unknown option ',settings.system)

    write_settings_log()
