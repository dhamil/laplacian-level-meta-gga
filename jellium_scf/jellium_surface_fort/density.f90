
module density_variables

  use comp_params
  implicit none

contains

  subroutine bkgd_dens(x,npos)

    implicit none
    real(dp), intent(in) :: x(nx)
    real(dp), intent(out) :: npos(nx)

    where (x <= 0._dp)
      npos = nbulk
    elsewhere
      npos = 0._dp
    end where

  end subroutine bkgd_dens


  subroutine density_k_integral_kx(k,kwg,phi,n)
    ! for situations where the wavefunctions are stored by
    ! row = k index
    ! column = x index
    implicit none

    real(dp), intent(in) :: k(nk), kwg(nk),phi(nk,nx)
    real(dp), intent(out) :: n(nx)

    n = 3*nbulk*matmul(kwg*(1._dp - k**2),abs(phi)**2)

  end subroutine density_k_integral_kx


  subroutine density_k_integral_xk(k,kwg,phi,n)
    ! for situations where the wavefunctions are stored by
    ! row = x index
    ! column = k index
    implicit none

    real(dp), intent(in) :: k(nk), kwg(nk),phi(nx,nk)
    real(dp), intent(out) :: n(nx)
    n = 3*nbulk*matmul(abs(phi)**2,kwg*(1._dp - k**2))

  end subroutine density_k_integral_xk

  subroutine all_density_variables(k,kwg,phi,vks,gn,lap,tau)

    use odes, only : fdiff_coeffs
    implicit none

    real(dp), intent(in) :: k(nk),kwg(nk),phi(nx,nk),vks(nx)
    real(dp), intent(out) :: gn(nx),lap(nx),tau(nx)

    integer :: ix,ifdc,ifdch
    real(dp) :: fwd_diff(finite_diff_order+1),cen_diff(finite_diff_order+1)
    real(dp) :: dphi(nx,nk),wgt(nk),ddphi(nx,nk),kfac(nk)

    ifdc = finite_diff_order
    ifdch = ifdc/2

    call fdiff_coeffs('F',ifdc,dx,fwd_diff)
    call fdiff_coeffs('C',ifdc,dx,cen_diff)

    kfac = (1._dp - k**2)
    wgt = kfac*kwg

    do ix = 1,nx

      if (ix < ifdch+1) then
        dphi(ix,:) = matmul(fwd_diff,phi(ix:ix+ifdc,:))
      else if (ix > nx - ifdch) then
        dphi(ix,:) = matmul(-fwd_diff,phi(ix-ifdc:ix,:))
      else
        dphi(ix,:) = matmul(cen_diff,phi(ix-ifdch:ix+ifdch,:))
      end if

      ddphi(ix,:) = (2*vks(ix) + kf0**2*kfac)*phi(ix,:)

    end do

    gn = 6*nbulk*abs( matmul(phi*dphi,wgt) )
    lap = 6*nbulk*matmul( dphi**2 + phi*ddphi, wgt )
    tau = 3*nbulk/2._dp*( matmul( abs(dphi)**2,wgt) + &
   &    matmul( 0.5_dp*kf0**2*abs(phi)**2 , wgt*kfac  ) )

  end subroutine all_density_variables


end module density_variables
