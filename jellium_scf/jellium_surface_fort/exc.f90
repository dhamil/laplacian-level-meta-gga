module xc_unpol

  use comp_params
  implicit none

  contains

    subroutine

end module xc_unpol

module lsda_xc

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), parameter :: pi = 3.14159265358979323846264338327950288419_dp

contains

  subroutine eps_xc_lsda_unpol(n,epsxc,vxc)

    implicit none
    real(dp), intent(in) :: n
    real(dp), intent(out) :: epsxc,vxc

    real(dp) :: rs,epsc,vc

    rs = (3._dp/(4._dp*pi*n))**(1._dp/3._dp)
    call eps_x_lsda_unpol(rs,epsxc,vxc)
    call ec_pw92_unpol(rs,epsc,vc)

    if (n > 1.d-14) then
      epsxc = epsxc + epsc
    else
      epsxc = 0._dp
    end if
    vxc = vxc + vc

  end subroutine eps_xc_lsda_unpol


  subroutine eps_x_lsda_unpol(rs,epsx,vx)

    implicit none
    real(dp), parameter :: ax=-0.238732414637843004535_dp!ax = -3._dp/(4._dp*pi)

    real(dp), intent(in) :: rs
    real(dp), intent(out) :: epsx,vx

    real(dp) :: kf

    kf = (9._dp*pi/4._dp)**(1._dp/3._dp)/rs
    epsx = ax*kf
    vx = -kf/pi

  end subroutine eps_x_lsda_unpol


  subroutine ec_pw92_unpol(rs,epsc,vc)

    implicit none

    real(dp), intent(in) :: rs
    real(dp), intent(out) :: epsc,vc

    real(dp) :: d_epsc_drs

    call pw92_g(rs,0.031091_dp,0.21370_dp,7.5957_dp,3.5876_dp,1.6382_dp,&
   &     0.49294_dp,epsc,d_epsc_drs)

    vc = epsc - rs/3._dp*d_epsc_drs

  end subroutine ec_pw92_unpol


  subroutine pw92_g(rs,apar,alpha,b1,b2,b3,b4,g,dg)

    implicit none

    real(dp), intent(in) :: rs,apar,alpha,b1,b2,b3,b4
    real(dp), intent(out) :: g,dg

    real(dp) :: q0,q1,q1p,q2,rsh

    rsh = rs**(0.5_dp)

    q0 = -2._dp*apar*(1._dp + alpha*rs)
    q1 = 2._dp*apar*(b1*rsh + b2*rs + b3*rs*rsh + b4*rs**2)
    q2 = log(1._dp + 1._dp/q1)
    g = q0*q2

    q1p = apar*(b1/rsh + 2*b2 + 3*b3*rsh + 4*b4*rs)
    dg = -2*apar*alpha*q2 - q0*q1p/(q1**2 + q1)

  end subroutine pw92_g

end module lsda_xc


module pbe_xc

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), parameter :: pi = 3.14159265358979323846264338327950288419_dp

contains

  subroutine pbe_xc_unpol(n,gn,exc,vxc,vxcg)

    implicit none
    real(dp), intent(in) :: n,gn
    real(dp), intent(out) :: exc,vxc,vxcg

    real(dp) :: ec,vc,vcg

    call pbe_x_unpol(n,gn,exc,vxc,vxcg)
    call pbe_c_unpol(n,gn,ec,vc,vcg)
    exc = exc + ec
    vxc = vxc + vc
    vxcg = vxcg + vcg
    
  end subroutine pbe_xc_unpol


  subroutine pbe_x_unpol(n,gn,ex,vxn,vxgn)

    implicit none

    real(dp), parameter :: kappa = 0.804_dp, mu = 0.2195164512208958307315498359457706101238727569580078125_dp

    real(dp), intent(in) :: n,gn
    real(dp), intent(out) :: ex,vxn,vxgn

    real(dp) :: kf,exlda,p,fx,d_exlda_dn,d_p_dn,d_p_dgn,d_fx_dp

    kf = (3*pi**2*n)**(1._dp/3._dp)
    exlda = -3._dp/(4._dp*pi)*kf*n

    p = (gn/(2*kf*n))**2
    fx = 1._dp + kappa - kappa/(1._dp + mu*p/kappa)
    ex = fx*exlda

    d_exlda_dn = -kf/pi

    d_p_dn = -8._dp/3._dp *p/n
    d_p_dgn = gn/(2*(kf*n)**2)

    d_fx_dp = mu/(1._dp + mu*p/kappa)**2

    vxn = fx*d_exlda_dn + exlda*d_fx_dp*d_p_dn
    vxgn = exlda*d_fx_dp*d_p_dgn

  end subroutine pbe_x_unpol


  subroutine pbe_c_unpol(n,gn,ec,vc,vcg)

    use lsda_xc, only : ec_pw92_unpol
    implicit none

    real(dp),parameter :: gamma = 0.0310906908696549_dp, beta = 0.066725_dp
    real(dp), intent(in) :: n,gn
    real(dp), intent(out) :: ec,vc,vcg

    real(dp) :: n,kf,rs,phi,gp3,s,t,t2,ec_lsda,vc_lsda
    real(dp) :: aa,v,v2,gt,hc,h,d_rs_dn
    real(dp) :: d_s_dgn,d_s_dn,d_phi_dzeta,dgp3_dzeta
    real(dp) :: d_t_drs,d_t_ds,d_t_dzeta,d_aa_drs,d_aa_dzeta
    real(dp) :: d_v_drs,d_v_ds,d_v_dzeta,dgt,d_gt_drs,d_gt_ds,d_gt_dzeta
    real(dp) :: d_h_drs,d_h_ds,d_h_dzeta

    kf = (3*pi**2*n)**(1._dp/3._dp)
    rs = (3._dp/(4._dp*pi*n))**(1._dp/3._dp)

    s = gn/(2*kf*n)
    t = (3*pi**2/16._dp)**(1._dp/3._dp)*s/(phi*rs**(0.5_dp))
    t2 = t*t

    call ec_pw92_unpol(rs,ec_lsda,vc_lsda)

    aa = beta/gamma/(exp(-ec_lsda/gp3)-1._dp)
    v = aa*t2
    v2 = v*v
    gt = (1._dp + v)/(1._dp + v + v2)

    hc = 1._dp + beta/gamma*t2*gt
    h = gp3*log(hc)
    ec = (ec_lsda + h)*n

    d_rs_dn = -rs/n/3._dp

    d_s_dgn = 1._dp/(2*kf*n)
    d_s_dn = -4._dp/3._dp*s/n

    d_t_drs = -0.5_dp*t/rs
    d_t_ds = (3*pi/16._dp)**(1._dp/3._dp)/(phi*rs**(0.5_dp))

    d_aa_drs = aa*d_ecl_drs*exp(-ec_lsda/gp3)/(exp(-ec_lsda/gp3)-1._dp)/gp3

    d_v_drs = d_aa_drs*t2 + 2*aa*t*d_t_drs
    d_v_dzeta = d_aa_dzeta*t2 + 2*aa*t*d_t_dzeta
    d_v_ds = 2*aa*t*d_t_ds

    dgt = v*(2._dp + v)/(1._dp + v + v2)**2
    d_gt_drs = 2*t*d_t_drs*gt - t2*dgt*d_v_drs
    d_gt_ds = 2*t*d_t_ds*gt - t2*dgt*d_v_ds

    d_h_drs = gp3/hc*beta/gamma*d_gt_drs
    d_h_ds = gp3/hc*beta/gamma*d_gt_ds

    vc = vc_lsda + h + n*(d_h_drs*d_rs_dn + d_h_ds*d_s_dn )
    vcg = n*d_h_ds*d_s_dgn

  end subroutine pbe_c

end module pbe_xc
