subroutine init_ks_potential(x,xwg,n,npos,var_bh,vh0,vks0)

  use odes, only : numerov_driver,fdiff_coeffs
  implicit none
  real(dp), intent(in) :: var_bh
  real(dp),dimension(nx),intent(in) :: x,xwg,n,npos
  real(dp),dimension(nx),intent(out) :: vh0,vks0

  integer :: ix
  real(dp) :: phi_m_inf,vxc_bulk,tmp,dphi,vxc(nx),otmp,dv,odv,slope,icpt
  real(dp) :: fdc(finite_diff_order+1)

  call fdiff_coeffs('F',finite_diff_order,dx,fdc)
  call eps_xc_lsda_unpol(n(1),tmp,vxc_bulk)
  phi_m_inf = vks_m_inf - vxc_bulk
  vxc = 0._dp
  tmp = 0._dp!0.1_dp
  call dipole_barrier(x,xwg,n,npos,dphi)

  do ix = 1,100
    call numerov_driver(4*pi*(npos - n),vxc,phi_m_inf,tmp,nx,dx,vh0)
    dv = dot_product(fdc,vh0(nx-finite_diff_order:nx))
    !print*,tmp,dv
    if (abs(dv) < 1.d-6) then
      exit
    else
      if (ix > 1) then
        slope = (odv - dv)/(otmp - tmp)
        icpt = odv - slope*otmp
        otmp = tmp
        tmp = -icpt/slope
      else
        otmp = tmp
        tmp = tmp+1.d-4
      end if
    end if
    odv = dv
  end do


  !vks0 = vks_m_inf
  vh0 = phi_m_inf
  where (x > 0._dp)
    vh0 = phi_m_inf + dphi
  !  vks0 = vks_m_inf + dphi
  end where
  print*,dphi
  stop
  do ix = 1,nx
    call eps_xc_lsda_unpol(n(ix),tmp,vxc(ix))
  end do
  vks0 = vh0 + vxc


end subroutine init_ks_potential
