#!/bin/zsh

rm -f jsscf
rm -rf jsscf.dSYM *.mod *.o

compiler=ifort
compiler_opts=(-g -traceback -i8 -I"${MKLROOT}/include" -O3 -xHost -heap-arrays) #-warn all)

libs=(${MKLROOT}/lib/libmkl_intel_ilp64.a ${MKLROOT}/lib/libmkl_sequential.a ${MKLROOT}/lib/libmkl_core.a -lpthread -lm -ldl)

subs=(settings.f90 grid_gen.f90 utilities.f90 lsda.f90 density.f90 surf_hamiltonian.f90 var_init_dens.f90 density_mixing.f90)

#ofs=(settings.o grid_gen.o utilities.o lsda.o density.o surf_hamiltonian.o var_init_dens.o density_mixing.o)

#$compiler ${compiler_opts[*]} -c ${subs[*]} #${libs[*]}
$compiler ${compiler_opts[*]} -o jsscf ${subs[*]} surface_scf.f90 ${libs[*]}

OMP_NUM_THREADS=4
export OMP_NUM_THREADS

#unlimit
time ./jsscf

rm -f jsscf
rm -rf jsscf.dSYM *.mod *.o
