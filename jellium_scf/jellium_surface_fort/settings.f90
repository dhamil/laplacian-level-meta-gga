!===============================================================================
!  User supplied input
module settings

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  integer, parameter :: max_scf_steps = 1000, min_scf_steps = 5
  ! NB: finite_diff_order must be even
  integer, parameter :: finite_diff_order = 6
  ! enter rs0 in bohr
  real(dp), parameter :: rs0 = 6._dp
  ! enter xmin, xmax, and xstep in units of Fermi wavelength
  real(dp), parameter :: x_min = -4.5_dp, x_max = 2.5_dp, x_step = 0.00125_dp

  integer, parameter :: nk = 100
  real(dp), parameter :: kstep = 0.0025_dp

  real(dp), parameter :: surface_charge = 0.0_dp, scf_conv = 1.d-6
  real(dp), parameter :: chg_conv=1.d-5, bv_conv=1.d-6

  logical, parameter :: save_orbitals = .false.

  ! DFA can be LDA or PBE
  ! DENSMIX can be NONE, LINR or DIIS
  character(len=*), parameter :: dfa = 'LDA',densmix = 'LINR'
  integer, parameter :: ndiis = 4
  real(dp), parameter :: nmix = 0.9_dp,vmix = 0.8_dp

end module settings
! end user supplied input
!===============================================================================


module comp_params

  use settings
  implicit none

  integer, parameter :: nx = ceiling((x_max - x_min)/x_step)+1
!  integer, parameter :: nk = ceiling(1._dp/kstep) + 1
  real(dp), parameter :: pi = 3.14159265358979323846264338327950288419_dp
  real(dp), parameter :: kf0 = (9._dp*pi/4._dp)**(1._dp/3._dp)/rs0, lf0 = 2._dp*pi/kf0
  real(dp), parameter :: nbulk = 3._dp/(4._dp*pi*rs0**3),vks_m_inf = -kf0**2/2._dp
  real(dp), parameter :: ktf = (4._dp*kf0/pi)**(0.5_dp)

  ! good values of kscr, in units of kf0:
  ! rs = 4, 0.8
  ! rs = 6, 0.5

  real(dp), parameter :: kscr = 0.5_dp*kf0!ktf
  real(dp), parameter :: dx = x_step*lf0, xmin = x_min*lf0, xmax = x_max*lf0
  real(dp), parameter :: au_to_cgs = 1556893.1028218947_dp

  ! NB: au_to_cgs is the conversion from Hartree/bohr**2 to erg/cm**2, as:
  !Eh_to_eV = 27.211386245988 ! https://physics.nist.gov/cgi-bin/cuu/Value?hr

  !bohr_to_ang = 5.29177210903 ! https://physics.nist.gov/cgi-bin/cuu/Value?bohrrada0
  !au_to_cgs = 4.3597447222071/bohr_to_ang**2*10**7


end module comp_params
