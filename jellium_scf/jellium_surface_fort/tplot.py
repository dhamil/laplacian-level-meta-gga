import matplotlib.pyplot as plt
import numpy as np

rs0 = 4
kf0 = (9*np.pi/4)**(1/3)/rs0
lf0 = 2*np.pi/kf0
n0 = 3/(4*np.pi*rs0**3)
#"""
dat = np.genfromtxt('./test.txt',delimiter='')
plt.plot(dat[:,0]/lf0,dat[:,1]/n0)
plt.plot(dat[:,0]/lf0,dat[:,2]*2/kf0**2)
plt.show()
exit()
#"""

wfile = './surface_data/rs_{:.4f}/density.csv'.format(rs0)

dat = np.genfromtxt(wfile,delimiter=',')
n = dat[:,1]
gn = dat[:,2]
ln = dat[:,3]
tau = dat[:,4]
p = (gn/(2*(3*np.pi**2*n)**(1/3)*n))**2
q = ln/(4*(3*np.pi**2*n)**(2/3)*n)
tw = gn**2/(8*n)
t0 = 3/10*(3*np.pi**2*n)**(2/3)*n
a = (tau - tw)/t0
plt.plot(dat[:,0],n/n0)
#plt.plot(dat[:,0],p)
#plt.plot(dat[:,0],q)
#plt.plot(dat[:,0],a)
plt.show()
