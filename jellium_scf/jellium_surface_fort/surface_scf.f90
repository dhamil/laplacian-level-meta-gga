program surface_scf

  use comp_params
  use grids, only : xk_mesh
  use density_variables
  use var_init_dens, only : var_opt
  use surface_hamiltonian, only : init_ks_potential,ks_potential_update,sigmas,&
  & get_phi_k_single,scf_errors,dipole_barrier
  use density_mixing, only : pulay_diis

  implicit none

  interface
    subroutine system(char)
      character :: char
    end subroutine system
  end interface

  logical :: ldone
  integer :: iscf,ik
  real(dp),dimension(nk) :: k,kwg,gk
  real(dp),dimension(nx) :: x,xwg,np,n0,n1,vh0,vks0,gn,lap,tau!,vks1
  real(dp) :: vbh,sig0,sig1,phik(nx,nk),sfdiff,apar,ochg,dmix(nx)
  real(dp) :: nchg,pchg,sig_ke,sig_h,sig_xc,tchg,dphi_re,wfn,bv_re,sl_sr_re
  real(dp), allocatable :: narr(:,:),nres(:,:),diis_mat(:,:)
  character(len=:),allocatable :: tstr, rs_str
  character(len=1000) :: tmpstr
  character(len=*),parameter :: fmtstr = '((I,","),8(E,","),E)', fmtstr2='(4(E,","),E)'

  write(tmpstr,"(F10.4)") rs0
  allocate(character(len=len(trim(adjustl(tmpstr)))) :: rs_str)
  rs_str = trim(adjustl(tmpstr))

  ! make sure we have a directory to store these results
  call system("mkdir -p ./surface_data/rs_"//rs_str)
  open(unit=1,file="./surface_data/rs_"//rs_str//"/logfile.csv")
  !deallocate(rs_str)

  write(1,'(a)') "Iteration, 1-sigma_i/sigma_(i-1), sigma_tot (erg/cm**2), KE, ESP, XC,&
 &Surf. charge, SL sum rule-pi/4, Delta Phi error, Budd-Vann. error"

  if (densmix(1:4)=='DIIS') then
    allocate(narr(ndiis,nx),nres(ndiis,nx),diis_mat(ndiis,ndiis))
  end if

  ! initialize x and k integration grids
  call xk_mesh(k,kwg,x,xwg)
  ! initialize positive background charge density
  call bkgd_dens(x,np)
  ! this is the integrated positive charge, we need the surface to be charge neutral
  pchg = -sum(xwg*np)
  ! starting density, a jellium slab subjected to a
  ! finite barrier vbh, determined variationally
  call var_opt(k,kwg,x,xwg,np,n0,vbh)
  ! use this to initialize the Hartree and KS potentials
  call init_ks_potential(x,xwg,n0,np,vbh,vh0,vks0)

  ! precondition the Hartree and KS potentials
  !do iscf = 1,5
  call ks_potential_update(x,xwg,n0,np,vh0,vks0)
  !end do

  ! initialize total surface formation energy
  sig0 = 1.d20
  ldone = .false.

  do iscf = 1,max_scf_steps

    !$OMP PARALLEL DO
    do ik = 1,nk
      call get_phi_k_single(k(ik),x,vks0,phik(:,ik),gk(ik))
    end do
    !$OMP END PARALLEL DO

    ! get the new density n1 from the updated wavefunctions phik
    call density_k_integral_xk(k,kwg,phik,n1)

    call sigmas(k,kwg,gk,xwg,n1,np,vks0,vh0,sig1,sig_ke,sig_h,sig_xc)
    call scf_errors(k,kwg,gk,x,xwg,n1,np,vh0,sl_sr_re,dphi_re,bv_re,wfn)

    if (sig1 /= sig1) then
      print*,"Total surface energy is NaN!"
      stop
    end if

    nchg = sum(xwg*n1)
    tchg = nchg + pchg
    sfdiff = 1._dp - sig1/sig0

    write(tmpstr,fmtstr) iscf,sfdiff,sig1*au_to_cgs,sig_ke*au_to_cgs,&
   &    sig_h*au_to_cgs,sig_xc*au_to_cgs,tchg,sl_sr_re,dphi_re,bv_re

    allocate(character(len=len(trim(adjustl(tmpstr)))) :: tstr)
    tstr = trim(adjustl(tmpstr))
    write(1,'(a)') tstr
    deallocate(tstr)

  !  print*,abs(sig1-sig0),scf_conv*abs(sig0),abs(tchg - surface_charge),chg_conv
    if ((abs(sig1-sig0) < scf_conv*abs(sig0)).and.(iscf > min_scf_steps).and. &
    &     (abs(tchg - surface_charge) < chg_conv)) then

      ldone = .true.
      call all_density_variables(k,kwg,phik,vks0,gn,lap,tau)
      open(unit=2,file="./surface_data/rs_"//rs_str//"/density.csv")
      write(2,*) "x, n(x), |n'(x)|, n''(x), tau(x)"
      do ik = 1,nx
        write(tmpstr,fmtstr2) x(ik),n1(ik),gn(ik),lap(ik),tau(ik)
        allocate(character(len=len(trim(adjustl(tmpstr)))) :: tstr)
        tstr = trim(adjustl(tmpstr))
        write(2,'(a)') tstr
        deallocate(tstr)
      end do
      close(2)
      exit
    end if

    if (densmix(1:4)=='NONE') then
      n0(:) = n1(:)
    else if (densmix(1:4)=='LINR') then
      !dmix = max(0.1_dp,min(0.95_dp,2._dp/(1._dp*nx)*sum(abs(n1-n0)/max(1.e-12,abs(n1+n0)))))
      !if ( abs(dmix*(n1(1)-n0(1))) < 0.05_dp*n0(1)  ) then
      !  n0 = dmix*n0 + (1._dp - dmix)*n1
      !end if
      !dmix = (1._dp - abs((n0 - n1)/max(1.e-12,n0 + n1)))
      !dmix = dmix/sum(dmix)
      !n0 = n0 + dmix*(n1 - n0)

      apar = nmix*(n0(1)-n1(1))
      if ( abs(apar) < 0.05_dp*n0(1)  ) then
        n0 = n0*nmix + n1*(1._dp - nmix)
      end if

    else if (densmix(1:4)=='ALIN') then
      if (iscf > 1) then
        apar = (surface_charge - ochg)/(tchg - ochg)
        if ((apar <= 0._dp).or.(apar >= 1._dp)) then
          n0 = n0*nmix + n1*(1._dp - nmix)
        else
          n0 = n0*apar + n1*(1._dp - apar)
        end if
      else
        n0 = n0*nmix + n1*(1._dp - nmix)
      end if
    else if (densmix(1:4)=='DIIS') then
      call pulay_diis(iscf,ndiis,xwg,n0,n1,narr,nres,diis_mat,nmix)
    else
      print*, "Unknown mixing type, ",densmix
      stop
    end if

    sig0 = sig1
    ochg = tchg

    call ks_potential_update(x,xwg,n0,np,vh0,vks0)

  end do

  open(unit=2,file="./test.txt")
  do ik = 1,nx
    write(2,*) x(ik),n0(ik),vh0(ik)
  end do
  close(2)

  call reporter(iscf,ldone,sfdiff,tchg,sig1*au_to_cgs,sig_ke*au_to_cgs,&
 &    sig_h*au_to_cgs,sig_xc*au_to_cgs,"./surface_data/rs_"//rs_str)

  deallocate(rs_str)
  close(1)

  if (densmix(1:4)=='DIIS') then
    deallocate(narr,nres,diis_mat)
  end if


end program surface_scf



subroutine reporter(nscf,lconv,ldiff,tchg,sigtot,sigke,sigh,sigxc,wdir)

  use comp_params
  implicit none

  logical, intent(in) :: lconv
  integer, intent(in) :: nscf
  real(dp), intent(in) :: ldiff,tchg,sigtot,sigke,sigh,sigxc
  character(len=*), intent(in) :: wdir

  open(unit = 10, file=wdir//"/runreport.txt")

  write(10,'(a)') "=============================="
  write(10,'(a)') "        Jellium Surface       "
  write(10,'(a)') "          Run Report          "
  write(10,'(a,/)') "=============================="

  if (lconv) then
    write(10,'(a,/)') "RUN CONVERGED"
  else
    write(10,'(a,/)') "RUN NOT CONVERGED"
  end if
  write(10,'(a,i4,a,i4,a)') "in ",nscf-1, " iterations, (",max_scf_steps," allowed)"
  write(10,'(a)') "SCF convergence criteria:"
  write(10,'(a,i2)') "    minimum SCF iterations: ",min_scf_steps
  write(10,'(a,e12.4)') "    sigma_tot converged within ",scf_conv
  write(10,'(a,e12.4,/)') "    surface charge converged within ",chg_conv
  write(10,'(a,e12.4)') "Last fractional difference in sigma_tot = ",ldiff
  write(10,'(a,e12.4,a,e12.4,a/)') "Total charge = ",tchg," (surface charge = ",surface_charge," )"

  write(10,'(a,f10.4)') "rs = ",rs0
  write(10,'(i6,a,/)') nx," x-points,"
  write(10,'(f10.4,a,f10.4,a,f10.6,a)') x_min," < x/lambda_F < ",x_max,", in steps of (",x_step,")*lambda_F"
  write(10,'(i4,a,/)') nk, " k-points"

  write(10,'(a)') 'Using '//dfa//' for XC energy'
  write(10,'(a,f10.4,a,f10.4,a,/)') "Poisson solver k_scr = ",kscr/kf0," k_F=",kscr/ktf," k_TF"

  if (densmix(1:4)=='NONE') then
    write(10,'(a)') 'No density mixing'
  else if (densmix(1:4)=='LINR') then
    write(10,'(a,f6.4)') 'Linear density mixing, mixing parameter = ',nmix
  else if (densmix(1:4)=='ALIN') then
    write(10,'(a,f6.4)') 'Adaptive linear density mixing, fallback mixing parameter = ',nmix
  else if (densmix(1:4)=='DIIS') then
    write(10,'(a,i3,a)') 'Pulay DIIS density mixing, ',ndiis,' iterations saved'
    write(10,'(a,f6.4)') '    initial linear mixing parameter = ',nmix
  end if
  write(10,'(a,f6.4,/)') 'Linear potential mixing, mixing parameter = ',vmix

  close(10)

end subroutine reporter
