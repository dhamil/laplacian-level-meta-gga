module lsda_xc

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), parameter :: pi = 3.14159265358979323846264338327950288419_dp

contains

  subroutine eps_xc_lsda_unpol(n,epsxc,vxc)

    implicit none
    real(dp), intent(in) :: n
    real(dp), intent(out) :: epsxc,vxc

    real(dp) :: rs,epsc,vc

    rs = (3._dp/(4._dp*pi*n))**(1._dp/3._dp)
    call eps_x_lsda_unpol(rs,epsxc,vxc)
    call ec_pw92_unpol(rs,epsc,vc)

    if (n > 1.d-14) then
      epsxc = epsxc + epsc
    else
      epsxc = 0._dp
    end if
    vxc = vxc + vc

  end subroutine eps_xc_lsda_unpol


  subroutine eps_x_lsda_unpol(rs,epsx,vx)

    implicit none
    real(dp), parameter :: ax=-0.238732414637843004535_dp!ax = -3._dp/(4._dp*pi)

    real(dp), intent(in) :: rs
    real(dp), intent(out) :: epsx,vx

    real(dp) :: kf

    kf = (9._dp*pi/4._dp)**(1._dp/3._dp)/rs
    epsx = ax*kf
    vx = -kf/pi

  end subroutine eps_x_lsda_unpol


  subroutine ec_pw92_unpol(rs,epsc,vc)

    implicit none

    real(dp), intent(in) :: rs
    real(dp), intent(out) :: epsc,vc

    real(dp) :: d_epsc_drs

    call pw92_g(rs,0.031091_dp,0.21370_dp,7.5957_dp,3.5876_dp,1.6382_dp,&
   &     0.49294_dp,epsc,d_epsc_drs)

    vc = epsc - rs/3._dp*d_epsc_drs

  end subroutine ec_pw92_unpol


  subroutine pw92_g(rs,apar,alpha,b1,b2,b3,b4,g,dg)

    implicit none

    real(dp), intent(in) :: rs,apar,alpha,b1,b2,b3,b4
    real(dp), intent(out) :: g,dg

    real(dp) :: q0,q1,q1p,q2,rsh

    rsh = rs**(0.5_dp)

    q0 = -2._dp*apar*(1._dp + alpha*rs)
    q1 = 2._dp*apar*(b1*rsh + b2*rs + b3*rs*rsh + b4*rs**2)
    q2 = log(1._dp + 1._dp/q1)
    g = q0*q2

    q1p = apar*(b1/rsh + 2*b2 + 3*b3*rsh + 4*b4*rs)
    dg = -2*apar*alpha*q2 - q0*q1p/(q1**2 + q1)

  end subroutine pw92_g

end module lsda_xc
