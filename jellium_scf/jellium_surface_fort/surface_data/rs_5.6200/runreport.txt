==============================
        Jellium Surface       
          Run Report          
==============================

RUN NOT CONVERGED

in 1001 iterations, (1000 allowed)
SCF convergence criteria:
    minimum SCF iterations:  5
    sigma_tot converged within   0.1000E-05
    surface charge converged within   0.1000E-04

Last fractional difference in sigma_tot =   0.1283E-01
Total charge =  -0.1057E+00 (surface charge =   0.0000E+00 )

rs =     5.6200
  5200 x-points,

   -4.5000 < x/lambda_F <     2.0000, in steps of (    0.0013)*lambda_F
 100 k-points

Using LDA for XC energy
Poisson solver k_scr =     2.0000 k_F=    1.0358 k_TF

Pulay DIIS density mixing,  10 iterations saved
    initial linear mixing parameter = 0.2000
Linear potential mixing, mixing parameter = 0.2000

