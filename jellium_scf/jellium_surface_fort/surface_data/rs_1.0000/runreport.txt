==============================
        Jellium Surface       
          Run Report          
==============================

RUN NOT CONVERGED

in 1001 iterations, (1000 allowed)
SCF convergence criteria:
    minimum SCF iterations:  5
    sigma_tot converged within   0.1000E-05
    surface charge converged within   0.1000E-04

Last fractional difference in sigma_tot =  -0.1590E-05
Total charge =  -0.3364E-04 (surface charge =   0.0000E+00 )

rs =     1.0000
  8400 x-points,

   -4.5000 < x/lambda_F <     6.0000, in steps of (    0.0013)*lambda_F
 100 k-points

Using LDA for XC energy
Poisson solver k_scr =     0.6000 k_F=    0.7366 k_TF

No density mixing
Linear potential mixing, mixing parameter = 0.3000

