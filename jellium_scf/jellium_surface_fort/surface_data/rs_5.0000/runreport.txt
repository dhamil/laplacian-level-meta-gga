==============================
        Jellium Surface       
          Run Report          
==============================

RUN CONVERGED

in  434 iterations, ( 500 allowed)
SCF convergence criteria:
    minimum SCF iterations:  5
    sigma_tot converged within   0.1000E-05
    surface charge converged within   0.1000E-04

Last fractional difference in sigma_tot =   0.3414E-06
Total charge =   0.5146E-05 (surface charge =   0.0000E+00 )

rs =     5.0000
  5600 x-points,

   -4.5000 < x/lambda_F <     2.5000, in steps of (    0.0013)*lambda_F
 100 k-points

Using LDA for XC energy
Poisson solver k_scr =     0.8000 k_F=    0.4392 k_TF

Linear density mixing, mixing parameter = 0.9000
Linear potential mixing, mixing parameter = 0.3000

