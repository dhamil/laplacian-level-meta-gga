
module odes

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  contains
    !========================================================
    !Numerov solver for problems of the form
    !  y''(x) = U(x) + V(x)y(x)
    !  bds = (x_min,x_max)
    !  u and v should be entered as vectors, not functions
    !  Needs y(x_0) and y'(x_0)
    !========================================================
    subroutine numerov_driver(u,v,y0,y0p,n,h,y)

      implicit none

      integer, intent(in) :: n
      real(dp), intent(in), dimension(n) :: u,v
      real(dp), intent(in) :: y0,y0p,h
      real(dp), intent(out), dimension(n) :: y

      integer :: step,ind,dir,b,c,f
      real(dp) :: a,h2,a11,a12,a21,a22,b1,b2,numer

      y = 0._dp
      h2 = h**2
      a = h2/12._dp

      if (h>0._dp) then
        dir = 1
        ind = 1
      else
        dir = -1
        ind = n
      end if

      y(ind) = y0
      b = ind
      c = ind + dir
      f = ind + 2*dir
    !========================================================
    !  first step suggested by
    !  J.L.M. Quiroz González and D. Thompson,
    !  ``Getting started with Numerov's method''
    !  Computers in Physics 11, 514--515 (1997).
    !  doi: https://doi.org/10.1063/1.168593
    !========================================================

      a11 = 1._dp - v(c)*h2/4._dp
      a12 = v(f)*a/2._dp
      a21 = -2._dp - 5*v(c)*h2/6._dp
      a22 = 1._dp - v(f)*a
      b1 = y0 + h*y0p + a*( 7*(u(b) + v(b)*y0) + 6*u(c) - u(f) )/2._dp
      b2 = -y0 + a*( u(b) + v(b)*y0 + 10*u(c) + u(f) )
      y(c) = (a22*b1 - a12*b2)/(a11*a22 - a12*a21)

      do step = 1,n-2

        b = ind + dir*(step - 1)
        c = ind + dir*step
        f = ind + dir*(step + 1)

        numer = (a*v(b) - 1._dp)*y(b) + 2*y(c)*(1._dp + 5*a*v(c)) + &
       &     a*(u(b) + 10*u(c) + u(f))

        y(f) = numer/(1._dp - v(f)*a)

      end do

    end subroutine numerov_driver

    !========================================================
    ! Numerov solver for problems of the form
    !  y''(x) = V(x)y(x)
    !  bds = (x_min,x_max)
    !  v should be entered as a vector, not function
    !  Needs y(x_0) and y'(x_0)
    !========================================================
    subroutine numerov_driver_eigen(v,y0,y0p,n,h,y)

      implicit none

      integer, intent(in) :: n
      real(dp), intent(in) :: v(n),y0,y0p,h
      real(dp), intent(out) :: y(n)

      integer :: step,ind,dir,b,c,f
      real(dp) :: a,h2,a11,a12,a21,a22,b1,b2

      h2 = h**2
      a = h2/12._dp

      if (h>0._dp) then
        dir = 1
        ind = 1
      else
        dir = -1
        ind = n
      end if

      y(ind) = y0
      b = ind
      c = ind + dir
      f = ind + 2*dir
    !========================================================
    !  first step suggested by
    !  J.L.M. Quiroz González and D. Thompson,
    !  ``Getting started with Numerov's method''
    !  Computers in Physics 11, 514--515 (1997).
    !  doi: https://doi.org/10.1063/1.168593
    !========================================================

      a11 = 1._dp - v(c)*h2/4._dp
      a12 = v(f)*a/2._dp
      a21 = -2._dp - 5*v(c)*h2/6._dp
      a22 = 1._dp - v(f)*a
      b1 = y0 + h*y0p + 7*a*v(b)*y0/2._dp
      b2 = -y0 + a*v(b)*y0
      y(c) = (a22*b1 - a12*b2)/(a11*a22 - a12*a21)

      do step = 1,n-2

        b = ind + dir*(step - 1)
        c = ind + dir*step
        f = ind + dir*(step + 1)

        y(f) = ( (a*v(b) - 1._dp)*y(b) + 2*y(c)*(1._dp + 5*a*v(c)) )/(1._dp - v(f)*a)

      end do

    end subroutine numerov_driver_eigen


    !==========================================================
    ! Finite difference coefficients from
    !    B. Fornberg,
    !    ``Generation of finite difference formulas on arbitrarily spaced grids''
    !    Math. Comp. 51, 699 (1988),
    !    DOI: https://doi.org/10.1090/S0025-5718-1988-0935077-0
    !==========================================================

    subroutine fdiff_coeffs(dir,order,h,fdc)

      implicit none
      character(1), intent(in) :: dir
      integer, intent(in) :: order
      real(dp), intent(in) :: h
      real(dp), dimension(order+1), intent(out) :: fdc

      if (dir=='C') then

        ! of the form + h, +2 h, + 3 h, + 4 h
        ! Table 1, ibid.

        if (order == 2) then
          fdc = (/ -0.5_dp, 0._dp, 0.5_dp /)
        else if (order == 4) then
          fdc = (/ 1._dp/12._dp,-2._dp/3._dp,0._dp,2._dp/3._dp,-1._dp/12._dp /)
        else if (order == 6) then
          fdc = (/ -1._dp/60._dp,3._dp/20._dp,-3._dp/4._dp,0._dp,3._dp/4._dp,&
         &     -3._dp/20._dp,1._dp/60._dp /)
        else if (order == 8) then
          fdc = (/ 1._dp/280._dp,-4._dp/105._dp,1._dp/5._dp,-4._dp/5._dp,0._dp,&
         &     4._dp/5._dp,-1._dp/5._dp,4._dp/105._dp,-1._dp/280._dp /)
        end if

        ! of the form +0, + h, +2 h, + 3 h, + 4 h,...
        ! Table 3, ibid.

      else if ((dir == 'F').or.(dir == 'B')) then

        if (order == 2) then
          fdc = (/ -1.5_dp, 2._dp, -0.5_dp /)
        else if (order == 4) then
          fdc = (/ -25._dp/12._dp,4._dp,-3._dp,4._dp/3._dp,-1._dp/4._dp /)
        else if (order == 6) then
          fdc = (/ -49._dp/20._dp,6._dp,-15._dp/2._dp,20._dp/3._dp,&
         &      -15._dp/4._dp,6._dp/5._dp,-1._dp/6._dp /)
        else if (order == 8) then
          fdc = (/ -761._dp/280._dp,8._dp,-14._dp,56._dp/3._dp,-35._dp/2._dp,&
         &      56._dp/5._dp,-14._dp/3._dp,8._dp/7._dp,-1._dp/8._dp /)
        end if

        if (dir == 'B') then
          fdc = -1*fdc
        end if

      end if

      fdc = fdc/h

    end subroutine fdiff_coeffs


end module odes


!==========================================================
! The following routines adapted from W. Press et al. Numerical Recipes
! in FORTRAN 77, Cambridge University Press, 1992.
!==========================================================

module roots

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  contains

    subroutine bracket(fun,bds,intervals,nivals)

      implicit none

      integer, parameter :: nstep = 500

      real(dp), external :: fun
      real(dp), dimension(2), intent(in) :: bds

      integer, intent(out) :: nivals
      real(dp), dimension(10,2),intent(out) :: intervals

      integer :: istep
      real(dp) :: tmp,step,cfun,ofun

      step = (bds(2)-bds(1))/(1._dp*nstep)
      tmp = bds(1)
      nivals = 0
      do istep = 1,nstep
        cfun = fun(tmp)
        if (istep == 1) then
          ofun = cfun
        end if
        if (ofun*cfun <= 0._dp) then
          nivals = nivals + 1
          intervals(nivals,1) = tmp-step
          intervals(nivals,2) = tmp
        end if
        ofun = cfun
        tmp = tmp + step
      end do

    end subroutine bracket


    subroutine bisect(fun,bds,mid,mtmp)

      implicit none

      integer,parameter :: maxstep=500
      real(dp),parameter :: tol=1.5d-7

      real(dp), external :: fun
      real(dp), dimension(2), intent(in) :: bds

      real(dp),intent(out) :: mid,mtmp

      integer :: istep
      real(dp) :: ltmp,utmp,lbd,ubd

      ltmp = fun(bds(1))
      utmp = fun(bds(2))
      if (ltmp*utmp > 0._dp) then
        print*,"No root in bracket"
        stop
      end if

      if (ltmp < 0._dp) then
        lbd = bds(1)
        ubd = bds(2)
      else
        lbd = bds(2)
        ubd = bds(1)
      end if

      do istep =1,maxstep
        mid = (lbd + ubd)/2._dp
        mtmp = fun(mid)

        !if ((abs(mid - lbd)<tol*abs(mid)).or.(abs(mtmp)<tol)) then
        if (abs(mtmp)<tol) then
          exit
        end if

        if (mtmp < 0._dp) then
          lbd = mid
        else
          ubd = mid
        end if

      end do

    end subroutine bisect

end module roots


module optimization

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307),maxstep=500

  contains

    subroutine min_bracket(a,b,c,fa,fb,fc,fun)

      implicit none
      real(dp), parameter :: minres=1.d-20,maxres=100._dp
      ! gr = golden ratio = (1 + 5**(0.5))/2, from julia
      real(dp), parameter :: gr = 1.618033988749894902526_dp

      real(dp), intent(inout) :: a,b
      real(dp), intent(out) :: c,fa,fb,fc
      real(dp), external :: fun

      integer :: step
      real(dp) :: tmp,fu,q,r,u,ulim,qmr

      fa = fun(a)
      fb = fun(b)

      if (abs(fa - fb) < 1.d-16) then
        a = a + sign(1.d-6,b-a)
        fa = fun(a)
      end if

      if (fb > fa) then
        tmp = a
        a = b
        b = tmp
        tmp = fb
        fb = fa
        fa = tmp
      end if

      c = b
      b = (c + gr*a)/(1._dp + gr)
      fc = fb
      fb = fun(b)
      !c = b + gr*(b - a)
      !fc = fun(c)

      do step = 1,maxstep
        ! original routine had a simple goto statement,
        ! but that runs the risk of an endless loop
        ! if you need more than 500 steps, there's probably
        ! not a minimum here

        if (fb >= fc) then

          r = (b - a)*(fb - fc)
          q = (b - c)*(fb - fa)
          qmr = sign(max(abs(q - r),minres),q-r)
          u = b - ( (b-c)*q - (b-a)*r )/(2*qmr)
          ulim = b + maxres*(c - b)

          if ((b - u)*(u - c) > 0._dp) then
            fu = fun(u)
            if (fu < fc) then
              a = b
              fa = fb
              b = u
              fb = fu
              return
            else if (fu > fb) then
              c = u
              fc = fu
              return
            end if

            u = c + gr*(c - b)
            fu = fun(u)
          else if ((c - u)*(u-ulim) > 0._dp) then
            fu = fun(u)
            if (fu < fc) then
              b = c
              c = u
              u = c + gr*(c - b)
              fb = fc
              fc = fu
              fu = fun(u)
            end if
          else if ((u - ulim)*(ulim - c) >= 0._dp) then
            u = ulim
            fu = fun(u)
          else
            u = c + gr*(c - b)
            fu = fun(u)
          end if

          a = b
          b = c
          c = u
          fa = fb
          fb = fc
          fc = fu
        end if

      end do

    end subroutine min_bracket


    subroutine brent_min(a0,b0,fun,tol,xopt,fopt,foundmin)

      implicit none
      integer, parameter :: dp = selected_real_kind(15, 307),maxstep=500

      real(dp), parameter :: cgr = 0.38196601125010509747426_dp, eps = 1.d-10
      ! cgr = (3 - 5**(0.5))/2

      real(dp), intent(in) :: a0,b0,tol
      real(dp), external :: fun
      integer,intent(out) :: foundmin
      real(dp), intent(out) :: xopt,fopt

      logical :: goodfit
      integer :: step
      real(dp) :: a,b,c,a1,b1,fa,fb,fc
      real(dp) :: c1,d,dtmp,fu,fv,fw,fx,p,q,r,tol1,tol2
      real(dp) :: u,v,w,x,xm

      a1 = a0 ; b1 = b0
      call min_bracket(a1,b1,c1,fa,fb,fc,fun)
      print*,a1,b1,c1,fa,fb,fc
      stop
      a = min(a1,c1)
      b = max(a1,c1)
      v = b1
      w = v
      x = v
      d = 0._dp
      fx = fb!fun(x)
      fv = fx
      fw = fx

      do step = 1,maxstep

        ! no goto's in my programs!
        goodfit = .true.
        xm = 0.5_dp*(a + b)
        tol1 = tol*abs(x) + eps
        tol2 = 2*tol1

        if ( abs(x - xm) <= (tol2 - 0.5_dp*(b-a)) ) then
          foundmin=step
          xopt = x
          fopt = fx
          return
        end if

        if (abs(d) > tol1) then

          r = (x - w)*(fx - fv)
          q = (x - v)*(fx - fw)
          p = (x - v)*q - (x - w)*r
          q = 2._dp*(q - r)
          if (q > 0._dp) then
            p = -p
          end if
          q = abs(q)
          dtmp = d
          d = c
          if (  ( abs(p) >= abs(0.5_dp*q*dtmp) ).or.(p <= q*(a - x) )&
           &    .or.(p >= q*(b - x))  ) then

            goodfit = .true.

          else

            goodfit = .false.
            c = p/q
            u = x + c
            if (  (u - a < tol2).or.(b - u < tol2)  ) then
              c = sign(tol1,xm-x)
            end if

          end if

        end if

        if (goodfit) then
          if (x >= xm) then
            d = a - x
          else
            d = b - x
          end if
          c = cgr*d
        end if

        if ( abs(c) >= tol1 ) then
          u = x + c
        else
          u = x + sign(tol1,c)
        end if

        fu = fun(u)
        if (fu <= fx) then
          if (u >= x) then
            a = x
          else
            b = x
          end if

          v = w
          fv = fw
          w = x
          fw = fx
          x = u
          fx = fu

        else

          if (u < x) then
            a = u
          else
            b = u
          end if

          if (  (fu <= fw).or.(w == x)  ) then
            v = w
            fv = fw
            w = u
            fw = fu
          else if (  (fu <= fv).or.(v==x).or.(v==w)  ) then
            v = u
            fv = fu
          end if

        end if

      end do

      foundmin=-step
      xopt = x
      fopt = fx

    end subroutine brent_min


    !===============================================================================
    ! this is a simple routine that tries to find a minimum by going downhill
    ! from min(x0,x1) to max(x0,x1), and bounded by those points
    ! at each step, the function is evaluated. If the function begins to increase,
    ! the routine turns around and decreases the step size to approach the last
    ! minimum point
    !===============================================================================
    subroutine downhill_cons_bounds(x0,x1,fun,xopt,fopt,nfev)

      implicit none
      integer, parameter :: dp = selected_real_kind(15, 307),maxref=10, maxstep=10


      real(dp), intent(in) :: x0,x1
      real(dp), external :: fun
      integer,intent(out) :: nfev
      real(dp), intent(out) :: xopt,fopt

      integer :: istep,imin
      real(dp) :: step,cfun,ofun,a,b,dir,tx

      if (x0 < x1) then
        a = x0
        b = x1
      else
        a = x1
        b = x0
      end if

      step = (b - a)/(1._dp*maxstep)
      tx = a
      ofun = fun(tx)
      xopt = tx
      fopt = ofun
      nfev = 1
      dir = sign(1._dp,step)

      do istep = 1,maxref

        do imin = 1,maxstep

          tx = min(max(a,tx + dir*step),b)
          cfun = fun(tx)
          if (abs(ofun-cfun)<1.d-8*abs(ofun)) then
            return
          end if
          nfev = nfev+1
          if ( cfun - ofun > 0._dp ) then
            ! if the function has moved away from the minimum,
            ! turn around and decrease the step size
            xopt = tx - dir*step
            fopt = ofun
            ofun = cfun
            dir = -dir
            step = step/(1._dp*maxstep)
            exit
          end if
          ofun = cfun
        end do

      end do

    end subroutine downhill_cons_bounds

end module optimization
