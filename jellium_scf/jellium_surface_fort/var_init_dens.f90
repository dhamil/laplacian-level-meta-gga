!==========================================================
! This code adapted from
!  [M1975] G.D. Mahan,
!   ``Surface energy of jellium metal'',
!  Phys. Rev. B 12, 5585 (1975).
!  https://doi.org/10.1103/PhysRevB.12.5585
!==========================================================

module var_init_dens

  use comp_params

  implicit none

  public :: var_opt
  private :: phi_mat,hartree,sigtot

  contains

    subroutine phi_mat(lam,k,x,phi)
      ! Eqs. 2.5 of [M1975]
      implicit none

      real(dp), intent(in) :: lam, k(nk), x(nx)
      real(dp), intent(out), dimension(nk,nx) :: phi

      integer :: ix
      real(dp) :: d,lh
      real(dp),dimension(nx) :: y

      ! Eq. 2.2 of [M1975]
      d = 3/(4*kf0)*( pi/2._dp - (2._dp-lam)*asin(lam**(-0.5_dp)) &
     &    - max(lam-1._dp,0._dp)**(0.5_dp) )

      lh = lam**(0.5_dp)
      y = kf0*(d-x)
      do ix = 1,nx
        if (y(ix) < 0._dp) then
          phi(:,ix) = k*exp(y(ix)*(lam - k**2)**(0.5_dp))/lh
        else
          phi(:,ix) = sin(k*y(ix) + asin(k/lh))
        end if
      end do

    end subroutine phi_mat

    subroutine hartree(dens,bdens,x,xwg,uh)

      ! Above Eq. 2.6 of [M1975]

      implicit none

      real(dp),dimension(nx),intent(in) :: dens,bdens,x,xwg
      real(dp), intent(out) :: uh

      integer :: ix
      real(dp),dimension(nx) :: out_int,in_int

      out_int = xwg*(dens - bdens)
      ! ix = 1 element contributes nothing to sum
      do ix = 2,nx
        in_int = (x - x(ix))*out_int
        uh = uh + out_int(ix)*sum(in_int(1:ix))
      end do
      uh = 2*pi*uh

    end subroutine hartree

    subroutine sigtot(lam,k,kwg,x,xwg,bdens,en,dens,phi)

      use lsda_xc, only : eps_xc_lsda_unpol
      use density_variables, only : density_k_integral_kx
      use surface_hamiltonian, only : xc_energy
      implicit none

      real(dp), intent(in) :: lam, k(nk), kwg(nk), x(nx),xwg(nx),bdens(nx)
      real(dp), intent(out) :: en, dens(nx), phi(nk,nx)

      real(dp) :: ke,h,epsxc(nx),exc,eh,epsxc_bulk,tmp

      if (lam < 1._dp) then
        en = 1.d20
        dens = 0._dp
        phi = 0._dp
        return
      end if

      ! Eqs. 2.3 of [M1975]
      h = ( (15*lam**2 - 16*lam - 8._dp)*asin(lam**(-0.5_dp)) &
      &   + 3*(2._dp - 5*lam)*max(lam-1._dp,0._dp)**(0.5) )/(2*pi)
      ! Eqs. 2.3 of [M1975]
      ke = kf0**4/(160*pi)*(1._dp + h)

      call phi_mat(lam,k,x,phi)
      call density_k_integral_kx(k,kwg,phi,dens)

      call xc_energy(dens,epsxc)
      call eps_xc_lsda_unpol(nbulk,epsxc_bulk,tmp)
      exc = sum(xwg*dens*(epsxc-epsxc_bulk))

      call hartree(dens,bdens,x,xwg,eh)

      en = ke + eh + exc

    end subroutine sigtot


    subroutine var_opt(k,kwg,x,xwg,bdens,dens,v0)

      use optimization, only : downhill_cons_bounds
      implicit none

      real(dp), intent(in) :: k(nk),kwg(nk),x(nx),xwg(nx),bdens(nx)

      real(dp), intent(out) :: v0,dens(nx)!,phi(nk,nx)

      ! logical :: brent_success
      integer :: nsteps
      real(dp) :: lopt,enopt,en,phi(nk,nx)

      ! call brent_min(1._dp,5._dp,wrap_en,1.d-6,lopt,enopt,brent_success)
      ! print*,brent_success,lopt,enopt
      call sigtot(5._dp,k,kwg,x,xwg,bdens,en,dens,phi)
      call downhill_cons_bounds(1._dp,5._dp,wrap_en,lopt,enopt,nsteps)
      call sigtot(lopt,k,kwg,x,xwg,bdens,en,dens,phi)
      v0 = kf0**2*lopt/2._dp*sum(xwg*dens)

      contains

        function wrap_en(lam)
          implicit none
          real(dp),intent(in) :: lam
          real(dp) :: wrap_en,tmp1(nx),tmp2(nk,nx)
          call sigtot(lam,k,kwg,x,xwg,bdens,wrap_en,tmp1,tmp2)
        end function

    end subroutine var_opt



end module var_init_dens
