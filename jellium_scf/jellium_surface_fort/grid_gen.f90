module grids

  use comp_params
  implicit none

  contains

  subroutine gauss_legendre(npts,grid,wg)

    implicit none

    interface
      subroutine dsteqr(compz, n, d, e, z, ldz, work, info )
        integer, parameter :: dp = selected_real_kind(15, 307)
        character(len=1) :: compz
        integer :: info, ldz, n
        real(dp) :: d(n), e(n-1), work(max(1,2*n-2)), z(ldz, n)
      end subroutine dsteqr
    end interface

    integer, intent(in) :: npts
    real(dp), dimension(npts), intent(out) :: grid,wg

    integer :: i
    real(dp), dimension(npts,npts) :: eigvs
    real(dp), dimension(npts-1) :: udiag
    real(dp), dimension((max(1,2*npts-2))) :: twork

    grid = 0._dp
    do i=1,npts-1
      udiag(i) = gl_udiag(i-1)
    end do
    call dsteqr('I',npts,grid,udiag,eigvs,npts,twork,i)

    if (i /= 0) then
      print*,'WARNING, call to DSTEQR failed with INFO = ',i
    end if

    wg = 2._dp*eigvs(1,:)**2

    contains

      function gl_udiag(n)
        implicit none

        integer, intent(in) :: n
        real(dp) :: gl_udiag,an,anp1

        an = (2*n + 1._dp)/(n + 1._dp)
        anp1 = (2*n + 3._dp)/(n + 2._dp)
        gl_udiag = ( (n + 1._dp)/(n + 2._dp)/(an*anp1) )**(0.5_dp)

      end function gl_udiag


  end subroutine gauss_legendre

  subroutine clenshaw_curtis(np1,x,wg)

    ! Clenshaw-Curtis weights and points taken from
    ! https://dlmf.nist.gov/3.5
    ! Subsection 3.5(iv), Eqs. 3.5.15 -- 3.5.17

    implicit none
    integer, intent(in) :: np1
    real(dp),intent(out) :: x(np1),wg(np1)

    integer :: i,j,n,fn2,n2
    real(dp) :: g,b,rn

    wg = 0._dp

    n = np1-1
    rn = (1._dp*n)
    fn2 = floor(rn/2._dp)
    n2 = int(rn/2._dp)

    do i = 0,n
      x(n-i+1) = cos((i*pi)/rn)
      if ((i == 0).or.(i==n)) then
        g = 1._dp
      else
        g = 2._dp
      end if
      do j = 1,fn2
        if (j == n2) then
          b = 1._dp
        else
          b = 2._dp
        end if
        wg(n-i+1) = wg(n-i+1) + b/(4._dp*j**2 - 1._dp)*cos(2*j*i*pi/rn)
      end do
      wg(n-i+1) = g/(1._dp*n)*(1._dp - wg(n-i+1))
    end do

  end subroutine clenshaw_curtis


  subroutine xk_mesh(k,kwg,x,xwg)

    implicit none
    real(dp), intent(out) :: k(nk),kwg(nk), x(nx), xwg(nx)

    integer :: ix
    ! Gauss-Legendre grid for k-points
    ! GL grid gives -1 < k/kF0 < 1
    !call gauss_legendre(nk,k,kwg)
    call clenshaw_curtis(nk,k,kwg)
    ! so transform to 0 < k/kF0 < 1
    k = 0.5_dp*(k + 1._dp)
    kwg = 0.5_dp*kwg

    !kwg = kstep
    !kwg(1) = 0.5_dp*kstep
    !kwg(nk) = 0.5_dp*kstep
    !do ix = 0,nk-1
    !  k(ix+1) = ix*kstep
    !end do

    ! simple equally-spaced trapezoidal mesh for x grid
    xwg = dx
    xwg(1) = 0.5_dp*dx
    xwg(nx) = 0.5_dp*dx

    do ix = 0,nx-1
      x(ix+1) = xmin + ix*dx
    end do

  end subroutine xk_mesh

end module grids
