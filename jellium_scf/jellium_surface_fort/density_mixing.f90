module density_mixing

use comp_params
implicit none

contains

  subroutine pulay_diis(iter,dim,wg,n_in,n_out,past_n,res,amat,lmix)

    !----------------------------------------------------
    ! Adapted from:
    !  G. Kresse and J. Furthmueller,
    !  ``Efficient iterative schemes for ab initio
    !      total-energy calculations using a plane-wave
    !      basis set'',
    !  Phys. Rev. B 54, pp. 11169 (1996).
    !  https://doi.org/10.1103/PhysRevB.54.11169
    !----------------------------------------------------

    implicit none

    interface

      subroutine dgetrf(m,n,a,lda,ipiv,info)
        integer, parameter :: dp = selected_real_kind(15, 307)
        integer, intent(in) :: m,n,lda
        real(dp), intent(inout) :: a(lda,n)
        integer, intent(out) :: ipiv(min(m,n)),info
      end subroutine dgetrf

      subroutine dgetri(n,a,lda,ipiv,work,lwork,info)
        integer, parameter :: dp = selected_real_kind(15, 307)
        integer, intent(in) :: n,lda,lwork,ipiv(n)
        real(dp), intent(inout) :: a(lda,n)
        real(dp), intent(out) :: work(max(1,lwork))
        integer, intent(out) :: info
      end subroutine dgetri

    end interface

    integer, intent(in) :: iter,dim
    real(dp), intent(in) :: n_out(nx),wg(nx)
    real(dp), intent(inout) :: n_in(nx),past_n(dim,nx),res(dim,nx),amat(dim,dim)
    real(dp), optional :: lmix ! linear mixing parameter, if needed

    integer :: i,pivs(min(iter,dim)),iinfo,ind
    real(dp) :: alpha(min(iter,dim)),ainv(min(iter,dim),min(iter,dim)),det
    real(dp) :: tmparr(min(iter,dim)),dmix

    if (present(lmix)) then
      dmix = lmix
    else
      dmix = nmix
    end if

    if (dim == 1) then
      ! if we only store one step, just linearly mix
      n_in = n_in*(1._dp - dmix) + n_out*dmix
      return
    end if

    ! iter is the SCF iteration, dim is the number of iterations to mix
    ! so when iter < dim, we need to appropriately modify the size of the matrices invovled

    ind = min(iter,dim)
    if (iter > dim) then
      ! update residual vectors to eliminate previous values
      do i = 1,dim-1
        past_n(i,:) = past_n(i+1,:)
        res(i,:) = res(i+1,:)
        amat(i,:) = amat(i+1,:)
      end do
    end if

    past_n(ind,:) = n_in
    res(ind,:) = n_in - n_out
    do i = 1,ind
      do iinfo = 1,ind
        amat(iinfo,i) = sum(wg*res(iinfo,:)*res(i,:))
      end do
    end do

    if (iter < dim) then
      ! first step, just linearly mix
      n_in = n_in*(1._dp - dmix) + n_out*dmix
      return
    end if

    if ((dim == 2).or.(iter==2)) then
      det = amat(1,1)*amat(2,2)-amat(1,2)*amat(2,1)
      if (abs(det)==0._dp) then
        print*,"Pulay DIIS matrix is singular"
        stop
      end if
      ainv(1,1) = amat(2,2)/det
      ainv(1,2) = -amat(1,2)/det
      ainv(2,1) = -amat(2,1)/det
      ainv(2,2) = amat(1,1)/det
    else
      ainv(:,:) = amat(1:ind,1:ind)
      ! first get LU decomposition of A
      call dgetrf(ind,ind,ainv,ind,pivs,iinfo)
      if (iinfo == 0) then
        ! if that was succesful, invert
        call dgetri(ind,ainv,ind,pivs,tmparr,ind,iinfo)
      end if

      if (iinfo /= 0) then
        ! but if either LAPACK subroutine failed, raise error and stop
        print*,"Pulay DIIS matrix couldn't be inverted:"
        if (iinfo < 0) then
          print*,"    probably a NaN in element",iinfo
        else
          print*,"    matrix was singular"
        end if
        stop
      end if
    end if
    ! DIIS weight factors from least-squares optimization
    det = 0._dp
    do i = 1,ind
      alpha(i) = sum(ainv(:,i))
      ! optimally mixed density
      det = det + alpha(i)
    end do
    ! appropriately normalized
    alpha = alpha/det

    det = 0._dp
    do i = 1,ind
      det = det + sum(wg*abs(alpha(i)*res(i,:)))
    end do

    if (det < 1.d-6) then
      !n_in = 0._dp
      !do i = 1,ind
      n_in = matmul(alpha,past_n)
      !end do
      where (n_in < 1.d-14)
        n_in = 1.d-14
      end where
    else
      n_in = n_in*(1._dp - dmix) + n_out*dmix
    end if

  end subroutine pulay_diis


end module density_mixing
