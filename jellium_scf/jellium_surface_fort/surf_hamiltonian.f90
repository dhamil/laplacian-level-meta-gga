!========================================================
!    This code benefited greatly from the works of

!    N.D. Lang and W. Kohn,
!         ``Theory of Metal Surfaces: Charge Density and Surface Energy''
!         Phys. Rev. B 1, 4555 (1970).
!         https://doi.org/10.1103/PhysRevB.1.4555

!    R. Monnier and J.P. Perdew,
!        ``Surfaces of real metals by the variational self-consistent method'',
!        Phys. Rev. B 17, 2595 (1978).
!        https://doi.org/10.1103/PhysRevB.17.2595

!    J.P. Perdew and Y. Wang,
!        ``Jellium work function for all electron densities'',
!        Phys. Rev. B 38, 12228 (1988).
!        https://doi.org/10.1103/PhysRevB.38.1222
!========================================================


module surface_hamiltonian

  use lsda_xc, only : eps_xc_lsda_unpol
  use comp_params
  implicit none

  contains

    subroutine init_ks_potential(x,xwg,n,npos,var_bh,vh0,vks0)

      implicit none
      real(dp), intent(in) :: var_bh
      real(dp),dimension(nx),intent(in) :: x,xwg,n,npos
      real(dp),dimension(nx),intent(out) :: vh0,vks0

      integer :: ix
      real(dp) :: phi_m_inf,vxc_bulk,tmp,dphi,vxc(nx)

      call eps_xc_lsda_unpol(nbulk,tmp,vxc_bulk)
      phi_m_inf = vks_m_inf - vxc_bulk

      !call dipole_barrier(x,xwg,n,npos,dphi)

      vh0 = phi_m_inf
      where (x > 0._dp)
        vh0 = phi_m_inf + var_bh!dphi
      end where

      do ix = 1,nx
        call eps_xc_lsda_unpol(n(ix),tmp,vxc(ix))
      end do
      vks0 = vh0 + vxc

    end subroutine init_ks_potential


    subroutine xc_energy(n,exc)

      implicit none
      real(dp), intent(in) :: n(nx)
      real(dp), intent(out) :: exc(nx)

      integer :: ix
      real(dp) :: tmp

      do ix = 1,nx
        call eps_xc_lsda_unpol(n(ix),exc(ix),tmp)
      end do

    end subroutine xc_energy


    subroutine xc_energy_potential(n,exc,vxc)

      implicit none
      real(dp), intent(in) :: n(nx)
      real(dp), intent(out),dimension(nx) :: exc,vxc

      integer :: ix

      do ix = 1,nx
        call eps_xc_lsda_unpol(n(ix),exc(ix),vxc(ix))
      end do

    end subroutine xc_energy_potential


    subroutine hartree_numerov(x,xwg,n,npos,vh0,vh1)

      !--------------------------------------------------
      !    vh''(x) = - 4*pi*[ n(x) - n+(x)]
      !    solved by the Numerov method
      !--------------------------------------------------
      use odes, only : numerov_driver,fdiff_coeffs
      implicit none
      integer :: i0
      real(dp), intent(in) :: x(nx),xwg(nx),n(nx),npos(nx),vh0(nx)
      real(dp), intent(out) :: vh1(nx)
      real(dp) :: ker(nx),tmp(nx),vh_m_inf,vxc_bulk,vks_m_inf,ctmp
      real(dp) :: fdc(finite_diff_order+1),df0,vmp

      i0 = minloc(abs(x),1)
      ker = -4._dp*pi*(n - npos)
      tmp = 0._dp

      call eps_xc_lsda_unpol(nbulk,tmp(1),vxc_bulk)
      vh_m_inf = vks_m_inf - vxc_bulk

      call fdiff_coeffs('F',finite_diff_order,dx,fdc)
      df0 = dot_product(fdc,vh0(1:finite_diff_order+1))!-dot_product(xwg,ker)!
      !print*,df0
      call numerov_driver(ker(1:i0),tmp,vh_m_inf,0._dp,i0,dx,vh1(1:i0))
      ctmp = vh1(i0)
      call numerov_driver(ker(i0:nx),tmp,vh0(nx),0._dp,nx-i0+1,-dx,vh1(i0:nx))
      vh1(i0:nx) = vh1(i0:nx) + ctmp - vh1(i0)

      !vmp = max(0.1_dp,min(0.95_dp,200._dp/(1._dp*nx)*sum(abs(vh1-vh0)/max(1.e-12,abs(vh1+vh0)))))
      !vh1 = vh0*vmp + (1._dp-vmp)*vh1
      vh1 = vh1 + (vh0 - vh1)*vmix

    end subroutine hartree_numerov


    subroutine hartree_update(x,xwg,n,npos,vh0,vh1)

      !--------------------------------------------------
      !    vh''(x) = - 4*pi*[ n(x) - n+(x)]
      !    Solved by iteration of the Poisson equation
      !    according to Monnier and Perdew
      !--------------------------------------------------
      use odes, only : fdiff_coeffs
      implicit none
      real(dp), intent(in) :: x(nx),xwg(nx),n(nx),npos(nx),vh0(nx)
      real(dp), intent(out) :: vh1(nx)

      integer :: ix,iref
      real(dp) :: vh_m_inf,vxc_bulk,tmp,ker(nx)
      real(dp) :: igrl0,igrl1,detc,c1,c2,vmp

      call eps_xc_lsda_unpol(nbulk,tmp,vxc_bulk)
      vh_m_inf = vks_m_inf - vxc_bulk

      ker = xwg*(4*pi*(n - npos) + kscr**2*vh0)/(2*kscr)

      ! impose boundary conditions that phi(x(1)) = fixed constant and phi'(x(Nx)) = 0
      tmp = exp(-kscr*(x(nx)-x(1)))
      igrl0 = vh_m_inf - sum(ker*exp(-kscr*(x-x(1))))
      igrl1 = sum(ker*exp(-kscr*(x(nx)-x)))
      detc = 1._dp/(1._dp + tmp**2)
      c1 = (igrl0 - igrl1*tmp )*detc
      c2 = (igrl0*tmp + igrl1)*detc

      do ix = 1,nx

        vh1(ix) = sum(ker*exp(-kscr*abs(x(ix)-x))) &
        &    + c1*exp(-kscr*(x(ix)-x(1))) + c2*exp(-kscr*(x(nx) - x(ix)))

      end do
      vmp = max(0.1_dp,min(0.95_dp,200._dp/(1._dp*nx)*sum(abs(vh1-vh0)/max(1.e-12,abs(vh1+vh0)))))
      vh1 = vh0*vmp + (1._dp-vmp)*vh1
      !vh1 = vh0*vmix + (1._dp-vmix)*vh1

    end subroutine hartree_update


    subroutine ks_potential_update(x,xwg,n,npos,vh,vks)

      implicit none

      real(dp),dimension(nx),intent(in) :: x,xwg,n,npos
      real(dp),intent(inout) :: vh(nx),vks(nx)

      real(dp) :: tmpl(nx),vxc(nx)

      call hartree_update(x,xwg,n,npos,vh,tmpl)
      !call hartree_numerov(x,xwg,n,npos,vh,tmpl)
      vh = tmpl(:)
      call xc_energy_potential(n,tmpl,vxc)

      vks = vh + vxc

    end subroutine ks_potential_update


    subroutine sigmas(k,kwg,gk,xwg,n,npos,vks,vh,sig,sig_ke,sig_h,sig_xc)

      implicit none

      real(dp), dimension(nk), intent(in) :: k,kwg,gk
      real(dp), dimension(nx), intent(in) :: xwg,n,npos,vks,vh

      real(dp),intent(out) :: sig,sig_ke,sig_h,sig_xc

      real(dp) :: epsxc(nx),vxc(nx),epsxc_bulk,vxc_bulk

      sig_ke = kf0**4/(32._dp*pi) - kf0**4/(2._dp*pi**2)*sum(kwg*(1._dp - k**2)*gk*k) &
     &     - sum(xwg*n*(vks - vks_m_inf))

      sig_h = 0.5_dp*sum(xwg*vh*(n - npos))

      call xc_energy_potential(n,epsxc,vxc)
      call eps_xc_lsda_unpol(nbulk,epsxc_bulk,vxc_bulk)
      sig_xc = sum(xwg*n*(epsxc - epsxc_bulk))

      sig = sig_ke + sig_h + sig_xc

    end subroutine sigmas


    subroutine get_phi_k_single(k,x,vks,phi,gk)

      use odes, only : numerov_driver_eigen,fdiff_coeffs
      implicit none
      real(dp), intent(in) :: k,x(nx),vks(nx)
      real(dp), intent(out) :: phi(nx),gk

      real(dp) :: two_epsk,two_d2_phi_dx2(nx),asyk,phi_inf,dphi_inf
      real(dp) :: fdc(finite_diff_order+1),dphi0,kx0_m_phase,kd

      ! first step, set up boundary condition at x --> + infinity
      kd = kf0*k
      !two_epsk = kd**2 + 2._dp*vks_m_inf
      asyk = (2._dp*(vks(nx) - vks_m_inf)-kd**2)**(0.5_dp)!(2._dp*vks(nx)-two_epsk)**(0.5_dp)!
      if (2._dp*(vks(nx) - vks_m_inf) - kd**2 < 0._dp) then
        print*,'ERROR: could not determine wavefunctions'
        print*,'Asymptotic solution could not be matched, likely that density is in error'
        stop
      end if
      phi_inf = exp(-asyk*x(nx))
      dphi_inf = -asyk*phi_inf
      two_d2_phi_dx2 = 2._dp*(vks-vks_m_inf) - kd**2

      ! solve for phi(x) using Numerov method
      call numerov_driver_eigen(two_d2_phi_dx2,phi_inf,dphi_inf,nx,-dx,phi)
      ! now impose boundary conditions at x --> -infinity
      call fdiff_coeffs('F',finite_diff_order,dx,fdc)
      dphi0 = dot_product(fdc,phi(1:finite_diff_order+1))

      ! match wavefunction to plane wave at x = x_min
      kx0_m_phase = atan(kd*phi(1)/dphi0)
      phi = sin(kx0_m_phase)*phi/phi(1)
      ! also gives us the phase, gamma(k), up to a multiple of pi/2
      gk = modulo(kd*x(1) - kx0_m_phase,pi)
      !gk = acos(cos(kd*x(1)-asin(phi(1))))

    end subroutine get_phi_k_single


    ! these let us check the self-consistency of the outputs

    subroutine dipole_barrier(x,xwg,n,npos,delphi)

      implicit none
      real(dp), dimension(nx), intent(in) :: x,xwg,n,npos
      real(dp), intent(out) :: delphi

      delphi = 4*pi*sum(xwg*x*(n - npos))

    end subroutine


    subroutine work_function(x,xwg,n,npos,delta_phi,wfn)

      implicit none
      real(dp),dimension(nx),intent(in) :: x,xwg,n,npos
      real(dp),intent(out) :: wfn

      real(dp) :: phi_m_inf,vxc_bulk,tmp,delta_phi

      call eps_xc_lsda_unpol(nbulk,tmp,vxc_bulk)
      phi_m_inf = vks_m_inf - vxc_bulk

      wfn = phi_m_inf + delta_phi

    end subroutine work_function


    subroutine budd_vannimenus(x,vh,bv_rel_err)

      !---------------------------------------------------
      !  Budd-Vannimenus theorem (Eq. 4.18 of Monnier and Perdew) states that
      !   Phi(0) - Phi(-inf) = n_bulk d [3/5 eps_F(n_bulk) + eps_xc(n_bulk)] d n_bulk
      !  at the converged density
      ! eps_F = k_F**2/2
      ! eps_xc is XC energy per electron
      !---------------------------------------------------
      implicit none

      real(dp),intent(in) :: x(nx),vh(nx)
      real(dp),intent(out) :: bv_rel_err

      integer :: icz
      real(dp) :: exc_bulk,vxc_bulk,bv_lhs,bv_rhs

      call eps_xc_lsda_unpol(nbulk,exc_bulk,vxc_bulk)

      icz = minloc(abs(x),dim=1)
      bv_lhs = vh(icz) - vh(1)
      bv_rhs = kf0**2/5._dp + vxc_bulk - exc_bulk

      bv_rel_err = 1._dp - bv_lhs/bv_rhs

    end subroutine budd_vannimenus


    subroutine scf_errors(k,kwg,gk,x,xwg,n,npos,vh,slsr,dphi,bvre,wfn)

      implicit none
      real(dp), dimension(nk), intent(in) :: k,kwg,gk
      real(dp), dimension(nx), intent(in) :: x,xwg,n,npos,vh

      real(dp), intent(out) :: dphi,wfn,bvre,slsr
      ! Sugiyama-Langreth sum rule, see Eq. 4.17 of Monnier and Perdew
      slsr = 2*sum(kwg*k*gk) - pi/4._dp

      call dipole_barrier(x,xwg,n,npos,dphi)
      call work_function(x,xwg,n,npos,dphi,wfn)
      dphi = 1._dp - (vh(nx) - vh(1))/dphi
      call budd_vannimenus(x,vh,bvre)

    end subroutine scf_errors

end module surface_hamiltonian
