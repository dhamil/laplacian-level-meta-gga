import numpy as np
from scipy.linalg import eigh,eigh_tridiagonal

import settings,constants
from dft.lsda import exc_lsda
from utilities.finite_differences import discrete_laplacian
from utilities.ode_integrators import numerov_driver,fdiff_coeffs
from utilities.roots import bisect
from utilities.gauss_quad import gauss_quad

from basis.sto import overlap,ke,vcoul,factorial,sto_sph

"""
    For details specific to jellium clusters (e.g., external potential) see

    L.M. Almeida, J.P. Perdew, and C. Fiolhais,
    ``Surface and curvature energies from jellium spheres: Density functional hierarchy and quantum Monte Carlo''
    Phys. Rev. B 66, 075115 (2002).
    https://doi.org/10.1103/PhysRevB.66.075115
"""

pi = constants.pi

rs = settings.rs
nelec = settings.N
if settings.method=='STO':
    nr = settings.rgrid[0] + settings.rgrid[1]
elif settings.method=='numerov':
    nr = settings.rgrid[0]

Rc = rs*nelec**(1.0/3.0)
nbulk = 3.0/(4.0*pi*rs**3)

def grad_sph_avh_sph_harm(ell):
    """
        returns spherical average of |grad Y_l0(theta)|^2
    """
    wg,x = gauss_quad(2*ell+1)
    p = np.zeros((ell+1,2*ell+1))
    dpldth = np.zeros((ell+1,2*ell+1))
    sph = np.zeros(ell+1)
    p[0] = 1.0
    p[1] = x
    # For the P_l(x) recursion, see NIST DLMF 18.9(i)
    # https://dlmf.nist.gov/18.9
    for ellp in range(2,ell+1):
        p[ellp] = ( (2*ellp - 1.0)*x*p[ellp-1] - (ellp-1.0)*p[ellp-2])/ellp
    # For the derivative recursion relation, see NIST DLMF 14.10
    # https://dlmf.nist.gov/14.10
    for ellp in range(1,ell+1):
        dpldth[ellp] = ellp/(1.0 - x**2)**(0.5)*(x*p[ellp] - p[ellp-1])
        sph[ellp] = ((2*ellp+1.0)/(8*pi)*np.sum(wg*dpldth[ellp]**2) )**(0.5)
    return sph

def order_eigenvalues(eig_l,eigv_l,ell_l,order):
    nell = len(eig_l)
    norder = len(order)
    ubd = min(nell,norder)
    for iell in range(ubd):
        ell = order[iell]
        if ell_l[iell] != ell:
            for jell in range(iell+1,nell):
                if ell_l[jell] == ell:
                    # switch ith and jth eigen- values, vectors, and quantum numbers
                    tmp_eig = eig_l[jell]
                    eig_l[jell] = eig_l[iell]
                    eig_l[iell] = tmp_eig
                    eigv_l[[jell,iell]] = eigv_l[[iell,jell]]
                    ell_l[jell] = ell_l[iell]
                    ell_l[iell] = ell
                    break
    return eig_l,eigv_l,ell_l

def reg_n(n_in,regpar=1.e-14):
    n_in[n_in<regpar] = regpar
    return n_in

def background_chg_dens(r):
    npos = np.zeros(nr)
    npos[r<Rc] = nbulk
    return npos

def vext(r):
    if settings.system == 'cluster':
        vp = np.zeros(nr)
        vp[r<=Rc] = -nelec/(2*Rc)*(3.0 - (r[r<=Rc]/Rc)**2)
        vp[r>Rc] = -nelec/r[r>Rc]
    elif settings.system == 'atom':
        vp = -nelec/r
    return vp

def vext_mat(par2,par1):
    if settings.system == 'cluster':
        n1,z1,l1,norm1 = par1
        n2,z2,l2,norm2 = par2
        if l1 != l2:
            return 0.0
        nt = n1 + n2
        zt = z1 + z2
        rt = zt*Rc
        vpg = 0.0
        for itr in range(nt):
            fac = factorial(itr)
            vpg += rt**itr/fac
        vpl_1 = vpg + rt**(nt)/factorial(nt)
        vpl_2 = vpl_1 + rt**(nt+1)/factorial(nt+1) + rt**(nt+2)/factorial(nt+2)

        vpl_1 = factorial(nt)*(1.0 - np.exp(-rt)*vpl_1)/zt**(nt+1)
        vpl_2 = factorial(nt+2)*(1.0 - np.exp(-rt)*vpl_2)/zt**(nt+3)

        vpl = (3*vpl_1 - vpl_2/Rc**2)/(2.0*Rc)
        vpg = factorial(nt-1)*np.exp(-rt)*vpg/zt**(nt)
        vp = -nelec*(vpl + vpg)
    elif settings.system == 'atom':
        vp = -nelec*vcoul(par2,par1)
    return vp

def sto_dens_init(conf_l,r,rwg):
    dens = np.zeros(nr)
    tau = np.zeros(nr)
    ntot = 0.0
    for i,conf in enumerate(conf_l):
        qn,z,ell,_ = conf
        chi,_,_,chi_lap = sto_sph(r,qn,z,ell)
        mult = 2*ell+1.0
        occ = min(nelec/2-ntot,mult)
        ntot += occ
        dens += occ*chi**2
        tau += -0.5*occ*chi*chi_lap
    etot = total_energy(r,rwg,dens,dens,tau,tau)
    return dens,etot[0]

def sto_ham_init(conf_l,r):
    # this is stuff that remains unchanged between iterations,
    # i.e. only depends upon the individual basis function used, not the density
    ke_vc = np.zeros((len(conf_l),len(conf_l)))
    s = np.zeros((len(conf_l),len(conf_l)))
    chi_mat = np.zeros((len(conf_l),nr))
    chi_grad_r_mat = np.zeros((len(conf_l),nr))
    chi_grad_ang_mat = np.zeros((len(conf_l),nr))
    chi_lap_mat = np.zeros((len(conf_l),nr))
    for i1,conf1 in enumerate(conf_l):
        qn,z,ell,_ = conf1
        for i2 in range(i1+1):
            conf2 = conf_l[i2]
            ke_vc[i2,i1] = ke(conf2,conf1) + vext_mat(conf2,conf1)
            s[i2,i1] = overlap(conf2,conf1)
            if i1 != i2:
                ke_vc[i1,i2] = ke_vc[i2,i1] # real symmetric matrix
                s[i1,i2] = s[i2,i1]
        chi_mat[i1],chi_grad_r_mat[i1],chi_grad_ang_mat[i1],chi_lap_mat[i1] = sto_sph(r,qn,z,ell)
    return ke_vc,s,chi_mat,chi_grad_r_mat,chi_grad_ang_mat,chi_lap_mat

def sto_ham(r,rad_wg,n,orbs,conf_l,ke_vc,s,chi,chi_grad_r,chi_grad_ang,chi_lap):
    nbas = len(conf_l)
    nu_new = np.zeros(nr)
    eps_up = np.zeros(0)
    tau_up = np.zeros(nr)

    ham = np.zeros((nbas,nbas))
    ham[:,:] = ke_vc

    vH = esp(r,rad_wg,n)
    _,vxc,_ = exc_lsda(n/2.0,n/2.0)
    vmod = vH+vxc
    for i1 in range(nbas):
        n1,z1,l1,norm1 = conf_l[i1]
        for i2 in range(i1+1):
            n2,z2,l2,norm2 = conf_l[i2]
            if l1 == l2:
                v_expec = np.sum(rad_wg*orbs[i1]*orbs[i2]*vmod)
                ham[i2,i1] += v_expec
                ham[i1,i2] += v_expec
    eps,coeffs = eigh(ham,b=s)
    #orbout = np.zeros((0,nr))
    ntot = 0
    #coeffs[np.abs(coeffs)<1.e-14] =0.0

    cmat = np.zeros((0,nbas))

    for ielec in range(int(nelec/2)):
        phi = np.zeros(nr)
        gphi_r = np.zeros(nr)
        gphi_ang = np.zeros(nr)
        lphi = np.zeros(nr)
        coeffl = coeffs[:,ielec]
        orbind = np.argmax(np.abs(coeffl))
        orbchar = conf_l[orbind][2]
        ncoeffl = np.zeros(nbas)
        for icoeff,acoeff in enumerate(coeffl):
            n,z,ell,_ = conf_l[icoeff]
            if ell == orbchar:
                ncoeffl[icoeff] = acoeff
                phi += acoeff*chi[icoeff]
                gphi_r += acoeff*chi_grad_r[icoeff]
                gphi_ang += acoeff*chi_grad_ang[icoeff]
                lphi += acoeff*chi_lap[icoeff]
            else:
                ncoeffl[icoeff] = 0.0
        norm = (np.sum(rad_wg*phi**2))**(0.5)
        cmat = np.vstack((cmat,ncoeffl/norm))
        phi /= norm
        gphi_r/=norm
        gphi_ang/=norm
        lphi/=norm
        multiplicity = 2*orbchar+1.0
        occ = min(nelec/2-ntot,multiplicity)
        ntot += occ
        tau_up += 0.5*occ*(gphi_r**2+gphi_ang**2)#-0.5*occ*phi*lphi#
        nu_new += occ*phi**2
        eps_up = np.append(eps_up,eps[ielec])
        #orbout = np.vstack((orbout,phi))
        if ntot == nelec/2:
            break
    nd_new = nu_new
    eps_dn = eps_up
    tau_dn = tau_up
    #nu_new = reg_n(nu_new)
    #nd_new = reg_n(nd_new)
    etot = total_energy(r,rad_wg,nu_new,nd_new,tau_up,tau_dn)

    return nu_new,nd_new,eps_up,eps_dn,tau_up,tau_dn,etot,cmat

def esp(r,rad_wg,n):
    """
        Hartree electrostatic potential
        For a system with spherical symmetry, can easily be shown that
        vH(r) = 4 pi/r int_0^r r'^2 n(r') d r' + 4 pi int_r^inf r' n(r') d r'
    """
    esp = np.zeros(nr)
    for ir,ar in enumerate(r):
        esp[ir] = 1.0/ar*np.sum(rad_wg[:ir]*n[:ir])
        esp[ir] += np.sum(rad_wg[ir:]/r[ir:]*n[ir:])
    return esp

def total_energy(r,rad_wg,nu,nd,tu,td):
    def radint(targ):
        return np.sum(rad_wg*targ)
    n = nu+nd
    etot = np.zeros(6)
    etot[1] = radint(tu+td)

    vpos = vext(r)
    etot[2] = radint(vpos*n)

    vH = esp(r,rad_wg,n)
    etot[3] = 0.5*radint(vH*n)

    ex,ec,_,_,_,_ = exc_lsda(nu,nd,sep=True)
    etot[4] = radint(ex*n)
    etot[5] = radint(ec*n)

    if settings.system == 'cluster':
        self_en_pos = 3*nelec**2/(5.0*Rc)
    elif settings.system == 'atom':
        self_en_pos = 0.0
    etot[0] = np.sum(etot[1:]) + self_en_pos
    return etot

def ks_potential(r,spc,rad_wg,nu,nd):
    # returns LSDA (PW92) XC energy density and potential
    _,vxcu,vxcd = exc_lsda(nu,nd)
    vpos = vext(r)
    vesp = esp(r,rad_wg,nu+nd)
    return vpos+vesp+vxcu, vpos+vesp+vxcd

def numerov_hamiltonian(r,spc,rad_wg,nu_in,nd_in):

    def normalization(worb):
        """
            returns normalization factor of KS orbital
            radial wavefunction R(r) = u(r)/r
            normalization int_0^inf |R(r)|^2 r^2 dr = int_0^inf |u(r)|^2 dr = 1
            one-electron wavefunction phi_i(r) = R(r)/sqrt(4 pi)
        """
        orb2 = np.abs(worb)**2/r**2
        #orb2_norm = 4*pi*spc*np.sum(rwg*r**2*orb2)
        #return orb2/(orb2_norm*r)
        orb2_norm = np.sum(rad_wg*orb2)
        return orb2_norm**(0.5)

    if settings.system=='cluster':
        if nelec <= 2.0:
            max_ell = 0
        elif nelec <= 8:
            max_ell = 1
        elif nelec <= 18:
            max_ell = 2
        elif nelec <= 40:
            max_ell = 3
        elif nelec <= 58:
            max_ell = 4
        elif nelec <= 106:
            max_ell = 5
        else:
            max_ell = 12
    elif settings.system == 'atom':
        if nelec <= 2.0:
            max_ell = 0
        elif nelec <= 10:
            max_ell = 1
        elif nelec <= 36:
            max_ell = 2
        elif nelec <= 86:
            max_ell = 3
        elif nelec <= 168:
            max_ell = 4
        elif nelec <= 290:
            max_ell = 5
        else:
            max_ell = 12
    max_ell += 1
    fwd_diff = fdiff_coeffs('fwd',6)/spc
    cen_diff = fdiff_coeffs('cen',6)/spc

    vs,_ = ks_potential(r,spc,rad_wg,nu_in,nd_in)

    def log_deriv(eval,ell):

        veff = vs + ell*(ell+1)/(2*r**2)
        #veff = vs + (ell+0.5)**2/(2*r**2)

        if eval < veff.max():
            irm = np.argmin(np.abs(eval - veff)) # approximate turning surface radius
        else:
            irm = int((nr + nr%2)/2)
        rm = r[irm]
        if eval > 0.0:
            eval+=0.j
            return 1e20,1e20

        rhs = 2*(veff - eval)
        u_inf = np.exp(-(-2*eval)**(0.5)*r[-1])/r[-1]
        du_inf = -(-2*eval)**(0.5)*u_inf - u_inf/r[-1]
        u0 = r[0]**(ell+1)
        du0 = (ell+1)*r[0]**ell
        """

        rhs = 2*(veff - eval)*r**2
        u_inf = np.exp(-(-2*eval)**(0.5)*r[-1])/r[-1]**(0.5)
        du_inf = -(-2*eval)**(0.5)*u_inf - 0.5*u_inf/r[-1]
        u0 = r[0]**(ell+0.5)
        du0 = (ell+0.5)*r[0]**(ell-0.5)
        """
        if irm < 3 or nr - irm < 3:
            u = numerov_driver(np.zeros(nr),rhs,u0,du0,spc)
            delta = u[-1] # can only require that this satisfies the r --> infinity bdy condition
        else:
            ug = numerov_driver(np.zeros(nr-irm),rhs[irm:],u_inf,du_inf,-spc)
            nfdc = min(6,nr-irm-1)
            fdc = fdiff_coeffs('fwd',nfdc)/spc
            dug_rm = np.dot(fdc,ug[:nfdc+1])#(ug[1]-ug[0])/spc#

            ul = numerov_driver(np.zeros(irm+1),rhs[:irm+1],u0,du0,spc)
            nfdc = min(6,irm)
            fdc = -fdiff_coeffs('fwd',nfdc)/spc
            dul_rm = np.dot(fdc,ul[-1:-nfdc-2:-1])#(ul[-1]-ul[-2])/spc#

            delta = dug_rm/ug[0] - dul_rm/ul[-1] # logarithmic derivative

            u = np.zeros(nr)
            if ul[-1] < 0:
                scale_out = ug[0]/ul[-1]
                scale_in = 1.0
            elif ug[0] < 0:
                scale_out = 1.0
                scale_in = ul[-1]/ug[0]
            else:
                scale_out = ug[0]/ul[-1]
                scale_in = 1.0
            u[:irm] = ul[:-1]*scale_out
            u[irm:] = ug*scale_in
        return u,delta

    neig = 0
    eigvals = np.zeros(0)
    eigvecs = np.zeros((0,nr))
    eig_l = np.zeros(0,dtype='int')
    for ell in np.arange(0,max_ell+1,1):
        """
        epsl = np.arange(-2,0,0.1)
        for aneps in epsl:
            eps = aneps
            for isrc in range(30):
                _,ld1=log_deriv(eps,ell)
                if abs(ld1) < 1.e-6:
                    break
                _,ld2=log_deriv(eps-1.e-3,ell)
                appd = (ld1-ld2)/1.e-3
                if abs(appd)>0.0:
                    eps -= ld1/appd
                else:
                    break
            print(ell,eps,ld1)
        continue


        epl=np.arange(-0.505,0.0,.001)
        fun = np.zeros(epl.shape[0])
        for i,eps in enumerate(epl):
            _,fun[i] = log_deriv(eps,ell)
        ind  = np.argmin(np.abs(fun))
        fun = np.sign(fun)
        oval = fun[0]
        reg_l = []
        for ival,val in enumerate(fun):
            if val*oval <= 0.0:
                reg_l.append([ival-1,ival])
            oval = val
        """

        ham_diag = 1.0/spc**2 + vs + ell*(ell+1.0)/(2*r**2)
        ham_udiag = -1.0/(2*spc**2)*np.ones(nr-1)
        # first estimate of eigenvalues
        epst = eigh_tridiagonal(ham_diag,ham_udiag,eigvals_only=True,select='v',select_range=(-1e3,0.0))
        #print(ell,epst)
        for eps in epst: # Newton-Raphson method for finding zeros
            for isrc in range(100):
                _,ld1=log_deriv(eps,ell)
                if abs(ld1) < 1.e-6:
                    eigvals = np.append(eigvals,eps)
                    eig_l = np.append(eig_l,ell)
                    u,_ = log_deriv(eps,ell)
                    eigvecs = np.vstack((eigvecs,u))
                    neig += (2*ell+1.0)
                    break
                _,ld2=log_deriv(eps-1.e-6,ell)
                appd = (ld1-ld2)/1.e-6
                if abs(appd)>0.0:
                    eps -= ld1/appd
                else:
                    break
        #if neig >= nelec/2.0 and ell >= 3:
        #    break
        """
        def res_log_deriv(en):
            _,val = log_deriv(en,ell)
            return val

        for areg in reg_l:
            eps,good_eps = bisect(res_log_deriv,(epl[areg[0]],epl[areg[1]]))
            if abs(good_eps) < 1.e-2:
                eigvals = np.append(eigvals,eps)
                eig_l = np.append(eig_l,ell)
                u,_ = log_deriv(eps,ell)
                eigvecs = np.vstack((eigvecs,u[:]))
                neig += (2*ell+1.0)

        if neig >= nelec/2.0 and ell >= 3:
            break
        """

    def get_energy(eigs,eigvs,ells):
        nu = np.zeros(nr)
        nd = np.zeros(nr)
        gnu = np.zeros(nr)
        gnd = np.zeros(nr)
        gn = np.zeros((2,nr))
        lnu = np.zeros(nr)
        lnd = np.zeros(nr)
        tau_up = np.zeros(nr)
        tau_dn = np.zeros(nr)
        eps_up = np.zeros(0)
        neig = 0
        ang_grad = grad_sph_avh_sph_harm(max_ell)
        for ielec in range(len(eigs)):
            ell = ells[ielec]
            eps = eigs[ielec]
            u = eigvs[ielec]
            norm = normalization(u)
            phi = u/(norm*r)
            phi2 = np.abs(phi)**2
            du_r = np.zeros(nr)
            for ir in range(nr):
                if ir < 3:
                    du_r[ir] = np.dot(fwd_diff,u[ir:ir+7])
                elif ir > nr - 4:
                    du_r[ir] = np.dot(-fwd_diff,u[ir:ir-7:-1])
                else:
                    du_r[ir] = np.dot(cen_diff,u[ir-3:ir+4])
            dphi_r = (du_r - u/r)/(norm*r)
            dphi_ang = (4*pi)**(0.5)*phi*ang_grad[ell]/r
            phi_lap = 2*(vs-eps)*phi

            multiplicity = 2*ell+1.0
            occ = min(nelec/2-neig,multiplicity)
            neig += occ
            tau_up += 0.5*occ*(dphi_r**2+dphi_ang**2)#occ*(eps - vs)*phi2#
            nu += occ*phi2
            gn[0] += 2*occ*phi*dphi_r
            """
                gn is a 2 x N_r array
                The first row of gn stores the radial derivatives, sum 2*Re( phi* d phi/dr)
                and the second stores the polar angle derivatives, sum 2*Re( phi* d phi/d theta)
                we're ignoring the d/d phi (azimuth) part
                But if the density is spherically averaged, only take the radial part of the derivative
            """
            #gn[1] += 2*occ*phi*dphi_ang
            #lnu += 2*occ*(dphi_r**2 + dphi_ang**2 + phi*phi_lap)
            """
                Same for the laplacian of the density:
                lap n(r) = div grad(n(r)) = 1/r**2 d/dr [r**2 * n'(r)] = 2*n'(r)/r + n''(r)
            """
            d2_phi_dr2 = (2*vs + ell*(ell+1)/r**2 - 2*eps)*phi - 2*du_r/(norm*r**2) + 2*phi/r**2
            lnu += 2*occ*(2*phi*dphi_r/r + dphi_r**2 + phi*d2_phi_dr2)

            eps_up = np.append(eps_up,eps)
            if neig == nelec/2:
                break

        nd[:] = nu

        gnu = (gn[0]**2 + gn[1]**2)**(0.5)
        gnd[:] = gnu
        gnt = 2*gnu
        lnd[:] = lnu

        tau_dn[:] = tau_up

        eps_dn = eps_up[:]
        etot = total_energy(r,rad_wg,nu,nd,tau_up,tau_dn)

        return nu,nd,gnu,gnd,gnt,lnu,lnd,eps_up,eps_dn,tau_up,tau_dn,etot

    cargs = np.argsort(eigvals)
    eigvals = eigvals[cargs]
    eigv2 = eigvecs[cargs]
    eigvecs = eigvecs[cargs]
    eig_l = eig_l[cargs]
    if settings.enforce_order == 'always':
        eigvals,eigvecs,eig_l=order_eigenvalues(eigvals,eigvecs,eig_l,settings.ell_order)
    elif settings.enforce_order == 'near_degen':
        diffs = np.abs(eigvals[1:]-eigvals[:-1])#/np.abs(eigvals[1:]+eigvals[:-1])
        if np.any(diffs<1.e-2):
            nu,nd,gnu,gnd,gnt,lnu,lnd,eps_up,eps_dn,tau_up,tau_dn,etot = get_energy(eigvals,eigvecs,eig_l)
            eigvals,eigvecs,eig_l=order_eigenvalues(eigvals,eigvecs,eig_l,settings.ell_order)
            nu2,nd2,gnu2,gnd2,gnt2,lnu2,lnd2,eps_up2,eps_dn2,tau_up2,tau_dn2,etot2 = get_energy(eigvals,eigvecs,eig_l)
            if etot2[0] < etot[0]:
                return nu2,nd2,gnu2,gnd2,gnt2,lnu2,lnd2,eps_up2,eps_dn2,tau_up2,tau_dn2,etot2
            else:
                return nu,nd,gnu,gnd,gnt,lnu,lnd,eps_up,eps_dn,tau_up,tau_dn,etot
    nu,nd,gnu,gnd,gnt,lnu,lnd,eps_up,eps_dn,tau_up,tau_dn,etot = get_energy(eigvals,eigvecs,eig_l)

    return nu,nd,gnu,gnd,gnt,lnu,lnd,eps_up,eps_dn,tau_up,tau_dn,etot

def hamiltonian(r,spc,rad_wg,nu,nd):

    def aug_dens(worb):
        """
            returns square of normalized KS orbital
            radial wavefunction R(r) = u(r)/r
            normalization int_0^inf |R(r)|^2 r^2 dr = int_0^inf |u(r)|^2 dr = 1
            one-electron wavefunction phi_i(r) = R(r)/sqrt(4 pi)
        """
        orb2 = np.abs(worb)**2
        orb2_norm = 4*pi*spc*np.sum(orb2) # omitting factors of 4 pi and r**2
        return orb2/(orb2_norm*r**2) # restoring those factors
        #orb2_norm = 4*pi*spc*np.sum(rwg*r**2*orb2)
        #return orb2/(orb2_norm*r)

    nu_new = np.zeros(nr)
    eps_up = np.zeros(0)

    tau_up = np.zeros(nr)

    if nelec %2 == 0:
        vs,_ = ks_potential(r,spc,rad_wg,nu,nd)
        ham_diag = 1.0/spc**2 + vs
        ham_udiag = -1.0/(2*spc**2)*np.ones(nr-1)
        eps,phi = eigh_tridiagonal(ham_diag,ham_udiag)
        #eps,phi = eigh(ham)#,b=rmat)
        if nelec > 2:
            eps_p,phi_p = eigh_tridiagonal(ham_diag+3.0/r**2,ham_udiag)#eigh(ham_p)#,b=rmat)
        else:
            eps_p = 1e20*np.ones(1)
        if nelec > 10:
            eps_d,phi_d = eigh_tridiagonal(ham_diag+5.0/r**2,ham_udiag)#eigh(ham_d,b=rmat)
        else:
            eps_d = 1e20*np.ones(1)
        js = 0
        ip = 0
        id = 0
        neig = 0
        for ielec in range(int(nelec/2)):
            next_orb = min(eps[js],eps_p[ip],eps_d[id])
            if eps[js] == next_orb:
                norb2 = aug_dens(phi[:,js])
                ell = 0.0
                js += 1
            elif eps_p[ip] == next_orb:
                norb2 = aug_dens(phi_p[:,ip])
                ell = 1.0
                ip += 1
            elif eps_d[id] == next_orb:
                norb2 = aug_dens(phi_d[:,id])
                ell = 2.0
                id += 1
            vcen = ell*(ell+1.0)/r**2
            multiplicity = 2*ell+1.0
            occ = min(nelec/2-neig,multiplicity)
            neig += occ
            tau_up += occ*(next_orb - vs - vcen)*norb2
            nu_new += occ*norb2
            eps_up = np.append(eps_up,next_orb)
            if nelec/2-neig==0:
                break
        nd_new = nu_new[:]
        eps_dn = eps_up[:]
        tau_dn = tau_up[:]

    elif nelec %2 == 1:
        nd_new = np.zeros(nr)
        eps_dn = np.zeros(0)
        tau_dn = np.zeros(nr)

        vs_up,vs_dn = ks_potential(r,spc,nu,nd)
        ham_up = discrete_laplacian(r,spc,fac=-0.5)
        ham_dn = ham_up[:,:]

        for ind in range(nr):
            ham_up[ind,ind] += vs_up[ind]
            ham_dn[ind,ind] += vs_dn[ind]
        eps_up,phi_up = eigh(ham_up)
        eps_dn,phi_dn = eigh(ham_dn)
        iup = 0
        idn = 0
        for ielec in range(nelec):

            if eps_up[iup] < eps_dn[idn]:
                nu_new += aug_dens(phi_up[:,iup])
                eps_up = np.append(eps_up,eps_up[iup])
                iup += 1
            elif eps_up[iup] > eps_dn[idn]:
                nd_new += aug_dens(phi_dn[:,idn])
                eps_dn = np.append(eps_dn,eps_dn[idn])
                idn += 1
            else:
                # this probably never happens, but just to be sure, include it here
                nu_new += aug_dens(phi_up[:,iup])
                eps_up = np.append(eps_up,eps_up[iup])
                iup += 1
                nd_new += aug_dens(phi_dn[:,idn])
                eps_dn = np.append(eps_dn,eps_dn[idn])
                idn += 1

            if iup + idn == nelec:
                # need to include this in case up and down eps are degenerate
                break
    nu_new = reg_n(nu_new)
    nd_new = reg_n(nd_new)
    etot = total_energy(r,rad_wg,nu_new,nd_new,tau_up,tau_dn)

    return nu_new,nd_new,eps_up,eps_dn,tau_up,tau_dn,etot
