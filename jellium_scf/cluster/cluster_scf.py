import numpy as np
from scipy.optimize import minimize_scalar,minimize
from os import path,system
import matplotlib.pyplot as plt

import settings,constants
from basis.sto import init_orbitals,sto_norm
from cluster.cluster_hamiltonian import sto_ham_init,sto_ham,numerov_hamiltonian,sto_dens_init
from dft.thomas_fermi_density import tf_dens_scf
from utilities.density_mixing import modified_broyden,initiate_modified_broyden,linear_mixing
from utilities.gauss_quad import gauss_quad

pi = constants.pi

rs = settings.rs
nelec = settings.N
kf = (9*pi/4.0)**(1.0/3.0)/rs
nbulk = 3.0/(4.0*pi*rs**3)

if settings.method=='STO':
    nr = settings.rgrid[0] + settings.rgrid[1]
elif settings.method=='numerov':
    nr = settings.rgrid[0]

Rc = rs*nelec**(1.0/3.0)
if settings.system == "cluster":
    #r_max = 6*Rc
    # this is a least-squares fit to N = 2,8,18,20,34,40,58,92 data
    # attempts to give a reasonable cutoff, where n ~< 10**(-14) at the farthest point
    fac = 1.0 + 0.2*(4-rs)
    if rs < 2:
        fac *= 1.5
    elif rs > 5:
        fac *= 1.25
    r_max = fac*Rc/(0.06397951752837527*np.log(1.0*nelec)+0.12170501871533101)
    r_max = max(2*Rc,r_max)
    r_min = 1.e-6
    self_en_pos = 3*nelec**2/(5.0*Rc)
elif settings.system == "atom":
    r_max = 6.0
    r_min = 1.e-2/nelec

"""
def init_dens_tf(r,dr):

    n0 = 4*nelec/Rc**3*np.exp(-2*r/Rc)/(4*pi)

    def unp_ks_pot(n):
        vs,_ = ks_potential(r,dr,n/2.0,n/2.0)
        return vs

    ntf = tf_dens_scf(r,dr*rwg,unp_ks_pot,nelec,n0)
    return ntf
"""

def trial_dens(r,rwg,zeta_l):

    def gto_s(r,zeta):
        return (2*zeta/pi)**(0.75)*np.exp(-zeta*r**2)

    dens = np.zeros(nr)
    tau = np.zeros(nr)
    occ = nelec/8.0
    for zeta in zeta_l:
        orb = gto_s(r,zeta)
        dens += occ*orb**2
        tau += occ*2*(zeta*r*orb)**2
    etot = total_energy(r,rwg,dens,dens,tau,tau)
    return dens,etot[0]

def init_dens(r,rwg):
    """
    def wrap_res(zl):
        _,gs= trial_dens(r,rwg,zl)
        return gs
    zl0 = np.asarray([1.1**i*0.01 for i in range(2)])
    bds = [(0.0,None) for i in range(len(zl0))]
    res = minimize(wrap_res,zl0,method='L-BFGS-B',bounds=bds,tol=1.e-6)
    opt_zl = res.x
    n,_ = trial_dens(r,rwg,opt_zl)
    return n
    """
    def trial(rate):
        if rs < 2:
            fac = 1.05
        else:
            fac = 1.2
        y = (r - 0.9*Rc)/rate
        return fac*nbulk*(1.0 - np.tanh(y))/2.0
    def res(rate):
        return np.abs(nelec - np.sum(rwg*trial(rate)))
    irate = minimize_scalar(res,bracket=(1.e-3,1))
    return trial(irate.x)


def sto_dens(r,config_l,occ_l):
    """
    if settings.system=="cluster":
        scl = 5*nelec
        orbs = init_orbitals(r/scl,config_l)
        orbs /= scl**(3.0/2.0)
    elif settings.system=="atom":
    """
    orbs = init_orbitals(r,config_l)
    dens = np.zeros(nr)
    for iorb,anorb in enumerate(orbs):
        dens += occ_l[iorb]*anorb**2
    return dens,orbs

def init_atomic_dens(r,rwg):
    if settings.system == "atom":
        # 1s, 2s, 2p, 3s, 3p, 4s, 3d,...
        hydro_l = [(1,0), # He, Z = 2
        (2,0),(2,1), # Ne, Z = 10
        (3,0),(3,1), # Ar, Z = 18
        (4,0),(3,2),(4,1), # Kr, Z = 36
        (5,0),(4,2),(5,1), # Xe, Z = 54
        (6,0),(4,3),(5,2),(6,1), # Rn, Z = 86
        (7,0),(5,3),(6,2),(7,1), # Og, Z = 118
        (8,0),(5,4),(6,3),(7,2),(8,1), # Z = 168
        (9,0),(6,4),(7,3),(8,2),(9,1), # Z = 218
        (10,0),(7,5),(8,4),(9,3),(10,2),(11,1)] # Z = 290
    elif settings.system == "cluster":
        hydro_l = [(1,0),(1,1),(1,2),(2,0),(1,3),(2,1),(1,4),(2,2),(3,0)]
    config_l = []
    occ_l = []
    occ = 0
    qns = [-1,-1,-1,-1,-1]
    zl0 = []
    if settings.system=="cluster":
        alp = (nbulk**(0.5)/2)**(2.0/3)#nelec/4.0
    else:
        alp = nelec
    for ielec in range(nelec):
        n,ell = hydro_l[ielec]
        qns[ell] = max(qns[ell],n)
        mult = 2*(2*ell+1.0)
        tocc = min(nelec-occ,mult)
        if occ >= nelec:
            occ_l.append(0.0)
        else:
            occ_l.append(tocc)
        config_l.append((n,alp,ell,sto_norm(n,alp)))
        zl0.append(alp)
        occ += tocc
        if occ >= nelec and len(config_l)>=2:
            break
    if not settings.minimal_basis:
        plc = len(config_l)
        for iconf in range(plc):
            n,z,ell,norm = config_l[iconf]
            for fac in [1.1]:#,1.5]:
                occ_l.append(0.0)
                config_l.append((n,z*fac,ell,sto_norm(n,z*fac)))
                zl0.append(z*fac)
    """
    for ell,maxn in enumerate(qns):
        if maxn == -1:
            maxell = ell
            break
        # add a few extra basis functions for each ell quantum number

        for ann in range(1,maxn+2):
            if settings.system=="cluster":
                falp = 0.9**ann*alp
            else:
                falp = (0.9)**ann*alp
            config_l.append((ann,falp,ell,sto_norm(ann,falp)))
            occ_l.append(0.0)

        for incn in range(1):
            falp = 1.5*(incn+1)*alp
            config_l.append((maxn+incn+1,alp,ell,sto_norm(maxn+incn+1,alp)))
            occ_l.append(0.0)
            for ann in range(1,maxn+3):
                config_l.append((ann,falp,ell,sto_norm(ann,falp)))
                occ_l.append(0.0)

    if settings.system=="cluster":
        for inc in range(1,3):
            falp = 0.5**(inc-1)*alp
            for inc_ell in range(inc):
                config_l.append((maxell+inc,falp,maxell+inc_ell,sto_norm(maxell+inc,falp)))
                occ_l.append(0.0)
        """
    if settings.system=="atom":
        def wrap_res(zl,ret_confl=False):
            cl = []
            for iconf in range(len(zl)):
                n,_,ell,_ = config_l[iconf]
                cl.append((n,zl[iconf],ell,sto_norm(n,zl[iconf])))
            if ret_confl:
                return cl
            else:
                _,en=sto_dens_init(cl,r,rwg)
                return en
        res = minimize(wrap_res,zl0,method='L-BFGS-B',bounds=[(0.5,None) for i in range(len(zl0))],tol=1.e-4)
        config_l = wrap_res(res.x,ret_confl=True)
    dens,orbs = sto_dens(r,config_l,occ_l)
    return dens,occ_l,config_l,orbs

def ham_scf(r,rad_wg,nu,nd,orbs,confl,cfl,iopt):

    etot_last = 1e20
    if settings.system=="atom":
        mp = 0.75
    elif settings.system == "cluster":
        mp = 0.75

    for iscf in range(settings.max_scf_steps['HKS']):

        if iscf == 0 and settings.density_mixing=='mod_broyden':
            F,dF,u_vec,a_mat,mix_wg = initiate_modified_broyden(nr)
            if nelec %2 == 1:
                F2,dF2,u_vec2,a_mat2,mix_wg2 = initiate_modified_broyden(nr)

        kevc,overlap,chi_mat,chi_grad_r_mat,chi_grad_ang_mat,chi_lap_mat = sto_ham_init(confl,r)

        nu1,nd1,e1_up,e1_dn,tu,td,etot_v,cmat = sto_ham(r,rad_wg,nu+nd,orbs,confl,kevc,overlap,chi_mat,chi_grad_r_mat,chi_grad_ang_mat,chi_lap_mat)
        etot = etot_v[0]

        vt = np.sum(etot_v[2:])/etot_v[1]
        if settings.system=='cluster':
            vt += self_en_pos/etot_v[1]
        #cfl.write(('{:},'*8 + '{:}\n').format(iscf,etot,etot/etot_last - 1,*etot_v[1:],vt))
        if settings.density_mixing == 'none':
            nu_new = nu1
        elif settings.density_mixing=='linear':
            nu_new = linear_mixing(nu,nu1,mixpar=mp)
        elif settings.density_mixing=='mod_broyden':
            nu_new,F,dF,mix_wg,u_vec,a_mat = modified_broyden(rad_wg,nu,nu1,F,dF,u_vec,a_mat,mix_wg,iscf)

        N1 = np.sum(rad_wg*nu1)
        Nnew = np.sum(rad_wg*nu_new)
        if abs(N1 - nelec/2) > 1.e-6 or abs(Nnew - nelec/2) > 1.e-6:
            print(iscf,N1,Nnew,np.sum(rad_wg*nu1),np.sum(rad_wg*nu))
            print(confl)
            exit()

        if abs(etot - etot_last)<settings.scf_conv['HKS']*abs(etot_last) and iscf > settings.min_scf['HKS']:
            cfl.write(('{:},'*8 + '{:}\n').format(iscf,etot,etot/etot_last - 1,*etot_v[1:],vt))
            print(etot,etot/etot_last - 1,np.sum(rad_wg*(nu1+nd1)),vt)
            break
        etot_last = etot

        nu = nu_new
        if nelec %2 == 0:
            nd = nu_new
        else:
            if settings.density_mixing == 'none':
                nd_new = nd1
            elif settings.density_mixing == 'linear':
                nd_new = linear_mixing(nd,nd1)
            elif settings.density_mixing == 'mod_broyden':
                nd_new,F2,dF2,mix_wg2,u_vec2,a_mat2 = modified_broyden(rad_wg,nd,nd1,F2,dF2,u_vec2,a_mat2,mix_wg2,iscf)
            nd = nd_new
    return nu1,nd1,etot_v,cmat

def optimize_sto_exponents(r,rad_wg,exps,confl_in,occup,conv_file,dens_fl,orb_fl,iopt,wdir):
    confl = []
    for iconf,aconf in enumerate(confl_in):
        nq,_,ell,_ = aconf
        new_exp = exps[iconf]
        confl.append((nq,new_exp,ell,sto_norm(nq,new_exp)))
    n0,new_orbs = sto_dens(r,confl,occup)
    if settings.system=="cluster":
        n0 = init_dens(r,rad_wg)
    nu,nd,etotv,cmat = ham_scf(r,rad_wg,n0/2.0,n0/2.0,new_orbs,confl,conv_file,iopt)
    np.savetxt(wdir+'/'+dens_fl,np.transpose((r,nu,nd)),delimiter=',',header='r,n_up,n_dn')
    np.savetxt(wdir+'/'+orb_fl,np.transpose(cmat),delimiter=',')
    return nu,nd,etotv

def numerov_scf(r,dr,rad_wg,n0,cfl):

    etot_last = 1e20
    #ochange = 1.0
    if settings.system=="atom" and settings.density_mixing=='linear':
        mp = 0.5
    elif settings.system == "cluster" and settings.density_mixing=='linear':
        mp = 0.25
        #if nelec >= 100:
        #    mp = 0.1
        if nelec >= 92:
            mp = 0.1
        elif nelec >= 58 and rs > 5:
            mp = 0.1

    nu = n0/2.0
    nd = n0/2.0

    for iscf in range(settings.max_scf_steps['HKS']):

        if iscf == 0 and settings.density_mixing=='mod_broyden':
            F,dF,u_vec,a_mat,mix_wg = initiate_modified_broyden(nr)
            if nelec %2 == 1:
                F2,dF2,u_vec2,a_mat2,mix_wg2 = initiate_modified_broyden(nr)

        nu1,nd1,gnu,gnd,gnt,lnu,lnd,e1_up,e1_dn,tu,td,etot_v = numerov_hamiltonian(r,dr,rad_wg,nu,nd)
        etot = etot_v[0]
        vt = np.sum(etot_v[2:])/etot_v[1]
        #cchange = abs(1.0 - etot/etot_last)

        if settings.system=='cluster':
            vt += self_en_pos/etot_v[1]

        if settings.system=='cluster':
            val = etot/nelec
        else:
            val = etot
        print(('{:},  {:.4f},  {:.4f},  {:.4e},  {:.1f},  {:.4f}').format(iscf,val,val*constants.Eh_to_eV,etot/etot_last - 1,np.sum(rad_wg*(nu1+nd1)),vt))

        cfl.write(('{:},'*8 + '{:}\n').format(iscf,etot,etot/etot_last - 1,*etot_v[1:],vt))
        if settings.density_mixing == 'none':
            nu_new = nu1
        elif settings.density_mixing=='linear':
            nu_new = linear_mixing(nu,nu1,mixpar=mp)
        elif settings.density_mixing=='mod_broyden':
            nu_new,F,dF,mix_wg,u_vec,a_mat = modified_broyden(rad_wg,nu,nu1,F,dF,u_vec,a_mat,mix_wg,iscf)

        N1 = np.sum(rad_wg*nu1)
        Nnew = np.sum(rad_wg*nu_new)
        if abs(N1 - nelec/2) > 1.e-6 or abs(Nnew - nelec/2) > 1.e-6 and iscf > 0:
            print(iscf,N1,Nnew,np.sum(rad_wg*nu1),np.sum(rad_wg*nu))
            exit()

        if abs(etot - etot_last)<settings.scf_conv['HKS']*abs(etot_last) and iscf > settings.min_scf['HKS']:
            break
        #ochange = cchange
        etot_last = etot

        nu = nu_new
        if nelec %2 == 0:
            nd = nu_new
        else:
            if settings.density_mixing == 'none':
                nd_new = nd1
            elif settings.density_mixing == 'linear':
                nd_new = linear_mixing(nd,nd1)
            elif settings.density_mixing == 'mod_broyden':
                nd_new,F2,dF2,mix_wg2,u_vec2,a_mat2 = modified_broyden(rad_wg,nd,nd1,F2,dF2,u_vec2,a_mat2,mix_wg2,iscf)
            nd = nd_new

    return nu1,nd1,gnu,gnd,gnt,lnu,lnd,tu,td,etot_v


def scf():

    if settings.system == 'cluster':
        wdir = './data/'+settings.system+'/rs_'+str(rs)+'/N_'+str(nelec)+'_rs_'+str(rs)
    elif settings.system == 'atom':
        wdir = './data/'+settings.system+'/N_'+str(nelec)
    for adir in [wdir,'./grids']:
        if not path.isdir(adir):
            system(('mkdir -p {:}').format(adir))

    if settings.method=='STO':

        grid_fl = './grids/gauss_legendre_'+str(settings.rgrid[0])+'_pts.csv'
        supp_grid_fl = './grids/gauss_legendre_'+str(settings.rgrid[1])+'_pts.csv'
        for igrid,agrid in enumerate([grid_fl,supp_grid_fl]):
            if not path.isfile(agrid):
                gauss_quad(settings.rgrid[igrid])
        t1wg,t1 = np.transpose(np.genfromtxt(grid_fl,delimiter=',',skip_header=1))
        t1 = 0.5*r_max*(t1 + 1)
        t1wg = 0.5*r_max*t1wg
        t2wg,t2 = np.transpose(np.genfromtxt(supp_grid_fl,delimiter=',',skip_header=1))
        t2 = 0.5*(10*r_max-r_max)*t2 + 0.5*(10*r_max + r_max)
        t2wg *= 0.5*(10*r_max-r_max)
        r = np.concatenate((t1,t2))
        rwg = np.concatenate((t1wg,t2wg))
        rad_wg = 4*pi*rwg*r**2

        n0,occl,confl,orbs = init_atomic_dens(r,rad_wg)

    elif settings.method=='numerov':
        r = np.linspace(r_min,r_max,nr)
        dr = r[1]-r[0]
        rwg = np.ones(nr)
        rwg[[0,-1]]=0.5
        rad_wg = 4*pi*dr*rwg*r**2
        if settings.system=='atom':
            n0,_,_,_ = init_atomic_dens(r,rad_wg)
    else:
        raise ValueError('Uknown method in settings!',settings.method)


    if settings.system=="cluster":

        n0 = init_dens(r,rad_wg)

        fname = 'cluster_rs_'+str(rs)+'_N='+str(settings.N)+'.csv'
        fname2 = 'cluster_density_rs_'+str(rs)+'_N='+str(settings.N)+'.csv'
        fname3 = 'cluster_orbitals_rs_'+str(rs)+'_N='+str(settings.N)+'.csv'
    elif settings.system=="atom":
        fname = 'atom_N='+str(settings.N)+'.csv'
        fname2 = 'atom_density_N='+str(settings.N)+'.csv'
        fname3 = 'atom_orbitals_N='+str(settings.N)+'.csv'

    conv_file = open(wdir+'/'+fname,'w+')
    conv_file.write('SCF iter., E[n] (hartree), E[n_i]/E[n_(i-1)]-1, T, V_ext, U_H, Ex, Ec, Virial Theorem \n')

    if settings.method=='STO':

        def wrap_opt_fun(expl):
            _,_,env= optimize_sto_exponents(r,rad_wg,expl,confl,occl,conv_file,fname2,fname3,0,wdir)
            return env[0]

        bds = [(0.5,None) for i in range(len(confl))]
        init_exps = np.zeros(len(confl))
        conf_to_file = []
        lqn_str = ['s','p','d','f','g','h']
        for iconf,aconf in enumerate(confl):
            pqn,init_exps[iconf],lqn,_ = aconf
            conf_to_file.append(('{:}{:}').format(pqn,lqn_str[lqn]))
        res = minimize(wrap_opt_fun,init_exps,method='L-BFGS-B',bounds=bds,tol=1.e-6)
        bexp = res.x
        if not res.success:
            print(res)
        cmat = np.genfromtxt(wdir+'/'+fname3,delimiter=',')
        norbs_tup = cmat.shape
        if len(norbs_tup)>1:
            norbs = norbs_tup[1]
        else:
            norbs = 1
        with open(wdir+'/'+fname3,'w+') as ofl:
            ofl.write('STO, STO exponent, Orbital 1 coeff., Orbital 2 coeff.,...\n')
            for ind in range(len(confl)):
                if norbs == 1:
                    ofl.write(('{:},{:},'+'{:}\n').format(conf_to_file[ind],bexp[ind],cmat[ind]))
                else:
                    ofl.write(('{:},{:},'+'{:},'*(norbs-1) +'{:}\n').format(conf_to_file[ind],bexp[ind],*cmat[ind,:]))

        dat=np.genfromtxt(wdir+'/'+fname2,delimiter=',',skip_header=1)
        r = dat[:,0]
        nu = dat[:,1]

    elif settings.method=='numerov':

        nu,nd,gnu,gnd,gnt,lnu,lnd,tu,td,etot_v=numerov_scf(r,dr,rad_wg,n0,conv_file)
        np.savetxt(wdir+'/'+fname2,np.transpose((r,rad_wg,nu,nd,gnu,gnd,gnt,lnu,lnd,tu,td)),delimiter=',',header='r,rad_wg,n_up,n_dn,|grad n_up|, |grad n_dn|, |grad n|,lap n_up,lap n_dn,tau_up,tau_dn')
        print('integral of lap n',np.sum(rad_wg*lnu))
    conv_file.close()

    print('N',np.sum(rad_wg*2*nu),'n_up(inf)',nu[-1])
    return
    if settings.system == "cluster":
        plt.plot(r,2*nu/nbulk)
        tr = np.linspace(0.0,1.5*Rc,2000)
        step = np.ones(tr.shape)
        mask = tr>Rc
        step[mask]=0.0
        plt.plot(tr,step)
        plt.xlim([0.0,1.5*Rc])
    elif settings.system == "atom":
        plt.plot(r,2*nu*r**2)
        plt.xlim([0.0,r_max])
    plt.ylim([0.0,plt.ylim()[1]])
    plt.show()

    return


if __name__=="__main__":

    scf()
