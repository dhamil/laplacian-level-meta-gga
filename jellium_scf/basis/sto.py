import numpy as np
#class STO:
import constants
pi = constants.pi

def sto_norm(n,z):
    # (2n)! precomputed for 0 <= n <= 10
    facl = [1.0,2,24,720.0,40320.0,3628800, 479001600, 87178291200, 20922789888000, 6402373705728000, 2432902008176640000]
    norm = (2.0*z)**(n+0.5)/(facl[n])**(0.5)
    return norm

def factorial(x):
    fac = [1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800, 39916800,
    479001600, 6227020800, 87178291200, 1307674368000, 20922789888000,
    355687428096000, 6402373705728000, 121645100408832000,
    2432902008176640000] # 0!, 1!, 2!, 3!, ... , 20! precomputed
    return fac[x]

def sto_radial(r,n,zeta): # r is numpy array, n and zeta are scalars; generates Slater-type orbitals
    chi = sto_norm(n,zeta)*r**(n-1.0)*np.exp(-zeta*r)
    chi_grad = ((n-1.0)/r - zeta)*chi
    chi_lap = (n*(n-1.0)/r**2 - 2*zeta*n/r + zeta**2)*chi
    return chi, chi_grad, chi_lap

def sto_sph(r,n,zeta,ell):
    chi,grad,lap = sto_radial(r,n,zeta)
    ang,ang_grad,ang_lap = sph_avg_harm(ell)
    #return chi*ang,(grad*ang)**2 + (chi*ang_grad/r)**2,lap*ang + chi*ang_lap/r**2
    return chi*ang,grad*ang,chi*ang_grad/r,lap*ang + chi*ang_lap/r**2

def sph_avg_harm(ell): # NB, density and all other variables are spherically-averaged
    if ell == 0: # when using this function
        return 1.0/(4*pi)**(0.5),0.0,0.0
    elif ell == 1:
        cons = (3.0/(4.0*pi))**(0.5)
        return  1.0/(4*pi)**(0.5),1.0/(2*pi)**(0.5),-2.0/(4*pi)**(0.5)
    elif ell == 2:
        cons = (5.0/(16*pi))**(0.5)
        return 1.0/(4*pi)**(0.5),(3.0/(2*pi))**(0.5),-6.0/(4*pi)**(0.5)

def rm_mat_elts(par2,par1,order):
    assert order >= -2, "Must have M>=-2 to calculate <n'z'a'|r**M|nza>"
    n1,z1,l1,norm1 = par1
    n2,z2,l2,norm2 = par2
    if l1 != l2:
        return 0.0
    numer = norm1*norm2*factorial(n1+n2+order)
    denom = (z1+z2)**(n1+n2+order+1)
    return numer/denom

def overlap(par2,par1):
    return rm_mat_elts(par2,par1,0)

def ke(par2,par1):
    n1,z1,l1,norm1 = par1
    n2,z2,l2,norm2 = par2
    if l1 != l2:
        return 0.0
    t1 = -0.5*z1**2*rm_mat_elts(par2,par1,0)
    t2 = z1*n1*rm_mat_elts(par2,par1,-1)
    t3 = -0.5*(n1*(n1-1) - l1*(l1 + 1))*rm_mat_elts(par2,par1,-2)
    return t1+t2+t3

def vcoul(par2,par1):
    return rm_mat_elts(par2,par1,-1)

def init_orbitals(r,orb_l):
    orbs = np.zeros((len(orb_l),r.shape[0]))
    for ind,orb_pars in enumerate(orb_l):
        n,z,ell,_ = orb_pars
        orbs[ind],_,_,_ = sto_sph(r,n,z,ell)
    return orbs

if __name__== "__main__":
    n = 1
    l = 0
    z = 1
    norm = sto_norm(n,z)
    pars = (n,z,l,norm)
    print(ke(pars,pars),overlap(pars,pars))
    print(ke(pars,(2,1,1,norm)),overlap(pars,(2,1,1,norm)))
