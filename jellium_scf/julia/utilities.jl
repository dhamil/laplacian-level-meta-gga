using LinearAlgebra,Plots

#=------------------------------------------------------------------------------
ordinary differential equations solvers
------------------------------------------------------------------------------=#

function rkck_o5_adaptive(bds,y0,ode;opts=nothing,args=[],kwargs=Dict())
    dir = sign(bds[2] - bds[1])
    neq = size(y0)[2]

    defaults = Dict("prec" => 1.e-6, "min_step" => (bds[2] - bds[1])/1.e6, "max_step" => (bds[2] - bds[1])/5.e3,
    "max_refine" => 5)
    defaults["max_steps"] = ceil((bds[2] - bds[1])/defaults["min_step"])
    defaults["step"] = dir*max(min(abs(bds[2] - bds[1])/5.e3,1.e-2),defaults["min_step"])
    if opts != nothing
        for (opt,val) in opts
            defaults[opt] = val
        end
    end

    clamp_step(h0) = dir*min(defaults["max_step"],max(defaults["min_step"],abs(h0)))
    function new_step(h0,sig,increase)
        if increase
            h1 = dir*min( maximum(0.9*abs(h0)*sig.^(-1/4)), 5*abs(h0))
        else
            h1 = dir*max(minimum(0.9*abs(h0)*sig.^(-1/5)), 0.1*abs(h0))
        end
        return clamp_step(h1)
    end

    x = [bds[1]]
    y = vcat(zeros(0,neq),y0)
    h = defaults["step"]
    yscl = abs.(y0)
    tmask = yscl.==0.0
    yscl[tmask] .= h
    mstep = convert(Int64,defaults["max_steps"])
    mref = convert(Int64,defaults["max_refine"])
    ty = zeros(neq)
    err = zeros(neq)

    for i = 1:mstep

        terr = defaults["prec"].*yscl

        for j = 1:mref
            #= loop to modify the step-size until error estimate is tolerable =#
            ty,err = runge_kutta_cash_karp_o5(x[i],y[[i],:],h,ode;args=args,kwargs=kwargs)
            sig = err./terr
            if maximum(sig) > 1
                #= if the error was too large for any equation, estimate a smaller step size and repeat =#
                h = new_step(h,sig,false)

            else
                #= if the error was tolerable, estimate next step size and proceed to the next iteration =#
                h = new_step(h,sig,true)
                break
            end
        end
        append!(x,x[i].+h)
        y = vcat(y,ty)

        if x[i] >= bds[2]
            break
        end

        for iter = 1:neq
            yscl[iter] = sum(abs.(y[:,[iter]]))./size(y)[1]
        end

    end
    return x,y
end

#=-----------------------------------------------------------------
Runge-Kutta driver using Cash-Karp parameters to estimate error
Cash-Karp parameters from Numerical Recipes in Fortran 77
only returns one step, as stepsize is assumed variable
-----------------------------------------------------------------=#

function runge_kutta_cash_karp_o5(x,y,h,ode;args=[],kwargs=Dict())
    neq = length(y)
    a_l = [0 1/5 3/10 3/5 1 7/8]
    b_m = [ 0 0 0 0 0 0 ; 1/5 0 0 0 0 0 ; 3/40 9/40 0 0 0 0 ;
    3/10 -9/10 6/5 0 0 0 ; -11/54 5/2 -70/27 35/27 0 0 ;
    1631/55296 175/512 575/13824 44275/110592 253/4096 0]
    #c4_l = [37/378 0 250/621 125/594 0 512/1771]
    c5_l = [2825/27648 0 18575/48384 13525/55296 277/14336 1/4]
    # cerr = c4_l - c5_l
    cerr = [-(277/64512), 0, 6925/370944, -(6925/202752), -(277/14336), 277/7084]
    k = zeros(6,neq)
    ny = zeros(1,neq)
    err = zeros(1,neq)
    for ind = 1:6
        ty = zeros(neq)
        tx = x + a_l[ind]*h
        for iter = 1:neq
            ty[iter] = y[iter] + dot(b_m[ind,:],k[:,iter])
        end
        k[ind,:] = h*ode(tx,ty,args...;kwargs...)
    end
    for iter = 1:neq
        ny[iter] = y[iter] + dot(c5_l,k[:,[iter]])
        err[iter] = abs(dot(cerr,k[:,[iter]]))
    end
    return ny,err

end

#=------------------------------------------------------------------------------
Finite difference routines
------------------------------------------------------------------------------=#

function finite_diff_coeffs(order,spc_l)

    # computes finite difference coefficients using
    # an algorithm from B. Fornberg, Math. Comp. 51, 699-706 (1988)
    #doi: 10.1090/S0025-5718-1988-0935077-0

    # Inputs:
    #     order is the order of the derivative, d^n/d x^n
    #     spc_l is a list of points x_m
    # Outputs:
    #     coefficients c_mn needed to evaluate derivative
    #     d^n f(x)/d x^n ~~ sum_m c_mn f(x_m)

    npts = length(spc_l)
    d = zeros((npts,npts,npts))
    d[1,1,1] = 1.0
    c1 = 1.0
    for t = 1:npts-1
        c2 = 1.0
        for nu = 0:t-1
            c3 = spc_l[t+1] - spc_l[nu+1]
            c2 *= c3
            for m = 0:min(t,order)
                d[t+1,nu+1,m+1] = spc_l[t+1]*d[t,nu+1,m+1]/c3
                if m > 0
                    d[t+1,nu+1,m+1] -= m*d[t,nu+1,m]/c3
                end
            end
        end
        for m = 0:min(t,order)
            d[t+1,t+1,m+1] = -c1/c2* spc_l[t]*d[t,t,m+1]
            if m > 0
                d[t+1,t+1,m+1] += c1/c2*m*d[t,t,m]
            end
        end
        c1 = c2
    end
    return d[npts,:,order+1]
end

function laplacian_1d(rank,spc; order=8)

    # central difference laplacian, appropriate for hermitian discrete Hamiltonian
    # coefficients from B. Fornberg, Math. Comp. 51, 699-706 (1988)
    #doi: 10.1090/S0025-5718-1988-0935077-0

    # Inputs:
    #    rank, of Hamiltonian matrix/symmetric/hermitian operator
    #    spc, spacing of points
    # Optional keyword inputs:
    #    order, gives accuracy of approximation using order+1 points
    # Outputs:
    #    Discrete 1D, linear laplacian operator in a rank x rank matrix

    if order + 1 > rank
        # must have rank >= number of points used in finite differences derivative
        error("Number of points in finite difference derivative cannot be
        greater than the rank!")
    end

    max_disp = convert(Int64,floor(order/2))
    if order == 8
        fdcoeff = [-1/560, 8/315, -1/5, 8/5, -205/72, 8/5, -1/5, 8/315, -1/560]
    elseif order == 6
        fdcoeff = [1/90, -3/20, 3/2, -49/18, 3/2, -3/20, 1/90]
    elseif order == 4
        fdcoeff = [-1/12, 4/3, -5/2, 4/3, -1/12]
    elseif order == 2
        fdcoeff = [1, -2, 1]
    else
        if mod(order,2) == 0
            bd = order/2
            fdcoeff = finite_diff_coeffs(2,collect(-bd:1:bd))
        else
            max_disp = Convert(Int64,floor((order+1)/2))
            fdcoeff = finite_diff_coeffs(2,collect(-order -1:1.0:order+1))
        end
    end

    del = zeros(rank,rank)

    for it = 1:rank
        max_lbd = it-max_disp
        c_lbd = 1
        if max_lbd < 1
            max_lbd = 1
            c_lbd += max_disp-it + 1
        end
        c_ubd = length(fdcoeff)
        min_ubd = it+max_disp
        if min_ubd > rank
            min_ubd = rank
            c_ubd -= it+max_disp-rank
        end
        del[it,max_lbd:min_ubd] = fdcoeff[c_lbd:c_ubd] ./spc
    end

    return del

end
