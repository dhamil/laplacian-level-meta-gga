using LinearAlgebra,DelimitedFiles

if !isdir("./grids")
    mkdir("./grids")
end

function gauss_quad(lvl;grid_type="legendre",alag=0.0)

    lvl = convert(Int64,floor(lvl))

    if grid_type=="legendre"
        fname = "gauss_legendre_" *string(lvl)*"_pts.csv"
    elseif grid_type=="laguerre"
        fname = "gauss_laguerre_"*string(alag)*"_alpha_"*string(lvl)*"_pts.csv"
        if alag < -1.0
            throw(DomainError(alag,"Generalized Laguerre grid requires alag > -1"))
        end
    elseif grid_type == "cheb"
        fname = "gauss_cheb_"*string(lvl)*"_pts.csv"
        # NIST DLMF Eqs. 3.5.22 and 3.5.23
        k = collect(1,1,lvl)
        x = @. cos((2k-1)*pi/(2lvl))
        wg = @. pi/lvl*(1 - x^2)^(0.5)
        writedlm("./grids/"*fname,transpose(grid,wg);header="point,weight")
        return
    end

    # algorithm from Golub and Welsch, Math. Comp. 23, 221 (1969)
    function coeffs(n)# coefficients from NIST"s DLMF, sec. 18.9
        if grid_type == "legendre"
            an = (2n + 1)/(n + 1)
            alp = 0.0
            anp1 = (2n + 3)/(n + 2)
            cnp1 = (n + 1)/(n + 2)
        elseif grid_type=="laguerre"
            an = -1 /(n + 1)
            alp = -(2n + 1 + alag)/(n + 1)/an
            anp1 = -1 /(n + 2)
            cnp1 = (n + alag + 1)/(n + 2)
        end
        return alp,(cnp1/an/anp1)^(0.5)
    end

    tmp = @. coeffs(0:1.0:lvl-1)
    diag = getindex.(tmp,1)
    udiag = getindex.(tmp,2)

    jac = SymTridiagonal(diag,udiag[1:end-1])
    grid = eigvals(jac)
    v = eigvecs(jac)

    if grid_type=="legendre"
        mu_0 = 2
    elseif grid_type == "laguerre"
        mu_0 = gamma(1+alag)
    end

    wg = @. mu_0*v[1,:]^2
    writedlm("./grids/"*fname,[ ["point" "weight"] ; [grid wg]],",")

    return
end

gauss_quad(10)
