
function exc_lsda(nu,nd)
    ex,vxu,vxd = ex_lsda(nu,nd)
    ec,vcu,vcd = ec_pw92(nu,nd)
    return ex+ec, vxu+vcu, vxd+vcd
end

function ex_lsda(nu,nd)
    exu,vxu = ex_lda(2nu)
    exd,vxd = ex_lda(2nd)
    return 0.5*(exu + exd), vxu, vxd
end

function ex_lda(n)
    kf = (3*pi^2*n)^(1/3)
    ex = -3/(4*pi)* kf * n
    vx = - kf/pi
    return ex,vx
end

function ec_pw92(nu,nd)
    # J. P. Perdew and Y. Wang, PRB 45, 13244 (1992).
    # doi: 10.1103/PhysRevB.45.13244

    n = nu + nd
    rs = (3/(4*pi *n))^(1/3)
    zeta = (nu - nd)/(nu + nd)

    fz_den = 2^(4/3) - 2
    fdd0 = 8/9/(2.0^(4/3) - 2)
    fz = ((1 + zeta)^(4/3) + (1 - zeta)^(4/3) - 2)/fz_den
    dfz = 4/3*( (1 + zeta)^(1/3) - (1 - zeta)^(1/3))/fz_den

    function g(v,rs)
        q0 = -2v[1]*(1 + v[2]*rs)
        q1 = 2v[1]*(v[3]*rs^(0.5) + v[4]*rs + v[5]*rs^(1.5) + v[6]*rs^2)
        q1p = v[1]*(v[3]*rs^(-0.5) + 2v[4] + 3v[5]*rs^(0.5) + 4v[6]*rs)
        dg = -2*v[1]*v[2]*log(1 + 1/q1) - q0*q1p/(q1^2 + q1)
        return q0*log(1 + 1/q1),dg
    end

    ec0,d_ec_drs_0 = g([0.031091,0.21370,7.5957,3.5876,1.6382,0.49294],rs)
    ec1,d_ec_drs_1 = g([0.015545,0.20548,14.1189,6.1977,3.3662,0.62517],rs)
    ac,d_ac_drs = g([0.016887,0.11125,10.357,3.6231,0.88026,0.49671],rs)
    ac *= -1
    d_ac_drs *= -1

    ec = ec0 + ac*fz/fdd0*(1 - zeta^4) + (ec1 - ec0)*fz*zeta^4

    d_ec_drs = d_ec_drs_0*(1 - fz*zeta^4) + d_ec_drs_1*fz*zeta^4
    d_ec_drs += d_ac_drs*fz/fdd0*(1 - zeta^4)

    d_ec_dz = 4*zeta^3*fz*(ec1 - ec0 - ac/fdd0)
    d_ec_dz += dfz*(zeta^4*ec1 - zeta^4*ec0 + (1 - zeta^4)*ac/fdd0)

    vcu = ec - rs/3*d_ec_drs - (zeta - 1)*d_ec_dz
    vcd = ec - rs/3*d_ec_drs - (zeta + 1)*d_ec_dz

    return ec*n,vcu,vcd
end
