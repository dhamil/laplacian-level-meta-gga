using LinearAlgebra,DelimitedFiles
include("./utilities.jl")
include("./lsda.jl")
include("./gauss_quad.jl")

function self_consistency_cycle(rs;prec=1.e-6,max_cycles=100,x_bds=[-10.0 10.0],phase_power=2,n_kpts=100)

    k,k_wg = k_mesh(n_kpts)
    kf = (9*pi/4)^(1/3)/rs
    k_wg = @. 0.5*(k_wg + 1 )*(9*pi/4)^(1/3)/rs

    n_old = [-1.e20 -1.e20]

    dev(n0,n1) = abs(sum(abs.(n0))/length(n0) - sum(abs.(n1))/length(n1))

    for i_scf = 1:max_cycles
        x,n = self_consistency_iteration(x_bds,k,k_wg,rs;phase_alpha=phase_power)
        if dev(n,n_old) < prec
            return n
        else
            n_old = n
        end
    end

    return
end

function self_consistency_iteration(x_bds,k,k_wg,rs;phase_alpha=2)
    phi0 = init_phi_k(x_bds[1],k;alpha=phase_alpha)
    phi = zeros(length(k))
    for (ik,ak) in enumerate(k)
        x,phi_m = rkck_o5_adaptive(x_bds,phi0,hamiltonian_rk;args=[ak,k_wg,rs])
        phi[ik] = phi_m[:,1]
    end
    n = get_density(x,k,k_wg,phi[:,1],rs)
    return x,n
end

function hamiltonian_rk(x,phi,k,w_wg,rs)

    n = get_density(x,k,k_wg,phi[:,1],rs)

    tmp = exc_lsda(n/2,n/2)
    exc = getindex.(tmp,1)
    vxc = getindex.(tmp,2)

    spc = zeros(length(x))
    for i=1:length(x)-1
        spc[i] = x[i+1]-x[i]
    end
    spc[end] = spc[end-1]

    delta_phi = dipole_barrier(x,spc,n,npos)
    kf = (9*pi/4)^(1/3)/rs
    mu_bulk = kf^2/2
    Phi = delta_phi - mu_bulk


    vh = hartree(n,spc,n,npos)

    two_eps_k = kf^2*(1 - k^2)
    d2_op = 2*(Phi+vh+vxc) + two_eps_k

    return [phi[2], d2_op*phi[1]]

end

function hamiltonian_finite_diff(x,spc,rs,n)
    # discrete Laplacian
    npts = length(x)
    ts_op = -1/2 .* laplacian_1d(npts,spc;order=10)

    # exchange-correlation energy density and potential
    # assuming spin-unpolarized
    tmp = exc_lsda(n/2,n/2)
    exc = getindex.(tmp,1)
    vxc = getindex.(tmp,2)

    npos = @. backgd_chg_den(x,rs)
    delta_phi = dipole_barrier(x,spc,n,npos)
    kf = (9*pi/4)^(1/3)/rs
    mu_bulk = kf^2/2
    Phi = delta_phi - mu_bulk

    vh = hartree(n,spc,n,npos)

    # Kohn-Sham potential
    vs_op = Diagonal(vh) + Diagonal(vxc) + Phi .* I

    return ts_op + vs_op
end

function hartree(x,spc,n,np)
    vh = zeros(length(n))
    for ix = 1:length(x)
        tmp = @. spc*x[ix:end]*(n[ix:end] - np[ix:end])
        vh[ix] = sum(tmp)
    end
    return vh
end

function dipole_barrier(x,spc,n,np)
    itgd = @. spc*x*(n - np)
    return 4*pi*sum(itgd)
end

function backgd_chg_den(x,rs)
    if x <= 0
        return 3/(4*pi*rs^3)
    end
    return 0.0
end

function init_phi_k(x,k;alpha=2)
    # variables here are dimensionless
    # x = x kF
    # k = k/kF
    # alpha controls phase shift gamma
    # form of gamma is determined by
    # Sugiyama-Langreth phase shift sum rule
    gamma = pi/8 *(1 + alpha)*k^(alpha-1)
    phik = @. sin(k*x - gamma)/(exp(k*x) + 1)
    d_phik_dx = @. k*(cos(k*x - gamma) - exp(k*x)*phik)/(exp(k*x) + 1)
    return [phik d_phik_dx]
end

function k_mesh(npts)
    fname="./grids/gauss_legendre_" *string(npts)*"_pts.csv"
    if !isfile(fname)
        gauss_quad(npts)
    end
    tmp = readdlm(fname,',',Float64,'\n';header=true)
    k = getindex(tmp,1)[:,1]
    k_wg = getindex(tmp,1)[:,2]
    return k,k_wg
end

function get_density(x,k,k_wg,phi,rs)
    kf = (9*pi/4)^(1/3)/rs
    itgd = @. k_wg*(1 - k^2)*abs(phi)^2
    return kf^3/pi^2 * sum(itgd)
end

function eps_k(k,rs)
    kf = (9*pi/4)^(1/3)/rs
    return kf^2*(k^2 - 1)/2
end
