import numpy as np
from scipy.optimize import minimize_scalar

import settings
from constants import pi
from surface.surface_hamiltonian import density_k_integral,xc_energy_potential

rs = settings.rs
wg = settings.int_wgts
nx = settings.N_x_pts
nbulk = 3.0/(4.0*pi*rs**3)
kf = (9*pi/4.0)**(1.0/3.0)/rs
exc_bulk,_ = xc_energy_potential(nbulk)

"""
    This code adapted from
    [M1975] G.D. Mahan,
        ``Surface energy of jellium metal'',
        Phys. Rev. B 12, 5585 (1975).
        https://doi.org/10.1103/PhysRevB.12.5585
"""

def background_charge(x):
    npos = np.zeros(nx)
    npos[x <= 0.0] = nbulk
    return npos

def d(vp):
    """ Eq. 2.2 of [M1975] """
    d = pi/2 - (2-vp)*np.arcsin(vp**(-0.5)) - (vp-1)**(0.5)
    d *= 3/(4*kf)
    return d

def h(vp):
    """ Eqs. 2.3 of [M1975] """
    h = (15*vp**2 - 16*vp - 8)*np.arcsin(vp**(-0.5))
    h += 3*(2-5*vp)*(vp-1)**(0.5)
    h *= 1/(2*pi)
    return h

def ke(vp):
    """ Eqs. 2.3 of [M1975] """
    return kf**4/(160*pi)*(1 + h(vp))

def phi_mat(vp,k,x):
    """ Eqs. 2.5 of [M1975] """
    y = kf*(-x + d(vp))
    umask = y < 0.0
    lmask = y >= 0.0
    phi = np.zeros((k.shape[0],nx))
    for ik,ak in enumerate(k):
        phi[ik][umask] = ak*np.exp(y[umask]*(vp - ak**2)**(0.5))/vp**(0.5)
        phi[ik][lmask] = np.sin(ak*y[lmask] + np.arcsin(ak/vp**(0.5)))
    return phi

def hartree(n,npos,x,spc):
    """ Above Eq. 2.6 of [M1975] """
    uh = 0.0
    out_int = wg*(n - npos)
    for ix in range(1,nx):
        in_int = (x - x[ix])*out_int
        uh += out_int[ix]*np.sum(in_int[:ix+1])
    return 2*pi*spc**2*uh

def etot(vp,k,kwg,x,spc):
    eke = ke(vp)

    npos = background_charge(x)
    phi = phi_mat(vp,k,x)
    n = density_k_integral(k,kwg,phi)

    eps_xc,_ = xc_energy_potential(n)
    exc = spc*np.sum(wg*n*(eps_xc - exc_bulk))
    ees = hartree(n,npos,x,spc)
    en = eke + ees + exc
    return en,n,phi

def variational_opt(k,kwg,x,spc):

    def wrap_res(vp):
        en,_,_ = etot(vp,k,kwg,x,spc)
        return en
    res = minimize_scalar(wrap_res,bounds=(1.0,5.0),method='bounded')
    vp_opt = res.x
    barrier = kf**2/2.0*vp_opt
    _,dens,orbs = etot(vp_opt,k,kwg,x,spc)

    return dens,orbs,barrier


def wrap_en(c1,c2,h,k,kwg,npos,x):
    n,phi,dphi = construct(c1,c2,k,kwg,x)
    tau = np.zeros(nx)
    wgt = (1.0 - k**2)*kwg
    for ix in range(nx):
        tau_int = np.abs(dphi[:,ix])**2 + 0.5*kf**2*np.abs(phi[:,ix])**2*(1.0 - k**2)
        tau[ix] = 3*nbulk/2.0*h*np.sum(wgt*tau_int)
    eps_xc,_ = xc_energy_potential(n)
    return h*np.sum(wg*(tau + n*eps_xc)) + hartree(n,npos,x,h)

def construct(a,b,k,kwg,x):
    phi = np.zeros((k.shape[0],nx))
    dphi = np.zeros(phi.shape)
    phase = (a*k + b*k**2)/(a + b)#a*3*pi/8.0*k + b*pi/2.0*k**2 +(1-a-b)*5*pi/8.0*k**3
    #phase[phase < 0.0] = 0.0
    #phase[phase > 1.0] = 1.0
    for ik,ak in enumerate(k):
        phi[ik][x<=0.0] = np.sin(ak*kf*x[x<=0.0] - np.arcsin(phase[ik]))
        dphi[ik][x<=0.0] = ak*kf*np.cos(ak*kf*x[x<=0.0] - np.arcsin(phase[ik]))
        phi[ik][x>0.0] = -phase[ik]*np.exp(-kf*ak*x[x>0.0]*(1.0 - phase[ik]**2)**(0.5)/phase[ik])
        dphi[ik][x>0.0] = kf*ak*(1.0 - phase[ik]**2)**(0.5)*np.exp(-kf*ak*x[x>0.0]*(1.0 - phase[ik]**2)**(0.5)/phase[ik])
    n = density_k_integral(k,kwg,phi)
    return n,phi,dphi


def var_dens(k,kwg,npos,x,h):
    import multiprocessing
    from itertools import product
    """
        initial wavefunction tries to simulate:
        x --> -inf, phi_k --> sin(k*x - gamma(k)) asymptotic behavior deep within bulk
        for simplicity, fix gamma(k) = 3 pi/8 k for initial guess

        Generally, gamma(k) must satisfy Sugiyama-Langreth phase shift sum rule
        2/kf**2 int_0^kf k gamma(k) dk = pi/4
        and satisfy gamma(0) = 0

        x > 0, phi_k is an exponentially damped oscillation outside surface

        Note also that lim_x-->-inf phi_k = sin(k*x - gamma(k)),
        which correctly yields the density
        n(x->-inf) = kf**3/pi**2 *int_0^1 [1 - (k/kF)**2]*sin(k*x - gamma(k))**2 d(k/kF) = kf**3/(3*pi**2)
    """

    oopt = 1e20
    steps = [0.5,0.1,0.05,0.01]
    best = []
    for istep,astep in enumerate(steps):
        if istep == 0:
            cmin1 = 0.01
            cmax1 = 4.0
            cmin2 = 0.0
            cmax2 = 4.0
        else:
            print(best)
            cmin1 = max(0.01,best[0]-steps[istep-1])
            cmax1 = min(4,best[0]+steps[istep-1])
            cmin2 = max(0.0,best[1]-steps[istep-1])
            cmax2 = min(4,best[1]+steps[istep-1])
        c1l = np.arange(cmin1,cmax1,astep)
        c2l = np.arange(cmin2,cmax2,astep)
        pl = product(c1l,c2l)
        nl = c1l.shape[0]*c2l.shape[0]
        if settings.nproc > 1 and nl>1:
            workgroup = multiprocessing.Pool(processes=min(settings.nproc,nl))
            wkl = []
            for pp in pl:
                wkl.append((pp[0],pp[1],h,k,kwg,npos,x))
            outlist = workgroup.starmap(wrap_en,wkl)
            workgroup.close()
            outlist = np.asarray(outlist)
            bind = np.argmin(outlist)
            print(outlist[bind])
            best = [wkl[bind][0],wkl[bind][1]]
        else:
            for pp in pl:
                opt = wrap_en(pp[0],pp[1],h,k,kwg,npos,x)
                if opt < oopt:
                    best = [pp[0],pp[1]]
                    oopt = opt
    n_opt,phi_opt,_ = construct(best[0],best[1],k,kwg,x)
    return n_opt,phi_opt
