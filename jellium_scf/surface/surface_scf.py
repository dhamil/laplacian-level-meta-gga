import numpy as np
from os import path,system

import settings,constants
from surface.surface_hamiltonian import ks_hamiltonian,density_k_integral,dens_vars
from surface.var_init_dens import variational_opt
from utilities.gauss_quad import gauss_quad
from utilities.ode_integrators import fdiff_coeffs
from utilities.density_mixing import modified_broyden,initiate_modified_broyden,linear_mixing

pi = constants.pi
rs = settings.rs
wg = settings.int_wgts
nx = settings.N_x_pts
nbulk = 3.0/(4.0*pi*rs**3)
kf = (9*pi/4.0)**(1.0/3.0)/rs
LF = 2*pi/kf

def k_mesh(): # initiates k-grid
    fname = './grids/gauss_legendre_'+str(settings.N_k_pts)+'_pts.csv'
    if not path.isfile(fname):
        gauss_quad(settings.N_k_pts)

    kwg,k = np.transpose(np.genfromtxt(fname,delimiter=',',skip_header=1))
    k = 0.5*(k + 1.0) # shift k/kF grid from -1 < k/kF < 1 to 0 < k/kF < 1
    kwg *= 0.5
    return k,kwg

def background_charge(x):
    npos = np.zeros(nx)
    npos[x <= 0.0] = nbulk
    return npos

def init_dens(k,kwg,npos,x,h):

    """
        initial wavefunction tries to simulate:
        x --> -inf, phi_k --> sin(k*x - gamma(k)) asymptotic behavior deep within bulk
        for simplicity, fix gamma(k) = 3 pi/8 k for initial guess

        Generally, gamma(k) must satisfy Sugiyama-Langreth phase shift sum rule
        2/kf**2 int_0^kf k gamma(k) dk = pi/4
        and satisfy gamma(0) = 0

        x > 0, phi_k is an exponentially damped oscillation outside surface

        Note also that lim_x-->-inf phi_k = sin(k*x - gamma(k)),
        which correctly yields the density
        n(x->-inf) = kf**3/pi**2 *int_0^1 [1 - (k/kF)**2]*sin(k*x - gamma(k))**2 d(k/kF) = kf**3/(3*pi**2)
    """

    def construct(a,b):
        phase = a*3*pi/8.0*k + b*pi/2.0*k**2 +(1-a-b)*5*pi/8.0*k**3
        for ik,ak in enumerate(k):
            ophi_m[ik][x<=0.0] = np.sin(ak*kf*x[x<=0.0] - phase[ik])
            ophi_m[ik][x>0.0] = -np.sin(phase[ik])*np.exp(-kf*ak*x[x>0.0]/np.tan(phase[ik]))
        n = density_k_integral(k,kwg,ophi_m)
        return n,ophi_m

    ophi_m = np.zeros((k.shape[0],nx))
    oopt = 1e20
    for c1 in np.arange(0.1,1.01,0.1):
        for c2 in np.arange(0.1,c1,0.1):
            n,ophi_m = construct(c1,c2)
            opt = abs(h*np.sum(wg*(n-npos)))
            if opt < oopt:
                oopt = opt
                best = [c1,c2]
            if oopt < settings.scf_conv['CHG']:
                break
        if oopt < settings.scf_conv['CHG']:
            break
    return construct(best[0],best[1])

def scf():

    wdir = './data/surface/rs_'+str(rs)
    for dir in [wdir,'./grids']:
        if not path.isdir(dir):
            system(('mkdir -p {:}').format(dir))

    k,kwg = k_mesh()

    x = np.linspace(settings.x_min,settings.x_max,settings.N_x_pts)
    delta_x = x[1]-x[0]
    x0 = x.min()
    npos = background_charge(x)
    sig = settings.surface_charge

    n,ophi_m,bh = variational_opt(k,kwg,x,delta_x)
    #n,ophi_m = init_dens(k,kwg,npos,x,delta_x)

    oetot = 1e20
    scf_fl = open(wdir+'/rs_'+str(rs)+'_logfile.csv','w+')
    scf_fl.write('Iteration, sigma_tot, 1-sigma_i/sigma_(i-1), KE, ESP, XC, Surf. charge, SL sum rule-pi/4, Delta Phi error, Budd-Vann.\n')

    pos_chg = -delta_x*np.sum(wg*npos)
    for step in range(settings.max_scf_steps['HKS']):
        if step == 0:
            if settings.density_mixing=='mod_broyden':
                F,dF,u_vec,a_mat,mix_wg = initiate_modified_broyden(settings.N_x_pts)
            old_esp =  np.zeros(nx)#bh*np.ones(nx)
            vks_old = np.zeros(nx)
        else:
            old_esp = new_esp
            vks_old = vks
        ochg = delta_x*np.sum(wg*n)

        phi_m,new_esp,n_out,en_v,slsr,dphi_err,bv_err,vks = ks_hamiltonian(x,delta_x,n,npos,ophi_m,k,kwg,step,old_esp,vks_old)

        etot = en_v[0]
        en_v = [y*constants.au_to_erg_cm_m2 for y in en_v]

        cchg = delta_x*np.sum(wg*n_out)
        chg_cons = cchg + pos_chg
        omix = abs(chg_cons - sig)
        ostring = ('{:},'*9+'{:}\n').format(step,en_v[0],1-etot/oetot,*en_v[1:],chg_cons,slsr-pi/4,dphi_err,bv_err)
        scf_fl.write(ostring)

        if settings.density_mixing=='none':
            n_new = n_out
        elif settings.density_mixing=='linear':
            if rs > 5:
                mxpr = 0.05
            elif rs > 4:
                mxpr = 0.35
            else:
                mxpr = 0.75
            n_new = linear_mixing(n,n_out,mixpar=mxpr)
        elif settings.density_mixing=='mod_broyden':
            n_new,F,dF,mix_wg,u_vec,a_mat = modified_broyden(delta_x*wg,n,n_out,F,dF,u_vec,a_mat,mix_wg,step)

        if rs > 5 and abs(etot) > 1.e5:
            raise ValueError('ODE solver has gone off the rails! \n Never going to converge')

        #print(step,1-etot/oetot,chg_cons,new_esp[-1]-new_esp[0])
        endiff = abs(oetot - etot)

        if endiff < settings.scf_conv['HKS']*abs(oetot) and omix < settings.scf_conv['CHG'] and step > settings.min_scf['HKS']:
            ofname = wdir+'/rs_'+str(rs)+'_'
            if settings.save_orbitals:
                np.savetxt(ofname+'orbitals.csv',np.transpose((np.vstack((x,phi_m)))),delimiter=',',header='x, phi_k0, phi_k1,...')
            gn,ln,tau = dens_vars(delta_x,k,kwg,phi_m,vks)
            np.savetxt(ofname+'density.csv',np.transpose((x,n_out,gn,ln,tau)),delimiter=',',header='x,n(x),grad n,lap n, tau')
            np.savetxt(ofname+'esp.csv',np.transpose((x,new_esp)),delimiter=',',header='x,ESP(x)')
            return
        else:
            ophi_m = phi_m
            n = n_new
            oetot = etot
    scf_fl.close()
    """
    import matplotlib.pyplot as plt
    plt.plot(x*kf/(2*pi),n_out/nbulk)
    plt.show()
    exit()
    #"""

    print(('WARNING: SCF not converged! \n Last error in charge density = {:} \n and total surface energy = {:}').format(chg_cons,endiff/abs(oetot)))
    return

    raise RecursionError(('WARNING: SCF not converged! \n Last error in charge density = {:} \n and total surface energy = {:}').format(chg_cons,endiff/abs(oetot)))

if __name__ == "__main__":

    scf()
