import numpy as np
from os import path
from scipy.linalg import eigh_tridiagonal
import multiprocessing as mp

import settings
import constants
from dft.lsda import lda_x,eps_c_pw92,ec_pz81
from utilities.ode_integrators import numerov_shooting,numerov_driver,fdiff_coeffs
from utilities.roots import bisect

"""
    This code benefited greatly from the works of

    N.D. Lang and W. Kohn,
    ``Theory of Metal Surfaces: Charge Density and Surface Energy''
    Phys. Rev. B 1, 4555 (1970).
    https://doi.org/10.1103/PhysRevB.1.4555

    R. Monnier and J.P. Perdew,
    ``Surfaces of real metals by the variational self-consistent method'',
    Phys. Rev. B 17, 2595 (1978).
    https://doi.org/10.1103/PhysRevB.17.2595

    J.P. Perdew and Y. Wang,
    ``Jellium work function for all electron densities'',
    Phys. Rev. B 38, 12228 (1988).
    https://doi.org/10.1103/PhysRevB.38.12228

"""

pi = constants.pi
wg = settings.int_wgts
nx = settings.N_x_pts

rs = settings.rs
kf = (9*pi/4.0)**(1.0/3.0)/rs
vol = 2*pi/kf*(settings.x_max-settings.x_min)
nbulk = kf**3/(3*pi**2)

vks_m_inf = -kf**2/2.0

def xc_energy_potential(n):
    # returns LSDA (PW92) XC energy per electron and potential
    eps_x,vx = lda_x(n)
    eps_c,vc,_ = eps_c_pw92(n,0.0)
    #eps_c,vc,_ = ec_pz81(n,0.0)
    return eps_x+eps_c, vx+vc

exc_bulk,vxc_bulk = xc_energy_potential(nbulk)

def ks_hamiltonian(x,spc,n,npos,phi_k0,k,kwg,iter,oesp,ovks):
    """
        Eq. 2.16a of Lang and Kohn
        -1/2 phi_k''(x) + [v_s(x)  + 1/2(kf**2 - k**2)]phi_k(x) = 0
    """

    wfn,vxc,cesp,vs = ks_potential(x,spc,n,npos,iter,oesp,ovks)
    #vs = vxc + cesp
    phi_k = np.zeros((k.shape[0],x.shape[0]))
    gamma = np.zeros(k.shape[0])
    if settings.nproc > 1 and settings.N_k_pts > 1:
        workgroup = mp.Pool(processes=min(settings.nproc,settings.N_k_pts))
        wlist = [(k[ik],x,vs,spc,phi_k0[ik,0]) for ik in range(settings.N_k_pts)]
        outlist = workgroup.starmap(get_phi_k,wlist)
        workgroup.close()
        for ik in range(settings.N_k_pts):
            phi_k[ik],gamma[ik] = outlist[ik]
    else:

        for ik,ak in enumerate(k):
            phi_k[ik],gamma[ik] = get_phi_k(ak,x,vs,spc,phi_k0[ik,0])

    n_out = density_k_integral(k,kwg,phi_k)
    en_vec = energy(x,spc,n_out,npos,vs,cesp,gamma,k,kwg)

    # Sugiyama-Langreth sum rule, see Eq. 4.17 of Monnier and Perdew
    sugi_lang = 2*np.sum(gamma*k*kwg)

    dip_bar = dipole_barrier(x,spc,n_out,npos)
    delta_phi = cesp[-1]-cesp[0]

    bvr,bvl = budd_vannimenus_theorem(x,cesp)

    return phi_k,cesp,n_out,en_vec,sugi_lang,1.0 - delta_phi/dip_bar,1.0 - bvl/bvr,vs

def energy(x,spc,n,npos,vso,esp,gammak,k,kwg):
    epsxc,vs = xc_energy_potential(n)
    sigma_ke = kf**4/(32*pi) - kf**4/(2*pi**2)*np.sum(gammak*(1.0 - k**2)*k*kwg)
    sigma_ke -= spc*np.sum(wg*n*(vso-vks_m_inf))
    sigma_esp = 0.5*spc*np.sum(wg*esp*(n - npos))

    sigma_xc = spc*np.sum(wg*n*(epsxc - exc_bulk))
    #sigma_xc = spc*np.sum(wg*(n*epsxc - npos*exc_bulk))

    sigma = sigma_ke + sigma_esp + sigma_xc

    return [sigma, sigma_ke, sigma_esp, sigma_xc]

def get_phi_k(k,x,vs,spc,phi_in):
    tepsk = kf**2*k**2 + 2*vks_m_inf
    d2_phik_dx2 = 2*vs - tepsk

    asyk = (-tepsk)**(0.5)
    phi_inf = np.exp(-asyk*x[-1])
    dphi_inf = -asyk*phi_inf
    tphi = numerov_driver(np.zeros(nx),d2_phik_dx2,phi_inf,dphi_inf,-spc)

    fdcoeff = fdiff_coeffs('fwd',settings.finite_diff_order)/spc
    dphi0 = np.dot(fdcoeff,tphi[:len(fdcoeff)])
    kx0_m_phase = np.arctan(k*kf*tphi[0]/dphi0)#np.arctan2(k*kf*tphi[0],dphi0)
    gammak = (kf*k*x[0] - kx0_m_phase)%(pi)
    cphi0 = np.sin(kx0_m_phase)
    tphi *= cphi0/tphi[0]
    return tphi,gammak

def ks_potential(x,spc,n,npos,iter,ovH,ovks):

    phi_m_inf = vks_m_inf - vxc_bulk
    delta_phi = dipole_barrier(x,spc,n,npos)
    nstep = 1
    if iter == 0:
        #ovH = np.zeros(x.shape)
        ovH[x <= 0.0] = phi_m_inf
        if rs >= 5:
            ovH[x > 0.0] = phi_m_inf
        else:
            ovH[x > 0.0] = delta_phi + phi_m_inf
        #ovH[x > 0.0] += phi_m_inf
        nstep = settings.max_scf_steps['ESP']

    vH = hartree_scf(x,spc,n,npos,ovH,nstep)
    """
    if rs > 5:
        feedback = 0.05
    elif rs > 4:
        feedback = 0.35
    else:
        feedback = 1.0
    """
    feedback = 0.3
    #vH = (1.0 - feedback)*ovH + feedback*vH

    _,vxc = xc_energy_potential(n)

    if iter > 0:
        vks = ovks + feedback*(vxc + vH - ovks)
    else:
        vks = vxc + vH

    return delta_phi+phi_m_inf,vxc,vH,vks

def hartree_scf(x,spc,n,npos,inphi,nstep):

    """
        Following method of Monnier and Perdew,
        assume initial potential is
        square barrier. The x -> -infinity limit is given by

        near Eq. 2.13, phi(-inf) = -1/2 kF**2 - v_xc(n_bulk)
        with n_bulk = 3/(4 pi rs**3)
    """
    if nstep>1:
        feedback = 0.1
    else:
        feedback = 1.0
    fwd_diff_coeff = fdiff_coeffs('fwd',settings.finite_diff_order)
    ophi = inphi
    for ipot in range(nstep):
        dophi0 = np.sum(fwd_diff_coeff*ophi[:len(fwd_diff_coeff)])/spc
        phi = hartree_one_step(x,spc,n,npos,ophi,dophi0)
        dphi = phi - ophi
        ophi = ophi + feedback*dphi
        """
        if np.sum(np.abs(dphi)) < settings.scf_conv['ESP'] and ipot > settings.min_scf['ESP']:
            return phi
        else:
            ophi = phi
        """
    return ophi
    #raise RecursionError(('WARNING: electrostatic potential not converged! \n Last residual = {:}').format(res))

def hartree_one_step(x,spc,n,npos,phi0,dphi0):
    """
        vh''(x) = - 4*pi*[ n(x) - n+(x)]
        Solved by iteration of the Poisson equation according to Monnier and Perdew
    """
    vh = np.zeros(n.shape)
    kernel = 4*pi*(n - npos)

    phi_m_inf = vks_m_inf - vxc_bulk
    kscr = kf*settings.kscr
    for ix,ax in enumerate(x):
        vh[ix] = spc/(2*kscr)*np.sum(wg*(kernel+kscr**2*phi0)*np.exp(-kscr*np.abs(ax-x)))
        vh[ix] += 0.5*(phi_m_inf - dphi0/kscr)*np.exp(-kscr*(ax-x[0]))
        vh[ix] += 0.5*phi0[-1]*np.exp(-kscr*(x[-1]-ax))
    return vh

def dipole_barrier(x,h,n,npos):
    return 4*pi*h*np.sum(wg*x*(n - npos))

"""
    Density-related variables and functions

"""

def budd_vannimenus_theorem(x,phi):
    """
        Budd-Vannimenus theorem (Eq. 4.18 of Monnier and Perdew) states that
        Phi(0) - Phi(-inf) = n_bulk d [3/5 eps_F(n_bulk) + eps_xc(n_bulk)] d n_bulk
        at the converged density
        eps_F = k_F**2/2
        eps_xc is XC energy per electron
    """
    icz = np.argmin(np.abs(x))
    lhs = phi[icz] - phi[0]
    rhs = kf**2/5.0 + vxc_bulk - exc_bulk
    return rhs,lhs

def density_k_integral(k,k_wg,phi):
    """
        yields density from orbitals phi_k(x)
        rows of matrix phi are for constant k
    """
    n = 3*nbulk*np.matmul(k_wg*(1 - k**2),np.abs(phi)**2)
    return n

def dens_vars(spc,k,kwg,phi,vs):

    fwd_diff = np.asarray([-49.0/20.0,6.0,-15.0/2.0,20.0/3.0,-15.0/4.0,6.0/5.0,-1.0/6.0])/spc
    cen_diff = np.asarray([-1.0/60.0,3.0/20.0,-3.0/4.0,0.0,3.0/4.0,-3.0/20.0,1.0/60.0])/spc

    fwd_diff_2 = np.asarray([469.0/90.0,-223.0/10.0,879.0/20.0,-949.0/18.0,41.0,-201.0/10.0,1019.0/180.0,-7.0/10])/spc**2
    cen_diff_2 = np.asarray([1.0/90.0,-3.0/20.0,3.0/2.0,-49.0/18.0,3.0/2.0,-3.0/20.0,1.0/90.0])/spc**2

    dn_x = np.zeros(nx)
    dn_rho = np.zeros(nx)
    lapn = np.zeros(nx)
    tau = np.zeros(nx)

    dphi = np.zeros(phi.shape)
    #ddphi = np.zeros(phi.shape)
    """
    for ix in range(nx):
        if ix < 3:
            dn[ix] = np.dot(fwd_diff,n[ix:ix+7])
            lapn[ix] = np.dot(fwd_diff_2,n[ix:ix+8])
        elif ix > nx - 4:
            dn[ix] = np.dot(-fwd_diff,n[ix:ix-7:-1])
            lapn[ix] = np.dot(fwd_diff_2,n[ix:ix-8:-1])
        else:
            dn[ix] = np.dot(cen_diff,n[ix-3:ix+4])
            lapn[ix] = np.dot(cen_diff_2,n[ix-3:ix+4])
    """
    for ik in range(k.shape[0]):
        for ix in range(nx):
            if ix < 3:
                dphi[ik,ix] = np.dot(fwd_diff,phi[ik,ix:ix+7])
                #ddphi[ik,ix] = np.dot(fwd_diff_2,phi[ik,ix:ix+8])
            elif ix > nx - 4:
                dphi[ik,ix] = np.dot(-fwd_diff,phi[ik,ix:ix-7:-1])
                #ddphi[ik,ix] = np.dot(fwd_diff_2,phi[ik,ix:ix-8:-1])
            else:
                dphi[ik,ix] = np.dot(cen_diff,phi[ik,ix-3:ix+4])
                #ddphi[ik,ix] = np.dot(cen_diff_2,phi[ik,ix-3:ix+4])

    wgt = (1.0 - k**2)*kwg

    for ix in range(nx):
        dn_x[ix] = 6*nbulk*np.sum(wgt*phi[:,ix]*dphi[:,ix])
        #dn_rho[ix] = 4*nbulk*kf*np.sum(wgt*(1.0 - k**2)**(0.5)*np.abs(phi[:,ix])**2)

        ddphi = (2*vs[ix] + kf**2*(1.0-k**2))*phi[:,ix]

        lapn_int = dphi[:,ix]**2 + phi[:,ix]*ddphi
        lapn[ix] = 6*nbulk*np.sum(wgt*lapn_int)

        tau_int = np.abs(dphi[:,ix])**2 + 0.5*kf**2*np.abs(phi[:,ix])**2*(1.0 - k**2)
        tau[ix] = 3*nbulk/2.0*np.sum(wgt*tau_int)

    dn = np.abs(dn_x)

    return dn,lapn,tau
