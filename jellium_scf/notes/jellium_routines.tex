\documentclass[11pt]{article}
\usepackage[margin=1in]{geometry}

\usepackage[T1]{fontenc}
\usepackage{palatino}

\usepackage{amsmath,amssymb,graphicx,bm,bibentry,tocbasic,etoolbox,authblk,xspace,hyperref}

\newcommand{\hop}{\ensuremath{\hat{H}}\xspace}
\newcommand{\re}{\mathrm{Re}~}
\newcommand{\br}{\ensuremath{\bm{r}}}
\newcommand{\rs}{r_{\mathrm{s}}}
\newcommand{\vext}{v_{\mathrm{ext}}}
\newcommand{\sux}{_{\mathrm{x}}}
\newcommand{\suc}{_{\mathrm{c}}}
\newcommand{\suxc}{_{\mathrm{xc}}}
\newcommand{\kf}{k_{\mathrm{F}}}

\begin{document}

\title{Notes on jellium subroutines}
\author{Aaron Kaplan}
\date{\today}
\maketitle


\section{Clusters}

For clusters, I've tried two methods: a Numerov/shooting calculation and a basis-set expansion method. An $N$-electron jellium cluster of bulk Seitz radius $\rs$ experiences a potential
\begin{equation}
  \vext = \left\{ \begin{array}{lr} -\frac{N}{2R\suc}\left[3 - \left(\frac{r}{R\suc}\right)^2 \right] & r \leq R\suc \\
  -\frac{N}{r} & r > R\suc \end{array} \right.
\end{equation}
from the neutralizing background charge. $R\suc = N^{1/3}\rs$ is the radius of the cluster.

\subsection{Numerov method}

The Numerov method solves a Schr\"odinger-type differential equation
\begin{equation}
  y''(x) = U(x) y(x)
\end{equation}
from initial conditions $y(x_0)\equiv y_0$ and $y'(x_0)\equiv y'_0$. The algorithm is recursive, and can be solved either forwards or backwards for a uniform step size $h$ according to
\begin{equation}
  0 = [a U(x_{i+1}) -1] y(x_{i+1}) + 2[1 + 5 a U(x_i)] y(x_i) + [a U(x_{i-1}) -1] y(x_{i-1})
\end{equation}
where $a = h^2/12$ and $x_{i\pm 1} = x_i \pm h$. There are different ways of finding $y_1$ from $y_0$ and $y'_0$; I'm using a suggestion from J. L. M. Quiroz Gonz\'alez and D. Thompson \footnote{``Getting started with Numerov's method'', Computers in Physics {\bf 11}, 514--515 (1997), \url{https://doi.org/10.1063/1.168593}} that approximates the first step to the same order as Numerov's method ($\mathcal{O}(h^6)$).

To use Numerov's method, we assume that the cluster (and atomic) orbitals have the form
\begin{equation}
  \phi_{\varepsilon,\ell}(\br) = \frac{u_{\varepsilon,\ell}(r)}{r} Y_{\ell,0}(\theta,\phi),
\end{equation}
where $Y_{\ell,0}(\theta,\phi)$ is a spherical harmonic. We can ignore the ``magnetic quantum number'' by keeping track of the degeneracy of each angular function, $2\ell + 1$. Then the Kohn-Sham equations take the form
\begin{equation}
  \left[ -\frac{1}{2} \frac{d^2}{d r^2} + \frac{\ell(\ell+1)}{2r^2} + \vext(r) + v_{\mathrm{H}}(r) + v\suxc(r) - \varepsilon \right] u_{\varepsilon,\ell}(r) = 0,
\end{equation}
for which the Numerov method can be applied straightforwardly. The Hartree potential is evaluated using a spherically-symmetric density
\begin{equation}
  v_{\mathrm{H}}([n];r) = \frac{4\pi}{r}\int_0^r n(r') r'^2 d r' + 4 \pi \int_r^{\infty} n(r') r' dr',
\end{equation}
and the exchange-correlation potential is found from the PW92 LSDA.

For $r \to 0$, the KS potential is less singular than the $1/r^2$ term of the centrifugal potential, $\lim_{r \to 0} r^2[\vext(r) + v_{\mathrm{H}}(r) + v\suxc(r) - \varepsilon] \to 0$, and thus
\begin{equation}
  \lim_{r\to 0} u_{\varepsilon,\ell}''(r) = -\frac{\ell(\ell+1)}{r^2}u_{\varepsilon,\ell}(r).
\end{equation}
For a finite solution at the origin, as required on physical principles, $u_{\varepsilon,\ell}(r \to 0) \propto r^{\ell}$.

For $r \to \infty$, the KS potential tends to zero (the XC part decays exponentially, and the external and Hartree terms decay like $1/r$), thus
\begin{equation}
  \lim_{r \to \infty} u_{\varepsilon,\ell}''(r) = -2 \varepsilon u_{\varepsilon,\ell}(r).
\end{equation}
The solution for $r\to \infty$ must decay, thus $\varepsilon \leq 0$ for bound states ($\varepsilon > 0$ gives continuum states), and $u_{\varepsilon,\ell}(\to \infty) \propto \exp[-\sqrt{2\varepsilon} r]$.

Often, when propagating a solution from the classically allowed region into the classically forbidden region, the exponentially increasing solution in the CFR is found, rather than the decaying solution. Moreover, we have the following boundary value problem
\begin{align}
  u_{\varepsilon,\ell}(r_0) &= \text{const.} ~r_0^{\ell} \\
  u'_{\varepsilon,\ell}(r_0) &= \ell u_{\varepsilon,\ell}(r_0) \\
  u_{\varepsilon,\ell}(r_{\infty}) &= \text{const.} ~\exp[-\sqrt{2\varepsilon} r_{\infty}] \\
  u'_{\varepsilon,\ell}(r_{\infty}) &= -\sqrt{2\varepsilon} u_{\varepsilon,\ell}(r_{\infty})
\end{align}
where $r_0=10^{-2}/N$ bohr is ``practically zero'' and $r_{\infty}=50$ bohr is ``practically infinity.'' A grid of 8,000 points is used, corresponding to a stepsize $h \approx 0.006250$ regardless of $N$. Both of bounds are suggested by authors of atomic codes, particularly C. Froese Fischer \footnote{``Self-consistent-field (SCF) and multiconfiguration (MC) Hartree-Fock (HF) methods in atomic calculations: Numerical integration approaches'', Computer Physics Reports {\bf 3}, 274-325 (1986), \url{https://doi.org/10.1016/0167-7977(86)90001-8}}. Let $r\suc$ be the approximate turning surface radius,
\begin{equation}
  \frac{\ell(\ell+1)}{r^2 \suc} + \vext(r\suc) + v_{\mathrm{H}}(r\suc) + v\suxc(r\suc) - \varepsilon \approx 0.
\end{equation}
We then propagate one solution, $u_{\text{out}}(r)$, outwards from $r_0 \leq r \leq r\suc$, and another, $u_{\text{in}}(r)$, inwards from $r\suc \leq r \leq r_{\infty}$, and attempt to match their logarithmic derivatives at $r\suc$,
\begin{equation}
  \frac{u'_{\text{out}}(r\suc)}{u_{\text{out}}(r\suc)} - \frac{u'_{\text{in}}(r\suc)}{u_{\text{in}}(r\suc)} = 0,
\end{equation}
which avoids determination of the normalization constant until a suitable solution is found. The roots of this equation are found by a simple bisection method.

Once the eigenvalues are determined, the solutions are stored and the normalization constants are determined. The density is then calculated from the lowest $N/2$ eigenvalues, assigning each spin orbital a degeneracy of $(2\ell+1)$.

\subsection{Basis sets: Slater type orbitals}

Alternatively, we can use a basis set with free parameters to determine the ground state energy variationally.

A Slater type orbital (STO) is identified by its orbital principal quantum number $n$, exponent $\zeta$, and angular momentum quantum numbers $\ell$ and $m$,
\begin{equation}
  \chi_{n\zeta\ell m}(\br) = \chi_{n\zeta}(r) Y_{\ell m}(\theta,\phi),
\end{equation}
where $Y_{\ell m}(\theta,\phi)$ is a normalized spherical harmonic and $\chi_{n\zeta}(r)$ is a Slater radial orbital,
\begin{equation}
  \chi_{n\zeta}(r) = A_{n\zeta}r^{n-1}e^{-\zeta r}
\end{equation}
with normalization constant $A_{n\zeta} = (2\zeta)^{n+1/2}/[(2n)!]^{1/2}$. STOs are \emph{not} orthonormal. Consider the general moment $  \langle n'\zeta'\ell' m'| r^M | n\zeta \ell m \rangle$,
\begin{align}
  \int \chi^*_{n'\zeta'\ell' m'}(\br) r^M \chi_{n\zeta\ell m}(\br) d^3 r &= \delta_{\ell,\ell'}\delta_{m,m'} A_{n'\zeta'}A_{n\zeta} \int_0^{\infty} r^{2+M} r^{n+n'-2} e^{-(\zeta+\zeta')r} dr \\
  &= \delta_{\ell,\ell'}\delta_{m,m'} \frac{A_{n'\zeta'}A_{n\zeta}}{(\zeta+\zeta')^{n+n'+M+1}} \int_0^{\infty} \widetilde{r}^{n+n'+ M} e^{-\widetilde{r}} d \widetilde{r}, \qquad \widetilde{r} = (\zeta+\zeta')r \\
  &= \delta_{\ell,\ell'}\delta_{m,m'} \frac{A_{n'\zeta'}A_{n\zeta}}{(\zeta+\zeta')^{n+n'+M+1}} \Gamma(n+n'+ M+1).
\end{align}
Restricting $M$ to integer values, and using hydrogenic, integer valued $n$ and $n'$ (as opposed to non-integral, Dirac-like principal numbers),
\begin{equation}
  \langle n'\zeta'\ell' m'| r^M | n\zeta \ell m \rangle=\int \chi^*_{n'\zeta'\ell' m'}(\br) r^M \chi_{n\zeta\ell m}(\br) d^3 r = \delta_{\ell,\ell'}\delta_{m,m'} \frac{A_{n'\zeta'}A_{n\zeta} (n+n'+ M)!}{(\zeta+\zeta')^{n+n'+M+1}}.
\end{equation}
Thus the overlap matrix $\underbar{S}$ has matrix elements
\begin{equation}
  S_{n'\zeta'\ell' m'}^{n\zeta\ell m} = \langle n'\zeta'\ell' m' | n\zeta \ell m \rangle = \delta_{\ell,\ell'}\delta_{m,m'} \frac{A_{n'\zeta'}A_{n\zeta} (n+n')!}{(\zeta+\zeta')^{n+n'+1}},
\end{equation}
diagonal in the angular quantum numbers but not in the radial quantum numbers.

Similarly, the Laplacian of an STO is
\begin{equation}
  \nabla^2 \chi_{n\zeta\ell m}(\br) = \left[\frac{n(n-1) - \ell(\ell+1)}{r^2} - \frac{2\zeta n}{r} + \zeta^2 \right] \chi_{n\zeta}(r) Y_{\ell m}(\theta,\phi)
\end{equation}
thus the kinetic energy operator has matrix elements
\begin{align}
  \langle n'\zeta'\ell' m' | -\frac{1}{2}\nabla^2 |n\zeta \ell m \rangle &=  -\frac{1}{2}[n(n-1) - \ell(\ell+1)]\langle n'\zeta'\ell' m'| r^{-2} | n\zeta \ell m \rangle \nonumber \\
  & + \zeta n \langle n'\zeta'\ell' m'| r^{-1} | n\zeta \ell m \rangle - \frac{1}{2}\zeta^2 \langle n'\zeta'\ell' m'| n\zeta \ell m \rangle.
\end{align}
Similarly, for an atomic external potential,
\begin{equation}
  \langle n'\zeta'\ell' m'| -\frac{Z}{r} | n\zeta \ell m \rangle = -Z \langle n'\zeta'\ell' m'| r^{-1} | n\zeta \ell m \rangle.
\end{equation}

For the cluster potential, we need to evaluate
\begin{align}
    \langle n'\zeta'\ell' m' | \vext(r) |n\zeta \ell m \rangle &= \int d\Omega \int_0^{R\suc} \chi^*_{n'\zeta'\ell' m'}(\br)\left\{ -\frac{N}{2R\suc}\left[3 - \left(\frac{r}{R\suc}\right)^2 \right] \right\} \chi_{n\zeta\ell m}(\br) r^2 dr \nonumber \\
    & + \int d\Omega \int_{R\suc}^{\infty} \chi^*_{n'\zeta'\ell' m'}(\br)\left[ -\frac{N}{r} \right] \chi_{n\zeta\ell m}(\br) r^2 dr.
\end{align}
After some algebra, this simplifies to
\begin{align}
  \langle n'\zeta'\ell' m' | \vext(r) |n\zeta \ell m \rangle &= \delta_{\ell,\ell'}\delta_{m,m'}\left\{ -\frac{3N}{2R\suc(\zeta + \zeta')^{n+n'+1}} \int_0^{\widetilde{R}\suc} \widetilde{r}^{n+n'} e^{-\widetilde{r}} d\widetilde{r} \nonumber \right. \\
  & + \frac{N}{2R^3\suc(\zeta + \zeta')^{n+n'+3}} \int_0^{\widetilde{R}\suc} \widetilde{r}^{n+n'+2} e^{-\widetilde{r}} d\widetilde{r} \nonumber \\
  & \left. -\frac{N}{(\zeta + \zeta')^{n+n'}} \int_{\widetilde{R}\suc}^{\infty} \widetilde{r}^{n+n'-1} e^{-\widetilde{r}} d\widetilde{r} \right\},
\end{align}
where $\widetilde{r}=(\zeta+\zeta')r$ and $\widetilde{R}\suc = (\zeta+\zeta')R\suc$. We need only compute general integrals of the form
\begin{equation}
  \int_0^K x^M e^{-x} dx = M! \left[ 1 - e^{-K}\sum_{j = 0}^M \frac{K^j}{j!}\right]
\end{equation}
with $M$ integer valued. This can be found using induction on the product rule for integrals. We know that the limit $K \to \infty$ yields $\Gamma(M+1)=M!$ (for $K \to \infty$, the exponential damps out the sum over $j$ and only $M!$ remains), thus
\begin{equation}
  \int_0^{\infty} x^M e^{-x} dx = \int_0^K x^M e^{-x} dx + \int_K^{\infty} x^M e^{-x} dx \implies \int_K^{\infty} x^M e^{-x} dx = M! e^{-K}\sum_{j = 0}^M \frac{K^j}{j!}.
\end{equation}
Then the cluster potential has matrix elements
\begin{align}
  \langle n'\zeta'\ell' m' | \vext(r) |n\zeta \ell m \rangle &= -N ~\delta_{\ell,\ell'}\delta_{m,m'} \left\{ \frac{3(n+n')!}{2R\suc(\zeta + \zeta')^{n+n'+1}} \left[ 1 - e^{-\widetilde{R}\suc}\sum_{j=0}^{n+n'}\frac{\widetilde{R}\suc^j}{j!}\right] \right. \nonumber \\
  & - \frac{(n+n'+2)!}{2R^3\suc(\zeta + \zeta')^{n+n'+3}}\left[ 1 - e^{-\widetilde{R}\suc}\sum_{j=0}^{n+n'+2}\frac{\widetilde{R}\suc^j}{j!}\right] \nonumber \\
  & \left. + \frac{(n+n'-1)!}{(\zeta + \zeta')^{n+n'}} e^{-\widetilde{R}\suc}\sum_{j=0}^{n+n'-1}\frac{\widetilde{R}\suc^j}{j!} \right\}.
\end{align}

The SCF routine has two stages: an inner SCF cycle where the optimal mixing of orbitals is determined (each orbital must have a single orbital character) according to the generalized eigenvalue problem
\begin{equation}
  (\underbar{H} - \varepsilon \underbar{S})\bm{c} = \bm{0},
\end{equation}
and an outer variational cycle where the $\zeta$ parameters are determined by minimizing the energy (a bounded Broyden-Fletcher-Goldfarb-Shanno algorithm is used). At the beginning of each step in the variational cycle, the (partial) Hamiltonian matrix elements are evaluated analytically using the above formulae. The Hartree and XC matrix elements are determined by numeric integration, and need to be redetermined at each step in the SCF cycle.


\section{Jellium surfaces}

The procedure I'm using for jellium surfaces is superficially similar to the Numerov method used for jellium clusters. For a jellium surface along the $x$ direction, the eigenstates are
\begin{equation}
  \phi_{\bm{k}}(\br) = \phi_k(x)e^{i (k_y y + k_z z)}
\end{equation}
with eigenvalues $\varepsilon_{\bm{k}} = \bm{k}^2/2 - \kf^2/2$ measured with respect to the bulk Fermi wavevector. The Kohn-Sham equations are one-dimensional,
\begin{equation}
  \left[-\frac{1}{2}\frac{d}{dx^2} + \phi([n];x) + v\suxc(x) \right] \phi_k(x) = \frac{1}{2}(k^2 - \kf^2) \phi_k(x)
\end{equation}
with $\phi([n];x)$ the electrostatic potential (ESP) due to the electronic charge and neutralizing background charge
\begin{equation}
  n_+(x) = \left\{ \begin{array}{lr} \frac{3}{4\pi \rs^3}\equiv \bar{n}, & x \leq 0 \\
  0, & x > 0 \end{array}\right.
\end{equation}
$\rs$ is the bulk Seitz radius as before. The ESP is solved according to Monnier and Perdew \footnote{``Surfaces of real metals by the variational self-consistent method'', Phys. Rev. B {\bf 17}, 2595 (1978), \url{https://doi.org/10.1103/PhysRevB.17.2595}.} Eq. A3
\begin{align}
  \phi_{\text{out}}([n];x) &= \frac{1}{2K} \int_{x_0}^{x_1} \{ 4\pi[ n(x') - n_+(x')] + K^2 \phi_{\text{in}}([n];x) \}\exp[-K|x - x'|]d x' \nonumber \\
  & + \frac{1}{2}\left[ \phi_{\text{in}}(x_0) - \frac{1}{K}\frac{d\phi_{\text{in}}}{d x}(x_0)\right]\exp[-K (x - x_0)] \nonumber \\
  & + \frac{1}{2}\left[ \phi_{\text{in}}(x_1) - \frac{1}{K}\frac{d\phi_{\text{in}}}{d x}(x_1)\right]\exp[-K (x_1 - x)],
\end{align}
where $K= \kf$ is a convergence rate parameter, and $x_0 \leq x \leq x_1$ are the bounds of the equally spaced grid. To enforce the boundary conditions appropriately, $\phi'_{\text{in}}(x_1)$ is taken to be zero and $\phi_{\text{in}}(x_0) $ is set to the anticipated asymptotic value
\begin{equation}
  \phi_{\text{in}}(-\infty) = -\left(\frac{1}{2}\kf^2 + v\suxc(\bar{n}) \right),
\end{equation}
which enforces
\begin{equation}
  v_s(x \to -\infty) = -\frac{1}{2}\kf^2.
\end{equation}

The density is constructed as
\begin{equation}
  n(x) = \frac{1}{2\pi^2}\int_{-\kf}^{\kf} (\kf^2 - k^2)|\phi_k(x)|^2 dk.
\end{equation}

\subsection{Initial density and ESP}

The intial wavefunctions are assumed to have the form
\begin{equation}
  \phi_k(x) = \left\{ \begin{array}{lr} \sin\left(k x - \frac{3\pi}{8}k \right), & x \leq 0 \\
  -\sin\left(\frac{3\pi}{8}k \right) \exp[-k \cot\left( \frac{3\pi}{8}k\right) x], & x > 0 \end{array}\right.,
\end{equation}
which satisfies the asymptotic behavior of the orbitals
\begin{equation}
  \lim_{x \to -\infty} \phi_k(x) = \sin[k x - \gamma(k)]
\end{equation}
with $\gamma(k)$ a phase shift. The form selected also satisfies the Sugiyama-Langreth sum rule on the phase shift (Eq. 4.17 of Monnier and Perdew)
\begin{equation}
  \frac{1}{\kf^2}\int_{-\kf}^{\kf} k \gamma(k) dk = \frac{\pi}{4}
\end{equation}
and the physical principles (a) $\gamma(0) = 0$, and (b) $\gamma(k)$ is continuous (see just below Eq. 2.11b of Lang and Kohn \footnote{``Theory of Metal Surfaces: Charge Density and Surface Energy'', Phys. Rev. B {\bf 1}, 4555 (1970), \url{https://doi.org/10.1103/PhysRevB.1.4555}.}). This ansatz is continuous, has continuous derivatives, and yields Friedel-like oscillations near the surface.

Eq. 2.12a of Lang and Kohn suggests that $\phi(x\to -\infty) = -[v\suxc(\bar{n}) + \kf^2/2]$, thus following the suggestion of Monnier and Perdew, the initial ESP is taken to be a square barrier
\begin{equation}
  \phi^{(0)}(x) = \left\{ \begin{array}{lr} -[v\suxc(\bar{n}) + \kf^2/2], & x \leq 0 \\
  -[v\suxc(\bar{n}) + \kf^2/2] + \Delta \phi, & x > 0 \end{array}, \right.
\end{equation}
where
\begin{equation}
  \Delta \phi[n] = 4\pi \int_{-\infty}^{\infty} x[ n(x) - n_+(x)] dx
\end{equation}
is the surface dipole barrier.

To solve the Kohn-Sham equations, a solution is propagated from practically infinity, $x_1 = 1.25(2\pi/\kf)$, inwards towards $x_0 = -3(2\pi/\kf)$ with a stepsize of $h = 0.00125(2\pi/\kf)$. At $x_1$, we assume the solution has taken its asymptotic value
\begin{equation}
  \lim_{x \to \infty} \phi_k(x) = A \exp[-\sqrt{\kf^2 - k^2}x]
\end{equation}
with $A$ a constant determined by matching in the bulk region. The initial conditions for the Numerov algorithm are
\begin{align}
  \phi_k(x_1) &= \exp[-\sqrt{\kf^2 - k^2}x_1] \\
  \phi'_k(x_1) &= -\sqrt{\kf^2 - k^2}\phi_k(x_1).
\end{align}
For $x < x_1$, the exact bulk solution is assumed
\begin{equation}
  \phi_k(x < x_1) = \sin[k x - \gamma(k)].
\end{equation}
At $x_1$, the logarithmic derivatives are matched (see e.g., C.M. Horowitz, L.A. Constantin, C.R. Proetto, and J.M. Pitarke \footnote{``Position-dependent exact-exchange energy for slabs and semi-infinite jellium'',Phys. Rev. B {\bf 80}, 235101 (2009), \url{https://doi.org/10.1103/PhysRevB.80.235101}.})
\begin{align}
  0 &= \frac{\phi'_k(x_1 + 0^+)}{\phi_k(x_1 + 0^+)}  - \frac{\phi'_k(x_1-0^+)}{\phi_k(x_1-0^+)}\\
  0 &= \frac{\phi'_k(x_1 + 0^+)}{\phi_k(x_1 + 0^+)} - k \cot[k x_1 - \gamma(k)].
\end{align}
Thus the constant $A$ and the phase difference $\gamma(k)$ are
\begin{align}
  A &= \frac{1}{\phi_k(x_1)}\sin\left[\arctan\left(\frac{k \phi_k(x_1)}{\phi'_k(x_1)}\right)\right] \\
  \gamma(k) &= k x_1 - \arctan\left(\frac{k \phi_k(x_1)}{\phi'_k(x_1)}\right)
\end{align}
To approximate $\phi'_k(x_1)$, a seven-point forward finite difference is used, as this has the same $\mathcal{O}(h^6)$ accuracy as the Numerov algorithm.

\subsection{Avoiding charge sloshing}

A few methods are used to avoid charge sloshing. The simplest by far is a linear mixing scheme in the density,
\begin{equation}
  n_{i+1}(x) = (1-\alpha)n_i(x) + \alpha n_{\text{out}}(x)
\end{equation}
where $n_{\text{out}}(x)$ is the density found as the output of the $i^{\text{th}}$ self-consistent iteration, and $n_i(x)$ is the input density for that iteration. A static value of the mixing parameter $\alpha = 0.3$ could be used, or a variable value could be determined according to whichever density, $n_i$ or $n_{\text{out}}$, gives lower total energies or is closer to satisfying the charge neutral condition
\begin{equation}
  \Sigma = 4\pi \int_{-\infty}^{\infty} [ n(x) - n_+(x)] dx
\end{equation}
where $\Sigma = 0$ is the intended surface charge density.

Alternatively, the modified Broyden mixing scheme of D. D. Johnson \footnote{``Modified Broyden's method for accelerating convergence in self-consistent calculations'', Phys. Rev. B 38, pp. 12807-12813 (1988), \url{https://doi.org/10.1103/PhysRevB.38.12807}.} is standard in modern code like VASP and GAUSSIAN, and claims to reduce charge sloshing and speed convergence. I've written a routine based on this work, however it needs to be bug checked, and the convergence parameters need to be optimized.

\subsection{Surface energies}

To determine convergence, we need to compute the surface energy
\begin{align}
  \sigma &= \sigma_s + \sigma_{\text{ESP}} + \sigma\suxc \\
  \sigma_s &= \frac{1}{4\pi^2}\int_{-\kf}^{\kf} \left[\frac{\pi}{4} -\gamma(k) \right](\kf^2 - k^2)k dk - \int_{-\infty}^{\infty}[v_s(x) - v_s(-\infty)] n(x) dx \\
  \sigma_{\text{ESP}} &= \frac{1}{2}\int_{-\infty}^{\infty} \phi(x)[n(x) - n^+(x)]dx \\
  \sigma\suxc &= \int_{-\infty}^{\infty} [n(x) \varepsilon\suxc(n(x)) - \bar{n} \varepsilon\suxc(\bar{n})] dx.
\end{align}
$\sigma_s$ is given by Eq. 2.22 of Lang and Kohn, $\sigma_{\text{ESP}}$ by Eq. 2.24 of Lang and Kohn, and $\sigma\suxc $ by Eq. 3.4 of Monnier and Perdew.

\subsection{Density variables}

Similar to the case of a completely uniform electron gas (UEG), we need to convert sums over wavevector to integrals,
\begin{equation}
  \frac{2}{V}\sum_{|\bm{k}|\leq \kf} F(\bm{k}) = 2V\int\frac{d^3 k}{(2\pi)^3} \frac{1}{V} F(\bm{k}) \theta(\kf - |\bm{k}|).
\end{equation}
The volume of space is $V = L^3$, where $L$ is the infinite length of one side of a box containing the UEG. The factor of 2 accounts for spin-degeneracy.

As there is no special direction in the $y-z$ plane, there can be no special direction in the $k_y-k_z$ plane. A more natural set of coordinates would be
\begin{equation}
  (x,\rho,\phi), (k,k_{\rho},\phi_k)
\end{equation}
where $\rho^2 = y^2 + z^2$ and $\phi = \arctan(z/y)$. Thus the integral becomes
\begin{align}
  \sum_{|\bm{k}|\leq \kf} F(\bm{k}) &= \frac{L^3}{4 \pi^3} \int_{-\kf}^{\kf} d k \int_0^{\sqrt{\kf^2-k^2}} d k_{\rho} ~k_{\rho}\int_0^{2\pi} d \phi_k F(\bm{k}) \\
  &= \frac{L^3}{2 \pi^2} \int_{-\kf}^{\kf} d k \int_0^{\sqrt{\kf^2-k^2}} d k_{\rho}~ k_{\rho} F(\bm{k}).
\end{align}
In the special case where $F(\bm{k}) = F(k)$,
\begin{equation}
  \frac{2}{V}\sum_{|\bm{k}|\leq \kf} F(k) = \frac{L^3}{4 \pi^2} \int_{-\kf}^{\kf} (\kf^2-k^2) F(k) = \frac{L^3}{2 \pi^2} \int_{0}^{\kf} (\kf^2-k^2) F(k).
\end{equation}

The jellium surface orbitals are free-electron like in the $y-z$ plane, and solve the above KS equation along the $x$-axis. As they only span half of the $x$ axis, and the entire $y-z$ plane, the overall normalization constant is $(2/L^3)^{1/2}$ rather than $L^{-3/2}$ for the UEG. Without loss of generality, we will replace $(y,z) \to (\rho,\phi)$ and $(k_y,k_z) \to (k_{\rho},\phi_k)$,
\begin{equation}
  \psi_{\bm{k}}(\br) = \left(\frac{2}{L^3} \right)^{1/2} \phi_k(x) e^{i(k_y y + k_z z)} = \left(\frac{2}{L^3} \right)^{1/2} \phi_k(x)e^{i k_{\rho} \rho}.
\end{equation}
The density is
\begin{align}
  n(\br) &= \sum_{|\bm{k}|\leq \kf} |\psi_{\bm{k}}(\br)|^2 = \frac{L^3}{2 \pi^2} \int_{0}^{\kf} (\kf^2-k^2) \frac{2}{L^3}|\phi_k(x)|^2 \\
  n(x) &= \frac{1}{\pi^2}\int_{0}^{\kf} (\kf^2-k^2) |\phi_k(x)|^2.
\end{align}
Similarly, the gradient of the density is
\begin{align}
  \nabla n(\br) &= \sum_{|\bm{k}|\leq \kf} \nabla [\psi^*_{\bm{k}}(\br)\psi_{\bm{k}}(\br)] = 2\mathrm{Re}\sum_{|\bm{k}|\leq \kf} \psi^*_{\bm{k}}(\br)\nabla \psi_{\bm{k}}(\br) \\
  \nabla_x \psi_{\bm{k}}(\br) &= \left(\frac{2}{L^3} \right)^{1/2} \phi'_k(x)e^{i k_{\rho} \rho} \\
  \nabla_{\rho} \psi_{\bm{k}}(\br) &= i k_{\rho} \left(\frac{2}{L^3} \right)^{1/2} \phi_k(x)e^{i k_{\rho} \rho} \\
  \psi^*_{\bm{k}}(\br)\nabla \psi_{\bm{k}}(\br) &= \hat{x} \frac{2}{L^3}\phi_k(x)\phi'_k(x) + i \hat{\rho} \frac{2}{L^3}|\phi_k(x)|^2 k_{\rho}\\
  \nabla n(\br) &= \frac{4}{L^3} \hat{x} \sum_{|\bm{k}|\leq \kf}\phi_k(x)\phi'_k(x) \\
  \nabla n(x) &= \frac{2}{\pi^2}\hat{x} \int_{0}^{\kf} (\kf^2-k^2) \phi_k(x)\phi'_k(x).
\end{align}
Last, the laplacian of the density is
\begin{align}
  \nabla^2 n(\br) &= \nabla \cdot \nabla n(\br) = \frac{\partial}{\partial x} \frac{2}{\pi^2} \int_{0}^{\kf} (\kf^2-k^2) \phi_k(x)\phi'_k(x)\\
  \nabla^2 n(x) &= \frac{2}{\pi^2} \int_{0}^{\kf} (\kf^2-k^2) \{|\phi'_k(x)|^2 + \phi_k(x)\phi''_k(x)\} \\
  \nabla^2 n(x) &= \frac{2}{\pi^2} \int_{0}^{\kf} (\kf^2-k^2) \{|\phi'_k(x)|^2 + |\phi_k(x)|^2[2v_s(x) + (\kf^2-k^2)]\}
\end{align}
using the KS equation for $\phi_k(x)$.

Last is the kinetic energy density. For the purposes of testing meta-GGAs, we're interested in the positive definite form
\begin{equation}
  \tau(\br) = \frac{1}{2}\sum_{|\bm{k}|\leq \kf} |\nabla \psi_{\bm{k}}(\br)|^2.
\end{equation}
We found earlier that
\begin{equation}
  \nabla \psi_{\bm{k}}(\br) = \left(\frac{2}{L^3} \right)^{1/2}\left[ \hat{x} \phi'_k(x) + \hat{\rho} k_{\rho}\phi_k(x) \right]e^{i k_{\rho} \rho}
\end{equation}
then
\begin{align}
  \tau(\br) &= \frac{L^3}{4 \pi^2} \int_{-\kf}^{\kf} d k \int_0^{\sqrt{\kf^2-k^2}} d k_{\rho}~ k_{\rho} \frac{2}{L^3}\left[ |\phi'_k(x)|^2 + k^2_{\rho} |\phi_k(x)|^2\right] \\
  \tau(\br) &= \frac{1}{2 \pi^2} \int_{-\kf}^{\kf} \left[ \frac{1}{2}(\kf^2-k^2)|\phi'_k(x)|^2 + \frac{1}{4}(\kf^2-k^2)^2|\phi_k(x)|^2 \right]dk\\
  \tau(\br) &= \frac{1}{2\pi^2} \int_{0}^{\kf} (\kf^2-k^2) \left[|\phi'_k(x)|^2 + \frac{1}{2}(\kf^2-k^2)|\phi_k(x)|^2 \right]dk.
\end{align}
Practically, $\phi'_k(x)$ must be evaluated using a high-order finite difference; values of $\mathcal{O}(h^6)$ coefficients are tabulated.



\end{document}
