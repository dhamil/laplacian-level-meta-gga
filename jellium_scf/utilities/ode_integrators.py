import numpy as np
from scipy.linalg import eigh

def numerov_driver(u,v,y0,y0p,h):
    """
        Numerov solver for problems of the form
        y''(x) = U(x) + V(x)y(x)
        bds = (x_min,x_max)
        u and v should be entered as vectors, not functions
        Needs y(x_0) and y'(x_0)
    """
    #y0*np.ones(1)
    nstep = u.shape[0]
    y = np.zeros(nstep)
    dir = int(np.sign(h))
    if dir > 0:
        ind = 0
    else:
        ind = -1
    y[ind] = y0

    h2 = h**2
    a = h2/12.0 # h**2/12.0, and not h, is used by the Numerov alogorithm
    """
        first step suggested by
        J.L.M. Quiroz González and D. Thompson,
        ``Getting started with Numerov's method''
        Computers in Physics 11, 514--515 (1997).
        doi: https://doi.org/10.1063/1.168593
    """
    b = ind
    c = ind + dir
    f = ind + 2*dir
    a11 = 1.0 - v[c]*h2/4.0
    a12 = v[f]*h2/24.0
    a21 = -2.0 - 5*v[c]*h2/6.0
    a22 = 1.0 - v[f]*a
    b1 = y0 + h*y0p + a*(7*(u[b] + v[b]*y0) + 6*u[c] - u[f])/2.0
    b2 = -y0 + a*(u[b] + v[b]*y0 + 10*u[c] + u[f])
    y[ind+dir] = (a22*b1 - a12*b2)/(a11*a22 - a12*a21)
    for step in range(1,nstep-1):
        b = ind + dir*(step-1)
        c = ind + dir*step
        f = ind + dir*(step+1)
        numer = (a*v[b]-1.0)*y[b] + 2*y[c]*(1.0 + 5.0*a*v[c]) + a*(u[b] + 10*u[c] + u[f])
        denom = 1.0 - v[f]*a
        y[f] = numer/denom
    return y

def eigen_matrix_numerov(v,h):

    npts = v.shape[0]
    lmat = np.zeros((npts,npts))
    rmat = np.zeros((npts,npts))
    a = h**2/12.0
    for ind in range(npts):
        if ind > 0:
            lmat[ind,ind-1] = a*v[ind-1] - 0.5
            rmat[ind,ind-1] = a
        lmat[ind,ind] = (1.0 + 10*a*v[ind])
        rmat[ind,ind] = 10*a
        if ind < npts-1:
            lmat[ind,ind+1] = a*v[ind+1] - 0.5
            rmat[ind,ind+1] = a
    e,psim = eigh(lmat,b=rmat)
    return psim,e

def numerov_shooting(u,v,y0,yn,y0p,h,opts={}):
    """
        shooting method for the Numerov ODE solver
        yn should be the boundary condition, y(x_N)
        ynp should be an initial guess for the initial derivative y'(x_0)

        optional: opts a dictionary of options
    """
    prec = 1.e-6
    if 'prec' in opts:
        prec = opts['prec']

    max_iterations = 20
    if 'max_iter' in opts:
        max_iterations = opts['max_iter']

    def shoot(yp1,yp2,y1,y2) :
        c1 = (y1 - y2)/(yp1 - yp2) # Slope of linear interpolation
        c2 = y1 - c1*yp1 # intercept of linear interpolation
        return (yn - c2)/c1 # returns third, interpolated guess for energy eigenvalue
    def soln(yp):
        return numerov_driver(u,v,y0,yp,h)

    y0p1 = y0p
    y0p2 = 1.5*y0p1
    yt1 = soln(y0p1)
    bp = abs(yt1[-1] - yn)
    yb = yt1

    for iter in range(max_iterations):

        yt2 = soln(y0p2)

        ndiff = abs(yt2[-1] - yn)
        # this is the solution that is closest to meeting the bdy condition
        # if the shooting method fails, this solution is returned
        if ndiff < bp:
            yb = yt2

        if ndiff < prec:
            return yt2
        else:
            y0p3 = shoot(y0p1,y0p2,yt1[-1],yt2[-2])
            if abs(y0p3 - y0p2) < 1.e-14:
                print('warning: Numerov-shooting solver could not find unique initial conditions')
                return yb
            yt1 = yt2
            y0p1 = y0p2
            y0p2 = y0p3

    if iter == max_iterations-1:
        print('warning: Numerov-shooting solver could not satisfy bdy conditions')
        return yb

def fdiff_coeffs(dir,order):

    """
        fun_tab should be tabulated as a vector
        h is the spacing
        dir = cen(tral) or fwd (forward) for method of finite differences
        order is the order of accuracy

        Coefficients from
        B. Fornberg,
        ``Generation of finite difference formulas on arbitrarily spaced grids''
        Math. Comp. 51, 699 (1988),
        DOI: https://doi.org/10.1090/S0025-5718-1988-0935077-0
    """
    if dir == 'cen':
        coeff_d = {
        2: [-1.0/2.0, 0.0, 1.0/2.0],
        4: [1.0/12.0,-2.0/3.0,0.0,2.0/3.0,-1.0/12.0],
        6: [-1.0/60.0,3.0/20.0,-3.0/4.0,0.0,3.0/4.0,-3.0/20.0,1.0/60.0],
        8: [1.0/280.0,-4.0/105.0,1.0/5.0,-4.0/5.0,0.0,4.0/5.0,-1.0/5.0,4.0/105.0,-1.0/280.0]
        }
        # of the form + h, +2 h, + 3 h, + 4 h
        # Table 1, ibid.
    elif dir == 'fwd':
        coeff_d = {
        1: [-1.0,1.0],
        2: [-3.0/2.0,2.0,-1.0/2.0],
        3: [-11.0/6.0,3.0,-3.0/2.0,1.0/3.0],
        4: [-25.0/12.0,4.0,-3.0,4.0/3.0,-1.0/4.0],
        5: [-137.0/60.0,5.0,-5.0,10.0/3.0,-5.0/4.0,1.0/5.0],
        6: [-49.0/20.0,6.0,-15.0/2.0,20.0/3.0,-15.0/4.0,6.0/5.0,-1.0/6.0],
        7: [-363.0/140.0,7.0,-21.0/2.0,35.0/3.0,-35.0/4.0,21.0/5.0,-7.0/6.0,1.0/7.0],
        8: [-761.0/280.0,8.0,-14.0,56.0/3.0,-35.0/2.0,56.0/5.0,-14.0/3.0,8.0/7.0,-1.0/8.0]
        } # of the form +0, + h, +2 h, + 3 h, + 4 h,...
        # Table 3, ibid.
    return np.asarray(coeff_d[order])

if __name__=="__main__":

    x,n,gn,lap,tau = np.transpose(np.genfromtxt('../data/surface/rs_4/rs_4_density.csv',delimiter=',',skip_header=1))
    x,esp = np.transpose(np.genfromtxt('../data/surface/rs_4/rs_4_esp.csv',delimiter=',',skip_header=1))
    import matplotlib.pyplot as plt
    #plt.plot(x,esp)

    #x,n,esp2 = np.transpose(np.genfromtxt('../jellium_surface_fort/test.txt',delimiter='',skip_header=1))
    desp2d2 = np.zeros(x.shape[0]-2)
    for i in range(1,x.shape[0]-2):
        desp2d2[i] = (esp[i+1] -2*esp[i] + esp[i-1])/(x[1]-x[0])**2
    plt.plot(x[1:-1],desp2d2)

    nb = np.zeros_like(x)
    n0 = 3/(4*np.pi*4**3)
    nb[x<0.0] = n0
    u = -4*np.pi*(n - nb)
    plt.plot(x[1:-1],u[1:-1])
    plt.show()
    exit()

    fn = np.zeros_like(x)
    iind = x.shape[0]-1#np.argmin(np.abs(x))
    dphi0 = -(x[1]-x[0])*np.sum(u)#(esp[1]-esp[0])/(x[1]-x[0])#
    fn[:iind+1] = numerov_driver(u[:iind+1],np.zeros_like(x),esp[0],dphi0,x[-1]-x[-2])
    ctmp = fn[iind]
    #fn[iind:] = numerov_driver(u[iind:],np.zeros_like(x),esp[-1],0.0,x[-2]-x[-1])
    #fn[iind:] += ctmp - fn[iind]

    plt.plot(x,fn)
    plt.show()
    exit()

    h = 0.01
    x = np.arange(0.0,5.0+h,h)
    v = x**2-1.0
    y0 = 1
    yn = 0.0
    y0p = -1.0
    #y = numerov_shooting(np.zeros(x.shape[0]),v,y0,yn,y0p,h)
    y = numerov_driver(np.zeros(x.shape[0]),v,np.exp(-x[-1]**2/2.0),-x[-1]*np.exp(-x[-1]**2/2.0),-h)
    y /= (2*h*np.sum(y**2))**(0.5)
    import matplotlib.pyplot as plt
    plt.plot(x,y)
    plt.plot(x,np.exp(-x**2/2.0)/(np.pi**(0.25)))
    plt.show()
    exit()

    h = 0.01
    bds = (1.e-10,6)
    x = np.arange(bds[0],bds[1],h)
    v = -2/x + 1.0
    y = numerov_driver(np.zeros(x.shape[0]),v,0.0,1.0,h)
    import matplotlib.pyplot as plt
    plt.plot(x,y)
    plt.show()
