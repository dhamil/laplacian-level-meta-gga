import numpy as np

def linear_mixing(n_in,n_out,mixpar=0.1):
    return (1.0-mixpar)*n_in + mixpar*n_out

def initiate_modified_broyden(npts):
    """
        handy utility for allocating arrays and quantities needed for modified
        Broyden density mixing scheme
    """
    F = 0.0
    dF = np.zeros((0,npts))
    u_vec = np.zeros((0,npts))
    a_mat = np.zeros((0,0))
    mix_wg = np.zeros(0)
    return F,dF,u_vec,a_mat,mix_wg

def modified_broyden(int_wg,n_in,n_out,f_in,df,u,amat,mix_wg,iter,g0_par=0.35,wj = 1.e-2):

    """
        Adapted from:
        D.D. Johnson,
        ``Modified Broyden's method for accelerating convergence in self-consistent calculations'',
        Phys. Rev. B 38, pp. 12807-12813 (1988).
        https://doi.org/10.1103/PhysRevB.38.12807
    """

    def abs_int(targ):
        return np.sum(int_wg*np.abs(targ)**2)

    dim = iter+1
    imat = np.identity(dim)

    #g0 = g0_par*imat
    f_out = n_out - n_in # mth iteration of F_m = n_out_m - n_in_m

    norm = abs_int(f_out-f_in)
    df_out = (f_out-f_in)/norm # (m-1)th iteration of delta F_m
    df = np.vstack((df,df_out))

    dn_out = (n_in - n_out)/norm
    u_out = g0_par*df_out + dn_out
    u = np.vstack((u,u_out))

    mix_wg = np.append(mix_wg,(abs_int(f_out))**(-0.5)) # mixing weights

    new_a = np.zeros((dim,dim))
    new_a[:dim-1,:dim-1] = amat
    #for ind in range(amat.shape[0]):
    #    for ind2 in range(amat.shape[1]):
    #        new_a[ind,ind2] = amat[ind,ind2]
    for ind in range(dim):
        # only need the mth column and mth row of updated a matrix
        new_a[dim-1,ind] = mix_wg[dim-1]*mix_wg[ind]*np.sum(int_wg*df[ind]*f_out[dim-1])
        new_a[ind,dim-1] = mix_wg[dim-1]*mix_wg[ind]*np.sum(int_wg*df[dim-1]*f_out[ind])

    beta = np.linalg.inv(wj**2*imat + new_a)

    cv = np.zeros(dim-1)
    gamma = np.zeros(dim)
    for ind in range(dim-1):
        cv[ind] = mix_wg[ind]*np.sum(int_wg*df[ind]*f_out)
        gamma += cv[ind]*beta[ind,:]

    n_next = n_in + g0_par*f_out
    for iu in range(dim-1):
        n_next -= mix_wg[iu]*gamma[iu]*u[iu]#np.sum(mix_wg*gamma*au)
    n_next[n_next<1.e-14]=1.e-14

    if np.sum(int_wg*np.abs(n_next)**2) > 10*np.sum(int_wg*np.abs(n_out)**2):
        n_next = n_in + g0_par*f_out

    return n_next,f_out,df,mix_wg,u,new_a
