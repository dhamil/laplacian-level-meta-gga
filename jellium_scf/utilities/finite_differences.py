import numpy as np

def fdiff_coeffs(ord,alp):

    """
        B. Fornberg,
        ``Generation of finite difference formulas on arbitrarily spaced grids''
        Math. Comp. 51, 699 (1988),
        https://doi.org/10.1090/S0025-5718-1988-0935077-0
    """

    npts = len(alp)
    mval = ord
    nval = npts-1

    d = np.zeros((nval+1,nval+1,mval+1))

    d[0,0,0] = 1.0
    c1 =1.0
    for t in range(1,nval+1):
        c2 = 1.0
        for nu in range(t):
            c3 = (alp[t]-alp[nu])
            c2 *=c3
            for m in range(min(t,mval)+1):
                d[t,nu,m]=(alp[t]*d[t-1,nu,m]-m*d[t-1,nu,m-1])/c3
        for m in range(min(t,mval)+1):
            d[t,t,m] = c1/c2*(m*d[t-1,t-1,m-1]-alp[t-1]*d[t-1,t-1,m])
        c1 = c2

    return d[nval,:,ord]

def discrete_laplacian(x,h,fac=1.0,uneven=False):
    """
        by default, uses a 12th order finite differences method
        use fac to control the prefactor of the operator,
        i.e. fac = -1/2 gives the 1D KE operator in the Schroedinger eqn.
    """
    if uneven:
        nfdc = 6
    else:
        fdc = fdiff_coeffs(2,np.arange(-6,7,1))
        lfdc = fdc.shape[0]
        nfdc = int((lfdc-1)/2)

    nx = x.shape[0]
    nabla = np.zeros((nx,nx))
    if uneven:
        pfc = fac
    else:
        pfc = fac/h**2

    for ind in range(nx):
        lind = max(ind-nfdc,0)
        uind = min(ind+nfdc+1,nx)
        if uneven:
            fdc = fdiff_coeffs(2,x[lind:uind])
            nabla[ind,lind:uind] = pfc*fdc
        else:
            clind = max(nfdc-ind,0)
            cuind = min(nx-ind+nfdc,lfdc)
            nabla[ind,lind:uind] = pfc*fdc[clind:cuind]

    return nabla,nfdc

if __name__=="__main__":
    print(fdiff_coeffs(1,np.arange(0,-7,-1)))
    print(fdiff_coeffs(1,np.arange(0,7,1)))
    """
    wg,pt = np.transpose(np.genfromtxt('./grids/gauss_legendre_20_pts.csv',delimiter=',',skip_header=1))
    pt = 0.5*10*(pt+1.0)
    nabla,_ = discrete_laplacian(pt,None,fac=1.0,uneven=True)
    """
    nabla,_ = discrete_laplacian(np.arange(0,10,1),1,fac=1.0)
    print(np.abs(nabla - np.transpose(nabla)).max())
    exit()
    print(fdiff_coeffs(2,np.arange(-5,6,1)))
