import numpy as np

import constants
pi = constants.pi

def exc_lsda(nu_in,nd_in,sep=False):
    nu = np.zeros(nu_in.shape)
    nd = np.zeros(nd_in.shape)
    nu[:] = nu_in
    nd[:] = nd_in
    nu[nu<1.e-14] = 1.e-14
    nd[nd<1.e-14] = 1.e-14
    ex,vxu,vxd = ex_lsda(nu,nd)
    n = nu + nd
    z = np.minimum(np.maximum((nu - nd)/n,-1.0),1.0)
    #rs = (3.0/(4.0*pi*n))**(1.0/3.0)
    ec,vcu,vcd = eps_c_pw92(n,z)
    if sep:
        return ex,ec,vxu,vxd,vcu,vcd
    else:
        return ex+ec, vxu+vcu, vxd+vcd

def ex_lsda(nu,nd):
    if hasattr(nu,'__len__'):
        if len(nu[nu==0.0]) == len(nu): # one-electron systems
            exd,vxd = lda_x(2*nd)
            exu = np.zeros(nd.shape)
            vxu = np.zeros(nd.shape)
        elif len(nd[nd<1.e-14]) == len(nd):# one-electron systems
            one_elec = True
            exu,vxu = lda_x(2*nu)
            exd = np.zeros(nu.shape)
            vxd = np.zeros(nu.shape)
        else:
            exu,vxu = lda_x(2*nu)
            exd,vxd = lda_x(2*nd)
    else:
        if nu == 0.0: # one-electron systems
            exd,vxd = lda_x(2*nd)
            vxu = 0.0
            exu = 0.0
        elif nd == 0.0:# one-electron systems
            exu,vxu = lda_x(2*nu)
            vxd = 0.0
            exd = 0.0
        else:
            exu,vxu = lda_x(2*nu)
            exd,vxd = lda_x(2*nd)

    return 0.5*(exu + exd), vxu, vxd

def lda_x(n):
    ax = -3.0/(4.0*pi)
    kf = (3*pi**2*n)**(1.0/3.0)
    return ax*kf, -kf/pi

def eps_c_pw92(n,z):
    """
        J.P. Perdew and Y. Wang,
        ``Accurate and simple analytic representation of the electron-gas correlation energy'',
        Phys. Rev. B 45, 13244 (1992).
        https://doi.org/10.1103/PhysRevB.45.13244
    """
    rs = (3.0/(4.0*pi*n))**(1.0/3.0)
    def g(v,rs):
        q0 = -2.0*v[0]*(1.0 + v[1]*rs)
        q1 = 2.0*v[0]*(v[2]*rs**(0.5) + v[3]*rs + v[4]*rs**(1.5) + v[5]*rs**2)
        q1p = v[0]*(v[2]*rs**(-0.5) + 2*v[3] + 3*v[4]*rs**(0.5) + 4*v[5]*rs)
        dg = -2*v[0]*v[1]*np.log(1.0 + 1.0/q1) - q0*q1p/(q1**2 + q1)
        return q0*np.log(1.0 + 1.0/q1),dg

    if not hasattr(z,'__len__') and z == 0.0:
        ec,d_ec_drs = g([0.031091,0.21370,7.5957,3.5876,1.6382,0.49294],rs)
        vc = ec - rs/3.0*d_ec_drs
        return ec,vc,vc

    fz = (1.0+z)**(4.0/3.0) + (1.0-z)**(4.0/3.0)-2.0
    fz /= 2.0**(4.0/3.0)-2.0
    dfz = 4.0/3.0*( (1.0 +z)**(1.0/3.0) - (1.0 -z)**(1.0/3.0))
    dfz /= 2.0**(4.0/3.0)-2.0
    fdd0 = 8.0/9.0/(2.0**(4.0/3.0)-2.0)

    ec0,d_ec_drs_0 = g([0.031091,0.21370,7.5957,3.5876,1.6382,0.49294],rs)
    ec1,d_ec_drs_1 = g([0.015545,0.20548,14.1189,6.1977,3.3662,0.62517],rs)
    ac,d_ac_drs = g([0.016887,0.11125,10.357,3.6231,0.88026,0.49671],rs)
    ac *= -1.0
    d_ac_drs *= -1.0
    ec = ec0 + ac*fz/fdd0*(1.0 - z**4) + (ec1 - ec0)*fz*z**4

    d_ec_drs = d_ec_drs_0*(1.0 - fz*z**4) + d_ec_drs_1*fz*z**4
    d_ec_drs += d_ac_drs*fz/fdd0*(1.0 - z**4)

    d_ec_dz = 4*z**3*fz*(ec1 - ec0 - ac/fdd0)
    d_ec_dz += dfz*(z**4*ec1 - z**4*ec0 + (1.0 - z**4)*ac/fdd0)

    vcu = ec - rs/3.0*d_ec_drs - (z - 1.0)*d_ec_dz
    vcd = ec - rs/3.0*d_ec_drs - (z + 1.0)*d_ec_dz

    return ec,vcu,vcd

def ec_rpa_unp(rs):
    """
        J.P. Perdew and Y. Wang,
        ``Accurate and simple analytic representation of the electron-gas correlation energy'',
        Phys. Rev. B 45, 13244 (1992).
        https://doi.org/10.1103/PhysRevB.45.13244
    """
    def g(v,rs):
        q0 = -2.0*v[0]*(1.0 + v[1]*rs)
        q1 = 2.0*v[0]*(v[2]*rs**(0.5) + v[3]*rs + v[4]*rs**(1.5) + v[5]*rs**(1.75))
        return q0*np.log(1.0 + 1.0/q1)
    return g([0.031091,0.082477,5.1486,1.6483,0.23647,0.20614],rs)


def ec_pz81(n,z):
    """
    J. P. Perdew and A. Zunger,
    ``Self-interaction correction to density-functional approximations for many-electron systems''
    Phys. Rev. B 23, 5048 (1981).
    https://doi.org/10.1103/PhysRevB.23.5048
    """
    rs = (3.0/(4.0*pi*n))**(1.0/3.0)

    fz = ((1.0+z)**(4.0/3.0) + (1.0-z)**(4.0/3.0) -2.0)/(2.0**(4.0/3.0)-2.0)
    dfz = 4.0/3.0*( (1.0 +z)**(1.0/3.0) - (1.0 -z)**(1.0/3.0))/(2.0**(4.0/3.0)-2.0)

    # first entry is for spin-unpolarized, second for spin-polarized
    gamma = [-0.1423, -0.0843]
    beta1 = [1.0529, 1.3981]
    beta2 = [0.3334, 0.2611]
    A = [0.0311, 0.01555]
    B = [-0.048, -0.0269]
    C = [0.0020, 0.0007]
    D = [-0.0116, -0.0048]
    ecd = {}
    d_ecd_drs = {}
    lmask = rs < 1.0
    umask = rs >= 1.0
    if not hasattr(z,'__len__') and z == 0.0:
        spin = 1
    else:
        spin = 2

    if hasattr(rs,'__len__'):
        rsl = rs[lmask]
        rsu = rs[umask]
        ecd = np.zeros((2,rs.shape[0]))
        d_ecd_drs = np.zeros((2,rs.shape[0]))

        for sigma in range(spin):
            ecd[sigma][umask] = gamma[sigma]/(1.0 + beta1[sigma]*rsu**(0.5) + beta2[sigma]*rsu)
            d_ecd_drs[sigma][umask] = -(ecd[sigma][umask])**2/gamma[sigma]*(0.5*beta1[sigma]/rsu**(0.5) + beta2[sigma])

            ecd[sigma][lmask] = A[sigma]*np.log(rsl) + B[sigma] + C[sigma]*rsl*np.log(rsl) + D[sigma]*rsl
            d_ecd_drs[sigma][lmask] = A[sigma]/rsl + C[sigma]*(1 + np.log(rsl)) + D[sigma]
    else:
        ecd = np.zeros(2)
        d_ecd_drs = np.zeros(2)
        for sigma in range(spin):
            if lmask:
                ecd[sigma] = A[sigma]*np.log(rs) + B[sigma] + C[sigma]*rs*np.log(rs) + D[sigma]*rs
                d_ecd_drs[sigma] = A[sigma]/rs + C[sigma]*(1 + np.log(rs)) + D[sigma]
            else:
                ecd[sigma] = gamma[sigma]/(1.0 + beta1[sigma]*rs**(0.5) + beta2[sigma]*rs)
                d_ecd_drs[sigma] = -(ecd[sigma])**2/gamma[sigma]*(0.5*beta1[sigma]/rs**(0.5) + beta2[sigma])

    if spin == 1:
        vc = ecd[0] - rs/3.0*d_ecd_drs[0]
        return ecd[0],vc,vc

    ec = ecd[0] + fz*(ecd[1] - ecd[0])
    d_ec_dz = ecd[0] + dfz*(ecd[1] - ecd[0])
    d_ec_drs = d_ecd_drs[0] + fz*(d_ecd_drs[1] - d_ecd_drs[0])

    vcu = ec - rs/3.0*d_ec_drs - (z - 1.0)*d_ec_dz
    vcd = ec - rs/3.0*d_ec_drs - (z + 1.0)*d_ec_dz

    return ec,vcu,vcd
