import numpy as np
from scipy.optimize import minimize_scalar

import settings
import constants
pi = constants.pi

#def e_tf(n,vh,eps_xc):
#    ke = 3.0/10.0*(3*pi**2*n)**(2.0/3.0)

# Adapted from Lee, Constantin, Perdew, and Burke, JCP 130, 034107 (2009)
# https://doi.org/10.1063/1.3059783
def phimod(x):
    y = x**(0.5)
    bmaj=1.5880710226
    cfa = np.array([-bmaj,4.0/3.0,0.0,-2.0*bmaj/5.0,1.0/3.0,3.0*bmaj**2/70.0,-2.0*bmaj/15.0,2.0/27.0+bmaj**3/252.0])
    cfb = np.array([-0.0144050081,0.0231427314,-0.00617782965,0.0103191718,-0.000154797772,0.0,0.0,0.0])
    numr = 1.0
    denom = 1.0 + cfa[len(cfa)-1]*y**(15)/144.0

    dydx = 0.5/y
    d_numr_dy = 0.0
    d_denom_dy = 15*cfa[len(cfa)-1]*y**(14)/144.0

    for i in range(len(cfa)):
        numr += cfa[i]*y**(i+2)
        denom += cfb[i]*y**(i+10)

        d_numr_dy += (i+2)*cfa[i]*y**(i+1)
        d_denom_dy += (i+10)*cfb[i]*y**(i+9)

    return numr/denom,(d_numr_dy/denom -numr*d_denom_dy/denom**2)*dydx

def n_tf_atomic(x,z):
    ac = (0.75*pi)**(2.0/3.0)/2.0
    fm,dfm = phimod(z**(1.0/3.0)*x/ac)
    fm = np.maximum(fm,np.zeros(x.shape))
    dfm *= z**(1.0/3.0)/ac
    return (z*fm/x/ac)**(1.5)/(4.0*pi),1.5*((z/ac)**3*fm/x)**(0.5)*(dfm/x - fm/x**2)/(4.0*pi)

def tf_dens(mu,vs):
    ntf_new = 1.e-14*np.ones(vs.shape[0])
    ntfc = mu - vs
    car = ntfc>0.0
    ntf_new[car] = 1.0/(3*pi**2)*(2*ntfc[car])**(1.5)
    return ntf_new

def tf_dens_scf(r,rwg,vs,nelec,n0):
    """
        vs should be a callable KS potential
        nelec should be the number of electrons
        n0 should be an initial guess for the charge density
    """

    #n0,_ = n_tf_atomic(r,nelec) # instantiate with neutral TF atomic density
    def int_n(nn):
        return 4*pi*np.sum(rwg*r**2*nn)

    for i_adj_mu in range(200):

        vks = vs(n0)

        def opt_mu(mu):
            nnew = tf_dens(mu,vks)
            return abs(nelec - int_n(nnew))
        # use a root finder to optimize mu at fixed KS potential
        #res = minimize_scalar(opt_mu,bounds=(1.01*vks.min(),10*abs(vks.max())),method='bounded')
        #mu = res.x
        #print(mu,res.fun)
        #"""
        # linear optimizing
        mu1 = 1.01*vks.max()
        mu2 = 1.1*mu1
        err1 = opt_mu(mu1)
        berr = err1
        mu = mu1

        for j_adj_mu in range(50):

            #if mu2 < 1.001*vks.max():
            #    break
            err2 = opt_mu(mu2)
            if mu2 != mu2:
                break

            if err2 < berr:
                berr = err2
                mu = mu2
            if berr < .01:
                break
            else:
                c1 = (err2 - err1)/(mu2 - mu1)
                c2 = err1 - c1*mu1
                mu3 = -c2/c1
                err1 = err2
                mu1 = mu2
                mu2 = mu3
        #"""
        n1 = tf_dens(mu,vks)
        if abs(nelec - int_n(n1)) < .001:
            break
        else:
            n0 = n1
    return n1
