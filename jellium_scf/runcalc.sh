#!/bin/bash
#PBS -l walltime=2:00:00
#PBS -q normal
#PBS -l nodes=1:ppn=20
#PBS -N jellium_surface
#PBS -j oe
#PBS -o oe.txt
#PBS -m abe
#PBS -M kaplan@temple.edu
cd "$PBS_O_WORKDIR"

python3 main.py
