import numpy as np
from settings import dataset
"""
    This file simply contains reference data needed for appropriate norm calculations

    citations included in references.txt
"""

"""
--------------------------------------------------------------------------------
    Atomic data
--------------------------------------------------------------------------------
"""

atom_xc = {'H': -0.3125, 'He': -1.068}

# Exact exchange energies from [SP2019]
exx = {'Ne': -12.108,'Ar': -30.188,'Kr': -93.890, 'Xe': -179.200}
# exact correlation energies from [BCGP2016]
ecc = {'Ne': -0.0391*10, 'Ar': -0.0403*18, 'Kr': -0.0514*36, 'Xe': -0.0556*54}
excx = {}
for akey in exx:
    excx[akey] = exx[akey] + ecc[akey]

# large-Z asymptotic series coefficients calculated with this routine and the LSDA values in coeff.py.
# Defined in [SRP2015]
if dataset == 'CR74':
    #lda_ref_x = np.asarray([-11.033435988832077, -27.862900108627116, -88.62382802941022, -170.56235963848704])
    lda_ref_x = np.asarray([-11.033435988831796, -27.862900108629965, -88.62382804347148, -170.56235834320242])
    lzx1 = -0.2259166064522402
    lzx2 = 0.25531527525086517
elif dataset == 'BBB93':
    lda_ref_x = np.asarray([-11.033492312459403, -27.863057726034135, -88.6239545505498, -170.5653884722815])
    lzx1 = -0.22583372606304986
    lzx2 = 0.25513305469366987
elif dataset == 'K99':
    lda_ref_x = np.asarray([-11.033479640902536, -27.863064147615933, -88.6239865115906, -170.56546445043608])
    lzx1 = -0.2258288712191412
    lzx2 = 0.25512029245333423

lzc1 = 0.038732731104756446
lzxc1 = lzx1 + lzc1

"""
lzx1 = -0.22591656886614597 # also called Bx
lzx2 = 0.2553151855588318 # also called Cx
ax = 0.22082853204075997 # calculated here

lzc1 = 0.038732731104756446 # also called Bc
"""


"""
--------------------------------------------------------------------------------
    Jellium data
--------------------------------------------------------------------------------
"""

# extracted from jellium_norms.get_dmc_ref_vals. In mhartree/bohr, gamma_DMC - gamma_LSD
dmc_ref_delta_gamma_xc = {2: -1.2372426894580084, 3.25: -0.4548723711660518, 4: -0.3104198153041719}#, 5.62: None}

# extracted from jellium_norms.fit_lsd_gamma_xc
gamma_xc_lsd_pars = [2.05672187, 0.22969907, 3.80638849]

# Exact exchange results in erg/cm^2 [PE2001]
jellium_surface_x_ref = {2: 2624.0, 3: 526.0, 4: 157.0, 5: 57.0}

# DMC reference total energies per electron (eV/electron) for jellium clusters from [SB2001]
jellium_cluster_dmc_ref = {2: {2:-0.7147, 8:-0.4181, 18:-0.1561, 20:-0.2623, 34:-0.0874, 40:-0.1537, 58:-0.0780, 92:-0.0778, 106: -0.0289},
3.25: {2: -1.6716, 8: -1.7379, 18: -1.7306, 20: -1.7615, 34: -1.7668, 40: -1.7656, 58: -1.8032, 92: -1.8266, 106: -1.7891},
4: {2: -1.7440, 8: -1.8349, 18: -1.8624, 20: -1.8784, 34: -1.9035, 40: -1.8972, 58: -1.9390, 92: -1.9626, 106: -1.9326},
#5.62: {2: -1.6485, 8: -1.7377, 18: -1.7758, 20: -1.7804, 34: -1.8126, 40: -1.8026, 58: -1.8419, 92: -1.8605, 106: -1.8417}
#{2:, 8:, 18:, 20:, 34:, 40:, 58:, 92:, 106: }
}
# LSDA total energies per electron (eV/electron) for jellium clusters from [SB2001]; used to avoid differences in SCF densities
jellium_cluster_lsda_ref = {2: {2:-0.5188, 8:-0.3580, 18:-0.1446, 20:-0.2492, 34:-0.0933, 40:-0.1644, 58:-0.0957, 92:-0.1009, 106:-0.0599},
3.25: {2: -1.5803, 8: -1.7213, 18: -1.7376, 20: -1.7687, 34: -1.7807, 40: -1.7861, 58: -1.8216, 92: -1.8489, 106: -1.8153},
4: {2:-1.682, 8:-1.8336, 18:-1.8732, 20:-1.8899, 34:-1.9197, 40:-1.9158, 58:-1.9582, 92:-1.9836, 106:-1.9561 },
#5.62: {2: -1.6230, 8: -1.7467, 18: -1.7911, 20: -1.7959, 34:-1.8301, 40: -1.8215, 58: -1.8594, 92:-1.8786, 106: -1.8605}
#{2:, 8:, 18:, 20:, 34:, 40:, 58:, 92:, 106: }
}

for rs in jellium_cluster_dmc_ref:
    for nn in jellium_cluster_dmc_ref[rs]:
        jellium_cluster_dmc_ref[rs][nn] -= jellium_cluster_lsda_ref[rs][nn]

def sigma_xc_reference(rs,pars='RPA+'):
    """
        Fit function and parameters for pars = LSDA, RPA+ from Eq. 15 / Table XI of [APF2002]

        Parameters for pars = DMC and ISTLS from Eq. 5 of [C2008]

        RPA+ values from [YPK2000]
    """
    sxc = {}
    if pars == 'RPA+':
        sxc = {2: 3413.0, 3: 781.0, 4: 268.0, 5: 113.0, 6: 54.0}
    if rs in sxc:
        # best practice is to use the known value
        return sxc[rs]
    else:
        # failing that, use interpolated values
        x = (1.0 + rs)**(0.5) - 1.0
        if pars=='DMC':
            cf = [50000.0,0.6549,-0.511,0.248]
        elif pars == 'ISTLS':
            cf = [50000.0,0.7437,-0.653,0.248]
        elif pars == 'LSDA':
            cf = [50695,0.74651,-0.57888,0.25146]
        elif pars == 'RPA+':
            cf = [52227.0, 0.87924, -0.79810, 0.34685]
        denom = 1.0 + cf[1]*x + cf[2]*x**2 + cf[3]*x**3
    return cf[0]/(rs**(7.0/2.0)*denom)

def fit_lsd_gamma_xc():
    lsd_ref_curv = np.asarray([[1.58, .890], [2.07, .442], [2.3, .318], [2.65, .197], [3.24,.098], [3.27, .094], [3.57, .068], [3.71, .059], [3.93, .047], [4.86,.020], [5.2, .016], [5.62, .016]])
    p0 = np.asarray([2*lsd_ref_curv[0,1],1.0,0.0])
    p0[2] = np.log(p0[0]/lsd_ref_curv[-1,1]-1.0)/np.log(lsd_ref_curv[-1,0]/lsd_ref_curv[0,0])
    p0[1] = 1.0/lsd_ref_curv[0,0]**p0[2]
    def fit_fn(rs,p):
        return 2*rs*p[0]/(1.0 + p[1]*rs**p[2])
    def resid(p):
        return np.sum((2*lsd_ref_curv[:,0]*lsd_ref_curv[:,1]-fit_fn(lsd_ref_curv[:,0],p))**2)
    from scipy.optimize import minimize
    opr = minimize(resid,p0)
    """
    import matplotlib.pyplot as plt
    plt.scatter(lsd_ref_curv[:,0],2*lsd_ref_curv[:,0]*lsd_ref_curv[:,1])
    rsl = np.linspace(1.5,5.7,2000)
    plt.plot(rsl,fit_fn(rsl,opr.x))
    plt.xlabel('$r_s$',fontsize=16)
    plt.ylabel('$\gamma^{\\mathrm{LSD}}_{xc}$',fontsize=16)
    plt.show()
    """
    return opr

def gamma_xc_lsd_ref(rs):
    pars = gamma_xc_lsd_pars
    return 2*rs*pars[0]/(1.0 + pars[1]*rs**pars[2])
