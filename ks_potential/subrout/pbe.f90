

subroutine pbe_x(nu,nd,gnu,gnd,ex,vxu,vxd,vxgu,vxgd)

  implicit none

  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), intent(in) :: nu,nd,gnu,gnd
  real(dp), intent(out) :: ex,vxu,vxd,vxgu,vxgd

  real(dp) :: tmp

  ex = 0._dp ; vxu = 0._dp ; vxd = 0._dp
  vxgu = 0._dp ; vxgd = 0._dp

  if (nu > 0._dp) then
    call pbe_x_unpol(2._dp*nu,2._dp*gnu,tmp,vxu,vxgu)
    ex = ex + tmp/2._dp
  endif
  if (nd > 0._dp) then
    call pbe_x_unpol(2._dp*nd,2._dp*gnd,tmp,vxd,vxgd)
    ex = ex + tmp/2._dp
  endif

end subroutine pbe_x


subroutine pbe_x_unpol(n,gn,ex,vxn,vxgn)

  implicit none

  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), parameter :: pi = 3.14159265358979323846264338327950288419_dp
  real(dp), parameter :: kappa = 0.804_dp, mu = 0.21951_dp!64512208958307315498359457706101238727569580078125_dp

  real(dp), intent(in) :: n,gn
  real(dp), intent(out) :: ex,vxn,vxgn

  real(dp) :: kf,exlda,p,fx,d_exlda_dn,d_p_dn,d_p_dgn,d_fx_dp

  kf = (3*pi**2*n)**(1._dp/3._dp)
  exlda = -3._dp/(4._dp*pi)*kf*n

  p = (gn/(2*kf*n))**2
  fx = 1._dp + kappa - kappa/(1._dp + mu*p/kappa)
  ex = fx*exlda

  d_exlda_dn = -kf/pi

  d_p_dn = -8._dp/3._dp *p/n
  d_p_dgn = gn/(2*(kf*n)**2)

  d_fx_dp = mu/(1._dp + mu*p/kappa)**2

  vxn = fx*d_exlda_dn + exlda*d_fx_dp*d_p_dn
  vxgn = exlda*d_fx_dp*d_p_dgn

end subroutine pbe_x_unpol


subroutine pbe_c(nu,nd,gnu,gnd,gn,ec,vcu,vcd,vcgu,vcgd,vcg)

  implicit none

  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), parameter :: pi = 3.14159265358979323846264338327950288419_dp
  real(dp),parameter :: gamma = 0.0310906908696549_dp, beta = 0.066725_dp
  real(dp), intent(in) :: nu,nd,gnu,gnd,gn
  real(dp), intent(out) :: ec,vcu,vcd,vcgu,vcgd,vcg

  real(dp) :: n,kf,rs,zeta,phi,gp3,s,t,t2,ec_lsda,d_ecl_drs,d_ecl_dzeta
  real(dp) :: aa,v,v2,gt,hc,h
  real(dp) :: d_rs_dn,d_zeta_du,d_zeta_dd,grad_dot,d_gn_dgu,d_gn_dgd
  real(dp) :: d_s_dgn,d_s_dn,d_phi_dzeta,dgp3_dzeta
  real(dp) :: d_t_drs,d_t_ds,d_t_dzeta,d_aa_drs,d_aa_dzeta
  real(dp) :: d_v_drs,d_v_ds,d_v_dzeta,d_gt_drs,d_gt_ds,d_gt_dzeta
  real(dp) :: d_h_drs,d_h_ds,d_h_dzeta

  n = nu + nd
  kf = (3*pi**2*n)**(1._dp/3._dp)
  rs = (3._dp/(4._dp*pi*n))**(1._dp/3._dp)
  zeta = (nu - nd)/n
  zeta = min(max(-0.99999999999990_dp,zeta),0.99999999999990_dp)

  phi = ((1._dp + zeta)**(2._dp/3._dp) &
 &      + (1._dp - zeta)**(2._dp/3._dp))/2._dp
  gp3 = gamma*phi**3

  s = gn/(2*kf*n)
  t = (3*pi**2/16._dp)**(1._dp/3._dp)*s/(phi*rs**(0.5_dp))
  t2 = t*t

  call ec_pw92(rs,zeta,ec_lsda,d_ecl_drs,d_ecl_dzeta)

  aa = beta/gamma/(exp(-ec_lsda/gp3)-1._dp)
  v = aa*t2
  v2 = v*v
  gt = t2*(1._dp + v)/(1._dp + v + v2)

  hc = 1._dp + beta/gamma*gt
  h = gp3*log(hc)
  ec = (ec_lsda + h)*n

  d_rs_dn = -rs/n/3._dp

  d_zeta_du = 2._dp*nd/n**2
  d_zeta_dd = -2._dp*nu/n**2

  grad_dot = (gn**2 - gnu**2 - gnd**2)/2._dp ! gnu_vec . gnd_vec
  d_gn_dgu = (gnu + grad_dot/gnu)/gn
  d_gn_dgd = (gnd + grad_dot/gnd)/gn

  d_s_dgn = 1._dp/(2*kf*n)
  d_s_dn = -4._dp/3._dp*s/n

  d_phi_dzeta = ((1._dp + zeta)**(-1._dp/3._dp) &
 &      - (1._dp - zeta)**(-1._dp/3._dp))/3._dp
  dgp3_dzeta = 3*gamma*phi**2*d_phi_dzeta

  d_t_drs = -0.5_dp*t/rs
  d_t_dzeta = -t/phi*d_phi_dzeta
  d_t_ds = (3*pi/16._dp)**(1._dp/3._dp)/(phi*rs**(0.5_dp))

  d_aa_drs = aa*d_ecl_drs*exp(-ec_lsda/gp3)/(exp(-ec_lsda/gp3)-1._dp)/gp3
  d_aa_dzeta = aa*(d_ecl_dzeta - ec_lsda*dgp3_dzeta/gp3)*exp(-ec_lsda/gp3)&
&      /(exp(-ec_lsda/gp3)-1._dp)/gp3

  d_v_drs = d_aa_drs*t2 + 2*aa*t*d_t_drs
  d_v_dzeta = d_aa_dzeta*t2 + 2*aa*t*d_t_dzeta
  d_v_ds = 2*aa*t*d_t_ds

  d_gt_drs = 2*t*d_t_drs*(1._dp + v)/(1._dp + v + v2) &
&     - t2*v*(2._dp + v)/(1._dp + v + v2)**2*d_v_drs
  d_gt_dzeta = 2*t*d_t_dzeta*(1._dp + v)/(1._dp + v + v2) &
&     - t2*v*(2._dp + v)/(1._dp + v + v2)**2*d_v_dzeta
  d_gt_ds = 2*t*d_t_ds*(1._dp + v)/(1._dp + v + v2) &
&     - t2*v*(2._dp + v)/(1._dp + v + v2)**2*d_v_ds

  d_h_drs = gp3/hc*beta/gamma*d_gt_drs
  d_h_dzeta = dgp3_dzeta*log(hc) + gp3/hc*beta/gamma*d_gt_dzeta
  d_h_ds = gp3/hc*beta/gamma*d_gt_ds

  vcu = ec_lsda + h + n*((d_ecl_drs + d_h_drs)*d_rs_dn &
&      + ((d_ecl_dzeta + d_h_dzeta)*d_zeta_du) + d_h_ds*d_s_dn )
  vcd = ec_lsda + h + n*((d_ecl_drs + d_h_drs)*d_rs_dn &
&      + ((d_ecl_dzeta + d_h_dzeta)*d_zeta_dd) + d_h_ds*d_s_dn )

  vcg = n*d_h_ds*d_s_dgn
  vcgu = vcg*d_gn_dgu
  vcgd = vcg*d_gn_dgd

end subroutine pbe_c
