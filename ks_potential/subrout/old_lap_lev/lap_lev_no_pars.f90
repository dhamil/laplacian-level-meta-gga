
subroutine zapp_unpol(n,gn,lap,z,dzdp,dzdq)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), parameter :: pi = 3.14159265358979323846264338327950288419_dp

  real(dp), intent(in) :: n,gn,lap
  real(dp), intent(out) :: z,dzdp,dzdq

  real(dp) :: kf,z2,z4,zt,p,q,d_qr_dq
  real(dp) :: d_z2_dp,d_z4_dp,d_z4_dq,d_zt_dp,d_zt_dq,qr

  kf = (3._dp*pi**2*n)**(1._dp/3._dp)
  p = (gn/(2._dp*kf*n))**2
  q = lap/(4._dp*kf**2*n)

  qr = q/(1._dp + q**2)**(0.5_dp)

  z2 = 5._dp*p/3._dp
  z4 = -(20._dp*qr/9._dp + 5._dp*p/27._dp)
  zt = z2*( 1._dp + z4/(1._dp + z4**2) )
  z = zt/(1._dp  + zt**2)**(0.5_dp)

  d_qr_dq = 1._dp/(1._dp + q**2)**(1.5_dp)

  d_z2_dp = 5._dp/3._dp
  d_z4_dp = -5._dp/27._dp
  d_z4_dq = -20._dp/9._dp*d_qr_dq
  d_zt_dp = d_z2_dp*(1._dp + z4/(1._dp + z4**2)) + z2*d_z4_dp*(1._dp - z4**2)/(1._dp + z4**2)**2
  d_zt_dq = z2*d_z4_dq*(1._dp - z4**2)/(1._dp + z4**2)**2
  dzdp = d_zt_dp/(1._dp + zt**2)**(1.5_dp)
  dzdq = d_zt_dq/(1._dp + zt**2)**(1.5_dp)

end subroutine zapp_unpol

subroutine zapp(n,gn,lap,z,dzdp,dzdq)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), parameter :: pi = 3.14159265358979323846264338327950288419_dp

  real(dp), intent(in) :: n,gn,lap
  real(dp), intent(out) :: z,dzdp,dzdq

  real(dp) :: kf,z1,zt,p,q,d_qr_dq
  real(dp) :: d_z1_dp,d_z1_dq,d_zt_dp,d_zt_dq,qr

  kf = (3._dp*pi**2*n)**(1._dp/3._dp)
  p = (gn/(2._dp*kf*n))**2
  q = lap/(4._dp*kf**2*n)

  qr = q/(1._dp + q**2)**(0.5_dp)

  z1 = (5._dp*p/9._dp + 10*q/3._dp)**2
  zt = 5._dp*p/3._dp + 100*q**2/9._dp - z1/(1._dp + z1**2)
  z = tanh(zt)

  d_qr_dq = 1._dp/(1._dp + q**2)**(1.5_dp)

  d_z1_dp = 10._dp/9._dp*(5._dp*p/9._dp + 10*q/3._dp)
  d_z1_dq = 20._dp/3._dp*(5._dp*p/9._dp + 10*q/3._dp)
  d_zt_dp = 5._dp/3._dp - d_z1_dp*(1._dp - z1**2)/(1._dp + z1**2)**2
  d_zt_dq = 200*q/9._dp - d_z1_dq*(1._dp - z1**2)/(1._dp + z1**2)**2
  dzdp = d_zt_dp/cosh(zt)**2
  dzdq = d_zt_dq/cosh(zt)**2

end subroutine zapp

!===============================================================================
! Subroutine: lap_lev_x
! author: Aaron D. Kaplan (kaplan@temple.edu)
! inputs: nu (up-spin density), nd (down-spin density), gnu (absolute magnitude
!         of gradient of nu), gnd (abs. mag. of gradient of nd), gn (abs. mag.
!         of  gradient of total density), lu (Laplacian of nu), ld (Laplacian of
!         nd)
! outputs: ex (exchange energy density), vxu (d ex/d nu), vxd (d ex /d nd),
!          vxgu (d ex/d |gnu|), vxgd (d ex/d |gnd|), vxgn (d ex/d |gn|),
!          vxlu (d ex/d lu), vxld (d ex/d ld)
!===============================================================================

subroutine lap_lev_x(nu,nd,gnu,gnd,lu,ld,&
  & ex,vxu,vxd,vxgu,vxgd,vxlu,vxld)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)


  real(dp),intent(in) :: nu,nd,gnu,gnd,lu,ld
  real(dp),intent(out) :: ex,vxu,vxd,vxgu,vxgd,vxlu,vxld
  real(dp) :: ex_tmp

  ex = 0._dp ; vxu = 0._dp ; vxd = 0._dp
  vxgu = 0._dp ; vxgd = 0._dp
  vxlu = 0._dp ; vxld  = 0._dp

  if (nu > 0._dp) then
    call lap_lev_x_unpol(2._dp*nu,2._dp*gnu,2._dp*lu,ex_tmp,vxu,vxgu,vxlu)
    ex = ex + ex_tmp/2._dp
  endif
  if (nd > 0._dp) then
    call lap_lev_x_unpol(2._dp*nd,2._dp*gnd,2._dp*ld,ex_tmp,vxd,vxgd,vxld)
    ex = ex + ex_tmp/2._dp
  endif

end subroutine lap_lev_x

!===============================================================================
! Subroutine: lap_lev_x_unpol
! author: Aaron D. Kaplan (kaplan@temple.edu)
! inputs: n (density), gn (absolute magnitude of gradient of n), lap (Laplacian
!         of n)
! outputs: ex (exchange energy density), vxn (d ex/d n), vxgn (d ex /d |gn|),
!          vxl (d ex / d lap)
!===============================================================================

subroutine lap_lev_x_unpol(n,gn,lap,ex,vxn,vxgn,vxl)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), parameter :: pi = 3.14159265358979323846264338327950288419_dp


  real(dp),parameter :: kappa=0.174_dp,fx0 = 1.174_dp
  real(dp),parameter :: uak = 10._dp/81._dp, cqq = 146._dp/2025._dp
  real(dp),parameter :: gg = 1._dp/(25._dp/8._dp*cqq*kappa/uak**2 - 1._dp)


  real(dp),intent(in) :: n, gn, lap
  real(dp), intent(out) :: ex, vxn, vxgn, vxl

  real(dp) :: kf,p,q,delta,fx1,fx,exlda
  real(dp) :: d_exlda_dn, d_p_dn, d_p_dgn
  real(dp) :: d_q_dn, d_q_dlap,qr,d_qr_dq!,pr,d_pr_dp
  real(dp) :: z,d_z_dp,d_z_dq!,pexp,d_pexp_dp
  real(dp) :: d_delta_dp, d_delta_dq,fz,d_fz_dz
  real(dp) :: d_fx1_dp, d_fx1_dq, d_fx_dp, d_fx_dq

  kf = (3._dp*pi**2*n)**(1._dp/3._dp)
  exlda = -3._dp/(4._dp*pi)*kf*n

  p = (gn/(2._dp*kf*n))**2
  q = lap/(4._dp*kf**2*n)
  qr = q/(1._dp + q**2)**(0.5_dp)

  ! energy density ex, such that Ex[n] = int ex d**3 r

  call zapp(n,gn,lap,z,d_z_dp,d_z_dq)
  fz = 2*z**6/(1._dp + z**6)

  delta = uak*p + cqq*(q - 5*p/4._dp)**2
  fx1 = 1._dp + kappa - kappa/(1._dp + delta/(gg*kappa))**gg

  fx = fx1 + fz*(fx0 - fx1)

  ex = fx*exlda

  ! first derivatives of the energy density follow

  d_exlda_dn = -kf/pi

  d_p_dn = -8._dp/3._dp *p/n
  d_p_dgn = gn/(2._dp*(kf*n)**2)

  d_q_dn = -5._dp/3._dp *q/n
  d_q_dlap = 1._dp/(4._dp*kf**2*n)

  d_qr_dq = 1._dp/(1._dp + q**2)**(1.5_dp)

  d_fz_dz = 12*z**5/(1._dp + z**6)**2

  d_delta_dp = uak - 5._dp/2._dp*cqq*(q - 5*p/4._dp)
  d_delta_dq = cqq*(2*q - 5*p/2._dp)!*d_qr_dq

  d_fx1_dp = d_delta_dp/(1._dp + delta/(gg*kappa))**(gg+1)
  d_fx1_dq = d_delta_dq/(1._dp + delta/(gg*kappa))**(gg+1)

  d_fx_dp = d_fx1_dp*(1._dp - fz) + d_fz_dz*d_z_dp*(fx0 - fx1)
  d_fx_dq = d_fx1_dq*(1._dp - fz) + d_fz_dz*d_z_dq*(fx0 - fx1)

  vxn = fx*d_exlda_dn + exlda*(d_fx_dp * d_p_dn + d_fx_dq * d_q_dn)
  vxgn = exlda * d_fx_dp * d_p_dgn
  vxl = exlda * d_fx_dq * d_q_dlap

end subroutine lap_lev_x_unpol

!===============================================================================
! Subroutine: lap_lev_c
! author: Aaron D. Kaplan (kaplan@temple.edu)
! inputs: nu (up-spin density), nd (down-spin density), gnu (absolute magnitude
!         of gradient of nu), gnd (abs. mag. of gradient of nd), gn (abs. mag.
!         of  gradient of total density), lu (Laplacian of nu), ld (Laplacian of
!         nd)
! outputs: ec (correlation energy density), vcu (d ec/d nu), vcd (d ec /d nd),
!          vcgu (d ec/d |gnu|), vcgd (d ec/d |gnd|), vcgn (d ec/d |gn|),
!          vclu (d ec/d lu), vcld (d ec/d ld)
!===============================================================================

subroutine lap_lev_c(nu,nd,gnu,gnd,gn,lu,ld,&
  & ec,vcu,vcd,vcgu,vcgd,vcgn,vclu,vcld)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), parameter :: pi = 3.14159265358979323846264338327950288419_dp


  real(dp),intent(in) :: nu,nd,gnu,gnd,gn,lu,ld
  real(dp),intent(out) :: ec,vcu,vcd,vcgu,vcgd,vcgn,vclu,vcld

!  real(dp),parameter :: gamma = (1._dp - log(2._dp))/pi**2,beta_mb=0.06672454968309953_dp
  real(dp),parameter :: gamma = 0.0310906908696549_dp, beta_mb=0.066725_dp, cqq = 146._dp/2025._dp
  real(dp),parameter :: bfac_num=0.1_dp,bfac_den=0.1778_dp
  real(dp) :: kf,rs,n,zeta,s,t,phi,beta,w,y,h,gy
  real(dp) :: afn,d_afn_drs,d_afn_dzeta,p
  real(dp) :: gp3,ec_lsda,d_ecl_drs, d_ecl_dzeta
  real(dp) :: d_zeta_du, d_zeta_dd, d_rs_dn
  real(dp) :: grad_dot,d_gn_dgu,d_gn_dgd,d_s_dn,d_s_dgn,d_phi_dzeta
  real(dp) :: dgp3_dzeta,d_beta_drs,d_t_drs,d_t_dzeta,d_t_ds
  real(dp) :: d_w_drs, d_w_dzeta
  real(dp) :: d_y_drs,d_y_dzeta,d_y_ds,dgy,d_gy_drs,d_gy_dzeta,d_gy_ds
  real(dp) :: d_h_drs, d_h_dzeta, d_h_ds
  real(dp) :: z,d_z_ds,d_z_dq,q,d_q_dn,d_q_dlap,lap
  real(dp) :: g0,dg0,d_g0_drs,d_g0_dzeta,d_g0_ds,h0,d_h0_drs,d_h0_dzeta,d_h0_ds
  real(dp) :: epsc,d_epsc_drs,d_epsc_dz,d_epsc_ds,d_epsc_dq

  ! first the correlation energy

  n = nu + nd
  kf = (3._dp*pi**2*n)**(1._dp/3._dp)
  rs = (3._dp/(4._dp*pi*n))**(1._dp/3._dp)
  zeta = (nu - nd)/n
  zeta = min(max(-0.99999999999990_dp,zeta),0.99999999999990_dp)

  phi = ((1._dp + zeta)**(2._dp/3._dp) &
 &      + (1._dp - zeta)**(2._dp/3._dp))/2._dp
  gp3 = gamma*phi**3

  s = gn/(2._dp*kf*n)
  t = (3._dp*pi**2/16._dp)**(1._dp/3._dp)*s/(phi*rs**(0.5_dp))

  lap = lu+ld
  call zapp_unpol(n,gn,lap,z,d_z_ds,d_z_dq)
  d_z_ds = d_z_ds*2*s

  beta = beta_mb*(1._dp + bfac_num*rs)/(1._dp + bfac_den*rs)

  call ec_pw92(rs,zeta,ec_lsda,d_ecl_drs,d_ecl_dzeta)

  w = exp(-ec_lsda/gp3) - 1._dp
  afn = beta/(gamma*w)
  y = afn*t**2

  gy = (y + y**2)/(1._dp + y + y**2)
  h = gp3 * log(1._dp + w*gy)

  g0 = 1._dp - 1._dp/(1._dp + 4*y)**(0.25_dp)!(y + 5._dp/2._dp*y**2) )**(0.25_dp)
  h0 = gp3 * log(1._dp + w*g0)

  epsc = ec_lsda + h! + z**3*(h0 - h)
  ec = epsc*n

  ! now the potential components
  ! we're using the chain e_c(rs, zeta, s,q)

  d_rs_dn = -rs/n/3._dp

  d_zeta_du = 2._dp*nd/n**2
  d_zeta_dd = -2._dp*nu/n**2

  ! gnu_vec . gnd_vec
  grad_dot = (gn**2 - gnu**2 - gnd**2)/2._dp
  d_gn_dgu = (gnu + grad_dot/gnu)/gn
  d_gn_dgd = (gnd + grad_dot/gnd)/gn

  d_s_dgn = 1._dp/(2._dp*kf*n)
  d_s_dn = -4._dp/3._dp*s/n

  d_q_dn = -5._dp/3._dp *lap/(4._dp*kf**2*n**2)
  d_q_dlap = 1._dp/(4._dp*kf**2*n)

  d_phi_dzeta = ((1._dp + zeta)**(-1._dp/3._dp) &
 &      - (1._dp - zeta)**(-1._dp/3._dp))/3._dp
  dgp3_dzeta = 3*gamma*phi**2*d_phi_dzeta

  d_t_drs = -0.5_dp*t/rs
  d_t_dzeta = -t*d_phi_dzeta/phi
  d_t_ds = (3._dp*pi**2/16._dp)**(1._dp/3._dp)/(phi*rs**(0.5_dp))

  d_beta_drs = beta_mb*(bfac_num - bfac_den)/(1._dp + bfac_den*rs)**2

  d_w_drs = -d_ecl_drs/gp3*exp(-ec_lsda/gp3)
  d_w_dzeta = (-d_ecl_dzeta/gp3 + ec_lsda*dgp3_dzeta/gp3**2)*exp(-ec_lsda/gp3)

  d_afn_drs = (d_beta_drs - beta*d_w_drs/w)/(gamma*w)
  d_afn_dzeta = -d_w_dzeta*afn/w

  d_y_drs = d_afn_drs*t**2 + 2*afn*t*d_t_drs
  d_y_dzeta = d_afn_dzeta*t**2 + 2*afn*t*d_t_dzeta
  d_y_ds = 2._dp*afn*t*d_t_ds

  dgy = (1._dp + 2._dp*y)/(1._dp + y + y**2)**2
  d_gy_drs = dgy*d_y_drs
  d_gy_dzeta = dgy*d_y_dzeta
  d_gy_ds = dgy*d_y_ds

  d_h_drs = gp3*( d_w_drs*gy + w*d_gy_drs)&
 &      /(1._dp + w*gy)
  d_h_dzeta = gp3*( d_w_dzeta*gy + w*d_gy_dzeta)&
 &      /(1._dp + w*gy) + dgp3_dzeta*log(1._dp + w*gy)
  d_h_ds =  gp3*w*d_gy_ds/(1._dp + w*gy)

  dg0 = 1._dp/(1._dp + 4*y)**(1.25_dp)!(1._dp + 5._dp*y)/(1._dp + 4*(y + 5._dp/2._dp*y**2) )**(1.25_dp)
  d_g0_drs = dg0*d_y_drs
  d_g0_dzeta = dg0*d_y_dzeta
  d_g0_ds = dg0*d_y_ds

  d_h0_drs = gp3*( d_w_drs*g0 + w*d_g0_drs)&
 &      /(1._dp + w*g0)
  d_h0_dzeta = gp3*( d_w_dzeta*g0 + w*d_g0_dzeta)&
 &      /(1._dp + w*g0) + dgp3_dzeta*log(1._dp + w*g0)
  d_h0_ds =  gp3*w*d_g0_ds/(1._dp + w*g0)

  d_epsc_drs = d_ecl_drs + d_h_drs!*(1._dp-z**3) + d_h0_drs*z**3
  d_epsc_dz = d_ecl_dzeta + d_h_dzeta!*(1._dp-z**3) + d_h0_dzeta*z**3
  d_epsc_ds = d_h_ds!*(1._dp-z**3) + d_h0_ds*z**3 + 3*z**2*d_z_ds*(h0 - h)
  d_epsc_dq = 0._dp!3*z**2*d_z_dq*(h0 - h)

  vcu = epsc + n*( d_epsc_drs*d_rs_dn + d_epsc_dz*d_zeta_du + d_epsc_ds*d_s_dn &
&     + d_epsc_dq*d_q_dn )
  vcd = epsc + n*( d_epsc_drs*d_rs_dn + d_epsc_dz*d_zeta_dd + d_epsc_ds*d_s_dn &
&     + d_epsc_dq*d_q_dn )

  vcgn = n*d_epsc_ds*d_s_dgn
  vcgu = n*d_epsc_ds*d_s_dgn * d_gn_dgu
  vcgd = n*d_epsc_ds*d_s_dgn * d_gn_dgd

  vclu = 0._dp!n*d_epsc_dq*d_q_dlap
  vcld = vclu

end subroutine lap_lev_c


subroutine ec_pw92(rs,zeta,ec,d_ec_drs,d_ec_dzeta)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)


  real(dp),intent(in) :: rs,zeta
  real(dp),intent(out) :: ec,d_ec_drs,d_ec_dzeta

  real(dp),parameter :: fz_den=(2._dp**(4._dp/3._dp)-2._dp),fdd0 = 8._dp/9._dp/fz_den
  real(dp) :: eps0,deps0,eps1,deps1,ac,dac
  real(dp) :: fz,dfz

  call pw92_g(rs,0.031091_dp,0.21370_dp,7.5957_dp,3.5876_dp,&
 &       1.6382_dp,0.49294_dp,eps0,deps0)
  if (zeta==0._dp) then
    ec = eps0
    d_ec_drs = deps0
    d_ec_dzeta = 0._dp
  else

    fz = ((1._dp + zeta)**(4._dp/3._dp) &
   &     + (1._dp - zeta)**(4._dp/3._dp) - 2._dp)/fz_den

    dfz = 4._dp/3._dp*((1._dp + zeta)**(1._dp/3._dp) &
   &     - (1._dp - zeta)**(1._dp/3._dp))/fz_den

    call pw92_g(rs,0.015545_dp,0.20548_dp,14.1189_dp,6.1977_dp,&
   &       3.3662_dp,0.62517_dp,eps1,deps1)
    call pw92_g(rs,0.016887_dp,0.11125_dp,10.357_dp,3.6231_dp,&
   &       0.88026_dp,0.49671_dp,ac,dac)

    ec = eps0*(1._dp - fz*zeta**4) - ac*fz/fdd0*(1._dp - zeta**4) + eps1*fz*zeta**4

    d_ec_drs = deps0*(1._dp - fz*zeta**4) + deps1*fz*zeta**4 &
   &       - dac*fz/fdd0*(1._dp - zeta**4)
    d_ec_dzeta = 4*zeta**3*fz*(eps1 - eps0 + ac/fdd0) &
   &       + dfz*(zeta**4 * (eps1 - eps0) - (1._dp - zeta**4)*ac/fdd0)
  end if

end subroutine ec_pw92

subroutine pw92_g(rs,a,alp,b1,b2,b3,b4,eps,deps)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp),intent(in) :: rs,a,alp,b1,b2,b3,b4
  real(dp),intent(out) :: eps,deps
  real(dp) :: rsh,q0,q1,q1p

  rsh = rs**(0.5_dp)

  q0 = -2*a*(1._dp + alp*rs)
  q1 = 2*a*(b1*rsh + b2*rs + b3*rsh**3 + b4*rs**2)
  q1p = a*(b1/rsh + 2*b2 + 3*b3*rsh + 4*b4*rs)

  eps = q0*log(1._dp + 1._dp/q1)
  deps = -2*a*alp*log(1._dp + 1._dp/q1) - q0*q1p/(q1**2 + q1)

end subroutine pw92_g
