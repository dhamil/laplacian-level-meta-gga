subroutine scan_orb_free_x(wfxn,nu,nd,gnu,gnd,lnu,lnd,&
 & ex,vxu,vxd,vxgu,vxgd,vxlu,vxld)

  implicit none

  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), dimension(2), parameter :: cppars = (/ 1.784720_dp, 0.258304_dp /)
  character(len=4), intent(in) :: wfxn
  real(dp), intent(in) :: nu,nd,gnu,gnd,lnu,lnd
  real(dp), intent(out) :: ex,vxu,vxd,vxgu,vxgd,vxlu,vxld

  integer :: ia,iief,ifx
  real(dp) :: ex_tmp, tauu,taud,dtdnu,dtdgnu,dtdlnu,dtdnd,dtdgnd,dtdlnd

  ex = 0._dp
  vxu = 0._dp ; vxd = 0._dp
  vxgu = 0._dp ; vxgd = 0._dp
  vxlu = 0._dp ; vxld = 0._dp

  if (wfxn == 'SCAL') then
    ia = 0
    iief = 0
    ifx = 0
  else if (wfxn == 'RSCL') then
    ia = 1
    iief = 1
    ifx = 0
  else if (wfxn == 'R2SL') then
    ia = 2
    iief = 1
    ifx = 1
  end if

  tauu = 0._dp
  if (nu > 0._dp) then
    call pc_tau_unp(cppars,2,.false.,2*nu,2*gnu,2*lnu,tauu,dtdnu,dtdgnu,dtdlnu)
    call eps_SCANx(2*nu, 2*gnu, tauu, ex_tmp, vxu, vxgu, vxlu, &
       & ia, iief, ifx)
    vxu = vxu + vxlu*dtdnu
    vxgu = vxgu + vxlu*dtdgnu
    vxlu = vxlu*dtdlnu
    ex = ex + ex_tmp/2._dp
  end if

  taud = 0._dp
  if (nd > 0._dp) then
    call pc_tau_unp(cppars,2,.false.,2*nd,2*gnd,2*lnd,taud,dtdnd,dtdgnd,dtdlnd)
    call eps_SCANx(2*nd, 2*gnd, taud, ex_tmp, vxd, vxgd, vxld, &
       & ia, iief, ifx)
    vxd = vxd + vxld*dtdnd
    vxgd = vxgd + vxld*dtdgnd
    vxld = vxld*dtdlnd
    ex = ex + ex_tmp/2._dp
  end if

end subroutine scan_orb_free_x


subroutine scan_orb_free_c(wfcn,nu,nd,gnu,gnd,gn,lnu,lnd,&
 & ec,vcu,vcd,vcgu,vcgd,vcg,vclu,vcld)

  implicit none

  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), dimension(2), parameter :: cppars = (/ 1.784720_dp, 0.258304_dp /)

  character(len=4), intent(in) :: wfcn
  real(dp), intent(in) :: nu,nd,gnu,gnd,gn,lnu,lnd
  real(dp), intent(out) :: ec,vcu,vcd,vcgu,vcgd,vcg,vclu,vcld

  integer :: ia,iief,iec
  real(dp) :: tauu,taud
  real(dp) :: gnud,dtdnu,dtdgnu,dtdlnu,dtdnd,dtdgnd,dtdlnd

  ec = 0._dp
  vcu = 0._dp ; vcd = 0._dp
  vcgu = 0._dp ; vcgd = 0._dp ; vcg = 0._dp
  vclu = 0._dp ; vcld = 0._dp

  if (wfcn == 'SCAL') then
    ia = 0
    iief = 0
    iec = 0
  else if (wfcn == 'RSCL') then
    ia = 1
    iief = 1
    iec = 0
  else if (wfcn == 'R2SL') then
    ia = 2
    iief = 1
    iec = 1
 end if

  tauu = 0._dp ; dtdnu = 0._dp ; dtdgnu = 0._dp ; dtdlnu = 0._dp
  if (nu > 0._dp) call pc_tau_unp(cppars,2,.false.,2*nu,2*gnu,2*lnu,tauu,dtdnu,dtdgnu,dtdlnu)

  taud = 0._dp ; dtdnd = 0._dp ; dtdgnd = 0._dp ; dtdlnd = 0._dp
  if (nd > 0._dp) call pc_tau_unp(cppars,2,.false.,2*nd,2*gnd,2*lnd,taud,dtdnd,dtdgnd,dtdlnd)

  call vrSCANc(nu, nd, gnu, gnd, gn, tauu/2._dp, taud/2._dp, ec, &
      &   vcu, vcd, vcgu, vcgd, vclu, vcld, ia, iief, iec)

  vcu = vcu + vclu*dtdnu
  vcd = vcd + vcld*dtdnd

  vcgu = vcgu + vclu*dtdgnu
  vcgd = vcgd + vcld*dtdgnd

  vclu = vclu*dtdlnu
  vcld = vcld*dtdlnd

end subroutine scan_orb_free_c


subroutine pc_tau(pars,nu,nd,gnu,gnd,gn,lnu,lnd,tau,&
 & d_tau_dnu,d_tau_dnd,d_tau_dgnu,d_tau_dgnd,&
 & d_tau_dlnu,d_tau_dlnd)

  implicit none

  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), intent(in),dimension(2) :: pars
  real(dp), intent(in) :: nu,nd,gnu,gnd,gn,lnu,lnd
  real(dp), intent(out) :: d_tau_dnu,d_tau_dnd,d_tau_dgnu,d_tau_dgnd
  real(dp), intent(out) :: tau,d_tau_dlnu,d_tau_dlnd
  real(dp) :: tmp

  tau = 0._dp
  d_tau_dnu = 0._dp ; d_tau_dnd= 0._dp
  d_tau_dgnu = 0._dp ; d_tau_dgnd = 0._dp
  d_tau_dlnu = 0._dp ; d_tau_dlnd = 0._dp

  if (nu > 0._dp) then
    call pc_tau_unp(pars,2,.false.,2*nu,2*gnu,2*lnu,tmp,d_tau_dnu,&
   &  d_tau_dgnu,d_tau_dlnu)
    tau = tau + tmp
  end if
  if (nd > 0._dp) then
    call pc_tau_unp(pars,2,.false.,2*nd,2*gnd,2*lnd,tmp,d_tau_dnd,&
  &    d_tau_dgnd,d_tau_dlnd)
    tau = tau + tmp
  end if
  tau = tau/2._dp

end subroutine pc_tau



subroutine pc_tau_unp(pars,npar,rev,n,gn,ln,tau,dtdn,dtdgn,dtdln)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), parameter :: pi = 3.14159265358979323846264338327950288419_dp

  integer, intent(in) :: npar
  logical, intent(in) :: rev
  real(dp), intent(in), dimension(npar) :: pars
  real(dp), intent(in) :: n,gn,ln
  real(dp), intent(out) :: tau,dtdn,dtdgn,dtdln

  real(dp) :: kf,tau0,p,q,ftw,ft2,ft4,ft
  real(dp) :: freg,ftge4,z,theta,d_theta_dz,opftw
  real(dp) :: d_tau0_dn,d_p_dn,d_p_dgn,d_q_dn,d_q_dln
  real(dp) :: d_ftw_dp, d_ft2_dp, d_ft2_dq, d_ft4_dp, d_ft4_dq
  real(dp) :: d_freg_dp, d_freg_dq,d_ftge4_dp,d_ftge4_dq
  real(dp) :: d_z_dp,d_z_dq, d_ft_dp,d_ft_dq

  ! first the approximate tau

  kf = (3*pi**2*n)**(1._dp/3._dp)
  tau0 = 0.3_dp*kf**2*n

  p = (gn/(2*kf*n))**2
  q = ln/(4*kf**2*n)

  ftw = 5._dp*p/3._dp
  ft2 = 5._dp*p/27._dp + 20._dp*q/9._dp
  ft4 = 8._dp*q**2/81._dp - p*q/9._dp + 8._dp*p**2/243._dp

  opftw = 1._dp + ftw

  freg = (1._dp + (ft4/opftw)**2)**(0.5_dp)
  ftge4 = (1._dp + ft2 + ft4)/freg
  z = ftge4 - ftw
  if (rev) then
    call new_tau_interp(pars,z,theta,d_theta_dz)
  else
    call pc_interpolation(pars,z,theta,d_theta_dz)
  end if

  ft = ftw + z*theta
  tau = ft*tau0

  ! now the derivatives of tau

  d_tau0_dn = kf**2/2._dp

  d_p_dn = -8._dp/3._dp *p/n
  d_p_dgn = gn/(2*(kf*n)**2)

  d_q_dn = -5._dp/3._dp *q/n
  d_q_dln = 1._dp/(4*kf**2*n)

  d_ftw_dp = 5._dp/3._dp
  d_ft2_dp = 5._dp/27._dp
  d_ft2_dq = 20._dp/9._dp

  d_ft4_dp = -q/9._dp + 16._dp*p/243._dp
  d_ft4_dq = 16._dp*q/81._dp - p/9._dp

  d_freg_dp = ft4*(d_ft4_dp - ft4*d_ftw_dp/opftw)/(freg*opftw**2)
  d_freg_dq = ft4*d_ft4_dq/(freg*opftw**2)

  d_ftge4_dp = (d_ft2_dp + d_ft4_dp - ftge4*d_freg_dp)/freg
  d_ftge4_dq = (d_ft2_dq + d_ft4_dq - ftge4*d_freg_dq)/freg

  d_z_dp = d_ftge4_dp - d_ftw_dp
  d_z_dq = d_ftge4_dq

  d_ft_dp = d_ftw_dp + (d_theta_dz*z + theta)*d_z_dp
  d_ft_dq = (d_theta_dz*z + theta)*d_z_dq

  dtdn = ft*d_tau0_dn + tau0*( d_ft_dp*d_p_dn + d_ft_dq*d_q_dn )
  ! NB: tau0*d_p_dgn = 3/20 * |grad n|/n = 0.15 * |grad n|/n
  dtdgn = 0.15_dp*gn/n*d_ft_dp!tau0*d_ft_dp*d_p_dgn
  ! NB: tau0*d_q_dln = 3/40 = 0.075
  dtdln = 0.075_dp*d_ft_dq!tau0*d_ft_dq*d_q_dln

end subroutine pc_tau_unp



subroutine pc_interpolation(pars,z,th,dthdz)

  implicit none

  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), parameter :: eps = 1.d-2

  real(dp), intent(in),dimension(2) :: pars
  real(dp), intent(in) :: z
  real(dp), intent(out) :: th,dthdz

  real(dp) :: a_fuzzy,fac1,fac2,dfac1,dfac2

  a_fuzzy = pars(1)-eps

  th = 0._dp
  dthdz = 0._dp
  if ((eps < z).and.(z < a_fuzzy)) then
    fac1 = exp(pars(1)/(pars(1) - z))
    dfac1 = pars(1)/(pars(1) - z)**2*fac1
    fac2 = exp(pars(1)/z)
    dfac2 = -pars(1)/z**2*fac2

    th = ( (1._dp + fac1)/(fac2 + fac1) )**pars(2)
    dthdz = pars(2)* ( (1._dp + fac1)/(fac2 + fac1) )**(pars(2) - 1)
    dthdz = dthdz*( dfac1*(fac2 - 1._dp) - dfac2*(1._dp + fac1) )/(fac2 + fac1)**2
  else if (z >= a_fuzzy) then
    th = 1._dp
  end if

end subroutine pc_interpolation


subroutine new_tau_interp(pars,z,th,dthdz)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in),dimension(3) :: pars
  real(dp), intent(in) :: z
  real(dp), intent(out) :: th,dthdz

  real(dp) :: omz, opz, th_num, th_den, thc

  th = 0._dp
  dthdz = 0._dp
  if (z > 0._dp) then
    omz = 1._dp - z
    opz = 1._dp + z
    th_den = 1._dp + z**4*(pars(2) + pars(3)*log(opz))
    th_num = 1._dp + pars(1)*z
    thc = omz**3*th_num/th_den
    th = 1._dp - thc

    dthdz = ( omz**2*(3._dp*th_num - pars(1)*omz) + (4._dp*z**3*(pars(2) + &
   &     pars(3)*log(opz) ) + z**4*pars(3)/opz)*thc )/th_den
  end if

end subroutine new_tau_interp
