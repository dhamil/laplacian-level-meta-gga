subroutine r2scan_of_x(nu,nd,gnu,gnd,lnu,lnd,&
 & ex,vxu,vxd,vxgu,vxgd,vxlu,vxld)

  implicit none

  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), intent(in) :: nu,nd,gnu,gnd,lnu,lnd
  real(dp), intent(out) :: ex,vxu,vxd,vxgu,vxgd,vxlu,vxld

  integer :: ia,iief,ifx
  real(dp) :: ex_tmp, tauu,taud,dtdnu,dtdgnu,dtdlnu,dtdnd,dtdgnd,dtdlnd
  real(dp), parameter :: tp(3) = (/-8.286095_dp,-5.131367_dp,6.056968_dp /)

  ex = 0._dp
  vxu = 0._dp ; vxd = 0._dp
  vxgu = 0._dp ; vxgd = 0._dp
  vxlu = 0._dp ; vxld = 0._dp

  ia = 2
  iief = 1
  ifx = 1

  tauu = 0._dp
  if (nu > 0._dp) then
    call tau_rpp_unp(2*nu,2*gnu,2*lnu,tauu,dtdnu,dtdgnu,dtdlnu)
    !call pc_tau_unp(tp,3,.true.,2*nu,2*gnu,2*lnu,tauu,dtdnu,dtdgnu,dtdlnu)
    call eps_SCANx(2*nu, 2*gnu, tauu, ex_tmp, vxu, vxgu, vxlu, &
       & ia, iief, ifx)
    vxu = vxu + vxlu*dtdnu
    vxgu = vxgu + vxlu*dtdgnu
    vxlu = vxlu*dtdlnu
    ex = ex + ex_tmp/2._dp
  end if

  taud = 0._dp
  if (nd > 0._dp) then
    call tau_rpp_unp(2*nd,2*gnd,2*lnd,taud,dtdnd,dtdgnd,dtdlnd)
    !call pc_tau_unp(tp,3,.true.,2*nd,2*gnd,2*lnd,taud,dtdnd,dtdgnd,dtdlnd)
    call eps_SCANx(2*nd, 2*gnd, taud, ex_tmp, vxd, vxgd, vxld, &
       & ia, iief, ifx)
    vxd = vxd + vxld*dtdnd
    vxgd = vxgd + vxld*dtdgnd
    vxld = vxld*dtdlnd
    ex = ex + ex_tmp/2._dp
  end if

end subroutine r2scan_of_x


subroutine r2scan_of_c(nu,nd,gnu,gnd,gn,lnu,lnd,&
 & ec,vcu,vcd,vcgu,vcgd,vcg,vclu,vcld)

  implicit none

  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in) :: nu,nd,gnu,gnd,gn,lnu,lnd
  real(dp), intent(out) :: ec,vcu,vcd,vcgu,vcgd,vcg,vclu,vcld

  integer :: ia,iief,iec
  real(dp) :: tauu,taud
  real(dp) :: gnud,dtdnu,dtdgnu,dtdlnu,dtdnd,dtdgnd,dtdlnd
  real(dp), parameter :: tp(3) = (/-8.286095_dp,-5.131367_dp,6.056968_dp /)


  ec = 0._dp
  vcu = 0._dp ; vcd = 0._dp
  vcgu = 0._dp ; vcgd = 0._dp ; vcg = 0._dp
  vclu = 0._dp ; vcld = 0._dp

  ia = 2
  iief = 1
  iec = 1

  tauu = 0._dp ; dtdnu = 0._dp ; dtdgnu = 0._dp ; dtdlnu = 0._dp
  if (nu > 0._dp) call tau_rpp_unp(2*nu,2*gnu,2*lnu,tauu,dtdnu,dtdgnu,dtdlnu)
  !if (nu > 0._dp) call pc_tau_unp(tp,3,.true.,2*nu,2*gnu,2*lnu,tauu,dtdnu,dtdgnu,dtdlnu)

  taud = 0._dp ; dtdnd = 0._dp ; dtdgnd = 0._dp ; dtdlnd = 0._dp
  if (nd > 0._dp) call tau_rpp_unp(2*nd,2*gnd,2*lnd,taud,dtdnd,dtdgnd,dtdlnd)
  !if (nd > 0._dp) call pc_tau_unp(tp,3,.true.,2*nd,2*gnd,2*lnd,taud,dtdnd,dtdgnd,dtdlnd)

  call vrSCANc(nu, nd, gnu, gnd, gn, tauu/2._dp, taud/2._dp, ec, &
      &   vcu, vcd, vcgu, vcgd, vclu, vcld, ia, iief, iec)

  vcu = vcu + vclu*dtdnu
  vcd = vcd + vcld*dtdnd

  vcgu = vcgu + vclu*dtdgnu
  vcgd = vcgd + vcld*dtdgnd

  vclu = vclu*dtdlnu
  vcld = vcld*dtdlnd

end subroutine r2scan_of_c


subroutine tau_non_emp(nu,nd,gnu,gnd,gn,lnu,lnd,tau,&
 & d_tau_dnu,d_tau_dnd,d_tau_dgnu,d_tau_dgnd,&
 & d_tau_dlnu,d_tau_dlnd)

  implicit none

  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), intent(in) :: nu,nd,gnu,gnd,gn,lnu,lnd
  real(dp), intent(out) :: d_tau_dnu,d_tau_dnd,d_tau_dgnu,d_tau_dgnd
  real(dp), intent(out) :: tau,d_tau_dlnu,d_tau_dlnd
  real(dp) :: tmp

  tau = 0._dp
  d_tau_dnu = 0._dp ; d_tau_dnd = 0._dp
  d_tau_dgnu = 0._dp ; d_tau_dgnd = 0._dp
  d_tau_dlnu = 0._dp ; d_tau_dlnd = 0._dp

  if (nu > 0._dp) then
    call tau_non_emp_gga_unp(2*nu,2*gnu,2*lnu,tmp,d_tau_dnu,&
   &  d_tau_dgnu,d_tau_dlnu)
    tau = tau + tmp
  end if
  if (nd > 0._dp) then
    call tau_non_emp_gga_unp(2*nd,2*gnd,2*lnd,tmp,d_tau_dnd,&
  &    d_tau_dgnd,d_tau_dlnd)
    tau = tau + tmp
  end if
  tau = tau/2._dp

end subroutine tau_non_emp


subroutine tau_exp2(n,gn,ln,tau,dtdn,dtdgn,dtdln)
  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), parameter :: pi = 3.14159265358979323846264338327950288419_dp

  real(dp), intent(in) :: n,gn,ln
  real(dp), intent(out) :: tau,dtdn,dtdgn,dtdln

  real(dp) :: kf,tau0,p,q,ft,d_p_dn,d_p_dgn,d_q_dn,d_q_dlap
  real(dp) :: d_tau0_dn, d_ft_dp,d_ft_dq, x, alp, dalpdx

  kf = (3*pi**2*n)**(1._dp/3._dp)
  tau0 = 0.3_dp*kf**2*n

  p = (gn/(2*kf*n))**2
  q = ln/(4*kf**2*n)

  x = -40._dp/27._dp*p + 20._dp/9._dp*q
  ! doing this switching to avoid overflows
  if (x <= 0._dp) then
    alp = (exp(x) + (1._dp+x)*exp(5._dp*x) )/(1._dp + exp(5._dp*x))
  else
    alp = (1._dp + x + exp(-4._dp*x) )/(1._dp + exp(-5._dp*x))
  end if


  ! bounds chosen so that dalpdx lies within 10**(-16) of the asymptotic value
  if (x <= -37._dp) then
    dalpdx = 0._dp
  else if (x >= 11._dp) then
    dalpdx = 1._dp
  else
    dalpdx = ( (6._dp + 5._dp*x)*exp(3._dp*x) + exp(8._dp*x) &
    &      - 4._dp*exp(4._dp*x) + exp(-x))/(exp(-x) + exp(4._dp*x))**2
  end if

  ft = 5._dp*p/3._dp + alp
  d_ft_dp = 5._dp/3._dp - 40._dp/27._dp*dalpdx
  d_ft_dq = 20._dp/9._dp*dalpdx

  tau = tau0*ft

  d_tau0_dn = kf**2/2._dp

  d_p_dn = -8._dp/3._dp *p/n
  !d_p_dgn = gn/(2*(kf*n)**2)

  d_q_dn = -5._dp/3._dp*q/n
!  d_q_dlap = 1._dp/(4*kf**2*n)

  dtdn = ft*d_tau0_dn + tau0*(d_ft_dp*d_p_dn + d_ft_dq*d_q_dn)
  ! NB: tau0*d_p_dgn = 3/20 * |grad n|/n = 0.15 * |grad n|/n
  dtdgn = 0.15_dp*gn/n*d_ft_dp!tau0*d_ft_dp*d_p_dgn
  ! NB: tau0*d_q_dln = 3/40 = 0.075
  dtdln = 0.075_dp*d_ft_dq!tau0*d_ft_dq*d_q_dln

end subroutine tau_exp2


subroutine tau_exp(n,gn,ln,tau,dtdn,dtdgn,dtdln)
  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), parameter :: pi = 3.14159265358979323846264338327950288419_dp

  real(dp), intent(in) :: n,gn,ln
  real(dp), intent(out) :: tau,dtdn,dtdgn,dtdln

  integer, parameter :: cn = 3
  real(dp), parameter :: x0 = 0._dp

  real(dp) :: kf,tau0,p,q,ft,d_p_dn,d_p_dgn,d_q_dn,d_q_dlap
  real(dp) :: d_tau0_dn, d_ft_dp,d_ft_dq, x, alp, dalpdx, y,dydx,g,dg

  kf = (3*pi**2*n)**(1._dp/3._dp)
  tau0 = 0.3_dp*kf**2*n

  p = (gn/(2*kf*n))**2
  q = ln/(4*kf**2*n)

  x = -40._dp/27._dp*p + 20._dp/9._dp*q
  alp = 0._dp
  dalpdx = 0._dp
  if (x <= x0) then
    y = (x - x0)/(1._dp + x0)
    dydx = 1._dp/(1._dp + x0)
    call tau_exp_h(y,cn,g,dg)
    if (g > -691._dp) then
      ! exp(-691) ~~ 8 x 10**(-301), which is just a little larger
      ! than the minimum representable dp precision number, ~ 10**(-307)
      ! trying to avoid underflow error
      alp = (1._dp + x0)*exp(g)
      dalpdx = alp*dg*dydx
    end if
  else if (x > x0) then
    alp = 1._dp + x
    dalpdx = 1._dp
  end if

  ft = 5._dp*p/3._dp + alp
  d_ft_dp = 5._dp/3._dp - 40._dp/27._dp*dalpdx
  d_ft_dq = 20._dp/9._dp*dalpdx

  tau = tau0*ft

  d_tau0_dn = kf**2/2._dp

  d_p_dn = -8._dp/3._dp *p/n
  !d_p_dgn = gn/(2*(kf*n)**2)

  d_q_dn = -5._dp/3._dp*q/n
!  d_q_dlap = 1._dp/(4*kf**2*n)

  dtdn = ft*d_tau0_dn + tau0*(d_ft_dp*d_p_dn + d_ft_dq*d_q_dn)
  ! NB: tau0*d_p_dgn = 3/20 * |grad n|/n = 0.15 * |grad n|/n
  dtdgn = 0.15_dp*gn/n*d_ft_dp!tau0*d_ft_dp*d_p_dgn
  ! NB: tau0*d_q_dln = 3/40 = 0.075
  dtdln = 0.075_dp*d_ft_dq!tau0*d_ft_dq*d_q_dln

end subroutine tau_exp

subroutine tau_exp_h(y,k,h,dhdy)
  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)
  integer, intent(in) :: k
  real(dp), intent(in) :: y
  real(dp), intent(out) :: h,dhdy

  real(dp) :: g2,dg2,y2k

  call tau_exp_g(y,2*k,g2,dg2)

  y2k = y**(2*k)
  h = ( g2 + 2._dp*(k+1._dp)/(2._dp*k+1._dp)*y*y2k )/(1._dp + y2k)
  dhdy = (dg2 + 2._dp*(k + 1._dp)*y2k - 2._dp*k*y**(2*k-1)*h )/(1._dp + y2k)

end subroutine tau_exp_h

subroutine tau_exp_g(y,k,g,dgdy)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)
  integer, intent(in) :: k
  real(dp), intent(in) :: y
  real(dp), intent(out) :: g,dgdy

  integer :: i
  real(dp) :: fac

  g = 0._dp
  dgdy = 0._dp
  fac = 1._dp
  do i = 1,k
    g = g + fac * y**i/i
    dgdy = dgdy + fac * y**(i-1)
    fac = -fac
  end do

end subroutine tau_exp_g



subroutine tau_gea2_interp_unp_2_par(n,gn,ln,tau,dtdn,dtdgn,dtdln)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), parameter :: pi = 3.14159265358979323846264338327950288419_dp

  real(dp), intent(in) :: n,gn,ln
  real(dp), intent(out) :: tau,dtdn,dtdgn,dtdln

  real(dp), parameter :: x0 = -0.805925_dp, x1 = -0.520048_dp
  real(dp), parameter :: c0 = 2._dp*(5._dp + 2._dp*x0 + 3._dp*x1), c1 = -(15._dp + 7._dp*x0 + 8._dp*x1)
  real(dp), parameter :: c2 = 3._dp*(2._dp + x0 + x1)

  real(dp) :: kf,tau0,p,q,ft,d_p_dn,d_p_dgn,d_q_dn,d_q_dlap
  real(dp) :: d_tau0_dn, d_ft_dp,d_ft_dq, x, alp, dalpdx, y,dydx

  kf = (3*pi**2*n)**(1._dp/3._dp)
  tau0 = 0.3_dp*kf**2*n

  p = (gn/(2*kf*n))**2
  q = ln/(4*kf**2*n)

  x = -40._dp/27._dp*p + 20._dp/9._dp*q
  if (x < x0) then
    alp = 0._dp
    dalpdx = 0._dp
  else if ((x0 <= x).and.(x <= x1)) then
    y = (x - x0)/(x1 - x0)
    dydx = 1._dp/(x1 - x0)

    alp = y**3*(c0 + y*(c1 + y*c2))
    dalpdx = y**2*(3._dp*c0 + y*(4._dp*c1 + 5._dp*y*c2))*dydx
  else if (x > x1) then
    alp = 1._dp + x
    dalpdx = 1._dp
  end if

  ft = 5._dp*p/3._dp + alp
  d_ft_dp = 5._dp/3._dp - 40._dp/27._dp*dalpdx
  d_ft_dq = 20._dp/9._dp*dalpdx

  tau = tau0*ft

  d_tau0_dn = kf**2/2._dp

  d_p_dn = -8._dp/3._dp *p/n
  !d_p_dgn = gn/(2*(kf*n)**2)

  d_q_dn = -5._dp/3._dp*q/n
!  d_q_dlap = 1._dp/(4*kf**2*n)

  dtdn = ft*d_tau0_dn + tau0*(d_ft_dp*d_p_dn + d_ft_dq*d_q_dn)
  ! NB: tau0*d_p_dgn = 3/20 * |grad n|/n = 0.15 * |grad n|/n
  dtdgn = 0.15_dp*gn/n*d_ft_dp!tau0*d_ft_dp*d_p_dgn
  ! NB: tau0*d_q_dln = 3/40 = 0.075
  dtdln = 0.075_dp*d_ft_dq!tau0*d_ft_dq*d_q_dln

end subroutine tau_gea2_interp_unp_2_par

subroutine tau_na(n,gn,ln,tau,dtdn,dtdgn,dtdln)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), parameter :: pi = 3.14159265358979323846264338327950288419_dp

  real(dp), intent(in) :: n,gn,ln
  real(dp), intent(out) :: tau,dtdn,dtdgn,dtdln

  real(dp), parameter :: x0 = 0.594922_dp

  real(dp) :: kf,tau0,p,q,ft,d_p_dn,d_p_dgn,d_q_dn,d_q_dlap,xden
  real(dp) :: d_tau0_dn, d_ft_dp,d_ft_dq, x, alp, dalpdx,dxdp,dxdq

  ! first the approximate tau

  kf = (3*pi**2*n)**(1._dp/3._dp)
  tau0 = 0.3_dp*kf**2*n

  p = (gn/(2*kf*n))**2
  q = ln/(4*kf**2*n)

  x = - 40._dp/27._dp*p + 20._dp/9._dp*q
  dxdp = - 40._dp/27._dp
  dxdq = 20._dp/9._dp

  if (abs(x) < 1.d-12) then
    alp = 1._dp + x
    dalpdx = 1._dp
  else if (x >= 1.d-12) then
    alp = 1._dp + x*(1._dp + exp(-x0/x))
    dalpdx = 1._dp + exp(-x0/x) + x0*exp(-x0/x)/x
  else
    alp = 1._dp + x*(1._dp - exp(x0/x))
    dalpdx = 1._dp - exp(x0/x) + x0*exp(x0/x)/x
  end if

  ft = 5._dp*p/3._dp + alp
  d_ft_dp = 5._dp/3._dp + dalpdx*dxdp
  d_ft_dq = dalpdx*dxdq

  tau = tau0*ft

  d_tau0_dn = kf**2/2._dp

  d_p_dn = -8._dp/3._dp *p/n
  d_p_dgn = gn/(2*(kf*n)**2)

  d_q_dn = -5._dp/3._dp*q/n
  d_q_dlap = 1._dp/(4*kf**2*n)

  dtdn = ft*d_tau0_dn + tau0*(d_ft_dp*d_p_dn + d_ft_dq*d_q_dn)
  ! NB: tau0*d_p_dgn = 3/20 * |grad n|/n = 0.15 * |grad n|/n
  dtdgn = 0.15_dp*gn/n*d_ft_dp!tau0*d_ft_dp*d_p_dgn
  ! NB: tau0*d_q_dln = 3/40 = 0.075
  dtdln = 0.075_dp*d_ft_dq!tau0*d_ft_dq*d_q_dln

end subroutine tau_na


subroutine tau_rpp_unp(n,gn,ln,tau,dtdn,dtdgn,dtdln)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), parameter :: pi = 3.14159265358979323846264338327950288419_dp

  real(dp), intent(in) :: n,gn,ln
  real(dp), intent(out) :: tau,dtdn,dtdgn,dtdln

  !real(dp), parameter :: x0 = 0.5245_dp, ca = 6._dp/x0**2, cb =  -8._dp/x0**3
  !real(dp), parameter :: cc = 3._dp/x0**4
  !real(dp), parameter :: x0 = 0.529772_dp!0.5583_dp

  ! tau-specific parameters
  real(dp), parameter :: x0 = 0.819411_dp, c1= 0.201352_dp, c2 = 0.185020_dp
  real(dp), parameter :: c3 = 1.53804_dp
  real(dp), parameter :: ca = 20._dp/x0**3, cb =  -45._dp/x0**4
  real(dp), parameter :: cc = 36._dp/x0**5, cd = -10._dp/x0**6

  ! r2 SCAN-specific parameters
  real(dp), parameter :: eta = 1.d-3, k0 = 0.174_dp, k1 = 0.065_dp
  real(dp), parameter :: uak = 10._dp/81._dp, epne = (8._dp + 9._dp*eta)
  real(dp), parameter :: cxv(8) = (/ 1._dp, -0.667_dp, -0.4445555_dp,&
  &     -0.663086601049_dp, 1.451297044490_dp, -0.887998041597_dp,&
  &     0.234528941479_dp, -0.023185843322_dp /)


  integer :: i
  real(dp) :: kf,tau0,p,q,ft,d_p_dn,d_p_dgn,d_q_dn,d_q_dlap
  real(dp) :: d_tau0_dn, d_ft_dp,d_ft_dq, x, alp, dalpdx,dxdp,dxdq
  real(dp) :: cpp,cpq,cqq,f4,f4damp,d_f4_dp,d_f4_dq, d_f4damp_dp,d_f4damp_dq
  real(dp) :: f4c, d_f4c_dp, f1,f2,h1,h2

  ! first the approximate tau

  kf = (3*pi**2*n)**(1._dp/3._dp)
  tau0 = 0.3_dp*kf**2*n

  p = (gn/(2*kf*n))**2
  q = ln/(4*kf**2*n)

  ! fourth-order gradient correction for r2 SCAN

  f1 = 0._dp
  f2 = 0._dp
  do i = 1,7
      f1 = f1 + (1._dp*i)*cxv(i+1)
      f2 = f2 + i*(i-1._dp)*cxv(i+1)
  end do

  h1 = 5._dp*(4._dp + 9._dp*eta)*k0*f1/27._dp + uak
  h2 = -2._dp*h1**2/k1

  cqq = (146._dp/2025._dp/k0 - 200._dp/81._dp*f2)/f1
  cpq = 100._dp/243._dp*epne*f2/f1 + 20._dp/9._dp*h1/k0 &
  &     + 100._dp*eta/27._dp - 73._dp/405._dp/(k0*f1)
  cpp = -(h2/2._dp + 5._dp/27*epne*h1*f1 + 25._dp/1458._dp*epne**2*k0*f2 )/(k0*f1)&
  &     - 25._dp*eta**2/9._dp - 200._dp*eta/81._dp

  f4 = (cpp-c3)*p**2 + cpq*p*q + cqq*q**2
  d_f4_dp = 2._dp*(cpp-c3)*p + cpq*q
  d_f4_dq = cpq*p + 2._dp*cqq*q
  f4damp = exp(-(p/c1)**2 - (q/c2)**2)
  d_f4damp_dp = -2._dp*p/c1**2*f4damp
  d_f4damp_dq = -2._dp*q/c2**2*f4damp

  f4c = c3*p**2*exp(-c3*p)
  d_f4c_dp = c3*p*(2._dp - c3*p)*exp(-c3*p)

  x = 1._dp - 40._dp/27._dp*p + 20._dp/9._dp*q + f4*f4damp + f4c
  dxdp = -40._dp/27._dp + d_f4_dp*f4damp + f4*d_f4damp_dp + d_f4c_dp
  dxdq = 20._dp/9._dp + d_f4_dq*f4damp + f4*d_f4damp_dq

  if (x < 0._dp) then
    alp = 0._dp
    dalpdx = 0._dp
  else if ((0._dp <= x).and.(x <= x0)) then
    !alp = x**3*(ca + x*(cb + x*cc))
    !dalpdx = x**2*(3._dp*ca + x*(4._dp*cb + 5._dp*x*cc))
    alp = x**4*(ca + x*(cb + x*(cc + x*cd)))
    dalpdx = x**3*(4._dp*ca + x*(5._dp*cb + x*(6._dp*cc + x*7._dp*cd)))
  else if (x > x0) then
    alp = x
    dalpdx = 1._dp
  end if

  ft = 5._dp*p/3._dp + alp
  d_ft_dp = 5._dp/3._dp + dalpdx*dxdp
  d_ft_dq = dalpdx*dxdq

  tau = tau0*ft

  d_tau0_dn = kf**2/2._dp

  d_p_dn = -8._dp/3._dp *p/n
  d_p_dgn = gn/(2*(kf*n)**2)

  d_q_dn = -5._dp/3._dp*q/n
  d_q_dlap = 1._dp/(4*kf**2*n)

  dtdn = ft*d_tau0_dn + tau0*(d_ft_dp*d_p_dn + d_ft_dq*d_q_dn)
  ! NB: tau0*d_p_dgn = 3/20 * |grad n|/n = 0.15 * |grad n|/n
  dtdgn = 0.15_dp*gn/n*d_ft_dp!tau0*d_ft_dp*d_p_dgn
  ! NB: tau0*d_q_dln = 3/40 = 0.075
  dtdln = 0.075_dp*d_ft_dq!tau0*d_ft_dq*d_q_dlap

end subroutine tau_rpp_unp


subroutine tau_ratpoly_unp(n,gn,ln,tau,dtdn,dtdgn,dtdln)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), parameter :: pi = 3.14159265358979323846264338327950288419_dp

  real(dp), intent(in) :: n,gn,ln
  real(dp), intent(out) :: tau,dtdn,dtdgn,dtdln

  real(dp), parameter :: cc = (800._dp/243._dp-1._dp/9._dp)/2._dp
  real(dp), parameter :: ca = 8._dp/243._dp - (20._dp/27._dp)**2 + cc
  real(dp), parameter :: cb = 8._dp/81._dp - (10._dp/9._dp)**2 + cc
  real(dp), parameter :: cd = 0.417489_dp, ce=0.424039_dp!((20._dp/27._dp)**2 + ca)*((10._dp/9._dp)**2 + cb)!2.035_dp
  real(dp) :: kf,tau0,p,q,ft,d_p_dn,d_p_dgn,d_q_dn,d_q_dlap
  real(dp) :: d_tau0_dn,fge_reg,fge_reg_den,fge_reg_p,fge_reg_q
  real(dp) :: d_fge_reg_dp, d_fge_reg_dq,d_ft_dp,d_ft_dq

  ! first the approximate tau

  kf = (3*pi**2*n)**(1._dp/3._dp)
  tau0 = 0.3_dp*kf**2*n

  p = (gn/(2*kf*n))**2
  q = ln/(4*kf**2*n)

  fge_reg_den = 1._dp + cc*(p-q)**2 + ((p-q)/cd)**4 + ce*p**2*q**2
  fge_reg_p = (1._dp - 20._dp*p/27._dp)**2 + ca*p**2
  fge_reg_q = (1._dp + 10._dp*q/9._dp)**2 + cb*q**2
  fge_reg = fge_reg_p * fge_reg_q / fge_reg_den

  ft = 5._dp*p/3._dp + fge_reg

  tau = ft*tau0

  ! now the derivatives of tau

  d_tau0_dn = kf**2/2._dp

  d_p_dn = -8._dp/3._dp *p/n
  d_p_dgn = gn/(2*(kf*n)**2)

  d_q_dn = -5._dp/3._dp*q/n
  d_q_dlap = 1._dp/(4*kf**2*n)

  d_fge_reg_dp = ( (-40._dp/27._dp + 2._dp*(ca + (20._dp/27._dp)**2)*p)*fge_reg_q &
 &    - 2._dp*(cc*(p-q) + 2._dp*(p-q)**3/cd**4 + ce*p*q**2)*fge_reg )/fge_reg_den

  d_fge_reg_dq = ( (20._dp/9._dp + 2._dp*(cb + (10._dp/9._dp)**2)*q)*fge_reg_p &
 &    - 2._dp*(cc*(q-p) + 2._dp*(q-p)**3/cd**4 + ce*p**2*q)*fge_reg )/fge_reg_den

  d_ft_dp = 5._dp/3._dp + d_fge_reg_dp
  d_ft_dq = d_fge_reg_dq

  dtdn = ft*d_tau0_dn + tau0*(d_ft_dp*d_p_dn + d_ft_dq*d_q_dn)
  ! NB: tau0*d_p_dgn = 3/20 * |grad n|/n = 0.15 * |grad n|/n
  dtdgn = 0.15_dp*gn/n*d_ft_dp!tau0*d_ft_dp*d_p_dgn
  ! NB: tau0*d_q_dln = 3/40 = 0.075
  dtdln = 0.075_dp*d_ft_dq!tau0*d_ft_dq*d_q_dln

end subroutine tau_ratpoly_unp


subroutine tau_non_emp_gga_unp(n,gn,ln,tau,dtdn,dtdgn,dtdln)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), parameter :: pi = 3.14159265358979323846264338327950288419_dp

  real(dp), intent(in) :: n,gn,ln
  real(dp), intent(out) :: tau,dtdn,dtdgn,dtdln

  real(dp) :: kf,tau0,p,q,ft,qinterp,pinterp,b1q2,d_p_dn,d_p_dgn,d_q_dn
  real(dp) :: d_q_dlap,qge,arsinp2,darsinp2dp
  real(dp) :: d_tau0_dn,qi_den,pi_den,mod_ge
  real(dp) :: d_qinterp_dq,d_qinterp_dp,d_pinterp_dp,d_ft_dp,d_ft_dq
  real(dp) :: d_tau_dn, d_tau_dp, d_tau_dq

  ! first the approximate tau

  kf = (3*pi**2*n)**(1._dp/3._dp)
  tau0 = 0.3_dp*kf**2*n

  p = (gn/(2*kf*n))**2

  arsinp2 = log(p/2._dp + (1._dp + p**2/4._dp)**(0.5_dp))
  pi_den = 1._dp + 5._dp/3._dp*p + 25._dp/9._dp*p**2 + 1._dp/80._dp*p**3*arsinp2**2
  pinterp = (1._dp + 5._dp/27._dp*p + 83._dp/243._dp*p**2 + 1._dp/27._dp*p**4)/pi_den

  ft = 5._dp*p/3._dp + pinterp

  tau = ft*tau0

  ! now the derivatives of tau

  d_tau0_dn = kf**2/2._dp

  d_p_dn = -8._dp/3._dp *p/n
  d_p_dgn = gn/(2*(kf*n)**2)

  darsinp2dp = 0.5_dp/(1._dp + p**2/4._dp)**(0.5_dp)
  d_pinterp_dp = (5._dp/27._dp + 166._dp/243._dp*p + 4._dp/27._dp*p**3 &
 &     - (5._dp/3._dp + 50._dp/9._dp*p + 3._dp/80._dp*p**2*arsinp2**2 &
 &     + 1._dp/40._dp*p**3*arsinp2*darsinp2dp)*pinterp)/pi_den


  d_ft_dp = 5._dp/3._dp + d_pinterp_dp
  d_ft_dq = 0._dp
  dtdn = ft*d_tau0_dn + tau0*d_ft_dp*d_p_dn
  dtdgn = tau0*d_ft_dp*d_p_dgn
  dtdln = 0._dp

end subroutine tau_non_emp_gga_unp


subroutine tau_non_emp_unp(n,gn,ln,tau,dtdn,dtdgn,dtdln)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), parameter :: pi = 3.14159265358979323846264338327950288419_dp

  real(dp), intent(in) :: n,gn,ln
  real(dp), intent(out) :: tau,dtdn,dtdgn,dtdln

  real(dp), parameter :: c0=0.41_dp,c1= 0.11_dp
  real(dp) :: kf,tau0,p,q,ft,qinterp,pinterp,b1q2,d_p_dn,d_p_dgn,d_q_dn,d_q_dlap,qge
  real(dp) :: d_tau0_dn,qi_den,pi_den,mod_ge
  real(dp) :: d_qinterp_dq,d_qinterp_dp,d_pinterp_dp,d_ft_dp,d_ft_dq
  real(dp) :: d_tau_dn, d_tau_dp, d_tau_dq

  ! first the approximate tau

  kf = (3*pi**2*n)**(1._dp/3._dp)
  tau0 = 0.3_dp*kf**2*n

  p = (gn/(2*kf*n))**2
  q = ln/(4*kf**2*n)

  pi_den = 1._dp + 5._dp/3._dp*p + 25._dp/9._dp*p**2 + (p/c0)**3
  pinterp = (1._dp + 5._dp/27._dp*p + (25._dp/81._dp + 8._dp/243._dp)*p**2)/pi_den

  qge = 20._dp/9._dp*q + 8._dp/81._dp*q**2
  qi_den = 1._dp + p/10._dp + (qge/(1._dp + p/c1))**2
  qinterp = qge/qi_den**(0.5_dp)

  mod_ge = 0._dp
  if (pinterp + qinterp >0._dp) mod_ge = pinterp + qinterp

  ft = 5._dp*p/3._dp + mod_ge

  tau = ft*tau0

  ! now the derivatives of tau

  d_tau0_dn = kf**2/2._dp

  d_p_dn = -8._dp/3._dp *p/n
  d_p_dgn = gn/(2*(kf*n)**2)

  d_q_dn = -5._dp/3._dp*q/n
  d_q_dlap = 1._dp/(4*kf**2*n)

  d_pinterp_dp = ( 5._dp/27._dp + 2*(25._dp/81._dp + 8._dp/243._dp)*p &
 &    - (5._dp/3._dp + 50._dp/9._dp*p + 3*p**2/c0**3)*pinterp)/pi_den

  d_qinterp_dq = (20._dp/9._dp + 16._dp/81._dp*q)/qi_den**(0.5_dp) &
 &    - qge/(1._dp + p/c1)**2*(20._dp/9._dp + 16._dp/81._dp*q)*qinterp/qi_den
  d_qinterp_dp = (-1._dp/20._dp + qge**2/c1/(1._dp + p/c1)**3 )* qinterp/qi_den

  d_ft_dp = 5._dp/3._dp
  d_ft_dq = 0._dp
  if (pinterp + qinterp >0._dp) then
    d_ft_dp = d_ft_dp + d_pinterp_dp + d_qinterp_dp
    d_ft_dq = d_ft_dq + d_qinterp_dq
  end if
  dtdn = ft*d_tau0_dn + tau0*(d_ft_dp*d_p_dn + d_ft_dq*d_q_dn)
  dtdgn = tau0*d_ft_dp*d_p_dgn
  dtdln = tau0*d_ft_dq*d_q_dlap

end subroutine tau_non_emp_unp


subroutine tau_non_emp_unp_simple(n,gn,ln,tau,dtdn,dtdgn,dtdln)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), parameter :: pi = 3.14159265358979323846264338327950288419_dp

  real(dp), intent(in) :: n,gn,ln
  real(dp), intent(out) :: tau,dtdn,dtdgn,dtdln

  real(dp), parameter :: b2 = 3.339_dp, b1=10._dp/9._dp*b2/(b2 - 1._dp)

  real(dp) :: kf,tau0,p,q,ft,qinterp,pinterp,b1q2,d_p_dn,d_p_dgn,d_q_dn,d_q_dlap
  real(dp) :: d_tau0_dn,qi_den,pi_den
  real(dp) :: d_qinterp_dq,d_pinterp_dp,d_ft_dp,d_ft_dq
  real(dp) :: d_tau_dn, d_tau_dp, d_tau_dq

  ! first the approximate tau

  kf = (3*pi**2*n)**(1._dp/3._dp)
  tau0 = 0.3_dp*kf**2*n

  p = (gn/(2*kf*n))**2
  q = ln/(4*kf**2*n)

  pi_den = 1._dp + b2*40._dp*p/27._dp
  pinterp = -40._dp*p/27._dp/pi_den

  b1q2 = (b1*q)**2
  qi_den = 1._dp + b1q2
  qinterp = 20._dp*q/9._dp/qi_den

  ft = 5._dp*p/3._dp + 1._dp + pinterp + qinterp

  tau = ft*tau0

  ! now the derivatives of tau

  d_tau0_dn = kf**2/2._dp

  d_p_dn = -8._dp/3._dp *p/n
  d_p_dgn = gn/(2*(kf*n)**2)

  d_q_dn = -5._dp/3._dp*q/n
  d_q_dlap = 1._dp/(4*kf**2*n)

  d_pinterp_dp = -40._dp/27._dp*(1._dp + b2*pinterp)/pi_den

  d_qinterp_dq = (20._dp/9._dp - 2*b1**2*q*qinterp)/qi_den

  d_ft_dp = 5._dp/3._dp + d_pinterp_dp
  d_ft_dq = d_qinterp_dq

  dtdn = ft*d_tau0_dn + tau0*(d_ft_dp*d_p_dn + d_ft_dq*d_q_dn)
  dtdgn = tau0*d_ft_dp*d_p_dgn
  dtdln = tau0*d_ft_dq*d_q_dlap

end subroutine tau_non_emp_unp_simple
