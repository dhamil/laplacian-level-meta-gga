#!/bin/zsh

mkdir -p data
rm -f runderivs
#gfortran -O3 -o runderivs fdiff_v_analyt.f90 subrout/lap_lev_no_pars.f90
gfortran -O3 -ffree-form -o runderivs fdiff_v_analyt.f90 subrout/lap_lev_no_pars.f90 subrout/xscan.f subrout/cscan.f subrout/orb_free_scan_mrt.f90 subrout/orb_free_scan_kp.f90

./runderivs

rm -f runvks
