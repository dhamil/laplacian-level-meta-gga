import matplotlib.pyplot as plt
import numpy as np

with open('tmp.txt','r') as infl:
    for ln in infl:
        fl = ln.strip()
        nfl = fl.split('_')[0]
        break


if fl == 'LLMG_ks_pot.csv':
    fnl = 'LL-MGGA'
elif fl == 'SCAN_L_ks_pot.csv':
    fnl = 'SCAN-L'
elif fl == 'rSCAN_L_ks_pot.csv':
    fnl = 'rSCAN-L'
elif fl == 'r2SCAN_L_ks_pot.csv':
    fnl = 'r$^2$SCAN-L'
elif fl == 'PBE_ks_pot.csv':
    fnl = 'PBE'
elif fl == 'OFR2_ks_pot.csv':
    fnl = 'OFR2'


r,vx,vc,vks = np.transpose(np.genfromtxt('./data/'+fl,delimiter=',',skip_header=1))

fsz = 18
clist = ['tab:blue','tab:orange','tab:green','tab:red']

fig,ax = plt.subplots(figsize=(8,6))

ax.plot(r,-1.0/r,color=clist[0],label='$-1/r$')
ax.plot(r,vx,color=clist[1],label='$v^{\\uparrow}_{\\mathrm{x}}(r)$')
ax.plot(r,vc,color=clist[2],label='$v^{\\uparrow}_{\\mathrm{c}}(r)$')
ax.plot(r,vks,color=clist[3],label='$v^{\\uparrow}_{\\mathrm{KS}}(r)$')

ax.set_xlim([0.0,6.0])
ax.set_ylim([-5.0,1.5])
ax.hlines(0.0,0.0,6.0,color='gray')
ax.legend(loc='lower center',fontsize=16,ncol=2)
ax.tick_params(axis='both',labelsize=18)
plt.annotate(fnl,(5.,-4.8),fontsize=24)
#plt.title(fnl+' X, C and KS potential using exact hydrogen atom density')
#plt.show() ;exit()
plt.savefig('./figs/{:}_h_atom_potential_finite_diff.pdf'.format(nfl),dpi=600,bbox_inches='tight')
