subroutine fdiff(nr,spc,order,fun,dfun)

  implicit none

  integer, parameter :: dp = selected_real_kind(15, 307)

  integer, intent(in) :: order,nr
  real(dp), intent(in) :: spc
  real(dp), intent(in),dimension(nr) :: fun
  real(dp), intent(out), dimension(nr) :: dfun

  integer :: i,nfdf
  real(dp), dimension(7) :: fdc
  real(dp),dimension(7+order-1) :: fdf

  ! coefficients from B. Fornberg, Math. Comp. 51, 699-706 (1988)
  !  doi: 10.1090/S0025-5718-1988-0935077-0

  if (order==1) then
    fdc = (/ -1.d0/60.d0, 3.d0/20.d0, -3.d0/4.d0, &
   &        0.d0, 3.d0/4.d0, -3.d0/20.d0, 1.d0/60.d0 /)
    fdf = (/-49.d0/20.d0,6.d0,-15.d0/2.d0,20.d0/3.d0, &
   &       -15.d0/4.d0,6.0/5.d0,-1.0/6.d0 /)
  elseif (order==2) then
    fdc = (/ 1.d0/90.d0, -3.d0/20.d0, 3.d0/2.d0,&
   &        -49.d0/18.d0, 3.d0/2.d0, -3.d0/20.d0, 1.d0/90.d0/)
    fdf = (/ 469.d0/90.d0,-223.d0/10.d0, 879.d0/20.d0,&
   &        -949.d0/18.d0, 41.d0,-201.d0/10.d0,1019.d0/180.d0,-7.d0/10.d0 /)
  endif

  nfdf = 7+order-1
  do i = 1,nr
    if (i < 4) then
      dfun(i) = dot_product(fdf,fun(i:i+nfdf-1))
    elseif (i > nr-3) then
      dfun(i) = -dot_product(fdf,fun(i-nfdf+1:i))
    else
      dfun(i) = dot_product(fdc,fun(i-3:i+3))
    end if
  end do
  dfun = dfun/spc**order

end subroutine fdiff

!-------------------------------------------------------------------------------

subroutine spherical_divergence(nr,spc,r,fun,fun_div)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  integer, intent(in) :: nr
  real(dp), intent(in) :: spc
  real(dp), intent(in),dimension(nr) :: r,fun
  real(dp), intent(out),dimension(nr) :: fun_div

  call fdiff(nr,spc,1,r**2*fun,fun_div)
  fun_div = fun_div/r**2

end subroutine spherical_divergence

!-------------------------------------------------------------------------------

subroutine spherical_laplacian(nr,spc,r,fun,fun_lap)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  integer, intent(in) :: nr
  real(dp), intent(in) :: spc
  real(dp), intent(in),dimension(nr) :: r,fun
  real(dp), intent(out),dimension(nr) :: fun_lap

  call fdiff(nr,spc,2,r*fun,fun_lap)
  fun_lap = fun_lap/r

end subroutine spherical_laplacian

!-------------------------------------------------------------------------------

subroutine vxc(wfxc,nr,spc,r,nup,ndn,gn_up,gn_dn,gn, &
  &    lap_up,lap_dn,vx_up,vx_dn,vc_up,vc_dn)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)
  real(dp), parameter :: pi = 3.14159265358979323846264338327950288419_dp

  character(4), intent(in) :: wfxc
  integer,intent(in) :: nr
  real(dp), intent(in) :: spc
  real(dp),intent(in), dimension(nr) :: r,nup,ndn,gn_up,gn_dn,gn,lap_up,lap_dn
  real(dp),intent(out), dimension(nr) :: vx_up,vx_dn,vc_up,vc_dn

  integer :: i
  real(dp), dimension(nr) :: ex, vxu, vxd, vx_gu, vx_gd, vx_lu, vx_ld
  real(dp), dimension(nr) :: vx_grad_up, vx_grad_dn, vx_lap_up, vx_lap_dn

  real(dp), dimension(nr) :: ec, vcu, vcd, vc_gu, vc_gd, vc_gn, vc_lu, vc_ld
  real(dp), dimension(nr):: vc_grad_up, vc_grad_dn, vc_lap_up, vc_lap_dn, vc_grad

  do i = 1,nr

    if (wfxc == 'LLMG') then

      call lap_lev_x(nup(i),ndn(i),abs(gn_up(i)),abs(gn_dn(i)),&
       &    lap_up(i),lap_dn(i),ex(i),vxu(i),vxd(i),&
       &    vx_gu(i),vx_gd(i),vx_lu(i),vx_ld(i))
      call lap_lev_c(nup(i),ndn(i),abs(gn_up(i)),abs(gn_dn(i)),&
       &    abs(gn(i)),lap_up(i),lap_dn(i),ec(i),vcu(i),vcd(i),vc_gu(i), &
       &    vc_gd(i),vc_gn(i),vc_lu(i), vc_ld(i))

    else if ((wfxc=='SCAL').or.(wfxc=='RSCL').or.(wfxc=='R2SL')) then

      call scan_orb_free_x(wfxc,nup(i),ndn(i),abs(gn_up(i)),abs(gn_dn(i)),&
       &    lap_up(i),lap_dn(i),ex(i),vxu(i),vxd(i),&
       &    vx_gu(i),vx_gd(i),vx_lu(i),vx_ld(i))
      call scan_orb_free_c(wfxc,nup(i),ndn(i),abs(gn_up(i)),abs(gn_dn(i)),&
       &    abs(gn(i)),lap_up(i),lap_dn(i),ec(i),vcu(i),vcd(i),vc_gu(i), &
       &    vc_gd(i),vc_gn(i),vc_lu(i), vc_ld(i))

    else if (wfxc=='PBEG') then
      call pbe_x(nup(i),ndn(i),abs(gn_up(i)),abs(gn_dn(i)),ex(i),vxu(i),vxd(i),&
      &    vx_gu(i),vx_gd(i))
      call pbe_c(nup(i),ndn(i),abs(gn_up(i)),abs(gn_dn(i)),abs(gn(i)),ec(i),vcu(i),vcd(i),&
      &    vc_gu(i),vc_gd(i),vc_gn(i))

      vx_lu(i) = 0._dp ; vx_ld(i) = 0._dp
      vc_lu(i) = 0._dp ; vc_ld(i) = 0._dp

    else if (wfxc=='OFR2') then

      call r2scan_of_x(nup(i),ndn(i),abs(gn_up(i)),abs(gn_dn(i)),lap_up(i),&
      &    lap_dn(i),ex(i),vxu(i),vxd(i),vx_gu(i),vx_gd(i),vx_lu(i),vx_ld(i))

      call r2scan_of_c(nup(i),ndn(i),abs(gn_up(i)),abs(gn_dn(i)),abs(gn(i)),&
      &    lap_up(i),lap_dn(i),ec(i),vcu(i),vcd(i),vc_gu(i), &
      &    vc_gd(i),vc_gn(i),vc_lu(i), vc_ld(i) )

    end if

  end do

  ! just a check here that the energies look correct
  print*, 'Ex =',4*pi*sum(ex*r**2)*spc,'    hartree'
  print*, 'Ec =',4*pi*sum(ec*r**2)*spc,'    hartree'
  print*, 'Exc = ',4*pi*sum((ex+ec)*r**2)*spc,'    hartree'

  ! gradient-dependent parts of the X potential
  call spherical_divergence(nr,spc,r,vx_gu*gn_up/abs(gn_up),vx_grad_up)
  call spherical_divergence(nr,spc,r,vx_gd*gn_dn/abs(gn_dn),vx_grad_dn)

  ! laplacian-dependent parts of the X potential
  call spherical_laplacian(nr,spc,r,vx_lu,vx_lap_up)
  call spherical_laplacian(nr,spc,r,vx_ld,vx_lap_dn)

  ! full X potential
  vx_up = vxu - vx_grad_up + vx_lap_up
  vx_dn = vxd - vx_grad_dn + vx_lap_dn

  ! gradient-dependent parts of the C potential
  call spherical_divergence(nr,spc,r,vc_gu*gn_up/abs(gn_up),vc_grad_up)
  call spherical_divergence(nr,spc,r,vc_gd*gn_dn/abs(gn_dn),vc_grad_dn)
  vc_grad = 0._dp
!  call spherical_divergence(nr,spc,r,(gn_up + gn_dn)*vc_gn/abs(gn),vc_grad)

  ! laplacian-dependent parts of the C potential
  call spherical_laplacian(nr,spc,r,vc_lu,vc_lap_up)
  call spherical_laplacian(nr,spc,r,vc_ld,vc_lap_dn)

  ! full C potential
  vc_up = vcu - vc_grad - vc_grad_up + vc_lap_up
  vc_dn = vcd - vc_grad - vc_grad_dn + vc_lap_dn

end subroutine vxc

!-------------------------------------------------------------------------------

subroutine v_hartree(nr,r,vh)
  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  integer, intent(in) :: nr
  real(dp),intent(in),dimension(nr) :: r
  real(dp),intent(out),dimension(nr) :: vh
  ! hartree potential for exact hydrogen atom density
  vh = 1.d0/r*(1.d0 - exp(-2*r)*(1.d0 + r))

end subroutine v_hartree

!-------------------------------------------------------------------------------
program write_vks

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  integer, parameter :: nlen = 10**4
  real(dp),parameter :: spc = 10.d0/nlen,pi=3.1415926535897932384626433832795_dp
  character(4), parameter :: wdfa = 'OFR2'

  logical :: do_h_atom
  integer :: i
  logical :: file_exists

  character(len=:),allocatable :: fname
  real(dp),dimension(nlen) :: r,n_up,gn_up,lap_up,n_dn,gn_dn,lap_dn,gn
  real(dp),dimension(nlen) :: vh,vx,vc,vks
  real(dp),dimension(nlen) :: vx_dum,vc_dum
  real(dp) :: kf,n0

  do_h_atom = .true.

  r = (/ ( i*spc, i = 1,nlen) /)

  if (do_h_atom) then

    n_dn = 0.d0
    gn_dn = 0.d0
    lap_dn = 0.d0

    n_up = exp(-2*r)/pi ! exact hydrogen atom density
    gn_up = -2*n_up
    gn = gn_up
    lap_up = 4*n_up*(1.d0 - 1.d0/r)
  else
    kf = (9.d0*pi/4.d0)**(1.d0/3.d0)/4.d0
    n0 = 3.d0/(4.d0**4*pi)
    n_up = n0*(1.d0 + sin(2*kf*r)/(2*kf*r))  ! slowly-varying, rs = 4 density
    gn_up = n0*(cos(2*kf*r) - sin(2*kf*r)/(2*kf*r))/r
    lap_up = - 2*kf*sin(2*kf*r)/r
    n_dn = n_up
    gn_dn = gn_up
    gn = (gn_up**2 + gn_dn**2 + 2*gn_up*gn_dn)**(0.5d0)
    lap_dn = lap_up
  end if

  call v_hartree(nlen,r,vh)
  call vxc(wdfa,nlen,spc,r,n_up,n_dn,gn_up,gn_dn,gn, &
    &    lap_up,lap_dn,vx,vx_dum,vc,vc_dum)

  vks = -1.d0/r + vh + vx + vc

  if (wdfa == 'LLMG') then
    fname = 'LLMG_ks_pot.csv'
  elseif (wdfa == 'SCAL') then
    fname = 'SCAN_L_ks_pot.csv'
  elseif (wdfa == 'RSCL') then
    fname = 'rSCAN_L_ks_pot.csv'
  elseif (wdfa == 'R2SL') then
    fname = 'r2SCAN_L_ks_pot.csv'
  elseif (wdfa == 'PBEG') then
    fname = 'PBE_ks_pot.csv'
  elseif (wdfa == 'OFR2') then
    fname = 'OFR2_ks_pot.csv'
  end if

  inquire(file='./data/'//fname,exist=file_exists)
  if (file_exists) then
    open(unit=1,file='./data/'//fname,status='replace',action='readwrite',recl=nlen-6)
  else
    open(unit=1,file='./data/'//fname,status='new',action='readwrite',recl=nlen-6)
  end if

  write(1,*) 'r (bohr), vx, vc, vks'
  do i = 1, nlen
    ! a poor man's CSV file
    write(1,*) r(i),',', vx(i),',', vc(i),',', vks(i)
  end do
  close(1)

  open(unit=2,file='tmp.txt',action='write')
  write(2,*) fname
  close(2)

end program write_vks
