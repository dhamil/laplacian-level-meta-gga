#!/bin/zsh

mkdir -p data
mkdir -p figs
rm -f runvks
#gfortran -ffree-form -Wall -O3 -o runvks ks_potential.f90 subrout/{lap_lev,orb_free_scan,pbe}.f90 subrout/{xscan,cscan}.f

ifort -g -traceback -free -O3 -heap-arrays -o runvks ks_potential.f90 subrout/{lap_lev_no_pars,orb_free_scan_mrt,orb_free_scan_kp,pbe}.f90 subrout/{xscan,cscan}.f

./runvks
python3 ks_plot.py

rm -f tmp.txt runvks  lap_lev_pars.mod
rm -rf runvks.dSYM
