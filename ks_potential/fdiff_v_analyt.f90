program test_implement

  implicit none

  integer,parameter :: nstep=20,step=4,whichen=0

  integer :: iter,in,ign,it,itn,itg,itt

  real(8),dimension(nstep) :: n,nu,nd,gnu,gnd,lu,ld
  real(8),dimension(3) :: xc,xc_d1,xc_d2,xc_dd1,xc_dd
  real(8),dimension(3) :: xc_dd2,xc_t1,xc_t2
  real(8) :: appd,gnt
  real(8) :: dn1,dn2,dgn1,dgn2,dgn,dt1,dt2
  real(8) :: tdn1,tdn2,tdgn1,tdgn2,tdgn,tdt1,tdt2

  real(8),parameter :: n_min = 1.d-3,n_max=2.d0,h=1.d-4

  if (whichen==0) then
    open(unit=1,file='./data/llmg_derivs_xc.csv')
  else if (whichen==1) then
    open(unit=1,file='./data/llmg_derivs_x.csv')
  else if (whichen==2) then
    open(unit=1,file='./data/llmg_derivs_c.csv')
  end if

  write(1,*) 'n_up = x*n',',','gn_up = x*gn',',','lap_up = x*lap',',',&
  &       '<d/dn> up',',','<d/dn> dn',',','<d/dgn> up',',','<d/dgn> dn',',','<d/dgn>',',',&
  &       '<d/dlap> up',',','<d/dlap> dn'

  tdn1=0.d0 ; tdn2=0.d0
  tdgn1=0.d0 ; tdgn2=0.d0 ; tdgn=0.d0
  tdt1=0.d0 ;   tdt2=0.d0

  do iter = 0,nstep-1
    n(iter+1) = n_min + (1.d0*iter)/(1.d0*nstep)*(n_max-n_min)
  end do

  do itn = 1,step-1

    nu = 1.d0*itn/(1.d0*step)*n
    nd = (1.d0- 1.d0*itn/(1.d0*step))*n

    do itg = 1,step-1

      gnu = 1.d0*itg/(1.d0*step)*n
      gnd = n-gnu

      do itt = 1,step-1

        lu = 1.d0*itt/(1.d0*step)*n
        ld = n-lu

        dn1=0.d0
        dn2=0.d0
        dgn1=0.d0
        dgn2=0.d0
        dgn = 0.d0
        dt1=0.d0
        dt2=0.d0

        do in = 1,nstep
          do ign = 1,nstep
            do it = 1,nstep


! central value
              call llmgga(nu(in),nd(in),gnu(ign),gnd(ign),&
              & gnu(ign)+gnd(ign),lu(it),ld(it),&
              & xc(2),xc_d1(2),xc_dd1(2),&
              & xc_d2(2),xc_dd2(2),xc_dd(2),&
              & xc_t1(2),xc_t2(2),whichen)

! d n_up

              call llmgga(nu(in)-h,nd(in),gnu(ign),gnd(ign),&
              & gnu(ign)+gnd(ign),lu(it),ld(it),&
              & xc(1),xc_d1(1),xc_dd1(1),&
              & xc_d2(1),xc_dd2(1),xc_dd(1),&
              & xc_t1(1),xc_t2(1),whichen)

              call llmgga(nu(in)+h,nd(in),gnu(ign),gnd(ign),&
              & gnu(ign)+gnd(ign),lu(it),ld(it),&
              & xc(3),xc_d1(3),xc_dd1(3),&
              & xc_d2(3),xc_dd2(3),xc_dd(3),&
              & xc_t1(3),xc_t2(3),whichen)

              appd = (xc(3)-xc(1))/(2*h)
              dn1 = dn1 + abs(xc_d1(2)-appd)/&
             &      max(1d-6,abs(xc_d1(2)+appd))/(1.d0*nstep-2.d0)**3

! d n_dn

             call llmgga(nu(in),nd(in)-h,gnu(ign),gnd(ign),&
             & gnu(ign)+gnd(ign),lu(it),ld(it),&
             & xc(1),xc_d1(1),xc_dd1(1),&
             & xc_d2(1),xc_dd2(1),xc_dd(1),&
             & xc_t1(1),xc_t2(1),whichen)

             call llmgga(nu(in),nd(in)+h,gnu(ign),gnd(ign),&
             & gnu(ign)+gnd(ign),lu(it),ld(it),&
             & xc(3),xc_d1(3),xc_dd1(3),&
             & xc_d2(3),xc_dd2(3),xc_dd(3),&
             & xc_t1(3),xc_t2(3),whichen)

             appd = (xc(3)-xc(1))/(2*h)
             dn2 = dn2 + abs(xc_d2(2)-appd)/&
            &      max(1d-6,abs(xc_d2(2)+appd))/(1.d0*nstep-2.d0)**3

! d |nabla n_up|
            gnt = ((gnu(ign)-h)**2+gnd(ign)**2 + 2*(gnu(ign)-h)*gnd(ign))**(0.5)
            call llmgga(nu(in),nd(in),gnu(ign)-h,gnd(ign),&
            & gnt,lu(it),ld(it),&
            & xc(1),xc_d1(1),xc_dd1(1),&
            & xc_d2(1),xc_dd2(1),xc_dd(1),&
            & xc_t1(1),xc_t2(1),whichen)

            gnt = ((gnu(ign)+h)**2+gnd(ign)**2 + 2*(gnu(ign)+h)*gnd(ign))**(0.5)
            call llmgga(nu(in),nd(in),gnu(ign)+h,gnd(ign),&
            & gnt,lu(it),ld(it),&
            & xc(3),xc_d1(3),xc_dd1(3),&
            & xc_d2(3),xc_dd2(3),xc_dd(3),&
            & xc_t1(3),xc_t2(3),whichen)

            appd = (xc(3)-xc(1))/(2*h)
            dgn1 = dgn1 + abs(xc_dd1(2)-appd)/&
           &      max(1d-6,abs(xc_dd1(2)+appd))/(1.d0*nstep-2.d0)**3

! d |nabla n|
            call llmgga(nu(in),nd(in),gnu(ign),gnd(ign),&
            & gnu(ign)+gnd(ign)-h,lu(it),ld(it),&
            & xc(1),xc_d1(1),xc_dd1(1),&
            & xc_d2(1),xc_dd2(1),xc_dd(1),&
            & xc_t1(1),xc_t2(1),whichen)

            call llmgga(nu(in),nd(in),gnu(ign),gnd(ign),&
            & gnu(ign)+gnd(ign)+h,lu(it),ld(it),&
            & xc(3),xc_d1(3),xc_dd1(3),&
            & xc_d2(3),xc_dd2(3),xc_dd(3),&
            & xc_t1(3),xc_t2(3),whichen)

            appd = (xc(3)-xc(1))/(2*h)
            dgn = dgn + abs(xc_dd(2)-appd)/&
            &      max(1d-6,abs(xc_dd(2)+appd))/(1.d0*nstep-2.d0)**3


! d |nabla n_dn|

           gnt = ((gnd(ign)-h)**2+gnu(ign)**2 + 2*(gnd(ign)-h)*gnu(ign))**(0.5)
           call llmgga(nu(in),nd(in),gnu(ign),gnd(ign)-h,&
           & gnt,lu(it),ld(it),&
           & xc(1),xc_d1(1),xc_dd1(1),&
           & xc_d2(1),xc_dd2(1),xc_dd(1),&
           & xc_t1(1),xc_t2(1),whichen)

           gnt = ((gnd(ign)+h)**2+gnu(ign)**2 + 2*(gnd(ign)+h)*gnu(ign))**(0.5)
           call llmgga(nu(in),nd(in),gnu(ign),gnd(ign)+h,&
           & gnt,lu(it),ld(it),&
           & xc(3),xc_d1(3),xc_dd1(3),&
           & xc_d2(3),xc_dd2(3),xc_dd(3),&
           & xc_t1(3),xc_t2(3),whichen)

           appd = (xc(3)-xc(1))/(2*h)
           dgn2 = dgn2 + abs(xc_dd2(2)-appd)/&
          &      max(1d-6,abs(xc_dd2(2)+appd))/(1.d0*nstep-2.d0)**3

! d lap_up

          call llmgga(nu(in),nd(in),gnu(ign),gnd(ign),&
          & gnu(ign)+gnd(ign),lu(it)-h,ld(it),&
          & xc(1),xc_d1(1),xc_dd1(1),&
          & xc_d2(1),xc_dd2(1),xc_dd(1),&
          & xc_t1(1),xc_t2(1),whichen)

          call llmgga(nu(in),nd(in),gnu(ign),gnd(ign),&
          & gnu(ign)+gnd(ign),lu(it)+h,ld(it),&
          & xc(3),xc_d1(3),xc_dd1(3),&
          & xc_d2(3),xc_dd2(3),xc_dd(3),&
          & xc_t1(3),xc_t2(3),whichen)

          appd = (xc(3)-xc(1))/(2*h)
          dt1 = dt1 + abs(xc_t1(2)-appd)/&
         &      max(1d-6,abs(xc_t1(2)+appd))/(1.d0*nstep-2.d0)**3

! d lap_dn

         call llmgga(nu(in),nd(in),gnu(ign),gnd(ign),&
         & gnu(ign)+gnd(ign),lu(it),ld(it)-h,&
         & xc(1),xc_d1(1),xc_dd1(1),&
         & xc_d2(1),xc_dd2(1),xc_dd(1),&
         & xc_t1(1),xc_t2(1),whichen)

         call llmgga(nu(in),nd(in),gnu(ign),gnd(ign),&
         & gnu(ign)+gnd(ign),lu(it),ld(it)+h,&
         & xc(3),xc_d1(3),xc_dd1(3),&
         & xc_d2(3),xc_dd2(3),xc_dd(3),&
         & xc_t1(3),xc_t2(3),whichen)

         appd = (xc(3)-xc(1))/(2*h)
         dt2 = dt2 + abs(xc_t2(2)-appd)/&
        &      max(1d-6,abs(xc_t2(2)+appd))/(1.d0*nstep-2.d0)**3

            end do
          end do
        end do

        write(1,*) (1.d0*itn)/(1.d0*step),',',(1.d0*itg)/(1.d0*step),',',(1.d0*itt)/(1.d0*step),&
        &',',dn1,',',dn2,',',dgn1,',',dgn2,',',dgn,',',dt1,',',dt2

        tdn1 = tdn1 + dn1/(1.d0*step-1.d0)**3
        tdn2 = tdn2 + dn2/(1.d0*step-1.d0)**3
        tdgn1 = tdgn1 + dgn1/(1.d0*step-1.d0)**3
        tdgn2 = tdgn2 + dgn2/(1.d0*step-1.d0)**3
        tdgn = tdgn + dgn/(1.d0*step-1.d0)**3
        tdt1 = tdt1 + dt1/(1.d0*step-1.d0)**3
        tdt2 = tdt2 + dt2/(1.d0*step-1.d0)**3

      end do
    end do
  end do

  write(1,*) '',',','',',','AVERAGES',&
  &',',tdn1,',',tdn2,',',tdgn1,',',tdgn2,',',tdgn,',',tdt1,',',tdt2

end program test_implement


subroutine llmgga(nu,nd,gnu,gnd,gn,lu,ld,&
  & en,vu,vgu,vd,vgd,vgn,vlu,vld,wen)

  implicit none

  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in) :: nu,nd,gnu,gnd,gn,lu,ld
  integer, intent(in) :: wen
  real(dp), intent(out) :: en,vu,vgu,vd,vgd,vgn,vlu,vld

  real(dp) :: ex,vxu,vxd,vxgu,vxgd,vxlu,vxld
  real(dp) :: ec,vcu,vcd,vcgu,vcgd,vclu,vcld

  ! wen = 0 --> XC
  ! wen = 1 --> X
  ! wen = 2 --> C

  if (wen == 0) then
    !call lap_lev_x(nu,nd,gnu,gnd,lu,ld,&
    !  & ex,vxu,vxd,vxgu,vxgd,vxlu,vxld)
    !call scan_orb_free_x('R2SL',nu,nd,gnu,gnd,lu,ld,&
    ! & ex,vxu,vxd,vxgu,vxgd,vxlu,vxld)
    call r2scan_of_x(nu,nd,gnu,gnd,lu,ld,&
      & ex,vxu,vxd,vxgu,vxgd,vxlu,vxld)
    !call lap_lev_c(nu,nd,gnu,gnd,gn,lu,ld,&
    !  & ec,vcu,vcd,vcgu,vcgd,vgn,vclu,vcld)
    !call scan_orb_free_c('R2SL',nu,nd,gnu,gnd,gn,lu,ld,&
    ! & ec,vcu,vcd,vcgu,vcgd,vgn,vclu,vcld)
    call r2scan_of_c(nu,nd,gnu,gnd,gn,lu,ld,&
      & ec,vcu,vcd,vcgu,vcgd,vgn,vclu,vcld)
      en = ex + ec
      vu = vxu + vcu ; vd = vxd + vcd
      vgu = vxgu + vcgu ; vgd = vxgd + vcgd
      vlu = vxlu + vclu ; vld = vxld + vcld
  else if (wen == 1) then
    call lap_lev_x(nu,nd,gnu,gnd,lu,ld,&
      & en,vu,vd,vgu,vgd,vlu,vld)
    !call scan_orb_free_x('SCAL',nu,nd,gnu,gnd,lu,ld,&
    ! & ex,vxu,vxd,vxgu,vxgd,vxlu,vxld)
      vgn = 0._dp
  else if (wen == 2) then
    call lap_lev_c(nu,nd,gnu,gnd,gn,lu,ld,&
      & en,vu,vd,vgu,vgd,vgn,vlu,vld)
    !call scan_orb_free_c('SCAL',nu,nd,gnu,gnd,gn,lu,ld,&
    ! & ec,vcu,vcd,vcgu,vcgd,vgn,vclu,vcld)
  end if

end subroutine llmgga
